
function deleteRecord(id,actionUrl,reloadUrl) {

    //id can contain comma separated ids too.

    if(confirm("Are you sure you want to delete?"))
    {
		$(".page_loader").show();
        $.ajax({
            type: "POST",
            url: base_url+''+actionUrl,
            data: {'id' : id, 'form_type' : 'delete'},
            dataType:"json",
            cache: false,
            //async:false,
            success: function(result){
                if(result.session_out == 'true')
                {
                    document.location.href = base_url+'admin';
                }

                if(result.error != 'false'){
                    $("#alert-message-heading").html('Error');
                    $("#alert-message").html(result.success);

                }else{
                    $("#alert-message-heading").html('Success');
                    $("#alert-message").html(result.success);
                    $("#"+id).hide();
                    $("."+id).hide();

                    if(reloadUrl!="") document.location.href = reloadUrl;
                }
                $(".page_loader").hide();
                $(".alert-message-button").click();

            }
        });
    }

}
function deletegalleryRecord(id,actionUrl,reloadUrl) {

    //id can contain comma separated ids too.

    if(confirm("Are you sure you want to delete?"))
    {
		$(".page_loader").show();
        $.ajax({
            type: "POST",
            url: base_url+''+actionUrl,
            data: {'id' : id, 'form_type' : 'deletegallery'},
            dataType:"json",
            cache: false,
            //async:false,
            success: function(result){
                if(result.session_out == 'true')
                {
                    document.location.href = base_url+'admin';
                }

                if(result.error != 'false'){
                    $("#alert-message-heading").html('Error');
                    $("#alert-message").html(result.success);

                }else{
                    $("#alert-message-heading").html('Success');
                    $("#alert-message").html(result.success);
                    $("#"+id).hide();
                    $("."+id).hide();

                    if(reloadUrl!="") document.location.href = reloadUrl;
                }
                $(".page_loader").hide();
                $(".alert-message-button").click();

            }
        });
    }

}
function deleteDepRecord(id,actionUrl,reloadUrl) {

    //id can contain comma separated ids too.

    if(confirm("Are you sure you want to delete?"))
    {
		$(".page_loader").show();
        $.ajax({
            type: "POST",
            url: base_url+''+actionUrl,
            data: {'id' : id, 'form_type' : 'DeleteDep'},
            dataType:"json",
            cache: false,
            //async:false,
            success: function(result){
                if(result.session_out == 'true')
                {
                    document.location.href = base_url+'admin';
                }

                if(result.error != 'false'){
                    $("#alert-message-heading").html('Error');
                    $("#alert-message").html(result.success);

                }else{
                    $("#alert-message-heading").html('Success');
                    $("#alert-message").html(result.success);
                    $("#"+id).hide();
                    $("."+id).hide();

                    if(reloadUrl!="") document.location.href = reloadUrl;
                }
                $(".page_loader").hide();
                $(".alert-message-button").click();

            }
        });
    }

}

function deleteRecordAll(id,actionUrl,reloadUrl, form_type)
{

	//id can contain comma separated ids too.
	
	if(confirm("Are you sure you want to delete?"))
	{
		$(".page_loader").show();
		$.ajax({
				type: "POST",
				url: base_url+''+actionUrl,
				data: {'id' : id, 'form_type' : form_type},
				dataType:"json",
				cache: false,
				//async:false,
				success: function(result){
					if(result.session_out == 'true')
					{
						document.location.href = base_url+'admin';
					}
				
			if(result.error != 'false'){
				$("#alert-message-heading").html('Error');
				$("#alert-message").html(result.error);
				//$("#mySmallModalLabel").html('Error');
				//$("#message").html(result.error);
				//document.getElementById("show_success_messge").click();
				
			}else{
				if(result.is_image_delete == 'true')
				{
					$('.image-'+id).remove();
					
				}else
				{
					$('.'+id).remove();
					
				}
				
				
				$("#alert-message-heading").html('Success');
				$("#alert-message").html(result.success);
				$("#"+id).hide();
				//$("#mySmallModalLabel").html('Success');
				//$("#message").html(result.success);
				//document.getElementById("show_success_messge").click();
				if(reloadUrl!="") document.location.href = reloadUrl;
				}
				$(".page_loader").hide();
			$(".alert-message-button").click();	
				
			}
		});
	}
	
}

$(".showDropDowns").on("click", function(){
	$('.hideDropdown').hide();
	
	var type = $(this).val();	
	if(type == '1')
	{
		$('#country_base').show();
	}
	else
	{
		$('#city_base').show();
		$('#city_base2').show();		
	}
});
$(".btn_cross").on("click", function(){
		$(this).parent().parent().parent().remove();
	});
	
	$('.reg_country').on('change', function(event) {

        var person = {
            country_code: $(this).val()
        };
		
		var loading_text = 'Loading...';
		
		$(".reg_city").html('');
		$(".reg_city").html('<option>'+loading_text+'</option>');
        $.ajax({
            url: base_url+"admin/ajax/getCities_html",
            type: 'post',
            dataType: 'json',
            success: function (data) {

                $(".reg_city").html(data.html);
            },
            data: person
        });
    });
	
	$('.reg_country_selectize').on('change', function(event) {

        var person = {
            country_code: $(this).val()
        };
		
		var loading_text = 'Loading...';
		
		$(".reg_city").html('');
		$(".reg_city").html('<option>'+loading_text+'</option>');
        $.ajax({
            url: base_url+"admin/ajax/getCitiesSelectize_html",
            type: 'post',
            dataType: 'json',
            success: function (data) {

				var options = [];
				var $select = $(document.getElementById('city')).selectize(options);
				var selectize = $select[0].selectize;
				selectize.clearOptions();
				$.each(data, function(key,value){
					selectize.addOption({value:value.opvalue,text:value.optext});
					selectize.refreshOptions();
				});
				
            },
            data: person
        });
    });
	
	$('.reg_country_mutiple').on('change', function(event) {
		if($('.city_ids').val() == 'add')
		{
			var person = {
				country_code: $(this).val()
			};
		}
		else
		{
			var person = {
				country_code: $(this).val(),
				city_ids: $(".city_ids").map(function(){ return this.value;}).get()
			};

		}
		var loading_text = 'Loading...';
		
		$(".reg_city_mutiple").html('');
		$(".reg_city_mutiple").html('<option>'+loading_text+'</option>');
        $.ajax({
            url: base_url+"admin/ajax/getCitiesMultiple_html",
            type: 'post',
            dataType: 'json',
            success: function (data) {

                $(".reg_city_mutiple").html(data.html);
            },
            data: person
        });
    });
	
$(document).on('submit','.ajax_form',function(e){
	
	
	for (instance in CKEDITOR.instances) {
		CKEDITOR.instances[instance].updateElement();
	}

 //e.preventDefault();
 $form = $(this);

 $form.find(':submit,:button').prop('disabled', true);
 buttonText = $form.find(':submit,:button').attr('value');
 $form.find(':submit,:button').attr('value', $form.find(':submit,:button').attr('temp-text'));

 btnText = $form.find('.btnText').html();
 $form.find('.btnText').html($form.find('.btnText').attr('temp-text'));
 
 $(".page_loader").show();

 $.ajax({
   type: $form.attr('method'),
   url: $form.attr('action'),
   data: new FormData( this ),
   dataType:"json",
   cache: false,
   contentType: false,
      processData: false,
   success: function(result){
	console.log(result);
    if(result.success == 'false'){  
		
     $("#validation-message-heading").html('Error');
	 $("#validation-message").html(result.error);
     $form.find(':submit,:button').attr('value', buttonText);
     $form.find('.btnText').html(btnText);
     $form.find(':submit,:button').prop('disabled', false);
	 $(".validation-message-button").click();
	 $(".page_loader").hide();
	 
    }else{
     
     if(result.reload=='1')
     {
      window.location.reload();
     }
     else if(result.redirect=='1')
     {
      window.location.href = base_url+result.url;
     }else if(result.session_out == 'true')
	 {
		
		document.location.href = base_url+'admin';
	 }
     else
     {
      if(result.is_images == 'true')
	  {
		 $("#images-light-box").append(result.images_html);
		 $("#light_box_of_images").append(result.images_light_box_html);
		 
		
	  }
	 
	  $form.find(':submit,:button').attr('value', buttonText);
      $form.find('.btnText').html(btnText);
      $form.find(':submit,:button').prop('disabled', false);
	  $form.find('#file_upload-select').val('');
	  $("#alert-message-heading").html('Success');
	  $("#alert-message").html(result.success);
	  $(".alert-message-button").click();
      
		/*if(result.reset != 0)
	  {
		  setTimeout(function(){  
			  location.reload();  
		  }, 3000);
	  }*/
      if(result.reset==1)
      {
		
       	$form[0].reset();
      }    
	  $(".page_loader").hide();
     }

    }
   
   }
 });
});


$( document ).ready(function() {

	$('.admin-modal').on({'hide.uk.modal': function(){
        location.reload();
    }
	});
	
	$('.admin-modal').click(function(){
		location.reload();
	});

    $('.submit_ajax_form').click(function(){
		$('.ajax_form').submit();
	});
	
	$('.all_types').click(function(){
		$('.all_types').removeClass('active');
		$(this).addClass('active');
		var div_id = $(this).attr('dir');
		$('.hide_all').hide('slow');
		$('#'+div_id).show('slow');
	});
	
	$('.all_types_orders').click(function(){
		$('.all_types_orders').removeClass('active');
		$(this).addClass('active');
		var div_id = $(this).attr('dir');
		$('.hide_all_order').hide('slow');
		$('#'+div_id).show('slow');
	});
	
	$('.dt_default').dataTable( {
		
		'aoColumnDefs': [{
        'bSortable': false,
        'aTargets': ['nosort']
		}]
		
	} );

    var table = $('.dt_default_cat').DataTable({
        rowReorder: true
    });

    $('.dt_default_cat').on('row-reorder.dt', function(dragEvent, data, nodes) {

		var po1  = parseInt(data[0].node._DT_RowIndex)+parseInt(1);
		var po2 = parseInt(data[1].node._DT_RowIndex)+parseInt(1);
		var pos = po1+','+po2;
        var id1 = data[1].node.id;
        var id2 = data[0].node.id;
		var ids = id1+','+id2;

        $(".page_loader").show();

        $.ajax({
            type: "POST",
            url: base_url+'admin/ajax/rearrangeCategories',
            data: {'ids' : ids,'pos':pos},
            dataType:"json",
            cache: false,
            success: function(result){
                $(".page_loader").hide();
                window.location.reload();
            }
        });


    });

    var table = $('.dt_default_slider').DataTable({
        rowReorder: true
    })


    $('.dt_default_slider').on('row-reorder.dt', function(dragEvent, data, nodes) {

        var po1  = parseInt(data[0].node._DT_RowIndex)+parseInt(1);
        var po2 = parseInt(data[1].node._DT_RowIndex)+parseInt(1);
        var pos = po1+','+po2;
        var id1 = data[1].node.id;
        var id2 = data[0].node.id;
        var ids = id1+','+id2;

        $(".page_loader").show();

        $.ajax({
            type: "POST",
            url: base_url+'admin/ajax/rearrangeSlider',
            data: {'ids' : ids,'pos':pos},
            dataType:"json",
            cache: false,
            success: function(result){
                $(".page_loader").hide();
                window.location.reload();
            }
        });


    });


    //$('.listing').DataTable();
			
	// daily offer start end date hide show
	
	$( ".daily_offer .switchery" ).on( "click", function() {	
		if($('#daily_offer:checkbox:checked').length > 0)
		{
			$('.daily_offer_date').show();
		}else
		{
			$('.daily_offer_date').hide();
		}
  
	});
	$( ".daily_offer .inline-label" ).on( "click", function() {	
		if($('#daily_offer:checkbox:checked').length > 0)
		{
			$('.daily_offer_date').hide();
		}else
		{
			$('.daily_offer_date').show();
		}
  
	});
	
	// end daily offer start end date hide show
	
	$("#light_box_of_images").html(light_box_images);
	// remove add variant group box if you add for newly added not for edit 
	$(document).on('click', ".closeN", function () {
		$( this ).parent(".innerEd").parent( ".remove-group" ).remove();
    });
	
	
	
	// discount price hide and show
	
	$( ".discount_price .switchery" ).on( "click", function() {	
		if($('#discount_price:checkbox:checked').length > 0)
		{
			$('.discount_price_field').show();
		}else
		{
			$('.discount_price_field').hide();
		}
  
	});
    $( ".discount_price .inline-label" ).on( "click", function() {
		if($('#discount_price:checkbox:checked').length > 0)
		{
			$('.discount_price_field').hide();
		}else
		{
			$('.discount_price_field').show();
		}

	});

});

$(document).on('submit','.api_settings_form',function(e){
	
        e.preventDefault();
        $("#submit_api_form").attr('disabled', 'disabled');
        $form = $(this);
        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: $form.serialize(),
            dataType: "json",
            cache: false,
            success: function (result) {
				if(result.error == true){
					$('.md-settings').html(result.success);
					$("#submit_api_form").removeAttr('disabled');
					$(".md-settings").delay(5000).fadeOut('slow');
				}else{
					$('.md-settings').html(result.success);
					$("#submit_api_form").removeAttr('disabled');
					$(".md-settings").delay(5000).fadeOut('slow');
				}
                
            }
        });
    
});

$(document).on('change', '.images_upload', function() {

   var files = $(this).prop("files");
   
   $(this).parent().parent().children("p.image_name").remove();
   
   for(var i=0; i<files.length; i++) {

    var f = files[i];

    $(this).parent().before('<p class="uk-text image_name">'+f.name+'</p>');

   }

});

function activeDropdown(id, cur)
{
	if($("#active_"+id).is(':checked'))
	{
		$('#varient_divs_'+id).show();
		$('#varient_divs_'+id).children('input').removeAttr('disabled');
	}
	else
	{
		$('#varient_divs_'+id).hide();
		$('#varient_divs_'+id).children('input').attr('disabled', 'disabled');
	}
}

function runJquery(cur, attName)
{
	if($(cur).children('.is_child').val() == 1)
	{
		$(cur).children('.is_child').attr('name', attName);
		$(cur).children('.is_child').val('0');
	}
	else
	{
		$(cur).children('.is_child').removeAttr('name');
		$(cur).children('.is_child').val('1');
	}
}
function getProduct(category_id)
{
	var person = {
            category_id: category_id
	}
	$.ajax({
            url: base_url+"admin/ajax/getProductsByCategory",
            type: 'post',
            dataType: 'json',
            success: function (data) {	
				if(data.success == '1')
				{
				   $('#select_main_div').show();
				   $('#select_demo_6').html(data.html);
				}
				else
				{
					 $('#select_main_div').hide();
				}
			},
			data: person
	});
}

function changeOrderStatus(order_id,order_status,user_id)
{
	 $(".page_loader").show();
	var person = {
            order_status: order_status,
			order_id: order_id,
			user_id:user_id
	}
	$.ajax({
            url: base_url+"admin/ajax/updateOrderStatus",
            type: 'post',
            dataType: 'json',
            success: function (data) {	
			 $(".page_loader").hide();
				if(data.success == '1')
				{
				   $('#select_main_div').show();
				   $('#select_demo_6').html(data.html);
				    window.location.reload();
				}
				else
				{
					 $('#select_main_div').hide();
				}
			},
			data: person
	});
}


function deleteRecordByString(id,actionUrl,reloadUrl, tr_id)
{

	//id can contain comma separated ids too.
	
	if(confirm("Are you sure you want to delete?"))
	{
		$.ajax({
				type: "POST",
				url: base_url+''+actionUrl,
				data: {'id' : id, 'form_type' : 'delete'},
				dataType:"json",
				cache: false,
				//async:false,
				success: function(result){
					if(result.session_out == 'true')
					{
						document.location.href = base_url+'admin';
					}
				
			if(result.error != 'false'){
				$("#alert-message-heading").html('Error');
				$("#alert-message").html(result.error);
				//$("#mySmallModalLabel").html('Error');
				//$("#message").html(result.error);
				//document.getElementById("show_success_messge").click();
				
			}else{
				
				
				
				$("#alert-message-heading").html('Success');
				$("#alert-message").html(result.success);
				$("#"+tr_id).hide();
				//$("#mySmallModalLabel").html('Success');
				//$("#message").html(result.success);
				//document.getElementById("show_success_messge").click();
				if(reloadUrl!="") document.location.href = reloadUrl;
				}
				
			$(".alert-message-button").click();	
				
			}
		});
	}
	
}
function catFilter(cat_id)
{
	if(cat_id == '0')
	{
		$(".allproducts").show();
	}
	else
	{
		$(".allproducts").hide();
		$(".cat_"+cat_id).show();
	}
}

function validateDates()
{
	var start_date = $("#start_date").val();
	var end_date = $("#end_date").val();
	
	dateFirst = start_date.split('/');
	dateSecond = end_date.split('/');
	var value = new Date(dateFirst[2], dateFirst[1], dateFirst[0]); //Year, Month, Date
	var current = new Date(dateSecond[2], dateSecond[1], dateSecond[0]);
	if(value > current)
	{
		alert('End date must be greater then start date.');
		$("#end_date").val('');
		return false;
	}
	else{
		$('#purchased_form').submit();
	}
	
}

$('.countryClassCart').on('change', function(event) {

    var person = {
        country: $(this).val()
    };
    $.ajax({
        url: base_url+"ajax/getCities",
        type: 'post',
        dataType: 'json',
        success: function (data) {

            $(".cityClassCart").html(data.html);
        },
        data: person
    });
});


function editShipment(id,url) {

    $.ajax({
        url: base_url+''+url,
        type: 'post',
        dataType: 'json',
		data:{id:id},
        success: function (data) {

        	$('#shipment_add_btn').hide();

        	$('#form_type').val('update');
        	$('#id').val(id);
        	$('#shipment_btn').html('update');

        	$('#eng_name').val(data.shipments.eng_name);
        	$('#eng_description').val(data.shipments.eng_description);
        	$('#arb_name').val(data.shipments.arb_name);
            $('#arb_description').val(data.shipments.arb_description);

        	$('#amount').val(data.shipments.amount);
            $('.shipment_methods').show();

            $('.focus').each(function (e) {
                $(this).focus();
            });

        }
    });

}


function editGift(id,url) {

    $.ajax({
        url: base_url+''+url,
        type: 'post',
        dataType: 'json',
        data:{id:id},
        success: function (data) {

            $('#gift_add_btn').hide();

            $('#gift_form_type').val('update_gift');
            $('#gift_id').val(id);
            $('#gift_btn').html('update');

            $('#gift_eng_name').val(data.gifts.eng_name);

            $('#gift_arb_name').val(data.gifts.arb_name);


            $('#gift_amount').val(data.gifts.amount);
            $('.gifts_types').show();

            $('.gift_focus').each(function (e) {
                $(this).focus();
            });

        }
    });

}



function editWire(id,url) {

    $.ajax({
        url: base_url+''+url,
        type: 'post',
        dataType: 'json',
        data:{id:id},
        success: function (data) {

            $('#wire_add_btn').hide();

            $('#wire_form_type').val('update_wire');
            $('#wire_id').val(id);
            $('#wire_btn').html('update');

            $('#eng_bank_name').val(data.wire.eng_bank_name);

            $('#arb_bank_name').val(data.wire.arb_bank_name);

            $('#account_no').val(data.wire.account_no);
            $('#iban_no').val(data.wire.iban_no);

            $('.wire_transfer').show();

            $('.wire_focus').each(function (e) {
                $(this).focus();
            });

        }
    });

}




$(document).on('submit','#login',function(e){   
	      $("#btn_login").attr('disabled');
		 e.preventDefault();   
		 $form = $(this);     
			 $.ajax({         
			 type: "POST",  
			 url: $form.attr('action'),  
			 data: $form.serialize(),  
			 dataType: "json",        
			 cache: false,          
			 success: function (result) {    
			 if(result.error == 0){ 			 
			 $(".login_msg").html(result.message);  
			 document.location.href = base_url+'admin';   
			 }else{ 
			 $(".login_msg").html(result.message);    
			 $("#btn_login").removeAttr('disabled');     
			 }          
			 }     
			 });   
	 
 });
 
 
 function viewContctMessage(id){
 
 	$('#contact_us-message-button-'+id).click();

 };



 $(".order_search").change(function () {
	 $(".order_search_value").val($(this).val());
	 $(".search_order_form").submit();
 });

$(".product_search").change(function () {
    $(".product_search_value").val($(this).val());
    $(".product_search_form").submit();
});

 function removeReceipt(order_id) {
 	if(confirm('Are you sure you want to remove this?')) {
        $(".page_loader").show();
        $.ajax({
            type: "POST",
            url: base_url + 'admin/orders/remove_receipt',
            data: {order_id: order_id},
            dataType: "json",
            cache: false,
            success: function (result) {
                $(".page_loader").hide();
                if (result.error == false) {
                    $("#validation-message-heading").html('Success');
                    $("#validation-message").html(result.message);
                    $(".validation-message-button").click();
                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                } else {
                    $("#validation-message-heading").html('Error');
                    $("#validation-message").html('<p>There was an Error to remove this</p>');
                    $(".validation-message-button").click();
                }
            }
        });
    }

 }

$(".get_product_id").on('click', function () {

	var id = $(this).attr('id');
	var prod_id = $(this).data('prod_id');
	if(prod_id == '') {
        $(this).data('prod_id', id);
    }else{
        $(this).data('prod_id', '');
	}

});


 $('#applyBtn').on('click',function () {
     var object = {};
     var product_ids = '';
     $(".get_product_id").each(function (index,value) {
         var product_id = $(this).data('prod_id');
         if(product_id != "") {

             $("#massDiscountBtn").click();

             product_ids += product_id+',';

         }
     });
     if(product_ids == ''){
         $("#validation-message-heading").html('Error');
         $("#validation-message").html('Please select at least one product');
         $(".validation-message-button").click();
	 }
     $("#product_ids").val(product_ids);
 });

 $('.discount_form').submit(function () {

 	$form = $(this);

     $(".page_loader").show();

     $.ajax({
         type: $form.attr('method'),
         url: $form.attr('action'),
         data: $form.serialize(),
         dataType:"json",
         cache: false,
         success: function(result){
             if(result.success == true){
                 $("#alert-message-heading").html('Success');
                 $("#alert-message").html(result.message);
                 $(".alert-message-button").click();

             }else{
                 $("#validation-message-heading").html('Error');
                 $("#validation-message").html(result.message);
                 $(".validation-message-button").click();
             }
             $(".page_loader").hide();


         }
     });

 });

/*prevent to enter value instead of select and put in hidden field*/
$( document ).ready(function() {
    $(".multi_selector :input").attr("readonly", true);

    $('.color_id').change(function () {
        var value = $(this).val();
        if(value == null){
            value = '';
        }
        $('#color_id').val(value);

    });
});


$('.arb_meta_keyword').change(function () {
    var value = $(this).val();
    if(value == null){
        value = '';
    }
    $('#arb_meta_keyword').val(value);

});

$('.eng_product_keyword').change(function () {
    var value = $(this).val();
    if(value == null){
        value = '';
    }
    $('#eng_product_keyword').val(value);

});
$('.arb_product_keyword').change(function () {
    var value = $(this).val();
    if(value == null){
        value = '';
    }
    $('#arb_product_keyword').val(value);

});


$(document).on('keyup','.mass_discount_field', function(e){

    this.value = this.value.replace(/[^0-9\.]/g,'');
});


function deleteProdImage(image_id) {

    if(confirm('Are you sure you want to delete ?')) {
        //$(".page_loader").show();

        $.ajax({
            type: 'post',
            url: base_url+'admin/product/deleteImage',
            data: {id : image_id},
            dataType: "json",
            cache: false,
            success: function (result) {

                if (result.error == false) {
                    $("#alert-message-heading").html('Success');
                    $("#alert-message").html(result.success);
                    $(".alert-message-button").click();

                }
                $(".page_loader").hide();
				
            }
        });
    }
}

$(".del_img").on('click', function() { 

	var id = $(this).attr('id');
	var img =  $(this).attr('imges');
console.log(img);
    if(confirm('Are you sure you want to delete ?')) {
        //$(".page_loader").show();

        $.ajax({
            type: 'post',
            url: base_url+'admin/projects/delete_project_image',
            data: {id : id,img:img},
            dataType: "json",
            cache: false,
            success: function (result) {

                if (result.error == false) {
                    $("#alert-message-heading").html('Success');
                    $("#alert-message").html(result.success);
                    $(".alert-message-button").click();
                       //location.reload();
                }
                $(".page_loader").hide();
				
            }
        });
    }


 });


	// change fileds by checkboxes status
$(document).ready(function () {


    var visa = $('.visa_methods').is(':checked');
    var sadad = $('.sadad_methods').is(':checked');
    var cash = $('.cash_methods').is(':checked');
    var wire = $('.wire_methods').is(':checked');


    if (wire) {
        $('.wire_fields').show();

        $('.sadad_fields').hide();
        $('.cash_fields').hide();
        $('.visa_fields').hide();

        $('.wire_method_tab').prop('checked',true);
        $('.visa_method_tab').prop('checked',false);

        $('.sadad_method_tab').prop('checked',false);
        $('.cash_method_tab').prop('checked',false);

    }
     if (cash) {
        $('.cash_fields').show();

        $('.visa_fields').hide();
        $('.sadad_fields').hide();
        $('.wire_fields').hide();

         $('.cash_method_tab').prop('checked',true);
         $('.visa_method_tab').prop('checked',false);
         $('.sadad_method_tab').prop('checked',false);
         $('.wire_method_tab').prop('checked',false);
    }
     if (sadad) {
        $('.sadad_fields').show();

        $('.visa_fields').hide();
        $('.cash_fields').hide();
        $('.wire_fields').hide();

         $('.sadad_method_tab').prop('checked',true);
         $('.visa_method_tab').prop('checked',false);
         $('.cash_method_tab').prop('checked',false);
         $('.wire_method_tab').prop('checked',false);
    }
     if (visa) {
        $('.visa_fields').show();

        $('.wire_fields').hide();
        $('.sadad_fields').hide();
        $('.cash_fields').hide();

        $('.visa_method_tab').prop('checked',true);
        $('.sadad_method_tab').prop('checked',false);
        $('.cash_method_tab').prop('checked',false);
        $('.wire_method_tab').prop('checked',false);


    }


    $(".visa_method_tab").click(function () {
    	var check = $(this).is(':checked');
    	if(check){
            $('.visa_fields').show();

            $('.wire_fields').hide();
            $('.sadad_fields').hide();
            $('.cash_fields').hide();
		}

    });
    $(".sadad_method_tab").click(function () {
        var check = $(this).is(':checked');
        if(check){
            $('.sadad_fields').show();

            $('.wire_fields').hide();
            $('.visa_fields').hide();
            $('.cash_fields').hide();
        }

    });
    $(".cash_method_tab").click(function () {
        var check = $(this).is(':checked');
        if(check){
            $('.cash_fields').show();

            $('.wire_fields').hide();
            $('.visa_fields').hide();
            $('.sadad_fields').hide();
        }

    });
    $(".wire_method_tab").click(function () {
        var check = $(this).is(':checked');
        if(check){
            $('.wire_fields').show();

            $('.cash_fields').hide();
            $('.visa_fields').hide();
            $('.sadad_fields').hide();
        }

    });
	
});

function getAramexInvoice(order_id,tracking_id,total_amount,user_id) {

    $.ajax({
        type: "POST",
        url: base_url+'admin/ajax/getAramexInvoice',
        data: {'order_id':order_id,'tracking_id' : tracking_id,'total_amount':total_amount, 'user_id' : user_id},
        dataType:"json",
        cache: false,
        success: function(result){
            window.open(result, '_blank');
        }
    });
}



$(document).ready(function () {

    $(document).on("click",".location",function () {
        $(this).data('exist', '');
		$(".location-pick-button").click();

        initialize();
    });

    initialize();

});

var myOptions = '';
var gmarkers = [];

function initialize() {

    var map;
    var bounds = new google.maps.LatLngBounds();
    var myLatlng = new google.maps.LatLng(24.720403782761128,46.678762435913086);
    var myOptions = {
        zoom: 6,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControlOptions: {position: google.maps.ControlPosition.CENTRE}
    };
    map = new google.maps.Map(document.getElementById("location_map"), myOptions);
    google.maps.event.addListener(map, "click", function(event) {
        removeMarkers();
        var lat = event.latLng.lat();
        var lng = event.latLng.lng();

        placeMarker(event.latLng);
        putLatLong(lat,lng);
    });

    function placeMarker(location) {
        var marker = new google.maps.Marker({
            position: location,
            map: map
        });
        gmarkers.push(marker);
    }

    function removeMarkers() {
        for (i = 0; i < gmarkers.length; i++) {
            gmarkers[i].setMap(null);
        }
    }
}

function putLatLong(lat,lng) {

	if($(".location").data("exist") !== undefined){
        //var exist = $(".location").data('exist');
        $(".location").each(function () {
            var exist = $(this).data('exist');
            if (exist == '') {
                $(this).val(lat + "," + lng);
                $(this).focus();
            }
        });

        $(".location").data('exist', '1');

    }else{
        $(".location").val(lat + "," + lng);
        $(".location").focus();
    }
}

$(document).on("click","#close_popup",function(){
    $('#modal_location .uk-modal-close').click();
});


$(".menu_type").change(function(){
	var value = $(this).val();
	if(value == 'footer') {

        $(".footer_menu_type").attr("name", "footer_menu_type");
        $(".footer_menu_type_show").show();

        $(".header_sub_menu").removeAttr("name");
        $(".header_sub_menu_show").hide();
    }else{

        $(".header_sub_menu").attr("name", "header_sub_menu");
        $(".header_sub_menu_show").show();
        $(".footer_menu_type").removeAttr("name");
        $(".footer_menu_type_show").hide();
	}
});


$(".is_link").change(function(){
	if($(this).val() == '2'){
		$(".links").hide();
		$(".popup_type_sec").show();
		$(".popup_type").attr("disabled",false);
		$("#eng_link").attr("disabled",true);
		$("#arb_link").attr("disabled",true);
	}else{
        $(".links").show();
        $(".popup_type_sec").hide();
        $(".popup_type").attr("disabled",true);
        $("#eng_link").attr("disabled",false);
        $("#arb_link").attr("disabled",false);
	}
});