$(document).ready(function () {

    $(".loginForm").submit(function () {

		$(".loader").show();

        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            dataType: "json",
            data: $(this).serialize(),
            success: function (response) {

				$(".loader").hide();

                if(response.username == ''){

                    $('#username').addClass('has-error');
                }else{

                    $('#username').removeClass('has-error');
                }
                if(response.password == ''){
                    $('#password').addClass('has-error');
                }else{
                    $('#password').removeClass('has-error');
                }
                if(response.success == 1){

                    $('.loginMsg').hide();
                    window.location.href = response.redirectUrl;
                }else{
                    $('.loginMsg').html(response.message);
                    $('.loginMsg').show();
                }
            }
        });

    });

    $(".loginFormSubmit").submit(function () {

        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            dataType: "json",
            data: $(this).serialize(),
            success: function (response) {

                if(response.username == ''){

                    $('#userName').addClass('has-error');
                }else{

                    $('#userName').removeClass('has-error');
                }
                if(response.password == ''){
                    $('#passWord').addClass('has-error');
                }else{
                    $('#passWord').removeClass('has-error');
                }
                if(response.success == 1){

                    $('.loginMessage').hide();
                    window.location.href = response.redirectUrl;
                }else{
                    $('.loginMessage').html(response.message);
                    $('.loginMessage').show();
                }
            }
        });

    });



    $('.countryClassCart').on('change', function(event) {

        var person = {
            country: $(this).val()
        };
		if(lang == 'eng'){
			var loading_text = 'Loading...';
		}else{
			var loading_text = 'تحميل...';
		}
		$(".cityClassCart").html('');
		$(".cityClassCart").html('<option>'+loading_text+'</option>');
        $.ajax({
            url: base_url+"ajax/getCities",
            type: 'post',
            dataType: 'json',
            success: function (data) {

                $(".cityClassCart").html(data.html);
                $(".page_loader").hide();
            },
            data: person
        });
    });

	$('.reg_country').on('change', function(event) {

        var person = {
            country_code: $(this).val()
        };
		if(lang == 'eng'){
			var loading_text = 'Loading...';
		}else{
			var loading_text = 'تحميل...';
		}
		$(".reg_city").html('');
		$(".reg_city").html('<option>'+loading_text+'</option>');
        $.ajax({
            url: base_url+"ajax/getCities_html",
            type: 'post',
            dataType: 'json',
            success: function (data) {

                $(".reg_city").html(data.html);
				$("#phone_no").intlTelInput("setCountry", data.ios);
				$("#mobile_no").intlTelInput("setCountry", data.ios);
                $(".page_loader").hide();
            },
            data: person
        });
    });


// get cities for add address
    $('.address_country').on('change', function(event) {

        var person = {
            country_code: $(this).val()
        };
        if(lang == 'eng'){
            var loading_text = 'Loading...';
        }else{
            var loading_text = 'تحميل...';
        }
        $(".reg_city").html('');
        $(".reg_city").html('<option>'+loading_text+'</option>');
        $.ajax({
            url: base_url+"ajax/get_cities_html_address",
            type: 'post',
            dataType: 'json',
            success: function (data) {

                $(".reg_city").html(data.html);
                $("#phone_no").intlTelInput("setCountry", data.ios);
                $("#mobile_no").intlTelInput("setCountry", data.ios);
                $(".page_loader").hide();
                if(data.other_country){
                    $("#region").attr('name','region');
                    $("#zip_code").attr('name','zip_code');
                    $("#showZipState").show('slow');
                }else{
                    $("#region").removeAttr('name');
                    $("#zip_code").removeAttr('name');
                    $("#showZipState").hide('slow');
                }
            },
            data: person
        });
    });




    $(".addAddressReset").click(function(){

        var id = $(this).attr('id');
        if(id == 'profile_address') {
            $('#address_type').val('register_address');
        }else if(id == 'shipping_address'){
            $('#address_type').val('cart_address');
        }
        $(".shippingAddForm")[0].reset();
        $("#city").html('<option value="">Select</option>');

    });

	$(".giftCard").click(function(){

		$(".loader").show();
		var grandTotal = 0;
		var giftData = $(this).val().split('|');

		var old_price_total = $("#old_price_total_val").val();
		//alert(giftData[1])
		var setCookie;
		if ($(this).is(':checked')) {
            var oldPrice = parseFloat(old_price_total)+parseFloat(giftData[1]);
			setCookie = 1;
			grandTotal =  parseFloat($("#total_prod_price_sum_val").val())+parseFloat(giftData[1]);

			$("#old_price_total").html(parseFloat(Math.round(oldPrice * 100) / 100).toFixed(2));
            $("#old_price_total_val").val(parseFloat(Math.round(oldPrice * 100) / 100).toFixed(2));

            $("#total_prod_price_sum").html(parseFloat(Math.round(grandTotal * 100) / 100).toFixed(2));
            $("#total_prod_price_sum_val").val(parseFloat(Math.round(grandTotal * 100) / 100).toFixed(2));
		}
		else
		{
			setCookie = 0;
            var oldPrice = parseFloat(old_price_total)-parseFloat(giftData[1]);

			grandTotal =  parseFloat($("#total_prod_price_sum_val").val())-parseFloat(giftData[1]);
			$("#total_prod_price_sum").html(parseFloat(Math.round(grandTotal * 100) / 100).toFixed(2));

			$("#old_price_total").html(parseFloat(Math.round(oldPrice * 100) / 100).toFixed(2));
			$("#old_price_total_val").val(parseFloat(Math.round(oldPrice * 100) / 100).toFixed(2));

			$("#total_prod_price_sum_val").val(parseFloat(Math.round(grandTotal * 100) / 100).toFixed(2));
		}

		var person = {
            id: giftData[0],
			amount: giftData[1],
			setCookie: setCookie
        };
        $.ajax({
            url: base_url+"ajax/addGiftCard",
            type: 'post',
            dataType: 'json',
            success: function (data) {
				if(data.status == 1)
				{
                	$(".loader").hide();
				}
			},
            data: person
        });
	});

 	$(".payment_method").click(function(){

        var method = $(this).attr('data-back-btn-text');
		$("#card_method_new").val(method);
		var totalAmount
		if(method == 'COD')
		{
			$("#cashChargeTr").show();
			totalAmount = $("#cod_price").val();
		}
		else
		{
			$("#cashChargeTr").hide();
			totalAmount = $("#org_price").val();
		}
		$("#priceTotal").html(totalAmount);

    });

	$(".proceed_checkout").click(function(){
		$(".submit_form").submit();
		$(this).attr('disabled', 'disabled');
    });
	$("#regAddress").click(function(){
	 if($(this).is(':checked'))
	 {
		 $("#showPassword").show('slow');
	 }
	 else
	 {
		 $("#showPassword").hide('slow');
	 }
	});

	/*for displaying loyalty popup*/
    var fullUrl = window.location.href;
    var lastUrl = fullUrl.substring(fullUrl.lastIndexOf('/') + 1);
    var lastUrlO = fullUrl.substring(fullUrl.lastIndexOf('/')-11);
    var secodUrl = lastUrlO.split('/');
   if(lastUrl == 'shippingAddress' || secodUrl[0] == 'orderPlaced') {
       var loyaltyPopup = $("#loyaltyPopupTotal").val();

       if (typeof loyaltyPopup !== "undefined") {
           if(secodUrl[0] == 'orderPlaced'){
               var page = secodUrl[0];
           }
           if(lastUrl == 'shippingAddress'){
               var page = lastUrl
           }
           var relodOnce = localStorage.getItem('relodOnce');
           if (!relodOnce) {
               $.ajax({
                   url: base_url + "ajax/getLoyaltyPopup",
                   type: 'post',
                   dataType: 'json',
                   data: {'loyaltyPopup': loyaltyPopup,'page':page},
                   success: function (data) {
                       if (data.loyalty) {
                           $('.alert-success').html(data.message);
                           $('#loyaltyPopup').modal('show');
                       }
                   }
               });
           }
           localStorage.setItem('relodOnce', 'relodOnce');
       }
   }else {
       localStorage.removeItem('relodOnce');

   }


});


function deleteCartAddress(id, form_type , confirm_msg)
{
    $('.loader').show();
    if(confirm(confirm_msg))
    {

        $.post( base_url+"ajax/action",
            { id: id, form_type: 'delete_address' },
            function( data ) {
                $('.loader').hide();
            var response = jQuery.parseJSON(data);
            if(response.success == 1)
            {
                location.reload();
            }
            else
            {

            }
        });
    }else{
		$('.loader').hide();
	}
}



function showEdit(id,address_type)
{
    $('.loader').show();
    $.ajax({
        type: 'POST',
        url: base_url+'ajax/getAddressForEdit',
        data: {address_id:id,'address_type':address_type},
        dataType:"json",
        success: function(result){
 			$("#full_name").val(result.full_name);
            $("#email").val(result.email);
            $("#mobile_no").val(result.phone_no);
            $("#address_title").val(result.address_title);
            $("#address_1").val(result.address_1);
            $("#address_2").val(result.address_2);
            $("#country").val(result.country);
            $("#showZipState").hide();
            $("#region").removeAttr('name');
            $("#zip_code").removeAttr('name');

            $.ajax({
                url: base_url+"ajax/get_cities_html_address",
                type: 'POST',
                data:{'country_code':result.country, 'city_id':result.city},
                dataType: 'json',
                success: function (data) {
                    $('.loader').hide();

                    $(".reg_city").html(data.html);
                    $("#phone_no").val(result.phone_no);
                    $("#update_id").val(result.id);
                    if(result.region != '' && result.zip_code != '') {
                        $("#region").val(result.region);
                        $("#zip_code").val(result.zip_code);
                        $("#region").attr('name','region');
                        $("#zip_code").attr('name','zip_code');
                        $("#showZipState").show('slow');
                    }


                    $("#addAddress").modal('show');
					$('#addAddress').on('shown.bs.modal', function (e) {

						$("#mobile_no").intlTelInput({

						  //utilsScript: "build/js/utils.js"

						});

						$("#phone_no").intlTelInput({

						  //utilsScript: "build/js/utils.js"

						});

					});
                }
            });


        }
    });
}

function addressId(id, country_code, city_id)
{
	$('.loader').show();
	$.ajax({
		  url: base_url+"ajax/getShipmentGroup",
		  type: 'POST',
		  data:{'country_code':country_code, 'city_id':city_id,'address_id':id},
		  dataType: 'json',
		  success: function (data) {

            if(data.error != ''){
                $('.loader').hide();
            var msg = '<i class="fa fa-exclamation-triangle errorMsgBody" aria-hidden="true"></i>'+data.error;
                $('.alert-danger').html(msg);
                $('#errorMSG').modal('show');
                $('#shipping_box').hide();
                return false;
            }
            if(data.html != false)
            {
            $('#shipping_box').html(data.html);
            $('#shipping_id_val').val(data.group_id);
            $('#shipping_box').show();
            // set value of shipping_method for shipment
            $('.shipment_method_select').click(function () {
                var shipping_val = $(this).val();
                $('#shipping_method').val(shipping_val);
            });
            }
            else
            {
              $('#shipping_box').hide();
            }
            $('.loader').hide();

		  }
	  });
      $('#address_id_val').val(id);

}

function submitForm(lang,Total)
{
    var shipping = $("#shipping_method").val();
    var arr = shipping.split('|');

    var grandTotal = parseInt(arr[1])+parseInt(Total);
    $('#temp_total_amount').val(grandTotal);

	if(lang == 'eng')
	{
		var address = '<i class="fa fa-exclamation-triangle errorMsgBody" aria-hidden="true"></i>Please select or add your address';
		var shipment = '<i class="fa fa-exclamation-triangle errorMsgBody" aria-hidden="true"></i>Please select shipment method';
	}
	else
	{
		var address = '<i class="fa fa-exclamation-triangle errorMsgBody" aria-hidden="true"></i>يرجى تحديد أو إضافة عنوانك';
		var shipment = '<i class="fa fa-exclamation-triangle errorMsgBody" aria-hidden="true"></i>يرجى تحديد طريقة الشحن';
	}
    var address_val = $("#address_id_val").val();

	if(address_val == '' || address_val == null)
	{
		$('.alert-danger').html(address);
        $('#errorMSG').modal('show');
		return false;
	}
	else if($("#shipping_method").val() == '')
	{
		$('.alert-danger').html(shipment);
        $('#errorMSG').modal('show');
		return false;
	}
	else
	{
		$("#address_form").submit();
	}
}

// select the shipping method for shipping
/*function selectShipmentMethod() {
    var status = false;
    $('.shipment_method_select').each(function () {
      var check = $(this).is(':checked');
      if(check == true) {
          status = check;
      }
    });

    return status;

}*/



function validationCartAddress() {

    var address_1 = $('#address_1').val();
    var country = $('#country').val();
    var city = $('#city').val();
    var phone_no = $('#phone_no').val();
    //Error Messages
    var mandatory_field = $('#mandatory_field').val(); // mandatory field
    var invalid_email = $('#invalid_email').val(); // invalid email
    if (!address_1) {
        $('#address_1').attr("data-original-title", mandatory_field);
        $('#address_1').tooltip('show');
        $('#address_1').focus();
    }else if (!country) {
        $('#country').attr("data-original-title", mandatory_field);
        $('#country').tooltip('show');
        $('#country').focus();
    }else if (!city) {
        $('#city').attr("data-original-title", mandatory_field);
        $('#city').tooltip('show');
        $('#city').focus();
    }else if (!phone_no) {
        $('#phone_no').attr("data-original-title", mandatory_field);
        $('#phone_no').tooltip('show');
        $('#phone_no').focus();
    }else {
        return true;

    }
}



function addToCart(id) {
    var quantity = $('.selQty').val();
	$('.loader').show();
    $.ajax({
        type: 'POST',
        url: lang_base_url+'shopping_cart/productAddToCart',
        dataType: "json",
        data: {'id' :id,'quantity':quantity},
        success: function (response) {
            if(response.success == 0) {

                $('.addToCartItem').html(response.message);
            }else{
                $('.addToCartItem').html(response.html);
				if($('.cartCount').html() == '')
				{
					$('.cartCount').html('1');
				}
				else
				{
					var cartcount = parseInt($('.cartCount').html())+1;
					$('.cartCount').html(cartcount)
				}
            }
			$('.loader').hide();
            $('.add2Cart').modal('show');
        }
    });



}



function delFromCart(temp_id, tr_id)
{
    var r = confirm("Are you sure you want to delete this product?");
    if(r)
    {
		$('.loader').show();
        $.ajax({
            url: base_url+"ajax/delOrderCart",
            type: 'post',
            dataType: 'json',
            data: {'prod_id':temp_id},
            success: function (data) {
				$('.loader').hide();
                if(data.deleted) {
					$('.cartCount').html(data.count);
					$('#total_prod_price_sum').html(numeral(data.grandTotal).format('0,0')+'.00');
                    $("#product_row_" + tr_id).remove();
					location.reload();
                }

            }
        });
    }
}


function calculateOrder(quan, item_no, prod_price, temp_order_id, cur)
{
    $('.loader').show();

	var subOldTotal = parseFloat($("#total_prod_price_sum_val").val())-parseFloat($("#item_totel_price_val_"+item_no).val());

	var totalPrice = parseInt(quan)* parseInt(prod_price);

	var grandTotal = parseFloat(subOldTotal)+parseFloat(totalPrice);

    $.post( base_url+"ajax/action",
        { quantity: quan, form_type: "save_quantity", temp_order_id:temp_order_id },
        function( data ) {

            $("#item_totel_price_"+item_no).html(totalPrice+'.00');
			$("#total_prod_price_sum").html(numeral(grandTotal).format('0,0')+'.00');

			$("#total_prod_price_sum_val").val(grandTotal);

            $('.loader').hide();
			location.reload();
        });


}



function saveForLater(temp_id, saved)
{
    var person = {
        temp_id: temp_id,
        saved: saved
    };
	$('.loader').show();
    $.ajax({
        url: base_url+"ajax/saveForLater",
        type: 'post',
        dataType: 'json',
        data: person,
        success: function (data) {
            location.reload();
			$('.loader').hide();
        }

    });
}


$(".searchByCat").click(function () {
    $('.loader').show();
    var values = '';
    $(".searchByCat:checked").each(function(){
        values += $(this).val()+',';
    });
    $.ajax({
        url: base_url+"ajax/searchByCategories",
        type: 'post',
        dataType: 'json',
        data: {cats:values},
        success: function (data) {

            $('.loader').hide();
            if(data != '') {
                $(".getProductByCat").html(data);
				equalheight('.productsListing > ul > li .whiteBox');
            }else{
                $(".getProductByCat").html('<div><li style="color: red; font-size: large; text-align: center;">' +
                    'No record found!</li></div>');
            }
        },
		error: function (jqXHR, exception) {

		}

    });

});

// applied job submition
$(".applied_jobs_prev").submit(function () {

    $form = $(this);

    $.ajax({
        url: $form.attr('action'),
        type: $form.attr('method'),
        dataType: 'json',
        processData: false,
        contentType: false,
        data: new FormData( this ),
        success: function (data) {
            $(".myModalLabel").html(data.message)
            $("#applyNowPU").modal('show');
            $form[0].reset();
        }

    });
});

//submit payment confirm form
$('.payment_confirm_prev').submit(function () {

    $('.loader').show();

    $form = $(this);

    $.ajax({
        url: $form.attr('action'),
        type: $form.attr('method'),
        dataType: 'json',
        processData: false,
        contentType: false,
        data: new FormData( this ),
        success: function (data) {
            $('.loader').hide();

            if(data.success) {

                $('.successMsgBody').html(data.message);
                $('#sussesMSG').modal('show');
                $form[0].reset();
            }else{
                $('.errorMsgBody').html(data.message);
                $('#errorMSG').modal('show');
            }
        }

    });

});


	// cancel orders from user profile by user
	function cancelOrder(order_id,message){
		if(confirm(message)){
			$('.loader').show();

			 $.ajax({
                url: base_url+"profile/cancel_order",
                type: 'POST',
                data:{'id':order_id},
                dataType: 'json',
                success: function (data) {
                    $('.loader').hide();

					if(data.success){
						$('.successMsgBody').html(data.message);
						$('#sussesMSG').modal('show');
					}else{
						$('.errorMsgBody').html(data.message);
                		$('#errorMSG').modal('show');
					}
					location.reload();
                }
            });

		}
	}


	// return order popup here
	function returnOrder(order_id){

		$("#returnOrder").modal('show');
		$("#return_order_id").val(order_id);
	}


	// return order request here
	$('.return_order_form').submit(function(event){

		event.preventDefault();

	if(validationOrderReturn()){

		$form = $(this);
		$('.loader').show();

		$.ajax({
			url:  $form.attr('action'),
        	type: $form.attr('method'),
			data: $form.serialize(),
			dataType: 'json',
			success: function (data) {
				$("#returnOrder").modal('hide');
				$('.loader').hide();

				if(data.success){
					$('.successMsgBody').html(data.message);
					$('#sussesMSG').modal('show');
				}else{
					$('.errorMsgBody').html(data.message);
					$('#errorMSG').modal('show');
				}
				location.reload();
			}
		});
	}else{

        if(lang == "eng"){
            var msg = "All fields are required";
        }else{
            var msg = "جميع الحقول مطلوبة";
        }
		$('.returnOrderMsg').html(msg);

	}

	});



	function validationOrderReturn() {

		var result = false;

		var reason = $('.reason').is(':checked');
		var package_status = $('.package_status').is(':checked');
		var comment = $('.comment').val();

		var mandatory_field = "Field is required";

	   		if (reason <= 0) {
			//$('.reason').attr("data-original-title", mandatory_field);
			//$('.reason').tooltip('show');
			$('.reason').focus();
		}else if (package_status<=0) {
			//$('.package_status').attr("data-original-title", mandatory_field);
			//$('.package_status').tooltip('show');
			$('.package_statusy').focus();
		}else if (comment == "") {
			//$('.comment').attr("data-original-title", mandatory_field);
			//$('.comment').tooltip('show');
			$('.comment').focus();
		}else {
			result =  true;

		}
	return result;
}



// save rating
$('.saveRating').click(function(){

	var product_rating = $(this).data('product_rating');
	var packaging_rating = $(this).data('packaging_rating');
	var packing_rating = $(this).data('packing_rating');
	var service_rating = $(this).data('service_rating');
	var shipment_rating = $(this).data('shipment_rating');
	var satisfaction_rating = $(this).data('satisfaction_rating');


	if (typeof product_rating  !== "undefined"){

		$('.stars_1').find('.saveRating').removeClass('active');
		for(var i = 0; i<product_rating; i++){

			$('.stars_1').find('.saveRating:eq("'+i+'")').addClass('active');

		}
		$('.product_rating').val(product_rating);
	}
	if (typeof packaging_rating  !== "undefined"){
		$('.stars_2').find('.saveRating').removeClass('active');
		for(var i = 0; i<packaging_rating; i++){

			$('.stars_2').find('.saveRating:eq("'+i+'")').addClass('active');

		}
		$('.packaging_rating').val(packaging_rating);
	}
	if (typeof packing_rating  !== "undefined"){
		$('.stars_3').find('.saveRating').removeClass('active');
		for(var i = 0; i<packing_rating; i++){

			$('.stars_3').find('.saveRating:eq("'+i+'")').addClass('active');

		}
		$('.packing_rating').val(packing_rating);
	}
	if (typeof service_rating  !== "undefined"){
		$('.stars_4').find('.saveRating').removeClass('active');
		for(var i = 0; i<service_rating; i++){

			$('.stars_4').find('.saveRating:eq("'+i+'")').addClass('active');

		}
		$('.service_rating').val(service_rating);
	}
	if (typeof shipment_rating  !== "undefined"){
		$('.stars_5').find('.saveRating').removeClass('active');
		for(var i = 0; i<shipment_rating; i++){

			$('.stars_5').find('.saveRating:eq("'+i+'")').addClass('active');

		}
		$('.shipment_rating').val(shipment_rating);
	}
	if (typeof satisfaction_rating  !== "undefined"){
		$('.stars_6').find('.saveRating').removeClass('active');
		for(var i = 0; i<satisfaction_rating; i++){

			$('.stars_6').find('.saveRating:eq("'+i+'")').addClass('active');

		}
		$('.satisfaction_rating').val(satisfaction_rating);
	}



});

// save rating feedback to database

$('.saveStarRating').submit(function(event){

		event.preventDefault();

		if(ratingValidation()) {
			$form = $(this);
			$('.loader').show();

			$.ajax({
				url:  $form.attr('action'),
				type: $form.attr('method'),
				data: $form.serialize(),
				dataType: 'json',
				success: function (data) {
					$("#returnOrder").modal('hide');
					$('.loader').hide();

					if(data.success){
						$('.successMsgBody').html(data.message);
						$('#sussesMSG').modal('show');
						$('.saveStarRating .btn-black').prop('disabled', true);

					}else{
						$('.errorMsgBody').html(data.message);
						$('#errorMSG').modal('show');
					}
				}
			});
		}else{
			if(lang == "eng"){
			    var msg = "Please select the star rating and give the feedback!";
            }else{
                var msg = "يرجى تحديد تصنيف النجوم وإدخال رأيك!";
            }
			$('.errorMsgBody').html(msg);
			$('#errorMSG').modal('show');

		}

});

	// this code is need to improve but for that time it is working
	$('.feedback').keyup(function(){
		$('.feedback').val($(this).val());
	});


	function ratingValidation(){
		var result = true;
		$('.commonRating').each(function(index, element) {

			if($(this).val() == ""){

				result = false;
			}
        });

		return result;

	}

    // maintenance mode
	function maintenanceMode() {

	    $(".maintenancePopup").modal('show');

    }


    function changeCurrency(currency_id) {

    $('.loader').show();

    $.ajax({
        url:  base_url+'ajax/getCurrency',
        type: 'POST',
        data: {'currency_id':currency_id},
        dataType: 'json',
        success: function (data) {
            $('.loader').hide();
            if(data.success){

                window.location.reload();
            }

        }
    });

}


function rateProduct(rating, product_id)
{
    var person = {
        rating: rating,
        product_id: product_id
    }
    $(".loader").show();
    $.ajax({
        url: base_url+"ajax/productRating",
        type: 'post',
        dataType: 'json',
        success: function (data) {
            $('.successMsgBody').html(data.message);
            $('#sussesMSG').modal('show');
            $(".loader").hide();
        },
        data: person
    });
}


$(function() {
    $('.example-bootstrap').barrating({
        theme: 'fontawesome-stars'
    });
});
function switchLang(lang,url_str)
{var person={lang:lang,url_str:url_str};$.ajax({url:base_url+"langSwitch/index",type:'post',dataType:'json',data:person,success:function(data){window.location.href=base_url+data.url;}});}