$(document).ready(function() {
    "use strict";
    /*Menu Opener Function Start hmGallery-col-1*/
    $('.menu-opener').click(function(e) {
        e.preventDefault();
        $('body').toggleClass('menu-active');
        $('.mianNav').toggleClass('resp-menu');
    });
    $(function() {
        $('li').each(function() {
            var parent = $(this).parent().parent();
            if (parent.is('li')) {
                parent.addClass("has-dropdown");
            }
        });
    });
});
$(document).mouseup(function(e) {
    "use strict";
    var container = $(".header, .menu-opener, #nav");
    // if the target of the click isn't the container...
    if (!container.is(e.target) && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        $('body').removeClass('menu-active');
    }
});
$(window).scroll(function() {
    "use strict";
    //$('body').removeClass('menu-active');
});
$(document).ready(function () {
    "use strict";
	if ($(window).width() > 767 )  {
    var divs = $('.homepage .scrlFX');
    var dir = 'up'; // wheel scroll direction
    var div = 0; // current div
    $(document.body).on('DOMMouseScroll mousewheel', function (e) {
        if (e.originalEvent.detail > 0 || e.originalEvent.wheelDelta < 0) {
            dir = 'down';
        } else {
            dir = 'up';
        }
        // find currently visible div :
        div = -1;
        divs.each(function(i){
            if (div<0 && ($(this).offset().top >= $(window).scrollTop())) {
                div = i;
            }
        });
        if (dir === 'up' && div > 0) {
            div--;
        }
        if (dir === 'down' && div < divs.length) {
            div++;
        }
        //console.log(div, dir, divs.length);
        $('html,body').stop().animate({
            scrollTop: divs.eq(div).offset().top
        }, 200);
        return false;
    });
    $(window).resize(function () {
        $('html,body').scrollTop(divs.eq(div).offset().top);
   	});
}
});


/*equal height script start*/
equalheight = function(container) {
	var currentTallest = 0,
		currentRowStart = 0,
		rowDivs = new Array(),
		$el,
		topPosition = 0;
	$(container).each(function() {
		$el = $(this);
		$($el).height('auto')
		topPostion = $el.position().top;
		if (currentRowStart != topPostion) {
			for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
				rowDivs[currentDiv].height(currentTallest);
			}
			rowDivs.length = 0; // empty the array
			currentRowStart = topPostion;
			currentTallest = $el.height();
			rowDivs.push($el);
		} else {
			rowDivs.push($el);
			currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
		}
		for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
			rowDivs[currentDiv].height(currentTallest);
		}
	});
}
$(window).load(function() {
	equalheight('.client-logos ul li, .main .content-wrap .awards-list li, .ptro-box, .career-col, .footer .footer-top .footer-col');
});
$(window).resize(function() {
	equalheight('.client-logos ul li, .main .content-wrap .awards-list li, .ptro-box, .career-col, .footer .footer-top .footer-col');
});
/*equal height script end*/
$(document).ready(function () {
	var productsSecHeight = $('.pro-left-col').height() + 222;
    var offersimgHeight = $('.offers-img').height();
    var projectLeftColHeight = $('.pro-left-col').height() + 145;
	var proDtlLeftHeight = $('.pro-dtl-left').height() + 145;
    var projectSecTotalHeight = offersimgHeight + projectLeftColHeight;
    //console.log(projectSecTotalHeight);
	$('.pro-right-col').css('min-height',productsSecHeight);
    $('.content-wrap.offers-sec .pro-right-col').css('min-height',projectSecTotalHeight);
	if ($(window).width() < 1025 )  {
		$('.product-detail-sec .pro-right-col').css('min-height',proDtlLeftHeight);
	}
	if ($(window).width() < 993 )  {
		$('.pro-right-col').css('min-height',productsSecHeight + 190);
	}

    /**/
    var kscRCHeight= $('.right-content').height();
    $('.chairman-thumbnail, .ceo-thumbnail').css('min-height',kscRCHeight);

	/*$('.products-wrap .pro-dtl .pro-atw a').each(function(){
		$(this).click(function(){
			$(this).parent().parent().parent().addClass('active');
		});
	});*/
	
	$("").mouseover(function(){
		$(this).addClass( "", 1000 );
	});
});
$(function(){
	$('form.material').materialForm(); // Apply material
	$('form').validate({ 
		errorPlacement: function(error, element) {}
	}); // Apply validator with no error messages but classes only
});

function ChangeText(oFileInput, sTargetID) {
    document.getElementById(sTargetID).value = oFileInput.value;
}

$('.raq-lightbox .pf-row .pf-col-right input[type=submit]').submit(function(e) {
    e.preventDefault();
    // Coding
    $('#Modal_5').modal('toggle'); //or  $('#IDModal').modal('hide');
    return false;
});

$(function() {
	$('.product-box').hover(function(){
        $(this).toggleClass('product-box-hover',1000);
    }, function(){
        $(this).toggleClass('product-box-hover',1000);
    });
});
