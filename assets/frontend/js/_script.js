
//      Equal Height JS Function
equalheight = function(container){
    var currentTallest = 0,
        currentRowStart = 0,
        rowDivs = new Array(),
        $el,
        topPosition = 0;
    $(container).each(function() {

        $el = $(this);
        $($el).height('auto')
        topPostion = $el.position().top;

        if (currentRowStart != topPostion) {
            for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
            rowDivs.length = 0; // empty the array
            currentRowStart = topPostion;
            currentTallest = $el.height();
            rowDivs.push($el);
        } else {
            rowDivs.push($el);
            currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
        }
        for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
            rowDivs[currentDiv].height(currentTallest);
        }
    });
}
$(window).load(function() {
    equalheight('.offersSec .offProList ul li .whiteBox, .productsListing > ul > li .whiteBox, .addressBoxes .AddBox .whiteBox .sandGrayBox, .haveEqHeight');
});
$(window).resize(function(){
    equalheight('.offersSec .offProList ul li .whiteBox, .productsListing > ul > li .whiteBox, .addressBoxes .AddBox .whiteBox .sandGrayBox, .haveEqHeight');
});


//      TextShadow effect
$(".makeShadow").each(function () {
    var makeShadow = $(this).text();
    $(this).attr('data-text', makeShadow);
});


//      Top Search Bar Click function
$('header .leftSec .leftMenu ul li.topSearch a').click(function (e) {
   $(this).next('.showSearch').addClass('active');
});
$(document).mouseup(function (e)
{
    var container = $("header .leftSec .leftMenu ul li.topSearch .showSearch.active");
    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        $('header .leftSec .leftMenu ul li.topSearch .showSearch').removeClass('active');
    }
});/*=======  END  ======*/
//		Top Search Bar Click functio END


// Select Language js
$("header .rightSec .rightTop select.changLang").change(function (event) {
    var URresult = $(this).val();

    window.location.href = '?lang='+URresult;

    // alert("You have Selected  :: "+$(this).val());
});


$(document).ready(function(){
    
    //      HOME PAGE VIDEO FUNCTION
    /* Get iframe src attribute value i.e. YouTube video url
     and store it in a variable */
    var url = $("#hVideoIframe").attr('src');
    /* Assign empty url value to the iframe src attribute when
     modal hide, which stop the video playing */
    $("#homeVideo").on('hide.bs.modal', function(){
        $("#hVideoIframe").attr('src', '');
    });
    /* Assign the initially stored url back to the iframe src
     attribute when modal is displayed again */
    $("#homeVideo").on('show.bs.modal', function(){
        $("#hVideoIframe").attr('src', url);
    });


    // Product Detail page view more and less function
    $('.divViewMore').click(function (e) {
        $(this).slideUp(1000);
        $('.prodMoreDtl .pMD_inner').slideDown(1000);
    });
    $('.prodMoreDtl .pMD_inner .divViewLess a').click(function (e) {
        $('.divViewMore').slideDown(1000);
        $('.prodMoreDtl .pMD_inner').slideUp(1000);
    });


    $('.fancybox').fancybox();
    $('.fancybox').fancybox({
        openEffect  : 'none',
        closeEffect : 'none',

        prevEffect : 'none',
        nextEffect : 'none',

        closeBtn  : false,

        helpers : {
            title : {
                type : 'inside'
            },
            buttons	: {}
        },

        afterLoad : function() {
            this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
        }
    });
    
});

$('.imgBox').imgZoom({
   /* boxWidth: 680,
    boxHeight: 600,*/
    origin: 'data-origin'
});

//      FAQ's answer Question function
$('.qusBox').click(function (e) {
    if( $(this).next('.ansBox').hasClass('open')) {
    } else {
        $('.faqs_Sec .qusAnsBox .ansBox').fadeOut();
        $('.faqs_Sec .qusAnsBox .ansBox').removeClass('open');
        $(this).next('.ansBox').addClass('open');
        $(this).next('.ansBox').fadeIn();
    }
});
$('.faqs_Sec .qusAnsBox .ansBox .faqLess').click(function (e) {
   $(this).parent().fadeOut();
    $(this).parent().removeClass('open');
});


// Upload file
$('.fileUploader .showFileType').click(function(e) {
    $(this).siblings('input.attachCV[type=file]').click();
    return false;
});
$('input.attachCV[type=file]').change(function(evt) {
    var valueIs = $(this).val();
    $(this).siblings('.fileUploader .showFileName').val(valueIs)
});	/*=======		END		======*/

//      Latest News Read More function
$('.newsBox .textBox .readMore').click(function (e) {
    if($(this).parent('.textBox').hasClass('open')) {
        $(this).parent('.textBox').removeClass('open');
    } else {
        $(this).parent('.textBox').addClass('open');
    }

    /*if( $(this).next('.ansBox').hasClass('open')) {
    } else {
        $('.faqs_Sec .qusAnsBox .ansBox').fadeOut();
        $('.faqs_Sec .qusAnsBox .ansBox').removeClass('open');
        $(this).next('.ansBox').addClass('open');
        $(this).next('.ansBox').fadeIn();
    }*/
});
$('.faqs_Sec .qusAnsBox .ansBox .faqLess').click(function (e) {
    $(this).parent().fadeOut();
    $(this).parent().removeClass('open');
});