if (lang == 'eng') {
    var required_msg = "Please select this field.";
	var required_cv = "You Cannot Process without uploading your CV";
	var required_file = "You Cannot Process without uploading File";
	var required_cpatcha = "You Cannot Process without using Captcha";
	var required_location = "You Cannot Process without selecting location on Map";
	var loading_text = "Loading...";
	var select_type = "Select Type *";
	var select_space = "Select Space *";
	var select_property = "Select Your Property *";
	var valid_file_msg = "Sorry! Selected file is invalid, allowed extensions are: ";
} else {
    var required_msg = "الرجاء تعبئة الخانة";
	var required_cv = "لا يمكنك الاستمرار دون ادخال سيرتك الذاتية";
	var required_file = "لا يمكنك الاستمرار دون ادخال تحميل الملف";
	var required_cpatcha = "لا يمكنك الاستمرار دون ادخال كلمة التحقق";
	var required_location = "لا يمكنك الاستمرار دون تحديد الموقع على الخريطة";
	var loading_text = "تحميل ...";
	var select_type = "حدد النوع *";
	var select_space = "حدد المساحة *";
	var select_property = "حدد العقار *";
	var valid_file_msg = "عفوا! الملف الذي اخترته غير صالح, الأمدادات المسموح بها هي: ";
}
$(document).ready(function(){
    $('#loadMoreNews').click(function(e) {
        e.preventDefault();
        $('#topNews').hide();
        $('#loadMore').hide();
        $('#allNews').show();
        $('#showLess').show();
    });
    $('#loadLessNews').click(function(e) {
        e.preventDefault();
        $('#topNews').show();
        $('#loadMore').show();
        $('#allNews').hide();
        $('#showLess').hide();
    });
    $('.video').on('hidden.bs.modal', function () {
        $('.vidFrame iframe').attr("src", $(".vidFrame iframe").attr("src"));
    });
    $(".cap_load")
        .mouseout(function() {
            $(".cap_load").removeClass("loading");
        })
        .mouseover(function() {
            $(".cap_load").addClass("loading");
        });
      var x_timer;
            $("#cv_email_id_bk").keyup(function (e){
                clearTimeout(x_timer);
                var user_email = $(this).val();
                x_timer = setTimeout(function(){
                    check_email_ajax(user_email);
                }, 1000);
            });
            function check_email_ajax(useremail){
                $("#email_exist").html('<img src="'+url+'assets/images/ajax-loader.gif">');
                $.post(url+'ajax/check_valid_email', {'email':useremail}, function(data) {
                    $("#email_exist").html(data);
                    var htmlString = $("#email_exist").html();
                    var firstimg = $(htmlString).attr('src');
                    var check = firstimg.split('images/');
                    if(check[1] == 'not-available.png'){
                        $("#submit_id").attr('disabled', 'disabled');
                    }else{
                        $("#submit_id").removeAttr('disabled');
                    }
                });
            }
		$("#store_number").keydown(function(event) {
        // Allow only backspace and delete
        if ( event.keyCode == 46 || event.keyCode == 8 ) {
            // let it happen, don't do anything
            if($.trim($(this).val()).length==0)
            {
                if(event.keyCode==48){
                event.preventDefault(); 
                }
            }
        }
        else {
            // Ensure that it is a number and stop the keypress
            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 106)) {
                event.preventDefault(); 
            }   
            else{
              if($.trim($(this).val()) =='')
            {
                if(event.keyCode == 48){
                event.preventDefault(); 
                }
            }
            }
        }
    });
});
$(document).on('submit','.searchForm',function(e){
    $('form input').attr("data-original-title", "");
    if (lang == 'eng') {
        var required_msg = "This field cannot be left blank";
    } else {
        var required_msg = "هذا الحقل لا يمكن أن يترك فارغا";
    }
    var retVal = true;
    $form = $(this);
    var form_id = $form.attr('id');
    if (form_id == 'searchForm')
    {
        var title = $('#searchMain').val();
        if (removeSpaces(title) == '') {
            $('#searchMain').attr("data-original-title", required_msg);
            $('#searchMain').tooltip('show');
            $('#searchMain').focus();
            retVal = false;
        }else {
            retVal = true;
        }
        return retVal;
    }else if (form_id == 'searchForm_detail')
    {
        var title = $('#title_id').val();
        if (removeSpaces(title) == '') {
            $('#title_id').attr("data-original-title", required_msg);
            $('#title_id').tooltip('show');
            $('#title_id').focus();
            retVal = false;
        }else {
            retVal = true;
        }
        return retVal;
    }
});
$(document).on('submit','#newsletterfrm',function(e){
    if(validation_newsletter())
    {
        e.preventDefault();
		$('.loader').show();
        var form = $(this);
        var url = lang_base_url+'Ajax/subscribeNewsLetter';
        $.post(url, form.serialize(), function (result) {
            $('.success-msg-newsletter').html(result.message);
			$('.loader').hide();
            $('.success-msg-newsletter').show();
            $('.success-msg-newsletter').delay(3000).fadeOut('slow');
            document.forms['newsletterfrm'].reset();
        }, "json");
    }
});


$(".voucher_code").submit(function () {
	
	var voucher_code = $("#voucher_code").val();
	
	if (lang == 'eng') {
        var required_msg = "Please fill out this field.";
    } else {
        var required_msg = "الرجاء تعبئة الخانة";
    }
	
	$("#cart_total").val($('#total_prod_price_sum_val').val());

	if (!removeSpaces(voucher_code)) {
        $('#voucher_code').attr("data-original-title", required_msg);
        $('#voucher_code').tooltip('show');
        $('#voucher_code').focus();
		return false;
    }
	
	$(".loader").show();
	
	$.ajax({
		type: $(this).attr('method'),
		url: $(this).attr('action'),
		dataType: "json",
		data: $(this).serialize(),
		success: function (response) {
			
			$(".loader").hide();
			if(response.success == 1)
			{
				$("#total_prod_price_sum").html(numeral(parseFloat(response.new_total)).format('0,0')+'.00');

				$("#old_price_total").html(numeral(parseFloat(response.old)).format('0,0')+'.00');
				$("#discount_price_total").html(numeral(parseFloat(response.discount)).format('0,0')+'.00');

				$("#total_prod_price_sum_val").val(parseFloat(response.new_total));
				$(".discountPriceShow").show();
				$(".couponAppliedBtn").attr('disabled',true);
                equalheight('.offersSec .offProList ul li .whiteBox, .productsListing > ul > li .whiteBox, .addressBoxes .AddBox .whiteBox, .haveEqHeight');
				$('.alert-success').html(response.message);
				$('#sussesMSG').modal('show');
                setTimeout(function () {
                    location.reload();
                }, 1000);
			}else
			{
				$('.alert-danger').html(response.message);
				$('#errorMSG').modal('show');
				return false;
			}
		}
	});

});
function validation_newsletter() {
    //e.preventDefault();
    $('form input').attr("data-original-title", "");
    var email = $('#newsltr_email_id').val();
    if (lang == 'eng') {
        var email_msg = "Please enter valid email address";
        var required_msg = "This field cannot be left blank";
    } else {
        var email_msg = "يرجى إدخال عنوان بريد إلكتروني صالح";
        var required_msg = "هذا الحقل لا يمكن أن يترك فارغا";
    }
    if(removeSpaces(email) != '' && !validateEmail(email)) {
        $('#newsltr_email_id').attr("data-original-title", email_msg);
        $('#newsltr_email_id').tooltip('show');
        $('#newsltr_email_id').focus();
    }else if (!removeSpaces(email)) {
        $('#newsltr_email_id').attr("data-original-title", required_msg);
        $('#newsltr_email_id').tooltip('show');
        $('#newsltr_email_id').focus();
    }else {
        return true;
    }
}
$(document).on('submit','#contactUs_id',function(e){
    if(validation())
    {
        e.preventDefault();
        $("#submit_id").attr('disabled', 'disabled');
        $('.loader').show();
        $form = $(this);
        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: $form.serialize(),
            dataType: "json",
            cache: false,
            success: function (result) {
				 $('.loader').hide();
                if (result.reset) {
                    $form[0].reset();
                }
				if(result.success){
					$('.successMsgBody').html(result.message);
					$("#sussesMSG").modal('show');
					$("#submit_id").removeAttr('disabled');
					document.forms['contactForm'].reset();
                    $('#mobile_id').val('');
				}else{
					$('.errorMsgBody').html(result.message);
					$("#errorMSG").modal('show');
				}
            }
        });
    }
});

function validation() {
    //e.preventDefault();
    $('form input').attr("data-original-title", "");
    var name = $('#name_id').val();
    var email = $('#email_id').val();
    var mobile = $('#mobile_id').val();
    var country = $('#country').val();
	var city = $('#city').val();
    var message = $('#message_id').val();
    if (lang == 'eng') {
        var required_msg = "Please fill out this field.";
        var email_msg = "Please enter valid email address";
        var mobile_msg = "Please enter 12 Digit valid mobile number without plus sign";
    } else {
        var required_msg = "الرجاء تعبئة الخانة";
        var email_msg = "يرجى إدخال عنوان بريد إلكتروني صالح";
        var mobile_msg = "يرجى إدخال رقم الجوال المكون من 12 رقم دون علامة زائدة ";
    }
    if(removeSpaces(email) != '' && !validateEmail(email)) {
        $('#email_id').attr("data-original-title", email_msg);
        $('#email_id').tooltip('show');
        $('#email_id').focus();
    }else if(removeSpaces(mobile) != '' && !isMobileValid(mobile)) {
        $('#mobile_id').attr("data-original-title", mobile_msg);
        $('#mobile_id').tooltip('show');
        $('#mobile_id').focus();
    }else if (!removeSpaces(name)) {
        $('#name_id').attr("data-original-title", required_msg);
        $('#name_id').tooltip('show');
        $('#name_id').focus();
    }else if(removeSpaces(country) == null){
        $('#country').attr("data-original-title", required_msg);
        $('#country').tooltip('show');
        $('#country').focus();
    }else if(removeSpaces(city) == null){
        $('#city').attr("data-original-title", required_msg);
        $('#city').tooltip('show');
        $('#city').focus();
    }
	else if (!removeSpaces(email)) {
        $('#email_id').attr("data-original-title", required_msg);
        $('#email_id').tooltip('show');
        $('#email_id').focus();
    }else if(!removeSpaces(mobile)){
        $('#mobile_id').attr("data-original-title", required_msg);
        $('#mobile_id').tooltip('show');
        $('#mobile_id').focus();
    }else if (!removeSpaces(message)) {
        $('#message_id').attr("data-original-title", required_msg);
        $('#message_id').tooltip('show');
        $('#message_id').focus();
    }else {
        return true;
    }
}
$(document).on('submit','#payment_form',function(e){
    if(validation_payment_confirm())
    {
        e.preventDefault();
        $("#submit_id").attr('disabled', 'disabled');
        $('.loader').show();
        $form = $(this);
        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: new FormData(this),
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {
				$('.loader').hide();
                if (result.reset) {
                    $form[0].reset();
                }
                if(result.success) {
                    $(".successMsgBody").html(result.message);
                    $("#sussesMSG").modal('show');
                    $("#submit_id").removeAttr('disabled');
                    document.forms['payment_form'].reset();
                }else{
                    $(".errorMsgBody").html(result.message);
                    $("#errorMSG").modal('show');
                }
                $("#submit_id").removeAttr('disabled');
            }
        });
    }
});
function validation_payment_confirm() {
    $('form input').attr("data-original-title", "");
    var order_number = $('#order_number_id').val();
    var customer_name = $('#customer_name_id').val();
    var bank_name = $('#bank_name_id').val();
    var account_holder = $('#account_holder_id').val();
    var transfer_date = $('#transfer_date_id').val();
    var transfer_amount = $('#transfer_amount_id').val();
    var phone = $('#mobile_id').val();
    if (lang == 'eng') {
        var required_msg = "Please fill out this field.";
        var mobile_msg = "Please enter 12 Digit valid mobile number without plus sign";
    } else {
        var required_msg = "الرجاء تعبئة الخانة";
        var mobile_msg = "يرجى إدخال رقم الجوال المكون من 12 رقم دون علامة زائدة ";
    }
    if(removeSpaces(phone) != '' && !isMobileValid(phone)) {
        $('#mobile_id').attr("data-original-title", mobile_msg);
        $('#mobile_id').tooltip('show');
        $('#mobile_id').focus();
    }else if (!removeSpaces(order_number)){
        $('#order_number_id').attr("data-original-title", required_msg);
        $('#order_number_id').tooltip('show');
        $('#order_number_id').focus();
    }else if (!removeSpaces(customer_name)){
        $('#customer_name_id').attr("data-original-title", required_msg);
        $('#customer_name_id').tooltip('show');
        $('#customer_name_id').focus();
    }else if (!removeSpaces(bank_name)){
        $('#bank_name_id').attr("data-original-title", required_msg);
        $('#bank_name_id').tooltip('show');
        $('#bank_name_id').focus();
    }else if (!removeSpaces(account_holder)){
        $('#account_holder_id').attr("data-original-title", required_msg);
        $('#account_holder_id').tooltip('show');
        $('#account_holder_id').focus();
    }else if (!removeSpaces(transfer_date)){
        $('#transfer_date_id').attr("data-original-title", required_msg);
        $('#transfer_date_id').tooltip('show');
        $('#transfer_date_id').focus();
    }else if (!removeSpaces(transfer_amount)){
        $('#transfer_amount_id').attr("data-original-title", required_msg);
        $('#transfer_amount_id').tooltip('show');
        $('#transfer_amount_id').focus();
    }else if (!removeSpaces(phone)){
        $('#mobile_id').attr("data-original-title", required_msg);
        $('#mobile_id').tooltip('show');
        $('#mobile_id').focus();
    }else {
        return true;
    }
}
$(document).on('submit','#apply_job',function(e){
    if(validation_submitCV())
    {
        e.preventDefault();
        $("#submit_id").attr('disabled', 'disabled');
        $('.loader').show();
        $form = $(this);
        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: new FormData(this),
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {
                if (result.reset) {
                    $form[0].reset();
                }
                $("#applyNowPU").modal('show');
                $('.loader').hide();
                $("#submit_id").removeAttr('disabled');
                document.forms['apply_job'].reset();
            }
        });
    }
});
	// cv file validation only for doc and pdf files
	$('.fileTypeValidation').on( 'change', function() {
		var myfile= $( this ).val();
		var ext = myfile.split('.').pop();
		if(ext=="pdf" || ext=="docx" || ext=="doc" || ext == "xlsx" || ext == "xls" || ext == "ppt" || ext == "pptx" || ext == "txt"){
			//result = true;
		} else{
		    $('.errorMsgBody').html(' Please attach doc or pdf file');
            $('#errorMSG').modal('show');
		}
	});
function validation_submitCV() {
    $('form input').attr("data-original-title", "");
    var name = $('#name_id').val();
	
    var age = $('#age_id').val();
    var email = $('#email_id').val();
    var mobile = $('#mobile_id').val();
    var experience = $('#experience_id').val();
    var cv = $('#cv_id').val();
    var cv_name = $('.fileupload-preview').html();
    if (lang == 'eng') {
        var required_msg = "Please fill out this field.";
        var email_msg = "Please enter valid email address";
		var mobile_msg = "Please enter 12 Digit valid mobile number without plus sign";
    } else {
        var required_msg = "الرجاء تعبئة الخانة";
        var email_msg = "يرجى إدخال عنوان بريد إلكتروني صالح";
		var mobile_msg = "يرجى إدخال رقم الجوال المكون من 12 رقم دون علامة زائدة ";
    }
    if(removeSpaces(email) != '' && !validateEmail(email)) {
        $('#email_id').attr("data-original-title", email_msg);
        $('#email_id').tooltip('show');
        $('#email_id').focus();
    }else if(removeSpaces(mobile) != '' && !isMobileValid(mobile)) {
        $('#mobile_id').attr("data-original-title", mobile_msg);
        $('#mobile_id').tooltip('show');
        $('#mobile_id').focus();
    }else if (!removeSpaces(name)){
        $('#name_id').attr("data-original-title", required_msg);
        $('#name_id').tooltip('show');
        $('#name_id').focus();
    }else if (!removeSpaces(age)){
        $('#age_id').attr("data-original-title", required_msg);
        $('#age_id').tooltip('show');
        $('#age_id').focus();
    }else if (!removeSpaces(email)){
        $('#email_id').attr("data-original-title", required_msg);
        $('#email_id').tooltip('show');
        $('#email_id').focus();
    }else if (!removeSpaces(mobile)){
        $('#mobile_id').attr("data-original-title", required_msg);
        $('#mobile_id').tooltip('show');
        $('#mobile_id').focus();
    }else if (!removeSpaces(experience)){
        $('#experience_id').attr("data-original-title", required_msg);
        $('#experience_id').tooltip('show');
        $('#experience_id').focus();
    }else if (!removeSpaces(cv)) {
        $('#cv_id').attr("data-original-title", required_msg);
        $('#cv_id').tooltip('show');
        $('#cv_id').focus();
		/*alert(required_cv);*/
    }else {
        return true;
    }
}
$(document).on('submit','#resgisterForm_id',function(e){
    if(validation_registration())
    {
		
        e.preventDefault();
        $("#submit_id").attr('disabled', 'disabled');
        
		var email = $('#reg_email_id').val();
		var user_id = $('#user_id').val();
		$('.loader').show();
		if(check_userEmail_exist(email) == false && user_id == ''){
			$('.loader').hide();
			$("#modal-email-exist").modal('show');
			$("#submit_id").removeAttr('disabled');
		}else{
		$form = $(this);
        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: new FormData(this),
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
			async: false,
            success: function (result) {
				$('.loader').hide();
                if (result.reset) {
                    $form[0].reset();
                }
                if(!result.success){
                    $('.errorMsgBody').html(result.message);
                    $('#errorMSG').modal('show');
                }

				if(user_id == '' && result.success){
				
				$("#modal-submit").modal({
					backdrop: 'static',
					keyboard: false
				});
                
                $("#submit_id").removeAttr('disabled');
                document.forms['resgisterForm'].reset();
				//window.location.href = lang_base_url+'profile';
				
				}else{
				$("#modal-update").modal('show');
               
                $("#submit_id").removeAttr('disabled');
                window.location.href = lang_base_url+'profile';
				}
            }
        });
		}
       
    }
});
function validation_registration() {
    $('form input').attr("data-original-title", "");
    $('form select').attr("data-original-title", "");
	
    var first_name = $('#fname_id').val();
    var last_name = $('#lname_id').val();
    var email = $('#reg_email_id').val();
    var mobile = $('#mobile_id').val();
    var country_name = $('.reg_country').val();
    var city_name = $('.reg_city').val();
    var dob = $('#dob_id').val();
    var password = $('#password_id').val();
    var re_password = $('#re_password_id').val();
	
	var user_id = $('#user_id').val();
    if (lang == 'eng') {
        var required_msg = "Please fill out this field.";
        var email_msg = "Please enter valid email address";
        var mobile_msg = "Please enter 12 Digit valid mobile number without plus sign";
		var terms_msg = "This field cannot be left blank";
		var password_msg = "Password is not matching. Please enter correct password";
    } else {
        var required_msg = "الرجاء تعبئة الخانة";
        var email_msg = "يرجى إدخال عنوان بريد إلكتروني صالح";
        var mobile_msg = "يرجى إدخال رقم الجوال المكون من 12 رقم دون علامة زائدة ";
		var terms_msg = "هذا الحقل لا يمكن أن يترك فارغا";
		var password_msg = "كلمة المرور غير متطابقة. الرجاء إدخال كلمة المرور الصحيحة";
    }
    if(removeSpaces(email) != '' && !validateEmail(email) && user_id == '') {
        $('#reg_email_id').attr("data-original-title", email_msg);
        $('#reg_email_id').tooltip('show');
        $('#reg_email_id').focus();
    }else if(removeSpaces(mobile) != '' && !isMobileValid(mobile)) {
        $('#mobile_id').attr("data-original-title", mobile_msg);
        $('#mobile_id').tooltip('show');
        $('#mobile_id').focus();
    }else if(removeSpaces(re_password) != '' && !isPasswordMatch(password, re_password) && user_id == '') {
        $('#re_password_id').attr("data-original-title", password_msg);
        $('#re_password_id').tooltip('show');
        $('#re_password_id').focus();
    }else if (!removeSpaces(first_name)) {
        $('#fname_id').attr("data-original-title", required_msg);
        $('#fname_id').tooltip('show');
        $('#fname_id').focus();
    }else if (!removeSpaces(last_name)) {
        $('#lname_id').attr("data-original-title", required_msg);
        $('#lname_id').tooltip('show');
        $('#lname_id').focus();
    }else if(!removeSpaces(mobile)){
        $('#mobile_id').attr("data-original-title", required_msg);
        $('#mobile_id').tooltip('show');
        $('#mobile_id').focus();
    }else if (!removeSpaces(email) && user_id == '') {
        $('#reg_email_id').attr("data-original-title", required_msg);
        $('#reg_email_id').tooltip('show');
        $('#reg_email_id').focus();
    }else if (removeSpaces(country_name) == null) {
        $('.reg_country').attr("data-original-title", required_msg);
        $('.reg_country').tooltip('show');
        $('.reg_country').focus();
    }else if (removeSpaces(city_name) == null) {
        $('.reg_city').attr("data-original-title", required_msg);
        $('.reg_city').tooltip('show');
        $('.reg_city').focus();
    }else if (!removeSpaces(dob)) {
        $('#dob_id').attr("data-original-title", required_msg);
        $('#dob_id').tooltip('show');
        $('#dob_id').focus();
    }else if (!removeSpaces(password) && user_id == '') {
        $('#password_id').attr("data-original-title", required_msg);
        $('#password_id').tooltip('show');
        $('#password_id').focus();
    }else if (!removeSpaces(re_password) && user_id == '') {
        $('#re_password_id').attr("data-original-title", required_msg);
        $('#re_password_id').tooltip('show');
        $('#re_password_id').focus();
    }else if(!($('#checkbox1').is(':checked')) && user_id == ''){
		/*$('#checkbox1').attr("data-original-title", terms_msg);
        $('#checkbox1').tooltip('show');
        $('#checkbox1').focus();*/
		$('#terms_n_condition').modal('show');
	}else {
        return true;
    }
}
// for forgot password 
$(".forgot_password_request").submit(function(event){
	event.preventDefault();
	if(forgotValidation()) {
        $('.loader').show();
        $form = $(this);
        $.ajax({
            url: $form.attr('action'),
            type: $form.attr('method'),
            dataType: 'json',
            data: $form.serialize(),
            success: function (data) {
                $('.loader').hide();
                $('#forGotPass').modal('hide');
                if (data.exist == true) {
                    $('.successMsgBody').html(data.message);
                    $('#sussesMSG').modal('show');
                    $form[0].reset();
                } else {
                    $('.errorMsgBody').html(data.message);
                    $('#errorMSG').modal('show');
                }
            }
        });
    }
});
   function forgotValidation(){
        var success = true;
    $('.forgot_password').removeClass('has-error');
        $('form input').attr("data-original-title", "");
        var forgot_password = $('.forgot_password').val();
        if (lang == 'eng') {
            var required_msg = "Please fill out this field.";
        } else {
            var required_msg = "الرجاء تعبئة الخانة";
        }
        if (!removeSpaces(forgot_password)) {
            $('.forgot_password').attr("data-original-title", required_msg);
            $('.forgot_password').tooltip('show');
            $('.forgot_password').addClass('has-error');
            $('.forgot_password').focus();
            var success = false;
        }
        return success;
    }
$(".reset_password").submit(function(event){
	
	event.preventDefault();
	
	if(password_validation()) {
	
		$('.loader').show();
	
		$form = $(this);
		
		 $.ajax({
			url: $form.attr('action'),
			type: $form.attr('method'),
			dataType: 'json',
			data: $form.serialize(),
			success: function (data) {
				$('.loader').hide();
				
				if(data.error == false) {
				
					$('.successMsgBody').html(data.message);
					$('#sussesMSG').modal('show');
					$form[0].reset();
				}else{
					
					$('.errorMsgBody').html(data.message);
					$('#errorMSG').modal('show');
				}
			}
	
		});
	}
});
function password_validation(){
    $('form input').attr("data-original-title", "");
    $('form select').attr("data-original-title", "");
	
    var password = $('#password_id').val();
    var re_password = $('#re_password_id').val();
    if (lang == 'eng') {
        var required_msg = "Please fill out this field.";
		var password_msg = "Password is not matching. Please enter correct password";
    } else {
        var required_msg = "الرجاء تعبئة الخانة";
		var password_msg = "كلمة المرور غير متطابقة. الرجاء إدخال كلمة المرور الصحيحة";
    }
	
	if (!removeSpaces(password)) {
        $('#password_id').attr("data-original-title", required_msg);
        $('#password_id').tooltip('show');
        $('#password_id').focus();
    }else if (!removeSpaces(re_password)) {
        $('#re_password_id').attr("data-original-title", required_msg);
        $('#re_password_id').tooltip('show');
        $('#re_password_id').focus();
    }else if(removeSpaces(re_password) != '' && !isPasswordMatch(password, re_password)) {
        $('#re_password_id').attr("data-original-title", password_msg);
        $('#re_password_id').tooltip('show');
        $('#re_password_id').focus();
    }else {
        return true;
    }
}
$(document).on('submit','.shippingAddForm',function(e){
    if(validation_addAddress())
    {
        e.preventDefault();
        $("#submit_id").attr('disabled', 'disabled');
        $('.loader').show();
        $form = $(this);
        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: new FormData(this),
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
			success: function(result){
                    $(".loader").hide();
                    if(result.success == '1'){
                        location.reload();
                    }else{
                    }
			}
        });
    }
});
function validation_addAddress() {
    $('form input').attr("data-original-title", "");
    var title = $('#address_title').val();
    var address_1 = $('#address_1').val();
    var address_2 = $('#address_2').val();
    var country = $('#country').val();
    var city = $('#city').val();
    var mobile = $('#phone_no').val();
	var user_id = $('#user_id').val();


	if($('input[name=region]').length >0 && $('input[name=zip_code]').length > 0){
        var region = $('input[name=region]').val();
        var zip_code = $('input[name=zip_code]').val();
    }else{
        var region = "validate";
        var zip_code = "validate";
    }

    if (lang == 'eng') {
        var required_msg = "Please fill out this field.";
        var email_msg = "Please enter valid email address";
		var mobile_msg = "Please enter 12 Digit valid mobile number without plus sign";
		var confirm_password_msg = "Password and confirm password does not match";

		var region_msg = "The State/Province field is required";
		var zip_code_msg = "The zip code filed is required";
    } else {
        var required_msg = "الرجاء تعبئة الخانة";
        var email_msg = "يرجى إدخال عنوان بريد إلكتروني صالح";
		var mobile_msg = "يرجى إدخال رقم الجوال المكون من 12 رقم دون علامة زائدة ";
		var confirm_password_msg = "الرقم السري و تأكيد الرقم السري غير مطابقين";
        var region_msg = "The State/Province field is required";
        var zip_code_msg = "The zip code filed is required";
    }
	
	if(user_id == 0)
	{
		var full_name = $('#full_name').val();
		var email = $('#email').val();
		var mobile_no = $('#mobile_no').val();
		if(email != '' && !validateEmail(email)) {
			$('#email').attr("data-original-title", email_msg);
			$('#email').tooltip('show');
			$('#email').focus();
			return false;
		}else if (!removeSpaces(full_name)){
	
			$('#full_name').attr("data-original-title", required_msg);
			$('#full_name').tooltip('show');
			$('#full_name').focus();
			return false;
	
		}else if (!removeSpaces(email)){
	
			$('#email').attr("data-original-title", email_msg);
			$('#email').tooltip('show');
			$('#email').focus();
			return false;
	
		}else if (!removeSpaces(mobile_no)){
	
			$('#mobile_no').attr("data-original-title", required_msg);
			$('#mobile_no').tooltip('show');
			$('#mobile_no').focus();
			return false;
	
		}else if (removeSpaces(mobile_no) != '' && !isMobileValid(mobile_no)){
	
			$('#mobile_no').attr("data-original-title", mobile_msg);
			$('#mobile_no').tooltip('show');
			$('#mobile_no').focus();
			return false;
	
		}
		if($("#regAddress").is(':checked'))
		{
			var password = $('.addressPass').val();
			var confirm_password = $('.addressConfirmPass').val();
			
			if(!removeSpaces(password)) {
				$('#password').attr("data-original-title", required_msg);
				$('#password').tooltip('show');
				$('#password').focus();
				return false;
	
			}else if (!removeSpaces(confirm_password)){
		
				$('#confirm_password').attr("data-original-title", required_msg);
				$('#confirm_password').tooltip('show');
				$('#confirm_password').focus();
				return false;
		
			}else if (confirm_password !== password){
				$('#confirm_password').attr("data-original-title", confirm_password_msg);
				$('#confirm_password').tooltip('show');
				$('#confirm_password').focus();
				return false;
		
			}
		}
		
	}
	else
	{
		 if(removeSpaces(mobile) != '' && !isMobileValid(mobile)) {
			$('#phone_no').attr("data-original-title", mobile_msg);
			$('#phone_no').tooltip('show');
			$('#phone_no').focus();
			return false;
	
		}else if (!removeSpaces(mobile)){
			
			$('#phone_no').attr("data-original-title", required_msg);
			$('#phone_no').tooltip('show');
			$('#phone_no').focus();
			return false;
	
		}
	}
	
   if (!removeSpaces(title)){
        $('#address_title').attr("data-original-title", required_msg);
        $('#address_title').tooltip('show');
        $('#address_title').focus();
    }else if (!removeSpaces(address_1)){
        $('#address_1').attr("data-original-title", required_msg);
        $('#address_1').tooltip('show');
        $('#address_1').focus();
    }
    else if (!removeSpaces(address_2)){
        $('#address_2').attr("data-original-title", required_msg);
        $('#address_2').tooltip('show');
        $('#address_2').focus();
    }
    else if (country == null || !country){
        $('#country').attr("data-original-title", required_msg);
        $('#country').tooltip('show');
        $('#country').focus();
    }else if (city == null || !city){
        $('#city').attr("data-original-title", required_msg);
        $('#city').tooltip('show');
        $('#city').focus();
    }else if (region == null || !region){

       $('#region').attr("data-original-title", region_msg);
       $('#region').tooltip('show');
       $('#region').focus();
       return false;

   }else if (zip_code == null || !zip_code){

       $('#zip_code').attr("data-original-title", zip_code_msg);
       $('#zip_code').tooltip('show');
       $('#zip_code').focus();
       return false;

   }
    else {
        return true;
    }
}

$(document).on('submit','.sendOrderEmail',function(e){
	
	var email = $("#email_order").val();
	
	if (lang == 'eng') {
        var required_msg = "Please fill out this field.";
        var email_msg = "Please enter valid email address";
    } else {
        var required_msg = "الرجاء تعبئة الخانة";
        var email_msg = "يرجى إدخال عنوان بريد إلكتروني صالح";
    }
	
	if(removeSpaces(email) != '' && !validateEmail(email)) {
        $('#email_order').attr("data-original-title", email_msg);
        $('#email_order').tooltip('show');
        $('#email_order').focus();
		return false;
    }else if (!removeSpaces(email)){
        $('#email_order').attr("data-original-title", required_msg);
        $('#email_order').tooltip('show');
        $('#email_order').focus();
		return false;
    }
	
	e.preventDefault();
	$(".loader").show();
	$form = $(this);
		$.ajax({
		type: "POST",
		url: $form.attr('action'),
		data: $form.serialize(),
		dataType: "json",
		cache: false,
		success: function (result) {
			$(".loader").hide();
			$("#email_msg").show();
			setTimeout( function(){ 
				$("#email_msg").hide();
			  }  , 5000 );
		}
	});
});

$(document).on('submit','.changePasswordForm',function(e){
    if(validation_changePassword())
    {
        e.preventDefault();
        $(".loader").show();
        $("#submit_id").attr('disabled', 'disabled');
        $form = $(this);
			$.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: $form.serialize(),
            dataType: "json",
            cache: false,
            success: function (result) {
                if(result.change == true){
                    $(".loader").hide();
					$("#password_error").hide();
					$("#password_msg").show();
                    location.reload();
                }else{
					$(".loader").hide();
					$("#password_msg").hide();
					$("#password_error").show();
                    return false;
                }
            }
        });
		
        
    }
});
function validation_changePassword() {
    var result = true;
    $('form input').attr("data-original-title", "");
    var old_password = $('#pre_password').val();
    var password = $('#new_password').val();
    var re_password = $('#confirm_password').val();
    if (lang == 'eng') {
        var differ_password_msg = "The old and new passwords should be different";
        var required_msg = "Please fill out this field.";
        var old_password_msg = "Please enter old password.";
        var new_password_msg = "Please enter new password.";
        var password_invalid_msg = "New password and confirm password are not matching.";
    } else {
        var differ_password_msg = "يجب أن تكون كلمة المرور القديمة و الجديدة مختلفة";
        var required_msg = "الرجاء تعبئة الخانة";
        var old_password_msg = "الرجاء إدخال كلمة المرور القديمة.";
        var new_password_msg = "يرجى إدخال الرمز هنا";
        var password_invalid_msg = "الرجاء إدخال النص الصحيح";
    }
    if(removeSpaces(re_password) != '' && !isPasswordMatch(password, re_password)){
        
        $('#confirm_password').attr("data-original-title", password_invalid_msg);
        $('#confirm_password').tooltip('show');
        $('#confirm_password').focus();
        result = false;
    }else if(removeSpaces(old_password) != '' && !isPassword_new(password, old_password)){
        
        $('#pre_password').attr("data-original-title", differ_password_msg);
        $('#pre_password').tooltip('show');
        $('#pre_password').focus();
        result = false;
    }else if(removeSpaces(old_password) == '') {
        $('#pre_password').attr("data-original-title", required_msg);
        $('#pre_password').tooltip('show');
        $('#pre_password').focus();
        result = false;
    }else if (removeSpaces(password) == '') {
        $('#new_password').attr("data-original-title", required_msg);
        $('#new_password').tooltip('show');
        $('#new_password').focus();
        result = false;
    }else if (removeSpaces(re_password) == '') {
        $('#confirm_password').attr("data-original-title", required_msg);
        $('#confirm_password').tooltip('show');
        $('#confirm_password').focus();
        result = false;
    }
    return result;
}
$(document).on('submit','#distributor_form',function(e){
    if(validation_distributor())
    {
        e.preventDefault();
        $("#submit_id").attr('disabled', 'disabled');
        $('.loader').show();
        $form = $(this);
        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: new FormData(this),
            dataType: "json",
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {
				$('.loader').hide();
                if (result.reset) {
                    $form[0].reset();
                }
				if(result.response == true){
					$('.successMsgBody').html(result.message);
					$("#sussesMSG").modal('show');
					
					$("#submit_id").removeAttr('disabled');
					document.forms['distributor_form'].reset();
				}else{
					$("#errorMSG").modal('show');
					$('.errorMsgBody').html(result.message);
					$("#submit_id").removeAttr('disabled');
				}
            }
        });
    }
});
function validation_distributor() {
    $('form input').attr("data-original-title", "");
    var full_name = $('#full_name').val();
    var mobile_no = $('#mobile_id').val();
    var store_name = $('#store_name').val();
    var email = $('#email_distributor').val();
    var country = $('#country').val();
    var city = $('#city').val();
    var street_address = $('#street_address').val();
    var district = $('#district').val();
    var store_number = $('#store_number').val();
    var location = $('#location').val();
    var images = $('#file').val();
    var cv_name = $('.fileupload-preview').html();
    if (lang == 'eng') {
        var required_msg = "Please fill out this field.";
        var email_msg = "Please enter valid email address";
		var mobile_msg = "Please enter 12 Digit valid mobile number without plus sign";
    } else {
        var required_msg = "الرجاء تعبئة الخانة";
        var email_msg = "يرجى إدخال عنوان بريد إلكتروني صالح";
		var mobile_msg = "يرجى إدخال رقم الجوال المكون من 12 رقم دون علامة زائدة ";
    }
    if(removeSpaces(email) != '' && !validateEmail(email)) {
        $('#email_distributor').attr("data-original-title", email_msg);
        $('#email_distributor').tooltip('show');
        $('#email_distributor').focus();
    }else if(removeSpaces(mobile_no) != '' && !isMobileValid(mobile_no)) {
        $('#mobile_id').attr("data-original-title", mobile_msg);
        $('#mobile_id').tooltip('show');
        $('#mobile_id').focus();
    }else if (!removeSpaces(full_name)){
        $('#full_name').attr("data-original-title", required_msg);
        $('#full_name').tooltip('show');
        $('#full_name').focus();
    }else if (!removeSpaces(mobile_no)){
        $('#mobile_id').attr("data-original-title", required_msg);
        $('#mobile_id').tooltip('show');
        $('#mobile_id').focus();
    }else if (!removeSpaces(store_name)){
        $('#store_name').attr("data-original-title", required_msg);
        $('#store_name').tooltip('show');
        $('#store_name').focus();
    }else if (!removeSpaces(email)){
        $('#email_distributor').attr("data-original-title", required_msg);
        $('#email_distributor').tooltip('show');
        $('#email_distributor').focus();
    }else if (!removeSpaces(country)){
        $('#country').attr("data-original-title", required_msg);
        $('#country').tooltip('show');
        $('#country').focus();
    }else if (!removeSpaces(city)){
        $('#city').attr("data-original-title", required_msg);
        $('#city').tooltip('show');
        $('#city').focus();
    }else if (!removeSpaces(street_address)) {
        $('#street_address').attr("data-original-title", required_msg);
        $('#street_address').tooltip('show');
        $('#street_address').focus();
    }else if (!removeSpaces(district)) {
        $('#district').attr("data-original-title", required_msg);
        $('#district').tooltip('show');
        $('#district').focus();
    }else if (!removeSpaces(store_number)) {
        $('#store_number').attr("data-original-title", required_msg);
        $('#store_number').tooltip('show');
        $('#store_number').focus();
    }else if (!removeSpaces(location)) {
        $('#location').attr("data-original-title", required_msg);
        $('#location').tooltip('show');
        $('#location').focus();
    }else if (!removeSpaces(images)) {
        /*$('#file').attr("data-original-title", required_msg);
        $('#file').tooltip('show');
        $('#file').focus();*/
		$('#file_upload_msg').modal('show');
    }else {
        return true;
    }
}
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
function validateCompanyEmail(email) {
    var re = /^([\w-.]+@(?!gmail\.com)(?!yahoo\.com)(?!hotmail\.com)([\w-]+.)+[\w-]{2,4})?$/;
    return re.test(email);
}
function isMobileValid(userInput) {
    var s = userInput;
    if(s.length > 6)
        return true;
    else
        return false;
}
function isPasswordMatch(password, re_password){
	if(password == re_password){
		return true;
	}else{
		return false;
	}
}
function isPassword_new(password, old_password){
	if(password == old_password){
		return false;
	}else{
		return true;
	}
}
function check_password_exist(old_password){
	var check;
    $.ajax({
        type: "POST",
        url: lang_base_url+"Register/check_password_exist",
        data: {'old_password': old_password},
        dataType: "json",
		cache: false,
		async: false,
        success: function(result){
            if(result.exist === true)
                check = true;
            else
                check = false;
        }
    });
    return check;
}
function check_userEmail_exist(email){
	var check;
    $.ajax({
        type: "POST",
        url: lang_base_url+"Register/check_userEmail_exist",
        data: {'email': email},
        dataType: "json",
		cache: false,
		async: false,
        success: function(result){
            if(result.exist === true)
                check = true;
            else
                check = false;
        }
    });
    return check;
}
function validateCaptchaCode(code) {
    var captcha_code = code;
    var check = true;
    $.ajax({
        type: "POST",
        url: url+"page/check_captcha",
        data: {'captcha_code': captcha_code},
        dataType: "json",
        cache: false,
        async: false,
        success: function(result){
            console.log(result);
            if(result.captcha === true)
                check = true;
            else
                check = false;
        }
    });
    return check;
}
function playVideo(video_id){
    $('#modal-video').modal({
        show: true
    });
    $('#vidIframe').html('');
    $('#vidIframe').html('<iframe src="https://www.youtube.com/embed/'+video_id+'" frameborder="0" allowfullscreen></iframe>');
}
var _validFile_Extensions = [".pdf", ".doc"];
        function ValidateSingleInput_file(oInput) {
            if (oInput.type == "file") {
                var sFileName = oInput.value;
                if (sFileName.length > 0) {
                    var blnValid = false;
                    for (var j = 0; j < _validFile_Extensions.length; j++) {
                        var sCurExtension = _validFile_Extensions[j];
                        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                            blnValid = true;
                            break;
                        }
                    }
                    if (!blnValid) {
                        alert(valid_file_msg + _validFile_Extensions.join(", "));
                        oInput.value = "";
                        return false;
                    }
                }
            }
            return true;
        }
		
		
	function removeSpaces(value){
		var result = $.trim(value);
		return result;
	}