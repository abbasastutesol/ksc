<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Projects extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
		
        $get_url_arr = explode('/', $_SERVER['REQUEST_URI']);
		if(!$this->session->userdata('site_lang'))
		{
			$this->session->set_userdata('site_lang','eng');
		}
		if(in_array('en', $get_url_arr))
		{
			$this->session->set_userdata('site_lang','eng');
		}
		else
		{
			$this->session->set_userdata('site_lang','arb');
		}
		$this->lang->load("message",$this->session->userdata('site_lang'));
	    $this->load->model('Model_product');
	    $this->load->model('Model_projects');
         $this->load->model('Model_projects_gallery');
			
		
    }
	
	public function data()
	{
		$data = array();
          
	     $data = $this->lang->line('all');
		$data['lang'] = $this->session->userdata('site_lang'); 
        $object = $this->Model_projects->allProjects();
		
		$json  = json_encode($object);
        $data['projects'] = json_decode($json, true);
        $data['page_id'] = $data['projects'][0]['page_id'];
		$data['content'] = 'projects/projects';
		$data['class'] = 'projects';
        
		$this->load->view('default',$data);	
		
	}
      
	  public function detail_project($id){
	    $record_num = end($this->uri->segment_array()); 
		$data = array();
        $data = $this->lang->line('all');
		
		$data['lang'] = $this->session->userdata('site_lang');    
		$data['project_detail'] = $this->Model_projects_gallery->get_Project_gallery($record_num);
		
		$data['content'] = 'projects/project_detail';
		$data['class'] = 'projects';
       
		$this->load->view('default',$data);	
		
	  }
    
}
