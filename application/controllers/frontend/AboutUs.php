<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AboutUs extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
		
		$this->load->helper('url');
		$get_url_arr = explode('/', $_SERVER['REQUEST_URI']);
		if(!$this->session->userdata('site_lang'))
		{
			$this->session->set_userdata('site_lang','arb');
		}
		if(in_array('en', $get_url_arr))
		{
			$this->session->set_userdata('site_lang','eng');
		}
		else
		{
			$this->session->set_userdata('site_lang','arb');
		}
        $this->lang->load("message",$this->session->userdata('site_lang'));
	    $this->load->model('Model_about_us');
	    $this->load->model('Model_mission_vision');
	    $this->load->model('Model_why_ioud');
	    $this->load->model('Model_Distinctiveness');
		//$res = checkLevels(2);
		//checkAuth($res);
    }
	
	public function index()
	{	
		$data = array();
		$data = $this->lang->line('all');
		$data['lang'] = $this->session->userdata('site_lang');
		$data['about_us'] = $this->Model_about_us->get('1', true);
		$data['content'] = 'aboutus/about_us';
		$data['class'] = 'about_us';
		$this->load->view('default',$data);	
		
	}

	public function mission_and_vision()
	{
		$data = array();
		$data = $this->lang->line('all');
		$data['lang'] = $this->session->userdata('site_lang');
		$data['mission_vision'] = $this->Model_mission_vision->get('1', true);
		$data['content'] = 'aboutus/mission_and_vision';
		$data['class'] = 'mission_and_vision';
		$this->load->view('default',$data);

	}

	public function why_ioud()
	{
		$data = array();
		$data = $this->lang->line('all');
		$data['lang'] = $this->session->userdata('site_lang');
		$data['why_ioud'] = $this->Model_why_ioud->get('1', true);
		$data['content'] = 'aboutus/why_ioud';
		$data['class'] = 'why_ioud';
		$this->load->view('default',$data);

	}


	public function distinctiveness()
	{
		$data = array();
		$data = $this->lang->line('all');
		$data['lang'] = $this->session->userdata('site_lang');
		$data['distinctiveness'] = $this->Model_Distinctiveness->get('1', true);
		$data['content'] = 'aboutus/distinctiveness';
		$data['class'] = 'distinctiveness';
		$this->load->view('default',$data);

	}
	
}