<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_service extends CI_Controller {


	public function __construct()
    {
        parent::__construct();
		
		$this->load->helper('url');
		$get_url_arr = explode('/', $_SERVER['REQUEST_URI']);
		if(!$this->session->userdata('site_lang'))
		{
			$this->session->set_userdata('site_lang','arb');
		}
		if(in_array('en', $get_url_arr))
		{
			$this->session->set_userdata('site_lang','eng');
		}
		else
		{
			$this->session->set_userdata('site_lang','arb');
		}
        $this->lang->load("message",$this->session->userdata('site_lang'));


	    $this->load->model('Model_customer_service');
        $this->load->model('Model_latest_news');
        $this->load->model('Model_return_policy');
        $this->load->model('Model_distributor');
        $this->load->model('Model_distributor_images');
        $this->load->model('Model_distributor_requests');
        $this->load->model('Model_privacy_policy');
        $this->load->model('Model_payment_confirm');
        $this->load->model('Model_orders');
        $this->load->model('Model_meduaf_program');
        $this->load->library('File_upload');


    }
	
	public function index()
	{
	    $data = array();
		$data = $this->lang->line('all');
        $data['lang'] = $this->session->userdata('site_lang');
	    $data['payment_delivery'] = $this->Model_customer_service->get('1',true);

		$data['content'] = 'customer_service/pay_and_delivery';
		$data['class'] = 'customer_service';

		$this->load->view('default',$data);	
		
	}

    // for latest news under customer service
    public function latest_news()
    {
        $data = array();
        $data = $this->lang->line('all');
        $data['lang'] = $this->session->userdata('site_lang');

        $data['latest_news'] = $this->Model_latest_news->getAll(true);

        //echo "<pre>"; print_r($data['latest_news']); exit;
        $data['content'] = 'customer_service/latest_news';
        $data['class'] = 'latest_news';

        $this->load->view('default',$data);

    }


    public function payment_confirm()
    {
        $data = array();
        $data = $this->lang->line('all');
        $data['lang'] = $this->session->userdata('site_lang');

        $data['content'] = 'customer_service/payment_confirm';
        $data['class'] = 'payment_confirm';

        $this->load->view('default',$data);

    }

    public function returns()
    {
        $data = array();
		$data = $this->lang->line('all');
        $data['lang'] = $this->session->userdata('site_lang');
        $data['returns'] = 	$this->Model_return_policy->get('1',true);
        $data['content'] = 'customer_service/returns';
        $data['class'] = 'returns';

        $this->load->view('default',$data);

    }

    public function distributor()
    {
        $data = array();
        $data = $this->lang->line('all');
        $data['lang'] = $this->session->userdata('site_lang');

        $data['distributor'] = 	$this->Model_distributor->get('1',true);

        $data['content'] = 'customer_service/distributor';
        $data['class'] = 'distributor';

        $this->load->view('default',$data);

    }

    public function privacy_policy()
    {
        $data = array();
        $data = $this->lang->line('all');
        $data['lang'] = $this->session->userdata('site_lang');

        $data['privacy_policy']	 = 	$this->Model_privacy_policy->get('1',true);

        $data['content'] = 'customer_service/privacy_policy';
        $data['class'] = 'privacy_policy';

        $this->load->view('default',$data);

    }


    public function payment_confirm_save(){

	    $response = array();
		$data = $this->lang->line('all');
        $data['lang'] = $this->session->userdata('site_lang');

	    $post_data = $this->input->post();


	    // check order exist
	    $order_no =  $post_data['order_number'];
        $order_id = ltrim($order_no, '0');

        $orders = $this->Model_orders->get($order_id);
	    if($orders) {
            $settings = getSettings();
            $to = $settings->admin_email;
            $subject = "Payment Confirmation Request";
            $message = "Payment Confirmation Request has done.";
            $post_data['order_number'] = str_pad($order_id, 6, "0", STR_PAD_LEFT);
            $saved = $this->Model_payment_confirm->save($post_data);

            if ($saved) {
                send_email_admin($subject, $message, $to);
            }
            $response['success'] = true;
            $response['message'] = $data['form_submitted_successfully'];
        }else{
            $response['success'] = false;
            $response['message'] = $data['order_not_find'];
        }

        echo json_encode($response);
	    exit;
        // also send email here to admin then save data
        //redirect($_SERVER['HTTP_REFERER']);

    }
    
    public function distributor_request(){

		$result = array();
		$data = $this->lang->line('all');
        $distributor_image = array();
        $post_data = $this->input->post();
		$settings = getSettings();

		$insert_id = $this->Model_distributor_requests->save($post_data);

		$subject = "New Distributor Request";
		$message = "New Distributor Request has received";
		$to = $settings->admin_email;
	
        // also send email here to distributor to confirm his/her request
        if($insert_id > 0){
				send_email_admin($subject, $message, $to);
				if($_FILES['image']['name'][0] != '' ){
					// create configuration fields for setting
					$config_array['path']       = 'uploads/images/distributors/';
					//$config_array['thumb_path'] = 'uploads/images/thumbs/products/';
					$config_array['height']     = '300'; //image thumb height
					$config_array['width']      = '300'; //image thumb width
					$config_array['field']      = 'image';//this is field name for html form
					$config_array['allowed_types']      = 'gif|png|jpg';//this is field name for html form

					$images = $this->file_upload->uploadFiles($config_array);
					
					if($images)
					{
						$i=0;
						foreach($images as $image)
						{
							$distributor_image['distributor_id'] = $insert_id;
							$distributor_image['distributor_image']  = $image;
							$distributor_image['user_id'] = $this->session->userdata['user']['id'];
							$this->Model_distributor_images->save($distributor_image);
							$i++;
						}
					}
				}
			
			$result['response'] = true;
			$result['message'] = $data['form_submitted_successfully'];
			echo json_encode($result);
			exit;
		}else{
			$result['response'] = false;
			$result['message'] = $data['form_submitted_failed'];
			echo json_encode($result);
			exit;
		}
    }


    private function payment_validation()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<p>', '</p>');

        $this->form_validation->set_rules('order_number', 'Order Number', 'required');
        $this->form_validation->set_rules('customer_name', 'Customer Name', 'required');
        $this->form_validation->set_rules('bank_name', 'Bank Name', 'required');
        $this->form_validation->set_rules('account_holder', 'Account Holder', 'required');
        $this->form_validation->set_rules('transfer_date', 'Transfer Date', 'required');
        $this->form_validation->set_rules('transfer_amount', 'Transfer Amount', 'required');
        $this->form_validation->set_rules('mobile_no', 'Mobile Number', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            //$errors = $this->form_validation->error_array();
           // return $errors;
            return false;

        }else
        {
            return true;
        }

    }

    private function distributor_validation()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<p>', '</p>');

        $this->form_validation->set_rules('full_name', 'Name', 'required');
        $this->form_validation->set_rules('mobile_no', 'Mobile number', 'required');
        $this->form_validation->set_rules('store_name', 'Store Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('country', 'Country', 'required');
        $this->form_validation->set_rules('city', 'City', 'required');
        $this->form_validation->set_rules('store_number', 'Store number', 'required');
        //$this->form_validation->set_rules('image', 'images', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            //$errors = $this->form_validation->error_array();
           // return $errors;
            return false;

        }else
        {
            return true;
        }

    }


    public function medauf_program(){
        $data = array();
        $data = $this->lang->line('all');
        $data['lang'] = $this->session->userdata('site_lang');

        $data['medauf_program']	 = 	$this->Model_meduaf_program->get('1',true);

        $data['content'] = 'customer_service/meduaf_program';
        $data['class'] = 'medauf_program';

        $this->load->view('default',$data);

    }
	
}