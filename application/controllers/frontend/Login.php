<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->lang->load("message", $this->session->userdata('site_lang'));
        $this->load->model('Model_registered_users');
        $this->load->model('Model_registered_user_address');
    }
    public function login()
    {
        $data = array();
        $message = $this->lang->line('all');
        $post_data = $this->input->post();
        // login popup files validation
        if ($post_data['username'] == '' || $post_data['password'] == '') {
            if ($post_data['username'] == '') {
                $data['username'] = '';
            } else {
                $data['username'] = $post_data['username'];
            }
            if ($post_data['password'] == '') {
                $data['password'] = '';
            } else {
                $data['password'] = sha1($post_data['password']);
            }
            echo json_encode($data);
            exit();
        } else {
            $postData = array();
            $data['username'] = $post_data['username'];
            $data['password'] = $post_data['password'];
            $postData['email'] = $post_data['username'];
            $postData['password'] = sha1($post_data['password']);
            $checkUser = $this->checkUser($postData);
            if ($checkUser != true) {
                $data = array();
                $data['success'] = '0';
                $data['message'] = $message['username_password_invalid_label'];
                echo json_encode($data);
                exit();
            } else {
                $data = array();
                $data['success'] = '1';
                $data['message'] = $message['login_success'];
                if ($post_data['formType'] == 'login') {
                    $data['redirectUrl'] = lang_base_url() . 'profile';
                } else {
                    $data['redirectUrl'] = lang_base_url() . 'shopping_cart';
                }
                echo json_encode($data);
                exit();
            }
        }
    }
    private function checkUser($post_data)
    {
        $user = $this->Model_registered_users->getSingleRow($post_data);
        if ($user != false) {
            $this->session->set_userdata('user_id', $user->id);
            $this->session->set_userdata('user_name', $user->first_name . " " . $user->last_name);
            $this->session->set_userdata('login', true);
            return true;
        } else {
            return false;
        }
    }
    public function logout()
    {
        $data = array();
        $arr_update = array();
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('login');
        redirect(lang_base_url());
    }
    public function forgotPassword()
    {
        $response = array();
        $msg = $this->lang->line('all');
        $lang = $this->session->userdata('site_lang');
        $email = $this->input->post('email');
        $isExist['email'] = $email;
        $existEmail = $this->Model_registered_users->getSingleRow($isExist);
        if (!$existEmail) {
            $response['exist'] = false;
            $response['message'] = $msg['forgot_password_email_not_found'];
            echo json_encode($response);
            exit;
        } else {
            $data['reset_token'] = randomPassword(20);
            $where_email['email'] = $email;
            $this->Model_registered_users->update($data, $where_email);
            $link = lang_base_url() . 'login/reset_password/' . $data['reset_token'];
            // set password in session to recheck
            $emailData['message'] = $msg['password_request_please_click'] . ' <a href="' . $link . '" target="_blank">' . $msg["password_request_click_here"] . '</a> ' . $msg['password_request_thanks'];

            $subject = $msg['forgot_password_email'];

            $emailData['title'] = $msg['forgot_password_email'];
            $emailData['info'] = array();

            if($lang == "eng") {
                $view = $this->load->view('layouts/emails/eng_general_email',$emailData,TRUE);
            }else{
                $view = $this->load->view('layouts/emails/arb_general_email',$emailData,TRUE);
            }
            $sent = send_email_user($subject, $view, $email);

            if ($sent) {
                $response['exist'] = true;
                $response['message'] = $msg['send_new_password_msg'];
            } else {
                $response['exist'] = false;
                $response['message'] = $msg['send_new_password_failed'];
            }
            echo json_encode($response);
            exit;
        }
    }
    public function reset_password()
    {
        $lang = $this->session->userdata('site_lang');
        if ($lang == 'eng') {
            $isExist['reset_token'] = $this->uri->segment(4);
        } else {
            $isExist['reset_token'] = $this->uri->segment(3);
        }
        $existEmail = $this->Model_registered_users->getSingleRow($isExist);
        if ($existEmail) {
            $data = array();
            $data = $this->lang->line('all');
            $data['reset_token'] = $isExist['reset_token'];
            $data['lang'] = $this->session->userdata('site_lang');
            $data['content'] = 'user/reset_password';
            $data['class'] = 'rest_password';
            $this->load->view('default', $data);
        } else {
            redirect(lang_base_url());
        }
    }
    public function reset_password_request()
    {
        $msg = $this->lang->line('all');
        $response = array();
        $isExist['reset_token'] = $this->input->post('reset_token');
        $existEmail = $this->Model_registered_users->getSingleRow($isExist);
        $enterd_password = $this->input->post('password');
        if ($existEmail) {
            $data['password'] = sha1($enterd_password);
            $data['reset_token'] = '';
            $reset_token['reset_token'] = $isExist['reset_token'];
            $this->Model_registered_users->update($data, $reset_token);
            $response['error'] = false;
            $response['message'] = $msg["password_reset_successfully"];
        } else {
            $response['error'] = true;
            $response['message'] = $msg["url_expire"];
        }
        echo json_encode($response);
        exit;
    }
    
}
