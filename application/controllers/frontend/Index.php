<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Index extends CI_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
        parent::__construct();
		
		$this->load->helper('url');
		$get_url_arr = explode('/', $_SERVER['REQUEST_URI']);
		if(!$this->session->userdata('site_lang'))
		{
			$this->session->set_userdata('site_lang','arb');
		}
		if(in_array('en', $get_url_arr))
		{
			$this->session->set_userdata('site_lang','eng');
		}
		else
		{
			$this->session->set_userdata('site_lang','arb');
		}
        $this->lang->load("message",$this->session->userdata('site_lang'));
	    $this->load->model('Model_product');
	    $this->load->model('Model_category');
	    $this->load->model('Model_variant');
	    $this->load->model('Model_product_variant_value');
	    $this->load->model('Model_product_variant_group');
		$this->load->model('Model_home_content');
	    $this->load->model('Model_home_slider');
		//$res = checkLevels(2);
		//checkAuth($res);
    }
	
	public function index()
	{
		$data = array();
		$data = $this->lang->line('all');
		$data['lang'] = $this->session->userdata('site_lang');
		$prod_all['active'] = 1;
		$prod_all['display_to_home_product'] = 1;
		$prod_all['category_status'] = 1;
		$data['products'] = $this->Model_product->getProductWithCategory($prod_all, true);
		//echo "<pre>"; print_r($data['products']); exit;
		$is_home_offer = '1';
		$daily_offer = '1';
		$category_status = 1;
		$data['offers'] = $this->Model_product->getOfferByDates($is_home_offer,$daily_offer,$category_status);
        $data['home_content'] = (array)$this->Model_home_content->get('1');
		$home_slider['is_active'] = '1';
		$data['images'] = $this->Model_home_slider->getMultipleRows($home_slider,true,'ASC','slider_order');
		//$data['meta_contents'] = getPageData($data['home_content']['home_page_id'], true);
		$data['content'] = 'index';
		$data['class'] = 'index';
		$this->load->view('default',$data);
	}
	
	public function query(){
        $this->load->model('Model_registered_users');
        $this->load->model('Model_cart_user_address');
        $res = run();
        echo "<pre>"; print_r($res); exit;
       foreach($res as $re){
           $orderUpdate['id'] = $re->id;
           $data['region'] = $re->city_name;
           $data['zip_code'] = "11111";
           $this->Model_cart_user_address->update($data,$orderUpdate);
       }
    }
    public function optimus(){
        $img = existImgOptimus();
        echo $img;
    }

}
