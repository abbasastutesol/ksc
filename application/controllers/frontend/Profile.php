<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Profile extends CI_Controller {



	 public function __construct()

    {

        parent::__construct();

       

		$this->lang->load("message",$this->session->userdata('site_lang'));
		
		$this->load->model('Model_registered_users');
		
		$this->load->model('Model_cart_user_address');
		
		$this->load->model('Model_registered_user_address');
		$this->load->model('Model_orders');
		$this->load->model('Model_order_product');
		$this->load->model('Model_return_order');
		$this->load->model('Model_order_feedback');
		$this->load->model('Model_settings');
		$this->load->model('Model_customer_loyalty');

        // if maintenanc mode is on off
        maintenanceMode('redirect');

    }





    public function index()

    {

        $data = array();
		
		$data = $this->lang->line('all');
        $data['lang'] = $this->session->userdata('site_lang');
        $fetch_by['id'] = $user_id = $this->session->userdata('user_id');

        $data['user'] = $this->Model_registered_users->getSingleRow($fetch_by);

        $getLoyalty['user_id'] = $user_id;
        $data['loyalty'] = $this->Model_customer_loyalty->getSingleRow($getLoyalty);

        $address = $this->Model_cart_user_address->getMultipleRows(array('user_id'=>$user_id),true);

        $data['addresses'] = $address;
        
        $data['content'] = 'user/profile';

        $data['class'] = 'profile';

        $this->load->view('default',$data);



    }
    
	public function myOrder()
	{
		$get_compete = array();
		$data = $this->lang->line('all');
		$data['lang'] = $this->session->userdata('site_lang');
        
		$user_id = $this->session->userdata('user_id');
		$data['user'] = getUserById($user_id);

		$data['current_orders'] = $this->Model_orders->getCurrentOrdres($user_id,true);

		$get_compete['user_id'] =  $user_id;
		$get_compete['order_status'] = 6;
		$get_compete['payment_order_status'] = '1';
		$data['complete_orders'] = $this->Model_orders->getMultipleRows($get_compete,true);
		$data['feedback'] = $this->Model_order_feedback->get(1);

		$data['content'] = 'cart/order_view';
		$data['class'] = 'order_view';
		$this->load->view('default',$data);
	}
	
	
	public function cancel_order() {
		
		$messages = $this->lang->line('all');
		$response = array();
        $lang = $this->session->userdata('site_lang');
		$order_id = $this->input->post('id');
		$updatedData['order_status'] = 10;

        $emailData['message'] = $messages['hi_label']." IOUD, <br>  ".$messages['want_to_cancel_order']." ".str_pad($order_id, 6, '0', STR_PAD_LEFT);
        $admin_email = $this->Model_settings->get('1');
        $to = $admin_email->admin_email;
        $subject = $messages['cancel_request_order'];
        
        $emailData['title'] = $messages['cancel_request_order'];
        $emailData['info'] = array();

        if($lang == "eng") {
            $view = $this->load->view('layouts/emails/eng_general_email',$emailData,TRUE);
        }else{
            $view = $this->load->view('layouts/emails/arb_general_email',$emailData,TRUE);
        }
        $sent = send_email_admin($subject, $view, $to);

        //$updatedData['receipt'] = 'dummy_fill';
		$update = $this->Model_orders->update($updatedData,array("id"=>$order_id));
		
		if($update) {
		 	$response['success'] = true;

            $response['message'] = $messages['cancel_order_success_msg'];
		}else{
			$response['success'] = false;

            $response['message'] = $messages['cancel_order_failed_msg'];
		}
		
		echo json_encode($response);

		exit;
		
		
	}
	
	
	public function return_order() {
		
		$messages = $this->lang->line('all');
		$response = array();
        $lang = $this->session->userdata('site_lang');
		$post_data = $this->input->post();
		
		$order_id = $this->input->post('order_id');
		$updatedData['order_status'] = 7;
		
		$update = $this->Model_orders->update($updatedData,array("id"=>$order_id));

        $emailData['message'] = $messages['hi_label']." IOUD, <br> ".$messages['want_to_return_order']." ".str_pad($order_id, 6, '0', STR_PAD_LEFT)." ".$messages['return_request_reason_label']." ".$post_data['reason'];
        $admin_email = $this->Model_settings->get('1');
        $to = $admin_email->admin_email;
        $subject = $messages['return_request_order'];

        $emailData['title'] = $messages['return_request_order'];
        $emailData['info'] = array();


        if($lang == "eng") {
            $view = $this->load->view('layouts/emails/eng_general_email',$emailData,TRUE);
        }else{
            $view = $this->load->view('layouts/emails/arb_general_email',$emailData,TRUE);
        }
        if(isset($order_id) && $order_id != 0) {
            $sent = send_email_admin($subject, $view, $to);
        }
		$save = $this->Model_return_order->save($post_data);
	
		if($save) {
		 	$response['success'] = true;

            $response['message'] = $messages['return_order_success_msg'];
		}else{
			$response['success'] = false;

            $response['message'] = $messages['return_order_failed_msg'];
		}
		
		echo json_encode($response);

		exit;
		
		
	}

}

