<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ContactUs extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
        parent::__construct();
		
		$this->load->helper('url');
		$get_url_arr = explode('/', $_SERVER['REQUEST_URI']);
		if(!$this->session->userdata('site_lang'))
		{
			$this->session->set_userdata('site_lang','arb');
		}
		if(in_array('en', $get_url_arr))
		{
			$this->session->set_userdata('site_lang','eng');
		}
		else
		{
			$this->session->set_userdata('site_lang','arb');
		}
        $this->lang->load("message",$this->session->userdata('site_lang'));
	    $this->load->model('Model_contact_us');
	    $this->load->model('Model_city');
	    $this->load->model('Model_contact_us_feedback');


    }
	
	public function index()
	{
		$data = array();
		$data = $this->lang->line('all');
		$data['lang'] = $this->session->userdata('site_lang');

		$data['contact_us'] = $this->Model_contact_us->get('1',true);
		$data['cities'] = $this->Model_city->getAll(true);
		//echo "<pre>"; print_r($data['cities']); exit;
		$data['content'] = 'contact_us/contact_us';
		$data['class'] = 'contact_us';

		$this->load->view('default',$data);	
		
	}

	public function save(){

	    $post_data = $this->input->post();
        /*$validation = $this->validation();*/
        echo "<pre>"; print_r($post_data); exit;
	    // before saving the data send the email to the admin
         $this->Model_contact_us_feedback->save($post_data);
        $data['success'] = true;        echo json_encode($data);	    exit;
    }



    private function validation()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<p>', '</p>');

        $this->form_validation->set_rules('full_name', 'Name', 'required');
        $this->form_validation->set_rules('city', 'City', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('message', 'Message', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            return validation_errors();
        }else
        {
            return true;
        }

    }
	
}
