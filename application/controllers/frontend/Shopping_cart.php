<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Shopping_cart extends CI_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$get_url_arr = explode('/', $_SERVER['REQUEST_URI']);
		if(!$this->session->userdata('site_lang'))
		{
			$this->session->set_userdata('site_lang','arb');
		}
		if(in_array('en', $get_url_arr))
		{
			$this->session->set_userdata('site_lang','eng');
		}
		else
		{
			$this->session->set_userdata('site_lang','arb');
		}
		$this->lang->load("message",$this->session->userdata('site_lang'));
		$this->load->model('Model_category');
		$this->load->model('Model_orders');
		$this->load->model('Model_order_product');
		$this->load->model('Model_product');
		$this->load->model('Model_temp_orders');
		$this->load->model('Model_temp_user_detail');
		$this->load->model('Model_product_variant_group');
		$this->load->model('Model_registered_user_address');
		$this->load->model('Model_registered_user_cards');
		$this->load->model('Model_registered_users');
		$this->load->model('Model_cart_user_address');
        $this->load->model('Model_shipment_methods');
        $this->load->model('Model_gifts');
		$this->load->model('Model_order_gifts');		
		$this->load->model('Model_coupons');		
		$this->load->model('Model_coupon_use');
		
		$this->load->model('Model_settings');
		$this->load->model('Model_wire_transfer');

		$this->load->model('Model_shipment_group_country');
		$this->load->model('Model_shipment_groups');
	    $this->load->model('Model_groups_payment');
	    $this->load->model('Model_general');

	    $this->load->model('Model_booking_cc_payment');
	    $this->load->model('Model_booking_sadad_payment');

		$this->load->library('PayfortCheckout');

		//$res = checkLevels(2);
		//checkAuth($res);

        // if maintenanc mode is on off
        maintenanceMode('redirect');
        // check if voucher is expire or note
        checkVourcherIsExpire();

	}
	
	public function index()
	{
	    $data = array();
		$data = $this->lang->line('all');
        $data['lang'] = $this->session->userdata('site_lang');
		/*if ($this->session->userdata('login') == false && !$this->session->userdata('user_id'))
		{
			$get_by['user'] = get_cookie('u_id');
		}
		else
		{
	    	$get_by['user'] = $this->session->userdata('user_id');
		}*/
		$get_by['user'] = get_cookie('u_id');
		$get_by['saved'] = 0;
        $data['carts'] = $this->Model_temp_orders->getMultipleRows($get_by,true);
        $products = array();
        foreach ($data['carts'] as $cart){
            $getBy['id'] = $cart['p_id'];
            $prods = $this->Model_product->getSingleRow($getBy,true);
            $products[] = $prods;
        }
		
		$get_by['saved'] = 1;
        $data['saved_carts'] = $this->Model_temp_orders->getMultipleRows($get_by,true);
		
		$saved_products = array();
        foreach ($data['saved_carts'] as $cart){
            $getBy['id'] = $cart['p_id'];
            $prods = $this->Model_product->getSingleRow($getBy,true);
            $saved_products[] = $prods;
        }
        $data['gifts'] = $this->Model_gifts->getAll(true);
        //echo "<pre>"; print_r($data['gifts']); exit;
        $data['products'] = $products;
		
		$data['saved_products'] = $saved_products;
        $data['content'] = 'cart/shopping_cart';
        $data['class'] = 'shopping_cart';
        $this->load->view('default',$data);
	}

	public function shippingAddress()
	{
		$data = $this->lang->line('all');
		$data['lang'] = $this->session->userdata('site_lang');
		$u_id = '';
		
		if(get_cookie('u_id'))
		{
			$user_id = get_cookie('u_id');
		}
		else
		{
			redirect(lang_base_url() . 'product');
		}
		$data['cart_data'] = $this->Model_temp_orders->getMultipleRows(array('user'=>$user_id, 'saved'=>'0'),true);
		$i=0;
		if($data['cart_data'])
		{
            $products = array();
            foreach ($data['cart_data'] as $cart){
                $getBy['id'] = $cart['p_id'];
                $prods = $this->Model_product->getSingleRow($getBy,true);
                $products[] = $prods;
            }
            $data['products'] = $products;
			if($this->session->userdata('login') == true && $this->session->userdata('user_id'))
			{
            	$address = $this->Model_cart_user_address->getMultipleRows(array('user_id'=>$this->session->userdata('user_id')),true);
            }
			else
			{
				$address = $this->Model_cart_user_address->getMultipleRows(array('user_id'=>$user_id, 'used'=>0),true);
			}
            $data['addresses'] = $address;
        }
		else
		{
			redirect(lang_base_url() . 'product');
		}

        $data['content'] = 'cart/shipping_address';
		$data['class'] = 'shipping_address';
		$this->load->view('default',$data);
	}
	
    public function addToCartPopup()
    {
        $fetch_by['id'] = $prod_id  = $this->input->post('id');
        $quantity = $this->input->post('quantity');
        $product_row = $this->Model_product->getSingleRow($fetch_by,true);
        $lang = $this->session->userdata('site_lang');
        $totalPrice = $product_row[$lang . "_price"]*$quantity;
        $images = getProductImages($product_row['id']);
        $img = base_url().'uploads/images/thumbs/products/'.$images[0][$lang.'_image'];
        $this->session->set_userdata('prod_quantity',$quantity);
        $this->session->set_userdata('product_id',$prod_id);
        if($product_row) {
            $html = '<table>'.
                        '<tbody>'.
                        '<tr>'.
                            '<td>'.
                                '<img src="'.$img.'" alt="img" height="46" width="52" />';
            $html .= $product_row[$lang . "_name"];
            $html .=   '</td>'.
                            '<td>Qty: <span>' . $quantity . '</span></td>'.
                            '<td>' . number_format($totalPrice, '2', '.', '') . ' <span>SR</span></td>'.
                        '</tr>'.
                        '</tbody>';
        }
        echo json_encode($html);
        exit();
    }

    public function productAddToCart(){
        $response = array();
        $messages = $this->lang->line('all');
        $quantity = $this->input->post('quantity');
        $prod_id = $this->input->post('id');
        $lang = $this->session->userdata('site_lang');
        $checkQuan['id']= $prod_id;
        $checkQuantity = $this->Model_product->getSingleRow($checkQuan,true);

        if((int)$checkQuantity[$lang.'_quantity'] > 0) {
            $data['p_id'] = $prod_id;
            $fetch_by['id'] = $prod_id;
            $product_row = $this->Model_product->getSingleRow($fetch_by, true);
            if ($this->session->userdata('login') == false && !$this->session->userdata('user_id')) {
                $data['guest'] = 1;
                $data['user_id'] = 0;
            } else {
                $data['guest'] = 0;
                $data['user_id'] = $this->session->userdata('user_id');
            }
            if (!get_cookie('u_id')) {
                $cid = strtotime(date('Y-m-d H:i:s')) . rand(0, 000);
                $cookie = array(
                    'name' => 'u_id',
                    'value' => $cid,
                    'expire' => time() + (10 * 365 * 24 * 60 * 60)
                );
                $this->input->set_cookie($cookie);
            } else {
                $cid = get_cookie('u_id');
            }
            $data['user'] = $cid;
            $productPrice = getPriceByOffer($prod_id);
            $productPrice = currencyRatesCalculate($productPrice);
            $data['prod_price'] = $productPrice['rate'];
            $data['quantity'] = $quantity;
            $checkDup_by['user'] = $cid;
            $checkDup_by['p_id'] = $prod_id;
            $checkDup = $this->Model_temp_orders->getRowCount($checkDup_by);
            if ($checkDup >= 1) {
                $response['message'] = '<div class="modal-header">
			
							<h4 class="modal-title green" id="myModalLabel" style="padding-bottom: 12px;color: red !important;">' . $messages['shopping_product_already_added_to_cart'] . '</h4>
			
						</div>';
                $response['success'] = 0;
                echo json_encode($response);
                exit;
            }

            $data = $this->currencyDetail($data);

            $this->Model_temp_orders->save($data);
            /*show product in popup window*/
            $fetch_by['id'] = $prod_id = $this->input->post('id');
            $quantity = $this->input->post('quantity');
            $product_row = $this->Model_product->getSingleRow($fetch_by, true);
            $lang = $this->session->userdata('site_lang');
            $totalPrice = getPriceByOffer($prod_id) * $quantity;
            $images = getProductImages($product_row['id']);

            $prod_image =  getProductImages($prod_id);

            foreach($prod_image as $key => $prod_img){
                if($prod_img['is_thumbnail'] == 1){
                    $thumbnail = $prod_img['thumbnail'];
                }
            }

            $img = base_url() . 'uploads/images/thumbs/products/' . $thumbnail;
            $this->session->set_userdata('prod_quantity', $quantity);
            $this->session->set_userdata('product_id', $prod_id);

            $totalPrice = currencyRatesCalculate($totalPrice);

            if ($product_row) {
                $html = '<div class="modal-header">
			
							<h4 class="modal-title green" id="myModalLabel">' . $messages['shopping_cart_added_to_cart'] . '</h4>
			
						</div>
						<div class="modal-body">
			
							<div class="addCartPopUp">
			
								<table>
			
									<tbody>
										<tr>
					
										<td>
					
										<img src="' . $img . '" alt="img" height="46" width="52" />';
                $html .= '<span class="cartItemTitle">'.$product_row[$lang . "_name"].'</span>';
                $html .= '</td>
					
										<td>' . $messages["shopping_cart_qty"] . ': <span>' . $quantity . '</span></td>
					
										<td>' . $totalPrice["rate"] . ' <span>' . $totalPrice["unit"] . '</span></td>
					
										</tr>
					
										</tbody>
			
								</table>
			
								<div class="clearfix"></div>
			
								<a href="' . lang_base_url() . 'shopping_cart' . '">
			
								<button type="button" class="btn btn-success" >
			
									<i class="sprite_ioud ioudSpcart"></i>
			
									&nbsp;&nbsp;
			
									' . $messages['shopping_cart_proceed_to_cart'] . '
			
								</button>
			
								</a>
			
								<a  href="' . lang_base_url() . 'product' . '"><button type="button" class="btn btn-default black" value="">' . $messages['checkout_continue_shopping'] . '</button></a>
			
								<div class="clearfix"></div>
			
							</div>
			
						</div>';
            }
            $response['html'] = $html;
            $response['success'] = 1;
            $response['message'] = $messages['shopping_product_added_to_cart'];
        }else{
            $response['success'] = 0;
            $response['message'] = '<div class="modal-header">
			
							<h4 class="modal-title green" id="myModalLabel" style="padding-bottom: 12px;color: red !important;">'.$messages['product_out_of_stock'].'</h4>
			
						</div>';
        }
            /*end popup */
            echo json_encode($response);
            exit;
    }

	public function addToCart()
	{
		$data = array();
		$fetch_by['id'] = $this->input->post('id');
		$product_row = $this->Model_product->getSingleRow($fetch_by);
		if($product_row)
		{
			$id = $product_row->id;
		}
		$data['customer_bought'] = $this->Model_product->sameCustomerProduct($id);
		$data['same_category_products'] = $this->Model_product->getMultipleRows(array('category_id'=>$getproduct->category_id, 'active'=>1), true);
		$data['content'] = 'cart/added_to_cart';
		$data['class'] = 'added_to_cart';
		$this->load->view('default',$data);	
		
	}

	public function setPaymentMethod()
	{
		$payment_method = $this->input->post('paymentMethod');
		if($payment_method == 'COD')
		{
			$cookie = array(
				'name'   => 'payment_method',
				'value'  => $payment_method,
				'expire' => time()+(10 * 365 * 24 * 60 * 60)
			);
			$this->input->set_cookie($cookie);
		}
		else
		{
			$user_card_id = $this->input->post('card_id');
			$cookie = array(
				'name'   => 'payment_method',
				'value'  => $payment_method,
				'expire' => time()+(10 * 365 * 24 * 60 * 60)
			);
			$this->input->set_cookie($cookie);
			$cookie = array(
				'name'   => 'user_card_id',
				'value'  => $user_card_id,
				'expire' => time()+(10 * 365 * 24 * 60 * 60)
			);
			$this->input->set_cookie($cookie);
		}
		$response['success'] = 1;
		echo json_encode($response);
		exit;
	}

	public function checkout(){

        /* if cart is empty then redirect to product page*/
            redirectToProducts();
        /*==============================================*/
        $data = array();
		$data = $this->lang->line('all');
		
		if($this->input->post('address_id'))
		{
        	$address_id = $this->input->post('address_id');
		}
		else
		{
			$address_id = get_cookie('address_id');
		}

		if(get_cookie('u_id'))
		{
			$user_id = get_cookie('u_id');
		}
		else
		{
			redirect(lang_base_url() . 'product');
		}
        $data['lang'] = $this->session->userdata('site_lang');
        $get_by['user'] = $user_id;
		
		$get_by['saved'] = '0';
        $data['carts'] = $this->Model_temp_orders->getMultipleRows($get_by,true);
        $products = array();
        foreach ($data['carts'] as $cart){
            $getBy['id'] = $cart['p_id'];
            $prods = $this->Model_product->getSingleRow($getBy,true);
            $products[] = $prods;
        }
		if($this->input->post('shipping_method'))
		{
        	$shipping_method = $this->input->post('shipping_method');
		}
		else
		{
			$shipping_method = get_cookie('shipping_method');
		}
		$shippingarr = explode('|', $shipping_method);

		$address = $this->Model_cart_user_address->get($address_id);

		//echo "<pre>"; print_r($shippingarr); exit;
		/*if($shippingarr[0] == 0) {
            $shipping = 'aramex';
            $payments_allow = 'aramex';
        }else{*/

            $shipping = $this->Model_shipment_groups->get($shippingarr[0]);

            $paymentArr = $this->Model_groups_payment->getMultipleRows(array('group_id' => $shippingarr[0]));

            $payments_allow = array();

            foreach ($paymentArr as $payment) {
                $payments_allow[] = $payment->payment_type;
            }
        //}

		$cookie = array(
			'name'   => 'address_id',
			'value'  => $address_id,
			'expire' => time()+(10 * 365 * 24 * 60 * 60)
		);
		$this->input->set_cookie($cookie);

		$cookie = array(

			'name'   => 'shipping_method',
			'value'  => $shipping_method,
			'expire' => time()+(10 * 365 * 24 * 60 * 60)
		);
		$this->input->set_cookie($cookie);
		
        //$this->session->set_userdata('shopping_address',$arrAddress);
        //$this->session->set_userdata('shipping_method',$data['shipping_method']);
		$country = getCoutryByCode($address->country, true);
		
		$data['country'] = $country;
        $data['address'] = $address;
        $data['products'] = $products;
		
		$data['shipping'] = $shipping;
		
		$data['payments_allow'] = $payments_allow;
		
		$data['shipping_amount'] = $shippingarr[1];
		
		$data['type'] = $shippingarr[2];

		$data['wire_transfers'] = $this->Model_wire_transfer->getAll(true);


        // save temp order for IPN
        $grandTotal = $this->input->post('temp_total_amount');

        $data['orderID'] = $this->tempOrderSave($grandTotal,$shipping_method,$address_id);
        //echo "<pre>"; var_dump($data['orderID']); exit;

        $data['cart_user_address'] = $address_id;
        $data['cart_user_id'] = $user_id;

        $data['content'] = 'cart/checkout';
        $data['class'] = 'checkout';
        $this->load->view('default',$data);
    }

    private function tempOrderSave($grandTotal,$shipmentArr,$address_id){

	    //------------------
        $lang = $this->session->userdata('site_lang');
        if(!$this->session->userdata('user_id') && $this->session->userdata('login') == false)
        {
            $user_id = get_cookie('u_id');
            $user_type = "0";
        }
        else
        {
            $user_id = $this->session->userdata('user_id');
            $user_type = "1";
        }

        $temp_ord_u_id = get_cookie('u_id');
        $data['address_id'] = $address_id;
        $address = $this->Model_cart_user_address->get($address_id);

        $settings = getSettings();

        $data['shipment_method'] = $settings->shipment_methods;

        $shipmentArr = explode('|', $shipmentArr);


        $data['shipment_group'] = $shipmentArr[0];

        $data['shipment_price'] = $shipmentArr[1];

        $data['shipment_type'] = $shipmentArr[2];

        $data['user_id'] = $user_id;

        $data['language'] = $lang;
        $data['total_amount'] = $grandTotal;

        $data['created_at'] = date('Y-m-d H:i:s');
        $data['user_type'] = $user_type;
        if(!$this->session->userdata('user_id') && $this->session->userdata('login') == false) {
            $data['guest_user_id'] = $address->id;
            $dataProd['guest_user_id'] = $address->id;
        }

        $data['payment_order_status'] = '0';

        $orderId = $this->session->userdata('temp_order_id');

        $data = $this->currencyDetail($data);
        if($orderId == NULL) {
            $orderId = $this->Model_orders->save($data);

        }else{

            $orderUpdate['id'] = $orderId;
            $this->Model_orders->update($data,$orderUpdate);
        }

        $i=0;
        $temp_orders = $this->Model_temp_orders->getMultipleRows(array('user' => $temp_ord_u_id, 'saved' => 0));

        $product_ids = array();
        foreach($temp_orders as $tem_order)
        {
            $product_ids[] = $tem_order->p_id;
            $dataProd['user_id'] = $user_id;
            $dataProd['order_id'] = $orderId;
            $dataProd['product_id'] = $tem_order->p_id;
            $dataProd['quantity'] = $tem_order->quantity;
            $dataProd['price'] = $tem_order->prod_price;
            $dataProd['guest'] = $tem_order->guest;

            $oId = $this->session->userdata('temp_order_id');

            $currency_detail = $this->session->userdata('currency_detail');
            $dataProd['currency_id'] = $currency_detail['currency_id'];
            $dataProd['currency_rate'] = $currency_detail['currency_rate'];
            $dataProd['currency_unit'] = $currency_detail['currency_unit'];

            /*updated code*/
            $existProd = $this->Model_order_product->getSingleRow(array('order_id'=>$orderId,'product_id'=>$tem_order->p_id));

            if(!$existProd){
                if($existProd->product_id != $tem_order->p_id) {
                    $this->Model_order_product->save($dataProd);
                }else{
                    $orderUpdateProd['order_id'] = $orderId;
                    $orderUpdateProd['product_id'] = $tem_order->p_id;
                    $up = $this->Model_order_product->update($dataProd,$orderUpdateProd);
                }

            }else{

                $orderUpdateProd['order_id'] = $orderId;
                $orderUpdateProd['product_id'] = $tem_order->p_id;
                $up = $this->Model_order_product->update($dataProd,$orderUpdateProd);
            }

            /*bug here*/
            /*if($oId == '') {
                $this->Model_order_product->save($dataProd);

            }else{
                $orderUpdate['order_id'] = $orderId;
                $orderUpdate['product_id'] = $tem_order->p_id;
                $this->Model_order_product->update($dataProd,$orderUpdate);
            }*/

            $product = $this->Model_product->get($tem_order->p_id);

            $prod['eng_quantity'] = ($product->eng_quantity-$tem_order->quantity);
            $prod['arb_quantity'] = ($product->eng_quantity-$tem_order->quantity);
            $prodUpdateBy['id'] = $tem_order->p_id;

            $this->Model_product->update($prod, $prodUpdateBy);

            $i++;
        }

        $this->session->set_userdata('temp_order_id',$orderId);
        return $orderId;

        //------------------//
    }

    private function currencyDetail($data){

        $currency_detail = $this->session->userdata('currency_detail');
        $data['currency_unit'] = $currency_detail['currency_unit'];
        $data['currency_rate'] = $currency_detail['currency_rate'];
        $data['currency_id'] = $currency_detail['currency_id'];
        return $data;
    }
	
	public function saveOrder()
	{
	    /*error_reporting(E_ALL);
	    ini_set('display_errors','1');*/
		$lang = $this->session->userdata('site_lang');
		if($this->input->post('card_brand'))
		{
			if($this->input->post('response_code') != '100')
			{
				$this->session->set_flashdata('paytabs_response', $this->input->post('response_message'));
				if($lang == 'eng')
				{		
					redirect($this->config->item('base_url') . 'en/shopping_cart/checkout');
				}
				else
				{
					redirect($this->config->item('base_url') . 'shopping_cart/checkout');
				}
			}
		}
		
		$cookie = array(
			'name'   => 'posted_data',
			'value'  => serialize($this->input->post()),
			'expire' => time()+(365 * 24 * 60 * 60)
		);
		$this->input->set_cookie($cookie);	
		$messages = $this->lang->line('all');
		$data = array();
		$dataProd = array();
		
		if(!$this->session->userdata('user_id') && $this->session->userdata('login') == false)
		{
			$user_id = get_cookie('u_id');
			$user_type = "0";
		}
		else
		{
			$user_id = $this->session->userdata('user_id');
            $user_type = "1";
		}
		
		$temp_ord_u_id = get_cookie('u_id');
		
		$data['address_id'] = get_cookie('address_id');
		if($this->input->post('card_method_new'))
		{
			if($this->input->post('card_method_new') == 'sadad')
			{
				$data['payment_method'] = 'SADAD';
				$data['card_id'] = '0';
				$data['payment_status'] = '0';
                $data['order_status'] = '2';
			}elseif($this->input->post('card_method_new') == 'bank_transfer')
			{
				$data['payment_method'] = 'bank_transfer';
				$data['card_id'] = '0';
				$data['payment_status'] = '0';
                $data['order_status'] = '2';
			}else
			{
				$data['payment_method'] = 'Cash On Delivery';
				$data['card_id'] = '0';
				$data['payment_status'] = '0';
				$data['order_status'] = '3';
			}
		}
		
		elseif($this->input->post('card_brand'))
		{
			$data['payment_method'] = $this->input->post('card_brand');
			$data['card_id'] = $this->input->post('transaction_id');
			$data['payment_status'] = '1';
            $data['order_status'] = '3';
		}
		if($this->input->post('total_amount'))
		{
			$data['total_amount'] = $this->input->post('total_amount');
		}
		else
		{
            $total_amount = currencyRatesCalculate($this->input->post('transaction_amount'));
            $data['total_amount'] = $total_amount['rate'];
		}

		if($this->input->post('card_method_new') == 'COD')
		{
            $paymentCharges = payment_methods();

            $extraAmounts = currencyRatesCalculate($paymentCharges->extra_cash_amount);

            $data['total_amount'] = ($data['total_amount'] + $extraAmounts['rate']);
            $data['extra_charges'] = $extraAmounts['rate'];

		}

        $address_id = get_cookie('address_id');
        $address = $this->Model_cart_user_address->get($address_id);
		
		if(get_cookie('voucher_code'))
		{
			$voucher_code = get_cookie('voucher_code');
			$fetch_by['code'] = $voucher_code;
			$coupon = $this->Model_coupons->getSingleRow($fetch_by);
			$data['coupon_id'] = $coupon->id;

			// old code updated by Kashif
			/*$address_id = get_cookie('address_id');
			$address = $this->Model_cart_user_address->get($address_id);*/
			
			$dataUse['coupon_id'] = $coupon->id;
			$dataUse['coupon_code'] = $voucher_code;
			$dataUse['email'] = $address->email;
			$this->Model_coupon_use->save($dataUse);
		}
		else
		{
			$data['coupon_id'] = 0;
		}
		$settings = getSettings();
		
		$data['shipment_method'] = $settings->shipment_methods;
		
		$shipmentArr = explode('|', get_cookie('shipping_method'));
		
		$data['shipment_group'] = $shipmentArr[0];
        $shipment_price = currencyRatesCalculate($shipmentArr[1]);
		$data['shipment_price'] = $shipment_price['rate'];
		
		$data['shipment_type'] = $shipmentArr[2];
		
		$data['user_id'] = $user_id;
		
		$data['language'] = $lang;	
		
		//$data['payment_status'] = '1';
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['user_type'] = $user_type;
        if(!$this->session->userdata('user_id') && $this->session->userdata('login') == false) {
            $data['guest_user_id'] = $address->id;
            $dataProd['guest_user_id'] = $address->id;
        }

        if($this->input->post('temp_order_id')) {
            $order_id = $this->input->post('temp_order_id');
        }else {
            $order_id = $this->input->post('order_id');
        }

        $data = $this->currencyDetail($data);

        $data['payment_order_status'] = '1';

        $ifcDiscount = getCouponPrice($data['total_amount']);
        if($ifcDiscount == 0) {
            $data['loyalty_discount'] = get_cookie('loyalty_discount');
        }

        $OrderUpdate['id'] = $order_id;
		$this->Model_orders->update($data,$OrderUpdate);

		// saving loyalty points for registered users
        if($this->session->userdata('user_id') && $this->session->userdata('login') == true) {

            saveLoyaltyPoints($data['total_amount'], $data['currency_rate'], $user_id);
        }
        //========================================

		$i=0;
		$temp_orders = $this->Model_temp_orders->getMultipleRows(array('user' => $temp_ord_u_id, 'saved' => 0));
		$product_ids = array();

		foreach($temp_orders as $tem_order)
		{
			$product_ids[] = $tem_order->p_id;

			$this->Model_temp_orders->delete(array("id"=>$tem_order->id));
			
			$i++;
		}

		$giftIds = unserialize(get_cookie('giftIds'));
        
		foreach($giftIds as $giftId)
		{
			$dataGift['order_id'] = $order_id;
			$dataGift['gift_id'] = $giftId;
			$this->Model_order_gifts->save($dataGift);
		}

		$updateAdd['used'] = 1;
		$updateAdd['order_id'] = $order_id;
		$updateAddBy['id'] = get_cookie("address_id");
		$this->Model_cart_user_address->update($updateAdd, $updateAddBy);
        // kashif work on Sep -14-2017
		
		delete_cookie("address_id");
		delete_cookie("payment_method");
		delete_cookie("user_card_id");
		delete_cookie("shippment_amount");
		delete_cookie("shipment_price");
		delete_cookie("shipment_type");
		delete_cookie("giftIds");
		delete_cookie("voucher_code");
        delete_cookie('loyalty_discount');
        delete_cookie('discount_type');
		/*$response = $messages;
		$response['success'] = 1;
		$response['lang'] = $lang;
		$response['customer_bought'] = $customer_bought;
		$response['product_count'] = $this->input->post('product_count');
		$response['full_name'] = $this->input->post('full_name');
		$response['delivery_time'] = $this->input->post('delivery_time');
		$response['order_id'] = str_pad($order_id, 10, "0", STR_PAD_LEFT);
		$response['content'] = 'cart/thank_you';
		$response['class'] = 'thank_you';*/

		if($lang == 'eng')
		{		
			redirect($this->config->item('base_url') . 'en/shopping_cart/orderPlaced/'.$order_id);
		}
		else
		{
			redirect($this->config->item('base_url') . 'shopping_cart/orderPlaced/'.$order_id);
		}
	}

    public function orderPlaced($order_id){

        // unset temp order id... it is using for IPN
        $this->session->unset_userdata('temp_order_id');
        $this->session->set_userdata('temp_order_id','');
        //========================================//
		$data = $this->lang->line('all');
        $data['lang'] = $this->session->userdata('site_lang');
		
      	$order = $this->Model_orders->get($order_id);
		if(!$this->session->userdata('user_id') && $this->session->userdata('login') == false)
		{
			$user = $this->Model_registered_users->getSingleRow(array('id'=>$order->user_id));
			$user_id = $user->id;
		}
		else
		{
			$user_id = $this->session->userdata('user_id');
		}
		
		$temp_ord_u_id = get_cookie('u_id');
		
		$data['user'] = $user = $this->Model_cart_user_address->get($order->address_id);
		
		$data['order_detail'] = $order;
		
		$get_by['order_id'] = $order_id;



        /*aramex tracking id*/


        $trackId = false;
        $trackId = getTrackingId($order->id);
        if(!$trackId) {
            $convertToSar = round($order->total_amount / $order->currency_rate, 2);
            $COD = round($order->extra_charges / $order->currency_rate, 2);
            $payment_type = strtolower($order->payment_method);

            $tracking = getAramexTrackingId($convertToSar, $COD, $order, $user->id, $payment_type);
            if($tracking) {
                $trackId = $tracking->Shipments->ProcessedShipment->ID;
                $aramexPdf = $tracking->Shipments->ProcessedShipment->ShipmentLabel->LabelURL;
                $tracking_detail['tracking_id'] = $trackId;
                $tracking_detail['aramex_invoice'] = $aramexPdf;
            }
        }
        if($trackId){
            saveTrackingId($order->id,$tracking_detail);
        }


        /*end tracking id*/

        // send sms as well
        $sent =  send_sms($order_id);
        $this->send_new_email($order_id);

        if($this->session->userdata('user_id') &&
            $this->session->userdata('login') == true) {
            $this->send_loyalty_email($data['total_amount']);
        }

        $this->send_admin_email($order_id);



        $data['carts'] = $this->Model_order_product->getMultipleRows($get_by,true);
	
        $products = array();
        foreach ($data['carts'] as $cart){
            $getBy['id'] = $cart['product_id'];
            $prods = $this->Model_product->getSingleRow($getBy,true);
            $products[] = $prods;
        }
        $totalQuan = 0;
        foreach ($products as $key => $product) {
            $totalQuan += $data['carts'][$key]['quantity'];
        }
		$data['shipmentGroup'] = $this->Model_shipment_groups->get($order->shipment_group, true);
        $data['totalQuantity'] = $totalQuan;
        $data['products'] = $products;
        $data['shopping_address'] = '';
        $data['shipping_method'] = '';
		
		$data['order_id'] = str_pad($order_id, 6, "0", STR_PAD_LEFT);

		$data['ord_id'] = $order_id;
        $data['content'] = 'cart/order_placed';
        $data['class'] = 'order_placed';
        $this->load->view('default',$data);
    }
	
    public function paytab(){
		$fields = $this->input->post();
		$html='';
		foreach($fields as $key=>$val)
		{
			$html .= $key.': '.$val.'<br />';
		}
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
		mail('usmanmazhar@astutesol.com', 'Test Paytabs', $html, $headers);
		die();
	}

    public function payTabIPN()
    {
        $lang = $this->session->userdata('site_lang');

        $response_from_api = $this->input->post();

        if (isset($response_from_api['first_4_digits']) && $response_from_api['first_4_digits'] > 0 && $response_from_api['card_brand'] != 'Unknown') { // this is a case of credit card

            if (isset($response_from_api['response_code'])) {

                //$res = explode('-', $response_from_api['order_id']);
                $booking_id = $response_from_api['order_id'];

                $user_mobile_no = $response_from_api['customer_phone'];
                if ($response_from_api['response_code'] == 100 || $response_from_api['response_code'] == 5002) {

                    $booking_cc_save['status'] = 'completed';
                    $booking_cc_save['order_id'] = $booking_id;
                    $booking_cc_save['transaction_id'] = $response_from_api['transaction_id'];
                    $booking_cc_save['first_4_digits'] = $response_from_api['first_4_digits'];
                    $booking_cc_save['last_4_digits'] = $response_from_api['last_4_digits'];
                    $booking_cc_save['card_brand'] = $response_from_api['card_brand'];
                    $booking_cc_save['trans_date'] = date('Y-m-d H:i:s', strtotime($response_from_api['datetime']));

                    $bookingId['order_id'] = $booking_id;
                    $result = $this->Model_booking_cc_payment->getSingleRow($bookingId);


                    $booking_cc_save = $this->currencyDetail($booking_cc_save);

                    if($result){
                        $this->Model_booking_cc_payment->update($booking_cc_save,$bookingId);
                    }else{
                        $this->Model_booking_cc_payment->save($booking_cc_save);
                    }


                }
            }

        } else {
            // this is a case of sadad
            if ($response_from_api['response_code'] == 5001) {

                $booking_id = $response_from_api['order_id'];
                $user_mobile_no = $response_from_api['customer_phone'];
                $booking_sadad_save['order_id'] = $booking_id;
                $booking_sadad_save['s_status'] = 'completed';
                $booking_sadad_save['s_transaction_id'] = $response_from_api['transaction_id'];
                $booking_sadad_save['s_invoice_id'] = $response_from_api['invoice_id'];
                $booking_sadad_save['s_trans_date'] = date('Y-m-d H:i:s', strtotime($response_from_api['datetime']));

                $bookingId['order_id'] = $booking_id;
                $result = $this->Model_booking_sadad_payment->getSingleRow($bookingId);

                $booking_sadad_save = $this->currencyDetail($booking_sadad_save);

                if($result) {
                    $this->Model_booking_sadad_payment->update($booking_sadad_save,$bookingId);
                }else{
                    $this->Model_booking_sadad_payment->save($booking_sadad_save);
                }

            }
        }

        $update['payment_order_status'] = '1';
        $updateBy['id'] = $booking_id;
        $this->Model_orders->update($update,$updateBy);

    }

	public function sendOrderEmail()
	{
		$order_id = $this->input->post('order_id');
		$email = $this->input->post('email');
        $data = $this->lang->line('all');
        $data['lang'] = $this->session->userdata('site_lang');
      	$order = $this->Model_orders->get($order_id);
        if(intval($order->total_amount) > 0) {
            if (!$this->session->userdata('user_id') && $this->session->userdata('login') == false) {
                $user = $this->Model_registered_users->getSingleRow(array('id' => $order->user_id));
                $user_id = $user->id;
            } else {
                $user_id = $this->session->userdata('user_id');
                $data['reg_user'] = $this->Model_registered_users->getSingleRow(array('id' => $user_id));
                //$email = $data['reg_user']->email;
            }

            $temp_ord_u_id = get_cookie('u_id');


            $data['user'] = $this->Model_cart_user_address->get($order->address_id);

            if ($email == '') {
                //$email = $data['user']->email;
            }

            $data['order_detail'] = $order;

            $get_by['order_id'] = $order_id;

            $data['carts'] = $this->Model_order_product->getMultipleRows($get_by, true);

            $products = array();
            foreach ($data['carts'] as $cart) {
                $getBy['id'] = $cart['product_id'];
                $prods = $this->Model_product->getSingleRow($getBy, true);
                $products[] = $prods;
            }
            $totalQuan = 0;
            foreach ($products as $key => $product) {
                $totalQuan += $data['carts'][$key]['quantity'];
            }
            $data['shipmentGroup'] = $this->Model_shipment_groups->get($order->shipment_group, true);
            $data['totalQuantity'] = $totalQuan;
            $data['products'] = $products;
            $data['shopping_address'] = '';
            $data['shipping_method'] = '';

            $data['order_id'] = str_pad($order_id, 6, "0", STR_PAD_LEFT);

            $data['ord_id'] = $order_id;

            $data['type'] = 'thirdPerson';

            if ($data['lang'] == "eng") {
                $view = $this->load->view('layouts/emails/eng_order', $data, TRUE);
            } else {
                $view = $this->load->view('layouts/emails/arb_order', $data, TRUE);
            }
            $body = $view;

            $subject = $data['my_order_placed_successfully'] . ":";
            /*$headers  = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
            $headers .= $data['register_user_from'].':<IOUDStore>' . "\r\n";*/
            if (htmlmail($email, $subject, $body, 'info@ioud.ed.sa')) {
                //return true;
            }

            $response['sent'] = true;
            echo json_encode($response);
            exit;
        }
 
     }
	
	public function send_new_email($order_id)
	{
        $data = $this->lang->line('all');
        $data['lang'] = $this->session->userdata('site_lang');
      	$order = $this->Model_orders->get($order_id);

        if(intval($order->total_amount) > 0) {
            if (!$this->session->userdata('user_id') && $this->session->userdata('login') == false) {
                $user = $this->Model_registered_users->getSingleRow(array('id' => $order->user_id));
                $user_id = $user->id;
            } else {
                $user_id = $this->session->userdata('user_id');
                $data['reg_user'] = $this->Model_registered_users->getSingleRow(array('id' => $user_id));
                $email = $data['reg_user']->email;
            }

            $temp_ord_u_id = get_cookie('u_id');


            $data['user'] = $this->Model_cart_user_address->get($order->address_id);

            if ($email == '') {
                $email = $data['user']->email;
            }

            $data['order_detail'] = $order;

            $get_by['order_id'] = $order_id;

            $data['carts'] = $this->Model_order_product->getMultipleRows($get_by, true);

            $products = array();
            foreach ($data['carts'] as $cart) {
                $getBy['id'] = $cart['product_id'];
                $prods = $this->Model_product->getSingleRow($getBy, true);
                $products[] = $prods;
            }
            $totalQuan = 0;
            foreach ($products as $key => $product) {
                $totalQuan += $data['carts'][$key]['quantity'];
            }
            $data['shipmentGroup'] = $this->Model_shipment_groups->get($order->shipment_group, true);
            $data['totalQuantity'] = $totalQuan;
            $data['products'] = $products;
            $data['shopping_address'] = '';
            $data['shipping_method'] = '';

            $data['order_id'] = str_pad($order_id, 6, "0", STR_PAD_LEFT);

            $data['ord_id'] = $order_id;

            $data['type'] = 'customer';

            if ($data['lang'] == "eng") {
                $view = $this->load->view('layouts/emails/eng_order', $data, TRUE);
            } else {
                $view = $this->load->view('layouts/emails/arb_order', $data, TRUE);
            }

            $body = $view;

            $subject = $data['my_order_placed_successfully'] . ":";
            /*$headers  = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
            $headers .= $data['register_user_from'].':<IOUDStore>' . "\r\n";*/
            $sent = htmlmail($email, $subject, $body, 'info@ioud.ed.sa');

            if ($sent) {
                return true;
            } else {

                return false;
            }
        }
     }

     public function send_loyalty_email($total){

         $ifcDiscount = getCouponPrice($total);
         $loyalty_discount = loyaltyDiscount($total,$ifcDiscount);

         if(intval($loyalty_discount['rate']) != 0) {

             $user_id = $this->session->userdata('user_id');
             $reg_user = $this->Model_registered_users->getSingleRow(array('id' => $user_id));
             $email = $reg_user->email;

             $lang = $this->session->userdata('site_lang');
             $discount = get_cookie('discount_type');
             if ($lang == 'eng') {
                 $title = 'Meduaf program';
                 $message = 'Hi <br> You have ' . $discount . ' discount this time and every time ';
             } else {
                 $title = 'برنامج مضياف';
                 $message = 'اهلا<br>
طالع عمرك صار عندك خصم ' . $discount . ' على مشترياتك هالمرة وكل مرة
';
             }
             $data['title'] = $title;
             $data['message'] = $message;

             if ($lang == "eng") {
                 $view = $this->load->view('layouts/emails/eng_general_email', $data, TRUE);
             } else {
                 $view = $this->load->view('layouts/emails/arb_general_email', $data, TRUE);
             }

             $body = $view;

             $subject = $title;
             /*$headers  = "MIME-Version: 1.0" . "\r\n";
             $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
             $headers .= $data['msg']['register_user_from'].':<IOUDStore>' . "\r\n";*/
             if (htmlmail($email, $subject, $body)) {
                 return true;
             }
         }
     }
	 
    private function send_admin_email($order_id){
		
		$data = $this->lang->line('all');
        $data['lang'] = $this->session->userdata('site_lang');
		
      	$order = $this->Model_orders->get($order_id);

      	if(intval($order->total_amount) > 0) {
            if (!$this->session->userdata('user_id') && $this->session->userdata('login') == false) {
                $user = $this->Model_registered_users->getSingleRow(array('id' => $order->user_id));
                $user_id = $user->id;
            } else {
                $user_id = $this->session->userdata('user_id');
                $data['reg_user'] = $this->Model_registered_users->getSingleRow(array('id' => $user_id));
            }

            $temp_ord_u_id = get_cookie('u_id');

            $data['user'] = $this->Model_cart_user_address->get($order->address_id);

            $data['order_detail'] = $order;

            $get_by['order_id'] = $order_id;

            $data['carts'] = $this->Model_order_product->getMultipleRows($get_by, true);

            $products = array();
            foreach ($data['carts'] as $cart) {
                $getBy['id'] = $cart['product_id'];
                $prods = $this->Model_product->getSingleRow($getBy, true);
                $products[] = $prods;
            }
            $totalQuan = 0;
            foreach ($products as $key => $product) {
                $totalQuan += $data['carts'][$key]['quantity'];
            }
            $data['shipmentGroup'] = $this->Model_shipment_groups->get($order->shipment_group, true);
            $data['totalQuantity'] = $totalQuan;
            $data['products'] = $products;
            $data['shopping_address'] = $shopping_address;
            $data['shipping_method'] = $shipping_method;

            $data['order_id'] = str_pad($order_id, 6, "0", STR_PAD_LEFT);

            $data['ord_id'] = $order_id;

            $data['type'] = 'admin';

            $admin_email = $this->Model_settings->get('1');

            if ($data['lang'] == "eng") {
                $view = $this->load->view('layouts/emails/eng_order', $data, TRUE);
            } else {
                $view = $this->load->view('layouts/emails/arb_order', $data, TRUE);
            }
            $body = $view;

            $subject = $data['my_order_placed_successfully'] . ":";
            /*$headers  = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
            $headers .= $data['register_user_from'].':<IOUDStore>' . "\r\n";*/
            if (htmlmail($admin_email->admin_email, $subject, $body, 'info@ioud.ed.sa')) {

                return true;
            } else {
                return false;
            }
        }
	 }
}
