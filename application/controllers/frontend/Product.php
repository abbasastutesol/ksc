<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Product extends CI_Controller {





	public function __construct()

    {

        parent::__construct();

		

        $get_url_arr = explode('/', $_SERVER['REQUEST_URI']);

		if(!$this->session->userdata('site_lang'))

		{

			$this->session->set_userdata('site_lang','arb');

		}

		if(in_array('en', $get_url_arr))

		{

			$this->session->set_userdata('site_lang','eng');

		}

		else

		{

			$this->session->set_userdata('site_lang','arb');

		}

		$this->lang->load("message",$this->session->userdata('site_lang'));

	    $this->load->model('Model_product');

	    $this->load->model('Model_category');

	    $this->load->model('Model_variant');

	    $this->load->model('Model_product_variant_value');

	    $this->load->model('Model_product_variant_group');

		$this->load->model('Model_image');

		$this->load->model('Model_variant_value');

		$this->load->model('Model_related_product');

		$this->load->model('Model_temp_orders');

		$this->load->model('Model_customer_questions');		

		

    }

	

	public function index()

	{

		$data = array();

		$data = $this->lang->line('all');

		$data['lang'] = $this->session->userdata('site_lang');

        $prod_all['active'] = 1;
		$prod_all['category_status'] = 1;

        if(isset($_GET['cat'])) {

            $prod_all['category_name'] = $_GET['cat'];
        }

        $data['products'] = $this->Model_product->getProductWithCategory($prod_all, true);


        $data['content'] = 'product/products';

		$data['class'] = 'product';

		$this->load->view('default',$data);	

	}
    
	public function product_page($id)

	{
	    // here save the users views against each product
        saveProductViewedStatus($id);
        // end viewed saved functionality

		$data = array();
		$data = $this->lang->line('all');

		$fetch_by = array();

        $data['lang'] = $this->session->userdata('site_lang');

		$fetch_by['id'] = $id;

        $data['product'] = $this->Model_product->getSingleRow($fetch_by,true);

        $prod_all['active'] = 1;

        $prod_all['category_id'] = $data['product']['category_id'];

        
        $data['related_products'] = $this->Model_product->getProductWithCategory($prod_all, true);

        $data['content'] = 'product/product_page';

		$data['class'] = 'product_page';



		$this->load->view('default',$data);	

		

	}

	

	

	public function dailyOffers()

	{	

		$data = array();

		$data = $this->lang->line('all');

		$data['lang'] = $this->session->userdata('site_lang');

		$prod_all['daily_offer'] = 1;

		$prod_all['active'] = 1;

		$data['products'] = $this->Model_product->getMultipleRows($prod_all,true);

		$data['content'] = 'pages/daily_offers';

		$data['class'] = 'daily_offers';

		$this->load->view('default',$data);

	}

	

	

	

	public function search() {

        $search = $this->input->get('search');
		$data = array();

		$data = $this->lang->line('all');

		$data['lang'] = $lang = $this->session->userdata('site_lang');

		
		$data['products'] = $this->Model_product->searchProduct($search,$data['lang']);
		//echo "<pre>test-"; print_r($data['products']); exit;

		//$prod_all['active'] = 1;
		//$prod_all[$lang.'_name'] = $search;

		//$data['products'] = $this->Model_product->getMultipleRows($prod_all, true);

		$data['content'] = 'product/products';

		$data['class'] = 'search';

		$this->load->view('default',$data);	

		

	}
	
	

}

