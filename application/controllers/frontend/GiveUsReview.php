<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GiveUsReview extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
        parent::__construct();
		
		$this->load->helper('url');
		$get_url_arr = explode('/', $_SERVER['REQUEST_URI']);
		if(!$this->session->userdata('site_lang'))
		{
			$this->session->set_userdata('site_lang','arb');
		}
        $this->lang->load("message",$this->session->userdata('site_lang'));
	    $this->load->model('Model_review');
		$this->load->library('File_upload');
		//$res = checkLevels(2);
		//checkAuth($res);
    }
	
	public function index()
	{	
		$data = array();
		$data = $this->lang->line('all');
		$data['lang'] = $this->session->userdata('site_lang');
		$data['content'] = 'review/your_review';
		$data['class'] = 'your_review';
		$this->load->view('default',$data);	
		
	}
	
	public function submit()
	{
		$data = array();
		$update_by = array();
		$message = $this->lang->line('all');
		$post_data = $this->input->post();
		$lang = $this->session->userdata('site_lang');
		$config = getCconfiguration();
		
		
		foreach($post_data as $key => $value)
		{
			$data[$key] = $value;	
		}
		
		
				
		$inserted_id = $this->Model_review->save($data);
		// end images save section
		if($lang == 'arb')
		{
			$body_style = 'style="direction:rtl"';
			$left = 'right';
			$right = 'left';
			$td_phone_style = ' unicode-bidi: plaintext;';
			
		}
		else
		{
			$body_style = '';
			$left = 'left';
			$right = 'right';
			$td_phone_style = '';
		}
		$html .= '
				<html>
				<head>
					<style type="text/css">
						body {
							color: #777;
							padding:0;
							margin: 0;
						}
						body, table, td, p, a, li, blockquote {
							-webkit-text-size-adjust: none !important;
							font-style: normal;
							font-weight: 400;
							font-family: \'Roboto\', sans-serif;
						}
						@import \'https://fonts.googleapis.com/css?family=Roboto\';
						@import \'url(http://fonts.googleapis.com/earlyaccess/notosanskannada.css)\';
						button {
							width: 90%;
						}
						p {
							font-size: 13px !important;
							color: #777 !important;
							line-height: normal !important;
							padding-bottom: 14px !important;
							margin: 0px !important;
						}
						@media screen and (max-width:600px) {
						/*styling for objects with screen size less than 600px; */
						body, table, td, p, a, li, blockquote {
							-webkit-text-size-adjust: none!important;
						}
						table {
							/* All tables are 100% width */
							width: 100%;
						}
						.footer {
							/* Footer has 2 columns each of 48% width */
							height: auto !important;
							max-width: 48% !important;
							width: 48% !important;
						}
						table.responsiveImage {
							/* Container for images in catalog */
							height: auto !important;
							max-width: 30% !important;
							width: 30% !important;
						}
						table.responsiveContent {
							/* Content that accompanies the content in the catalog */
							height: auto !important;
							max-width: 66% !important;
							width: 66% !important;
						}
						.top {
							/* Each Columnar table in the header */
							height: auto !important;
							max-width: 48% !important;
							width: 48% !important;
						}
						.catalog {
							margin-left: 0%!important;
						}
						}
						
						@media screen and (max-width:480px) {
						/*styling for objects with screen size less than 480px; */
						body, table, td, p, a, li, blockquote {
						}
						table {
							/* All tables are 100% width */
							width: 100% !important;
							border-style: none !important;
						}
						.footer {
							/* Each footer column in this case should occupy 96% width  and 4% is allowed for email client padding*/
							height: auto !important;
							max-width: 96% !important;
							width: 96% !important;
						}
						.table.responsiveImage {
							/* Container for each image now specifying full width */
							height: auto !important;
							max-width: 96% !important;
							width: 96% !important;
						}
						.table.responsiveContent {
							/* Content in catalog  occupying full width of cell */
							height: auto !important;
							max-width: 96% !important;
							width: 96% !important;
						}
						.top {
							/* Header columns occupying full width */
							height: auto !important;
							max-width: 100% !important;
							width: 100% !important;
						}
						.catalog {
							margin-left: 0%!important;
						}
						button {
							width: 90%!important;
						}
						}
						table a {color:#d80000 !important;text-decoration:none !important;}
						</style>
						</head>
						<body '.$body_style.'>
						<table width="100%" cellspacing="0" cellpadding="0">
						  <tbody>
							<tr>
							  <td>
								<table width="500"  align="center" cellpadding="0" cellspacing="0" style="background:url(images/header-top.jpg) repeat-x top left;">
								  <!-- Main Wrapper Table with initial width set to 60opx -->
								  <tbody>
									<tr>
									  <td>
											<table class="top" width="100%"  align="left" cellpadding="0" cellspacing="0" style="padding:35px 0 53px;">
											  <!-- First header column with Logo -->
											  <tbody>
												<tr>
												  <td align="center" valign="middle"><a href="'.base_url().'" target="_blank"><img src="'.base_url().'assets/frontend/images/logo.png" alt="eDesign"></a></td>
												</tr>
											  </tbody>
										  </table>
						
									  </td>
									</tr>
									<tr>
									  <td>
											<table width="100%" align="left"  cellpadding="0" cellspacing="0" >
										  <tr>
											<td align="center" valign="middle" style="font-size:13px; font-weight: 500; padding-bottom: 22px;">'.$message['email_template_hi'].' {hi_name},</td>
										  </tr>
										  <tr>
											<td align="center" valign="middle" style="font-family: \'Roboto\', sans-serif; font-weight: 100; font-size: 22px; color: #E00513; padding-bottom: 16px;">
											</td>
										  </tr>
										</table>
									  </td>
									</tr>
									<tr>
										<td>
											<table width="400" cellpadding="0" cellspacing="0" border="1" style="border:0px; border-bottom: 1px solid rgba(119, 119, 119, 0.51); margin:  0 auto 30px; padding: 35px 0 0;">
												<tr>
													<td align="'.$left.'" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; "><strong>'.$message['chat_name'].'</strong></td>
													<td align="'.$right.'" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-'.$left.':0; padding: 10px 5px;font-size:12px;font-weight: normal; color:#D80000; text-transform:uppercase; ">'.$data['name'].'</td>
												</tr>
												<tr>
													<td align="'.$left.'" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; "><strong>'.$message['register_email'].'</strong></td>
													<td align="'.$right.'" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-'.$left.':0; padding: 10px 5px;font-size:12px;font-weight: normal; color:#D80000; text-transform:uppercase; ">'.$data['email'].'</td>
												</tr>
												<tr>
													<td align="'.$left.'" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; "><strong>'.$message['register_mobile_number'].'</strong></td>
													<td align="'.$right.'" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-'.$left.':0; padding: 10px 5px;font-size:12px;font-weight: normal; color:#D80000; text-transform:uppercase; ">'.$data['mobile_no'].'</td>
												</tr>
												<tr>
													<td align="'.$left.'" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; "><strong>'.$message['chat_message'].'</strong></td>
													<td align="'.$right.'" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-'.$left.':0; padding: 10px 5px;font-size:12px;font-weight: normal; color:#D80000; text-transform:uppercase; ">'.$data['message'].'</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td align="center" valign="middle" style="font-size:13px; padding: 46px 0 18px;">'.$message['email_template_footer_need_more_help'].'</td>
									</tr>
									<tr>
										<td align="center" valign="middle" style="font-size:11px; padding: 0px 0 15px;">'.$message['email_template_footer_service_team'].' <span style="'.$td_phone_style.'"> '.$config->phone.' </span> '.$message['email_template_or'].'<br>'.$message['chat_email'].' '.$config->email.'</td>
									</tr>
									<tr>
										<td align="center" valign="middle" style="font-size:11px; padding: 0px 0 50px;">'.$message['email_template_thank_you_for_shopping'].'</td>
									</tr>
								  </tbody>
								  <tfoot bgcolor="#333">
									<tr>
										<td colspan="2">
											<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-top: 1px solid #f1f1f1; padding: 20px 55px 46px;">
												<tr>
													<td align="center">
														<span style="font-size:11px; color:#fff;">'.$message['login_follow_us'].'</span>
														<a href="'.$config->facebook_link.'" target="_blank" style="display:inline-block; margin-right:14px;"><img src="'.base_url().'assets/frontend/images/facebook-icon.png" alt=""></a>
														<a href="'.$config->twitter_link.'" target="_blank" style="display:inline-block; margin-right:14px;"><img src="'.base_url().'assets/frontend/images/twitter-icon.png" alt=""></a>
														<a href="'.$config->instagram_link.'" target="_blank" style="display:inline-block; margin-right:14px;"><img src="'.base_url().'assets/frontend/images/instagram-icon.png" alt=""></a>
														<a href="'.$config->snapchat_link.'" target="_blank" style="display:inline-block; margin-right:14px;"><img src="'.base_url().'assets/frontend/images/snapchat-icon.png" alt=""></a>
														<a href="'.$config->youtube_link.'" target="_blank" style="display:inline-block; margin-right:16px;"><img src="'.base_url().'assets/frontend/images/youtube-icon.png" alt=""></a>
														
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td>
											<table width="100%" cellpadding="0" cellspacing="0" border="0" style="padding: 0px 55px 24px;">
												<tr>
													<td align="'.$left.'" valign="middle" style="font-family: \'Noto Sans Kannada\', sans-serif; font-size:11px; color: #fff;">'.($lang == 'arb' ? 'جميع الحقوق محفوظة I eDesign '.date('Y').'' : 'All rights reserved '.date('Y').' | eDesign').'</td>
													<td align="'.$right.'" valign="middle"><a href="'.base_url().'" target="_balnk" style="font-size:11px; color: #ffffff; text-decoration: none;">'.$message['design_visit_our_store'].'</a></td>
												</tr>
											</table>
										</td>
									</tr>
								  </tfoot>
								</table>
								</td>
							</tr>
						  </tbody>
						</table>
				</body>
				</html>';
				
				//$admin = str_replace('{email_type}',$message['design_email'],$html);
				$admin = str_replace('{hi_name}', 'Admin', $html);
				htmlmail($config->admin_email, $message['give_us_review_email_subject'], $admin, 'developer@edesign.com.sa');
		
		
		$response['success'] = '1';
		$response['message'] = $message['give_us_your_review'];
		echo json_encode($response);
		exit;
	}
}
