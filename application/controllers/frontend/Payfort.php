<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payfort extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
        parent::__construct();
		$this->lang->load("message",$this->session->userdata('site_lang'));
		$this->load->library('PayfortIntegration');
		$this->load->library('PayfortCheckout');
		$this->load->library('PayfortSettings');
		$this->load->model('Model_registered_users');
		$this->load->model('Model_registered_user_cards');
		//$res = checkLevels(2);
		//checkAuth($res);
    }

	public function payfortAuth()
	{
		/**
		 * @copyright Copyright PayFort 2012-2016
		 */
		$data['lang'] = $this->session->userdata('site_lang');

		$settings = getSettings();
		
		
		if(!isset($_REQUEST['r'])) {
			echo 'Page Not Found!';
			exit;
		}
		$objFort = new PayfortCheckout();
		if ($_GET['r'] == 'getPaymentPage') {
			$this->session->set_userdata(array('command' => $_REQUEST['command'], 'retURL' => $_REQUEST['retURL'], 'customer_name' => $_REQUEST['customer_name']));
			if($data['lang'] == 'eng')
			{
				$objFort->language = 'en';
			}
			else
			{
				$objFort->language = 'ar';
			}
			
			$cookie_array['customer_name'] = $_REQUEST['customer_name'];
			$cookie_array['customerEmail'] = $_REQUEST['customerEmail'];
			$cookie_array['amount'] = $_REQUEST['amount'];
			
			$cookie = array(
				'name'   => 'customer_det',
				'value'  => serialize($cookie_array),
				'expire' => time()+(10 * 365 * 24 * 60 * 60)
			);
			$this->input->set_cookie($cookie);

			$objFort->merchantIdentifier = $settings->payfort_merchant_identifier;
			$objFort->accessCode = $settings->payfort_access_code;
			$objFort->shaIn = $settings->payfort_sha_in;
			$objFort->shaOut = $settings->payfort_sha_out;
			$objFort->sha256 = $settings->payfort_hash_algorith;
			
			if($_REQUEST['paymentMethod'] == 'cc_merchantpage')
			{
				$objFort->command = 'AUTHORIZATION';
			}
			elseif($_REQUEST['paymentMethod'] == 'sadad')
			{
				$objFort->command = 'PURCHASE';
			}
			$objFort->amount = $_REQUEST['amount'];
			$objFort->currency = getCurrency();			
			
			$objFort->customer_name = $_REQUEST['customer_name'];
			$objFort->customer_email = $_REQUEST['customerEmail'];
			
			$objFort->processRequest($_REQUEST['paymentMethod']);
		} elseif ($_GET['r'] == 'merchantPageReturn') {
			if($data['lang'] == 'eng')
			{
				$objFort->language = 'en';
			}
			else
			{
				$objFort->language = 'ar';
			}
			
			$check_array = unserialize(get_cookie('customer_det'));
			
			$objFort->command = 'AUTHORIZATION';
			$objFort->amount = $check_array['amount'];
			$objFort->currency = getCurrency();
			
			$objFort->customer_name = $check_array['customer_name'];
			$objFort->customer_email = $check_array['customerEmail'];
			
			//$objFort->retURL = lang_base_url().'Payfort/payfortRetUrl';
			$objFort->processMerchantPageResponse();
		} elseif ($_GET['r'] == 'processResponse') {
			if($data['lang'] == 'eng')
			{
				$objFort->language = 'en';
			}
			else
			{
				$objFort->language = 'ar';
			}
			
			$objFort->command = 'AUTHORIZATION';
			$objFort->currency = getCurrency();	
			
			$objFort->customer_name = $_REQUEST['customer_name'];
			$objFort->amount = $_REQUEST['amount'];//$this->session->userdata('carAmount');
			$objFort->customer_name = $_REQUEST['customerEmail'];
			if($data['lang'] == 'eng')
			{
				$objFort->language = 'en';
			}
			else
			{
				$objFort->language = 'ar';
			}
			//$objFort->retURL = lang_base_url().'Payfort/payfortRetUrl';
			$objFort->processResponse();
		} else {
			echo 'Page Not Found!';
			exit;
		}

	}

	public function payfortAuthSuccess()
	{
		
		$data = array();
		$card_data = array();
		$data = $this->lang->line('all');
		$data['lang'] = $this->session->userdata('site_lang');
		$regUserId = $this->session->userdata('regUserId');
		if(!$this->session->userdata('user_id') && $this->session->userdata('login') == false)
		{
			$user = $this->Model_registered_users->getSingleRow(array('cookie_id'=>get_cookie('u_id')));
			$user_id = $user->id;
			$temp_ord_u_id = get_cookie('u_id');
		}
		else
		{
			$user_id = $this->session->userdata('user_id');
			$temp_ord_u_id = $this->session->userdata('user_id');
		}
		if($_REQUEST['payment_option'] == 'SADAD')
		{
			$payment_method = 'sadad';
		}
		else
		{
			$payment_method = 'cc_merchantpage';
		}
		$data['user_id'] = $user_id;
		$card_data['customer_name'] = $_REQUEST['customer_name'];
		$card_data['customer_email'] = $_REQUEST['customer_email'];
		$card_data['card_number'] = $_REQUEST['card_number'];
		$card_data['token_name'] = $_REQUEST['token_name'];
		$card_data['payment_option'] = $_REQUEST['payment_option'];
		$card_data['expiry_date'] = $_REQUEST['expiry_date'];		
		$card_data['user_id'] = $user_id;
		$response_message = $_REQUEST['response_message'];
		$this->session->set_flashdata('message', $response_message);
		$insert_id = $this->Model_registered_user_cards->save($card_data);
		delete_cookie("payment_method");
		$cookie = array(
			'name'   => 'user_card_id',
			'value'  => $insert_id,
			'expire' => time()+(10 * 365 * 24 * 60 * 60)
		);
		$this->input->set_cookie($cookie);
		$cookie = array(
			'name'   => 'payment_method',
			'value'  => $payment_method,
			'expire' => time()+(10 * 365 * 24 * 60 * 60)
		);
		$this->input->set_cookie($cookie);
		//$data['paymentMethod'] = 'cc_merchantpage';
		$data['card_data_id'] = $insert_id;
		$data['payfortError'] = false;
		$lang = $this->session->userdata('site_lang');
		if($data['lang'] == 'eng')
		{
			redirect($this->config->item('base_url') . 'en/shopping_cart/orderView');
		}else{
			redirect($this->config->item('base_url') . 'shopping_cart/orderView');
		}
		
	}

	public function payfortAuthError()
	{
		
		$data = array();
		$fetch_by = array();
		$data = $this->lang->line('all');
		$data['lang'] = $this->session->userdata('site_lang');
		$regUserId = $this->session->userdata('regUserId');
		$fetch_by['id'] = $regUserId;
		$response_message = $_REQUEST['response_message'];
		$this->session->set_flashdata('message', $response_message);
		$user_data = $this->Model_registered_users->get($fetch_by['id']);
		//$data['paymentMethod'] = 'cc_merchantpage';
		$data['payfortError'] = true;
		$data['user_id'] = $regUserId;
		$data['user_data'] = $user_data;
		if($data['lang'] == 'eng')
		{
			redirect($this->config->item('base_url') . 'en/shopping_cart');
		}else{
			redirect($this->config->item('base_url') . 'shopping_cart');
		}	}

	public function payfortCheckout()
	{
		/**
		 * @copyright Copyright PayFort 2012-2016
		 */
		$data['lang'] = $this->session->userdata('site_lang');

		$settings = getSettings();

		if(!isset($_REQUEST['r'])) {
			echo 'Page Not Found!';
			exit;
		}
		$objFort = new PayfortCheckout();
		if ($_GET['r'] == 'getPaymentPage') {
			if($data['lang'] == 'eng')
			{
				$objFort->language = 'en';
			}
			else
			{
				$objFort->language = 'ar';
			}
			$objFort->merchantIdentifier = $settings->payfort_merchant_identifier;
			$objFort->accessCode = $settings->payfort_access_code;
			$objFort->shaIn = $settings->payfort_sha_in;
			$objFort->shaOut = $settings->payfort_sha_out;
			$objFort->sha256 = $settings->payfort_hash_algorith;

			$objFort->command = 'PURCHASE';
			$objFort->amount = $_REQUEST['amount'];
			$objFort->currency = getCurrency();

			$objFort->customer_name = $_POST['customer_name'];
			$objFort->customer_email = $_POST['customerEmail'];
			$objFort->item_name = 'test';
			//$objFort->retURL = lang_base_url().'Payfort/payfortRetUrl';

			$objFort->processRequest($_REQUEST['paymentMethod']);
		} elseif ($_GET['r'] == 'merchantPageReturn') {
			$objFort->command = 'PURCHASE';
			$objFort->amount = 10;
			$objFort->currency = getCurrency();

			if($data['lang'] == 'eng')
			{
				$objFort->language = 'en';
			}
			else
			{
				$objFort->language = 'ar';
			}
			$objFort->customer_name = $_POST['customer_name'];
			$objFort->customer_email = $_POST['customerEmail'];
			$objFort->item_name = 'test';
			//$objFort->retURL = lang_base_url().'Payfort/payfortRetUrl';
			$objFort->processMerchantPageResponse();
		} elseif ($_GET['r'] == 'processResponse') {
			$objFort->command = 'PURCHASE';
			$objFort->amount = 10;
			$objFort->currency = getCurrency();

			if($data['lang'] == 'eng')
			{
				$objFort->language = 'en';
			}
			else
			{
				$objFort->language = 'ar';
			}
			$objFort->customer_name = $_POST['customer_name'];
			$objFort->customer_email = $_POST['customerEmail'];
			$objFort->item_name = 'test';
			//$objFort->retURL = lang_base_url().'Payfort/payfortRetUrl';
			$objFort->processResponse();
		} else {
			echo 'Page Not Found!';
			exit;
		}

	}

	public function payfortCheckoutSuccess()
	{
		
		$data = array();
		$fetch_by = array();
		$card_data = array();
		$card_changed = 0;
		$data = $this->lang->line('all');
		$data['lang'] = $this->session->userdata('site_lang');
		/*if ($this->session->userdata('user_id') != '')
		{*/
		if(!$this->session->userdata('user_id') && $this->session->userdata('login') == false)
		{
			$user = $this->Model_registered_users->getSingleRow(array('cookie_id'=>get_cookie('u_id')));
			$user_id = $user->id;
			$temp_ord_u_id = get_cookie('u_id');
		}
		else
		{
			$user_id = $this->session->userdata('user_id');
			$temp_ord_u_id = $this->session->userdata('user_id');
		}
		$fetch_by['user_id'] = $user_id;
		$card_changed = 1;
		$user_cards = $this->Model_registered_user_cards->getMultipleRows($fetch_by);
		/*foreach ($user_cards as $card)
		{
			if (($card->card_number != $_REQUEST['card_number']) && ($card->token_name != $_REQUEST['token_name']))
			{
				$card_changed = 1;
			}else{
				$card_changed = 0;
			}
		}*/
		if ($card_changed == 1)
		{
			$card_data['customer_name'] = $_REQUEST['customer_name'];
			$card_data['customer_email'] = $_REQUEST['customer_email'];
			$card_data['card_number'] = $_REQUEST['card_number'];
			$card_data['token_name'] = $_REQUEST['token_name'];
			$card_data['payment_option'] = $_REQUEST['payment_option'];
			$card_data['user_id'] = $user_id;
			$card_data['expiry_date'] = $_REQUEST['expiry_date'];			
			$insert_id = $this->Model_registered_user_cards->save($card_data);
			$cookie = array(
				'name'   => 'user_card_id',
				'value'  => $insert_id,
				'expire' => time()+(10 * 365 * 24 * 60 * 60)
			);
			$this->input->set_cookie($cookie);
			$response_message = $_REQUEST['response_message'];
			$this->session->set_flashdata('message', $response_message);
		}
		/*}*/
		//$data['paymentMethod'] = 'cc_merchantpage';
		$data['card_data_id'] = $insert_id;
		$data['payfortError'] = false;
		$lang = $this->session->userdata('site_lang');
		if($data['lang'] == 'eng')
		{
			redirect($this->config->item('base_url') . 'en/shopping_cart/orderView');
		}else{
			redirect($this->config->item('base_url') . 'shopping_cart/orderView');
		}
	}

	public function payfortCheckoutError()
	{
		echo 'error';
		exit;
		$data = array();
		$fetch_by = array();
		$data = $this->lang->line('all');
		$data['lang'] = $this->session->userdata('site_lang');
		$regUserId = $this->session->userdata('regUserId');
		$fetch_by['id'] = $regUserId;
		$response_message = $_REQUEST['response_message'];
		$this->session->set_flashdata('message', $response_message);
		$user_data = $this->Model_registered_users->get($fetch_by['id']);
		//$data['paymentMethod'] = 'cc_merchantpage';
		$data['payfortError'] = true;
		$data['user_id'] = $regUserId;
		$data['user_data'] = $user_data;
		if($data['lang'] == 'eng')
		{
			redirect($this->config->item('base_url') . 'en/shopping_cart');
		}else{
			redirect($this->config->item('base_url') . 'shopping_cart');
		}
	}
	
	
	public function payfortAuthSetting()
	{
		/**
		 * @copyright Copyright PayFort 2012-2016
		 */
		$data['lang'] = $this->session->userdata('site_lang');

		$settings = getSettings();
		
		$user_data = $this->Model_registered_users->get($this->session->userdata('user_id'));

		if(!isset($_REQUEST['r'])) {
			echo 'Page Not Found!';
			exit;
		}
		$objFort = new PayfortSettings();
		if ($_GET['r'] == 'getPaymentPage') {
			$this->session->set_userdata(array('command' => $_REQUEST['command'], 'retURL' => $_REQUEST['retURL'], 'customer_name' => $_REQUEST['customer_name']));
			if($data['lang'] == 'eng')
			{
				$objFort->language = 'en';
			}
			else
			{
				$objFort->language = 'ar';
			}

			$objFort->merchantIdentifier = $settings->payfort_merchant_identifier;
			$objFort->accessCode = $settings->payfort_access_code;
			$objFort->shaIn = $settings->payfort_sha_in;
			$objFort->shaOut = $settings->payfort_sha_out;
			$objFort->sha256 = $settings->payfort_hash_algorith;
			$objFort->command = 'AUTHORIZATION';
			$objFort->amount = 10;
			$objFort->currency = getCurrency();
			$objFort->processRequest($_REQUEST['paymentMethod']);
		} elseif ($_GET['r'] == 'merchantPageReturn') {
			if($data['lang'] == 'eng')
			{
				$objFort->language = 'en';
			}
			else
			{
				$objFort->language = 'ar';
			}
			$objFort->command = 'AUTHORIZATION';
			$objFort->customerName = $user_data->first_name.' '.$user_data->last_name;
			$objFort->amount = '1';//$this->session->userdata('carAmount');
			$objFort->customerEmail = $user_data->email;
			$objFort->retURL = lang_base_url().'Payfort/payfortRetUrl';
			$objFort->processMerchantPageResponse();
		} elseif ($_GET['r'] == 'processResponse') {
			if($data['lang'] == 'eng')
			{
				$objFort->language = 'en';
			}
			else
			{
				$objFort->language = 'ar';
			}

			$objFort->command = 'AUTHORIZATION';
			$objFort->customerName = $user_data->first_name.' '.$user_data->last_name;
			$objFort->amount = '1';//$this->session->userdata('carAmount');
			$objFort->customerEmail = $user_data->email;
			$objFort->retURL = lang_base_url().'Payfort/payfortRetUrl';
			$objFort->processResponse();
		} else {
			echo 'Page Not Found!';
			exit;
		}

	}

	public function payfortAuthSettingSuccess()
	{
		$data = array();
		$card_data = array();
		$data = $this->lang->line('all');
		$data['lang'] = $this->session->userdata('site_lang');
		$fetch_by['user_id'] = $user_id = $this->session->userdata('user_id');
		$user_cards = $this->Model_registered_user_cards->getMultipleRows($fetch_by);
		$card_changed = 1;
		foreach ($user_cards as $card)
		{
			if (($card->card_number != $_REQUEST['card_number']) && ($card->token_name != $_REQUEST['token_name']))
			{
				$card_changed = 1;
			}else{
				$card_changed = 0;
			}
		}
		if ($card_changed == 1)
		{
			$card_data['customer_name'] = $_REQUEST['customer_name'];
			$card_data['customer_email'] = $_REQUEST['customer_email'];
			$card_data['card_number'] = $_REQUEST['card_number'];
			$card_data['token_name'] = $_REQUEST['token_name'];
			$card_data['payment_option'] = $_REQUEST['payment_option'];
			$card_data['expiry_date'] = $_REQUEST['expiry_date'];
			$card_data['user_id'] = $this->session->userdata('user_id');
			$response_message = $_REQUEST['response_message'];
			$this->session->set_flashdata('message', $response_message);
			$insert_id = $this->Model_registered_user_cards->save($card_data);
			//$data['paymentMethod'] = 'cc_merchantpage';
			$data['card_data_id'] = $insert_id;
		}
		
		$dataType['payment_type'] = 'cc_merchantpage';
		$this->Model_registered_users->update($dataType, array('id'=>$this->session->userdata('user_id')));
			
		$lang = $this->session->userdata('site_lang');
		if($data['lang'] == 'eng')
		{
			redirect($this->config->item('base_url') . 'en/setting');
		}else{
			redirect($this->config->item('base_url') . 'setting');
		}
	}

	public function payfortAuthSettingError()
	{
		$data = array();
		$fetch_by = array();
		$data = $this->lang->line('all');
		$data['lang'] = $this->session->userdata('site_lang');
		$regUserId = $this->session->userdata('regUserId');
		$fetch_by['id'] = $regUserId;
		$response_message = $_REQUEST['response_message'];
		$this->session->set_flashdata('message', $response_message);
		$user_data = $this->Model_registered_users->get($fetch_by['id']);
		$dataType['payment_type'] = 'cc_merchantpage';
		$this->Model_registered_users->update($dataType, array('id'=>$this->session->userdata('user_id')));
		//$data['paymentMethod'] = 'cc_merchantpage';
		$data['payfortError'] = true;
		$data['user_id'] = $regUserId;
		$data['user_data'] = $user_data;
		if($data['lang'] == 'eng')
		{
			redirect($this->config->item('base_url') . 'en/setting');
		}else{
			redirect($this->config->item('base_url') . 'setting');
		}
		/*$data['content'] = 'user/settings';
		$data['class'] = 'setting';
		$this->load->view('default',$data);*/
	}
}

