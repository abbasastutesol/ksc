<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class My_orders extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */



	public function __construct()

    {

        parent::__construct();

		$this->load->helper('url');

		/*if(!$this->session->userdata('login'))

		{

			redirect(lang_base_url());

		}*/

		$get_url_arr = explode('/', $_SERVER['REQUEST_URI']);

		if(!$this->session->userdata('site_lang'))

		{

			$this->session->set_userdata('site_lang','arb');

		}

		if(in_array('en', $get_url_arr))

		{

			$this->session->set_userdata('site_lang','eng');

		}

		else

		{

			$this->session->set_userdata('site_lang','arb');

		}

		$this->lang->load("message",$this->session->userdata('site_lang'));

	    $this->load->model('Model_product');

	    $this->load->model('Model_category');

	    $this->load->model('Model_order_product');

		$this->load->model('Model_orders');

		$this->load->model('Model_shipment_methods');

		$this->load->model('Model_cart_user_address');

		$this->load->model('Model_registered_users');
		
		//$res = checkLevels(2);

		//checkAuth($res);

    }

	public function printPage($order_id){

		$data = $this->lang->line('all');

        $data['lang'] = $this->session->userdata('site_lang');

		

      	$order = $this->Model_orders->get($order_id);
		if(!$this->session->userdata('user_id') && $this->session->userdata('login') == false)
		{
			$user = $this->Model_registered_users->getSingleRow(array('id'=>$order->user_id));
			$user_id = $user->id;
		}
		else
		{
			$user_id = $this->session->userdata('user_id');
			$data['reg_user'] = $this->Model_registered_users->getSingleRow(array('id'=>$user_id));
		}
		
		$temp_ord_u_id = get_cookie('u_id');
		
		$data['user'] = $this->Model_cart_user_address->get($order->address_id);
		
		$data['order_detail'] = $order;
		
		$get_by['order_id'] =$order_id;
		
		$data['carts'] = $this->Model_order_product->getMultipleRows($get_by,true);
	
        $products = array();

        foreach ($data['carts'] as $cart){

            $getBy['id'] = $cart['product_id'];

            $prods = $this->Model_product->getSingleRow($getBy,true);

            $products[] = $prods;

        }



        $totalQuan = 0;

        foreach ($products as $key => $product) {

            $totalQuan += $data['carts'][$key]['quantity'];

        }



        $data['totalQuantity'] = $totalQuan;



        $data['products'] = $products;


        $data['shopping_address'] = $shopping_address;

        $data['shipping_method'] = $shipping_method;
		
		$data['order_id'] = str_pad($order_id, 6, "0", STR_PAD_LEFT);
		
		$data['ord_id'] = $order_id;


		if($data['lang'] == 'eng')
		{
        	$view = 'layouts/print_page/eng_print';
		}
		else
		{
			$view = 'layouts/print_page/arb_print';
		}

        $this->load->view($view,$data);

    }

}

