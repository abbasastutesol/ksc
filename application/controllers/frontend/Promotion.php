<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Promotion extends CI_Controller {



	public function __construct()

    {

        parent::__construct();

		

        $get_url_arr = explode('/', $_SERVER['REQUEST_URI']);

		if(!$this->session->userdata('site_lang'))

		{

			$this->session->set_userdata('site_lang','arb');

		}

		if(in_array('en', $get_url_arr))

		{

			$this->session->set_userdata('site_lang','eng');

		}

		else

		{

			$this->session->set_userdata('site_lang','arb');

		}

		$this->lang->load("message",$this->session->userdata('site_lang'));

	    $this->load->model('Model_product');

	    $this->load->model('Model_category');

	    $this->load->model('Model_variant');

	    $this->load->model('Model_product_variant_value');

	    $this->load->model('Model_product_variant_group');

		$this->load->model('Model_image');

		$this->load->model('Model_variant_value');

		$this->load->model('Model_related_product');

		$this->load->model('Model_temp_orders');

		$this->load->model('Model_customer_questions');
		$this->load->model('Model_home_slider');
		$this->load->model('Model_promotion');
			

		

    }

	

	public function index()

	{
		$data = array();
		
		$data = $this->lang->line('all');
		$data['lang'] = $this->session->userdata('site_lang');

		$is_home_offer = '0';
		$daily_offer = '1';
        $category_status = 1;
		$data['promotions'] = $this->Model_product->getOfferByDates($is_home_offer,$daily_offer,$category_status);
		
		$data['promotion_slider'] = $this->Model_promotion->get('1');

		$data['content'] = 'promotion/promotion';

		$data['class'] = 'promotion';
        
		$this->load->view('default',$data);	

		

	}



}

