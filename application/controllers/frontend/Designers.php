<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Designers extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
        parent::__construct();
		
		$this->load->helper('url');
		$get_url_arr = explode('/', $_SERVER['REQUEST_URI']);
		if(!$this->session->userdata('site_lang'))
		{
			$this->session->set_userdata('site_lang','arb');
		}
		if(in_array('en', $get_url_arr))
		{
			$this->session->set_userdata('site_lang','eng');
		}
		else
		{
			$this->session->set_userdata('site_lang','arb');
		}
        $this->lang->load("message",$this->session->userdata('site_lang'));
	    $this->load->model('Model_designer');
		//$res = checkLevels(2);
		//checkAuth($res);
    }
	
	public function index()
	{	
		$data = array();
		$data = $this->lang->line('all');
		$data['lang'] = $this->session->userdata('site_lang');
		$data['designers'] = $this->Model_designer->getAll(true);
		$data['content'] = 'designers/designers';
		$data['class'] = 'designers';
		$this->load->view('default',$data);	
		
	}
	
}
