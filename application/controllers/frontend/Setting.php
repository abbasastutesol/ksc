<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
        parent::__construct();
		$this->load->helper('url');
		if(!$this->session->userdata('login'))
		{
			redirect(lang_base_url());
		}
		if(!$this->session->userdata('site_lang'))
		{
			$this->session->set_userdata('site_lang','arb');
		}
		$get_url_arr = explode('/', $_SERVER['REQUEST_URI']);
		
		$this->lang->load("message",$this->session->userdata('site_lang'));
		$this->load->model('Model_registered_users');
	    $this->load->model('Model_registered_user_address');
		$this->load->model('Model_registered_user_cards');
		$this->load->library('PayfortSettings');
		//$res = checkLevels(2);
		//checkAuth($res);
    }
	
	public function action()
	{
		$messages = $this->lang->line('all');
		
		if(isset($_POST['form_type']))
		{

			switch($_POST['form_type'])
			{

				case 'update':
					$this->checkDup($messages);
					$this->update();
					$data['message'] = $messages['settings_success_message'];
					$data['error'] = 'false';
					$data['success'] = 1;
					echo json_encode($data);
					exit;
				break;
				
			}
		}	
		
	}
	
	public function index()
	{	
		$data = array();
		$data = $this->lang->line('all');
		$data['lang'] = $this->session->userdata('site_lang');
		$user_id = $this->session->userdata('user_id');
		$data['user'] = $this->Model_registered_users->get($user_id);
		$fetch_by['user_id'] = $user_id; 
		$data['addresses'] = $this->Model_registered_user_address->getMultipleRows($fetch_by); 
		$data['cards'] = $this->Model_registered_user_cards->getMultipleRows($fetch_by); 
		$data['objFort'] = new PayfortSettings();
		$mobile_ios2 = explode('|', $data['user']->mobile_ios2_code);
		$phone_ios2 = explode('|', $data['user']->phone_ios2_code);
		$data['mobile_ios2'] = $mobile_ios2[0];
		$data['phone_ios2'] = $phone_ios2[0];
		$data['content'] = 'user/settings';
		$data['class'] = 'user';
		$this->load->view('default',$data);	
		
	}
	
	private function update()
	{
		$data = array();
		$html = '';
		$address_html = '';
		$message = $this->lang->line('all');
		$post_data = $this->input->post();	
		
		$keys[] =  'form_type';
		$keys[] =  'settings';
		$keys[] =  'old_email';
		$keys[] =  'update_id';
		$keys[] =  'street_name';
		$keys[] =  'property_no';
		$keys[] =  'address_city';
		$keys[] =  'more_details';
		$keys[] =  'address_country';
		$keys[] =  'address_district';
		$keys[] =  'address_po_box';
		$keys[] =  'address_zip_code';
		$keys[] =  'more_details';
		$keys[] =  'confirm_password';
		$keys[] =  'checkbox';
		$keys[] =  'lang';
		
		foreach($post_data as $key => $value)
		{
			
			if(!in_array($key,$keys))
			{
			 $data[$key] = $value;	
			}
		}
		$data['password'] = sha1($data['password']);
		$data['updated_at'] = date('Y-m-d H:i:s');
		$update_by['id'] = $this->input->post('update_id');	
		
		$update = $this->Model_registered_users->update($data,$update_by);
		$streets = $this->input->post('street_name');
		$property_no = $this->input->post('property_no');
		$address_city = $this->input->post('address_city');
		$more_details = $this->input->post('more_details');
		$address_country = $this->input->post('address_country');
		$address_district = $this->input->post('address_district');
		$address_po_box = $this->input->post('address_po_box');
		$address_zip_code = $this->input->post('address_zip_code');
		$delete_by['user_id'] = $this->input->post('update_id');
		$this->Model_registered_user_address->delete($delete_by);
		$i=0;
		foreach($streets as $street)
		{
			if($street != '')
			{
				$dataAdd['user_id'] = $this->input->post('update_id');
				$dataAdd['street_name'] = $street;
				$dataAdd['property_no'] = $property_no[$i];
				$dataAdd['address_city'] = $address_city[$i];
				$dataAdd['more_details'] = $more_details[$i];
				$dataAdd['address_country'] = $address_country[$i];
				$dataAdd['address_district'] = $address_district[$i];
				$dataAdd['address_po_box'] = $address_po_box[$i];
				$dataAdd['address_zip_code'] = $address_zip_code[$i];
				$this->Model_registered_user_address->save($dataAdd);
			}
			$i++;
		}
	}
	
	private function checkDup($messages)
	{
		$post_data['email'] = $this->input->post('email');
		$post_data['old_email'] = $this->input->post('old_email');
		if($post_data['old_email'] != $post_data['email'])
		{
			$results = $this->Model_registered_users->getRowCount($post_data);
			if($results > 0)
			{
				$data['message'] = $messages['register_duplication'];
				$data['error'] = 'false';
				$data['success'] = 0;
				echo json_encode($data);
				exit;
			}
		}
	}
	
}
