<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Ajax extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */

	public function __construct()

	{

		parent::__construct();

		$this->lang->load("message",$this->session->userdata('site_lang'));

		$this->load->model('Model_product');

		$this->load->model('Model_category');

		$this->load->model('Model_variant');

		$this->load->model('Model_variant_value');

		$this->load->model('Model_product_variant_value');

		$this->load->model('Model_product_variant_group');

		$this->load->model('Model_temp_orders');

		$this->load->model('Model_orders');

		$this->load->model('Model_order_product');

		$this->load->model('Model_wish_list');

		$this->load->model('Model_registered_users');

		$this->load->model('Model_registered_user_address');

		$this->load->model('Model_registered_user_cards');

		$this->load->model('Model_product_rating');
		$this->load->model('Model_product_ratings');

		$this->load->model('Model_temp_user_detail');

		$this->load->model('Model_customer_questions');

		$this->load->model('Model_cart_user_address');

		$this->load->model('Model_general');
		
		$this->load->model('Model_settings');
		
		$this->load->model('Model_coupons');
		
		
		$this->load->model('Model_shipment_group_country');

		$this->load->model('Model_shipment_groups');

	    $this->load->model('Model_groups_payment');
	    $this->load->model('Model_currency');

		

		$this->load->library('PayfortCheckout');



		//$res = checkLevels(2);

		//checkAuth($res);

	}



	public function action()

	{



		if(isset($_POST['form_type']))

		{



			switch($_POST['form_type'])

			{



				case 'save':

					$this->addToCart();

					break;



				case 'save_order':

					$this->saveOrder();

					break;



				case 'save_quantity':

					$this->saveQuantity();

					break;



				case 'get_price':

					$this->getPrice();

					break;



				case 'forgot_password':

					$this->sendForgotPasswordEmail();

					break;



				case 'update_password':

					$this->updatePassword();

					break;



				case 'save_newsletter':

					$this->subscribeNewsLetter();

					break;

					

				case 'save_question':

					$this->saveQuestion();

					break;

					

				case 'save_address':

					$this->saveAddress();

					break;

					

				case 'delete_address':

					$this->deleteCartAddress();

					break;

										

				case 'delete_card':

					$this->deleteCard();

					break;	





			}

		}



	}



	function getProduct($id)

	{

		$messages = $this->lang->line('all');

		$lang = $this->session->userdata('site_lang');

		$html = '';

		$style = '';

		$fetch_by = array();

		if(!get_cookie('mv_p_id'))

		{

			if(isset($id) && $id != '')

			{

				$cookie_array[] = $id;

				$cookie = array(

					'name'   => 'mv_p_id',

					'value'  => serialize($cookie_array),

					'expire' => time()+(10 * 365 * 24 * 60 * 60)

				);

				$this->input->set_cookie($cookie);

			}

		}

		else

		{

			if(isset($id) && $id != '')

			{

				$cookie_array = unserialize(get_cookie('mv_p_id'));

				if(!in_array($id, $cookie_array))

				{

					$cookie_array[] = $id;

					$cookie = array(

						'name'   => 'mv_p_id',

						'value'  => serialize($cookie_array),

						'expire' => time()+(10 * 365 * 24 * 60 * 60)

					);

					$this->input->set_cookie($cookie);

				}

			}

		}



		$fetch_by = $this->Model_product->get($id, true);

		$category_by = $this->Model_category->get($fetch_by['category_id'], true);

		$groupCount = $this->Model_product_variant_group->getMultipleRows(array('product_id'=>$id), true);

		$fetch_varient = $this->Model_variant->getAll(true);

		$image_arr = getProductImages($id);

		if($this->session->userdata('login') == false && !$this->session->userdata('user_id'))

		{

			if(get_cookie('u_id'))

			{

				$checkProductStatus = $this->Model_temp_orders->getRowCount(array('p_id'=>$id, 'user'=>get_cookie('u_id')));

				if($checkProductStatus > 0)

				{

					$style = 'disabled="" style="background-color: #343434; color: #fff; border-color: #222;"';

				}

	

			}

		}

		else

		{

			$checkProductStatus = $this->Model_temp_orders->getRowCount(array('p_id'=>$id, 'user'=>$this->session->userdata('user_id')));

			if($checkProductStatus > 0)

			{

				$style = 'disabled="" style="background-color: #343434; color: #fff; border-color: #222;"';

			}

		}

		if($groupCount)

		{

			$lastElement = sizeof($fetch_varient);

			$selectedVarients = $this->Model_product->fetchSelectedVariants($id);

			$alreadyIn = array();

			$i=1;

			foreach($fetch_varient as $fv)

			{

				$fetch_varient_values = $this->Model_variant_value->getMultipleRows(array('variant_id'=>$fv['id']),true);

				foreach($fetch_varient_values as $fvv){

				

					foreach($selectedVarients as $selVar)

					{

						if($selVar['variant_id'] == $fv['id'] && $selVar['variant_value_id'] == $fvv['id'])

						{

							if(in_array($fvv['id'], $alreadyIn))

							{

								continue;

							}

							else{

								$no_varient[] = $fvv['id'];

							}

						}

					}

					

				}

				if($no_varient)

				{

					$onchange = ' onchange="getVariantPrice();"';

					$varient_drop .= '<h3>'.$fv[$lang.'_name'].'</h3><input type="hidden" id="haveVarient" value="1"><select name="varient[]" id="varient_values_'.$fv['id'].'" '.$onchange.' data-toggle="tooltip" data-placement="top" title=""><option value="">'.$messages['home_display_select'].' </option>';

					foreach($fetch_varient_values as $fvv){

						foreach($selectedVarients as $selVar)

							{

								if($selVar['variant_id'] == $fv['id'] && $selVar['variant_value_id'] == $fvv['id'])

								{

									if(in_array($fvv['id'], $alreadyIn))

									{

										continue;

									}

									$alreadyIn[] = $fvv['id'];

									$varient_drop .= '<option value="'.$fv['id'].'|'.$fvv['id'].'">'.$fvv[$lang.'_value'].'</option>';

								}

							}

					}

					$varient_drop .= '</select>';

					if(strtolower($fv['eng_name']) == 'size')

					{

						//$varient_drop .= '<a class="fancybox fancybox.ajax" href="'.base_url().'ajax/getMeasurements" style="text-decoration:underline;">'.$messages['home_display_sizes_chart'].'</a> %80 ';

					}

					$i++;

				}

			}

		}

		$html .= '<div class="quickDisplay">

					<div class="qDispRight">

						<div id="productSlider" class="carousel slide" data-ride="carousel">

							<div class="bigImage">

								<div class="carousel-inner" role="listbox">';

		if(!empty($image_arr))

		{

			$im=0;

			foreach($image_arr as $img)

			{

				$active = '';

				if($im == 0)

				{

					$active = 'active';

				}

				$html .= '<div class="item '.$active.'"><img src="'.base_url().'uploads/images/products/'.$img[$lang.'_image'].'" alt="Big Image" height="398" width="182" /></div>';

				$im++;

			}

		}

		else

		{

			$html .= '<div class="item active"><img src="'.base_url().'uploads/images/products/no_imge.png" alt="Big Image" height="398" width="182" /></div>';

		}

		$html .= '</div>

							</div>

							<div class="smallImages">

								<ol class="carousel-indicators">';

		if(!empty($image_arr))

		{

			$im=0;

			foreach($image_arr as $img)

			{

				$active = '';

				if($im == 0)

				{

					$active = 'class="active"';

				}

				$html .= '<li data-target="#productSlider" data-slide-to="'.$im.'" '.$active.'><img src="'.base_url().'uploads/images/products/'.$img[$lang.'_image'].'" alt="image" height="67" width="46" /></li>';

				$im++;

			}

		}

		//<div class="qDisPrice oldPrice" id="old-price">'.$fetch_by[$lang.'_price'].' <span>'.getPriceCur($fetch_by[$lang.'_price'], true).'</span></div>

		$html .= '</ol>

							</div>

							<div class="clearfix"></div>

						</div>

						<div class="clearfix"></div>

					</div>

					<form class="ajax_form" action="'.base_url().'ajax/action/" onsubmit="return false;" method="post">

						<input type="hidden" name="form_type" value="save">

						<input type="hidden" name="prod_price" id="prod_price" value="'.($fetch_by['has_discount_price'] == 1 ? $fetch_by[$lang.'_discount_price'] : $fetch_by[$lang.'_price']).'">

						<input type="hidden" id="tpl_name" value="'.$fetch_by['tpl_name'].'">

						<div class="qDispLeft">

							<div class="qDisTopText">

								<h1>'.$category_by[$lang.'_name'].'</h1>

								<h2>'.$fetch_by[$lang.'_name'].'</h2>

								<div class="stars stars-example-bootstrap">';

								  $rate = getAverageRating($product['id']);

								$html .=  '<select class="example-bootstrap" name="rating" autocomplete="off" onchange="rateProduct(this.value, \''.$id.'\');">

									<option '.($rate == 1 ? 'selected' : '').' value="1">1</option>

									<option '.($rate == 2 ? 'selected' : '').' value="2">2</option>

									<option '.($rate == 3 ? 'selected' : '').' value="3">3</option>

									<option '.($rate == 4 ? 'selected' : '').' value="4">4</option>

									<option '.($rate == 5 ? 'selected' : '').' value="5">5</option>

								  </select>

								</div>

								<div class="clearfix"></div>

							</div>

							'.($fetch_by['has_discount_price'] == 1 ? '<div class="qDisPrice" id="new-price"><span class="discount" id="old-price">'.$fetch_by[$lang.'_price'].'</span>'.$fetch_by[$lang.'_discount_price'].' <span>'.getPriceCur($fetch_by[$lang.'_discount_price'], true).'</span></div>' : '<div class="qDisPrice" id="new-price">'.$fetch_by[$lang.'_price'].' <span>'.getPriceCur($fetch_by[$lang.'_price'], true).'</span></div>').$varient_drop.'					

							<div class="clearfix"></div>

							<div class="clearfix"></div>

							<div class="showProText">

								'.$fetch_by[$lang.'_description'].'

							</div>					

							<input type="hidden" name="product_id" id="p_id" value="'.$id.'">

							<div class="clearfix"></div>

							<a href="javascript:void(0);"><button type="submit" class="edBtnGreen square" '.$style.'><img src="'.base_url().'assets/frontend/images/nCartBtn.png" alt="Cart" height="34" width="33" />'.$messages['products_page_add_to_cart'].'</button></a>

							<a href="'.lang_base_url().'product/product_page/'.($fetch_by['tpl_name'] != '' ? $fetch_by['tpl_name'] : $id).'"><span class="edBtn red" style="border-radius: 8px;">'.$messages['home_display_more_details'].' ‎</span></a>

							'.($this->session->userdata('login') == true ? '<div class="clearfix"></div>

						<a href="javascript:void(0);" onclick="addToList(\''.$id.'\')" class="underlineTxt">'.$messages['products_page_add_to_wish_list'].'</a>':'').'

						</div>

					</form>

					<div class="clearfix"></div>

				</div>

				<button type="button" style="display:none;" id="btn-message" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-sm">Small modal</button>

				

				<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">

				  <div class="modal-dialog modal-sm">

					<div class="modal-content">

					   <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">

          <span aria-hidden="true">&times;</span>

        </button>

      </div>

      <div class="modal-body">

        <p id="message_created"></p>

      </div>

					</div>

				  </div>

				</div>

				<script>

					ratingEnable();

				</script>

				

				';

		echo $html;exit();

	}

	function getMeasurements()

	{

		$messages = $this->lang->line('all');

		$lang = $this->session->userdata('site_lang');

		$config = getCconfiguration(true);

		$html = '';

		$html .= '<div class="sizeChartPopup">

					<h1>'.$messages['size_chart_size_chart'].'</h1>

					<img src="'.base_url().'uploads/images/home/'.$config[$lang.'_size_chart'].'" alt="Big Image" />

				</div>';



		echo $html; exit();

	}



	private function addToCart()

	{

		$data = array();

		$varient_arr = array();

		$messages = $this->lang->line('all');

		$lang = $this->session->userdata('site_lang');

		$product_id = $this->input->post('product_id');

		$getproduct = $this->Model_product->get($product_id);

		if($getproduct->eng_quantity > 0)

		{

			$group_ids = $this->Model_product_variant_group->getMultipleRowsFields(array('product_id'=>$product_id), array('id'));

			if($group_ids)

			{

				foreach($this->input->post('varient') as $var)

				{

					$var2 = explode('|',$var);

					$varient_arr[] = $var2[1];

				}

				$varients = implode('|',$varient_arr);

				foreach($group_ids as $g_id)

				{

					/*$checkquant = $this->Model_product_variant_group->get($g_id->id);

					if($checkquant->eng_quantity < 1)

					{

						$success['message'] = $messages['shopping_out_of_stock'];

						$success['success'] = 0;

						echo json_encode($success);

						exit;

					}*/

					$varient_array=array();

					$varient_id_array=array();

					$fetch_varient_group_id = $this->Model_product_variant_value->checkVarient($g_id->id);

					foreach($fetch_varient_group_id as $fvg_id)

					{

						$varient_id_array[] = $fvg_id->variant_id;

						$varient_array[] = $fvg_id->variant_value_id;

					}

					if($varients == implode('|',$varient_array))

					{

						if($this->session->userdata('login') == false && !$this->session->userdata('user_id'))

						{

							if(!get_cookie('u_id'))

							{

								$cid = strtotime(date('Y-m-d H:i:s')).rand(0, 000);

								$cookie = array(

									'name'   => 'u_id',

									'value'  => $cid,

									'expire' => time()+(10 * 365 * 24 * 60 * 60)

								);



								$this->input->set_cookie($cookie);

							}

							else

							{

								$cid = get_cookie('u_id');

							}

							$data['guest'] = 1;

						}

						else

						{

							$cid = $this->session->userdata('user_id');

							$data['guest'] = 0;

						}

						$checkDup_by['user'] = $cid;

						$checkDup_by['p_id'] = $product_id;

						$checkDup = $this->Model_temp_orders->getRowCount($checkDup_by);

						if($checkDup >= 1)

						{

							$success['message'] = $messages['shopping_product_already_added_to_cart'];

							$success['success'] = 0;

							echo json_encode($success);

							exit;

						}

						else

						{

							$data['p_id'] = $product_id;

							$data['user'] = $cid;

							$data['varient_id'] = implode('|',$varient_id_array);

							$data['varient_value_id'] = $varients;

							$data['prod_price'] = $this->input->post('prod_price');

							$data['quantity'] = 1;

							$this->Model_temp_orders->save($data);

							$updateQuant['eng_quantity'] = $getproduct->eng_quantity-1;

							$updateQuant['arb_quantity'] = $getproduct->eng_quantity-1;

							$updateQuant_by['id'] = $product_id;

							$this->Model_product->update($updateQuant, $updateQuant_by);

							$success['message'] = $messages['shopping_product_added_to_cart'];

							$success['success'] = 1;

							echo json_encode($success);

							exit;

						}

					}

					else

					{

						$success['message'] = $messages['shopping_product_does_not_exist'];

						$success['success'] = 0;

					}

				}

			}

			else

			{

				if($this->session->userdata('login') == false && !$this->session->userdata('user_id'))

				{

					if(!get_cookie('u_id'))

					{

						$cid = strtotime(date('Y-m-d H:i:s')).rand(0, 000);

						$cookie = array(

							'name'   => 'u_id',

							'value'  => $cid,

							'expire' => time()+(10 * 365 * 24 * 60 * 60)

						);



						$this->input->set_cookie($cookie);

					}

					else

					{

						$cid = get_cookie('u_id');

					}

					$data['guest'] = 1;

				}

				else

				{

					$cid = $this->session->userdata('user_id');

					$data['guest'] = 0;

				}

				$checkDup_by['user'] = $cid;

				$checkDup_by['p_id'] = $product_id;

				$checkDup = $this->Model_temp_orders->getRowCount($checkDup_by);

				if($checkDup >= 1)

				{

					$success['message'] = $messages['shopping_product_already_added_to_cart'];

					$success['success'] = 0;

					echo json_encode($success);

					exit;

				}

				else

				{

					$data['p_id'] = $product_id;

					$data['user'] = $cid;

					$data['prod_price'] = $this->input->post('prod_price');

					$data['quantity'] = 1;

					

					$this->Model_temp_orders->save($data);

					$updateQuant['eng_quantity'] = $getproduct->eng_quantity-1;

					$updateQuant['arb_quantity'] = $getproduct->eng_quantity-1;

					$updateQuant_by['id'] = $product_id;

					$this->Model_product->update($updateQuant, $updateQuant_by);

					$success['message'] = $messages['shopping_product_added_to_cart'];

					$success['success'] = 1;

					echo json_encode($success);

					exit;

				}

			}

		}

		else

		{

			$success['message'] = $messages['shopping_out_of_stock'];

			$success['success'] = 0;

			echo json_encode($success);

			exit;

		}

		$success['message'] = $messages['shopping_product_not_added_to_cart'];

		$success['success'] = 0;

		echo json_encode($success);

		exit;

	}

	

	private function saveOrder()

	{

		

		$messages = $this->lang->line('all');

		$lang = $this->session->userdata('site_lang');

		$data = array();

		$dataProd = array();

		$variantarr = array();

		$variantval = array();

		$change_payment_method = 0;

		$total_prod_price = 0;

		$recurring_transaction = false;

		$data = $this->input->post();

		if($this->session->userdata('login') == true && $this->session->userdata('user_id') > 0)

		{

			$data['user_id'] = $this->session->userdata('user_id');

			$user = $this->Model_registered_users->get($this->session->userdata('user_id'));

			if(!$this->input->post('street_name') && $this->input->post('street_name') == '')

			{



				$reg_add = $this->Model_registered_user_address->get($data['address_checkbox']);

				$data['street_name'] = $reg_add->street_name;

				$data['property_no'] = $reg_add->property_no;

				$data['address_city'] = $reg_add->address_city;

				$data['more_detail'] = $reg_add->more_details;

				$data['address_country'] = $reg_add->address_country;

				$data['address_district'] = $reg_add->address_district;

				$data['address_po_box'] = $reg_add->address_po_box;

				$data['address_zip_code'] = $reg_add->address_zip_code;

			}

		}else

		{

			$data['user_id'] = get_cookie('u_id');

			

		}

		$temp_user = $this->Model_temp_user_detail->getSingleRow(array('temp_user_id'=>$data['user_id']));

		$data['first_name'] = $temp_user->first_name;

		$data['last_name'] = $temp_user->last_name;

		$data['email'] = $temp_user->email;

		$data['mobile_no'] = $temp_user->mobile_no;

		$data['phone_no'] = $temp_user->phone_no;

		$data['mobile_ios2_code'] = $temp_user->mobile_ios2_code;

		$data['phone_ios2_code'] = $temp_user->phone_ios2_code;

		$data['city'] = $temp_user->city;

		$data['country'] = $temp_user->country;

		/*if ($this->input->post('card_method_old') != '')

		{

			$data['payment_method'] = $this->input->post('card_method_old');

			$fetch_by['id'] = $data['payment_method'];

			$card_data = $this->Model_registered_user_cards->get($fetch_by['id'], true);

			$token = $card_data['token_name'];

			$change_payment_method = 0;

		}else{

			$data['payment_method'] = $this->input->post('card_method_new');

			$change_payment_method = 1;

		}*/

		if(get_cookie('payment_method') == 'cc_merchantpage')

		{

			$data['payment_method'] = 'VISA';

		}elseif(get_cookie('payment_method') == 'sadad')

		{

			$data['payment_method'] = 'SADAD';

		}else

		{

			$data['payment_method'] = 'Cash On Delivery';

		}

		$data['payment_status'] = '1';

		$data['created_at'] = date('Y-m-d H:i:s');

		$temp_order_id = $data['temp_order_id'];

		$temp_order_quantity = $data['temp_order_quantity'];

		$temp_prod_price = $data['temp_price'];

		unset($data['form_type']);

		unset($data['temp_order_id']);

		unset($data['temp_order_quantity']);

		unset($data['temp_price']);

		unset($data['address_checkbox']);

		$order_id = $this->Model_orders->save($data);

		$i=0;

		foreach($temp_order_id as $tem_order)

		{

			$temp_orders = $this->Model_temp_orders->get($tem_order);

			//$produt_detail = $this->Model_products->get($temp_orders->p_id);

			/*if($produt_detail->has_discount_price == 1)

			{

				$product_price = $produt_detail->eng_discount_price;

			}*/

			$variantarr = explode('|', $temp_orders->varient_id);

			$variantval = explode('|', $temp_orders->varient_value_id);

			$j=0;

			foreach($variantarr as $va_arr)

			{

				$option[] = $va_arr.'|'.$variantval[$j];

				$j++;

			}

			$this->Model_product_variant_group->updateQuantity($temp_orders->p_id, $option, $temp_order_quantity[$i]);

			if($this->session->userdata('login') == true && $this->session->userdata('user_id') > 0)

			{

				$dataProd['user_id'] = $this->session->userdata('user_id');

			}

			else

			{

				$dataProd['user_id'] = get_cookie('u_id');

			}

			$dataProd['order_id'] = $order_id;

			$dataProd['product_id'] = $temp_orders->p_id;

			$dataProd['varient_id'] = $temp_orders->varient_id;

			$dataProd['varient_value_id'] = $temp_orders->varient_value_id;

			$dataProd['quantity'] = $temp_order_quantity[$i];

			$dataProd['price'] = $temp_prod_price[$i];

			$total_prod_price = $total_prod_price+$dataProd['price'];

			$dataProd['guest'] = $temp_orders->guest;

			$this->Model_order_product->save($dataProd);

			$this->Model_temp_orders->delete(array("id"=>$tem_order));

			$this->Model_temp_user_detail->delete(array("temp_user_id"=>$dataProd['user_id']));

			$i++;

		}

		

		if($lang == 'eng')

		{

			emailTemplateEng($order_id, $messages);

		}

		else

		{

			emailTemplateArb($order_id, $messages);

		}



		//delete_cookie("u_id");

		$response['success'] = 1;

		$response['fname'] = $data['first_name'];

		$response['lname'] = $data['last_name'];

		$response['email'] = $data['email'];

		$response['amount'] = $total_prod_price;

		$response['payment_type'] = $data['payment_method'];

		$response['recurring_transaction'] = $recurring_transaction;

		$response['change_payment_method'] = $change_payment_method;

		$response['order_id'] = str_pad($order_id, 10, "0", STR_PAD_LEFT);

		echo json_encode($response);

		exit;

	}



	private function saveQuantity()

	{

        $lang = $this->session->userdata('site_lang');

		$temp_order_id = $this->input->post('temp_order_id');

		$p_id = $this->Model_temp_orders->get($temp_order_id);

		$data['quantity'] = $this->input->post('quantity');

		$getproduct = $this->Model_product->get($p_id->p_id);

		

		if($data['quantity'] > ($getproduct->eng_quantity+$p_id->quantity))

		{

			if($lang == 'eng')

			{

				$message['msg'] = 'Only '.$getproduct->eng_quantity.' pieces remaining.';

			}

			else

			{

				$message['msg'] = ''.$getproduct->eng_quantity.' قطع فقط متبقية';

			}

			$message['more'] = true;

			echo json_encode($message);

			exit;

		}

		else

		{	

			if($getproduct->eng_quantity-($data['quantity']) > 0)

			{

				$updateQuant['eng_quantity'] = ($getproduct->eng_quantity+$p_id->quantity)-($data['quantity']);

				$updateQuant['arb_quantity'] = ($getproduct->eng_quantity+$p_id->quantity)-($data['quantity']);

			}

			else

			{

				$updateQuant['eng_quantity'] = 0;

				$updateQuant['arb_quantity'] = 0;

				$updateQuant['out_of_stock'] = 1;

				$updateQuant['out_of_stock_date'] = date('Y-m-d');

			}

		}

		$updateQuant_by['id'] = $p_id->p_id;

		$this->Model_product->update($updateQuant, $updateQuant_by);

		$this->Model_temp_orders->update($data, array("id"=>$temp_order_id));

	}

	

	public function saveQuestion()

	{

		$messages = $this->lang->line('all');

		$data['product_id'] = $this->input->post('product_id');

		$data['question'] = $this->input->post('question');

		$data['lang'] = $this->session->userdata('site_lang');

		$inserted_id = $this->Model_customer_questions->save($data);

		if($inserted_id)

		{

			questionEmail($data['question'], $data['product_id'], $inserted_id);

			$response['message'] = $messages['products_page_customer_questions_success'];

			$response['success'] = 1;

		}

		echo json_encode($response);

		exit;

		

	}

	

	public function saveAddress()

	{

		$data = $this->input->post();



        $user_id = $this->session->userdata('user_id');

		if($this->session->userdata('login') == true && $user_id > 0)

		{

			$data['user_id'] = $this->session->userdata('user_id');

		}

		else

		{

			$data['user_id'] = get_cookie('u_id');



			$dataReg['cookie_id'] = $data['user_id'];



			$checkBy['id'] = $user_id;

			$count = $this->Model_registered_users->getRowCount($checkBy);

			if($count > 0)

			{

				$dataReg['guest'] = 0;

				$inserted_id = $this->Model_registered_users->update($dataReg, $checkBy);

			}

			else

			{
							
				if($data['regAdd'])
				{
					$dataReg['first_name'] = $data['full_name'];
					$dataReg['email'] = $data['email'];
					$dataReg['mobile_no'] = $data['mobile_no'];
					$dataReg['country'] = $data['country'];
					$dataReg['city'] = $data['city'];
					$dataReg['cookie_id'] = get_cookie('u_id');
					$dataReg['password'] = sha1($data['password']);
					$dataReg['guest'] = 0;
					$data['phone_no'] = $data['mobile_no'];
					$inserted_id = $this->Model_registered_users->save($dataReg);
					$data['user_id'] = $inserted_id;
					$this->send_new_email($data['email'], $data);
					$this->send_admin_email($data);
					$this->session->set_userdata(array('regUserId' => $inserted_id));
					$this->session->set_userdata('user_id',$inserted_id);
					$this->session->set_userdata('user_name',$data['full_name']);
					$this->session->set_userdata('login',true);
					$updateAddressBy['user_id'] = get_cookie('u_id');
					$updateAddressBy['used'] = 0;
					$updateAddress['user_id'] = $inserted_id;
 					$this->Model_cart_user_address->update($updateAddress, $updateAddressBy);
					
				}
				else
				{
					unset($data['password']);
					unset($data['regAdd']);
				}
				

			}

			

		}

		if(!isset($data['phone_no']))
		{
			$data['phone_no'] = $data['mobile_no'];
		}

		$update_by['id'] = $data['update_id'];

		$address_type = $data['address_type'];


		unset($data['form_type']);
		unset($data['update_id']);
		unset($data['mobile_no']);
		unset($data['password']);
		unset($data['regAdd']);

		if($update_by['id'] > 0)

		{
		    $updatedBy['id'] = $update_by['id'];


 			$this->Model_cart_user_address->update($data, $updatedBy);

			$inserted_id = $update_by['id'];

		}

		else

		{
			if($this->session->userdata('login') == false && !$this->session->userdata('user_id'))

			{
				$checkAddBy['user_id'] = get_cookie('u_id');
				
				$count = $this->Model_cart_user_address->getRowCount($checkAddBy);
				if($count > 0)
				{
					$data['used'] = 0;
					$inserted_id = $this->Model_cart_user_address->update($data, $checkAddBy);
				}
				else
				{
	
					$inserted_id = $this->Model_cart_user_address->save($data);
				}
			}
			else
			{
	
				$inserted_id = $this->Model_cart_user_address->save($data);
			}

		}

		if($inserted_id)

		{

			$response['success'] = 1;

			$response['inserted_id'] = $inserted_id;

		}

		echo json_encode($response);

		exit;

	}

	

	

	public function deleteCartAddress()

	{

	    $response = array();

		$delete_by['id'] = $this->input->post('id');

		$delete = $this->Model_cart_user_address->delete($delete_by);

		if($delete)

		{

			$response['success'] = 1;

		}

		echo json_encode($response);

		exit;

	}



    public function deleteRegisterAddress()

    {

        $response = array();

        $delete_by['id'] = $this->input->post('id');

        $delete = $this->Model_cart_user_address->delete($delete_by);

        if($delete)

        {

            $response['success'] = 1;

        }

        echo json_encode($response);

        exit;

    }

	

	public function deleteCard()

	{

		$delete_by['id'] = $this->input->post('id');

		$delete = $this->Model_registered_user_cards->delete($delete_by);

		if($delete)

		{

			$response['success'] = 1;

		}

		echo json_encode($response);

		exit;

	}

    
	public function saveRating()

	{
        $response = array();
		$messages = $this->lang->line('all');
        $lang = $this->session->userdata('site_lang');

		$data = $this->input->post();
        $user_id = $this->session->userdata('user_id');
        $existRating = getRatingByOrder($data['order_id'],$user_id);

        if(!$existRating) {
            if ($this->ratingValidation()) {
                $data['user_id'] = $this->session->userdata('user_id');

                $fetch_by['user_id'] = $data['user_id'];
                $fetch_by['order_id'] = $data['order_id'];

                $result = $this->Model_product_rating->getSingleRow($fetch_by);

                if (!$result) {

                    $inserted_id = $this->Model_product_rating->save($data);

                    $response['success'] = true;
                    $response['validation'] = true;
                    $response['message'] = $messages['give_us_your_review'];

                    // send email here to the admin
                    $order_id =  str_pad($data['order_id'], 6, "0", STR_PAD_LEFT);
                    $user = getUserById($data['user_id']);
                    $subject = $messages['order_feedback_subject'];
                    $emailData['message'] = $messages['hi_label']." IOUD, <br><br> ".$messages['order_feedback_received']." ".$user['first_name']." ".$user['last_name']." ".$messages['order_feedback_against_order']." ".$order_id;
                    $emailData['title'] = $messages['order_feedback_subject'];
                    $emailData['info'] = array();
                    $setting = getSettings();
                    $to = $setting->admin_email;

                    if($lang == "eng") {
                        $view = $this->load->view('layouts/emails/eng_general_email',$emailData,TRUE);
                    }else{
                        $view = $this->load->view('layouts/emails/arb_general_email',$emailData,TRUE);
                    }
                    $sent = send_email_admin($subject, $view, $to);
                    
                } else {
                    // old logic was to update feedback if submit again
                    //$inserted_id = $this->Model_product_rating->update($data,$fetch_by);

                    $response['success'] = false;
                    $response['validation'] = true;
                    $response['message'] = $messages['already_rated'];
                }
            } else {

                $response['validation'] = false;
                $response['message'] = $messages['rating_validation_msg'];
            }

        }else{
            $response['success'] = false;
            $response['validation'] = true;
            $response['message'] = $messages['already_rated'];
        }
		echo json_encode($response);

		exit;


	}

    public function productRating()
    {
        $messages = $this->lang->line('all');
        $data['product_id'] = $this->input->post('product_id');
        $data['rating'] = $this->input->post('rating');
        if($this->session->userdata('login') == true && $this->session->userdata('user_id') > 0)
        {
            if(get_cookie('prod_rate'))
            {
                $check_array = unserialize(get_cookie('prod_rate'));
                if(in_array($data['product_id'], $check_array))
                {
                    $response['message'] = $messages['already_rated'];
                    echo json_encode($response);
                    exit;

                }
            }
            $data['user_id'] = $this->session->userdata('user_id');

            $inserted_id = $this->Model_product_ratings->save($data);
            $product_rating['rating'] = round(getAverageRating($data['product_id']));
            $update_by['id'] = $data['product_id'];
            $this->Model_product->update($product_rating, $update_by);
            if($inserted_id)
            {
                if(!get_cookie('prod_rate'))
                {
                    $cookie_array[] = $data['product_id'];
                    $cookie = array(
                        'name'   => 'prod_rate',
                        'value'  => serialize($cookie_array),
                        'expire' => time() + (10 * 365 * 24 * 60 * 60)
                    );
                    $this->input->set_cookie($cookie);
                }
                else
                {
                    $cookie_array = unserialize(get_cookie('prod_rate'));
                    if(!in_array($data['product_id'], $cookie_array))
                    {
                        $cookie_array[] = $data['product_id'];
                        $cookie = array(
                            'name'   => 'prod_rate',
                            'value'  => serialize($cookie_array),
                            'expire' => time() + (10 * 365 * 24 * 60 * 60)
                        );
                        $this->input->set_cookie($cookie);
                    }
                }
                $response['message'] = $messages['place_feedback_success'];
                echo json_encode($response);
                exit;
            }
            else
            {
                $response['message'] = $messages['not_saved'];
                echo json_encode($response);
                exit;
            }
        }else
        {
            $response['message'] = $messages['login_to_rate_product'];
            echo json_encode($response);
            exit;
        }
    }
	
	
	private function ratingValidation(){

		    $errors = array();

			//$this->form_validation->set_error_delimiters('<p>', '</p>');

           

			$this->form_validation->set_rules('feedback', 'Feedback', 'required');

			$this->form_validation->set_rules('product_rating', 'Product Rating', 'required');
			
			$this->form_validation->set_rules('packaging_rating', 'Packaging Rating', 'required');
			$this->form_validation->set_rules('packing_rating', 'Packingt Rating', 'required');
			$this->form_validation->set_rules('service_rating', 'Service Rating', 'required');
			$this->form_validation->set_rules('shipment_rating', 'Shipmentt Rating', 'required');
			$this->form_validation->set_rules('satisfaction_rating', 'Satisfaction Rating', 'required');
			
			
			if ($this->form_validation->run() == FALSE)

			{

				//$errors['error'] = validation_errors();

				//$errors['success'] = 'false';
				
				return false;

			}else

			{

				return true;

			}
	
	}



	private function getPrice()

	{

		$messages = $this->lang->line('all');

		$product_id = $this->input->post('product_id');

		$option = explode(',',$this->input->post('option_sel'));

		$flg = false;

		foreach($option as $opt)

		{

			if($opt == '')

			{

				$flg = true;

			}

		}

		if($flg)

		{

			$result = $this->Model_product->get($product_id);

		}else

		{

			$result = $this->Model_product_variant_group->fetchPrice($product_id, $option);

		}

		if($result->eng_price != '')

		{

			$response['success'] = 1;

			$response['message'] = str_replace(' <span>', '|<span>', getPriceCur($result->eng_price)).'|'.str_replace(' <span>', '|<span>',getPriceCur($result->eng_discount_price));

		}

		else

		{

			$response['success'] = 0;

			$response['message'] = $messages['shopping_product_no_product'];

		}

		echo json_encode($response);

		exit;

	}



	public function addToList()

	{

		$messages = $this->lang->line('all');

		$data['product_id'] = $this->input->post('product_id');

		if($this->session->userdata('user_id') > 0 && $this->session->userdata('login') == true)

		{

			$data['user_id'] = $this->session->userdata('user_id');

		}

		elseif(get_cookie('u_id'))

		{

			$data['user_id'] = get_cookie('u_id');

		}

		else

		{

			$cid = strtotime(date('Y-m-d H:i:s')).rand(0, 000);

			$cookie = array(

				'name'   => 'u_id',

				'value'  => $cid,

				'expire' => time()+(10 * 365 * 24 * 60 * 60)

			);



			$this->input->set_cookie($cookie);

			$data['user_id'] = get_cookie('u_id');

		}



		$checkList = $this->Model_wish_list->getSingleRow(array('user_id'=>$data['user_id'], 'product_id'=>$data['product_id']));

		if($checkList)

		{

			$response['success'] = 0;

			$response['message'] = $messages['wishlist_already_in'];

		}

		else

		{

			$inserted_id = $this->Model_wish_list->save($data);

			$response['success'] = 1;

			$response['message'] = $messages['wishlist_added_to'];

		}

		echo json_encode($response);

		exit;

	}

	

	public function saveForLater()

	{

		$messages = $this->lang->line('all');

		$id = $this->input->post('temp_id');

		$data['saved'] = $this->input->post('saved');

		$update_by['id'] = $id;



		$update = $this->Model_temp_orders->update($data,$update_by);

		$response['saved'] = $data['saved'];

		echo json_encode($response);

		exit;

	}



	private function sendForgotPasswordEmail()

	{

		$email = $this->input->post('forgot_email');

		$result = $this->Model_registered_users->getSingleRow(array('email'=>$email));

		$lang = $this->session->userdata('site_lang');

		$messages = $this->lang->line('all');

		$config = getCconfiguration();

		if($lang == 'arb')

		{

			$body_style = 'style="direction:rtl"';

			$left = 'right';

			$right = 'left';

			$td_phone_style = ' unicode-bidi: plaintext;';



		}

		else

		{

			$body_style = '';

			$left = 'left';

			$right = 'right';

			$td_phone_style = '';

		}

		if($result)

		{

			$password = randomPassword();

			$data['password'] = sha1($password);

			$update_by['email'] = $this->input->post('forgot_email');

			$update = $this->Model_registered_users->update($data,$update_by);

			$html = '<html>

					<head>

					<!-- Please use an inliner tool to convert all CSS to inline as inpage or external CSS is removed by email clients -->

					<!-- When use in Email please remove all comments as it is removed by Email clients-->

					<!-- important in CSS is used to prevent the styles of currently inline CSS from overriding the ones mentioned in media queries when corresponding screen sizes are encountered -->

					

					<style type="text/css">

					body {

						color: #777;

						padding:0;

						margin: 0;

					}

					body, table, td, p, a, li, blockquote {

						-webkit-text-size-adjust: none !important;

						font-style: normal;

						font-weight: 400;

						font-family: \'Roboto\', sans-serif;

					}

					@import \'https://fonts.googleapis.com/css?family=Roboto\';

					@import \'url(http://fonts.googleapis.com/earlyaccess/notosanskannada.css)\';

					button {

						width: 90%;

					}

					p {

						font-size: 13px !important;

						color: #777 !important;

						line-height: normal !important;

						padding-bottom: 14px !important;

						margin: 0px !important;

					}

					@media screen and (max-width:600px) {

					/*styling for objects with screen size less than 600px; */

					body, table, td, p, a, li, blockquote {

						-webkit-text-size-adjust: none!important;

					}

					table {

						/* All tables are 100% width */

						width: 100%;

					}

					.footer {

						/* Footer has 2 columns each of 48% width */

						height: auto !important;

						max-width: 48% !important;

						width: 48% !important;

					}

					table.responsiveImage {

						/* Container for images in catalog */

						height: auto !important;

						max-width: 30% !important;

						width: 30% !important;

					}

					table.responsiveContent {

						/* Content that accompanies the content in the catalog */

						height: auto !important;

						max-width: 66% !important;

						width: 66% !important;

					}

					.top {

						/* Each Columnar table in the header */

						height: auto !important;

						max-width: 48% !important;

						width: 48% !important;

					}

					.catalog {

						margin-left: 0%!important;

					}

					}

					

					@media screen and (max-width:480px) {

					/*styling for objects with screen size less than 480px; */

					body, table, td, p, a, li, blockquote {

					}

					table {

						/* All tables are 100% width */

						width: 100% !important;

						border-style: none !important;

					}

					.footer {

						/* Each footer column in this case should occupy 96% width  and 4% is allowed for email client padding*/

						height: auto !important;

						max-width: 96% !important;

						width: 96% !important;

					}

					.table.responsiveImage {

						/* Container for each image now specifying full width */

						height: auto !important;

						max-width: 96% !important;

						width: 96% !important;

					}

					.table.responsiveContent {

						/* Content in catalog  occupying full width of cell */

						height: auto !important;

						max-width: 96% !important;

						width: 96% !important;

					}

					.top {

						/* Header columns occupying full width */

						height: auto !important;

						max-width: 100% !important;

						width: 100% !important;

					}

					.catalog {

						margin-left: 0%!important;

					}

					button {

						width: 90%!important;

					}

					}

					table a {color:#d80000 !important;text-decoration:none !important;}

					</style>

					</head>

					<body '.$body_style.'>

					<table width="100%" cellspacing="0" cellpadding="0">

					  <tbody>

						<tr>

						  <td>

							<table width="500"  align="center" cellpadding="0" cellspacing="0" style="background:url(images/header-top.jpg) repeat-x top left;">

							  <!-- Main Wrapper Table with initial width set to 60opx -->

							  <tbody>

								<tr>

								  <td>

										<table class="top" width="100%"  align="left" cellpadding="0" cellspacing="0" style="padding:35px 0 53px;">

										  <!-- First header column with Logo -->

										  <tbody>

											<tr>

											  <td align="center" valign="middle"><a href="'.base_url().'" target="_blank"><img src="'.base_url().'assets/frontend/images/logo.png" alt="eDesign"></a></td>

											</tr>

										  </tbody>

									  </table>

					

								  </td>

								</tr>

								<tr>

								  <td>

										<table width="100%" align="left"  cellpadding="0" cellspacing="0" >

									  <tr>

										<td align="center" valign="middle" style="font-size:13px; font-weight: 500; padding-bottom: 22px;">'.$messages['email_template_hi'].' '.$result->first_name.' '.$result->last_name.',</td>

									  </tr>

									  <tr>

										<td align="center" valign="middle" style="font-family: \'Roboto\', sans-serif; font-weight: 100; font-size: 22px; color: #E00513; padding-bottom: 16px;">

											'.$messages['settings_new_password'].'

										</td>

									  </tr>

									</table>

								  </td>

								</tr>

								<tr>

									<td>

										<table width="400" cellpadding="0" cellspacing="0" border="1" style="border:0px; border-bottom: 1px solid rgba(119, 119, 119, 0.51); margin: 0 auto 30px; padding: 35px 0 0; ">

											<tr>

												<td align="'.$left.'" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase;"><strong>'.$messages['chat_email'].'</strong></td>

												<td align="'.$right.'" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-'.$left.':0; padding: 10px 5px;font-size:12px;font-weight: normal; color:#777; ">'.$email.'</td>

											</tr>

											<tr>

												<td align="'.$left.'" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; "><strong>'.$messages['register_password'].'</strong></td>

												<td align="'.$right.'" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-'.$left.':0; padding: 10px 5px;font-size:12px;font-weight: normal; color:#777;" >'.$password.'</td>

											</tr>

										</table>

									</td>

								</tr>

								<tr>

									<td align="center" valign="middle" style="font-size:13px; padding: 46px 0 18px;">'.$messages['email_template_footer_need_more_help'].'</td>

								</tr>

								<tr>

									<td align="center" valign="middle" style="font-size:11px; padding: 0px 0 15px;">'.$messages['email_template_footer_service_team'].' <span style="'.$td_phone_style.'"> '.$config->phone.' </span> '.$messages['email_template_or'].'<br>'.$messages['chat_email'].' '.$config->email.'</td>

								</tr>

								<tr>

									<td align="center" valign="middle" style="font-size:11px; padding: 0px 0 50px;">'.$messages['email_template_thank_you_for_shopping'].'</td>

								</tr>

							  </tbody>

							  <tfoot bgcolor="#333">

									<tr>

										<td colspan="2">

											<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-top: 1px solid #f1f1f1; padding: 20px 55px 46px;">

												<tr>

													<td style="direction: '.($lang == 'arb' ? 'ltr' : 'rtl').';" align="center">

														<span style="font-size:11px; color:#fff;">'.$messages['login_follow_us'].'</span>

														<a href="'.$config->facebook_link.'" target="_blank" style="display:inline-block; margin-right:14px;"><img src="'.base_url().'assets/frontend/images/facebook-icon.png" alt=""></a>

														<a href="'.$config->twitter_link.'" target="_blank" style="display:inline-block; margin-right:14px;"><img src="'.base_url().'assets/frontend/images/twitter-icon.png" alt=""></a>

														<a href="'.$config->instagram_link.'" target="_blank" style="display:inline-block; margin-right:14px;"><img src="'.base_url().'assets/frontend/images/instagram-icon.png" alt=""></a>

														<a href="'.$config->snapchat_link.'" target="_blank" style="display:inline-block; margin-right:14px;"><img src="'.base_url().'assets/frontend/images/snapchat-icon.png" alt=""></a>

														<a href="'.$config->youtube_link.'" target="_blank" style="display:inline-block; margin-right:16px;"><img src="'.base_url().'assets/frontend/images/youtube-icon.png" alt=""></a>

														

													</td>

												</tr>

											</table>

										</td>

									</tr>

									<tr>

										<td>

											<table width="100%" cellpadding="0" cellspacing="0" border="0" style="padding: 0px 55px 24px;">

												<tr>

													<td align="'.$left.'" valign="middle" style="font-family: \'Noto Sans Kannada\', sans-serif; font-size:11px; color: #fff;">'.($lang == 'arb' ? 'جميع الحقوق محفوظة I eDesign '.date('Y').'' : 'All rights reserved '.date('Y').' | eDesign').'</td>

													<td align="'.$right.'" valign="middle"><a href="'.base_url().'" target="_balnk" style="font-size:11px; color: #ffffff; text-decoration: none;">'.$messages['design_visit_our_store'].'</a></td>

												</tr>

											</table>

										</td>

									</tr>

								  </tfoot>

							</table>

							</td>

						</tr>

					  </tbody>

					</table>

					</body>

					</html>';

			htmlmail($email, $messages['forgot_password_password_recovery'], $html, 'developer@edesign.com.sa');

			$response['success'] = 1;

			$response['message'] = $messages['forgot_password_restore_email'];

		}

		else

		{

			$response['success'] = 0;

			$response['message'] = $messages['forgot_password_no_such_user'];

		}

		echo json_encode($response);

		exit;

	}



	public function delOrderCart()

	{
		$response = array();
		$prod_id = $this->input->post('prod_id');
        $this->session->unset_userdata('temp_order_id');
		/*$p_id = $this->Model_temp_orders->get($prod_id);
		$getproduct = $this->Model_product->get($p_id->p_id);

		$updateQuant['eng_quantity'] = $getproduct->eng_quantity+($p_id->quantity);

		$updateQuant['arb_quantity'] = $getproduct->eng_quantity+($p_id->quantity);

		$updateQuant_by['id'] = $p_id->p_id;

		$this->Model_product->update($updateQuant, $updateQuant_by);*/

		$success =  $this->Model_temp_orders->delete(array('id'=>$prod_id));
		
		
		if ($this->session->userdata('login') == false && !$this->session->userdata('user_id')) 
		{
			$user_id = get_cookie('u_id');
		}
		else
		{
			$user_id = $this->session->userdata('user_id');
		}
		
		$get_by_user['user'] = $user_id;
		$all_products =  $this->Model_temp_orders->getMultipleRows($get_by_user);
		$productCount =  $this->Model_temp_orders->getRowCount($get_by_user);
		$grandTotal = 0;
		foreach($all_products as $products){
			$grandTotal += $products->prod_price;
		}
		
		if($productCount == 0)
		{
			delete_cookie("giftIds");
		}
		
		if($success){
			$response['deleted'] = true;
			$response['count'] = cartCount();
			$response['grandTotal'] = $grandTotal;
		}else{
			$response['deleted'] = false;
		}
		 echo json_encode($response);
		exit;

	}



	private function updatePassword()

	{

		$messages = $this->lang->line('all');

		$user_id = $this->session->userdata('user_id');

		$old_password = $this->input->post('old_password');

		$password = $this->input->post('password');

		$result = $this->Model_registered_users->getSingleRow(array('id'=>$user_id));

		if($result->password == sha1($old_password))

		{

			$data['password'] = sha1($password);

			$update_by['id'] = $user_id;

			$update = $this->Model_registered_users->update($data,$update_by);

			$response['success'] = 1;

			$response['message'] = $messages['settings_password_success_message'];

		}

		else

		{

			$response['success'] = 1;

			$response['message'] = $messages['settings_invalid_password'];

		}

		echo json_encode($response);

		exit;

	}



	public function subscribeNewsLetter()

	{

		$messages = $this->lang->line('all');

		$email = $this->input->post('newsletter');

		$settings = getSettings();

		$apiKey = $settings->mailChimp_api_key;

		$listID = $settings->mailchimp_list_id;

		// MailChimp API URL

		$memberID = md5(strtolower($email));

		$dataCenter = substr($apiKey,strpos($apiKey,'-')+1);

		$url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . $memberID;



		$check_subscribed = $this->mc_checklist($email, false, $apiKey, $listID, $dataCenter);

		if($check_subscribed == '404' || $check_subscribed == '' || !$check_subscribed)

		{

			// member information

			// member information
			
			/*[

				'email_address' => $email,

				'status'        => 'subscribed',

				'merge_fields'  => [

					'FNAME'     => '',

					'LNAME'     => ''

				]

			]*/
			
			$sub_arr['email_address'] = $email;
			$sub_arr['status'] = 'subscribed';
			$sub_arr['merge_fields']['FNAME'] = '';
			$sub_arr['merge_fields']['LNAME'] = '';
			

			$json = json_encode($sub_arr);

			// send a HTTP POST request with curl
			
			$ch = curl_init($url);

			curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);

			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type'=>'application/json'));

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			curl_setopt($ch, CURLOPT_TIMEOUT, 10);

			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');

			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

			curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

			$result = curl_exec($ch);

			$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

			curl_close($ch);
		


			// store the status message based on response code


			if ($httpCode == 200) {

				$response['message'] = '<p style="color: #34A853">'.$messages['newsletter_success_msg'].'</p>';

			} else {

				switch ($httpCode) {

					case 214:

						$msg = $messages['newsletter_already_subscribed_msg'];

						break;

					default:

						$msg = $messages['newsletter_error_msg'];

						break;

				}

				$response['message'] = '<p style="color: #EA4335">'.$msg.'</p>';

			}

		}

		else

		{

			$msg = $messages['newsletter_already_subscribed_msg'];

			$response['message'] = '<p style="color: #EA4335">'.$msg.'</p>';

		}

		echo json_encode($response);

		exit;

	}



	public function mc_checklist($email, $debug, $apikey, $listid, $server) {

		$userid = md5($email);

		$auth = base64_encode( 'user:'. $apikey );

		$data = array(

			'apikey'        => $apikey,

			'email_address' => $email

		);

		$json_data = json_encode($data);

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, 'https://'.$server.'.api.mailchimp.com/3.0/lists/'.$listid.'/members/' . $userid);

		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',

			'Authorization: Basic '. $auth));

		curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($ch, CURLOPT_TIMEOUT, 10);

		curl_setopt($ch, CURLOPT_POST, true);

		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);

		$result = curl_exec($ch);

		if ($debug) {

			var_dump($result);

		}

		$json = json_decode($result);

		return $json->{'status'};

	}



	public function filterSidbarSearch()

	{

		$data = $this->input->post();

		$lang = $this->session->userdata('site_lang');

		$messages = $this->lang->line('all');

		/*$min_price = $this->input->post('min_price');

		$max_price = $this->input->post('max_price');

		$min_range = $this->input->post('min_range');

		$max_range = $this->input->post('max_range');

		$colorCheck = $this->input->post('colorCheck');

		$sizeCheck = $this->input->post('sizeCheck');

		$reviewCheck = $this->input->post('reviewCheck');*/

		$products = $this->Model_product->search($data, $lang);



		$page_type = $this->input->post('page_type');



		$result = '';



		if($products)

		{



			if($page_type == 'product_page')

			{

				$main=1;

				foreach($products as $product){

					$image_arr = getProductImages($product['id']);

					if($image_arr)

					{

						$result .= '<li><div class="imgBox">';

						if(sizeof($image_arr) > 1){

							$result .= '<div id="Product_id_'.$main.'" class="carousel slide cntCenter" data-ride="carousel"><div class="carousel-inner" role="listbox">';

							$im=0;

							foreach($image_arr as $image){

								$result .= '<div class="item'.($im==0 ? ' active' : '').'"><a href="'.lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']).'"><img src="'.base_url().'uploads/images/products/'.$image[$lang.'_image'].'" alt="Product" height="199" width="129" ></a></div>';

								$im++;

							}

							$result .= '</div></div>';

						}else{

							if($image_arr){

								foreach($image_arr as $image){

									$result .= '<a href="'.lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']).'"><img src="'.base_url().'uploads/images/products/'.$image[$lang.'_image'].'" alt="Product" height="199" width="129" /></a>';

								}

							} else{

								$result .= '<a href="'.lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']).'"> <img src="'.base_url().'uploads/images/products/no_imge.png" alt="Product" height="199" width="129" /></a>';

							}

						}

						$result .= '</div>';

						if(sizeof($image_arr) > 1){

							$result .= '<ol class="carousel-indicators">';

							$imo=0;

							foreach($image_arr as $image){

								$result .= '<li data-target="#Product_id_'.$main.'" data-slide-to="'.$imo.'"'.($imo==0 ? ' class="active"' : '').'></li>';

								$imo++;

							}

							$result .= '</ol>';

						}

						$result .= '<h3><a href="'.lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']).'">'.$product[$lang.'_name'].'</a></h3>';

							if($product['has_discount_price'])

							{

								$result .= '<h4><span class="discount">'.$product[$lang.'_price'].'</span>'.getPriceCur($product[$lang.'_discount_price']).'</h4>';

							}

							else

							{

								$result .= '<h4>'.getPriceCur($product[$lang.'_price']).'</h4>';

							}

						$result .= '

						

						<div class="starAddToCart"><div class="addToCart"><a class="fancybox fancybox.ajax" href="'.base_url().'ajax/getProduct/'.$product['id'].'"><img src="'.base_url().'assets/frontend/images/addToCartGreen.png" alt="Cart" height="15" width="17" /></a></div><div class="openInNewWindow"><a href="'.lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']).'"><img src="'.base_url().'assets/frontend/images/openLinkSep.png" alt="Open" height="15" width="17" /></a></div><div class="stars stars-example-bootstrap">';

						$rate = getAverageRating($product['id']);

						$result .= '<select class="example-bootstrap" name="rating" autocomplete="off" onchange="rateProduct(this.value, \''.$product['id'].'\'"><option '.($rate == 1 ? 'selected' : '').' value="1">1</option><option '.($rate == 2 ? 'selected' : '').' value="2">2</option><option '.($rate == 3 ? 'selected' : '').' value="3">3</option><option '.($rate == 4 ? 'selected' : '').' value="4">4</option><option '.($rate == 5 ? 'selected' : '').' value="5">5</option></select></div><div class="clearfix"></div></div></li>';

						$main++;

					}

				}

			}

			if($page_type == 'search_page')

			{

				foreach($products as $product){

					$image_arr = getProductImages($product['id']);

					if($image_arr)

					{

						$result .= '<li><div class="imgBox">';

						if(sizeof($image_arr) > 1){

							$result .= '<div id="Product_id_'.$main.'" class="carousel slide cntCenter" data-ride="carousel"><div class="carousel-inner" role="listbox">';

							$im=0;

							foreach($image_arr as $image){

								$result .= '<div class="item'.($im==0 ? ' active' : '').'"><a href="'.lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']).'"><img src="'.base_url().'uploads/images/products/'.$image[$lang.'_image'].'" alt="Product" height="199" width="129" ></a></div>';

								$im++;

							}

							$result .= '</div></div>';

						}else{

							if($image_arr){

								foreach($image_arr as $image){

									$result .= '<a href="'.lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']).'"><img src="'.base_url().'uploads/images/products/'.$image[$lang.'_image'].'" alt="Product" height="199" width="129" /></a>';

								}

							} else{

								$result .= '<a href="'.lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']).'"> <img src="'.base_url().'uploads/images/products/no_imge.png" alt="Product" height="199" width="129" /></a>';

							}

						}

						$result .= '</div>';

						if(sizeof($image_arr) > 1){

							$result .= '<ol class="carousel-indicators">';

							$imo=0;

							foreach($image_arr as $image){

								$result .= '<li data-target="#Product_id_'.$main.'" data-slide-to="'.$imo.'"'.($imo==0 ? ' class="active"' : '').'></li>';

								$imo++;

							}

							$result .= '</ol>';

						}

						$result .= '<h3><a href="'.lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']).'">'.$product[$lang.'_name'].'</a></h3>';

							if($product['has_discount_price'])

							{

								$result .= '<h4><span class="discount">'.$product[$lang.'_price'].'</span>'.getPriceCur($product[$lang.'_discount_price']).'</h4>';

							}

							else

							{

								$result .= '<h4>'.getPriceCur($product[$lang.'_price']).'</h4>';

							}

						$result .= '<div class="starAddToCart"><div class="addToCart"><a class="fancybox fancybox.ajax" href="'.base_url().'ajax/getProduct/'.$product['id'].'"><img src="'.base_url().'assets/frontend/images/addToCartGreen.png" alt="Cart" height="15" width="17" /></a></div><div class="openInNewWindow"><a href="'.lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']).'"><img src="'.base_url().'assets/frontend/images/openLinkSep.png" alt="Open" height="15" width="17" /></a></div><div class="stars stars-example-bootstrap">';

						$rate = getAverageRating($product['id']);

						$result .= '<select class="example-bootstrap" name="rating" autocomplete="off" onchange="rateProduct(this.value, \''.$product['id'].'\');"><option '.($rate == 1 ? 'selected' : '').' value="1">1</option><option '.($rate == 2 ? 'selected' : '').' value="2">2</option><option '.($rate == 3 ? 'selected' : '').' value="3">3</option><option '.($rate == 4 ? 'selected' : '').' value="4">4</option><option '.($rate == 5 ? 'selected' : '').' value="5">5</option></select></div><div class="clearfix"></div></div></li>';

						$main++;

					}

				}

			}

		}

		else

		{

			$result = '<li>'.$messages['no_results_found'].'</li>';

		}

		echo $result;

		exit;

	}



	public function makePayment()

	{

		$data = array();

		$response = array();

		$change_payment_method = 0;

		$total_amount = 0;

		$recurring_transaction = 0;

		$lang = $this->session->userdata('site_lang');

		$user_data = $this->Model_registered_users->get($this->session->userdata('user_id', true));

		$total_amount = $this->input->post('total_amount');



		if ($this->input->post('card_method_old') != '')

		{

			$data['payment_method'] = $this->input->post('card_method_old');

			$fetch_by['id'] = $data['payment_method'];

			$card_data = $this->Model_registered_user_cards->get($fetch_by['id'], true);

			$token = $card_data['token_name'];

			$change_payment_method = 0;

		}else{

			$data['payment_method'] = $this->input->post('card_method_new');

			$change_payment_method = 1;

		}



		//echo 'here';exit();

		if (($change_payment_method == 0) && ($token != ''))

		{

			$ObjRecurring = new PayfortCheckout();



			$ObjRecurring->customer_email = $card_data['customer_email'];

			$ObjRecurring->token = $card_data['token_name'];

			$ObjRecurring->customer_name = $card_data['customer_name'];

			$ObjRecurring->payment_option = $card_data['payment_option'];

			$ObjRecurring->amount = $total_amount;

			$ObjRecurring->language = $lang;

			$ObjRecurring->item_name = 'test';



			$ObjRecurring->security_code = '123';

			//$objFort->payment_type = 'cc_merchantpage';



			$recurring_transaction_response = $ObjRecurring->merchantPageRecurringTrans();

			/*echo 'recurring';

			echo '<pre>';

			print_r($recurring_transaction_response);

			exit();*/

			$recurring_transaction = 1;

		}

		if(!$this->session->userdata('login')){

			$tempUser['temp_user_id'] = get_cookie('u_id');

		}

		else

		{

			$tempUser['temp_user_id'] = $this->session->userdata('user_id');

		}

		

		$tempUser['first_name'] = $this->input->post('first_name');

		$tempUser['last_name'] = $this->input->post('last_name');

		$tempUser['email'] = $this->input->post('email');

		if(strlen($this->input->post('phone_no'))>6)

		{

			$tempUser['phone_no'] = $this->input->post('phone_no');

			$tempUser['phone_ios2_code'] = $this->input->post('phone_ios2_code');

		}

		$tempUser['mobile_no'] = $this->input->post('mobile_no');

		$tempUser['mobile_ios2_code'] = $this->input->post('mobile_ios2_code');

		$tempUser['city'] = $this->input->post('city');

		$tempUser['country'] = $this->input->post('country');

		$check_rows = $this->Model_temp_user_detail->getRowCount(array('temp_user_id'=>$tempUser['temp_user_id']));

		if($check_rows>0)

		{

			$update = $this->Model_temp_user_detail->update($tempUser, array('temp_user_id'=>$tempUser['temp_user_id']));

		}

		else

		{

			$insert_id = $this->Model_temp_user_detail->save($tempUser);

		}

		

		

		$cookie = array(

			'name'   => 'payment_method',

			'value'  => $data['payment_method'],

			'expire' => time()+(10 * 365 * 24 * 60 * 60)

		);

		$this->input->set_cookie($cookie);

		

		$response['success'] = 1;

		$response['fname'] = $user_data->first_name;

		$response['lname'] = $user_data->last_name;

		$response['email'] = $user_data->email;

		$response['amount'] = $total_amount;

		$response['payment_type'] = $data['payment_method'];

		$response['recurring_transaction'] = $recurring_transaction;

		$response['change_payment_method'] = $change_payment_method;

		echo json_encode($response);

		exit;



	}

	public function getCities()

	{
		$lang = $this->session->userdata('site_lang');
		
		$country = $this->input->post('country');
		
		$country_row = getCoutryByName($country);

		/*if($lang == 'eng'){
			$country_name = $country_row->eng_country_name;
		}else{
			$country_name = $country_row->arb_country_name;
		}
		$response['country'] = getCoutries($country_name);*/
		$response['html'] = getCity($country);

		echo json_encode($response);

		exit;

	}
	public function getCities_html()

	{

		$lang = $this->session->userdata('site_lang');

		$country_code = $this->input->post('country_code');
		$country = $this->Model_general->getSingleRow('countries', array(
            'code' => $country_code
        ));
		$city_id = $this->input->post('city_id');
		$response['ios'] = strtolower($country->country_code);
		$response['html'] = getCity_html($country_code, $city_id);

		echo json_encode($response);

		exit;

	}

    public function get_cities_html_address()

    {

        $lang = $this->session->userdata('site_lang');

        $country_code = $this->input->post('country_code');
        $country = $this->Model_general->getSingleRow('countries', array(
            'code' => $country_code
        ));
        $city_id = $this->input->post('city_id');
        $response['ios'] = strtolower($country->country_code);

        $response['html'] = get_cities_html_address($country_code, $city_id);
        if($country_code == "SAU"){
            $response['other_country'] = false;
        }else{
            $response['other_country'] = true;
        }
        echo json_encode($response);

        exit;

    }

	public function getAddressForEdit(){



	    $id = $this->input->post('address_id');

	    $address_type = $this->input->post('address_type');

	    $addresses = $this->Model_cart_user_address->getSingleRow(array('id' => $id), true);

        echo json_encode($addresses);

        exit;

    }


	public function getShipmentGroup()
	{
		$lang = $this->session->userdata('site_lang');
        $response = array();
        $response['error'] = '';
		$messages = $this->lang->line('all');
		$country_code = $this->input->post('country_code');	
		$city_id = $this->input->post('city_id');		
		$coun = $this->Model_general->getSingleRow('countries', array('code'=>$country_code));
		$searchBy['country_id'] = $coun->id;
		$searchBy['city_id'] = $city_id;

        $settings = $this->Model_settings->get('1');
        $shipment = false;

        $shipping = $this->Model_shipment_groups->getShipmentDetailByLocation($searchBy);

        if($settings->shipment_methods == 1) {


            $normal_shipment_amount = currencyRatesCalculate($shipping['normal_shipment_amount']);
            $fast_shipment_amount = currencyRatesCalculate($shipping['fast_shipment_amount']);
            $shipment = true;
        }else{
           $address_id = $this->input->post('address_id');

            $aramex = getAramexRate($address_id);

            if($aramex['error'] != ""){
                $response['error'] =  $aramex['error'];
                echo json_encode($response);
                exit;
            }

            $aramex = $aramex['data'];
            //$currency = $aramex->TotalAmount->CurrencyCode;
            $toAmount = $aramex->TotalAmount->Value;

            $tAmount = currencyRatesCalculate($toAmount);
            $shipment = true;
        }



		$flag = true;
		
		if(get_cookie('voucher_code'))
		{
			$voucher_code = get_cookie('voucher_code');
			$fetch_by['code'] = $voucher_code;
			$coupon = $this->Model_coupons->getSingleRow($fetch_by);
			if($coupon->free_shipping == 1)
			{
				$flag = false;
			}
		}

        $html = '';
		if($shipment && $flag)
		{
			$html .= '<ul>';
            if($settings->shipment_methods == 1) {
                if ($shipping['normal_shipment_amount'] != 0 && $shipping['normal_shipment_amount'] != 0) {

                    $html .= '<li>
	
					<input id="option_1" class="shipment_method_select" data-id="' . $shipping['id'] . '" type="radio" name="payOption" value="' . $shipping['id'] . '|' . $shipping['normal_shipment_amount'] . '|normal" >
	
					<label for="option_1"><span>&nbsp;</span></label>
	
					<strong>' . $messages['checkout_shipping_address_normal_shipping'] . ' </strong>,  ' . $shipping[$lang . '_normal_shipment_days'] . ' <strong> ' . ($shipping['normal_shipment_amount'] != '0' && $shipping['normal_shipment_amount'] != '' ? '(' . $normal_shipment_amount["rate"] . ' ' . $normal_shipment_amount["unit"] . ' ' . $messages['checkout_shipping_address_extra'] . ')' : '') . '</strong>
	
				</li>';
                }
                if ($shipping['fast_shipment_amount'] != 0 && $shipping['fast_shipment_amount'] != 0) {
                    $html .= '<li>
                
                    <input  id="option_2" class="shipment_method_select" data-id="' . $shipping['id'] . '" type="radio" name="payOption" value="' . $shipping['id'] . '|' . $shipping['fast_shipment_amount'] . '|fast" >
    
                    <label for="option_2"><span>&nbsp;</span></label>
    
                    <strong>' . $messages['checkout_shipping_address_fast_shipping'] . '</strong> ' . $shipping[$lang . '_fast_shipment_days'] . ' <strong> ' . ($shipping['fast_shipment_amount'] != '0' && $shipping['fast_shipment_amount'] != '' ? '(' . $fast_shipment_amount["rate"] . ' ' . $fast_shipment_amount["unit"] . ' ' . $messages['checkout_shipping_address_extra'] . ')' : '') . '</strong>
    
                </li>';
                }
            }else{

                $html .= '<li>
	
					<input id="option_1" class="shipment_method_select" data-id="" type="radio" name="payOption" value="'.$shipping['id'].'|' . $toAmount . '|aramex" >
	
					<label for="option_1"><span>&nbsp;</span></label>
	
					<strong>' . $messages['aramex_shipment'] . ' </strong> <strong> ' . '( '.$tAmount["rate"]. ' ' . $tAmount["unit"].' ) '. '</strong>
	
				</li>';

            }


            $html .= '</ul>';

		}elseif(!$flag)
		{
			$html = '<ul>
	
								 
	
				<li>
	
					<input id="option_1" class="shipment_method_select" data-id="'.$shipping['id'].'" type="radio" name="payOption" value="'.$shipping['id'].'|0|free" >
	
					<label for="option_1"><span>&nbsp;</span></label>
	
					<strong>'.$messages['checkout_shipping_address_free_shipping'].' 0 '.getCurrency($lang).'</strong>
	
				</li>	
			</ul>';
		}else
		{
			$html = false;
		}
		$response['html'] = $html;
//		/$response['group_id'] = $shipping['id'];
		$response['shipping_charges'] = $flag;
		
		echo json_encode($response);
		exit;
							
	}




    public function searchByCategories(){



        $get_by = array();

        $lang = $this->session->userdata('site_lang');

        $cats = $this->input->post('cats');

        $html = '';



        if($cats != "") {

            $categories = rtrim($cats, ',');

            $cate = explode(',', $categories);

            $products = array();

            foreach ($cate as $key => $cat) {

                $get_by['category_name'] = $cat;

                $get_by['active'] = 1;

                $products[] = $this->Model_product->getProductWithCategory($get_by, true);



                foreach ($products[$key] as $product) {



                    $product_image = getProductImages($product['id']);
					
					foreach($product_image as $key => $product_img){
						if($product_img['is_thumbnail'] == 1){
							$thumbnail = $product_img['thumbnail'];
						}
					}
							

                    $html .= '<li>' .

                        '<div class="whiteBox">' .

                        '<a href="' . lang_base_url() . 'product/product_page/' . $product['id'] . '">' .

                        '<div class="imgBox"><img src="' . base_url() . 'uploads/images/thumbs/products/' . $thumbnail . '" alt="Product" width="111" height="144"></div>' .

                        '</a>' .

                        '<a href="' . lang_base_url() . 'product/product_page/' . $product['id'] . '">' .

                        '<h3>' . $product[$lang . "_name"] . '</h3></a>'.

                        '<p>'.$product[$lang."_cat_name"].'</p>'.

                        '<h4>' . number_format(getPriceByOffer($product['id']), '2') . ' '.getCurrency($lang).'</h4>' .

                        '<input type="hidden" class="selQty" name="selQty" value="1">
						<a href="javascript:void(0);" onclick="addToCart(' . $product["id"] . ')">' .

                        '<button class="btn btn-success" type="button">' .

                        '<img src="' . base_url() . '/assets/frontend/images/plusCart.png' . '" alt="Add Cart" width="34" height="16">' .

                        '</button>' .

                        '</a>' .

                        '</div>' .

                        '</li>';





                }

            }

        }else{

            $get_by['active'] = 1;
			$get_by['category_status'] = 1;

            $products = $this->Model_product->getProductWithCategory($get_by,true);
			
            foreach ($products as $product) {
				

                $product_image = getProductImages($product['id']);
				
				foreach($product_image as $key => $product_img){
					if($product_img['is_thumbnail'] == 1){
						$thumbnail = $product_img['thumbnail'];
					}
				}
					

                $html .= '<li>' .

                    '<div class="whiteBox">' .

                    '<a href="' . lang_base_url() . 'product/product_page/' . $product['id'] . '">' .

                    '<div class="imgBox"><img src="' . base_url() . 'uploads/images/thumbs/products/' . $thumbnail . '" alt="Product" width="111" height="144"></div>' .

                    '</a>' .

                    '<a href="' . lang_base_url() . 'product/product_page/' . $product['id'] . '">' .

                    '<h3>' . $product[$lang . "_name"] . '</h3></a>'.

                	'<p>'.$product[$lang."_cat_name"].'</p>'.

                    '<h4>' . number_format(getPriceByOffer($product['id']), '2') . ' '.getCurrency($lang).'</h4>' .

                    '<input type="hidden" class="selQty" name="selQty" value="1">
					<a href="javascript:void(0);" onclick="addToCart(' . $product["id"] . ')">' .

                    '<button class="btn btn-success" type="button">' .

                    '<img src="' . base_url() . '/assets/frontend/images/plusCart.png' . '" alt="Add Cart" width="34" height="16">' .

                    '</button>' .

                    '</a>' .

                    '</div>' .

                    '</li>';

            }



        }
		
        echo json_encode($html);

        exit();

    }

	public function send_new_email($email,$data)
	{ 	
		$lang = $this->session->userdata('site_lang');
		$data['msg'] = $this->lang->line('all');
		//echo "<pre>"; print_r($data); exit;
	
		
		/*if($lang == "eng"){
			$data['message'] = "Hi ".$data['full_name'].", <br> Your Account has been created successfully, and below are the details.";
		}else{
		$data['message'] = "Hi ".$data['full_name'].", <br> Your Account has been created successfully. <br> <br>";
		}*/
		$data['successMsg'] = $data['msg']['register_successfully_to_user'];
		
		$data['username'] = $data['full_name'];
		
		if($lang == "eng") {
			$view = $this->load->view('layouts/emails/eng_registration',$data,TRUE);
		}else{
			$view = $this->load->view('layouts/emails/arb_registration',$data,TRUE);
		}
		
		$body = $view;
		
		$subject = $data['msg']['register_user_subject'].":";
		/*$headers  = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
		$headers .= $msg['register_user_from'].':<IOUDStore>' . "\r\n";*/
		if(htmlmail($email,$subject,$body,'info@ioud.ed.sa')){
			return true;
		}
 
     }
	 
	 private function send_admin_email($data){
		
		$lang = $this->session->userdata('site_lang');
	 	$data['msg'] = $this->lang->line('all');
		
	 	$admin_email = $this->Model_settings->get('1');
		
		/*if($lang == "eng") {
		$data['message'] = "Hi Admin, <br> The user registration request has been received and here is the user detail. <br><br>";
		}else{
			$data['message'] = "Hi Admin, <br> The user registration request has been received and here is the user detail. <br><br>";
		}*/
		
		$data['successMsg'] = $data['msg']['user_request_to_admin'];
		
		$data['username'] = "IOUD";
		/*$body = "Hi Admin, <br> The user registration request has been received and here is the user detail. <br><br>
		Name: ".$data['first_name']." ".$data['last_name']." <br>
		Mobile: ".$data['mobile_no']." <br>
		Email: ".$data['email']." <br>
		Country: ".$data['country']." <br>
		City: ".$data['city']." <br>
		Date of birth: ".$data['dob']." <br><br>";*/
		
		if($lang == "eng") {
			$view = $this->load->view('layouts/emails/eng_registration',$data,TRUE);
		}else{
			$view = $this->load->view('layouts/emails/arb_registration',$data,TRUE);
		}
		$body = $view;
		
		$subject = $data['msg']['register_user_subject'].":";
		/*$headers  = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
		$headers .= $data['msg']['register_user_from'].':<IOUDStore>' . "\r\n";*/
		if(htmlmail($admin_email->admin_email,$subject,$body,'info@ioud.ed.sa')){
			 
			return true;
		}else{
			return false;
		}
		
		
	 }
	 
	 public function addGiftCard()
	 {
		 $id = $this->input->post('id');
		 $amount = $this->input->post('amount');
		 $setCookie = $this->input->post('setCookie');
		 if($setCookie)
		 {
			 if(get_cookie('giftIds'))
			 {
				 $arrCookie = unserialize(get_cookie('giftIds'));
				 $arrCookie[] = $id;	
			 }
			 else
			 {
				  $arrCookie[] = $id;
			 }
			 $cookie = array(
				'name'   => 'giftIds',
				'value'  => serialize($arrCookie),
				'expire' => time()+(365 * 24 * 60 * 60)
			  );
			  $this->input->set_cookie($cookie);
		 }
		 else
		 {
			 $arrCookie = unserialize(get_cookie('giftIds'));
			 foreach($arrCookie as $key=>$val)
			 {
				 if($val == $id)
				 {
					 unset($arrCookie[$key]);
				 }
			 }
			 $arrCookie = array_values($arrCookie);
			 $cookie = array(
				'name'   => 'giftIds',
				'value'  => serialize($arrCookie),
				'expire' => time()+(365 * 24 * 60 * 60)
			  );
			  $this->input->set_cookie($cookie);
		 }
		 $response['status'] = 1;
		 echo json_encode($response);
		 exit;
	 }

	 public function voucherCode()
	 {
         $flag = true;
		 $voucher_code = $this->input->post('voucher_code');
		 $cart_total = $this->input->post('cart_total');
		 $cart_total_val = $this->input->post('cart_total_val');
		 $product_ids = $this->input->post('product_ids');

		 $getBy['code'] = $voucher_code;
         $total_amount = $this->Model_coupons->getSingleRow($getBy);

         // if coupon is active else de active
         if($total_amount->status == 1) {
             // if there is no total amount add in coupon then consider unlimited
             //and coupon applied
             if ($total_amount->total_amount == '' && $total_amount->uses_per_coupon == 0) {
                 $coupon = $this->Model_coupons->checkVoucherCode($voucher_code, '', 0);
             }
             if ($total_amount->total_amount != '' && $total_amount->uses_per_coupon == 0) {
                 $coupon = $this->Model_coupons->checkVoucherCode($voucher_code, $cart_total_val, 0);
             }

             if ($total_amount->total_amount == '' && $total_amount->uses_per_coupon != 0) {
                 $coupon = $this->Model_coupons->checkVoucherCode($voucher_code, '', 1);
             }

             if ($total_amount->total_amount != '' && $total_amount->uses_per_coupon != 0) {
                 $coupon = $this->Model_coupons->checkVoucherCode($voucher_code, $cart_total_val, 1);
             }

             if ($coupon) {
                 if ($coupon->customer_login == 1 && $this->session->userdata('login') == false && !$this->session->userdata('user_id')) {
                     $flag = false;
                 }

                 foreach ($product_ids as $product_id) {
                     $product = $this->Model_product->get($product_id);
                     $checkCategory = $this->Model_coupons->checkVoucherCodeCategory($product->category_id, $coupon->id);

                     $checkProduct = $this->Model_coupons->checkVoucherCodeProduct($product_id, $coupon->id);

                     // here check if coupon's children tables do not have cat and prod both
                     $checkProdExist = $this->Model_coupons->checkCouponProdExist($coupon->code);
                     $checkCatExist = $this->Model_coupons->checkCouponCatExist($coupon->code);

                     // coupon has no cat and prod so applied on all products
                     if ($checkProdExist == 0 && $checkCatExist == 0) {
                         $flag = true;
                         if ($flag) break;
                     }
                     //==================================================

                     // there is no cat and prod in coupon then not applied
                     if (!$checkCategory && !$checkProduct) {
                         $flag = false;
                     }

                     // if cat and prod exist in coupon's children tables then applied
                     if ($checkCategory && $checkProduct) {
                         $flag = true;
                         if ($flag) break;
                     }
                     // if cat exist in coupon but no product in coupon_products
                     if ($checkCategory && $checkProdExist == 0) {
                         $flag = true;
                         if ($flag) break;
                     }
                     // if cat and prod exist in coupon's children tables
                     // then check if cat and prod exist in coupon
                     if ($checkCatExist > 0 && $checkProdExist > 0) {

                         if ($checkCategory && $checkProduct) {
                             $flag = true;
                             if ($flag) break;
                         } else {
                             $flag = false;
                         }
                     }
                 }

                 if ($flag) {
                     $this->couponApplied($coupon, $cart_total, $cart_total_val, $voucher_code);
                     exit;
                 } else {
                     $this->couponNotApplied();
                     exit;
                 }
             } else {
                 $this->couponNotApplied();
                 exit;
             }
         }else{
             $this->couponNotApplied();
             exit;
         }
	 }

	 private function couponApplied($coupon,$cart_total,$cart_total_val,$voucher_code){
         $messages = $this->lang->line('all');
         // for get discount
         if($coupon->type == 1)
         {
             $discount = ($cart_total_val*$coupon->discount)/100;
         }
         else
         {
             $discount = $coupon->discount;
         }
         $cookie = array(
             'name'   => 'voucher_code',
             'value'  => $voucher_code,
             'expire' => time()+(365 * 24 * 60 * 60)
         );
         $this->input->set_cookie($cookie);

         $response['success'] = 1;
         if(get_cookie('voucher_code'))
         {
             $response['new_total'] = $cart_total;
         }
         else
         {
             $response['new_total'] = ($cart_total - $discount);
         }


         $response['success'] = 1;
         $response['discount'] = $discount;
         $response['old'] = $cart_total;
         $response['message'] = '<i class="fa fa-exclamation-triangle errorMsgBody" aria-hidden="true"></i>'.$messages['shopping_cart_voucher_code_success'];
         echo json_encode($response);
         exit;
     }

    private function couponNotApplied(){
        $messages = $this->lang->line('all');
        $response['success'] = 0;
        $response['message'] = '<i class="fa fa-exclamation-triangle errorMsgBody" aria-hidden="true"></i>'.$messages['shopping_cart_voucher_code_error'];

        echo json_encode($response);
        exit;
    }
    
    public function getCurrency(){
        $data = array();
        $currency_id = $this->input->post('currency_id');

        $this->session->set_userdata('currency_id',$currency_id);


        $uid['user'] = get_cookie('u_id');

        $tmep_products = $this->Model_temp_orders->getMultipleRows($uid);

        foreach ($tmep_products as $tmep_product){

            if($tmep_product->currency_id != $currency_id){

                $getCurr = $this->Model_currency->get($currency_id);

                $prodPrice = $tmep_product->prod_price /$tmep_product->currency_rate;

                $newPrdPrice = $prodPrice*$getCurr->rate;
                $priceUpdate['prod_price'] = $newPrdPrice;
                $priceUpdate['currency_rate'] = $getCurr->rate;
                $priceUpdate['currency_id'] = $getCurr->id;

                $priceUpdateBy['id'] = $tmep_product->id;


                $this->Model_temp_orders->update($priceUpdate,$priceUpdateBy);
            }
        }

        $data['success'] = true;
        echo json_encode($data);
        exit;
    }

    public function getLoyaltyPopup(){
        $data = array();
        $total = $this->input->post('loyaltyPopup');
        $page = $this->input->post('page');
        $popup = popupForLoyalty($total);

        $lang = $this->session->userdata('site_lang');

        if($page == 'shippingAddress') {
            if ($lang == 'eng') {
                $title = 'You become one of us';
                $message = 'Hi <br> 
You become one of us and registered you in “Meduaf” Program
Click <a href="' . lang_base_url() . '/customer_service/medauf_program" target="_blank">here</a> for more information';
            } else {
                $title = 'صرت منا وفينا';
                $message = 'اهلا <br> 
صرت منا وفينا وسجلناك في برنامج "مضياف" 
اضغط <a href="' . lang_base_url() . '/customer_service/medauf_program" target="_blank"> هنا  </a>
لمعرفة تفاصيل اكثر
';
            }
        }else{

            $loDiscount = $popup;

            if($lang == 'eng'){
                $title = 'Meduaf program';
                $message = 'Hi <br> You have '.$loDiscount.' discount this time and every time ';
            }
            else{
                $title = 'برنامج مضياف';
                $message = 'اهلا<br>
طالع عمرك صار عندك خصم '. $loDiscount.' على مشترياتك هالمرة وكل مرة
';
            }
        }

        if($popup){
            $data['title'] = $title;
            $data['message'] = $message;
            $data['loyalty'] = true;
        }else{
            $data['loyalty'] = false;
        }
        echo json_encode($data);
        exit;
    }
}