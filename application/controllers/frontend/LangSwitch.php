<?php
class LangSwitch extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
    }

    function index() {
		$language = $this->input->post('lang');
		$url_get = $this->input->post('url_str');
        $this->session->set_userdata('site_lang', $language);
		$url_arr = explode('/', $url_get);
		$url_final=array();
		foreach($url_arr as $u)
		{
			if($u != 'en')
			{
				$url_final[] = $u;
			}
		}
		$url = implode('/', $url_final);
		$redirect_url['url'] = ($language == "eng") ? "en/".$url : $url;
        echo json_encode($redirect_url);
		exit;
    }
}