<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Career extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
        parent::__construct();
		$this->load->helper('url');
		$get_url_arr = explode('/', $_SERVER['REQUEST_URI']);
		if(!$this->session->userdata('site_lang'))
		{
			$this->session->set_userdata('site_lang','arb');
		}
		$this->lang->load("message",$this->session->userdata('site_lang'));
		$this->load->model('Model_career');
		$this->load->model('Model_career_image');
	    $this->load->model('Model_jobs');
		$this->load->model('Model_job_applied');
		
		//$res = checkLevels(2);
		//checkAuth($res);
    }
	
	public function action()
	{
		$messages = $this->lang->line('all');
		
		if(isset($_POST['form_type']))
		{

			switch($_POST['form_type'])
			{

				case 'save':
					$this->save();
					$job_id = $_POST['job_id'];
					if($job_id != 0)
					{
						$data['message'] = $messages['jobs_apply_success_message'];
					}
					else
					{
						$data['message'] = $messages['jobs_upload_cv_success_message'];
					}
					$data['error'] = 'false';
					$data['success'] = 1;
					echo json_encode($data);
					exit;
				break;
				
			}
		}	
		
	}
	
	public function index()
	{	
		$data = array();
		$data = $this->lang->line('all');
		$data['lang'] = $this->session->userdata('site_lang');
		$user_id = $this->session->userdata('user_id');
		$data['career'] = $this->Model_career->get('1', true);

		$active['active'] = '1';
		$data['jobs'] = $this->Model_jobs->getMultipleRows($active, true);
		//echo "<pre>"; print_r($data['jobs']); exit;
		$data['content'] = 'career/jobs';
		$data['class'] = 'jobs';
		$this->load->view('default',$data);	
		
	}

    public function career_open($id)
    {
        $data = array();
        $data = $this->lang->line('all');
        $data['lang'] = $this->session->userdata('site_lang');
        $user_id = $this->session->userdata('user_id');
        $data['career'] = $this->Model_career->get('1', true);

        $active['active'] = '1';
        $active['id'] = $id;
        $data['jobs'] = $this->Model_jobs->getsingleRow($active, true);

        $data['content'] = 'career/career_open';
        $data['class'] = 'career_open';
        $this->load->view('default',$data);

    }
	
	private function save()
	{
		$data = array();
		$html = '';
		$address_html = '';
		$message = $this->lang->line('all');
		$post_data = $this->input->post();
		$lang = $this->session->userdata('site_lang');

		$config = getCconfiguration();
		
		$keys[] =  'form_type';
		
		foreach($post_data as $key => $value)
		{
			
			if(!in_array($key,$keys))
			{
			 $data[$key] = $value;	
			}
		}

        $data['created_at'] = date('Y-m-d H:i:s');
		$data['cv'] = uploadFile('cv');

		if($lang == 'arb')
		{
			$body_style = 'style="direction:rtl"';
			$left = 'right';
			$right = 'left';
			$td_phone_style = ' unicode-bidi: plaintext;';
			
		}
		else
		{
			$body_style = '';
			$left = 'left';
			$right = 'right';
			$td_phone_style = '';
		}
		
		$insert_id = $this->Model_job_applied->save($data);
        if($insert_id > 0)
		{			
		$settings = getSettings();			
		$subject = $message['job_application'];			
		//$email_message = "New Job Application has been received";
		$to = $settings->admin_email;
		$emailData = array();
		unset($data['lang']);
		unset($data['created_at']);
		unset($data['job_id']);
		unset($data['lang']);
		$emailData['info'] = $data;	
		
		$emailData['message'] = $message['hi_label']." IOUD,<br><br> ".$message['job_application_received']." <br><br>";
		
		if($lang == "eng") {
			$view = $this->load->view('layouts/emails/eng_general_email',$emailData,TRUE);
		}else{
			$view = $this->load->view('layouts/emails/arb_general_email',$emailData,TRUE);
		}
		
		
		$adminEmail = $this->Model_settings->get('1');
		send_email_admin($subject, $view, $to);
		
		
		$emailData['message'] = $message['hi_label']." ".$data['name'].", <br> ".$message['job_application_user'];
		
		if($lang == "eng") {
			$view = $this->load->view('layouts/emails/eng_general_email',$emailData,TRUE);
		}else{
			$view = $this->load->view('layouts/emails/arb_general_email',$emailData,TRUE);
		}
			
		send_email_user($subject, $view,$data['email']);
		
			$job = '';
			if($data['job_id'] != 0)
			{
				$getjob = $this->Model_jobs->get($data['job_id'], true);
				$job = '<tr>
						  <td align="'.$left.'" valign="middle" style="font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; "><strong>'.$message['jobs_job'].'</strong></td>
						  <td align="'.$right.'" valign="middle" style="font-size:12px;font-weight: normal; color:#D80000; text-transform:uppercase; ">'.$getjob[$lang.'_title'].'</td>
					  </tr>';
			}
						
				
			//$admin =	str_replace('{email_heading}', $message['email_career_detail_message'], $html);
			//$admin =	str_replace('{admin_email}', $admin_temp, $admin);
			//htmlmail($config->admin_email, $message['email_career_subject'], $admin, 'developer@edesign.com.sa');
			
			//$applicant =	str_replace('HR', $data['name'], $html);			
			//$applicant =	str_replace('{email_heading}', $message['jobs_thank_you_message'], $applicant);
			//$applicant =	str_replace('{admin_email}', '', $applicant);
			//htmlmail($data['email'], $message['email_thank_you_career'], $applicant, 'developer@edesign.com.sa');
		}
		return $insert_id;
	}
	
	
}
