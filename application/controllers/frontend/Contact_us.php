<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Contact_us extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */



	public function __construct()

    {

        parent::__construct();

		

		$this->load->helper('url');

		$get_url_arr = explode('/', $_SERVER['REQUEST_URI']);

		if(!$this->session->userdata('site_lang'))

		{

			$this->session->set_userdata('site_lang','arb');

		}

		if(in_array('en', $get_url_arr))

		{

			$this->session->set_userdata('site_lang','eng');

		}

		else

		{

			$this->session->set_userdata('site_lang','arb');

		}

        $this->lang->load("message",$this->session->userdata('site_lang'));

	    $this->load->model('Model_contact_us');

	    $this->load->model('Model_city');

	    $this->load->model('Model_contact_us_feedback');
		$this->load->model('Model_settings');


    }

	

	public function index()

	{

		$data = array();

		$data = $this->lang->line('all');

		$data['lang'] = $this->session->userdata('site_lang');


		$data['contact_us'] = $this->Model_contact_us->get('1',true);

		$data['cities'] = $this->Model_city->getAll(true);

		//echo "<pre>"; print_r($data['cities']); exit;

		$data['content'] = 'contact_us/contact_us';

		$data['class'] = 'contact_us';



		$this->load->view('default',$data);	

		

	}


	public function save(){


		$response = array();
		$lang = $this->session->userdata('site_lang');

		$message = $this->lang->line('all');
	    $post_data = $this->input->post();

        $validation = $this->validation();


        if($validation) {
			
			$data['message'] = "Hi IOUD,<br><br> ".$message['contact_request_received']." <br><br>";
			
			$subject = $message['contact_request'].":";
			$data['info'] = $post_data;
			
			$view = "";
			
			if($lang == "eng") {
				$view = $this->load->view('layouts/emails/eng_general_email',$data,TRUE);
			}else{
				$view = $this->load->view('layouts/emails/arb_general_email',$data,TRUE);
			}
			
			
			$adminEmail = $this->Model_settings->get('1');
			send_email_admin($subject, $view, $adminEmail->admin_email);
			
			$subject = $message['contact_request'].":";
			
			$data['message'] = $message['hi_label']." ".$post_data['full_name']. ", <br><br>".$message['contact_request_submitted'];
			
			if($lang == "eng") {
					$view = $this->load->view('layouts/emails/eng_general_email',$data,TRUE);
			}else{
				$view = $this->load->view('layouts/emails/arb_general_email',$data,TRUE);
			}
			send_email_user($subject, $view,$post_data['email']);
			
            $this->Model_contact_us_feedback->save($post_data);
			
			$response['success'] = true;
			$response['reset'] = true;
			$response['message'] = $message['form_submitted_successfully'];

        }else{
			$response['success'] = false;
			$response['reset'] = false;
			$response['message'] = $message['form_submitted_failed'];

        }
		echo json_encode($response);
		exit;

    }



	/*private function send_email($message){ 
			
		$adminEmail = $this->Model_settings->get('1');
		$body = $message;
		$subject = "Contact us request: ";
		$headers  = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
		$headers .= 'From:<IOUD>' . "\r\n";
		if(mail($adminEmail->admin_email,$subject,$body,$headers)){
			return true;
		}else{
			return false;
		}
	 
	}*/




    private function validation()

    {

        $errors = array();

        $this->form_validation->set_error_delimiters('<p>', '</p>');



        $this->form_validation->set_rules('full_name', 'Name', 'required');

        $this->form_validation->set_rules('country', 'Country', 'required');
		 $this->form_validation->set_rules('city', 'City', 'required');

        $this->form_validation->set_rules('email', 'Email', 'required');

        $this->form_validation->set_rules('message', 'Message', 'required');



        if ($this->form_validation->run() == FALSE)

        {

            //return validation_errors();

            return false;

        }else

        {

            return true;

        }



    }

	

}