<?php
header('Access-Control-Allow-Origin: *');
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
        parent::__construct();
       	$get_url_arr = explode('/', $_SERVER['REQUEST_URI']);
		if(!$this->session->userdata('site_lang'))
		{
			$this->session->set_userdata('site_lang','arb');
		}
		$this->lang->load("message",$this->session->userdata('site_lang'));
	  	$this->load->model('Model_registered_users');
		$this->load->model('Model_registered_user_address');
		$this->load->model('Model_cart_user_address');
		$this->load->model('Model_temp_orders');
		$this->load->model('Model_settings');
		$this->load->model('Model_terms_conditions');
		$this->load->model('Model_customer_loyalty');
		$this->load->library('PayfortIntegration');

        // if maintenanc mode is on off
        maintenanceMode('redirect');
    }
	
	public function action()
	{
		$messages = $this->lang->line('all');
		
		if(isset($_POST['form_type']))
		{

			switch($_POST['form_type'])
			{

				case 'register':
				$this->save();
					/*$payment_type = $this->input->post('payment_type');
					$return = $this->checkDup();

					if($return == false) {

                        $this->session->set_flashdata('message', $messages['register_duplication']);
                        redirect($_SERVER['HTTP_REFERER']);
                    }

					$data['insert_id'] = $this->save();
					if($data['insert_id'] > 0)
					{
                        $this->session->set_flashdata('message', $messages['register_success']);
                        redirect($_SERVER['HTTP_REFERER']);
					}*/
				break;
				
				case 'update':
				$this->update();
				break;
				
				case 'passwordChange';
				$this->changePassword();
				break;
				
			}
		}	
		
	}

	public function index()
	{	
		$data = array();
		$data = $this->lang->line('all');
		
		$data['terms_text'] = $this->Model_terms_conditions->get('1');
		
		$data['lang'] = $this->session->userdata('site_lang');
		$user_id = $this->session->userdata('user_id');
		$data['user']	 = 	$this->Model_registered_users->get($user_id);
		$data['content'] = 'user/register';
		$data['class'] = 'register';
		$this->load->view('default',$data);	
		
	}
	public function profile()
	{	
		$data = array();
		$data = $this->lang->line('all');
		$data['lang'] = $this->session->userdata('site_lang');
		$user_id = $this->session->userdata('user_id');
		$data['user']	 = 	$this->Model_registered_users->get($user_id);
		$data['content'] = 'user/register';
		$data['class'] = 'register';
		$this->load->view('default',$data);	
		
	}
	private function save()
	{
		$data = array();
		$html = '';
		$address_html = '';
		$message = $this->lang->line('all');
		$post_data = $this->input->post();

        /*echo "<pre>"; print_r($post_data); exit;*/
		$lang = $this->session->userdata('site_lang');
		$config = getCconfiguration();
		
		$keys[] =  'form_type';
		$keys[] =  'street_name';
		$keys[] =  'property_no';
		$keys[] =  'address_city';
		$keys[] =  'more_details';
		$keys[] =  'address_country';
		$keys[] =  'address_district';
		$keys[] =  'address_po_box';
		$keys[] =  'address_zip_code';
		$keys[] =  'confirm_password';
		$keys[] =  'checkbox';
		$keys[] =  'id';

		foreach($post_data as $key => $value)
		{
			
			if(!in_array($key,$keys))
			{
			 $data[$key] = $value;	
			}
		}

		
		$data['password'] = sha1($data['password']);
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['guest'] = 0;
		
		$checkBy['email'] = $data['email'];
		
		$count = $this->Model_registered_users->getRowCount($checkBy);
		if($count > 0)
		{
			//$data['cookie_id'] = 0;
			$suc = $this->Model_registered_users->update($data, $checkBy);
			$singRow = $this->Model_registered_users->getSingleRow($checkBy);
			$insert_id = $singRow->id;
			$cookie_id = $singRow->cookie_id;
		}
		else
		{
		    if(get_cookie('u_id')) {
                $data['cookie_id'] = get_cookie('u_id');
            }
            else{
                $data['cookie_id'] = '0';
            }

			$insert_id = $this->Model_registered_users->save($data);
		}
		
		$this->session->set_userdata(array('regUserId' => $insert_id));
		/*$streets = $this->input->post('street_name');
		$property_no = $this->input->post('property_no');
		$address_city = $this->input->post('address_city');
		$more_details = $this->input->post('more_details');
		$address_country = $this->input->post('address_country');
		$address_district = $this->input->post('address_district');
		$address_po_box = $this->input->post('address_po_box');
		$address_zip_code = $this->input->post('address_zip_code');*/
		
		if($insert_id > 0)
		{
			$dataUser['user_id'] = $insert_id;
			$updated_by['user_id'] = $cookie_id; 
			$this->Model_cart_user_address->update($dataUser, $updated_by);
			$dataus['user'] = $insert_id;
			$updated_b['user'] = $cookie_id;
  			$this->Model_temp_orders->update($dataus, $updated_b);
			$this->session->set_userdata('user_id',$insert_id);
			$this->session->set_userdata('user_name',$data['first_name'].' '.$data['last_name']);
			$this->session->set_userdata('login',true);

			/*==========send emails===========*/
            $this->send_new_email($data['email'], $data);
            $this->send_admin_email($data);
            $this->send_user_loyalty($data['email']);
            /*==========================*/

			/* for every registered user the loyalty level will be saved as level 1*/
			$loyaltyData['user_id'] = $insert_id;
			$loyaltyData['loyalty_points'] = '0';
			$loyaltyData['loyalty_level'] = '1';
            $this->Model_customer_loyalty->save($loyaltyData);
            /*==============================================*/

            $result['insert_id'] = $insert_id;
            $result['success'] = true;
        }else{
            $result['success'] = false;
            $result['message'] = $message['form_submitted_failed'];
        }
        echo json_encode($result);
        exit();
	}
	
	private function update()
	{
		$data = array();
		$update_by = array();
		$html = '';
		$address_html = '';
		$message = $this->lang->line('all');
		$post_data = $this->input->post();

        /*echo "<pre>"; print_r($post_data); exit;*/
		$lang = $this->session->userdata('site_lang');
		$config = getCconfiguration();
		
		$keys[] =  'form_type';
		$keys[] =  'street_name';
		$keys[] =  'property_no';
		$keys[] =  'address_city';
		$keys[] =  'more_details';
		$keys[] =  'address_country';
		$keys[] =  'address_district';
		$keys[] =  'address_po_box';
		$keys[] =  'address_zip_code';
		$keys[] =  'password';
		$keys[] =  'confirm_password';
		$keys[] =  'checkbox';
		$keys[] =  'id';

		foreach($post_data as $key => $value)
		{
			
			if(!in_array($key,$keys))
			{
			 $data[$key] = $value;	
			}
		}

		$update_by['id'] = $post_data['id'];
		
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['guest'] = 0;
		
		$checkBy['email'] = $data['email'];
		
		
		$count = $this->Model_registered_users->getRowCount($checkBy);
		if($count > 0)
		{
			$data['cookie_id'] = 0;
			$suc = $this->Model_registered_users->update($data, $checkBy);
			$singRow = $this->Model_registered_users->getSingleRow($checkBy);
			$insert_id = $singRow->id;
			$cookie_id = $singRow->cookie_id;
		}
		else
		{
			$insert_id = $this->Model_registered_users->update($data,$update_by);
		}
		
		$this->session->set_userdata(array('regUserId' => $insert_id));

		if($insert_id > 0)
		{
			$dataUser['user_id'] = $insert_id;
			$updated_by['user_id'] = $cookie_id; 
			$this->Model_cart_user_address->update($dataUser, $updated_by);
			$dataus['user'] = $insert_id;
			$updated_b['user'] = $cookie_id;
  			$this->Model_temp_orders->update($dataus, $updated_b);
			$this->session->set_userdata('user_id',$post_data['id']);
			$this->session->set_userdata('user_name',$data['first_name'].' '.$data['last_name']);
			$this->session->set_userdata('login',true);

		}
		//emailTemplateRegister($message);
		$result['insert_id'] =$post_data['id'];
		echo json_encode($result);
		exit();
	}
	
	private function checkDup()
	{
		$post_data['email'] = $this->input->post('email');
		$post_data['guest'] = 0;
		$results = $this->Model_registered_users->getRowCount($post_data);
		if($results > 0)
		{
		    return false;
		}else{
		    return true;
        }
	}

	public function check_userEmail_exist($email){
		$email = $this->input->post('email');
		$results = $this->Model_registered_users->check_userEmail_exist($email);
		/*echo '<pre>'; print_r($results); exit();*/
		if($results > 0)
		{
			$result['exist'] = false;
            echo json_encode($result);
			
            exit();
		}else{
		    $result['exist'] = true;
            echo json_encode($result);
            exit();
        }
	}
	
	private function changePassword(){
		$data = array();
		$update_by['id'] = $user_id = $this->session->userdata('user_id');
		$old_password = sha1($this->input->post('pre_password'));
		$data['password'] = sha1($this->input->post('new_password'));
		
		$results = $this->Model_registered_users->get($user_id);
		$get_oldPassword = $results->password;
		
		if($get_oldPassword == $old_password){
			$this->Model_registered_users->update($data,$update_by);
			$result['change'] = true;
            echo json_encode($result);
            exit();
		}else{
			$result['change'] = false;
            echo json_encode($result);
            exit();
		}
	}
	public function check_password_exist(){
		$check_by = array();
		$check_by['id'] = $this->session->set_userdata('user_id');
		$check_by['password'] = sha1($this->input->post('old_password'));
	
		$counts = $this->Model_registered_users->getRowCount($check_by);

		if($counts > 0)
		{
			$result['exist'] = false;
            echo json_encode($result);
            exit();
		}else{
		    $result['exist'] = true;
            echo json_encode($result);
            exit();
        }
	}

	public function send_new_email($email,$data)
	{ 	
		$lang = $this->session->userdata('site_lang');
		$data['msg'] = $this->lang->line('all');

		$data['successMsg'] = $data['msg']['register_successfully_to_user'];
		
		$data['username'] = $data['first_name']." ".$data['last_name'];
		
		if($lang == "eng") {
			$view = $this->load->view('layouts/emails/eng_registration',$data,TRUE);
		}else{
			$view = $this->load->view('layouts/emails/arb_registration',$data,TRUE);
		}
		
		$body = $view;
		
		$subject = $data['msg']['register_user_subject'].":";
		/*$headers  = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
		$headers .= $data['msg']['register_user_from'].':<IOUDStore>' . "\r\n";*/
		if(htmlmail($email,$subject,$body,'info@ioud.ed.sa')){
			return true;
		}
 
     }

    public function send_user_loyalty($email)
    {
        $lang = $this->session->userdata('site_lang');

        if($lang == 'eng'){
            $title = 'You become one of us';
            $message = 'Hi <br> 
You become one of us and registered you in “Meduaf” Program
Click <a href="'.lang_base_url().'/customer_service/medauf_program">here</a> for more information';
        }
        else{
            $title = 'صرت منا وفينا';
            $message = 'اهلا <br> 
صرت منا وفينا وسجلناك في برنامج "مضياف" 
اضغط <a href="'.lang_base_url().'/customer_service/medauf_program"> هنا  </a>
لمعرفة تفاصيل اكثر
';
        }
        $data['title'] = $title;
        $data['message'] = $message;

        if($lang == "eng") {
            $view = $this->load->view('layouts/emails/eng_general_email',$data,TRUE);
        }else{
            $view = $this->load->view('layouts/emails/arb_general_email',$data,TRUE);
        }

        $body = $view;

        $subject = $title;
        /*$headers  = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
        $headers .= $data['msg']['register_user_from'].':<IOUDStore>' . "\r\n";*/
        if(htmlmail($email,$subject,$body)){
            return true;
        }

    }

	 private function send_admin_email($data){
		
		$lang = $this->session->userdata('site_lang');
	 	$data['msg'] = $this->lang->line('all');
		
	 	$admin_email = $this->Model_settings->get('1');

		$data['successMsg'] = $data['msg']['user_request_to_admin'];
		
		$data['username'] = "IOUD";

		if($lang == "eng") {
			$view = $this->load->view('layouts/emails/eng_registration',$data,TRUE);
		}else{
			$view = $this->load->view('layouts/emails/arb_registration',$data,TRUE);
		}
		$body = $view;
		
		
		$subject = $data['msg']['register_user_subject'].":";
		/*$headers  = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
		$headers .= $data['msg']['register_user_from'].':<IOUDStore>' . "\r\n";*/
		if(htmlmail($admin_email->admin_email,$subject,$body,'info@ioud.ed.sa')){
			 
			return true;
		}else{
			return false;
		}
		
		
	 }
	
}
