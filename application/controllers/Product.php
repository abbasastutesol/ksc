<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Product extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
		
        $get_url_arr = explode('/', $_SERVER['REQUEST_URI']);
		if(!$this->session->userdata('site_lang'))
		{
			$this->session->set_userdata('site_lang','eng');
		}
		if(in_array('en', $get_url_arr))
		{
			$this->session->set_userdata('site_lang','eng');
		}
		else
		{
			$this->session->set_userdata('site_lang','arb');
		}
		$this->lang->load("message",$this->session->userdata('site_lang'));
	    $this->load->model('Model_product');
	    $this->load->model('Model_category');
	    $this->load->model('Model_brand');
	    $this->load->model('Model_variant');
	    $this->load->model('Model_product_variant_value');
	    $this->load->model('Model_product_variant_group');
		$this->load->model('Model_image');
		$this->load->model('Model_variant_value');
		$this->load->model('Model_related_product');
		$this->load->model('Model_temp_orders');
		$this->load->model('Model_customer_questions');
		$this->load->model('Model_quota_request');
        $this->load->library("pagination");
		
    }
	
	public function index()
	{
		$data = array();
		$data = $this->lang->line('all');
		$data['lang'] = $this->session->userdata('site_lang');
        $prod_all['active'] = 1;
		$prod_all['category_status'] = 1;

        $total_rows = $this->Model_product->product_count();

        if(isset($_GET['per_page'])) {
            $page = $_GET['per_page'];
        }else{
            $page = 0;
        }
        $path = 'product';

        $paginate = productFrontPagination($path, $total_rows,12);

        $data['products'] = $this->Model_product->getProductWithCatBrand($prod_all,$paginate['per_page'],$page, true);

        $active_cate['active'] = 1;
        $active_brand['active'] = "On";
        $data['categories'] = $this->Model_category->getMultipleRows($active_cate, true);
        $data['brands'] = $this->Model_brand->getMultipleRows($active_brand,true);

        $data["links"] = $this->pagination->create_links();

        $data['limit'] = $paginate['per_page'];
        $data['count'] = $paginate['total_rows'];
        $data['content'] = 'product/products';
		$data['class'] = 'product';
		$this->load->view('default',$data);	
	}
    
	public function product_page($id)
	{

	    /*// here save the users views against each product
        saveProductViewedStatus($id);
        // end viewed saved functionality*/
		$data = array();
		$data = $this->lang->line('all');
		$fetch_by = array();
        $data['lang'] = $this->session->userdata('site_lang');

		$fetch_by['id'] = $id;
        $data['product'] = $this->Model_product->getSingleRow($fetch_by,true);
        $prod_all['active'] = 1;
        $prod_all['category_id'] = $data['product']['category_id'];
        $prod_all['id'] = $id;

        $data['content'] = 'product/product_page';
		$data['class'] = 'product_page';
		$this->load->view('default',$data);	
		
	}
	
	
	public function dailyOffers()
	{	
		$data = array();
		$data = $this->lang->line('all');
		$data['lang'] = $this->session->userdata('site_lang');
		$prod_all['daily_offer'] = 1;
		$prod_all['active'] = 1;
		$data['products'] = $this->Model_product->getMultipleRows($prod_all,true);
		$data['content'] = 'pages/daily_offers';
		$data['class'] = 'daily_offers';
		$this->load->view('default',$data);
	}

	public function search() {

        $data = array();
        $search = array();
        $data = $this->lang->line('all');
        $data['lang'] = $lang = $this->session->userdata('site_lang');
        $search['search'] = $this->input->get('search');
        $search['category_id'] = $this->input->get('category');
        $search['brand_id'] = $this->input->get('brand');

        $data['products'] = $this->Model_product->searchProduct($search,$data['lang']);

        $active_cate['active'] = 1;
        $active_brand['active'] = "On";
        $data['categories'] = $this->Model_category->getMultipleRows($active_cate, true);
        $data['brands'] = $this->Model_brand->getMultipleRows($active_brand,true);


        $data['search'] = 'search';
        $data['keyword'] = $search['search'];
        $data['category'] = $search['category_id'];
        $data['brand'] = $search['brand_id'];
		$data['content'] = 'product/products';
		$data['class'] = 'search';
		$this->load->view('default',$data);	
		
	}

	public function request_qouta(){

        $response = array();
        $message = $this->lang->line('all');

        $data['lang'] = $lang = $this->session->userdata('site_lang');
        $post_data = $this->input->post();

        $siteKey = $post_data["g-recaptcha-response"];

        $res = reCaptcha($siteKey);

        if ($res) {

            unset($post_data['g-recaptcha-response']);
            $save = $this->Model_quota_request->save($post_data);
            $post_data['country'] = getCoutryByCode($post_data['country']);
            $post_data['city'] = getCityById($post_data['city']);
            adminGeneralEmail($post_data,$message['user_quota_admin_subject'],$message['user_quota_admin_msg']);
            userGeneralEmail($post_data,$message['user_quota_user_subject'],$message['user_quota_user_msg']);

            $response['captcha'] = $res;
            $response['success'] = true;
            $response['message'] = $message['user_quota_user_subject'];

        } else {

            $response['captcha'] = $res;
            $response['success'] = false;
            if ($data['lang'] == 'eng') {
                $response['message'] = 'Please use the captcha!';
            } else {
                $response['message'] = 'نرجو التأكد من تعبئة خانة التحقق.';
            }

        }

        echo json_encode($response);
        exit;
    }

	
}
