<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ChairmanMessage extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
		
        $get_url_arr = explode('/', $_SERVER['REQUEST_URI']);
		if(!$this->session->userdata('site_lang'))
		{
			$this->session->set_userdata('site_lang','eng');
		}
		if(in_array('en', $get_url_arr))
		{
			$this->session->set_userdata('site_lang','eng');
		}
		else
		{
			$this->session->set_userdata('site_lang','arb');
		}
		$this->lang->load("message",$this->session->userdata('site_lang'));
	    $this->load->model('Model_product');
	    $this->load->model('Model_ChairManmessage');

			
		
    }
	
	public function message()
	{
		$data = array();
           
	    $data = $this->lang->line('all');
		$data['lang'] = $this->session->userdata('site_lang'); 
        $object = $this->Model_ChairManmessage->getmessage();
		
		$json  = json_encode($object);
        $data['chairmanMessage'] = json_decode($json, true);
		$data['page_id'] = $data['chairmanMessage'][0]['page_id'];
		$data['content'] = 'chairmanMessage/chairmanMessage';
		$data['class'] = 'chairmanMessage';
        
		$this->load->view('default',$data);	
		
	}

    
}
