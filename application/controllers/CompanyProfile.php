<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class CompanyProfile extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
		
        $get_url_arr = explode('/', $_SERVER['REQUEST_URI']);
		if(!$this->session->userdata('site_lang'))
		{
			$this->session->set_userdata('site_lang','eng');
		}
		if(in_array('en', $get_url_arr))
		{
			$this->session->set_userdata('site_lang','eng');
		}
		else
		{
			$this->session->set_userdata('site_lang','arb');
		}
		$this->lang->load("message",$this->session->userdata('site_lang'));
	    $this->load->model('Model_product');
	    $this->load->model('Model_company_profile');

			
		
    }
	
	public function profile()
	{ //echo 'ff'; exit;
		$data = array();

		 $data = $this->lang->line('all');
		$data['lang'] = $this->session->userdata('site_lang'); 
         $object = $this->Model_company_profile->getProfile();
		 $json  = json_encode($object);
        $data['companyProfile'] = json_decode($json, true);
		$data['page_id'] = $data['companyProfile'][0]['page_id'];
		$data['content'] = 'companyProfile/companyProfile';
		$data['class'] = 'companyProfile';
        
		$this->load->view('default',$data);	
		
	}

    
}
