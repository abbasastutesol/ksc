<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class VisionMission extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
		
        $get_url_arr = explode('/', $_SERVER['REQUEST_URI']);
		if(!$this->session->userdata('site_lang'))
		{
			$this->session->set_userdata('site_lang','eng');
		}
		if(in_array('en', $get_url_arr))
		{
			$this->session->set_userdata('site_lang','eng');
		}
		else
		{
			$this->session->set_userdata('site_lang','arb');
		}
		$this->lang->load("message",$this->session->userdata('site_lang'));
	    $this->load->model('Model_product');
	    $this->load->model('Model_mission_vision');

			
		
    }
	
	public function vision()
	{
		$data = array();
           
	    $data = $this->lang->line('all');
		$data['lang'] = $this->session->userdata('site_lang'); 
        $object = $this->Model_mission_vision->get('1');
		
		$json  = json_encode($object);
        $data['visionMission'] = json_decode($json, true);
		$data['page_id'] = $data['visionMission']['page_id'];
		$data['content'] = 'visionMission/visionMission';
		$data['class'] = 'visionMission';
        
		$this->load->view('default',$data);	
		
	}

    
}
