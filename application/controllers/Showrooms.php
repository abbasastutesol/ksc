<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Showrooms extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
		
        $get_url_arr = explode('/', $_SERVER['REQUEST_URI']);
		if(!$this->session->userdata('site_lang'))
		{
			$this->session->set_userdata('site_lang','eng');
		}
		if(in_array('en', $get_url_arr))
		{
			$this->session->set_userdata('site_lang','eng');
		}
		else
		{
			$this->session->set_userdata('site_lang','arb');
		}
		$this->lang->load("message",$this->session->userdata('site_lang'));
	    $this->load->model('Model_product');
	    $this->load->model('Model_showroom');

			
		
    }
	
	public function index()
	{
		$data = array();

	    $data = $this->lang->line('all');
		$data['lang'] = $this->session->userdata('site_lang');
        $data['showrooms'] = $this->Model_showroom->getAll(true);
		
        $data['page_id'] =  $data['showrooms'][0]['page_id'];
		$data['content'] = 'showrooms/showrooms';
		$data['class'] = 'offer';
        
		$this->load->view('default',$data);	
		
	}

    public function detail($id)
    {
        $data = array();

        $data = $this->lang->line('all');
        $data['lang'] = $this->session->userdata('site_lang');
        $data['showrooms'] = $this->Model_showroom->getAll(true);
        $data['locations'] = getShowroomsLocation($id,false);

        $data['content'] = 'showrooms/showrooms_detail';
        $data['class'] = 'offer';

        $this->load->view('default',$data);

    }
}
