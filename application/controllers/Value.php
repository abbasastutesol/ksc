<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Value extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
		
        $get_url_arr = explode('/', $_SERVER['REQUEST_URI']);
		if(!$this->session->userdata('site_lang'))
		{
			$this->session->set_userdata('site_lang','eng');
		}
		if(in_array('en', $get_url_arr))
		{
			$this->session->set_userdata('site_lang','eng');
		}
		else
		{
			$this->session->set_userdata('site_lang','arb');
		}
		$this->lang->load("message",$this->session->userdata('site_lang'));
	    $this->load->model('Model_product');
	    $this->load->model('Model_value');

			
		
    }
	
	public function values()
	{
		$data = array();
           
	    $data = $this->lang->line('all');
		$data['lang'] = $this->session->userdata('site_lang');
        $object = $this->Model_value->getData();
		
		$json  = json_encode($object);
        $data['values'] = json_decode($json, true);
	    $data['page_id'] = $data['values'][0]['page_id'];
		$data['content'] = 'values/values';
		$data['class'] = 'values';
        
		$this->load->view('default',$data);	
		
	}

    
}
