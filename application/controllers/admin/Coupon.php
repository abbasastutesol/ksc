<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Coupon extends CI_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_coupons');
		
	    $this->load->model('Model_coupon_categories');
	    $this->load->model('Model_coupon_products');
	    $this->load->model('Model_category');
	    $this->load->model('Model_product');
		checkAdminSession();
		//$res = checkLevels(2);
		//checkAuth($res);
        $this->user_rights_array = user_rights();
    }
	
	public function index()
	{
        if($this->user_rights_array['Coupon']['show_p'] == 1) {
            $data = array();
            if ($this->input->post('start_date') && $this->input->post('end_date')) {
                $start_date = explode('/', $this->input->post('start_date'));

                $data['start_date'] = $start_date[2] . '-' . $start_date[1] . '-' . $start_date[0];

                $end_date = explode('/', $this->input->post('end_date'));

                $data['end_date'] = $end_date[2] . '-' . $end_date[1] . '-' . $end_date[0];

                $data['coupons'] = $this->Model_coupons->getCouponOnFilter($data);
            } else {
                $data['coupons'] = $this->Model_coupons->getAll();
            }

            /*echo '<pre>';
            print_r($data);
            exit;*/
            $data['content'] = 'coupon/manage';
            $data['class'] = 'coupon';
            $this->load->view('template', $data);
        }else{
            show_404();
        }
	}
	
	public function action()
	{
		
		if(isset($_POST['form_type']))
		{
			switch($_POST['form_type'])
			{
				case 'save':
					$this->validation();
					$data['insert_id'] = $this->save();
					if($data['insert_id'] > 0)
					{
						$data['success'] = 'Coupon has been created successfully.';
						$data['error'] = 'false';
						$data['reset'] = 1;
						echo json_encode($data);
						exit;
					}
				break;
				case 'delete':
					
					if($this->deleteData())
					{
						$data['success'] = 'Coupon deleted successfully.';
						$data['error'] = 'false';
						echo json_encode($data);
						exit;
					}else
					{
						$data['success'] = 'false';
						$data['error'] = 'Coupon not deleted successfully. Please try again.';
						echo json_encode($data);
						exit;
					}
					
				break;
				case 'update':
					$this->validation();
					if($this->update())
					{
						$data['success'] = 'Coupon has been updated successfully.';
						$data['error'] = 'false';
						echo json_encode($data);
						exit;
					}else
					{
						$data['success'] = 'false';
						$data['error'] = 'Coupon has not been updated successfully.Please try again';
						echo json_encode($data);
						exit;
					}
										
				break;
				
			}
		}	
		
	}
	
	
	public function add()
	{
        if($this->user_rights_array['Coupon']['add_p'] == 1) {
            $data = array();
            $data['categories'] = $this->Model_category->getAll();

            $data['products'] = $this->Model_product->getAll();
            $data['content'] = 'coupon/add';
            $data['class'] = 'coupon';
            $this->load->view('template', $data);
        }else{
            show_404();
        }
	}
	
	public function edit($id)
	{
        if($this->user_rights_array['Coupon']['edit_p'] == 1) {
            $data = array();
            $data['coupon'] = $this->Model_coupons->get($id);

            $data['categories'] = $this->Model_category->getAll();

            $data['products'] = $this->Model_product->getAll();

            $couponCategories = $this->Model_coupon_categories->getMultipleRows(array('coupon_id' => $id));

            $coupCatArr = array();

            foreach ($couponCategories as $coupCat) {

                $coupCatArr[] = $coupCat->category_id;
            }

            $data['coup_category'] = $coupCatArr;

            $couponProducts = $this->Model_coupon_products->getMultipleRows(array('coupon_id' => $id));

            $coupPrdArr = array();

            foreach ($couponProducts as $coupPrd) {

                $coupPrdArr[] = $coupPrd->product_id;
            }

            $data['coup_product'] = $coupPrdArr;
            $data['content'] = 'coupon/edit';
            $data['class'] = 'coupon';
            $this->load->view('template', $data);
        }else{
            show_404();
        }
	}
	
	private function save()
	{
		$data = array();
		$post_data = $this->input->post();
		
		
		foreach($post_data as $key => $value)
		{
			
			if($key != 'form_type' && $key != 'product_id' && $key != 'category_id')
			{
				$data[$key] = $value;	
			}
		}
		
		$start_date = explode('/', $data['start_date']);
		
		$data['start_date'] = $start_date[2].'-'.$start_date[1].'-'.$start_date[0]; 
		
		$end_date = explode('/', $data['end_date']);
		
		$data['end_date'] = $end_date[2].'-'.$end_date[1].'-'.$end_date[0]; 
		
		$data['created_at'] = date('Y-m-d H:i:s');	
		$data['created_by'] = $this->session->userdata['user']['id'];
		$insert_id = $this->Model_coupons->save($data);
		
		$dataProd['coupon_id'] = $insert_id;
		
		$dataCat['coupon_id'] = $insert_id;
		
		$i = 0;
		
		foreach($post_data['product_id'] as $product_id)
		{
			$dataProd['product_id'] = $product_id;
			$dataProd['created_at'] = date('Y-m-d H:i:s');

			$this->Model_coupon_products->save($dataProd);
				
			$dataCat['category_id'] = $post_data['category_id'][$i];
			$dataCat['created_at'] = date('Y-m-d H:i:s');
			
			$this->Model_coupon_categories->save($dataCat);

			
			$i++;
		}

        // save logs here
        $logData = array(
            'type'=>    'add',
            'section'=> 'Coupon'
        );
        saveLogs($logData);

		return $insert_id;
	}
	
	private function update()
	{
		$data = array();
		$update_by = array();
		$post_data = $this->input->post();
		
		
		foreach($post_data as $key => $value)
		{
			
			if($key != 'form_type' && $key != 'id' && $key != 'product_id' && $key != 'category_id' && $key != 'edit_code')
			{
				$data[$key] = $value;	
			}
		}
		$id = $this->input->post('id');
		
		$start_date = explode('/', $data['start_date']);
		
		$data['start_date'] = $start_date[2].'-'.$start_date[1].'-'.$start_date[0]; 
		
		$end_date = explode('/', $data['end_date']);
		
		$data['end_date'] = $end_date[2].'-'.$end_date[1].'-'.$end_date[0]; 
		
		$data['updated_at'] = date('Y-m-d H:i:s');	
		$data['updated_by'] = $this->session->userdata['user']['id']; 			
		
		$updatedBy['id'] = $id;
		
		$update = $this->Model_coupons->update($data,$updatedBy);
		
		$delete_coupon_by['coupon_id'] = $id;
		$this->Model_coupon_products->delete($delete_coupon_by);
		$this->Model_coupon_categories->delete($delete_coupon_by);	
		
		$dataProd['coupon_id'] = $id;
		
		$dataCat['coupon_id'] = $id;
		
		foreach($post_data['product_id'] as $product_id)
		{
			$dataProd['product_id'] = $product_id;
			$dataProd['created_at'] = date('Y-m-d H:i:s');
			
			$this->Model_coupon_products->save($dataProd);
		}
		foreach($post_data['category_id'] as $category_id)
		{				
			$dataCat['category_id'] = $category_id;
			$dataCat['created_at'] = date('Y-m-d H:i:s');
			
			$this->Model_coupon_categories->save($dataCat);

		}


        // save logs here
        $logData = array(
            'type'=>    'update',
            'section'=> 'Coupon'
        );
        saveLogs($logData);

		return $update;
	}
	
	private function deleteData()
	{
		
		$delete = false;
		$delete_coupon_by['coupon_id'] = $this->input->post('id');
		$this->Model_coupon_products->delete($delete_coupon_by);
		$this->Model_coupon_categories->delete($delete_coupon_by);		
		$deleted_by['id'] = $this->input->post('id'); 
		$delete = $this->Model_coupons->delete($deleted_by);

        // save logs here
        $logData = array(
            'type'=>    'delete',
            'section'=> 'Coupon'
        );
        saveLogs($logData);

		return $delete;
		
	}
	
	private function validation()
	{
		    $errors = array();
			$error_message = "";
			$validation = true;
			if(spacesValidation($this->input->post('coupon_name')) == "") {
				
				$error_message .= "<p>The Coupon name field is required</p>";
				$validation = false;
			}
			if(spacesValidation($this->input->post('code')) == "") {
				
				$error_message .= "<p>The Code field is required</p>";
				$validation = false;
			}
			if(spacesValidation($this->input->post('type')) == "") {
				
				$error_message .= "<p>The Type field is required</p>";
				$validation = false;
			}
			if(spacesValidation($this->input->post('discount')) == "") {
				
				$error_message .= "<p>The Discount field is required</p>";
				$validation = false;
			}
            if(spacesValidation($this->input->post('free_shipping')) == "") {
                $error_message .= "<p>The Free shipping field is required</p>";
                $validation = false;
            }
            if(spacesValidation($this->input->post('start_date')) == "") {
                $error_message .= "<p>The Start date field is required</p>";
                $validation = false;
            }
            if(spacesValidation($this->input->post('end_date')) == "") {
                $error_message .= "<p>The End date field is required</p>";
                $validation = false;
            }
            if(spacesValidation($this->input->post('status')) == "") {
                $error_message .= "<p>The Status field is required</p>";
                $validation = false;
            }
			
			$countCoupon = $this->Model_coupons->getRowCount(array('code'=>$this->input->post('code')));
			$code_edit = $this->input->post('edit_code');
			
			if($countCoupon > 0 && $code_edit != $this->input->post('code')) {
				
				$error_message .= "<p>The Code already exist</p>";
				$validation = false;
			}
		
			if ($validation == false)
			{
				$errors['error'] = $error_message;
				$errors['success'] = 'false';
				echo json_encode($errors);
				exit;
			}else
			{
				return true;
			}
			
	}
	
    
}
