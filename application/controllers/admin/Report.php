<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Report extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->lang->load("message",$this->session->userdata('site_lang'));
        $this->load->helper("url");
        $this->load->library("pagination");
	    $this->load->model('Model_report');
	    $this->load->model('Model_registered_users');
        $this->load->model('Model_order_status');
        $this->load->model('Model_logs');

		checkAdminSession();
		$this->user_rights_array = user_rights();
    }

	public function orders()
	{

	    $this->userShowRights();

        $data = array();
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $group = $this->input->post('group');
        $status = $this->input->post('status');

        if($start_date != null) {
            $start_date = str_replace('/', '-', $start_date);
            $data['start_date'] = $start_date = date('Y-m-d', strtotime($start_date));
        }
        else {
            $data['start_date'] = $start_date = '';
        }
        if($end_date != null){
            $end_date = str_replace('/','-',$end_date);
            $data['end_date'] = $end_date = date('Y-m-d', strtotime($end_date));
        }
        else{

            $data['end_date'] = $end_date = '';
        }

        if ($group != null) {
            $data['group'] = $filter_group = $group;
        } else{
            $data['group'] = $filter_group = 'day';
        }
        if ($status != null) {
            $data['status'] =  $order_status = $status;
        } else {
            $data['status'] = $order_status = '';
        }

        $filter_data = array(
            'filter_date_start'	     => $start_date,
            'filter_date_end'	     => $end_date,
            'group'           => $filter_group,
            'status'           => $order_status
        );
        $data['orders'] = $this->Model_report->getSalesOrders($filter_data);
        $data['statuses'] = $this->Model_order_status->getAll();
        $data['content'] = 'admin/reports/order';
        $data['class'] = 'report_order';
        $this->load->view('template',$data);


    }

    public function shipping()
    {
        $this->userShowRights();

        $data = array();
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $group = $this->input->post('group');
        $status = $this->input->post('status');
        if ($start_date != null) {
            $start_date = str_replace('/', '-', $start_date);
            $data['start_date'] = $start_date = date('Y-m-d', strtotime($start_date));
        }
        else {
            $data['start_date'] = $start_date = '';
        }
        if ($end_date != null){
            $end_date = str_replace('/', '-', $end_date);
            $data['end_date'] = $end_date = date('Y-m-d', strtotime($end_date));
        }
        else{

            $data['end_date'] = $end_date = '';
        }

        if ($group != null) {
            $data['group'] = $filter_group = $group;
        } else{
            $data['group'] = $filter_group = 'day';
        }
        if ($status != null) {
            $data['status'] =  $order_status = $status;
        } else {
            $data['status'] = $order_status = '';
        }

        $filter_data = array(
            'filter_date_start'	     => $start_date,
            'filter_date_end'	     => $end_date,
            'group'           => $filter_group,
            'status'           => $order_status
        );

       $data['orders'] = $this->Model_report->getShippingOrders($filter_data);

        //echo $data['orders']; exit;
        $data['statuses'] = $this->Model_order_status->getAll();
        $data['content'] = 'admin/reports/shipping';
        $data['class'] = 'report_shipping';
        $this->load->view('template',$data);
    }

    public function returns()
    {
        $this->userShowRights();

        $data = array();
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $group = $this->input->post('group');

        if($start_date != null) {
            $start_date = str_replace('/', '-', $start_date);
            $data['start_date'] = $start_date = date('Y-m-d', strtotime($start_date));
        }else{
            $data['start_date'] = $start_date = '';
        }
        if($end_date != null){
            $end_date = str_replace('/','-',$end_date);
            $data['end_date'] = $end_date = date('Y-m-d', strtotime($end_date));
        }else{

            $data['end_date'] = $end_date = '';
        }
        if ($group != null) {
            $data['group'] = $filter_group = $group;
        } if($group == null || $group == '') {
            $data['group'] = $filter_group = 'day';
        }

        $filter_data = array(
            'filter_date_start'	     => $start_date,
            'filter_date_end'	     => $end_date,
            'filter_group'           => $filter_group,
            'group'           => $group
        );
        $data['returns'] = $this->Model_report->getSalesReturns($filter_data);

        $data['content'] = 'admin/reports/returns';
        $data['class'] = 'report_returns';
        $this->load->view('template',$data);
    }

    public function product_viewed()
    {
        $this->userShowRights();

        $data['viewed'] = array();
        $totalViews = $this->Model_report->getTotalViews();

        $results = $this->Model_report->getProductViewed();

        foreach ($results as $result) {

            if ($result['viewed']) {
                $percent = round($result['viewed'] / $totalViews * 100, 2);
            } else {
                $percent = 0;
            }

            $data['viewed'][] = array(
                'product_name'    => $result['product_name'],
                'cat_name'    => $result['cat_name'],
                'viewed'  => $result['viewed'],
                'percent' => $percent . '%'

            );
        }

        $data['content'] = 'admin/reports/viewed';
        $data['class'] = 'report_viewed';
        $this->load->view('template',$data);
    }

    public function purchased()
    {
        $this->userShowRights();

        $data = array();
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');

        if($start_date != null) {
            $start_date = str_replace('/', '-', $start_date);
            $data['start_date'] = $start_date = date('Y-m-d', strtotime($start_date));
        }else{
            $data['start_date'] = $start_date = '';
        }
        if( $end_date != null){
            $end_date = str_replace('/','-',$end_date);
            $data['end_date'] = $end_date = date('Y-m-d', strtotime($end_date));;
        }else{

            $data['end_date'] = $end_date = '';
        }

        $filter_data = array(
            'filter_date_start'	     => $start_date,
            'filter_date_end'	     => $end_date

        );

        $data['purchased'] = $this->Model_report->getPurchasedProduct($filter_data);
        //echo "<pre>"; print_r($data['purchased']); exit;

        $data['content'] = 'admin/reports/purchased';
        $data['class'] = 'report_purchased';
        $this->load->view('template',$data);
    }

    public function customer_orders()
    {

        $this->userShowRights();

        $data = array();
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $customer = $this->input->post('customer');
        $status = $this->input->post('status');

        if($start_date != null) {
            $start_date = str_replace('/', '-', $start_date);

            $data['start_date'] = $start_date = date('Y-m-d', strtotime($start_date));
        }else{
            $data['start_date'] = $start_date = '';
        }
        if($end_date != null){
            $end_date = str_replace('/','-',$end_date);
            $data['end_date'] = $end_date = date('Y-m-d', strtotime($end_date));
        }else{
            $data['end_date'] = $end_date = '';
        }

        if($customer == null || $customer == ''){
            $customer = '';
        }
        $data['customer_id'] = $customer;

        if ($status == null || $status == '') {
            $data['status'] =  $order_status = '';
        }else{
            $data['status'] =  $order_status = $status;
        }

        $filter_data = array(
            'filter_date_start'	     => $start_date,
            'filter_date_end'	     => $end_date,
            'status'           => $order_status,
            'customer'           => $customer
        );

        $data['cust_orders'] = $this->Model_report->getCustomerOrders($filter_data);
    //echo $data['cust_orders']; exit;
        $data['statuses'] = $this->Model_order_status->getAll();
        $data['customers'] = $this->Model_registered_users->getAll();
        $data['content'] = 'admin/reports/customer_order';
        $data['class'] = 'cust_order';
        $this->load->view('template',$data);
    }

    public function coupon()
    {
        $this->userShowRights();

        $data = array();
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');

        if($start_date != null ) {
            $start_date = str_replace('/', '-', $start_date);
            $data['start_date'] = $start_date = date('Y-m-d', strtotime($start_date));
        }else {
            $data['start_date'] = $start_date = '';
        }
        if($end_date != null) {
            $end_date = str_replace('/', '-', $end_date);
            $data['end_date'] = $end_date = date('Y-m-d', strtotime($end_date));
        }else{
            $data['end_date'] = $end_date = '';
        }

        $filter_data = array(
            'filter_date_start'	     => $start_date,
            'filter_date_end'	     => $end_date

        );

        $data['coupon'] = $this->Model_report->getCoupons($filter_data);
        //echo "<pre>"; print_r($data['coupon']); exit;

        $data['content'] = 'admin/reports/coupon';
        $data['class'] = 'report_coupon';
        $this->load->view('template',$data);
    }

    public function logs(){

        if($this->user_rights_array['Site Logs']['show_p'] == 1) {

            $data['logs'] = $this->Model_logs->getAll(false, 'desc');

            $data['content'] = 'admin/reports/logs';
            $data['class'] = 'logs';
            $this->load->view('template', $data);

        }else{
            show_404();
        }

    }

    private function userShowRights(){
        if($this->user_rights_array['Reports']['show_p'] == 1)
        {
        }else{
            show_404();
        }
    }

}
