<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Dashboard extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */



	public function __construct()

    {

        parent::__construct();

       

	    $this->load->model('Model_orders');

		$this->load->model('Model_return_order');

		$this->load->model('Model_registered_users');

		$this->load->model('Model_product');

		//$res = checkLevels(2);

		//checkAuth($res);

		checkAdminSession();
		$this->user_rights_array = user_rights();

    }

	

	public function index()

	{	
	
		/*if($this->user_rights_array['Dashboard']['show_p'] == 1)
		{*/

			$data = array();
	
			$data['orders_total'] = $this->Model_orders->ordersTotalAllData();

			$data['total_orders'] = $this->Model_orders->getMultipleRows(array('payment_order_status'=>'1'));


            $getData['order_status']  = '7';
            $getData['payment_order_status'] = '1';

			$data['return_orders'] = $this->Model_orders->getMultipleRows($getData);
			$data['customers'] = $this->Model_registered_users->getAll();
	
			$data['lowStock'] = $this->Model_product->lowStockProducts();

            $getBy['payment_order_status'] = '1';
			$data['latestOrder'] = $this->Model_orders->getMultipleRows($getBy,true,'DESC');
	

			//Charts for orders status
	
			
	
			$weeklyOrderChart = '';
	
			$monthlyOrderChart = '';
	
			$yearlyOrderChart = '';
	
			
	
			$weeklyOrder = $this->Model_orders->weeklyOrdersChart();
	
			$monthlyOrder = $this->Model_orders->monthlyOrdersChart();
	
			$yearlyOrder = $this->Model_orders->yearlyOrdersChart();
	
			foreach($weeklyOrder as $week)
	
			{
	
				$weeklyOrderChart .= "['".$week['created_date']."', ".$week['all'].", ".$week['pending'].", ".$week['cancelled'].", ".$week['completed']."],"; 
	
			}
	
			
	
			foreach($monthlyOrder as $month)
	
			{
	
				$monthlyOrderChart .= "['".date('M', strtotime($month['created_date'] . '01'))."', ".$month['all'].", ".$month['pending'].", ".$month['cancelled'].", ".$month['completed']."],"; 
	
				
	
			}
	
			
	
			foreach($yearlyOrder as $year)
	
			{
	
				$yearlyOrderChart .= "['".$year['created_date']."', ".$year['all'].", ".$year['pending'].", ".$year['cancelled'].", ".$year['completed']."],"; 
	
			}
	
			
	
			$data['weeklyOrderChart'] = rtrim($weeklyOrderChart, ',');	
	
			$data['monthlyOrderChart'] = rtrim($monthlyOrderChart, ',');
	
			$data['yearlyOrderChart'] = rtrim($yearlyOrderChart, ',');
	
			
	
			//end order charts
	
			
	
			//Charts for users
	
			
	
			$weeklyChart = '';
	
			$monthlyChart = '';
	
			$yearlyChart = '';
	
			
	
			$weekly = $this->Model_registered_users->registeredUsersChartWeek();
	
			$monthly = $this->Model_registered_users->registeredUsersChartMonth();
	
			$yearly = $this->Model_registered_users->registeredUsersChartYear();
	
			foreach($weekly as $week)
	
			{
	
				$weeklyChart .= "['".$week['created_date']."', ".$week['count']."],"; 
	
			}
	
			
	
			foreach($monthly as $month)
	
			{
	
				$monthlyChart .= "['".date('M', strtotime($month['created_date'] . '01'))."', ".$month['count']."],"; 
	
			}
	
			
	
			foreach($yearly as $year)
	
			{
	
				$yearlyChart .= "['".$year['created_date']."', ".$year['count']."],"; 
	
			}
	
			
	
			$data['weeklyChart'] = rtrim($weeklyChart, ',');	
	
			$data['monthlyChart'] = rtrim($monthlyChart, ',');
	
			$data['yearlyChart'] = rtrim($yearlyChart, ',');	
	
			
	
			//end users chart
	
			
	
			$data['content'] = 'admin/dashboard/manage';
	
			$data['class'] = 'dashboard';

			$this->load->view('template',$data);	
			
		/*} else {
			show_404();
		}*/


	}
    

	

}

