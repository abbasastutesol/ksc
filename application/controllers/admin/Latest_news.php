<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Latest_news extends CI_Controller {



	public function __construct()

    {

        parent::__construct();

       

	    $this->load->model('Model_latest_news');

		checkAdminSession();
		$this->user_rights_array = user_rights();



    }

	

	public function index()

	{
		if($this->user_rights_array['Latest News']['show_p'] == 1) 
		{
			$data = array();
	
			$data['latest_news']	 = 	$this->Model_latest_news->getAll();
	
			$data['content'] = 'customer_service/latest_news_view';
	
			$data['class'] = 'latest_news';
	
			$this->load->view('template',$data);	

		} else {
			show_404();
		}

	}

	

	public function action()

	{

		

		if(isset($_POST['form_type']))

		{



			switch($_POST['form_type'])

			{



                case 'save':

                    $this->validation();

                    $data['insert_id'] = $this->save();

                    if($data['insert_id'] > 0)

                    {

                        $data['success'] = 'News has been created successfully.';

                        $data['error'] = 'false';

                        $data['reset'] = 1;

                        echo json_encode($data);

                        exit;

                    }

                    else

                    {

                        $data['success'] = 'false';

                        $data['error'] = 'News has not been updated successfully.Please try again';

                        echo json_encode($data);

                        exit;

                    }

                    break;



				case 'update':



					//$this->validation();

					if($this->update())

					{

						$data['success'] = 'News has been updated successfully.';

						$data['error'] = 'false';

						echo json_encode($data);

						exit;

					}else

					{

						$data['success'] = 'false';

						$data['error'] = 'News has not been updated successfully.Please try again';

						echo json_encode($data);

						exit;

					}

				break;



                case 'delete':

                    if($this->deleteData()) {

                        $data['success'] = 'News deleted successfully.';

                        $data['error'] = 'false';

                        echo json_encode($data);

                        exit;

                    }

                    else{

                        $data['success'] = 'false';

                        $data['error'] = 'News has not been deleted successfully.Please try again';

                        echo json_encode($data);

                        exit;

                    }

                break;

				

			}

		}	

		

	}





    private function save()

    {

        $data = array();



        $post_data = $this->input->post();



        unset($post_data['form_type']);





        $file_name = '';



        if(isset($_FILES['news_image']) && $_FILES['news_image']['name'] != '')

        {

            $path = 'latest_news/';

            $file_name = uploadImage($path,'news_image');

            $post_data['news_image'] = $file_name;
            $fullPath = 'uploads/images/'.$path;
            compress_image($fullPath);

        }



        $post_data['created_at'] = date('Y-m-d H:i:s');





        $insert_id = $this->Model_latest_news->save($post_data);

        // save logs here
        $logData = array(
            'type'=>    'add',
            'section'=> 'Latest News'
        );
        saveLogs($logData);



        return $insert_id;

    }





    public function edit($id)

    {
		
		if($this->user_rights_array['Latest News']['edit_p'] == 1) 
		{

			$data = array();
	
			$data['news']	 = 	$this->Model_latest_news->get($id);
	
			$data['content'] 	 =  'customer_service/latest_news_edit';
	
			$data['class'] = 'product';
	
	
	
			$this->load->view('template',$data);
		} else {
			show_404();
		}

    }



	

	private function update()

	{

		$data = array();

		$post_data = $this->input->post();



		$keys[] =  'form_type';

		$keys[] =  'id';

		

		foreach($post_data as $key => $value)

		{

			

			if(!in_array($key,$keys))

			{

				$data[$key] = $value;	

			}

		}



		if(isset($_FILES['news_image']) && $_FILES['news_image']['name'] != '')

		{

			$path = 'latest_news/';

			$file_name = uploadImage($path,'news_image');

			$data['news_image'] = $file_name;
            $fullPath = 'uploads/images/'.$path;
            compress_image($fullPath);

		}  

		

		$data['created_at'] = date('Y-m-d H:i:s');



		$data['updated_at'] = date('Y-m-d H:i:s');

		

				

		$update_by['id'] = $this->input->post('id');

        //echo "<pre>"; print_r($update_by); exit;

		$update = $this->Model_latest_news->update($data,$update_by);

        // save logs here
        $logData = array(
            'type'=>    'update',
            'section'=> 'Latest News'
        );
        saveLogs($logData);

		

		return $update;

	}





    private function deleteData()

    {

        $deleted_by['id'] = $this->input->post('id');

        $getImage = $this->Model_latest_news->get($deleted_by['id']);



        unlink('uploads/images/latest_news/'.$getImage->news_image);



        $delete = $this->Model_latest_news->delete($deleted_by);

        // save logs here
        $logData = array(
            'type'=>    'delete',
            'section'=> 'Latest News'
        );
        saveLogs($logData);




        return $delete;



    }



	

	private function validation()

	{

		    $errors = array();

			$error_message = "";
			$validation = true;
			
			if(spacesValidation($this->input->post('eng_title')) == "") {
				
				$error_message .= "<p>The Eng title field is required</p>";
				$validation = false;
			}
			if(spacesValidation($this->input->post('arb_title')) == "") {
				
				$error_message .= "<p>The Arb title field is required</p>";
				$validation = false;
			}
			if(spacesValidation($this->input->post('eng_short_des')) == "") {
				
				$error_message .= "<p>The Eng description field is required</p>";
				$validation = false;
			}
			if(spacesValidation($this->input->post('arb_short_des')) == "") {
				
				$error_message .= "<p>The Arb description field is required</p>";
				$validation = false;
			}
			if(spacesValidation($this->input->post('eng_long_des')) == "") {
				
				$error_message .= "<p>The Eng long description field is required</p>";
				$validation = false;
			}
			if(spacesValidation($this->input->post('arb_long_des')) == "") {
				
				$error_message .= "<p>The Arb long description field is required</p>";
				$validation = false;
			}
         
			if (empty($_FILES['news_image']['name'])){
					$error_message .= "<p>The news image field is required</p>";
					$validation = false;
			}	

			if ($validation == false)

			{

				$errors['error'] = $error_message;

				$errors['success'] = 'false';

				echo json_encode($errors);

				exit;

			}else

			{

				return true;

			}

			

	}







    public function add()

    {
		if($this->user_rights_array['Latest News']['add_p'] == 1) 
		{
			$data = array();
	
			$data['content'] = 'customer_service/latest_news_add';
	
			$data['class'] = 'latest_news';
	
			$this->load->view('template',$data);

		} else {
			show_404();
		}

    }

	

	

	

}

