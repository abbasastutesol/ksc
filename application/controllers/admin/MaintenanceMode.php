<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class MaintenanceMode extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */



	public function __construct()

    {

        parent::__construct();

       

	    $this->load->model('Model_maintenance_mode');

		checkAdminSession();
		$this->user_rights_array = user_rights();


		//$res = checkLevels(2);

		//checkAuth($res);

    }

	

	public function index()

	{
		if($this->user_rights_array['Maintenance Mode']['show_p'] == 1) 
		{

			$data = array();
	
			$data['maintenance']	 = 	$this->Model_maintenance_mode->get('1');
	
			$data['content'] = 'maintenance/edit';
	
			$data['class'] = 'maintenance'; 
	
			$this->load->view('template',$data);	

		} else {
			show_404();
		}

	}

	

	public function action()

	{

		

		if(isset($_POST['form_type']))

		{



			switch($_POST['form_type'])

			{

				case 'update':



					$this->validation();

					if($this->update())

					{

						$data['success'] = 'Maintenance has been updated successfully.';

						$data['error'] = 'false';

						echo json_encode($data);

						exit;

					}else

					{

						$data['success'] = 'false';

						$data['error'] = 'Maintenance has not been updated successfully.Please try again';

						echo json_encode($data);

						exit;

					}

										



				break;

				

			}

		}	

		

	}

	

	private function update()

	{

		$data = array();

		$post_data = $this->input->post();

		$keys[] = 'form_type';
		$keys[] = 'id';		
		
		foreach($post_data as $key => $value)
		{
		

			if(!in_array($key, $keys))

			{

				$data[$key] = $value;	

			}

		}		

		
		if($data['active'])
		{
			$data['active'] = 'on';
		}
		else
		{
			$data['active'] = 'off';
		}
		

		$data['created_at'] = date('Y-m-d H:i:s');

		$data['created_by'] = $this->session->userdata['user']['id']; 

		$data['updated_at'] = date('Y-m-d H:i:s');

		$update_by['id'] = $this->input->post('id');		

		$update = $this->Model_maintenance_mode->update($data,$update_by);


        // save logs here
        $logData = array(
            'type'=>    'update',
            'section'=> 'Maintenance Mode'
        );
        saveLogs($logData);
		

		return $update;

	}

	

	private function validation()

	{

		    $errors = array();

			$this->form_validation->set_error_delimiters('<p>', '</p>');
        
			$this->form_validation->set_rules('eng_title', 'Eng Title', 'required');
			$this->form_validation->set_rules('arb_title', 'Arb Title', 'required');

			$this->form_validation->set_rules('eng_content', 'Eng Content', 'required');

			$this->form_validation->set_rules('arb_content', 'Arb Content', 'required');

			//$this->form_validation->set_rules('eng_content', 'English Content', 'required');

			//$this->form_validation->set_rules('arb_content', 'Arabic Content', 'required');

			

			

			if ($this->form_validation->run() == FALSE)

			{

				$errors['error'] = validation_errors();

				$errors['success'] = 'false';

				echo json_encode($errors);

				exit;

			}else

			{

				return true;

			}

			

	}


}

