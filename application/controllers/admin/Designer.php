<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Designer extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
        parent::__construct();
       
	   	$this->load->model('Model_designer');
	    $this->load->model('Model_design_with_us');
		$this->load->model('Model_designer_portfolio');
		checkAdminSession();
		//$res = checkLevels(2);
		//checkAuth($res);
    }
	
	public function action()
	{
		
		if(isset($_POST['form_type']))
		{

			switch($_POST['form_type'])
			{

				case 'save':
					$this->validation();
					$data['insert_id'] = $this->save();
					if($data['insert_id'] > 0)
					{
						$data['success'] = 'Designer has been saved successfully.';
						$data['error'] = 'false';
						$data['reset'] = 1;
						echo json_encode($data);
						exit;
					}
				break;

				case 'delete':
					
					if($this->deleteData())
					{
						$data['success'] = 'Designer deleted successfully.';
						$data['error'] = 'false';
						echo json_encode($data);
						exit;
					}else
					{
						$data['success'] = 'false';
						$data['error'] = 'Designer not deleted successfully. Please try again.';
						echo json_encode($data);
						exit;
					}
					

				break;

				case 'update':

					$this->validation();
					if($this->update())
					{
						$data['success'] = 'Designer has been updated successfully.';
						$data['error'] = 'false';
						echo json_encode($data);
						exit;
					}else
					{
						$data['success'] = 'false';
						$data['error'] = 'Designer has not been updated successfully.Please try again';
						echo json_encode($data);
						exit;
					}
										

				break;
				
				case 'deleteDesigner':
					
					if($this->deleteDataDesigner())
					{
						$data['success'] = 'Application deleted successfully.';
						$data['error'] = 'false';
						echo json_encode($data);
						exit;
					}else
					{
						$data['success'] = 'false';
						$data['error'] = 'Application not deleted successfully. Please try again.';
						echo json_encode($data);
						exit;
					}
					

				break;
				
			}
		}	
		
	}
	
	public function index()
	{
		$data = array();
		$data['designers']	 = 	$this->Model_designer->getAll();
		$data['content'] = 'designers/designers_list';
		$data['class'] = 'designers'; 
		$this->load->view('template',$data);	
		
	}
	
	public function applications()
	{
		$data = array();
		$data['designers']	 = 	$this->Model_design_with_us->getAll(false, 'desc');
		$data['content'] = 'designers/manage';
		$data['class'] = 'designers_app'; 
		$this->load->view('template',$data);	
		
	}
	
	public function view($id)
	{
		$data = array();

		$data['designer']	 = 	$this->Model_design_with_us->get($id);	
		$fetch_by['designer_id'] = $id;	
		$data['designer_portfolio']	 = 	$this->Model_designer_portfolio->getMultipleRows($fetch_by);
		$data['content'] = 'designers/view';
		$data['class'] = 'designers_app'; 
		$this->load->view('template',$data);
		
	}
	
	public function add()
	{
		$data = array();
		$data['content'] = 'designers/add';
		$data['class'] = 'designers'; 
		$this->load->view('template',$data);	
		
	}
	
	public function edit($id)
	{
		$data = array();
		//$data['categories']  =  $this->Model_category->getAll();
		$data['designer']	 = 	$this->Model_designer->get($id);
		$data['content'] 	 =  'designers/edit';
		$data['class'] = 'designers';
		$this->load->view('template',$data);
	}
	
	private function save()
	{
		$data = array();
		$post_data = $this->input->post();
		
		
		foreach($post_data as $key => $value)
		{
			
			if($key != 'form_type' && $key != 'category_id')
			{
				$data[$key] = $value;	
			}
		}
		
		$data['categories'] = implode(',',$this->input->post('category_id'));
		
		$file_name='';
		if($_FILES['image'])
		{
			$path = 'designer/';
			$file_name = uploadImage($path);	
		}  
		
		$data['image'] = $file_name;
		
		$data['created_at'] = date('Y-m-d H:i:s');
		
		
		$data['created_by'] = $this->session->userdata['user']['id']; 
		
				
		$insert_id = $this->Model_designer->save($data);
		return $insert_id;
	}
	
	private function update()
	{
		$data = array();
		$update_by = array();
		$post_data = $this->input->post();
		
		
		foreach($post_data as $key => $value)
		{
			
			if($key != 'form_type' && $key != 'id' && $key != 'category_id')
			{
				$data[$key] = $value;	
			}
		}
		
		$data['categories'] = implode(',',$this->input->post('category_id'));
		
		if(isset($_FILES['image']) && $_FILES['image']['name'] != '')
		{
			
			$path = 'designer/';
			$file_name = uploadImage($path);
			$data['image'] = $file_name;
		}  		
		
		$data['updated_at'] = date('Y-m-d H:i:s');
		
		
		$data['updated_by'] = $this->session->userdata['user']['id']; 
		
		$update_by['id'] = $this->input->post('id');		
		$update = $this->Model_designer->update($data,$update_by);
		return $update;
	}
	
	private function deleteData()
	{
		
		$delete = false;
		
		$deleted_by['id'] = $this->input->post('id'); 
		$delete = $this->Model_designer->delete($deleted_by);
		return $delete;
		
	}
	
	private function deleteDataDesigner()
	{
		
		$delete = false;
		
		$deleted_by['id'] = $this->input->post('id'); 
		$delete = $this->Model_design_with_us->delete($deleted_by);
		return $delete;
		
	}
	
	
	
	private function validation()
	{
		    $errors = array();
			$this->form_validation->set_error_delimiters('<p>', '</p>');
           
			$this->form_validation->set_rules('eng_name', 'Eng Name', 'required');
			$this->form_validation->set_rules('arb_name', 'Arb Name', 'required');
			
			
			if ($this->form_validation->run() == FALSE)
			{
				$errors['error'] = validation_errors();
				$errors['success'] = 'false';
				echo json_encode($errors);
				exit;
			}else
			{
				return true;
			}
			
	}
	
	
	
}
