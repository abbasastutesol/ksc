<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class CeoMessage extends CI_Controller {



	public function __construct()

    {

        parent::__construct();

       

	    $this->load->model('Model_contact_us');
		$this->load->model('Model_contact_us_feedback');

		$this->load->model('Model_page');
		$this->load->model('Model_CeoMessage');

		/* checkAdminSession();
		
		$this->user_rights_array = user_rights(); */

		//$res = checkLevels(2);

		//checkAuth($res);

    }

	

	public function index()

	{
	

			$data = array();
			
	            
			$data['record']	 = 	$this->Model_CeoMessage->getmessage();
            
			$data['content'] = 'aboutus/CeoMessage';
	
			$data['class'] = 'CeoMessage';
	
			$this->load->view('template',$data);	

	}

	

	public function action()

	{

		

		if(isset($_POST['form_type']))

		{



			switch($_POST['form_type'])

			{
               
				case 'update':



					$this->validation();

					if($this->update())

					{

						$data['success'] = 'Record has been updated successfully.';

						$data['error'] = 'false';

						echo json_encode($data);

						exit;

					}else

					{

						$data['success'] = 'false';

						$data['error'] = 'Record has not been updated successfully.Please try again';

						echo json_encode($data);

						exit;

					}

										



				break;
                
				

			}

		}	

		

	}

	
         public function edit_record($id)

	{         
		    $data['record'] = $this->Model_showroom->getSpecficData($id);
			
			$data['cities'] = getCity('Saudi Arabia','');
			
			$data['content'] = 'admin/show_room/showroom_edit';
	
			$data['class'] = 'show_room';
			
			$this->load->view('template',$data);	
		
	}
	
	private function update()

	{

		$data = array();

		$post_data = $this->input->post();



		$keys[] =  'form_type';

		$keys[] =  'id';

		

		foreach($post_data as $key => $value)

		{

			

			if(!in_array($key,$keys))

			{

				$data[$key] = $value;	

			}

		}

		

		if(isset($_FILES['image']) && $_FILES['image']['name'] != '')

		{

			

			$path = 'images/';

					$file_name = uploadImage($path);
					$data['banner_image'] = $file_name;
					$fullPath = 'assets/frontend/'.$path;
                      compress_image($fullPath);

		}  

		

		$data['created_at'] = date('Y-m-d H:i:s');

		$data['updated_by'] = $this->session->userdata['user']['id']; 

		$data['updated_at'] = date('Y-m-d H:i:s');

		
				$tpl_name = $this->input->post('tpl_name');

		$update_by['id'] = $this->input->post('id');
		$update_page['id'] = $this->input->post('page_id');
            //print_r($data); exit;

			$id = save_meta_data($data,$tpl_name);
		$value = unset_meta_date($data);
	     
		   
		$update = $this->Model_CeoMessage->update($value,$update_by); 
        
        // save logs here
        $logData = array(
            'type'=>    'update',
            'section'=> 'ceomessage'
        );
        saveLogs($logData);
		

		return $update;

	}

		

	private function validation()

	{

		    $errors = array();

			$this->form_validation->set_error_delimiters('<p>', '</p>');

           

			$this->form_validation->set_rules('eng_title', 'Eng title', 'required');

			$this->form_validation->set_rules('arb_title', 'Arb title', 'required');

			$this->form_validation->set_rules('eng_description', 'English Description', 'required');

			$this->form_validation->set_rules('arb_description', 'Arabic Description', 'required');
			
			$this->form_validation->set_rules('eng_name', 'English Name office', 'required');

			$this->form_validation->set_rules('arb_name', 'Arabic Name office', 'required');
			
			$this->form_validation->set_rules('eng_position', 'English Position', 'required');

			$this->form_validation->set_rules('arb_position', 'Arabic Position', 'required');

			

			

			if ($this->form_validation->run() == FALSE)

			{

				$errors['error'] = validation_errors();

				$errors['success'] = 'false';

				echo json_encode($errors);

				exit;

			}else

			{

				return true;

			}

			

	}
	
	

	
	public function showroom_delete(){
	
		$delete = false;
		
		$deleted_by['id'] = $this->input->post('id'); 

		$delete = $this->Model_showroom->delete($deleted_by);
		
		if($delete){

            // save logs here
            $logData = array(
                'type'=>    'delete',
                'section'=> 'showroom'
            );
            saveLogs($logData);

			$data['success'] = 'showroom deleted successfully.';
	
			$data['error'] = 'false';
	
			echo json_encode($data);
	
			exit;

		}else{

			$data['success'] = 'false';
	
			$data['error'] = 'showroom not deleted successfully. Please try again.';
	
			echo json_encode($data);
	
			exit;
		}
	}
    
	

}

