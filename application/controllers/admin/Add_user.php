<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Add_user extends CI_Controller
{
	public $user_rights_array;
	
    public function __construct()
    {
		parent::__construct();
        $this->load->model('Model_user_rights');
        $this->load->model('Model_admin_roles');
        $this->load->model('Model_page_rights');
        $this->load->model('Model_users'); 
        $this->load->model('Central_model'); 
        checkAdminSession();
		$this->user_rights_array = user_rights();
    }
    public function index()
    {
		if($this->user_rights_array['Add User']['show_p'] == 1) 
		{
			$data              = array();
			$data['UserRoles'] = $this->Model_users->getAll();
			$data['content']   = 'add_user/manage';
			$data['class']     = 'add_user';
			$this->load->view('template', $data); 
		} else {
			show_404();
		}
    }
    public function view_location()
    {
        $data                   = array();
        $id                     = $this->uri->segment(4);
        $data['shopper_driver'] = $this->Model_shopper_driver->get($id);
        $data['content']        = 'shopper_driver/view_location';
        $data['class']          = 'shopper_driver';
        $this->load->view('template', $data);
    }
    public function action()
    {
        if (isset($_POST['form_type'])) {
            switch ($_POST['form_type']) {
                case 'save':
                    $this->validation();
                    $data['insert_id'] = $this->save();
                    if ($data['insert_id'] > 0) {
                        $data['success'] = 'User created successfully.';
                        $data['error']   = 'false';
                        $data['reset']   = 1;
                        echo json_encode($data);
                        exit;
                    }
                    break;
                case 'delete':
                    if ($this->deleteData()) {
                        $data['success'] = 'User deleted successfully.';
                        $data['error']   = 'false';
                        echo json_encode($data);
                        exit;
                    } else {
                        $data['success'] = 'false';
                        $data['error']   = 'User not deleted successfully. Please try again.';
                        echo json_encode($data);
                        exit;
                    }
                    break;
                case 'update':
					$this->validation($this->input->post('email'));
                    if ($this->update()) {
                        $data['success'] = 'User has been updated successfully.';
                        $data['error']   = 'false';
                        echo json_encode($data);
                        exit;
                    } else {
                        $data['success'] = 'false';
                        $data['error']   = 'User has not been updated successfully.Please try again';
                        echo json_encode($data);
                        exit;
                    }
                    break;
                case 'update_status':
                    if ($this->update_status()) {
                        $data['success'] = 'User has been updated successfully.';
                        $data['error']   = 'false';
                        echo json_encode($data);
                        exit;
                    } else {
                        $data['success'] = 'false';
                        $data['error']   = 'User has not been updated successfully.Please try again';
                        echo json_encode($data);
                        exit;
                    }
                    break;
            }
        }
    }
    public function add()
    {
		if($this->user_rights_array['Add User']['add_p'] == 1) 
		{
			$data              = array();
			$data['all_roles'] = $this->Central_model->select_all_array('admin_roles', array('is_admin' => 0));
			$data['content']   = 'add_user/add';
			$data['class']     = 'add_user';
			$this->load->view('template', $data);
		} else {
			show_404();
		}
    }
    public function edit($id)
    {
		if($this->user_rights_array['Add User']['edit_p'] == 1) 
		{
			$data              = array();
			$data['user']      = $this->Central_model->first("users", array('id' => $id));
			$data['all_roles'] = $this->Central_model->select_all_array('admin_roles', array('is_admin' => 0));
			$data['content']   = 'add_user/edit';
			$data['class']     = 'add_user';
			$this->load->view('template', $data);
		} else {
			show_404();
		}
    }
    private function save()
    {
        $data      = array();
        $post_data = $this->input->post();
		$row = $this->Central_model->select_max_field("admin_roles", array('id' => $this->input->post('user_type')), 'is_admin');
		if($row->is_admin == 0) {
			$post_data['password'] = md5($post_data['password']);
			foreach ($post_data as $key => $value) {
				if ($key != 'form_type') {
					$data[$key] = $value;
				}
			}
			$insert_id = $this->Model_users->save($data);

            // save logs here
            $logData = array(
                'type'=>    'add',
                'section'=> 'Add User'
            );
            saveLogs($logData);

			return $insert_id;
		}
    }
    private function update()
    {
        $data      = array();
        $update_by = array();
		$row = $this->Central_model->select_max_field("admin_roles", array('id' => $this->input->post('user_type')), 'is_admin');
		if($row->is_admin == 0) {
			$id        = $this->input->post('id');
			$row = $this->Central_model->select_max_field("users", array('id' => $id), 'is_admin');
			if($row->is_admin == 0) {
				$post_data = $this->input->post();
				$post_data['password'] = md5($post_data['password']);
				foreach ($post_data as $key => $value) {
					if ($key != 'form_type' && $key != 'id') {
						$data[$key] = $value;
					}
				}
				$data['updated_at'] = date('Y-m-d H:i:s');
				$update             = $this->Central_model->update('users', $data, 'id', $id);

                // save logs here
                $logData = array(
                    'type'=>    'update',
                    'section'=> 'Add User'
                );
                saveLogs($logData);

				return $update;
			}
		}
    }
    private function deleteData()
    {
        $delete           = false;
        $deleted_by['id'] = $this->input->post('id');
        $delete           = $this->Model_users->delete($deleted_by);

        // save logs here
        $logData = array(
            'type'=>    'delete',
            'section'=> 'Add User'
        );
        saveLogs($logData);

        return $delete;
    }
    private function validation($email= Null)
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<p>', '</p>');
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required'.($email ? '|callback_check_email['.$this->input->post('id').']' : '|is_unique[users.email]'));
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('user_type', 'Role', 'required');
        if ($this->form_validation->run() == FALSE) {
            $errors['error']   = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }
    }
	
	public function check_email($email, $id)
	{
		$return_value = $this->Central_model->count_rows("users", array('email' => $email) , array('id' => $id));
		if ($return_value > 0)
		{
			//set Error message.
			$this->form_validation->set_message('check_email', 'Sorry, This email is already used by another user please select another one');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
}
