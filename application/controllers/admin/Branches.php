<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Branches extends CI_Controller {



    public function __construct()

    {

        parent::__construct();



        $this->load->model('Model_branches');
		checkAdminSession();
		$this->user_rights_array = user_rights();



    }



    public function index()

    {
		if($this->user_rights_array['Branches']['show_p'] == 1) 
		{
			$data = array();
	
			$data['branches']	 = 	$this->Model_branches->getAll();
	
			$data['content'] = 'branches/view';
	
			$data['class'] = 'branches';
	
			$this->load->view('template',$data);
		} else {
			show_404();
		}


    }



    public function add()

    {
		
		if($this->user_rights_array['Branches']['add_p'] == 1) 
		{

			$data = array();
	
			$data['content'] = 'branches/add';
	
			$data['class'] = 'branches';
	
			$this->load->view('template',$data);

		} else {
			show_404();
		}

    }





    public function action()

    {



        if(isset($_POST['form_type']))

        {



            switch($_POST['form_type'])

            {



                case 'save':
					$this->validation();
                    if($this->save()) {



                        $data['success'] = 'Branch has been saved successfully.';

                        $data['error'] = 'false';

                        echo json_encode($data);

                        exit;

                    }else{

                        $data['success'] = 'false';

                        $data['error'] = 'Branch has not been saved successfully.Please try again';

                        echo json_encode($data);

                        exit;



                    }



                    break;



                case 'update':



                    $this->validation();

                    if($this->update())

                    {

                        $data['success'] = 'Branch has been updated successfully.';

                        $data['error'] = 'false';

                        echo json_encode($data);

                        exit;

                    }else

                    {

                        $data['success'] = 'false';

                        $data['error'] = 'Branch has not been updated successfully.Please try again';

                        echo json_encode($data);

                        exit;

                    }





                    break;



                case 'delete':

                    if($this->delete()) {



                        $data['success'] = 'Branch has been deleted successfully.';

                        $data['error'] = 'false';

                        echo json_encode($data);

                        exit;

                    }else{

                        $data['success'] = 'false';

                        $data['error'] = 'Branch has not been deleted successfully.Please try again';

                        echo json_encode($data);

                        exit;



                    }



                    break;



            }

        }



    }



    private function save(){

        $post_data = $this->input->post();

        unset($post_data['form_type']);

        $returned_id = $this->Model_branches->save($post_data);



        if($returned_id){

            // save logs here
            $logData = array(
                'type'=>    'add',
                'section'=> 'Location'
            );
            saveLogs($logData);

            return true;

        }else{

            return false;

        }

    }



    private function update()

    {

        $data = array();

        $post_data = $this->input->post();



        $keys[] =  'form_type';

        $keys[] =  'id';



        foreach($post_data as $key => $value)

        {



            if(!in_array($key,$keys))

            {

                $data[$key] = $value;

            }

        }



        $update_by['id'] = $this->input->post('id');



        $update = $this->Model_branches->update($data,$update_by);


        // save logs here
        $logData = array(
            'type'=>    'update',
            'section'=> 'Location'
        );
        saveLogs($logData);

        return $update;

    }





    public function edit($id)

    {
		if($this->user_rights_array['Branches']['edit_p'] == 1) 
		{

			$data = array();
	
			$data['branch'] = $this->Model_branches->get($id);
	
			$data['content'] = 'branches/edit';
	
			$data['class'] = 'branches';
	
			$this->load->view('template',$data);

		} else {
			show_404();
		}

    }



    private function delete(){



        $deleted_by['id'] = $this->input->post('id');

        $delete = $this->Model_branches->delete($deleted_by);

        if($delete){

            // save logs here
            $logData = array(
                'type'=>    'delete',
                'section'=> 'Location'
            );
            saveLogs($logData);

            return true;

        }else{

            return false;

        }



    }


    private function validation()

    {

        $errors = array();
		
		$error_message = "";
		$validation = true;
		
		if(spacesValidation($this->input->post('eng_name')) == "") {
			
			$error_message .= "<p>The Eng name field is required</p>";
			$validation = false;
		}
		if(spacesValidation($this->input->post('arb_name')) == "") {
			$error_message .= "<p>The Arb name field is required</p>";
			$validation = false;
		}

		/*if(spacesValidation($this->input->post('phone')) == "") {
			$error_message .= "<p>The phone field is required</p>";
			$validation = false;
		}
		if(spacesValidation($this->input->post('fax')) == "") {
			$error_message .= "<p>The fax field is required</p>";
			$validation = false;
		}
		if(spacesValidation($this->input->post('email')) == "") {
			$error_message .= "<p>The email field is required</p>";
			$validation = false;
		}*/
		if(spacesValidation($this->input->post('lat_long')) == "") {
			$error_message .= "<p>The lat long field is required</p>";
			$validation = false;
		}

        if ($validation == false) {
            $errors['error'] = $error_message;

            $errors['success'] = 'false';

            echo json_encode($errors);

            exit;

        }else

        {

            return true;

        }



    }
    

}

