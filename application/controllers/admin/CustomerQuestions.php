<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CustomerQuestions extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
        parent::__construct();
       
	    $this->load->model('Model_customer_questions');
		checkAdminSession();
		//$res = checkLevels(2);
		//checkAuth($res);
    }
	
	public function manage($id)
	{
		$data = array();
		$data['questions']	 = 	$this->Model_customer_questions->getMultipleRows(array('product_id' => $id));
		$data['content'] = 'customerquestions/manage';
		$data['class'] = 'product'; 
		$this->load->view('template',$data);	
		
	}
	
	public function action()
	{
		
		if(isset($_POST['form_type']))
		{

			switch($_POST['form_type'])
			{

				case 'update':

					if($this->update())
					{
						$data['success'] = 'Data has been updated successfully.';
						$data['error'] = 'false';
						echo json_encode($data);
						exit;
					}else
					{
						$data['success'] = 'false';
						$data['error'] = 'Data has not been updated successfully.Please try again';
						echo json_encode($data);
						exit;
					}
										

				break;
				
			}
		}	
		
	}
	
	public function answer($id)
	{
		$data = array();		
		$data['question']	 = 	$this->Model_customer_questions->get($id);
		$data['content'] 	 =  'customerquestions/edit';
		$data['class'] = 'product';
		$this->load->view('template',$data);
	}
	
	private function update()
	{
		$data = array();
		$variant_value_ids = array();
		$post_data = $this->input->post();
		
		foreach($post_data as $key => $value)
		{
			
			if($key == 'form_type' || $key == 'id')
			{
				continue;	
			}
			$data[$key] = $value;
		}
		
		if($data['answer'] != '')
		{
			$data['answered'] = 1;
		}
		else
		{
			$data['answered'] = 0;
		}
		
		$data['updated_at'] = date('Y-m-d H:i:s');		
		$data['updated_by'] = $this->session->userdata['user']['id']; 
				
		$update_by['id'] = $this->input->post('id');		
		$update = $this->Model_customer_questions->update($data,$update_by);
	
		return $update;
	}
	
	
}
