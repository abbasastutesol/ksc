<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Company_profile extends CI_Controller {



	public function __construct()

    {

        parent::__construct();

       

	    $this->load->model('Model_contact_us');
		$this->load->model('Model_contact_us_feedback');
        $this->load->model('Model_company_profile');
		$this->load->model('Model_page');

		checkAdminSession();
		
		$this->user_rights_array = user_rights();

		//$res = checkLevels(2);

		//checkAuth($res);

    }

	

	public function index()

	{
		
		/* if($this->user_rights_array['Contact Us']['show_p'] == 1) 
		{ */

			$data = array();
	
			$data['company_profile']	 = 	$this->Model_company_profile->getProfile();
	
			$data['content'] = 'admin/company_profile/manage';
	
			$data['class'] = 'company_profile';
	
			$this->load->view('template',$data);	
		/* } else {
			show_404();
		} */
		

	}

	

	public function action()

	{

		

		if(isset($_POST['form_type']))

		{



			switch($_POST['form_type'])

			{

				case 'update':



					//$this->validation();

					if($this->update())

					{

						$data['success'] = 'Company profile Added successfully.';

						$data['error'] = 'false';

						echo json_encode($data);

						exit;

					}else

					{

						$data['success'] = 'false';

						$data['error'] = 'Something.Please try again.';

						echo json_encode($data);

						exit;

					}
                            case 'add':
										
                            if($this->save())

					{

						$data['success'] = 'Company profile Added successfully.';

						$data['error'] = 'false';

						echo json_encode($data);

						exit;

					}else

					{

						$data['success'] = 'false';

						$data['error'] = 'Something.Please try again';

						echo json_encode($data);

						exit;

					}


				break;

				

			}

		}	

		

	}

	public function save()
	{
		$data = array();

		$post_data = $this->input->post();
           
		$keys[] =  'form_type';

		$keys[] =  'id';

		

		foreach($post_data as $key => $value)

		{

			

			if(!in_array($key,$keys))

			{

				$data[$key] = $value;	

			}

		}
		
		if(isset($_FILES['image']) && $_FILES['image']['name'] != '')

		{

			$path = 'images/';

			$file_name = uploadImage($path);
			$data['banner_image'] = $file_name;
            $fullPath = 'assets/frontend/'.$path;
			
            compress_image($fullPath);

		}  

		

		$data['created_at'] = date('Y-m-d H:i:s');

		$data['created_by'] = $this->session->userdata['user']['id']; 

		$data['updated_at'] = date('Y-m-d H:i:s');
		
         
		
		$insert_id = $this->Model_company_profile->save($data);

		 $logData = array(
            'type'=>    'add',
            'section'=> 'Company profile'
        );
        saveLogs($logData);
		

		return $insert_id;
		
		
	}
	private function update()

	{

		$data = array();

		$post_data = $this->input->post();

		$keys[] =  'form_type';

		$keys[] =  'id';

		

		foreach($post_data as $key => $value)

		{

			

			if(!in_array($key,$keys))

			{

				$data[$key] = $value;	

			}

		}

		

		if(isset($_FILES['image']) && $_FILES['image']['name'] != '')

		{

			

			
			$path = 'images/';

			$file_name = uploadImage($path);
           //echo $file_name; exit;
			$data['banner_image'] = $file_name;
            $fullPath = 'assets/frontend/'.$path;
			
            compress_image($fullPath);

		}  

		

		$data['created_at'] = date('Y-m-d H:i:s');

		$data['created_by'] = $this->session->userdata['user']['id']; 

		$data['updated_at'] = date('Y-m-d H:i:s');

		$update_by['id'] = $this->input->post('id');
		
		$tpl_name = $this->input->post('tpl_name');

        $id = save_meta_data($data,$tpl_name);
		 
		$value = unset_meta_date($data);

		$update = $this->Model_company_profile->update($value,$update_by);


        // save logs here
        $logData = array(
            'type'=>    'update',
            'section'=> 'Company profile'
        );
        saveLogs($logData);
		

		return $update;

	}

	

	private function validation()

	{

		    $errors = array();

			$this->form_validation->set_error_delimiters('<p>', '</p>');

           

			$this->form_validation->set_rules('eng_description', 'Eng description', 'required');

			$this->form_validation->set_rules('arb_description', 'Arb description', 'required');

			//$this->form_validation->set_rules('eng_content', 'English Content', 'required');

			//$this->form_validation->set_rules('arb_content', 'Arabic Content', 'required');

			

			

			if ($this->form_validation->run() == FALSE)

			{

				$errors['error'] = validation_errors();

				$errors['success'] = 'false';

				echo json_encode($errors);

				exit;

			}else

			{

				return true;

			}

			

	}
	
	
	public function contact_requests()

	{
		if($this->user_rights_array['Contact Us Requests']['show_p'] == 1) 
		{
			$data = array();
	
			$data['feedbacks']	 = 	$this->Model_contact_us_feedback->getAll();

			$data['content'] = 'admin/contact_us/contact_us_requests';
	
			$data['class'] = 'contact_us_request';
	
			$this->load->view('template',$data);	
		} else {
			show_404();
		}
		

	}
	
	public function contact_delete(){
	
		$delete = false;
		
		$deleted_by['id'] = $this->input->post('id'); 

		$delete = $this->Model_contact_us_feedback->delete($deleted_by);
		
		if($delete){

            // save logs here
            $logData = array(
                'type'=>    'delete',
                'section'=> 'Contact Request'
            );
            saveLogs($logData);

			$data['success'] = 'Feedback deleted successfully.';
	
			$data['error'] = 'false';
	
			echo json_encode($data);
	
			exit;

		}else{

			$data['success'] = 'false';
	
			$data['error'] = 'Feedback not deleted successfully. Please try again.';
	
			echo json_encode($data);
	
			exit;
		}
	}
    
	

}

