<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller {

    /**

     * Index Page for this controller.

     *

     * Maps to the following URL

     * 		http://example.com/index.php/welcome

     *	- or -

     * 		http://example.com/index.php/welcome/index

     *	- or -

     * Since this controller is set as the default controller in

     * config/routes.php, it's displayed at http://example.com/

     *

     * So any other public methods not prefixed with an underscore will

     * map to /index.php/welcome/<method_name>

     * @see https://codeigniter.com/user_guide/general/urls.html

     */

    public function __construct()

    {

        parent::__construct();

        $this->load->helper("url");

        $this->load->library("pagination");

        $this->load->model('Model_product');

        $this->load->model('Model_category');

        $this->load->model('Model_variant');

        $this->load->model('Model_product_variant_value');

        $this->load->model('Model_product_variant_group');

        $this->load->model('Model_orders');

        $this->load->model('Model_order_product');

        $this->load->model('Model_filters');

        $this->load->model('Model_return_order');

        $this->load->model('Model_shipment_methods');

        $this->load->model('Model_question');

        $this->load->model('Model_lostitem');

        $this->load->model('Model_lostitem_answer');

        $this->load->model('Model_placefeedback');

        $this->load->model('Model_placefeedback_answer');

        $this->load->model('Model_order_history');

        $this->load->model('Model_order_status');

        $this->load->model('Model_product_rating');

        checkAdminSession();

        $this->user_rights_array = user_rights();



        //$res = checkLevels(2);

        //checkAuth($res);

    }





    public function action()

    {



        if(isset($_POST['form_type']))

        {

            switch($_POST['form_type'])

            {

                case 'delete':

                    $this->deleteData();

                    $data['success'] = 'Order deleted successfully.';

                    $data['error'] = 'false';

                    echo json_encode($data);

                    exit;

                    break;

                case 'update':

                    $this->validation();

                    $this->update();



                    break;



                case 'upload':

                    $this->validation();



                    $response = $this->upload();

                    if ($response) {

                        $data['success'] = 'Order Receipt verified successfully.';

                        $data['error'] = 'false';

                    } else {

                        $data['success'] = 'Order Receipt failed to verify.';

                        $data['error'] = 'true';

                    }



                    echo json_encode($data);

                    exit;

                    break;



            }

        }



    }



    public function index()

    {

        if($this->user_rights_array['Orders']['show_p'] == 1)

        {

            $this->lang->load("message",$this->session->userdata('site_lang'));

            $data = $this->lang->line('all');

            $order_search = $this->input->post('order_search_value');



            //$total_rows = $this->Model_orders->record_count();

            $condition = "payment_order_status = 1";

            $total_rows = $this->Model_orders->getWhereCount($condition);



            $order_status = $_COOKIE['order_status'];



            if($order_status == ""){

                $array['order_status'] = "";

            }



            if($order_status != NULL) {

                $order_all['order_status'] = $order_status;

                $array['order_status'] = $order_status;

                $count_by['order_status'] = $order_status;

                $count_by['payment_order_status'] = '1';

                $total_rows = $this->Model_orders->getMultipleRows($count_by, true);

                $total_rows = count($total_rows);

            }else{



                $array['order_status'] = "";

            }



            $page = $_GET['per_page'];



            $path = 'admin/orders';



            $paginate = orderPagination($path,$total_rows);



            if($order_search != null || $order_search != ""){

                $order_all['id'] = ltrim($order_search,'0');

                $order_all['payment_order_status'] = '1';



                $data['orders'] = $this->Model_orders->getMultipleRows($order_all,false,'DESC');

            }else {



                $data['orders'] = $this->Model_orders->getAllOrdersPaginate($paginate['per_page'], $page,$array);

            }

            $data["links"] = $this->pagination->create_links();

            $data['limit'] = $paginate['per_page'];

            $data['count'] = $paginate['total_rows'];



            // remove old cookies here

            unset($_COOKIE['order_limits']);

            setcookie('order_limits', '', time() - 3600);



            unset($_COOKIE['order_status']);

            setcookie('order_status', '', time() - 3600);



            $data['order_statuses'] = $this->Model_order_status->getAll();

            $data['content'] = 'admin/orders/manage';

            $data['class'] = 'orders';

            $this->load->view('template',$data);

        } else {

            show_404();

        }

    }



    private function upload(){

        $data = array();

        $update_by = array();

        $order_id = $this->input->post('order_id');

        $tempName = $_FILES["order_receipt"]["tmp_name"];

        $fileName = $_FILES["order_receipt"]["name"];



        $ext = pathinfo($fileName, PATHINFO_EXTENSION);

        $file_name = 'receipt-'.$order_id.'.'.$ext;

        $path = "uploads/receipt/";



        $order =  $this->Model_orders->get($order_id);

        if($order) {

            unlink($path . $order->receipt);

        }



        move_uploaded_file($tempName, $path . $file_name);

        $data['receipt'] = $file_name;

        $data['order_status'] = '2';

        $data['payment_status'] = '1';

        $update_by['id'] = $order_id;

        $success = $this->Model_orders->update($data,$update_by);



        // save logs here

        $logData = array(

            'type'=>    'add',

            'section'=> 'Order Receipt'

        );

        saveLogs($logData);



        if($success) {

            $response = true;

        }else{

            $response = false;

        }

        return $response;

    }



    private function validation(){

        $errors = array();

        $error_message = "";

        $validation = true;



        if (empty($_FILES['order_receipt']['name'])) {

            $error_message .= "<p>The Receipt field is required</p>";

            $validation = false;

        }

        if ($validation == false) {

            $errors['error'] = $error_message;



            $errors['success'] = 'false';



            echo json_encode($errors);



            exit;

        }

    }



    public function remove_receipt(){

        $res = array();

        $order_id = $this->input->post('order_id');

        $order =  $this->Model_orders->get($order_id);



        $path = "uploads/receipt/";

        if($order) {

            unlink($path . $order->receipt);

        }

        $data['receipt'] = "";

        $update_by['id'] = $order_id;

        $data['order_status'] = '1';

        $data['payment_status'] = '0';

        $update = $this->Model_orders->update($data,$update_by);



        // save logs here

        $logData = array(

            'type'=>    'delete',

            'section'=> 'Order Receipt'

        );

        saveLogs($logData);



        if($update) {

            $res['error'] = false;

            $res['message'] = "Receipt has been removed successfully";

        }else{

            $res['error'] = true;

            $res['message'] = "Receipt has not been removed";

        }



        echo json_encode($res);

        exit;

    }





    public function order_history()

    {

        $this->history_validation();



        $post_data = $this->input->post();

        $updat_by['id'] = $post_data['order_id'];

        $order_status = explode('|', $post_data['order_status']);



        $update['order_status'] = $order_status[0];

        $post_data['order_status'] = $order_status[0];



        $userData = $this->session->userdata('user');



        $post_data['user'] = $userData['name'];



        $order = $this->Model_orders->get($post_data['order_id']);





        $this->lang->load("message",$order->language);

        $lang = $order->language;

        //echo $lang; exit;

        $messages = $this->lang->line('all');



        /*if order_status 0 (cancelled) then override the

        ##condition to fill receipt with dummy data so

        ##that order can be cancelled by admin forcefully

        ##without attached payment receipt */

        if($order_status[0] == 0){

            $order->receipt = 'dummy_fill';

        }

        if($order->card_id != 0){

            $order->receipt = 'dummy_fill';

        }



        if($order_status[1] == 'Cancelled')

        {

            $orderStatus = $messages['my_order_cancelled_label'];

            $subject = $messages['order_processed_title'];

        }

        if($order_status[1] == 'Preparing Order')

        {

            $orderStatus = $messages['my_order_preparing_order_message'];

            $subject = $messages['order_preparing_title'];

        }

        if($order_status[1] == 'Verify Payment')

        {

            $orderStatus = $messages['my_order_verify_payment_label'];

            $subject = $messages['order_processed_title'];

        }

        if($order_status[1] == 'Preparing Shipment')

        {

            $orderStatus = $messages['my_order_preparing_ship_message'];

            $subject = $messages['order_preparing_ship_title'];

        }

        if($order_status[1] == 'Shipped')

        {

            $orderStatus = $messages['my_order_shipped_message'];

            $subject = $messages['order_shipped_title'];

        }

        if($order_status[1] == 'Delivered')

        {

            $orderStatus = $messages['my_order_delivered_message'];

            $subject = $messages['order_delivered_title'];

        }

        if($order_status[1] == 'Completed')

        {

            $orderStatus = $messages['my_order_completed_label'];

            $subject = $messages['order_processed_title'];

        }

        if($order_status[1] == 'Returned')

        {

            $orderStatus = $messages['my_order_returned_message'];

            $subject = $messages['order_returned_title'];

        }

        if($order_status[1] == 'Rejected')

        {

            $orderStatus = $messages['my_order_rejected_message'];

            $subject = $messages['order_rejected_title'];

        }

        /*****end*****/



        //if($order->receipt != "") {



        $user = getUserById($post_data['user_id']);



        if($user)

        {

            $user_name = $user['first_name'] . " " . $user['last_name'];

        }

        else

        {

            $user = getUserFromCartById($post_data['user_id']);

            $user_name = $user['full_name'];

        }

        $user['email'];

        $ordId = str_pad($post_data['order_id'], 6, "0", STR_PAD_LEFT);







        $emailData['message'] = $messages['hi_label'] . " " . $user_name . ", <br>" .

            $orderStatus." ". $messages['your_order_label'] . " " . $ordId . " 

            <br><br> ".$post_data['comment'];



        $smsMsg =  $orderStatus." ".$messages['your_order_label'] . " " . $ordId .

            " ".$post_data['comment'];



        $emailData['title'] = $messages['order_processed_title'];

        $emailData['order_status'] = $order_status[0];



        $emailData['info'] = array();

        $emailData['order_status_email'] = 'order_email';



        if($lang == "eng") {

            $view = $this->load->view('layouts/emails/eng_general_email',$emailData,TRUE);

        }else{

            $view = $this->load->view('layouts/emails/arb_general_email',$emailData,TRUE);

        }



        if ($post_data['notify_customer_sms'] == "sms" && $post_data['notify_customer_email'] == '') {

            send_sms($post_data['order_id'], $smsMsg);

            $post_data['notify_customer'] = $post_data['notify_customer_sms'];

        }

        if ($post_data['notify_customer_email'] == "email" && $post_data['notify_customer_sms'] == '') {

            $sent = send_email_user($subject, $view, $user['email']);

            $post_data['notify_customer'] = $post_data['notify_customer_email'];

        }

        if ($post_data['notify_customer_sms'] == "sms" && $post_data['notify_customer_email'] == "email") {



            send_email_user($subject, $view, $user['email']);

            send_sms($post_data['order_id'], $smsMsg);

            $post_data['notify_customer'] = 'sms,email';

        }



        unset($post_data['user_id']);

        unset($post_data['notify_customer_sms']);

        unset($post_data['notify_customer_email']);



        if ($order_status[0] == '6' || $order_status[0] == '5') {

            $update['payment_status'] = '1';

        }

        // exclude loyalty points

        if($order->user_type == 1) {

            if ($order_status[0] == '8' || $order_status[0] == '9' || $order_status[0] == '0') {

                excludeLoyaltyPoints($order->total_amount,$order->currency_rate,$order->user_id);

            }

        }



        $save = $this->Model_order_history->save($post_data);

        $this->Model_orders->update($update, $updat_by);





        // save logs here

        $logData = array(

            'type'=>    'add',

            'section'=> 'Order History'

        );

        saveLogs($logData);



        if ($save) {

            $data['success'] = 'The order history has been added successfully.';

            $data['error'] = 'false';

        } else {

            $data['success'] = 'The order history has not been added.';

            $data['error'] = 'true';

        }

        /*}else{

            $data['success'] = 'Please verify the payment first by attaching the

             receipt';

            $data['error'] = 'true';

        }*/

        echo json_encode($data);

        exit;



    }



    private function history_validation(){



        $errors = array();

        $error_message = "";

        $validation = true;



        if ($this->input->post('order_status') == NULL) {

            $error_message .= "<p>The order status field is required</p>";

            $validation = false;

        }



        if ($this->input->post('notify_customer_sms') == NULL || $this->input->post('notify_customer_email') == NULL) {

            $error_message .= "<p>Both notify customer fields are required</p>";

            $validation = false;

        }

        /*if ($this->input->post('notify_customer_email') == NULL) {

            $error_message .= "<p>The notify customer Email field is required</p>";

            $validation = false;

        }*/



        if ($this->input->post('comment') == NULL) {

            $error_message .= "<p>The comment field is required</p>";

            $validation = false;

        }

        if ($validation == false) {

            $errors['error'] = $error_message;



            $errors['success'] = 'false';



            echo json_encode($errors);



            exit;

        }

    }



    public function order_details($order_id)

    {

        if($this->user_rights_array['Orders']['view_p'] == 1)

        {

            $data['order'] = $this->Model_orders->get($order_id);



            $data['order_products'] = $this->Model_orders->getOrderProd($order_id);



            $fetch_by['order_id'] = $order_id;

            $data['order_history'] = $this->Model_order_history->getMultipleRows($fetch_by);



            $data['order_statuses'] = $this->Model_order_status->getAll();

            //echo "<pre>"; print_r($data['order_statuses']); exit;

            $data['content'] = 'admin/orders/order_detail';



            $data['class'] = 'orders';



            $this->load->view('template',$data);

        } else {

            show_404();

        }



    }



    public function returnItems()

    {

        $data = array();

        $data['orders'] = $this->Model_orders->getAllReturn();

        $data['content'] = 'admin/orders/return_items';

        $data['class'] = 'returnItems';

        $this->load->view('template',$data);



    }



    public function feedBack()

    {

        if($this->user_rights_array['Order Feedback']['view_p'] == 1) {

            $data = array();

            $data['feedback'] = $this->Model_product_rating->getAll();

            //echo "<pre>"; print_r($data['feedback']); exit;

            $data['content'] = 'admin/orders/feedback';

            $data['class'] = 'feedBack';

            $this->load->view('template', $data);

        }else{

            show_404();

        }



    }



    public function feedBackDetails($f_id)

    {

        $data = array();

        $data['feedback'] = $feed = $this->Model_product_rating->get($f_id);



        $data['content'] = 'admin/orders/feedback_detail';

        $data['class'] = 'feedBack';

        $this->load->view('template',$data);



    }



    public function lostItems()

    {

        $data = array();

        $data['orders'] = $this->Model_orders->getAllLostItem();

        $data['content'] = 'admin/orders/lost_items';

        $data['class'] = 'lostItems';

        $this->load->view('template',$data);



    }



    public function lostItemDetails($l_id)

    {

        $data = array();

        $data['lostitem'] = $this->Model_orders->getLostitemDetails($l_id);

        $data['lostitemAns'] = $this->Model_orders->getLostitemAnswers($l_id);

        $data['content'] = 'admin/orders/lostitem_detail';

        $data['class'] = 'lostItems';

        $this->load->view('template',$data);



    }



    public function currentCarts()

    {

        $data = array();

        $cart = $this->Model_orders->getCurrentCart();

        $cart_guest = $this->Model_orders->getCurrentGuestCart();

        $allCart = array_merge($cart, $cart_guest);

        usort($allCart, function($b, $a) {

            return $a['id'] - $b['id'];

        });

        $data['carts'] = $allCart;

        $data['content'] = 'admin/orders/current_carts';

        $data['class'] = 'currentCarts';

        $this->load->view('template',$data);



    }


    public function exportExcelOrder()

    {

        $filename = "orders";
        /*$get_by['payment_order_status'] = '1';
        $get_by['order_status'] = '1';*/
        $orders = $this->Model_orders->getExportOrders();
        //echo "<pre>"; print_r($orders); exit;
        // save logs here

        $logData = array(

            'type'=>    'export',

            'section'=> 'Order'

        );

        saveLogs($logData);



        $this->gen_xl($orders, $filename);

    }



    public function exportExcelReturnItems()

    {

        $filename = "ReturnItems";

        $orders = $this->Model_orders->getAllReturn();

        $this->gen_xl($orders, $filename, 'return');

    }



    public function exportExcelFeedBack()

    {

        $filename = "Feedback";

        $orders = $this->Model_orders->getAllFeedback();



        // save logs here

        $logData = array(

            'type'=>    'export',

            'section'=> 'Order Feedback'

        );

        saveLogs($logData);



        $this->gen_xl($orders, $filename);

    }



    public function exportExcelLostItems()

    {

        $filename = "LostItem";

        $orders = $this->Model_orders->getAllLostItem();

        $this->gen_xl($orders, $filename);

    }



    private function deleteData()

    {



        $deleted_by['id'] = $this->input->post('id');

        $delete = $this->Model_orders->delete($deleted_by);

        $fetch_by['order_id']  = $this->input->post('id');

        $this->Model_order_product->delete($fetch_by);



        // save logs here

        $logData = array(

            'type'=>    'delete',

            'section'=> 'Order'

        );

        saveLogs($logData);



        return $delete;



    }



    public function gen_xl($orders, $filename, $type = 'order')

    {

        $this->load->library('excel');


        $this->excel->getProperties()->setCreator("Soumya Biswas")

            ->setLastModifiedBy("Soumya Biswas")

            ->setTitle("Office 2007 XLSX Test Document")

            ->setSubject("Office 2007 XLSX Test Document")

            ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")

            ->setKeywords("office 2007 openxml php")

            ->setCategory("Test result file");

        $border = array(

            'borders' => array(

                'outline' => array(

                    'style' => PHPExcel_Style_Border::BORDER_THIN

                )

            )

        );

        $ans = array(

            'alignment' => array(

                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,

                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER

            )

        );

        $arr = array(

            'alignment' => array(

                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,

                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER

            ),

            'font'  => array(

                'bold'  => true,

                "color" => array("rgb" => "903")

            )

        );



        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth('20');

        $this->excel->getActiveSheet()->getStyle('A1')->applyFromArray($border);



        $this->excel->getActiveSheet()->setCellValue('A2', 'Rcpt_Name');

        $this->excel->getActiveSheet()->getStyle('A2')->applyFromArray($arr);

        $this->excel->getActiveSheet()

            ->getStyle('A2')

            ->getFill()

            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)

            ->getStartColor()

            ->setRGB('ededed');

        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth('20');

        $this->excel->getActiveSheet()->getStyle('A2')->applyFromArray($border);





        $this->excel->getActiveSheet()->setCellValue('B2', 'Rcpt_Reference');

        $this->excel->getActiveSheet()->getStyle('B2')->applyFromArray($arr);

        $this->excel->getActiveSheet()

            ->getStyle('B2')

            ->getFill()

            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)

            ->getStartColor()

            ->setRGB('ededed');

        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth('20');

        $this->excel->getActiveSheet()->getStyle('B2')->applyFromArray($border);

        $this->excel->getActiveSheet()->setCellValue('C2', 'Rcpt_Address');

        $this->excel->getActiveSheet()->getStyle('C2')->applyFromArray($arr);

        $this->excel->getActiveSheet()

            ->getStyle('C2')

            ->getFill()

            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)

            ->getStartColor()

            ->setRGB('ededed');

        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth('40');

        $this->excel->getActiveSheet()->getStyle('C2')->applyFromArray($border);

        $this->excel->getActiveSheet()->setCellValue('D2', 'Rcpt_City');

        $this->excel->getActiveSheet()->getStyle('D2')->applyFromArray($arr);

        $this->excel->getActiveSheet()

            ->getStyle('D2')

            ->getFill()

            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)

            ->getStartColor()

            ->setRGB('ededed');

        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth('60');

        $this->excel->getActiveSheet()->getStyle('D2')->applyFromArray($border);



        $this->excel->getActiveSheet()->setCellValue('E2', 'Rcpt_Country');

        $this->excel->getActiveSheet()->getStyle('E2')->applyFromArray($arr);

        $this->excel->getActiveSheet()

            ->getStyle('E2')

            ->getFill()

            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)

            ->getStartColor()

            ->setRGB('ededed');

        $this->excel->getActiveSheet()->getColumnDimension('ED')->setWidth('40');

        $this->excel->getActiveSheet()->getStyle('E2')->applyFromArray($border);



        $this->excel->getActiveSheet()->setCellValue('F2', 'Rcpt_Phone');

        $this->excel->getActiveSheet()->getStyle('F2')->applyFromArray($arr);

        $this->excel->getActiveSheet()

            ->getStyle('F2')

            ->getFill()

            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)

            ->getStartColor()

            ->setRGB('ededed');

        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth('40');

        $this->excel->getActiveSheet()->getStyle('F2')->applyFromArray($border);



        $this->excel->getActiveSheet()->setCellValue('G2', 'Rcpt_Fax');

        $this->excel->getActiveSheet()->getStyle('G2')->applyFromArray($arr);

        $this->excel->getActiveSheet()

            ->getStyle('G2')

            ->getFill()

            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)

            ->getStartColor()

            ->setRGB('ededed');

        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth('40');

        $this->excel->getActiveSheet()->getStyle('G2')->applyFromArray($border);





        $this->excel->getActiveSheet()->setCellValue('H2', 'Rcpt_Phone_2');

        $this->excel->getActiveSheet()->getStyle('H2')->applyFromArray($arr);

        $this->excel->getActiveSheet()

            ->getStyle('H2')

            ->getFill()

            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)

            ->getStartColor()

            ->setRGB('ededed');

        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth('20');

        $this->excel->getActiveSheet()->getStyle('H2')->applyFromArray($border);



        $this->excel->getActiveSheet()->setCellValue('I2', 'Rcpt_Contact');

        $this->excel->getActiveSheet()->getStyle('I2')->applyFromArray($arr);

        $this->excel->getActiveSheet()

            ->getStyle('I2')

            ->getFill()

            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)

            ->getStartColor()

            ->setRGB('ededed');

        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth('20');

        $this->excel->getActiveSheet()->getStyle('I2')->applyFromArray($border);



        $this->excel->getActiveSheet()->setCellValue('J2', 'Shpt_Customs_Value');

        $this->excel->getActiveSheet()->getStyle('J2')->applyFromArray($arr);

        $this->excel->getActiveSheet()

            ->getStyle('J2')

            ->getFill()

            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)

            ->getStartColor()

            ->setRGB('ededed');

        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth('20');

        $this->excel->getActiveSheet()->getStyle('J2')->applyFromArray($border);



        $this->excel->getActiveSheet()->setCellValue('K2', 'Shpt_Reference');

        $this->excel->getActiveSheet()->getStyle('K2')->applyFromArray($arr);

        $this->excel->getActiveSheet()

            ->getStyle('K2')

            ->getFill()

            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)

            ->getStartColor()

            ->setRGB('ededed');

        $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth('20');

        $this->excel->getActiveSheet()->getStyle('K2')->applyFromArray($border);



        $this->excel->getActiveSheet()->setCellValue('L2', 'Shpt_Customs_Currency');

        $this->excel->getActiveSheet()->getStyle('L2')->applyFromArray($arr);

        $this->excel->getActiveSheet()

            ->getStyle('L2')

            ->getFill()

            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)

            ->getStartColor()

            ->setRGB('ededed');


        $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth('40');

        $this->excel->getActiveSheet()->getStyle('L2')->applyFromArray($border);



        $this->excel->getActiveSheet()->setCellValue('M2', 'Shpt_Pieces');

        $this->excel->getActiveSheet()->getStyle('M2')->applyFromArray($arr);

        $this->excel->getActiveSheet()

            ->getStyle('M2')

            ->getFill()

            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)

            ->getStartColor()

            ->setRGB('ededed');

        $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth('20');

        $this->excel->getActiveSheet()->getStyle('M2')->applyFromArray($border);



        $this->excel->getActiveSheet()->setCellValue('N2', 'Shpt_Product_Type');

        $this->excel->getActiveSheet()->getStyle('N2')->applyFromArray($arr);

        $this->excel->getActiveSheet()

            ->getStyle('N2')

            ->getFill()

            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)

            ->getStartColor()

            ->setRGB('ededed');

        $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth('20');

        $this->excel->getActiveSheet()->getStyle('N2')->applyFromArray($border);

        $this->excel->getActiveSheet()->setCellValue('O2', 'Shpt_Weight');

        $this->excel->getActiveSheet()->getStyle('O2')->applyFromArray($arr);

        $this->excel->getActiveSheet()

            ->getStyle('O2')

            ->getFill()

            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)

            ->getStartColor()

            ->setRGB('ededed');

        $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth('20');

        $this->excel->getActiveSheet()->getStyle('O2')->applyFromArray($border);



        $this->excel->getActiveSheet()->setCellValue('P2', 'Shpmt_COD_Currency');

        $this->excel->getActiveSheet()->getStyle('P2')->applyFromArray($arr);

        $this->excel->getActiveSheet()

            ->getStyle('P2')

            ->getFill()

            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)

            ->getStartColor()

            ->setRGB('ededed');

        $this->excel->getActiveSheet()->getColumnDimension('P')->setWidth('30');

        $this->excel->getActiveSheet()->getStyle('P2')->applyFromArray($border);



        $this->excel->getActiveSheet()->setCellValue('Q2', 'Shpt_COD_Value');

        $this->excel->getActiveSheet()->getStyle('Q2')->applyFromArray($arr);

        $this->excel->getActiveSheet()

            ->getStyle('R2')

            ->getFill()

            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)

            ->getStartColor()

            ->setRGB('ededed');

        $this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth('20');

        $this->excel->getActiveSheet()->getStyle('Q2')->applyFromArray($border);



        $this->excel->getActiveSheet()->setCellValue('R2', 'Shpr_Account');

        $this->excel->getActiveSheet()->getStyle('R2')->applyFromArray($arr);

        $this->excel->getActiveSheet()

            ->getStyle('R2')

            ->getFill()

            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)

            ->getStartColor()

            ->setRGB('ededed');

        $this->excel->getActiveSheet()->getColumnDimension('R')->setWidth('20');

        $this->excel->getActiveSheet()->getStyle('R2')->applyFromArray($border);





        $this->excel->getActiveSheet()->setCellValue('S2', 'Shpr_Company');

        $this->excel->getActiveSheet()->getStyle('S2')->applyFromArray($arr);

        $this->excel->getActiveSheet()

            ->getStyle('S2')

            ->getFill()

            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)

            ->getStartColor()

            ->setRGB('ededed');

        $this->excel->getActiveSheet()->getColumnDimension('S')->setWidth('20');

        $this->excel->getActiveSheet()->getStyle('S2')->applyFromArray($border);



        $this->excel->getActiveSheet()->setCellValue('T2', 'Shpr_Name');

        $this->excel->getActiveSheet()->getStyle('T2')->applyFromArray($arr);

        $this->excel->getActiveSheet()

            ->getStyle('T2')

            ->getFill()

            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)

            ->getStartColor()

            ->setRGB('ededed');

        $this->excel->getActiveSheet()->getColumnDimension('T')->setWidth('20');

        $this->excel->getActiveSheet()->getStyle('T2')->applyFromArray($border);


        $this->excel->getActiveSheet()->setCellValue('U2', 'Shpr_Address');

        $this->excel->getActiveSheet()->getStyle('U2')->applyFromArray($arr);

        $this->excel->getActiveSheet()

            ->getStyle('U2')

            ->getFill()

            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)

            ->getStartColor()

            ->setRGB('ededed');

        $this->excel->getActiveSheet()->getColumnDimension('U')->setWidth('20');

        $this->excel->getActiveSheet()->getStyle('U2')->applyFromArray($border);




        $this->excel->getActiveSheet()->setCellValue('V2', 'Shpr_City');

        $this->excel->getActiveSheet()->getStyle('V2')->applyFromArray($arr);

        $this->excel->getActiveSheet()

            ->getStyle('V2')

            ->getFill()

            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)

            ->getStartColor()

            ->setRGB('ededed');

        $this->excel->getActiveSheet()->getColumnDimension('V')->setWidth('20');

        $this->excel->getActiveSheet()->getStyle('V2')->applyFromArray($border);





        $this->excel->getActiveSheet()->setCellValue('W2', 'Shpr_Country');

        $this->excel->getActiveSheet()->getStyle('W2')->applyFromArray($arr);

        $this->excel->getActiveSheet()

            ->getStyle('W2')

            ->getFill()

            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)

            ->getStartColor()

            ->setRGB('ededed');

        $this->excel->getActiveSheet()->getColumnDimension('W')->setWidth('20');

        $this->excel->getActiveSheet()->getStyle('W2')->applyFromArray($border);





        $this->excel->getActiveSheet()->setCellValue('X2', 'ID');

        $this->excel->getActiveSheet()->getStyle('X2')->applyFromArray($arr);

        $this->excel->getActiveSheet()

            ->getStyle('X2')

            ->getFill()

            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)

            ->getStartColor()

            ->setRGB('ededed');

        $this->excel->getActiveSheet()->getColumnDimension('X')->setWidth('20');

        $this->excel->getActiveSheet()->getStyle('X2')->applyFromArray($border);



        $this->excel->getActiveSheet()->setCellValue('Y2', 'Shpr_Phone');

        $this->excel->getActiveSheet()->getStyle('Y2')->applyFromArray($arr);

        $this->excel->getActiveSheet()

            ->getStyle('Y2')

            ->getFill()

            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)

            ->getStartColor()

            ->setRGB('ededed');

        $this->excel->getActiveSheet()->getColumnDimension('Y')->setWidth('20');

        $this->excel->getActiveSheet()->getStyle('Y2')->applyFromArray($border);


        $n = 2;

        foreach($orders as $order){



            $products = $this->Model_orders->getOrderProd($order->id);


            /*address new*/

            $addressCheck = false;
            $address = getUserAddressByOrder($order->id);
            if ($address) {
                $country = $address->country;
                $city = $address->city;
                $addressCheck = true;
            } else {

                $address = getCartAddress($order->address_id);
                if($address){

                    $city = $address->city;
                    $country = $address->country;
                    $addressCheck = true;
                }
            }


            $user = getUserById($order->user_id);

            if (!$user) {
                $fullName = $address->full_name;
                $email = $address->email;
                if(!$addressCheck) {
                    $country = $address->country;
                    $city = $address->city;
                }
                $contact = $address->phone_no;
            } else {
                $fullName = $user['first_name'] . " " . $user['last_name'];
                $email = $user['email'];
                if(!$addressCheck) {
                    $country = $user['country'];
                    $city = $user['city'];
                }
                $contact = $user['mobile_no'];
            }



            /*address*/



            $n++;



            $this->excel->getActiveSheet()->setCellValue('A'.$n, $fullName);

            $this->excel->getActiveSheet()->getStyle('A'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

            $this->excel->getActiveSheet()->getStyle('A'.$n)->applyFromArray($border);



            $this->excel->getActiveSheet()->setCellValue('B'.$n, $order->id);

            $this->excel->getActiveSheet()->getStyle('B'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

            $this->excel->getActiveSheet()->getStyle('B'.$n)->applyFromArray($border);



            $this->excel->getActiveSheet()->setCellValue('C'.$n, $address->address_1.' '.$address->address_2);

            $this->excel->getActiveSheet()->getStyle('C'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

            $this->excel->getActiveSheet()->getStyle('C'.$n)->applyFromArray($border);

            $this->excel->getActiveSheet()->setCellValue('D'.$n, getCityById($city,'eng'));

            $this->excel->getActiveSheet()->getStyle('D'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

            $this->excel->getActiveSheet()->getStyle('D'.$n)->applyFromArray($border);

            $this->excel->getActiveSheet()->setCellValue('E'.$n, getCoutryByCode($country,false,'eng'));

            $this->excel->getActiveSheet()->getStyle('E'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

            $this->excel->getActiveSheet()->getStyle('E'.$n)->applyFromArray($border);



            $this->excel->getActiveSheet()->setCellValue('F'.$n, $contact);

            $this->excel->getActiveSheet()->getStyle('F'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

            $this->excel->getActiveSheet()->getStyle('F'.$n)->applyFromArray($border);



            $this->excel->getActiveSheet()->setCellValue('G'.$n, '');

            $this->excel->getActiveSheet()->getStyle('G'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

            $this->excel->getActiveSheet()->getStyle('G'.$n)->applyFromArray($border);



            $this->excel->getActiveSheet()->setCellValue('H'.$n, '');

            $this->excel->getActiveSheet()->getStyle('H'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

            $this->excel->getActiveSheet()->getStyle('H'.$n)->applyFromArray($border);



            $this->excel->getActiveSheet()->setCellValue('I'.$n, $fullName);

            $this->excel->getActiveSheet()->getStyle('I'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

            $this->excel->getActiveSheet()->getStyle('I'.$n)->applyFromArray($border);



            $this->excel->getActiveSheet()->setCellValue('J'.$n, $order->total_amount);

            $this->excel->getActiveSheet()->getStyle('J'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

            $this->excel->getActiveSheet()->getStyle('J'.$n)->applyFromArray($border);



            $this->excel->getActiveSheet()->setCellValue('K'.$n, $order->id);

            $this->excel->getActiveSheet()->getStyle('K'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

            $this->excel->getActiveSheet()->getStyle('K'.$n)->applyFromArray($border);



            $this->excel->getActiveSheet()->setCellValue('L'.$n, $order->currency_unit);

            $this->excel->getActiveSheet()->getStyle('L'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

            $this->excel->getActiveSheet()->getStyle('L'.$n)->applyFromArray($border);



            $i = 0;

            $count = 0;

            $name = '';
            $prodQuantity = '';

            $price ='';

            foreach($products as $product)

            {
                $prodQuantity .= $product->quantity.',';
                $name .= $product->eng_name.',';

                $price .= $product->price.',';

                $userType = $product->gues;

                $count++;

            }
            $productQuantity = rtrim($prodQuantity,',');
            $productName = rtrim($name,',');

            $productprice = rtrim($price,',');

            if($i > 0)

            {

                $this->excel->getActiveSheet()->setCellValue('A'.$n, '');

                $this->excel->getActiveSheet()->getStyle('A'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

                $this->excel->getActiveSheet()->getStyle('A'.$n)->applyFromArray($border);



                $this->excel->getActiveSheet()->setCellValue('B'.$n, '');

                $this->excel->getActiveSheet()->getStyle('B'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

                $this->excel->getActiveSheet()->getStyle('B'.$n)->applyFromArray($border);



                $this->excel->getActiveSheet()->setCellValue('C'.$n, '');

                $this->excel->getActiveSheet()->getStyle('C'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

                $this->excel->getActiveSheet()->getStyle('C'.$n)->applyFromArray($border);



                $this->excel->getActiveSheet()->setCellValue('D'.$n, '');

                $this->excel->getActiveSheet()->getStyle('D'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

                $this->excel->getActiveSheet()->getStyle('D'.$n)->applyFromArray($border);

                $this->excel->getActiveSheet()->setCellValue('E'.$n, '');

                $this->excel->getActiveSheet()->getStyle('E'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

                $this->excel->getActiveSheet()->getStyle('E'.$n)->applyFromArray($border);



                $this->excel->getActiveSheet()->setCellValue('F'.$n, '');

                $this->excel->getActiveSheet()->getStyle('F'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

                $this->excel->getActiveSheet()->getStyle('F'.$n)->applyFromArray($border);



                $this->excel->getActiveSheet()->setCellValue('G'.$n, '');

                $this->excel->getActiveSheet()->getStyle('G'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

                $this->excel->getActiveSheet()->getStyle('G'.$n)->applyFromArray($border);



                $this->excel->getActiveSheet()->setCellValue('H'.$n, '');

                $this->excel->getActiveSheet()->getStyle('H'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

                $this->excel->getActiveSheet()->getStyle('H'.$n)->applyFromArray($border);



                $this->excel->getActiveSheet()->setCellValue('I'.$n, '');

                $this->excel->getActiveSheet()->getStyle('I'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

                $this->excel->getActiveSheet()->getStyle('I'.$n)->applyFromArray($border);



                $this->excel->getActiveSheet()->setCellValue('J'.$n, '');

                $this->excel->getActiveSheet()->getStyle('J'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

                $this->excel->getActiveSheet()->getStyle('J'.$n)->applyFromArray($border);





                $this->excel->getActiveSheet()->setCellValue('K'.$n, '');

                $this->excel->getActiveSheet()->getStyle('K'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

                $this->excel->getActiveSheet()->getStyle('K'.$n)->applyFromArray($border);



                $this->excel->getActiveSheet()->setCellValue('L'.$n, '');

                $this->excel->getActiveSheet()->getStyle('L'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

                $this->excel->getActiveSheet()->getStyle('L'.$n)->applyFromArray($border);



                $this->excel->getActiveSheet()->setCellValue('M'.$n, '');

                $this->excel->getActiveSheet()->getStyle('M'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

                $this->excel->getActiveSheet()->getStyle('M'.$n)->applyFromArray($border);

                $this->excel->getActiveSheet()->setCellValue('N'.$n, '');

                $this->excel->getActiveSheet()->getStyle('N'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

                $this->excel->getActiveSheet()->getStyle('N'.$n)->applyFromArray($border);



            }

            $this->excel->getActiveSheet()->setCellValue('M'.$n, $productQuantity);

            $this->excel->getActiveSheet()->getStyle('M'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

            $this->excel->getActiveSheet()->getStyle('M'.$n)->applyFromArray($border);

            $this->excel->getActiveSheet()->setCellValue('N'.$n, $productName);

            $this->excel->getActiveSheet()->getStyle('N'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

            $this->excel->getActiveSheet()->getStyle('N'.$n)->applyFromArray($border);

            $this->excel->getActiveSheet()->setCellValue('O'.$n, $product->weight_value.' '.$product->weight_unit);

            $this->excel->getActiveSheet()->getStyle('O'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

            $this->excel->getActiveSheet()->getStyle('O'.$n)->applyFromArray($border);

            $this->excel->getActiveSheet()->setCellValue('P'.$n, $order->currency_unit);

            $this->excel->getActiveSheet()->getStyle('P'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

            $this->excel->getActiveSheet()->getStyle('P'.$n)->applyFromArray($border);



            $this->excel->getActiveSheet()->setCellValue('Q'.$n, $order->extra_charges);

            $this->excel->getActiveSheet()->getStyle('Q'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

            $this->excel->getActiveSheet()->getStyle('Q'.$n)->applyFromArray($border);



            $this->excel->getActiveSheet()->setCellValue('R'.$n, '');

            $this->excel->getActiveSheet()->getStyle('R'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

            $this->excel->getActiveSheet()->getStyle('R'.$n)->applyFromArray($border);



            $this->excel->getActiveSheet()->setCellValue('S'.$n,'');

            $this->excel->getActiveSheet()->getStyle('S'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

            $this->excel->getActiveSheet()->getStyle('S'.$n)->applyFromArray($border);


            $this->excel->getActiveSheet()->setCellValue('T'.$n, '');

            $this->excel->getActiveSheet()->getStyle('T'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

            $this->excel->getActiveSheet()->getStyle('T'.$n)->applyFromArray($border);




            $this->excel->getActiveSheet()->setCellValue('U'.$n, '');

            $this->excel->getActiveSheet()->getStyle('U'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

            $this->excel->getActiveSheet()->getStyle('U'.$n)->applyFromArray($border);



            $this->excel->getActiveSheet()->setCellValue('V'.$n, '');

            $this->excel->getActiveSheet()->getStyle('V'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

            $this->excel->getActiveSheet()->getStyle('V'.$n)->applyFromArray($border);



            $this->excel->getActiveSheet()->setCellValue('W'.$n, '');

            $this->excel->getActiveSheet()->getStyle('W'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

            $this->excel->getActiveSheet()->getStyle('W'.$n)->applyFromArray($border);


            $this->excel->getActiveSheet()->setCellValue('X'.$n, $order->id);

            $this->excel->getActiveSheet()->getStyle('X'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

            $this->excel->getActiveSheet()->getStyle('X'.$n)->applyFromArray($border);



            $this->excel->getActiveSheet()->setCellValue('Y'.$n, '');

            $this->excel->getActiveSheet()->getStyle('Y'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);

            $this->excel->getActiveSheet()->getStyle('Y'.$n)->applyFromArray($border);



            //$n++;

            $i++;



        }

        $filename=$filename.".xls";

        header('Content-Type: application/vnd.ms-excel'); //mime type

        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name

        header('Cache-Control: max-age=0'); //no cache

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

        $objWriter->save('php://output');



    }

}

