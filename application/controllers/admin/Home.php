<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends CI_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
        parent::__construct();

	    $this->load->model('Model_home_content');
	    $this->load->model('Model_home_slider');
		$this->load->model('Model_page');
		$this->load->library('File_upload');
		checkAdminSession();
		$this->user_rights_array = user_rights();
		//$res = checkLevels(2);
		//checkAuth($res);
    }

	public function index($id)
	{
		if($this->user_rights_array['Home']['show_p'] == 1)
		{
			$data = array();



			$data['images'] = $this->Model_home_slider->getAll();

			//echo "<pre>"; print_r($data['images']); exit;

			$data['home_content'] = (array)$this->Model_home_content->get($id);



			$data['content'] = 'home/edit';

			$data['class'] = 'home';

			$this->load->view('template',$data);
		} else {
			show_404();
		}
	}
    public function slider()
    {
		if($this->user_rights_array['Sliders']['show_p'] == 1)
		{
			$data = array();



			$data['sliders'] = $this->Model_home_slider->getWithOrder();

			//echo "<pre>"; print_r($data['sliders']); exit;

			$data['content'] = 'home/slider_view';

			$data['class'] = 'slider';

			$this->load->view('template',$data);
		} else {
			show_404();
		}
    }
    public function slider_add()
    {
		if($this->user_rights_array['Sliders']['add_p'] == 1)
		{
			$data = array();



			$data['content'] = 'home/slider_add';

			$data['class'] = 'slider';

			$this->load->view('template',$data);
		} else {
			show_404();
		}
    }
    public function slider_edit($id)
    {
		if($this->user_rights_array['Sliders']['edit_p'] == 1)
		{
			$data = array();

			$data['slider']	 = 	$this->Model_home_slider->get($id);

			$data['content'] = 'home/slider_edit';

			$data['class'] = 'slider';

			$this->load->view('template',$data);
		} else {
			show_404();
		}
    }

	public function action()
	{

		if(isset($_POST['form_type']))
		{
			switch($_POST['form_type'])
			{
                case 'save_slider':
                    $this->validation();
                    if($this->save_slider())
                    {
                        $data['success'] = 'Slider Added successfully.';
                        $data['error'] = 'false';
                        echo json_encode($data);
                        exit;
                    }else
                    {
                        $data['success'] = 'false';
                        $data['error'] = 'Slider not updated successfully.Please try again';
                        echo json_encode($data);
                        exit;
                    }
                    break;
				case 'update':
					//$this->validation();
					if($this->update())
					{
						$data['success'] = 'Home page updated successfully.';
						$data['error'] = 'false';
						echo json_encode($data);
						exit;
					}else
					{
						$data['success'] = 'false';
						$data['error'] = 'Home page not updated successfully.Please try again';
						echo json_encode($data);
						exit;
					}
				break;
                case 'update_content':
                    //$this->validation();
                    if($this->update_content())
                    {
                        $data['success'] = 'Home page updated successfully.';
                        $data['error'] = 'false';
                        echo json_encode($data);
                        exit;
                    }else
                    {
                        $data['success'] = 'false';
                        $data['error'] = 'Home page not updated successfully.Please try again';
                        echo json_encode($data);
                        exit;
                    }
                    break;
                    case 'update_slider':
                        $this->validation();
                        if($this->update_slider())
                        {
                            $data['success'] = 'Slider updated successfully.';
                            $data['error'] = 'false';
                            echo json_encode($data);
                            exit;
                        }else
                        {
                            $data['success'] = 'false';
                            $data['error'] = 'Slider not updated successfully.Please try again';
                            echo json_encode($data);
                            exit;
                        }
                        break;
                case 'delete':
                    //$this->validation();
                    if($this->delete_slider())
                    {
                        $data['success'] = 'Slider deleted successfully.';
                        $data['error'] = 'false';
                        echo json_encode($data);
                        exit;
                    }else
                    {
                        $data['success'] = 'false';
                        $data['error'] = 'Slider not updated deleted.Please try again';
                        echo json_encode($data);
                        exit;
                    }
                    break;

			}
		}

	}

	public function edit($id)
	{
		$data = array();
		//$data['categories']  =  $this->Model_category->getAll();
		$data['category']	 = 	$this->Model_category->get($id);
		$data['content'] 	 =  'category/edit';
		$data['class'] = 'category';
		$this->load->view('template',$data);
	}
	private function update()
	{
		$data = array();
		$update_by = array();
		$post_data = $this->input->post();
		//print_r($post_data); exit;
		$keys[] =  'form_type';
		$keys[] =  'id';
		$keys[] =  'eng_title';
		$keys[] =  'eng_desc';
		$keys[] =  'eng_short_desc';
		$keys[] =  'arb_title';
		$keys[] =  'arb_desc';
		$keys[] =  'arb_short_desc';
		$keys[] =  'eng_meta_title';
		$keys[] =  'arb_meta_title';
		$keys[] =  'eng_meta_description';
		$keys[] =  'arb_meta_description';
		$keys[] =  'eng_meta_keyword';
		$keys[] =  'arb_meta_keyword';
		$keys[] =  'tpl_name';
		$keys[] =  'images';
		$keys[] =  'image_id';
		foreach($post_data as $key => $value)
		{

			if(!in_array($key,$keys))
			{
				$data[$key] = $value;
			}
		}

		if(isset($_FILES['banner_image']) && $_FILES['banner_image']['name'] != '')
		{
			$path = 'home/';
			$file_name = uploadImage($path, 'banner_image');
			$data['banner_image'] = $file_name;
		}

		if(isset($_FILES['banner_image2']) && $_FILES['banner_image2']['name'] != '')
		{
			$path = 'home/';
			$file_name = uploadImage($path, 'banner_image2');
			$data['banner_image2'] = $file_name;
		}


		$data['updated_at'] = date('Y-m-d H:i:s');


		$data['updated_by'] = $this->session->userdata['user']['id'];


		$image_id = $this->input->post('image_id');

		if($image_id)
		{
			foreach($image_id as $img_id)
			{
				if(isset($_FILES['image_'.$img_id]) && $_FILES['image_'.$img_id]['name'] != '')
				{
					$path = 'home/';
					$file_name = uploadImage($path, 'image_'.$img_id);
					$dataUpdate['images'] = $file_name;
				}
				else
				{
					$dataUpdate['images'] = $this->input->post('images_'.$img_id);
				}
				$dataUpdate['eng_title'] = $this->input->post('eng_title_'.$img_id);
				$dataUpdate['eng_desc'] = $this->input->post('eng_desc_'.$img_id);
				$dataUpdate['eng_short_desc'] = $this->input->post('eng_short_desc_'.$img_id);
				$dataUpdate['arb_title'] = $this->input->post('arb_title_'.$img_id);
				$dataUpdate['arb_desc'] = $this->input->post('arb_desc_'.$img_id);
				$dataUpdate['arb_short_desc'] = $this->input->post('arb_short_desc_'.$img_id);

				unset($data['eng_title_'.$img_id]);
				unset($data['eng_desc_'.$img_id]);
				unset($data['eng_short_desc_'.$img_id]);
				unset($data['arb_title_'.$img_id]);
				unset($data['arb_desc_'.$img_id]);
				unset($data['arb_short_desc_'.$img_id]);
				unset($data['images_'.$img_id]);

				$update_slider_by['id'] = $img_id;
				$this->Model_home_slider->update($dataUpdate, $update_slider_by);
			}
		}

		$update_by['id'] = $this->input->post('id');
        if(isset($_FILES['discover_more_image']) && $_FILES['discover_more_image']['name'] != '')
        {
            $path = 'home/';
            $file_name = uploadImage($path, 'discover_more_image');
            $data['discover_more_image'] = $file_name;
        }
        $update = $this->Model_home_content->update($data,$update_by);

		//image uploading
		if($_FILES['image']['name'][0] != '')
		{

			// create configuration fields for setting
			if($_FILES['image']['name'][0] != '')
			{
				$config_array['path']       = 'uploads/images/home/';
				$config_array['thumb_path'] = 'uploads/images/thumbs/home/';
				$config_array['height']     = '300'; //image thumb height
				$config_array['width']      = '300'; //image thumb width
				$config_array['field']      = 'image';//this is field name for html form
				$config_array['allowed_types']      = 'gif|png|jpg';//this is field name for html form

				$images = $this->file_upload->uploadFiles($config_array);
			}


			if($images)
			{
				$eng_title = $this->input->post('eng_title');
				$arb_title = $this->input->post('arb_title');
				$eng_short_desc = $this->input->post('eng_short_desc');
				$arb_short_desc = $this->input->post('arb_short_desc');
				$eng_desc = $this->input->post('eng_desc');
				$arb_desc = $this->input->post('arb_desc');
				$i=0;
				foreach($images as $image)
				{
					$sliderData['home_id'] = $this->input->post('id');
					$sliderData['images'] = $image;
					$sliderData['eng_title'] = $eng_title[$i];
					$sliderData['arb_title']  = $arb_title[$i];
					$sliderData['eng_short_desc']  = $eng_short_desc[$i];
					$sliderData['arb_short_desc']  = $arb_short_desc[$i];
					$sliderData['eng_desc']  = $eng_desc[$i];
					$sliderData['arb_desc']  = $arb_desc[$i];
					$sliderData['created_at'] = date('Y-m-d H:i:s');
					$sliderData['created_by'] = $this->session->userdata['user']['id'];
					$sliderData['updated_at'] = date('Y-m-d H:i:s');
					$sliderData['updated_by'] = $this->session->userdata['user']['id'];
					$this->Model_home_slider->save($sliderData);
					$i++;
				}
			}
		}
		// end images save section
		return $update;
	}
	private function save_slider()
	{
		$post_data = $this->input->post();

		unset($post_data['form_type']);
		if(isset($_FILES['eng_slider_image']) && $_FILES['eng_slider_image']['name'] != '')
		{

			$path = 'home/';
			$file_name = uploadImageNew($path, 'eng_slider_image');
			
            $post_data['eng_image'] = $file_name;
            $fullPath = 'uploads/images/'.$path;
            compress_image($fullPath);
		}

		if(isset($_FILES['arb_slider_image']) && $_FILES['arb_slider_image']['name'] != '')
		{
			$path = 'home/';
			$file_name = uploadImageNew($path, 'arb_slider_image');
            $post_data['arb_image'] = $file_name;
            $fullPath = 'uploads/images/'.$path;
            compress_image($fullPath);
		}
		//$post_data['page_name'] = 'home';
        $post_data['created_at'] = date('Y-m-d H:i:s');
//print_r($post_data); exit;
		if(isset($post_data['is_active'])){

			$post_data['is_active'] = '1';
		}else{
			$post_data['is_active'] = '0';
		}

		if(isset($post_data['is_promotion'])){

			$post_data['is_promotion'] = '1';
		}else{
			$post_data['is_promotion'] = '0';
		}

		if(isset($post_data['is_home'])){

			$post_data['is_home'] = '1';
		}else{
			$post_data['is_home'] = '0';
		}
        $save = $this->Model_home_slider->save($post_data);
        // save logs here
        $logData = array(
            'type'=>    'add',
            'section'=> 'Home Slider'
        );
        saveLogs($logData);
		return $save;
	}
	private function update_content()
	{
		$data = array();
		$update_by = array();
		$post_data = $this->input->post();
		$keys[] =  'form_type';
		$keys[] =  'id';
		foreach($post_data as $key => $value)
		{
			if(!in_array($key,$keys))
			{
				$data[$key] = $value;
			}
		}
		$data['updated_at'] = date('Y-m-d H:i:s');
		$data['updated_by'] = $this->session->userdata['user']['id'];
		$update_by['id'] = $this->input->post('id');
        if(isset($_FILES['discover_more_image']) && $_FILES['discover_more_image']['name'] != '')
        {
            $path = 'home/';
            $file_name = uploadImage($path, 'discover_more_image');
            $data['discover_more_image'] = $file_name;
        }
        //echo "<pre>"; print_r($data); exit;
        $update = $this->Model_home_content->update($data,$update_by);
        // save logs here
        $logData = array(
            'type'=>    'update',
            'section'=> 'Home'
        );
        saveLogs($logData);
		return $update;
	}
	private function update_slider()
	{
		$data = array();
		$update_by = array();
		$post_data = $this->input->post();
		$keys[] =  'form_type';
		$keys[] =  'id';
		foreach($post_data as $key => $value)
		{
			if(!in_array($key,$keys))
			{
				$data[$key] = $value;
			}
		}
		$data['updated_at'] = date('Y-m-d H:i:s');
		$data['updated_by'] = $this->session->userdata['user']['id'];
		$update_by['id'] = $this->input->post('id');
        if(isset($_FILES['eng_slider_image']) && $_FILES['eng_slider_image']['name'] != '')
		{
			$path = 'home/';
			$file_name = uploadImageNew($path, 'eng_slider_image');
            $data['eng_image'] = $file_name;
		}

		if(isset($_FILES['arb_slider_image']) && $_FILES['arb_slider_image']['name'] != '')
		{
			$path = 'home/';
			$file_name = uploadImageNew($path, 'arb_slider_image');
            $data['arb_image'] = $file_name;
		}
       	if(isset($data['is_active'])){

			$data['is_active'] = 1;
		}else{
			$data['is_active'] = 0;
		}

		if(isset($data['is_promotion'])){

			$data['is_promotion'] = 1;
		}else{
			$data['is_promotion'] = 0;
		}
		if(isset($data['is_home'])){

			$data['is_home'] = 1;
		}else{
			$data['is_home'] = 0;
		}
        $update = $this->Model_home_slider->update($data,$update_by);
        // save logs here
        $logData = array(
            'type'=>    'update',
            'section'=> 'Home Slider'
        );
        saveLogs($logData);
		return $update;
	}

	public function delete_slider()
	{
		$deleted_by['id'] = $this->input->post('id');
		$image = $this->Model_home_slider->get($this->input->post('id'));
		$delete = $this->Model_home_slider->delete($deleted_by);
        // save logs here
        $logData = array(
            'type'=>    'delete',
            'section'=> 'Home Slider'
        );
        saveLogs($logData);
		if($delete)
		{
			unlink('uploads/images/home/'.$image->images);
			$data['success'] = 'Slider has been deleted successfully.';
			$data['error'] = 'false';
			echo json_encode($data);
			exit;
		}


	}

	private function validation(){
        $errors = array();
		$error_message = "";
		$validation = true;

		if(spacesValidation($this->input->post('eng_title')) == "") {

			$error_message .= "<p>The Eng Title field is required</p>";
			$validation = false;
		}
		if(spacesValidation($this->input->post('arb_title')) == "") {
			$error_message .= "<p>The Arb Title field is required</p>";
			$validation = false;
		}
		if(spacesValidation($this->input->post('eng_desc')) == "") {
			$error_message .= "<p>The Eng description field is required</p>";
			$validation = false;
		}
		if(spacesValidation($this->input->post('arb_desc')) == "") {
			$error_message .= "<p>The Arb description field is required</p>";
			$validation = false;
		}
		/* if (empty($_FILES['eng_slider_image']['name'])){
				$error_message .= "<p>The english slider image field is required</p>";
				$validation = false;
		}else {

            $imageArr = getImageSizeDimension($_FILES['eng_slider_image'],'English slider');
            if (!$imageArr['success']) {
                $error_message .= "<p>" . $imageArr['message'] . "</p>";
                $validation = false;
            }
        } */

		/* if (empty($_FILES['arb_slider_image']['name'])){
				$error_message .= "<p>The arabic slider image field is required</p>";
				$validation = false;
		}else {

            $imageArr = getImageSizeDimension($_FILES['arb_slider_image'],'Arabic slider');
            if (!$imageArr['success']) {
                $error_message .= "<p>" . $imageArr['message'] . "</p>";
                $validation = false;
            }
        } */

        if ($validation == false) {
            $errors['error'] = $error_message;
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }
    }
}
