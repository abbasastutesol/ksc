<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Promotion extends CI_Controller {


	public function __construct()

    {

        parent::__construct();

		
		$this->load->model('Model_promotion');

		$this->load->library('File_upload');

		$this->load->library('form_validation');

		checkAdminSession();
		$this->user_rights_array = user_rights();	


		//$res = checkLevels(2);

		//checkAuth($res);

    }

	

	public function index()

	{
		if($this->user_rights_array['Promotions']['show_p'] == 1) 
		{
			$data = array();
	
	
			$data['promotion'] = $this->Model_promotion->get('1');
	
			$data['content'] = 'promotion/edit';
	
			$data['class'] = 'promotions';
	
			$this->load->view('template',$data);	
		} else {
			show_404();
		}

		

	}



	

	public function action()

	{
		

		if(isset($_POST['form_type']))

		{



			switch($_POST['form_type'])

			{

				case 'update':



					//$this->validation();

					if($this->update())

					{

						$data['success'] = 'Promotion has been updated successfully.';

						$data['error'] = 'false';

						echo json_encode($data);

						exit;

					}else

					{

						$data['success'] = 'false';

						$data['error'] = 'Promotion has not been updated successfully.Please try again';

						echo json_encode($data);

						exit;

					}

										



				break;

				

			}

		}	

		

	}

	

	private function update()

	{

		$data = array();

		$post_data = $this->input->post();



		$keys[] =  'form_type';

		$keys[] =  'id';


		foreach($post_data as $key => $value)

		{

			

			if(!in_array($key,$keys))

			{

				$data[$key] = $value;	

			}

		}

		if(isset($_FILES['image']) && $_FILES['image']['name'] != '')

		{

			$path = 'promotion/';
			$file_name = uploadImage($path);
			$data['banner_image'] = $file_name;

            $fullPath = 'uploads/images/'.$path;
            compress_image($fullPath);

		}  

		

		$data['created_at'] = date('Y-m-d H:i:s');

		$data['created_by'] = $this->session->userdata['user']['id']; 

		$data['updated_at'] = date('Y-m-d H:i:s');

		

				

		$update_by['id'] = $this->input->post('id');


		$update = $this->Model_promotion->update($data,$update_by);


        // save logs here
        $logData = array(
            'type'=>    'update',
            'section'=> 'Promotion'
        );
        saveLogs($logData);

		return $update;

	}
	

}

