<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Contact_us extends CI_Controller {



	public function __construct()

    {

        parent::__construct();

       

	    $this->load->model('Model_contact_us');
		$this->load->model('Model_contact_us_feedback');

		$this->load->model('Model_page');

		checkAdminSession();
		
		$this->user_rights_array = user_rights();

		//$res = checkLevels(2);

		//checkAuth($res);

    }

	

	public function index()

	{
		
		if($this->user_rights_array['Contact Us']['show_p'] == 1) 
		{

			$data = array();
	
			$data['contact_us']	 = 	$this->Model_contact_us->get('1');
	
			$data['content'] = 'admin/contact_us/edit';
	
			$data['class'] = 'contact_us';
	
			$this->load->view('template',$data);	
		} else {
			show_404();
		}
		

	}

	

	public function action()

	{

		

		if(isset($_POST['form_type']))

		{



			switch($_POST['form_type'])

			{

				case 'update':



					//$this->validation();

					if($this->update())

					{

						$data['success'] = 'Contact us has been updated successfully.';

						$data['error'] = 'false';

						echo json_encode($data);

						exit;

					}else

					{

						$data['success'] = 'false';

						$data['error'] = 'Contact us has not been updated successfully.Please try again';

						echo json_encode($data);

						exit;

					}

										



				break;

				

			}

		}	

		

	}

	

	private function update()

	{

		$data = array();

		$post_data = $this->input->post();



		$keys[] =  'form_type';

		$keys[] =  'id';

		

		foreach($post_data as $key => $value)

		{

			

			if(!in_array($key,$keys))

			{

				$data[$key] = $value;	

			}

		}

		

		if(isset($_FILES['image']) && $_FILES['image']['name'] != '')

		{

			

			$path = 'contactus/';

			$file_name = uploadImage($path);

			$data['banner_image'] = $file_name;
            $fullPath = 'uploads/images/'.$path;
            compress_image($fullPath);

		}  

		

		$data['created_at'] = date('Y-m-d H:i:s');

		$data['created_by'] = $this->session->userdata['user']['id']; 

		$data['updated_at'] = date('Y-m-d H:i:s');

		

				

		$update_by['id'] = $this->input->post('id');

        //echo "<pre>"; print_r($data); exit;

		$update = $this->Model_contact_us->update($data,$update_by);


        // save logs here
        $logData = array(
            'type'=>    'update',
            'section'=> 'Contact Us'
        );
        saveLogs($logData);
		

		return $update;

	}

	

	private function validation()

	{

		    $errors = array();

			$this->form_validation->set_error_delimiters('<p>', '</p>');

           

			$this->form_validation->set_rules('eng_description', 'Eng description', 'required');

			$this->form_validation->set_rules('arb_description', 'Arb description', 'required');

			//$this->form_validation->set_rules('eng_content', 'English Content', 'required');

			//$this->form_validation->set_rules('arb_content', 'Arabic Content', 'required');

			

			

			if ($this->form_validation->run() == FALSE)

			{

				$errors['error'] = validation_errors();

				$errors['success'] = 'false';

				echo json_encode($errors);

				exit;

			}else

			{

				return true;

			}

			

	}
	
	
	public function contact_requests()

	{
		if($this->user_rights_array['Contact Us Requests']['show_p'] == 1) 
		{
			$data = array();
	
			$data['feedbacks']	 = 	$this->Model_contact_us_feedback->getAll();

			$data['content'] = 'admin/contact_us/contact_us_requests';
	
			$data['class'] = 'contact_us_request';
	
			$this->load->view('template',$data);	
		} else {
			show_404();
		}
		

	}
	
	public function contact_delete(){
	
		$delete = false;
		
		$deleted_by['id'] = $this->input->post('id'); 

		$delete = $this->Model_contact_us_feedback->delete($deleted_by);
		
		if($delete){

            // save logs here
            $logData = array(
                'type'=>    'delete',
                'section'=> 'Contact Request'
            );
            saveLogs($logData);

			$data['success'] = 'Feedback deleted successfully.';
	
			$data['error'] = 'false';
	
			echo json_encode($data);
	
			exit;

		}else{

			$data['success'] = 'false';
	
			$data['error'] = 'Feedback not deleted successfully. Please try again.';
	
			echo json_encode($data);
	
			exit;
		}
	}
    
	

}

