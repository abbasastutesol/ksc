<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loyalty extends CI_Controller {


	public function __construct()
    {
        parent::__construct();

	    $this->load->model('Model_loyalty');
        $this->user_rights_array = user_rights();
		checkAdminSession();
		//$res = checkLevels(2);
		//checkAuth($res);
    }

	public function index(){

    if($this->user_rights_array['Loyalty']['show_p'] == 1)
    {
        $data = array();
        $data['loyalty']	 = 	$this->Model_loyalty->get('1');
        $data['content'] = 'loyalty/manage';
        $data['class'] = 'loyalty';
        $this->load->view('template',$data);
    } else {
        show_404();
    }
    }
	public function update()
	{
		$update_data = $this->input->post();

		if(isset($update_data['active_discount'])) {
            $update_data['active_discount'] = 1;
        }else{
            $update_data['active_discount'] = 0;
        }
        //echo "<pre>"; print_r($update_data); exit;
		$update_by['id'] = $this->input->post('id');
		$update = $this->Model_loyalty->update($update_data,$update_by);

		if($update)
		{
			$data['success'] = 'Update Loyalty successfully';
			$data['error'] = 'false';

		}else{
            $data['success'] = 'Failed to Update Loyalty';
            $data['error'] = 'true';
        }
        echo json_encode($data);
        exit;

	}
	
	
	
	
}
