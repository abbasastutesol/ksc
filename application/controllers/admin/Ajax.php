<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Ajax extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */

	  public function __construct()

    {

        parent::__construct();

       
		 $this->lang->load("message",$this->session->userdata('site_lang'));
	 	  $this->load->model('Model_product');

	  	  $this->load->model('Model_category');

		  $this->load->model('Model_variant');

		  $this->load->model('Model_variant_value');

		  $this->load->model('Model_product_variant_value');

		  $this->load->model('Model_product_variant_group');

		  $this->load->model('Model_temp_orders');

		  $this->load->model('Model_orders');

		  $this->load->model('Model_order_product');
		  
		  $this->load->model('Model_general');
		  $this->load->model('Model_cart_user_address');
		  $this->load->model('Model_registered_users');
        $this->load->model('Model_home_slider');

		//$res = checkLevels(2);

		//checkAuth($res);

    }	

	public function getProductsByCategory()

	{

		$response = array();

		$category_id = $this->input->post('category_id');

		$result = $this->Model_product->getMultipleRows(array('category_id'=>$category_id));

		if($result)

		{

			foreach($result as $product)

			{

				$response['html'] .= '<option value="'.$product->id.'">'.$product->eng_name.'</option>';

			}

			$response['success'] = 1;

		}

		else

		{

			$response['success'] = 0;

			$response['html'] = '';

		}

		echo json_encode($response);

		exit;

	}

    

	public function updateOrderStatus()

	{

		$response = array();
		$orderData = $this->lang->line('all');
		$lang = $this->session->userdata('site_lang');
		$update_by['id'] = $order_id = $this->input->post('order_id');
		$user_id = $this->input->post('user_id');
		
		
		
		$data['order_status'] = $this->input->post('order_status');
		
		// send email to the user
		if($data['order_status'] == 3 || $data['order_status'] == 4) {
            $this->send_new_email($order_id, $user_id, $data['order_status']);

        }
		// end email
		
		
		

		
			//emailOrderStatus($update_by['id'], $data['order_status']);

			if($data['order_status'] == 0)

			{

				$fetch_by['order_id'] = $this->input->post('order_id');

				$order_product = $this->Model_order_product->getMultipleRows($fetch_by);

				

				foreach($order_product as $ord_pro)

				{

					$product = $this->Model_product->get($ord_pro->product_id);

					$dataProd['eng_quantity'] = ($product->eng_quantity + $ord_pro->quantity);

					$dataProd['arb_quantity'] = ($product->eng_quantity + $ord_pro->quantity);

					

					$dataProd['out_of_stock'] = 0;

					$dataProd['out_of_stock_date'] = '0000-00-00';

					

					$update_product_by['id'] = $ord_pro->product_id;

					

					$this->Model_product->update($dataProd,$update_product_by);

				}
			}

			$result = $this->Model_orders->update($data,$update_by);


		if($result)

		{

			$response['success'] = 1;

		}

		else

		{

			$response['success'] = 0;

			$response['html'] = '';

		}

		echo json_encode($response);

		exit;

	}
	
	
	private function send_new_email($order_id,$user_id,$order_status)
	{ 	
		$data = $this->lang->line('all');

        $data['lang'] = $this->session->userdata('site_lang');


      	$order = $this->Model_orders->get($order_id);
		
			if($order_status ==  "4"){
				$data['order_message'] = $data["delevered_order_success_msg"];
				$data['order_status_label'] = $data["my_order_delivered_label"];
				$data['status'] = "delevered";
			}if($order_status ==  "3"){
				$data['order_message'] = $data["shipped_order_success_msg"];
				$data['order_status_label'] = $data["my_order_shipped_label"];
				$data['status'] = "shipped";
			}
			$data['order_status'] = $order_status;
			
			$data['reg_user'] = $this->Model_registered_users->getSingleRow(array('id'=>$user_id));
			$email = $data['reg_user']->email;

		
		
		$data['user'] = $this->Model_cart_user_address->get($order->address_id);
		
		if($email == '')
		{
			$email = $data['user']->email;
		}
		
		$data['order_detail'] = $order;
		
		$get_by['order_id'] =$order_id;
		
		$data['carts'] = $this->Model_order_product->getMultipleRows($get_by,true);
	
        $products = array();

        foreach ($data['carts'] as $cart){

            $getBy['id'] = $cart['product_id'];

            $prods = $this->Model_product->getSingleRow($getBy,true);

            $products[] = $prods;

        }


	
        $totalQuan = 0;

        foreach ($products as $key => $product) {

            $totalQuan += $data['carts'][$key]['quantity'];

        }

        $data['totalQuantity'] = $totalQuan;

        $data['products'] = $products;
		
		$data['order_id'] = str_pad($order_id, 6, "0", STR_PAD_LEFT);
		
		$data['ord_id'] = $order_id;
		
		$data['type'] = 'cutomer';
	
		//echo "<pre>"; print_r($data); exit;
		if($data['lang'] == "eng") {
			$view = $this->load->view('layouts/emails/eng_order_status',$data,TRUE);
		}else{
			$view = $this->load->view('layouts/emails/arb_order_status',$data,TRUE);
		}
		
		$body = $view;
		
		$subject = $data['order_status_label'].":";
		$headers  = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
		$headers .= $data['register_user_from'].':<IOUD>' . "\r\n";
		if(mail($email,$subject,$body,$headers)){
			
			return true;
		}else{
			
		}
 
     }
	
	
	public function getCities_html()

	{

		$lang = $this->session->userdata('site_lang');
		
		$country_code = $this->input->post('country_code');
		$city_id = $this->input->post('city_id');
		$response['ios'] = strtolower($country->country_code);
		$response['html'] = getCityAdmin($country_code, $city_id);

		echo json_encode($response);

		exit;

	}
	
	public function getCitiesSelectize_html()

	{

		$lang = $this->session->userdata('site_lang');
		
		$country_code = $this->input->post('country_code');
		$city_id = $this->input->post('city_id');
		$results = $this->Model_general->getMultipleRows('city', array('countrycode'=>$country_code), true);
		$city_arr = array();

        $result = sort_by_alphabet($results,'city');
		foreach($result as $res){
			$val = array();
			$val['opvalue'] = $res['id'];
			$val['optext'] = $res['eng_name'];
			$city_arr[] = $val;
		}
		
		$response = $city_arr;
		
		echo json_encode($response);

		exit;

	}
	
	public function getCitiesMultiple_html()

	{
		$cities_arr = array();

		$lang = $this->session->userdata('site_lang');
		
		$country_codes = $this->input->post('country_code');
		
		$city_ids = $this->input->post('city_ids');
		
		$arrCity = getCityAdminArr($country_codes, $city_ids);
		
		$response['html'] = $arrCity;

		echo json_encode($response);

		exit;

	}

	public function rearrangeCategories(){
        $pos = $this->input->post('pos');
        $ids =  $this->input->post('ids');

        $posArr = explode(',',$pos);
        $idsArr = explode(',',$ids);
        foreach($idsArr as $i=> $id) {
            $data['itme_order'] = $posArr[$i];
            $update_by['id'] = $id;
           // echo "id:".$id." Po:".$posArr[$i];

           $this->Model_category->update($data, $update_by);
        }
        echo json_encode(true);
        exit;
    }

    public function rearrangeSlider(){
        $pos = $this->input->post('pos');
        $ids =  $this->input->post('ids');

        $posArr = explode(',',$pos);
        $idsArr = explode(',',$ids);
        foreach($idsArr as $i=> $id) {
            $data['slider_order'] = $posArr[$i];
            $update_by['id'] = $id;
            // echo "id:".$id." Po:".$posArr[$i];

            $this->Model_home_slider->update($data, $update_by);
        }
        echo json_encode(true);
        exit;
    }

    public function getAramexInvoice(){

        $tracking_id = $this->input->post('tracking_id');
        $total_amount = $this->input->post('total_amount');
        $user_id = $this->input->post('user_id');
        $order_id = $this->input->post('order_id');
        $response = aramexPrintLabel($order_id,$tracking_id,$total_amount,$user_id);
        $labelPdf = $response->ShipmentLabel->LabelURL;
        echo json_encode($labelPdf);
    }

}