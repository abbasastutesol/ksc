<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Shipment_groups extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */



	public function __construct()

    {

        parent::__construct();

       

	    $this->load->model('Model_shipment_group_country');

		$this->load->model('Model_shipment_groups');

	    $this->load->model('Model_groups_payment');

	    $this->load->model('Model_general');

		checkAdminSession();
		$this->user_rights_array = user_rights();

		//$res = checkLevels(2);

		//checkAuth($res);

    }

	

	public function index()

	{
		if($this->user_rights_array['Shipment Groups']['show_p'] == 1) 
		{
			$data = array();
	
			
	
			$data['groups'] = $this->Model_shipment_groups->getAll();
	
			$data['content'] = 'shipment_groups/manage';
	
			$data['class'] = 'shipment_groups'; 
	
			$this->load->view('template',$data);	
		} else {
			show_404();
		}
		

	}

	

	public function action()

	{

		

		if(isset($_POST['form_type']))

		{



			switch($_POST['form_type'])

			{



				case 'save':

					$this->validation();

					$data['insert_id'] = $this->save();

					if($data['insert_id'] > 0)

					{

						$data['success'] = 'Group has been created successfully.';

						$data['error'] = 'false';

						$data['reset'] = 1;

						echo json_encode($data);

						exit;

					}

				break;



				case 'delete':

					

					if($this->deleteData())

					{

						$data['success'] = 'Group deleted successfully.';

						$data['error'] = 'false';

						echo json_encode($data);

						exit;

					}else

					{

						$data['success'] = 'false';

						$data['error'] = 'Group not deleted successfully. Please try again.';

						echo json_encode($data);

						exit;

					}

					



				break;



				case 'update':



					$this->validation();

					if($this->update())

					{

						$data['success'] = 'Group has been updated successfully.';

						$data['error'] = 'false';

						echo json_encode($data);

						exit;

					}else

					{

						$data['success'] = 'false';

						$data['error'] = 'Group has not been updated successfully.Please try again';

						echo json_encode($data);

						exit;

					}

										



				break;

				

			}

		}	

		

	}

	

	public function add()

	{
		if($this->user_rights_array['Shipment Groups']['add_p'] == 1) 
		{
			$data = array();
	
			$data['content'] = 'shipment_groups/add';
	
			$data['class'] = 'shipment_groups';
	
			$this->load->view('template',$data);
		}else {
			show_404();
		}
	}

	

	public function edit($id)

	{
		if($this->user_rights_array['Shipment Groups']['edit_p'] == 1) 
		{
			$data = array();
	
			$data['group']	 = 	$this->Model_shipment_groups->get($id);
			$countries	 = 	$this->Model_shipment_group_country->getMultipleRows(array('group_id'=>$id));
			$payment_methods	 = 	$this->Model_groups_payment->getMultipleRows(array('group_id'=>$id));
			
			$cityArr = array();
			$countryArr = array();
			foreach($countries as $country)
			{
				$cityArr[] = $country->city_id;
				$countryArr[] = $country->country_id;
			}
			
			$paymentArr = array();
			foreach($payment_methods as $payment_method)
			{
				$paymentArr[] = $payment_method->payment_type;
			}
			
			$data['countries'] = $countryArr;
			$data['cities'] = $cityArr;
			
			$data['payment_method']	 = 	$paymentArr;
			
			$data['content'] 	 =  'shipment_groups/edit';
	
			$data['class'] = 'shipment_groups';
	
			$this->load->view('template',$data);
		}else {
			show_404();
		}

	}

	

	private function save()

	{

		$data = array();

		$post_data = $this->input->post();

		$keys[] = 'form_type';
		$keys[] = 'country';
		$keys[] = 'scountry';
		$keys[] = 'city';
		$keys[] = 'visa';
		$keys[] = 'sadad';
		$keys[] = 'cash_on_delivery';
		$keys[] = 'transfer';

		

		foreach($post_data as $key => $value)

		{

			

			if(!in_array($key, $keys))

			{

				$data[$key] = $value;	

			}

		}
		if(!isset($data['active_status']))

		{

			$data['active_status'] = '0';

		}

		else

		{

			$data['active_status'] = '1';

		}
		
		$data['created_at'] = date('Y-m-d H:i:s');


		$data['created_by'] = $this->session->userdata['user']['id']; 
		
		
		$insert_id = $this->Model_shipment_groups->save($data);
		
		if($post_data['based_on'] == 1)
		{
			$countries = $post_data['country'];
			$countryArr = array();
			foreach($countries as $country)
			{
				$coun = $this->Model_general->getSingleRow('countries', array('code'=>$country));
				$countryArr['group_id'] = $insert_id;
				$countryArr['country_id'] = $coun->id;
				$this->Model_shipment_group_country->save($countryArr);	
			}
		}
		
		if($post_data['based_on'] == 2)
		{
			$cities = $post_data['city'];
			
			$coun = $this->Model_general->getSingleRow('countries', array('code'=>$post_data['scountry']));
			$country = $coun->id;
			foreach($cities as $city)
			{
				
				$dataC['group_id'] = $insert_id;
				$dataC['country_id'] = $country;
				$dataC['city_id'] = $city;
				$this->Model_shipment_group_country->save($dataC);
			
			}
		}
		
		$dataP['group_id'] = $insert_id;
		
		if(isset($post_data['visa']))
		{
			$dataP['payment_type'] = '1';
			$this->Model_groups_payment->save($dataP);
		}
		
		if(isset($post_data['sadad']))
		{
			$dataP['payment_type'] = '2';
			$this->Model_groups_payment->save($dataP);
		}
		
		if(isset($post_data['cash_on_delivery']))
		{
			$dataP['payment_type'] = '3';
			$this->Model_groups_payment->save($dataP);
		}
		
		if(isset($post_data['transfer']))
		{
			$dataP['payment_type'] = '4';
			$this->Model_groups_payment->save($dataP);
		}


        // save logs here
        $logData = array(
            'type'=>    'add',
            'section'=> 'Group Payment'
        );
        saveLogs($logData);
		
		return $insert_id;

	}

	

	private function update()

	{

		$data = array();

		$update_by = array();

		$post_data = $this->input->post();

		
		$keys[] = 'form_type';
		$keys[] = 'id';
		$keys[] = 'country';
		$keys[] = 'scountry';
		$keys[] = 'city';
		$keys[] = 'visa';
		$keys[] = 'sadad';
		$keys[] = 'cash_on_delivery';
		$keys[] = 'transfer';

		foreach($post_data as $key => $value)

		{

			

			if(!in_array($key, $keys))

			{

				$data[$key] = $value;	

			}

		}

		

		if(!isset($data['active_status']))

		{

			$data['active_status'] = '0';

		}

		else

		{

			$data['active_status'] = '1';

		}		

		$data['updated_at'] = date('Y-m-d H:i:s');

		

		

		$data['updated_by'] = $this->session->userdata['user']['id']; 

		$id = $this->input->post('id');

		$update_by['id'] = $id;		

		$update = $this->Model_shipment_groups->update($data,$update_by);
		
		$deleted_by['group_id'] = $id; 
		
		$this->Model_shipment_group_country->delete($deleted_by);
		
		$this->Model_groups_payment->delete($deleted_by);
		
		if($post_data['based_on'] == 1)
		{
			$countries = $post_data['country'];
			$countryArr = array();
			foreach($countries as $country)
			{
				$coun = $this->Model_general->getSingleRow('countries', array('code'=>$country));
				$countryArr['group_id'] = $id;
				$countryArr['country_id'] = $coun->id;
				$this->Model_shipment_group_country->save($countryArr);	
			}
		}
		
		if($post_data['based_on'] == 2)
		{
			$cities = $post_data['city'];
			
			$coun = $this->Model_general->getSingleRow('countries', array('code'=>$post_data['scountry']));
			$country = $coun->id;
			foreach($cities as $city)
			{
				
				$dataC['group_id'] = $id;
				$dataC['country_id'] = $country;
				$dataC['city_id'] = $city;
				$this->Model_shipment_group_country->save($dataC);
			
			}
		}
		
		$dataP['group_id'] = $id;
		
		if(isset($post_data['visa']))
		{
			$dataP['payment_type'] = '1';
			$this->Model_groups_payment->save($dataP);
		}
		
		if(isset($post_data['sadad']))
		{
			$dataP['payment_type'] = '2';
			$this->Model_groups_payment->save($dataP);
		}
		
		if(isset($post_data['cash_on_delivery']))
		{
			$dataP['payment_type'] = '3';
			$this->Model_groups_payment->save($dataP);
		}
		
		if(isset($post_data['transfer']))
		{
			$dataP['payment_type'] = '4';
			$this->Model_groups_payment->save($dataP);
		}


        // save logs here
        $logData = array(
            'type'=>    'update',
            'section'=> 'Group Payment'
        );
        saveLogs($logData);

		return $update;

	}

	

	private function deleteData()

	{
		$delete = false;

		$deleted_by['id'] = $this->input->post('id'); 

		$delete = $this->Model_shipment_groups->delete($deleted_by);
		
		$deleted_sub_by['group_id'] = $this->input->post('id'); 
		
		$this->Model_shipment_group_country->delete($deleted_sub_by);
		
		$this->Model_groups_payment->delete($deleted_sub_by);

        // save logs here
        $logData = array(
            'type'=>    'delete',
            'section'=> 'Group Payment'
        );
        saveLogs($logData);

		return $delete;

	}


	private function validation()

	{

		    $errors = array();

			$this->form_validation->set_error_delimiters('<p>', '</p>');

           

			$this->form_validation->set_rules('eng_group_name', 'English Group', 'required');
			
			$this->form_validation->set_rules('arb_group_name', 'Arabic Group', 'required');
			$countryMsg = '';
			$post_data = $this->input->post();
			
			if($post_data['based_on'] == 1)
			{
				$countries = $post_data['country'];
				$countryArr = array();
				$i=0;
				foreach($countries as $country)
				{
					$country_ids = array();
					$coun = $this->Model_general->getSingleRow('countries', array('code'=>$country));
					$countryArr['country_id'] = $coun->id;
					$checkCount = $this->Model_shipment_group_country->getRowCount($countryArr);
					if($post_data['id'])
					{
						$groupData = $this->Model_shipment_group_country->getMultipleRows(array('group_id'=>$post_data['id']));
						foreach($groupData as $gd)
						{
							$country_ids[] = $gd->country_id;
						}
					}
					if($checkCount > 0)
					{
						if(!in_array($coun->id, $country_ids))
						{
						
							if($i==0)
							{
								$countryMsg = 'Following contries ';
							}
							$countryMsg .= $coun->eng_country_name.', ';
							if(count($countries)-1 == $i)
							{
								$countryMsg = rtrim($countryMsg, ', ');
								$countryMsg .= ' are already added to another group.';
							}
						}
					}
					$i++;
				}
				
			}
			
			if($post_data['based_on'] == 2)
			{
				$cities = $post_data['city'];
				$country_ids = array();
				$coun = $this->Model_general->getSingleRow('countries', array('code'=>$post_data['scountry']));
				$countryArr['country_id'] = $coun->id;
				$checkCount = $this->Model_shipment_group_country->getRowCount($countryArr);
				if($post_data['id'])
				{
					$groupData = $this->Model_shipment_group_country->getMultipleRows(array('group_id'=>$post_data['id']));
					foreach($groupData as $gd)
					{
						$country_ids[] = $gd->country_id;
					}
				}
				if(!in_array($coun->id, $country_ids))
				{
					if($checkCount > 0)
					{
						$countryMsg .= 'Following contries '.$coun->eng_country_name.' are already added to another group.';
					}
				}
			}
			

			if ($this->form_validation->run() == FALSE)

			{

				$errors['error'] = validation_errors();

				$errors['success'] = 'false';

				echo json_encode($errors);

				exit;

			}elseif($countryMsg != ''){
				
				$errors['error'] = $countryMsg;

				$errors['success'] = 'false';

				echo json_encode($errors);

				exit;
				
			}else

			{

				return true;

			}

			

	}

}

