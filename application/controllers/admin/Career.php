<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Career extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */



	public function __construct()

    {

        parent::__construct();

       

	    $this->load->model('Model_career');

		$this->load->model('Model_career_image');
		
		$this->load->model('Model_Departments');

	    $this->load->model('Model_jobs');

		$this->load->model('Model_job_applied');

		$this->load->library('File_upload');

		/* checkAdminSession();
		$this->user_rights_array = user_rights(); */

		//$res = checkLevels(2);

		//checkAuth($res);

    }

	

	public function index()

	{
		/* if($this->user_rights_array['Career Content']['show_p'] == 1) 
		{ */
			$data = array();
	
			
	
			$data['career'] = $this->Model_career->get('1');
	
			$fetch_by['career_id'] = '1';
	
			$data['content'] = 'career/career';
	
			$data['class'] = 'career'; 
	           
			$this->load->view('template',$data);	
		/* } else {
			show_404();
		} */
		

	}

	

	public function jobs()

	{
		/* if($this->user_rights_array['Jobs']['show_p'] == 1) 
		{ */
			$data = array();
	
			
	
			$data['jobs'] = $this->Model_jobs->getAll();
	
			$data['content'] = 'career/manage';
	
			$data['class'] = 'career_jobs'; 
	
			$this->load->view('template',$data);	
		/* } else {
			show_404();
		} */
		

	}
	
	public function job_dep()

	{
		/* if($this->user_rights_array['Jobs']['show_p'] == 1) 
		{
			 */
			 $data = array();
			 
	        $data['dep'] = $this->Model_Departments->getAllDep();
		 
			$data['content'] = 'career/dep_view';
	
			$data['class'] = 'career_dep'; 
	
			$this->load->view('template',$data);	
		/* } else {
			show_404();
		} */
		

	}
    public function add_dep()

    {
    /* if($this->user_rights_array['Jobs']['show_p'] == 1)
    {
    */
    $data = array();

    //$data['dep'] = $this->Model_Departments->getAllDep();

    $data['content'] = 'career/add_job_dep';

    $data['class'] = 'career_dep';

    $this->load->view('template',$data);
    /* } else {
    show_404();
    } */


    }

    public function edit_dep($id)

    {
    /* if($this->user_rights_array['Jobs']['show_p'] == 1)
    {
    */
    $data = array();

    $data['dep'] = $this->Model_Departments->getDep($id);

    $data['content'] = 'career/edit_job_dep';

    $data['class'] = 'career_dep';

    $this->load->view('template',$data);
    /* } else {
    show_404();
    } */


    }


	public function action()

	{

		

		if(isset($_POST['form_type']))

		{



			switch($_POST['form_type'])

			{



				case 'save':
                    if($_POST['tpl_name'] == "deparments"){
					$this->validation();
					}else{
					$this->jobValidation();	
					}

					$data['insert_id'] = $this->save();

					if($data['insert_id'] > 0)

					{

						$data['success'] = 'Record saved successfully.';

						$data['error'] = 'false';

						$data['reset'] = 1;

						echo json_encode($data);

						exit;

					}

				break;


				case 'delete':

					

					if($this->deleteData())

					{

						$data['success'] = 'Record deleted successfully.';

						$data['error'] = 'false';

						echo json_encode($data);

						exit;

					}else

					{

						$data['success'] = 'false';

						$data['error'] = 'Record not deleted successfully. Please try again.';

						echo json_encode($data);

						exit;

					}

					



				break;

                  case 'DeleteDep':
				  
				  if($this->deleteDep())

					{

						$data['success'] = 'Record deleted successfully.';

						$data['error'] = 'false';

						echo json_encode($data);

						exit;

					}else

					{

						$data['success'] = 'false';

						$data['error'] = 'Record not deleted successfully. Please try again.';

						echo json_encode($data);

						exit;

					}

					



				break;


				case 'update':



					if($_POST['tpl_name'] == "deparments"){
					$this->validation();
					}else{
					$this->jobValidation();	
					}


					if($this->update())

					{

						$data['success'] = 'Record has been updated successfully.';

						$data['error'] = 'false';

						echo json_encode($data);

						exit;

					}else

					{

						$data['success'] = 'false';

						$data['error'] = 'Record has not been updated successfully.Please try again';

						echo json_encode($data);

						exit;

					}

										



				break;

				

				case 'update_career':



					$this->update_career();

										



				break;

				

				case 'deleteApplied':

					

					if($this->deleteDataApplied())

					{

						$data['success'] = 'Application deleted successfully.';

						$data['error'] = 'false';

						echo json_encode($data);

						exit;

					}else

					{

						$data['success'] = 'false';

						$data['error'] = 'Application not deleted successfully. Please try again.';

						echo json_encode($data);

						exit;

					}

					



				break;

				

			}

		}	

		

	}

	

	public function appliedJobs()

	{
		/* if($this->user_rights_array['Job Applicants']['show_p'] == 1) 
		{ */
			$data = array();
	
			$data['applicants'] = $this->Model_job_applied->getAllAppliedJobs();
	
			$data['content'] = 'career/applied_jobs';
	
			$data['class'] = 'applied_jobs';
	
			$this->load->view('template',$data);
		/* } else {
			show_404();
		} */
	}

	

	public function cv()

	{

		$data = array();

		$data['applicants'] = $this->Model_job_applied->getAllCV();

		$data['content'] = 'career/submited_cv';

		$data['class'] = 'submited_cv';

		$this->load->view('template',$data);

	}

	

	public function add()

	{
		/* if($this->user_rights_array['Jobs']['add_p'] == 1) 
		{ */
			$data = array();
			
			$data['dep'] = $this->Model_Departments->getAllDep();
	
			$data['content'] = 'career/add';
	
			$data['class'] = 'career_jobs';
	
			$this->load->view('template',$data);
		/* }else {
			show_404();
		} */

	}

	

	public function edit($id)

	{
		/* if($this->user_rights_array['Jobs']['add_p'] == 1) 
		{ */
			$data = array();
			
	         $data['dep'] = $this->Model_Departments->getAllDep();
			 
			$data['job']  = $this->Model_jobs->get_job($id);
	
			$data['content'] 	 =  'career/edit';
	
			$data['class'] = 'career_jobs';
	
			$this->load->view('template',$data);
		/* }else {
			show_404();
		} */

	}

	

	private function save()

	{

		$data = array();

		$post_data = $this->input->post();

		foreach($post_data as $key => $value)

		{
			if($key != 'form_type')

			{

				$data[$key] = trim($value);	

			}

		}

		$data['created_at'] = date('Y-m-d H:i:s');

		$data['created_by'] = $this->session->userdata['user']['id']; 

		$tpl_name = $this->input->post('tpl_name');
		
		$id = save_meta_data($data,$tpl_name);
		
		$value = unset_meta_date($data);
		
	     $value['page_id'] = $id; 
		 
        if($tpl_name == "deparments"){ 
		
		$insert_id = $this->Model_Departments->save($value);
		
		}else if($tpl_name == 'jobs'){
			
			$insert_id = $this->Model_jobs->save($value);
		}
        // save logs here
        $logData = array(
            'type'=>    'add',
            'section'=> 'Carrer'
        );
        saveLogs($logData);

		return $insert_id;

	}

	

	private function update()

	{

		$data = array();

		$update_by = array();

		$post_data = $this->input->post();

		foreach($post_data as $key => $value)

		{
            if($key != 'form_type' && $key != 'id')

			{

				$data[$key] = trim($value);	

			}

		}

		/* if(!isset($data['active']))

		{

			$data['active'] = '0';

		}

		else

		{

			$data['active'] = '1';

		}		
 */
		$data['updated_at'] = date('Y-m-d H:i:s');

		$data['updated_by'] = $this->session->userdata['user']['id']; 
		
        $tpl_name = $this->input->post('tpl_name');
		
		$update_by['id'] = $this->input->post('id');		

		$id = save_meta_data($data,$tpl_name);
		
		$value = unset_meta_date($data);
		
	     //$value['page_id'] = $id; 
		 
        if($tpl_name == "deparments"){ 
		
		//$insert_id = $this->Model_Departments->save($value);
		$update = $this->Model_Departments->update($value,$update_by);
		
		}else{
			
			$update = $this->Model_jobs->update($value,$update_by);
		}
		
		

        // save logs here
        $logData = array(
            'type'=>    'update',
            'section'=> 'Career'
        );
        saveLogs($logData);

		return $update;

	}

	

	private function update_career()

	{

		$data = array();

		$update_by = array();

		$post_data = $this->input->post();

		

		

		foreach($post_data as $key => $value)

		{

			

			if($key != 'form_type' && $key != 'id' && $key != 'image')

			{

				$data[$key] = $value;	

			}

		}

		$data['updated_at'] = date('Y-m-d H:i:s');

		

		

		$data['updated_by'] = $this->session->userdata['user']['id']; 

		

		$update_by['id'] = $this->input->post('id');		

		$update = $this->Model_career->update($data,$update_by);

		

		if($_FILES['image']['name'][0] != '' && isset($_FILES['image']))

		{

			$config_array['path']       = 'uploads/images/career/';

			$config_array['thumb_path'] = 'uploads/images/thumbs/career/';

			$config_array['height']     = '300'; //image thumb height 

			$config_array['width']      = '300'; //image thumb width 

			$config_array['field']      = 'image';//this is field name for html form

			$config_array['allowed_types']      = 'gif|png|jpg';//this is field name for html form

			$images = $this->file_upload->uploadFiles($config_array);

			$image_lis = '';

			$images_lis_light_boxes = '';

			if($images)

			{

				foreach($images as $image)

				{

					$career_image['career_id'] = $this->input->post('id');

					$career_image['eng_image']  = $image;

					$career_image['arb_image']  = $image;

					$career_image['created_by'] = $this->session->userdata['user']['id'];

					$image_insert_id = $this->Model_career_image->save($career_image);

					$get_image = $this->Model_career_image->get($image_insert_id);

					$image_lis .="<li class='uk-position-relative image-".$get_image->id."'>

                                        <button type='button' class='uk-modal-close uk-close uk-close-alt uk-position-absolute' onClick='deleteRecord(".$get_image->id.",\"admin/career/deleteImage\",\"\");'></button><img src='".base_url().'uploads/images/thumbs/products/'.$get_image->eng_image."' alt='' class='img_small' data-uk-modal='{target:'#modal_lightbox_".$get_image->id."'}'/></li>";

					$images_lis_light_boxes .= "<div class='uk-modal' id='modal_lightbox_".$get_image->id."' ><div class='uk-modal-dialog uk-modal-dialog-lightbox'><button type='button' class='uk-modal-close uk-close uk-close-alt'></button><img src='".base_url()."uploads/images/products/".$get_image->eng_image."' alt=''/></div></div>";

					$i++;

				}

			}

			// end images save section

			$image_fetch = true;

		}

		$data['success'] = 'Career has been updated successfully.';

		$data['error'] = 'false';

		if($image_fetch)

		{

			$data['is_images'] = 'true';

			$data['images_html'] = $image_lis;

			$data['images_light_box_html'] = $images_lis_light_boxes;

		}

		echo json_encode($data);

		exit;

	}

	

	private function deleteData()

	{
		$delete = false;
		$deleted_by['id'] = $this->input->post('id'); 

		$delete = $this->Model_jobs->delete($deleted_by);

        // save logs here
        $logData = array(
            'type'=>    'delete',
            'section'=> 'Career'
        );
        saveLogs($logData);

		return $delete;

		

	}
     
		private function deleteDep()

	{
		$delete = false;
		$deleted_by['id'] = $this->input->post('id'); 

		$delete = $this->Model_Departments->delete($deleted_by);

        // save logs here
        $logData = array(
            'type'=>    'delete',
            'section'=> 'Career'
        );
        saveLogs($logData);

		return $delete;

		

	} 
	

	public function deleteImage()

	{

		

		$deleted_by['id'] = $this->input->post('id');

		$image = $this->Model_career_image->get($this->input->post('id'));

		$delete = $this->Model_career_image->delete($deleted_by);

		if($delete)

		{

			unlink('uploads/images/career/'.$image->eng_image);

			unlink('uploads/images/thumbs/career/'.$image->eng_image);

			$data['success'] = 'image has been deleted successfully.';

			$data['error'] = 'false';

			$data['is_image_delete'] = 'true';

			echo json_encode($data);

			exit;

			

		}

		

		

	}

	

	private function deleteDataApplied()

	{

		$delete = false;

		$deleted_by['id'] = $this->input->post('id'); 

		$delete = $this->Model_job_applied->delete($deleted_by);

        // save logs here
        $logData = array(
            'type'=>    'delete',
            'section'=> 'Applied Jobs'
        );
        saveLogs($logData);

		return $delete;

		

	}

    
	private function validation()

	{

		    $errors = array();
			$error_message = "";
			$validation = true;
			
			if(spacesValidation($this->input->post('eng_title')) == "") {
				
				$error_message .= "<p>The Eng Title field is required</p>";
				$validation = false;
			}
			if(spacesValidation($this->input->post('arb_title')) == "") {
				
				$error_message .= "<p>The Arb Title field is required</p>";
				$validation = false;
			}
			/* if(spacesValidation($this->input->post('eng_description')) == "") {
				
				$error_message .= "<p>The Eng description field is required</p>";
				$validation = false;
			}
			if(spacesValidation($this->input->post('arb_description')) == "") {
				
				$error_message .= "<p>The Arb description field is required</p>";
				$validation = false;
			} */
			/* if(spacesValidation($this->input->post('country')) == "") {
				
				$error_message .= "<p>The Country field is required</p>";
				$validation = false;
			}
			if(spacesValidation($this->input->post('city')) == "") {
				
				$error_message .= "<p>The City field is required</p>";
				$validation = false;
			} */
			

			if ($validation == false)

			{

				$errors['error'] = $error_message;

				$errors['success'] = 'false';

				echo json_encode($errors);

				exit;

			}else

			{
				return true;

			}

			

	}
    
		
	
	private function jobValidation()

	{

		    $errors = array();
			$error_message = "";
			$validation = true;
			
			if(spacesValidation($this->input->post('eng_title')) == "") {
				
				$error_message .= "<p>The Eng Title field is required</p>";
				$validation = false;
			}
			if(spacesValidation($this->input->post('arb_title')) == "") {
				
				$error_message .= "<p>The Arb Title field is required</p>";
				$validation = false;
			}
			 if(spacesValidation($this->input->post('eng_description')) == "") {
				
				$error_message .= "<p>The Eng description field is required</p>";
				$validation = false;
			}
			if(spacesValidation($this->input->post('arb_description')) == "") {
				
				$error_message .= "<p>The Arb description field is required</p>";
				$validation = false;
			} 
			 if(spacesValidation($this->input->post('country')) == "") {
				
				$error_message .= "<p>The Country field is required</p>";
				$validation = false;
			}
			if(spacesValidation($this->input->post('city')) == "") {
				
				$error_message .= "<p>The City field is required</p>";
				$validation = false;
			} 
			

			if ($validation == false)

			{

				$errors['error'] = $error_message;

				$errors['success'] = 'false';

				echo json_encode($errors);

				exit;

			}else

			{
				return true;

			}

			

	}
       
}

