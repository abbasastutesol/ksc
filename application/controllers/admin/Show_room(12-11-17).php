<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Show_room extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
       
	    $this->load->model('Model_contact_us');
		$this->load->model('Model_contact_us_feedback');
		$this->load->model('Model_page');
		$this->load->model('Model_showroom');
		$this->load->model('Model_general');
		/* checkAdminSession();
		
		$this->user_rights_array = user_rights();
 */
		//$res = checkLevels(2);
		//checkAuth($res);
    }
	
	public function index()
	{
		
			$data = array();
			
	            
			$data['records']	 = 	$this->Model_showroom->getAllData();
			$data['content'] = 'admin/show_room/showroom_view';
	
			$data['class'] = 'show_room';
	
			$this->load->view('template',$data);	
	}

	public function add_showRoom()
	{
			$data = array();
			
			$data['content'] = 'admin/show_room/showroom_add';
			
	        $data['cities'] = getCity('Saudi Arabia','');
			 
			$data['class'] = 'show_room';
	
			$this->load->view('template',$data);	
	}

	public function action()
	{
		
		if(isset($_POST['form_type']))
		{
			switch($_POST['form_type'])
			{
               
			   case 'save':
					$this->validation();
					$data['insert_id'] = $this->save();
					if($data['insert_id'] > 0)
					{
						$data['success'] = 'showroom saved successfully.';
						$data['error'] = 'false';
						$data['reset'] = 1;
						echo json_encode($data);
						exit;
					}
				break;
			   
			   
				case 'update':
					$this->validation();
					if($this->update())
					{
						$data['success'] = 'Record has been updated successfully.';
						$data['error'] = 'false';
						echo json_encode($data);
						exit;
					}else
					{
						$data['success'] = 'false';
						$data['error'] = 'Record has not been updated successfully.Please try again';
						echo json_encode($data);
						exit;
					}
										
				break;
                case 'delete':
					
					if($this->showroom_delete())
					{
						$data['success'] = 'Record deleted successfully.';
						$data['error'] = 'false';
						echo json_encode($data);
						exit;
					}else
					{
						$data['success'] = 'false';
						$data['error'] = 'Record not deleted. Please try again.';
						echo json_encode($data);
						exit;
					}
					
				break;
				
			}
		}	
		
	}
	
    public function edit_record($id)
	{         
        $data['record'] = $this->Model_showroom->getSpecficData($id);
        $data['cities'] = getCity('Saudi Arabia','');
        $get_by['showroom_id'] = $id;
        $data['locations'] = $this->Model_general->getMultipleRows("showrooms_location",$get_by);
        $data['content'] = 'admin/show_room/showroom_edit';
        $data['class'] = 'show_room';
        $this->load->view('template',$data);
		
	}
	
	private function update()
	{
		$data = array();
		$post_data = $this->input->post();
        
		$keys[] =  'form_type';
		$keys[] =  'id';
		$keys[] =  'location';
		
		foreach($post_data as $key => $value)
		{
			
			if(!in_array($key,$keys))
			{
				$data[$key] = $value;	
			}
		}
		
		if(isset($_FILES['image']) && $_FILES['image']['name'] != '')
		{
			
			$path = 'images/';
					$file_name = uploadImage($path);
					$data['banner_image'] = $file_name;
					$fullPath = 'assets/frontend/'.$path;
                      compress_image($fullPath);
		}  
		
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['created_by'] = $this->session->userdata['user']['id']; 
		$data['updated_at'] = date('Y-m-d H:i:s');
		$update_by['id'] = $this->input->post('id');
		
		$tpl_name = $this->input->post('tpl_name');

        $id = save_meta_data($data,$tpl_name);
		
		$value = unset_meta_date($data);  
		 //print_r($value); exit;
		$update = $this->Model_showroom->update($value,$update_by);
        $lat_longs = $this->input->post('location');
        $lat_longs = array_filter($lat_longs);
        $where_by['showroom_id'] = $this->input->post('id');
       //display_errors();
        $this->Model_general->delete_where("showrooms_location",$where_by);
        foreach($lat_longs as $lat_long) {
            $locations['lat_long'] = $lat_long;
            $locations['showroom_id'] = $this->input->post('id');
            $this->Model_general->save("showrooms_location",$locations);
        }
        // save logs here
        $logData = array(
            'type'=>    'update',
            'section'=> 'Showroom'
        );
        saveLogs($logData);
		
		return $update;
	}
	private function save()
    {
        //display_errors();
        $data = array();
        $data = $this->input->post();
        unset($data['form_type']);
        unset($data['location']);
        if (isset($_FILES['image']) && $_FILES['image']['name'] != '') {
            $path = 'images/';
            $file_name = uploadImage($path);
            $data['banner_image'] = $file_name;
            $fullPath = 'assets/frontend/' . $path;
            compress_image($fullPath);
        }
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['created_by'] = $this->session->userdata['user']['id'];
        $tpl_name = $this->input->post('tpl_name');

        $id = save_meta_data($data, $tpl_name);
        $value = unset_meta_date($data);
        $value['page_id'] = $id;
        $insert_id = $this->Model_showroom->save($value);
        /*save showroom locations*/
        $lat_longs = $this->input->post('location');
        $lat_longs = array_filter($lat_longs);
        foreach($lat_longs as $lat_long) {
            $locations['showroom_id'] = $insert_id;
            $locations['lat_long'] = $lat_long;
            $this->Model_general->save("showrooms_location",$locations);
        }
        // save logs here
        $logData = array(
            'type'=>    'add',
            'section'=> 'show room'
        );
		
        saveLogs($logData);
		return $insert_id;
	}
	private function validation()
	{
		    $errors = array();
			$this->form_validation->set_error_delimiters('<p>', '</p>');
           
			$this->form_validation->set_rules('eng_title', 'Eng title', 'required');
			$this->form_validation->set_rules('arb_title', 'Arb title', 'required');
			$this->form_validation->set_rules('eng_head_office', 'English Head office', 'required');
			$this->form_validation->set_rules('arb_head_office', 'Arabic Head office', 'required');
            if (count(array_filter($this->input->post('location'))) == 0) {

                $this->form_validation->set_rules('location[]', 'Locations', 'required');
            }

			
			if ($this->form_validation->run() == FALSE)
			{
				$errors['error'] = validation_errors();
				$errors['success'] = 'false';
				echo json_encode($errors);
				exit;
			}else
			{
				return true;
			}
			
	}

	public function showroom_delete(){
	
		$delete = false;
		
		$deleted_by['id'] = $this->input->post('id'); 
		$delete = $this->Model_showroom->delete($deleted_by);
		
		if($delete){
            // save logs here
            $logData = array(
                'type'=>    'delete',
                'section'=> 'showroom'
            );
            saveLogs($logData);
			$data['success'] = 'showroom deleted successfully.';
	
			$data['error'] = 'false';
	
			echo json_encode($data);
	
			exit;
		}else{
			$data['success'] = 'false';
	
			$data['error'] = 'showroom not deleted successfully. Please try again.';
	
			echo json_encode($data);
	
			exit;
		}
	}
    
	
}
