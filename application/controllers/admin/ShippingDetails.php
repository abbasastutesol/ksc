<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ShippingDetails extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
        parent::__construct();
       
	   	$this->load->model('Model_shipping_details');
		checkAdminSession();
		//$res = checkLevels(2);
		//checkAuth($res);
    }
	
	public function action()
	{
		
		if(isset($_POST['form_type']))
		{

			switch($_POST['form_type'])
			{

				case 'save':
					//$this->validation();
					$data['insert_id'] = $this->save();
					if($data['insert_id'] > 0)
					{
						$data['success'] = 'Data has been saved successfully.';
						$data['error'] = 'false';
						$data['reset'] = 1;
						echo json_encode($data);
						exit;
					}
				break;

				case 'delete':
					
					if($this->deleteData())
					{
						$data['success'] = 'Data deleted successfully.';
						$data['error'] = 'false';
						echo json_encode($data);
						exit;
					}else
					{
						$data['success'] = 'false';
						$data['error'] = 'Data not deleted successfully. Please try again.';
						echo json_encode($data);
						exit;
					}
					

				break;

				case 'update':

					//$this->validation();
					if($this->update())
					{
						$data['success'] = 'Data has been updated successfully.';
						$data['error'] = 'false';
						echo json_encode($data);
						exit;
					}else
					{
						$data['success'] = 'false';
						$data['error'] = 'Data has not been updated successfully.Please try again';
						echo json_encode($data);
						exit;
					}
										

				break;
				
				
			}
		}	
		
	}
	
	public function index()
	{
		$data = array();
		$data['shippings']	 = 	$this->Model_shipping_details->getAll();
		$data['content'] = 'shipping/manage';
		$data['class'] = 'shipping'; 
		$this->load->view('template',$data);	
		
	}
	
	public function add()
	{
		$data = array();
		$data['content'] = 'shipping/add';
		$data['class'] = 'shipping'; 
		$this->load->view('template',$data);	
		
	}
	
	public function edit($id)
	{
		$data = array();
		//$data['categories']  =  $this->Model_category->getAll();
		$data['shipping']	 = 	$this->Model_shipping_details->get($id);
		$data['content'] 	 =  'shipping/edit';
		$data['class'] = 'shipping';
		$this->load->view('template',$data);
	}
	
	private function save()
	{
		$data = array();
		$post_data = $this->input->post();
		
		
		foreach($post_data as $key => $value)
		{
			
			if($key != 'form_type')
			{
				$data[$key] = $value;	
			}
		}
		
		$data['created_at'] = date('Y-m-d H:i:s');
		
		
		$data['created_by'] = $this->session->userdata['user']['id']; 
		
				
		$insert_id = $this->Model_shipping_details->save($data);
		return $insert_id;
	}
	
	private function update()
	{
		$data = array();
		$update_by = array();
		$post_data = $this->input->post();
		
		
		foreach($post_data as $key => $value)
		{
			
			if($key != 'form_type')
			{
				$data[$key] = $value;	
			}
		}
		
		$data['updated_at'] = date('Y-m-d H:i:s');
		
		
		$data['updated_by'] = $this->session->userdata['user']['id']; 
		
		$update_by['id'] = $this->input->post('id');		
		$update = $this->Model_shipping_details->update($data,$update_by);
		return $update;
	}
	
	private function deleteData()
	{
		
		$delete = false;
		
		$deleted_by['id'] = $this->input->post('id'); 
		$delete = $this->Model_shipping_details->delete($deleted_by);
		return $delete;
		
	}
	
	
}
