<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller
{


    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_shipment_methods');
        $this->load->model('Model_payment_types');
        $this->load->model('Model_social_media');
        $this->load->model('Model_gifts');
        $this->load->model('Model_settings');
        $this->load->model('Model_order_feedback');
        $this->load->model('Model_wire_transfer');
        $this->load->model('Model_menu_bar');
        checkAdminSession();
        $this->user_rights_array = user_rights();

        //$res = checkLevels(2);
        //checkAuth($res);
    }


    public function action()
    {

        if (isset($_POST['form_type'])) {

            switch ($_POST['form_type']) {

                case 'save':
                    //$this->validation();
                    $this->save_shipment_types();

                    break;

                case 'update':

                    $this->update_shipment_types();
                    break;

                case 'save_gifts':

                    $this->save_gifts();
                    break;

                case 'update_gift':

                    $this->update_gifts();
                    break;

                case 'save_wire':
                    $this->wire_validation();
                    $this->save_wire();
                    break;
                case 'update_wire':

                    $this->update_wire();
                    break;
                
                case 'update_settings':
                    $this->update_settings();
                    break;

                case 'menu_bar':
                    $this->menu_bar_validation();
                    $this->save_menu_bar();
                    break;

                case 'update_menu':
                    $this->menu_bar_validation();
                    $this->update_menu();
                    break;

                case 'delete':
                    $this->delete_menu();
                    break;
            }
        }

    }


    public function index()
    {
        if ($this->user_rights_array['Settings']['show_p'] == 1) {
            $data = array();
            $data['shipments'] = $this->Model_shipment_methods->getAll();
            $data['gifts'] = $this->Model_gifts->getAll();
            $data['payment'] = $this->Model_payment_types->get(1);
            $data['social'] = $this->Model_social_media->get(1);
            $data['api'] = $this->Model_settings->get(1);
            $data['feedback'] = $this->Model_order_feedback->get(1);

            $data['wire_transfer'] = $this->Model_wire_transfer->getAll();

            $data['content'] = 'admin/settings';
            $data['class'] = 'settings';
            $this->load->view('template', $data);
        } else {
            show_404();
        }
    }


    private function save_shipment_types()
    {
        $this->shipment_validation();

        $post_data = $this->input->post();
        unset($post_data['form_type']);
        unset($post_data['id']);
        $this->Model_shipment_methods->save($post_data);

        // save logs here
        $logData = array(
            'type'=>    'add',
            'section'=> 'Shipment Method'
        );
        saveLogs($logData);

        $errors['error'] = false;
        $errors['success'] = 'Shipping methods have been Added successfully';
        echo json_encode($errors);
        exit;
        //redirect($_SERVER['HTTP_REFERER']);
    }

    private function shipment_validation()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('eng_name', 'Eng Title', 'required');
        $this->form_validation->set_rules('arb_name', 'Arb Title', 'required');
        $this->form_validation->set_rules('eng_description', 'Eng Description', 'required');
        $this->form_validation->set_rules('arb_description', 'Arb Description', 'required');

        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }
    }


    public function payment_types()
    {
        $post_data = $this->input->post();
        unset($post_data['tab']);

        if ($post_data['visa'] != null && $post_data['visa'] == 1) {
            $post_data['visa'] = 1;

        } else {
            $post_data['visa'] = 0;

        }
        if ($post_data['sadad'] != null && $post_data['sadad'] == 1) {
            $post_data['sadad'] = 1;
        } else {
            $post_data['sadad'] = 0;
        }

        if ($post_data['cash_on_delivery'] != null && $post_data['cash_on_delivery'] == 1) {
            $post_data['cash_on_delivery'] = 1;
        } else {
            $post_data['cash_on_delivery'] = 0;
        }

        if ($post_data['transfer'] != null && $post_data['transfer'] == 1) {
            $post_data['transfer'] = 1;
        } else {
            $post_data['transfer'] = 0;
        }

        $update_by['id'] = $post_data['id'];

        $update = $this->Model_payment_types->update($post_data, $update_by);

        // save logs here
        $logData = array(
            'type'=>    'update',
            'section'=> 'Payment Types'
        );
        saveLogs($logData);

        redirect($_SERVER['HTTP_REFERER']);
    }


    public function shipmentType()
    {
        $post_data = $this->input->post();

        $update_by['id'] = '1';

        $update = $this->Model_settings->update($post_data, $update_by);

        // save logs here
        $logData = array(
            'type'=>    'update',
            'section'=> 'Shipment Types'
        );
        saveLogs($logData);

        redirect($_SERVER['HTTP_REFERER']);
    }

    private function update()
    {
        $data = array();
        $eng_values = array();
        $arb_values = array();
        $variant_value_ids = array();
        $post_data = $this->input->post();

        $keys[] = 'form_type';
        $keys[] = 'id';

        foreach ($post_data as $key => $value) {

            if (!in_array($key, $keys)) {
                $data[$key] = $value;
            }
        }

        $data['created_at'] = date('Y-m-d H:i:s');
        $data['created_by'] = $this->session->userdata['user']['id'];
        $data['updated_at'] = date('Y-m-d H:i:s');
        $data['updated_by'] = $this->session->userdata['user']['id'];

        $update_by['id'] = $this->input->post('id');
        $update = $this->Model_settings->update($data, $update_by);

        // save logs here
        $logData = array(
            'type'=>    'update',
            'section'=> 'Settings'
        );
        saveLogs($logData);

        return $update;
    }


    public function deleteShipments()
    {
        $data = array();
        $id = $this->input->post('id');
        $delete_by['id'] = $id;
        $delete = $this->Model_shipment_methods->delete($delete_by);

        if ($delete) {

            // save logs here
            $logData = array(
                'type'=>    'delete',
                'section'=> 'Shipment Methods'
            );
            saveLogs($logData);

            $data['error'] = 'false';
            $data['success'] = 'Deleted successfully !';
        } else {
            $data['error'] = 'true';
            $data['success'] = 'There was a problem please try again';
        }
        echo json_encode($data);
        exit;
    }


    public function editShipment()
    {
        $data = array();
        $id = $this->input->post('id');

        $data['shipments'] = $this->Model_shipment_methods->get($id);

        echo json_encode($data);
        exit;
    }

    public function update_shipment_types()
    {

        $data = $this->input->post();
        unset($data['form_type']);
        unset($data['id']);
        $update_by['id'] = $this->input->post('id');
        $update = $this->Model_shipment_methods->update($data, $update_by);
        if ($update) {

            // save logs here
            $logData = array(
                'type'=>    'update',
                'section'=> 'Shipment Methods'
            );
            saveLogs($logData);

            $data['error'] = 'false';
            $data['success'] = 'Updated successfully !';
        } else {
            $data['error'] = 'true';
            $data['success'] = 'There was a problem please try again';
        }
        echo json_encode($data);
        exit;
    }

    public function social_links()
    {

        $data = $this->input->post();
        unset($data['id']);
        $update_by['id'] = $this->input->post('id');
        $update = $this->Model_social_media->update($data, $update_by);
        redirect($_SERVER['HTTP_REFERER']);
    }

    private function save_gifts()
    {

        $this->gifts_validation();

        $post_data = $this->input->post();
        unset($post_data['form_type']);

        $this->Model_gifts->save($post_data);

        // save logs here
        $logData = array(
            'type'=>    'add',
            'section'=> 'Gift Settings'
        );
        saveLogs($logData);

        $errors['error'] = false;
        $errors['success'] = 'Gift have been Added successfully';
        echo json_encode($errors);
        exit;
    }



    private function save_wire()
    {

        //$this->gifts_validation();

        $post_data = $this->input->post();
        unset($post_data['form_type']);

        $this->Model_wire_transfer->save($post_data);

        // save logs here
        $logData = array(
            'type'=>    'add',
            'section'=> 'Wire Transfer Settings'
        );
        saveLogs($logData);

        $errors['error'] = false;
        $errors['success'] = 'Wire Transfer Detail have been Added successfully';
        echo json_encode($errors);
        exit;
    }


    public function deleteGifts()
    {
        $data = array();
        $id = $this->input->post('id');
        $delete_by['id'] = $id;
        $delete = $this->Model_gifts->delete($delete_by);

        if ($delete) {


            // save logs here
            $logData = array(
                'type'=>    'delete',
                'section'=> 'Gifts Settings'
            );
            saveLogs($logData);

            $data['error'] = 'false';
            $data['success'] = 'Deleted successfully !';
        } else {
            $data['error'] = 'true';
            $data['success'] = 'There was a problem please try again';
        }
        echo json_encode($data);
        exit;
    }

    public function deleteWire()
    {
        $data = array();
        $id = $this->input->post('id');
        $delete_by['id'] = $id;
        $delete = $this->Model_wire_transfer->delete($delete_by);

        if ($delete) {


            // save logs here
            $logData = array(
                'type'=>    'delete',
                'section'=> 'Wire Transfer Settings'
            );
            saveLogs($logData);

            $data['error'] = 'false';
            $data['success'] = 'Deleted successfully !';
        } else {
            $data['error'] = 'true';
            $data['success'] = 'There was a problem please try again';
        }
        echo json_encode($data);
        exit;
    }


    public function editGifts()
    {
        $data = array();
        $id = $this->input->post('id');

        $data['gifts'] = $this->Model_gifts->get($id);

        echo json_encode($data);
        exit;
    }



    public function editWire()
    {
        $data = array();
        $id = $this->input->post('id');

        $data['wire'] = $this->Model_wire_transfer->get($id);

        echo json_encode($data);
        exit;
    }

    private function update_gifts()
    {

        $data = $this->input->post();
        unset($data['form_type']);
        unset($data['id']);
        $update_by['id'] = $this->input->post('id');
        $update = $this->Model_gifts->update($data, $update_by);

        if ($update) {


            // save logs here
            $logData = array(
                'type'=>    'update',
                'section'=> 'Gifts Settings'
            );
            saveLogs($logData);

            $data['error'] = 'false';
            $data['success'] = 'Updated successfully !';
        } else {
            $data['error'] = 'true';
            $data['success'] = 'There was a problem please try again';
        }
        echo json_encode($data);
        exit;
    }

    private function update_wire()
    {

        $data = $this->input->post();
        unset($data['form_type']);
        unset($data['id']);
        $update_by['id'] = $this->input->post('id');
        $update = $this->Model_wire_transfer->update($data, $update_by);

        if ($update) {

            // save logs here
            $logData = array(
                'type'=>    'update',
                'section'=> 'Wire Transfer Settings'
            );
            saveLogs($logData);

            $data['error'] = 'false';
            $data['success'] = 'Updated successfully !';
        } else {
            $data['error'] = 'true';
            $data['success'] = 'There was a problem please try again';
        }
        echo json_encode($data);
        exit;
    }

    private function update_settings()
    {

        $data = $this->input->post();

        if(isset($data['chat'])) {

            if (isset($data['tawk_is_active'])) {
                $data['tawk_is_active'] = 1;
            } else {
                $data['tawk_is_active'] = 0;
            }

            unset($data['chat']);
        }

        unset($data['form_type']);
        unset($data['id']);

        $update_by['id'] = $this->input->post('id');


        $update = $this->Model_settings->update($data, $update_by);
        if ($update) {

            // save logs here
            $logData = array(
                'type'=>    'update',
                'section'=> 'Settings'
            );
            saveLogs($logData);

            $result['error'] = 'true';
            $result['success'] = 'Updated successfully !';
        } else {
            $result['error'] = 'false';
            $result['success'] = 'There was a problem please try again';
        }
        echo json_encode($result);
        exit;

    }


    private function gifts_validation()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $this->form_validation->set_rules('eng_name', 'Eng Title', 'required');
        $this->form_validation->set_rules('arb_name', 'Arb Title', 'required');

        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }
    }


    public function feedback()
    {
        $post_data = $this->input->post();

        if ($post_data['product_quality'] != null && $post_data['product_quality'] == 1) {
            $post_data['product_quality'] = 1;

        } else {
            $post_data['product_quality'] = 0;

        }
        if ($post_data['shipment_packaging'] != null && $post_data['shipment_packaging'] == 1) {
            $post_data['shipment_packaging'] = 1;
        } else {
            $post_data['shipment_packaging'] = 0;
        }

        if ($post_data['product_packing'] != null && $post_data['product_packing'] == 1) {
            $post_data['product_packing'] = 1;
        } else {
            $post_data['product_packing'] = 0;
        }

        if ($post_data['customer_service'] != null && $post_data['customer_service'] == 1) {
            $post_data['customer_service'] = 1;
        } else {
            $post_data['customer_service'] = 0;
        }
        if ($post_data['shipment_duration'] != null && $post_data['shipment_duration'] == 1) {
            $post_data['shipment_duration'] = 1;
        } else {
            $post_data['shipment_duration'] = 0;
        }
        if ($post_data['satisfaction'] != null && $post_data['satisfaction'] == 1) {
            $post_data['satisfaction'] = 1;
        } else {
            $post_data['satisfaction'] = 0;
        }

        $update_by['id'] = $post_data['id'];
        $this->Model_order_feedback->update($post_data, $update_by);

        // save logs here
        $logData = array(
            'type'=>    'update',
            'section'=> 'Feedback Settings'
        );
        saveLogs($logData);

        redirect($_SERVER['HTTP_REFERER']);

    }

    public function menu_bar(){

        $data['menu_bars'] = $this->Model_menu_bar->getAll();
        //echo "<pre>"; print_r($data['menu_bars']); exit;
        $data['content'] = 'admin/menu_bar/view';
        $data['class'] = 'menu_bar';
        $this->load->view('template', $data);
    }

    public function add_menu_bar(){

        $data['content'] = 'admin/menu_bar/add';
        $data['class'] = 'menu_bar';
        $this->load->view('template', $data);
    }

    public function edit_menu($id){

        $data['menu'] = $this->Model_menu_bar->get($id);
        $data['content'] = 'admin/menu_bar/edit';
        $data['class'] = 'menu_bar';
        $this->load->view('template', $data);
    }

    private function menu_bar_validation(){
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('eng_menu_title', 'Eng Menu Title', 'required');
        $this->form_validation->set_rules('arb_menu_title', 'Arb Menu Title', 'required');

        /*$this->form_validation->set_rules('eng_link', 'Eng Link', 'required');
        $this->form_validation->set_rules('arb_link', 'Arb Link', 'required');*/

        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }
    }

    private function wire_validation(){
        $errors = array();
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('eng_bank_name', 'Eng Bank Name', 'required');
        $this->form_validation->set_rules('arb_bank_name', 'Arb Bank Name', 'required');

        $this->form_validation->set_rules('account_no', 'Account No', 'required');
        $this->form_validation->set_rules('iban_no', 'IBAN No', 'required');

        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }
    }

    private function save_menu_bar(){
       $data = array();
       $posts = $this->input->post();
       unset($posts['form_type']);

       if(isset($posts['is_active'])){
           $posts['is_active'] = 1;
       }else{
           $posts['is_active'] = 0;
       }
        if(!isset($posts['eng_link'])) $posts['eng_link'] = '';
        if(!isset($posts['arb_link'])) $posts['arb_link'] = '';
        if(!isset($posts['popup_type'])) $posts['popup_type'] = '';

       $this->Model_menu_bar->save($posts);

        // save logs here
        $logData = array(
            'type'=>    'add',
            'section'=> 'Menu Bar'
        );
        saveLogs($logData);

        $result['error'] = 'true';
        $result['success'] = 'Menu Bar Added Successfully !';
        echo json_encode($result);
        exit;
    }


    private function update_menu(){
       $data = array();
       $posts = $this->input->post();
       $id = $this->input->post('id');
       unset($posts['form_type']);

       if(isset($posts['is_active'])){
            $posts['is_active'] = 1;
        }else{
            $posts['is_active'] = 0;
        }

        if(!isset($posts['eng_link'])) $posts['eng_link'] = '';
        if(!isset($posts['arb_link'])) $posts['arb_link'] = '';
        if(!isset($posts['popup_type'])) $posts['popup_type'] = '';

        $updateBy['id'] = $id;

        $this->Model_menu_bar->update($posts,$updateBy);

        // save logs here
        $logData = array(
            'type'=>    'update',
            'section'=> 'Menu Bar'
        );
        saveLogs($logData);

        $result['error'] = 'true';
        $result['success'] = 'Menu Bar Updated Successfully !';
        echo json_encode($result);
        exit;
    }

    public function delete_menu(){

        $data = array();
        $id = $this->input->post('id');
        $delete_by['id'] = $id;
        $delete = $this->Model_menu_bar->delete($delete_by);

        if ($delete) {

            // save logs here
            $logData = array(
                'type'=>    'delete',
                'section'=> 'Menu Bar'
            );
            saveLogs($logData);

            $data['error'] = 'false';
            $data['success'] = 'Deleted successfully !';
        } else {
            $data['error'] = 'true';
            $data['success'] = 'There was a problem please try again';
        }
        echo json_encode($data);
        exit;
    }
}

