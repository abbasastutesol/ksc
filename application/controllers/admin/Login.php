<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
        parent::__construct();
       
	    $this->load->model('Model_users');
		
		//$res = checkLevels(2);
		//checkAuth($res);
    }
	
	public function index()
	{
		
		if($this->session->userdata('user'))
	   {
		    redirect($this->config->item('base_url') . 'admin/dashboard');
		   
	   }else
	   {
		   $this->load->view('admin/login');
	   }
				
		
	}
    
	public function validate()
	{

		$data = array();
		$post_data = $this->input->post();
		$checkUser = false;
		
		foreach($post_data as $key => $value)
		{
			//if($key != 'submit_btn' && $key!='')
			$data[$key] = $value;
		}

		$user = $this->Model_users->validateLogin($data['email'],md5($data['password']),$user_type);
	    
		if($user){
			
			
			$result = array();
			
			
			$this->session->set_userdata('user', $user[0]);
			
			$result['message'] = 'Login Successfully';
			$result['error'] = '0';
			$result['reload'] = '0';
			$result['reset'] = '0';
			$result['redirect'] = '1';
			$this->user_rights_array = user_rights();
			
			if($this->user_rights_array['Dashboard']['show_p'] == 1) 
			{
            	$data['url']      = 'admin/dashboard';
			}
			else
			{
				$data['url']      = 'admin/index/adminProfile';
			}

			echo json_encode($result);
			exit();

		}else{

			$result = array();
			$result['error'] = '1';
			$result['message'] = 'Username or password incorrect.';
			echo json_encode($result);
			exit();
		}

	}

	public function logout()
	{
		$data = array();
		$arr_update = array();
		$user = $this->session->userdata('user');
		$arr_update['id'] = $user['id'];
		//$this->Model_user->update($data,$arr_update);
		$this->session->unset_userdata('user');
		
		redirect($this->config->item('base_url') . 'admin');
		
	}
	
}
