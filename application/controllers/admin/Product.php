<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Product extends CI_Controller {
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Model_product');
        $this->load->model('Model_category');
        $this->load->model('Model_brand');
        $this->load->model('Model_general');
        $this->load->model('Model_variant');
        $this->load->model('Model_product_variant_value');
        $this->load->model('Model_product_variant_group');
        $this->load->model('Model_image');
        $this->load->model('Model_related_product');
        $this->load->library('File_upload');
        $this->load->library('form_validation');
        $this->load->library("pagination");

        $this->user_rights_array = user_rights();
        checkAdminSession();
        //$res = checkLevels(2);
        //checkAuth($res);
    }

    public function index()
    {
        if($this->user_rights_array['Products']['show_p'] == 1) {
            $data = array();
            $search = $this->input->post('product_search_value');

            $prod_cat_id = $_COOKIE['prod_cat_id'];
            $available_products = $_COOKIE['available_product'];

            if ($available_products != NULL) {
                if ($available_products == 1) {
                    $statement = " eng_quantity > 0";
                } else {
                    $statement = " eng_quantity = 0";
                }

                $total_rows = $this->Model_product->getWhereCount($statement);


            } else {
                $total_rows = $this->Model_product->record_count();
            }

            $page = $_GET['per_page'];
            $path = 'admin/product';

            $paginate = productPagination($path, $total_rows);


            if ($prod_cat_id != NULL) {

                $filtered_by['category_id'] = $prod_cat_id;
                $filtered_by['archive'] = 0;
                $data['products'] = $this->Model_product->getMultipleRows($filtered_by);

            } elseif ($available_products != NULL) {

                $data['products'] = $prods = $this->Model_product->getAllProductPaginate($paginate['per_page'], $page, $available_products);
            } else {

                if ($search != null || $search != "") {

                    $data['products'] = $this->Model_product->searchProduct($search, false);

                } else {
                    $data['products'] = $this->Model_product->getAllProductPaginate($paginate['per_page'], $page);
                }
            }

            unset($_COOKIE['prod_cat_id']);
            setcookie('prod_cat_id', '', time() - 3600);

            unset($_COOKIE['product_limits']);
            setcookie('product_limits', '', time() - 3600);

            unset($_COOKIE['available_product']);
            setcookie('available_product', '', time() - 3600);

            $data['categories'] = $this->Model_category->getAll();

            $data["links"] = $this->pagination->create_links();
            $data['limit'] = $paginate['per_page'];
            $data['count'] = $paginate['total_rows'];
            $data['content'] = 'admin/product/manage';
            $data['class'] = 'product';
            $this->load->view('template', $data);

        }else{
            show_404();
        }

    }


    public function action()
    {

        if(isset($_POST['form_type']))
        {
            switch($_POST['form_type'])
            {
                case 'save':
                    //$this->validation();
                    $data['insert_id'] = $this->save();
                    if($data['insert_id'] > 0)
                    {
                        $data['success'] = 'Product has been created successfully.';
                        $data['error'] = 'false';
                        $data['reset'] = 1;
                        echo json_encode($data);
                        exit;
                    }
                    break;
                case 'delete':

                    $this->deleteData();
                    $data['success'] = 'Product deleted successfully.';
                    $data['error'] = 'false';
                    echo json_encode($data);
                    exit;
                    break;
                case 'update':
                    //$this->validation();
                    $this->update();
                    break;

                case 'discount':
                    $this->discount_validation();
                    $this->discount();
                    break;

            }
        }

    }

    private function save()
    {
        $data = array();
        $varient_merge_array = array();
        $post_data = $this->input->post();

        $keys[] =  'form_type';
        $keys[] =  'color_id';

        foreach($post_data as $key => $value)
        {

            if(!in_array($key,$keys))
            {
                $data[$key] = $value;
            }
        }
        debug($data);
        if($post_data['eng_quantity'] == 0)
        {
            $data['out_of_stock'] = 1;
            $data['out_of_stock_date'] = date('Y-m-d');;
        }

        $data['arb_price'] = $post_data['eng_price'];

        $data['arb_quantity'] = $post_data['eng_quantity'];

        $data['total_quantity'] = $post_data['eng_quantity'];

        $data['created_at'] = date('Y-m-d H:i:s');

        // default this will be 1 dont change it
        $data['category_status'] = '1';

        $data['created_by'] = $this->session->userdata['user']['id'];
        $data['start_date'] = date('Y-m-d H:i:s',strtotime(str_replace('/','-',$data['start_date'])));
        //'0000-00-00 00:00:00';
        $data['end_date'] = date('Y-m-d H:i:s',strtotime(str_replace('/','-',$data['end_date'])));

        $insert_id = $this->Model_product->save($data);


        $color_id = $this->input->post("color_id");
        $colors['product_id'] = $insert_id;
        $color_ids = explode(",",$color_id);
        foreach($color_ids as $c_id){
            $colors['color_id'] = $c_id;
            $ids = $this->Model_general->save("product_color",$colors);
        }

        //image uploading
        if($_FILES['image']['name'][0] != '' )
        {
            // create configuration fields for setting
            $config_array['path']   = $file_path   = 'uploads/images/products/';
            //$config_array['thumb_path'] = 'uploads/images/thumbs/products/';
            $config_array['height']     = '300'; //image thumb height
            $config_array['width']      = '300'; //image thumb width
            $config_array['field']      = 'image';//this is field name for html form
            $config_array['allowed_types']      = 'gif|png|jpg';//this is field name for html form

            $images = $this->file_upload->uploadFiles($config_array);
            //echo "<pre>"; print_r($images); exit;
            if($images)
            {
                foreach($images as $image)
                {

                    $product_image['product_id'] = $insert_id;
                    $product_image['eng_image']  = $image;
                    $product_image['arb_image']  = $image;
                    $product_thumbnail['is_thumbnail']  = '0';
                    $product_image['created_at'] = date('Y-m-d H:i:s');
                    $product_image['created_by'] = $this->session->userdata['user']['id'];
                    $this->Model_image->save($product_image);

                    //optimizeImage($file_path,$image);
                    $path = $file_path.$image;
                    compress_image($path);
                }
            }
        }
        //echo "file_name".$image;

        // thumbnail upload

        if($_FILES['thumbnail']['name'][0] != '' )
        {
            // create configuration fields for setting
            //$config_array['path']       = 'uploads/images/products/';
            $config_array['thumb_path'] = $thmb_path = 'uploads/images/thumbs/products/';
            $config_array['height']     = '164'; //image thumb height
            $config_array['width']      = '112'; //image thumb width
            $config_array['field']      = 'thumbnail';//this is field name for html form
            $config_array['allowed_types']      = 'gif|png|jpg';//this is field name for html form

            $thumbnail = $this->file_upload->uploadFiles($config_array);
            if($thumbnail)
            {
                $product_thumbnail['product_id'] = $insert_id;
                $product_thumbnail['thumbnail']  = $thumbnail[0];
                $product_thumbnail['is_thumbnail']  = '1';
                $product_thumbnail['created_at'] = date('Y-m-d H:i:s');
                $product_thumbnail['created_by'] = $this->session->userdata['user']['id'];

                $this->Model_image->save($product_thumbnail);
            }

            $path = $thmb_path.$thumbnail[0];
            compress_image($path);
        }

        $pdf_eng_name = '';
        $pdf_arb_name = '';
        //working here for upload pdfs
        if($_FILES['eng_pdf']['name'] != '') {
            $path = 'images/products/pdf/';
            $file_name = 'eng_pdf';
            $pdf_eng_name = uploadImage($path, $file_name);
            $path = 'asstes/frontend/images/'.$path.$file_name;
            compress_image($path);
        }
        if($_FILES['arb_pdf']['name'] != '') {
            $path = 'images/products/pdf/';
            $file_name = 'arb_pdf';
            $pdf_arb_name = uploadImage($path, $file_name);
            $path = 'asstes/frontend/images/'.$path.$file_name;
            compress_image($path);
        }

        if($pdf_eng_name != '' && $pdf_arb_name != ''){

            $product_pdf['eng_pdf'] = $pdf_eng_name;
            $product_pdf['arb_pdf'] = $pdf_arb_name;
            $product_pdf['product_id'] = $insert_id;
            $product_pdf['is_thumbnail']  = '0';
            $product_pdf['created_at'] = date('Y-m-d H:i:s');
            $product_pdf['created_by'] = $this->session->userdata['user']['id'];
            $this->Model_image->save($product_pdf);
        }
        if($pdf_eng_name != '' && $pdf_arb_name == ''){

            $product_pdf['eng_pdf'] = $pdf_eng_name;
            $product_pdf['product_id'] = $insert_id;
            $product_pdf['is_thumbnail']  = '0';
            $product_pdf['created_at'] = date('Y-m-d H:i:s');
            $product_pdf['created_by'] = $this->session->userdata['user']['id'];
            $this->Model_image->save($product_pdf);
        }
        if($pdf_arb_name != '' && $pdf_eng_name == ''){
            $product_pdf['arb_pdf'] = $pdf_arb_name;
            $product_pdf['product_id'] = $insert_id;
            $product_pdf['is_thumbnail']  = '0';
            $product_pdf['created_at'] = date('Y-m-d H:i:s');
            $product_pdf['created_by'] = $this->session->userdata['user']['id'];
            $this->Model_image->save($product_pdf);
        }





        // end images save section
        $count_array = array();
        $array_count = $this->input->post('array_count');

        $variants_sku = array();
        $variants_price = array();
        $variants_quantity = array();
        $variants_in_stock = array();

        $variants_sku      		 = $this->input->post('eng_sku_variant');
        $variants_price	   		 = $this->input->post('eng_price_variant');
        $variants_discount_price = $this->input->post('eng_discount_price_variant');
        $variants_quantity 		 = $this->input->post('eng_quantity_variant');
        $variants_in_stock 		 = $this->input->post('available_in_stock');

        $i = 0;
        $k = 1;
        $value = array();
        $total = count($array_count);
        foreach($array_count as $val)
        {
            if($total != $k)
            {

                if($_FILES['varient_image']['name'][$i])
                {
                    $path = 'products/';
                    $var_file_name = uploadMultipleImage($path, $_FILES['varient_image'], $i);
                    $value['eng_image'] = $var_file_name;
                    $value['arb_image'] = $var_file_name;
                }
                $value['variant_group_title'] = 'test';
                $value['product_id'] = $insert_id;
                $value['eng_sku'] = $variants_sku[$i];
                $value['arb_sku'] = $variants_sku[$i];
                $value['eng_price'] = $variants_price[$i];
                $value['arb_price'] = $variants_price[$i];
                $value['eng_discount_price'] = $variants_discount_price[$i];
                $value['arb_discount_price'] = $variants_discount_price[$i];
                /*$value['eng_quantity'] = $variants_quantity[$i];
                $value['arb_quantity'] = $variants_quantity[$i];*/
                $value['available_in_stock'] = $variants_in_stock[$i];
                $value['created_at'] = date('Y-m-d H:i:s');
                $value['created_by'] = $this->session->userdata['user']['id'];
                $group_id = $this->Model_product_variant_group->save($value);

                foreach($variants_values as $val)
                {
                    if($val[$i] != '')
                    {
                        $product_variant['product_variant_group_id'] = $group_id;
                        $var_explode = array();
                        $var_explode = explode('|',$val[$i]);
                        $product_variant['variant_id'] = $var_explode[0];
                        $product_variant['variant_value_id'] = $var_explode[1];
                        $product_variant['created_at'] = date('Y-m-d H:i:s');
                        $product_variant['created_by'] = $this->session->userdata['user']['id'];
                        $this->Model_product_variant_value->save($product_variant);

                    }

                }
            }

            $i++;
            $k++;
        }

        // save logs here
        $logData = array(
            'type'=>    'add',
            'section'=> 'Product'
        );
        saveLogs($logData);

        return $insert_id;
    }


    public function add()
    {
        if($this->user_rights_array['Products']['add_p'] == 1) {
            $data = array();

            $data['content'] = 'admin/product/add';

            $get_by['active'] = 'On';
            $data['categories'] = $this->Model_category->getMultipleRows($get_by);
            $data['brands'] = $this->Model_brand->getMultipleRows($get_by);
            $data['colors'] = $this->Model_general->getAll("colors");

            $data['class'] = 'product';

            $this->load->view('template', $data);

        }else{
            show_404();
        }
    }

    public function edit($id)
    {
        if($this->user_rights_array['Products']['edit_p'] == 1) {
            $data = array();
            $fetch_by['product_id'] = $id;
            $data['variants'] = $this->Model_variant->getAll();
            $data['variant_groups'] = $this->Model_product_variant_group->getMultipleRows($fetch_by);
            $fetch_by['is_thumbnail'] = 0;
            $data['images'] = $this->Model_image->getMultipleRows($fetch_by);

            $fetch_thumb['product_id'] = $id;
            $fetch_thumb['is_thumbnail'] = 1;
            //$data['thumbs'] = $this->Model_image->getMultipleRows($fetch_thumb);
            $get_by['active'] = 'On';
            $data['categories'] = $this->Model_category->getMultipleRows($get_by);
            $data['brands'] = $this->Model_brand->getMultipleRows($get_by);
            $data['colors'] = $this->Model_general->getAll("colors");
            $data['product'] = $this->Model_product->get($id);
            $data['content'] = 'admin/product/edit';
            $data['class'] = 'product';
            $this->load->view('template', $data);
        }else{
            show_404();
        }
    }

    public function detail($id)
    {
        if($this->user_rights_array['Products']['view_p'] == 1) {
            $data = array();
            $data['product'] = $this->Model_product->get($id);
            //echo "<pre>"; print_r($data['product']); exit;

            $data['content'] = 'admin/product/detail';
            $data['class'] = 'product';

            $this->load->view('template', $data);
        }else{
            show_404();
        }
    }


    private function update()
    {
        $data = array();
        $update_by = array();
        $post_data = $this->input->post();
        //echo "<pre>"; print_r($post_data); exit;
        $keys[] =  'form_type';
        $keys[] =  'eng_sku_variant';
        $keys[] =  'eng_price_variant';
        $keys[] =  'eng_discount_price_variant';
        $keys[] =  'eng_quantity_variant';
        $keys[] =  'available_in_stock';
        $keys[] =  'array_count';
        $keys[] =  'id';
        $keys[] =  'groups';
        $keys[] =  'related_product';
        $keys[] =  'color_id';

        foreach($post_data as $key => $value)
        {

            if(!in_array($key,$keys))
            {
                $data[$key] = $value;
            }
        }

        $data['start_date'] = date('Y-m-d', strtotime($data['start_date']));
        $data['end_date'] = date('Y-m-d', strtotime($data['end_date']));
        $data['arb_price'] = $post_data['eng_price'];// price for both is same we will get data easily for eng and arb that's why we added seprate price for both.
        $data['arb_quantity'] = $post_data['eng_quantity'];
        $data['total_quantity'] = $post_data['eng_quantity'];
        $data['discount_price'] = $post_data['discount_price'];
        $data['arb_sku'] = $post_data['eng_sku'];
        $data['arb_color'] = $post_data['eng_color'];
        $data['arb_cloth_category'] = $post_data['eng_cloth_category'];
        $data['arb_size_in_image'] = $post_data['eng_size_in_image'];
        $data['arb_model_number_supplier'] = $post_data['eng_model_number_supplier'];
        $data['arb_model_size'] = $post_data['eng_model_size'];
        $data['arb_model_height'] = $post_data['eng_model_height'];
        $file_name = '';

        if($_FILES['slider_image']['name'][0])
        {
            $path = 'products/';
            $file_name = uploadMultipleImage($path, $_FILES['slider_image'], 0);
            $data['eng_slider_image'] = $file_name;
            $data['arb_slider_image'] = $file_name;
        }

        if($post_data['eng_quantity'] == 0)
        {
            $data['out_of_stock'] = 1;
            $data['out_of_stock_date'] = date('Y-m-d');;
        }

        if(!isset($data['active']))
        {
            $data['active'] = '0';
        }
        else
        {
            $data['active'] = '1';
        }
        if(!isset($data['display_to_home_product']))
        {
            $data['display_to_home_product'] = '0';
        }
        else
        {
            $data['display_to_home_product'] = '1';
        }
        if(!isset($data['display_to_home_offer']))
        {
            $data['display_to_home_offer'] = '0';
        }
        else
        {
            $data['display_to_home_offer'] = '1';
        }
        if(!isset($data['daily_offer']))
        {
            $data['daily_offer'] = '0';
        }
        else
        {
            $data['daily_offer'] = '1';
        }

        $data['updated_at'] = date('Y-m-d H:i:s');


        $data['updated_by'] = $this->session->userdata['user']['id'];

        $update_by['id'] = $this->input->post('id');
        $data['start_date'] = date('Y-m-d H:i:s',strtotime(str_replace('/','-',$this->input->post('start_date'))));
        $data['end_date'] = date('Y-m-d H:i:s',strtotime(str_replace('/','-',$this->input->post('end_date'))));

        $update = $this->Model_product->update($data,$update_by);
        $image_fetch = false;
        // end deleting data

        $color_ids = explode(",",$this->input->post("color_id"));
        $colors['product_id'] = $this->input->post('id');
        $c_update_by['product_id'] = $this->input->post('id');
        if(count($color_ids) > 0) {
            $this->Model_general->delete_where("product_color", $colors);
            foreach ($color_ids as $c_id) {
                $colors['color_id'] = $c_id;
                $c_update_by['color_id'] = $c_id;
                $this->Model_general->save("product_color", $colors);
            }
        }


        //image uploading
        // create configuration fields for setting

        if($_FILES['image']['name'][0] != '' && isset($_FILES['image']))
        {
            $config_array['path']    = $file_path  = 'uploads/images/products/';
            //$config_array['thumb_path'] = 'uploads/images/thumbs/products/';
            $config_array['height']     = '300'; //image thumb height
            $config_array['width']      = '300'; //image thumb width
            $config_array['field']      = 'image';//this is field name for html form
            $config_array['allowed_types']      = 'gif|png|jpg';//this is field name for html form
            $images = $this->file_upload->uploadFiles($config_array);
            $image_lis = '';
            $images_lis_light_boxes = '';
            if($images)
            {
                $i=0;
                foreach($images as $image)
                {
                    $product_image['product_id'] = $this->input->post('id');
                    $product_image['eng_image']  = $image;
                    $product_image['arb_image']  = $image;
                    $product_thumbnail['is_thumbnail']  = '0';
                    $product_image['created_at'] = date('Y-m-d H:i:s');
                    $product_image['created_by'] = $this->session->userdata['user']['id'];
                    $image_insert_id = $this->Model_image->save($product_image);
                    $get_image = $this->Model_image->get($image_insert_id);
                    $image_lis .="<li class='uk-position-relative image-".$get_image->id."'>
                                        <button type='button' class='uk-modal-close uk-close uk-close-alt uk-position-absolute' onClick='deleteRecord(".$get_image->id.",\"admin/product/deleteImage\",\"\");'></button><img src='".base_url().'uploads/images/thumbs/products/'.$get_image->eng_image."' alt='' class='img_small' data-uk-modal='{target:'#modal_lightbox_".$get_image->id."'}'/></li>";
                    $images_lis_light_boxes .= "<div class='uk-modal' id='modal_lightbox_".$get_image->id."' ><div class='uk-modal-dialog uk-modal-dialog-lightbox'><button type='button' class='uk-modal-close uk-close uk-close-alt'></button><img src='".base_url()."uploads/images/products/".$get_image->eng_image."' alt=''/></div></div>";
                    $i++;

                    //optimizeImage($file_path,$image);
                    $path = $file_path.$image;
                    compress_image($path);

                }
            }
            // end images save section
            $image_fetch = true;
        }

        // thumbnail


        // thumbnail upload

        if($_FILES['thumbnail']['name'][0] != '' )
        {
            // create configuration fields for setting
            //$config_array['path']       = 'uploads/images/products/';
            $config_array['thumb_path'] = $optPath = 'uploads/images/thumbs/products/';
            $config_array['height']     = '164'; //image thumb height
            $config_array['width']      = '112'; //image thumb width
            $config_array['field']      = 'thumbnail';//this is field name for html form
            $config_array['allowed_types']      = 'gif|png|jpg';//this is field name for html form

            $thumbnail = $this->file_upload->uploadFiles($config_array);


            if($thumbnail)
            {
                $product_thumbnail['product_id'] = $prod_id = $this->input->post('id');
                $product_thumbnail['thumbnail']  = $thumbnail[0];
                $product_thumbnail['is_thumbnail']  = '1';
                $product_thumbnail['created_at'] = date('Y-m-d H:i:s');
                $product_thumbnail['created_by'] = $this->session->userdata['user']['id'];

                $deleted_thumb['product_id'] = $prod_id;
                $deleted_thumb['is_thumbnail'] = '1';
                $thums  = $this->Model_image->getMultipleRows($deleted_thumb);
                $tes = '';
                foreach($thums as $thum){
                    $path = 'uploads/images/thumbs/products/'.$thum->thumbnail;
                    unlink($path);
                }


                $dl = $this->Model_image->delete($deleted_thumb);

                $insert_thumb = $this->Model_image->save($product_thumbnail);
            }

            $path = $optPath.$thumbnail[0];
            compress_image($path);

        }

        // end thumbnail

        $pdf_eng_name = '';
        $pdf_arb_name = '';
        if($_FILES['eng_pdf']['name'] != '') {
            $path = 'images/products/pdf/';
            $file_name = 'eng_pdf';
            $pdf_eng_name = uploadImage($path, $file_name);

            $path = 'asstes/frontend/images/'.$path.$file_name;
            compress_image($path);

        }
        if($_FILES['arb_pdf']['name'] != '') {
            $path = 'images/products/pdf/';
            $file_name = 'arb_pdf';
            $pdf_arb_name = uploadImage($path, $file_name);

            $path = 'asstes/frontend/images/'.$path.$file_name;
            compress_image($path);
        }


        if($pdf_eng_name != '' && $pdf_arb_name != ''){

            $product_pdf['eng_pdf'] = $pdf_eng_name;
            $product_pdf['arb_pdf'] = $pdf_arb_name;
            $product_pdf['product_id'] = $this->input->post('id');
            $product_pdf['is_thumbnail']  = '0';
            $product_pdf['created_at'] = date('Y-m-d H:i:s');
            $product_pdf['created_by'] = $this->session->userdata['user']['id'];

            $deleted_pdfs['product_id'] = $this->input->post('id');
            $deleted_pdfs['is_thumbnail'] = '0';
            $pdfs  = $this->Model_image->getMultipleRows($deleted_pdfs);

            foreach($pdfs as $pdf){

                if($pdf->eng_pdf == '') {
                    $path = 'uploads/images/thumbs/products/' . $pdf->eng_pdf;
                    unlink($path);
                    $del = $this->Model_image->delete($deleted_pdfs);
                    //echo "tests"; debug($del,true,true);
                }
            }
            $this->Model_image->save($product_pdf);

        }
        elseif($pdf_eng_name != '' && $pdf_arb_name == ''){

            $product_pdf['eng_pdf'] = $pdf_eng_name;
            $product_pdf['product_id'] = $this->input->post('id');
            $product_pdf['is_thumbnail']  = '0';
            $product_pdf['created_at'] = date('Y-m-d H:i:s');
            $product_pdf['created_by'] = $this->session->userdata['user']['id'];

            $deleted_pdfs['product_id'] = $this->input->post('id');
            $deleted_pdfs['is_thumbnail'] = '0';
            $pdfs  = $this->Model_image->getMultipleRows($deleted_pdfs);

            foreach($pdfs as $pdf){

                if($pdf->eng_pdf == '') {
                    $path = 'uploads/images/thumbs/products/' . $pdf->eng_pdf;
                    unlink($path);
                    $del = $this->Model_image->delete($deleted_pdfs);
                    //echo "tests"; debug($del,true,true);
                }
            }
            $this->Model_image->save($product_pdf);
        }
        elseif($pdf_arb_name != '' && $pdf_eng_name == ''){

            $product_pdf['arb_pdf'] = $pdf_arb_name;
            $product_pdf['product_id'] = $this->input->post('id');
            $product_pdf['is_thumbnail']  = '0';
            $product_pdf['created_at'] = date('Y-m-d H:i:s');
            $product_pdf['created_by'] = $this->session->userdata['user']['id'];

            $deleted_pdfs['product_id'] = $this->input->post('id');
            $deleted_pdfs['is_thumbnail'] = '0';
            $pdfs  = $this->Model_image->getMultipleRows($deleted_pdfs);

            foreach($pdfs as $pdf){

                if($pdf->eng_pdf == '') {
                    $path = 'uploads/images/thumbs/products/' . $pdf->eng_pdf;
                    unlink($path);
                    $del = $this->Model_image->delete($deleted_pdfs);
                    //echo "tests"; debug($del,true,true);
                }
            }
            $this->Model_image->save($product_pdf);
        }

        $count_array = array();
        $array_count = $this->input->post('array_count');


        if($update)
        {

            // save logs here
            $logData = array(
                'type'=>    'update',
                'section'=> 'Product'
            );
            saveLogs($logData);

            $data['success'] = 'Product has been updated successfully.';
            $data['error'] = 'false';
            if($image_fetch)
            {
                $data['is_images'] = 'true';
                $data['images_html'] = $image_lis;
                $data['images_light_box_html'] = $images_lis_light_boxes;
            }
            echo json_encode($data);
            exit;
        }else
        {
            $data['success'] = 'false';
            $data['error'] = 'Product has not been updated successfully.';
            echo json_encode($data);
            exit;
        }
    }

    private function deleteData()
    {

        $deleted_by['id'] = $this->input->post('id');
        $updateProduct['active'] = 0;
        $updateProduct['archive'] = 1;
        $update = $this->Model_product->update($updateProduct,$deleted_by);
        if($update)
        {
            $fetch_by['product_id']  = $this->input->post('id');
            //$this->Model_image->delete($fetch_by);
            $this->Model_related_product->delete($fetch_by);
            $product_variant_groups = $this->Model_product_variant_group->getMultipleRows($fetch_by);

            if($product_variant_groups)
            {

                $product_group_deleted_by['product_id'] = $this->input->post('id');
                $this->Model_product_variant_group->delete($product_group_deleted_by);
                foreach($product_variant_groups as $value)
                {
                    $product_variant_group_id['product_variant_group_id'] = $value->id;
                    $this->Model_product_variant_value->delete($product_variant_group_id);

                }

            }

        }

        // save logs here
        $logData = array(
            'type'=>    'delete',
            'section'=> 'Product'
        );
        saveLogs($logData);

        return $update;

    }

    public function deleteImage()
    {
        $deleted_by['id'] = $this->input->post('id');

        $image = $this->Model_image->get($this->input->post('id'));
        $delete = $this->Model_image->delete($deleted_by);
        if($delete)
        {
            unlink('uploads/images/products/'.$image->eng_image);
            unlink('uploads/images/thumbs/products/'.$image->eng_image);
            $data['success'] = 'image has been deleted successfully.';
            $data['error'] = false;
            $data['is_image_delete'] = 'true';
            echo json_encode($data);
            exit;

        }


    }

    private function validation()
    {
        $errors = array();
        $error_message = "";
        $validation = true;

        if(spacesValidation($this->input->post('eng_name')) == "") {

            $error_message .= "<p>The Eng Title field is required</p>";
            $validation = false;
        }
        if(spacesValidation($this->input->post('arb_name')) == "") {

            $error_message .= "<p>The Arb Title field is required</p>";
            $validation = false;
        }
        if(spacesValidation($this->input->post('eng_description')) == "") {

            $error_message .= "<p>The Eng description field is required</p>";
            $validation = false;
        }
        if(spacesValidation($this->input->post('arb_description')) == "") {

            $error_message .= "<p>The Arb description field is required</p>";
            $validation = false;
        }
        if(spacesValidation($this->input->post('eng_more_detail')) == "") {

            $error_message .= "<p>The eng more detail field is required</p>";
            $validation = false;
        }
        if(spacesValidation($this->input->post('arb_more_detail')) == "") {

            $error_message .= "<p>The Arb more detail field is required</p>";
            $validation = false;
        }
        if(spacesValidation($this->input->post('category_id')) == "") {

            $error_message .= "<p>The category field is required</p>";
            $validation = false;
        }
        if(spacesValidation($this->input->post('eng_price')) == "" || $this->input->post('eng_price') == 0) {

            $error_message .= "<p>The Price field is required</p>";
            $validation = false;
        }
        if(spacesValidation($this->input->post('eng_quantity')) == "") {

            $error_message .= "<p>The Quantity field is required</p>";
            $validation = false;
        }
        if(spacesValidation($this->input->post('weight_unit')) == "") {
            $error_message .= "<p>The weight unit field is required</p>";
            $validation = false;
        }
        if(spacesValidation($this->input->post('weight_value')) == "") {
            $error_message .= "<p>The weight value field is required</p>";
            $validation = false;
        }
        $discount_amount = $this->input->post('discount_amount');
        if(is_numeric($discount_amount) && $discount_amount != null || $discount_amount == '') {
            if ($this->input->post('discount_amount') >= $this->input->post('eng_price')) {
                $error_message .= "<p>The discount figure could not greater then or equal to original price</p>";
                $validation = false;
            }
        }

        if($this->input->post('daily_offer') != null && $this->input->post('daily_offer') == "1") {
            if($this->input->post('start_date') == "") {
                $error_message .= "<p>The start date field is required</p>";
                $validation = false;
            }
            if($this->input->post('end_date') == "") {
                $error_message .= "<p>The end date field is required</p>";
                $validation = false;
            }

        }
        if($this->input->post('form_type') == "save") {
            if (empty($_FILES['thumbnail']['name'][0])) {
                $error_message .= "<p>The thumbnail field is required</p>";
                $validation = false;
            }
            if (empty($_FILES['image']['name'][0])) {
                $error_message .= "<p>The Product Image field is required</p>";
                $validation = false;
            }
        }

        $checkTempName = preg_match('/\s/',$this->input->post('tpl_name'));

        if($this->input->post('tpl_name') != $this->input->post('old_tpl_name'))
        {
            $fetch_by['tpl_name'] = $this->input->post('tpl_name');

            $checkDup = $this->Model_product->getMultipleRows($fetch_by);
        }
        else
        {
            $checkDup = false;
        }

        if ($validation == false)
        {
            $errors['error'] = $error_message;
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }elseif($checkTempName == 1){
            $errors['error'] = 'There should not be any spaces in template name.';
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }elseif($checkDup){
            $errors['error'] = 'Template name already exist.';
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }elseif($this->input->post('daily_offer') == 1)
        {
            $startDate =  date('Y-m-d', strtotime(str_replace('/','-',$this->input->post('start_date'))));

            $endDate = date('Y-m-d', strtotime(str_replace('/','-',$this->input->post('end_date'))));
            if($startDate > $endDate)
            {
                $errors['error'] = 'Daily Offer start date must be less then end date.';
                $errors['success'] = 'false';
                echo json_encode($errors);
                exit;
            }
            else
            {
                return true;
            }
        }else
        {
            return true;
        }

    }


    private function discount(){
        $data = array();
        $response = array();
        $product_ids = $this->input->post('product_ids');
        $data['discount_type'] = $this->input->post('discount_type');
        $data['discount_amount'] = $this->input->post('discount_amount');

        unset($data['form_type']);
        $prod_ids = rtrim($product_ids,',');
        $prod_ids = explode(',',$prod_ids);
        foreach($prod_ids as $id) {
            $update_by['id'] = $id;
            $update = $this->Model_product->update($data, $update_by);

        }

        // save logs here
        $logData = array(
            'type'=>    'add',
            'section'=> 'Product Discount'
        );
        saveLogs($logData);

        $response['success'] = true;
        $response['message'] = "Discount has been successfully applied";

        echo json_encode($response);
        exit;

    }

    private function discount_validation(){

        $errors = array();
        $error_message = "";
        $validation = true;

        if($this->input->post('product_ids') == "") {

            $error_message .= "<p>Please select at least one product </p>";
            $validation = false;
        }
        if($this->input->post('discount_type') == "") {

            $error_message .= "<p>The discount type field is required</p>";
            $validation = false;
        }
        if($this->input->post('discount_amount') == "") {

            $error_message .= "<p>The discount amount field is required</p>";
            $validation = false;
        }

        if ($validation == false)
        {
            $errors['message'] = $error_message;
            $errors['success'] = false;
            echo json_encode($errors);
            exit;
        }
    }


    public function productNotPurchased()
    {

        $data = array();

        $data['products'] = $this->Model_product->productsNotPurchased($this->input->post());
        $data['content'] = 'product/products_not_purchased';
        $data['class'] = 'products_not_purchased';
        $this->load->view('template',$data);


    }

    public function exportExcelProductsNotPurchased()
    {
        $filename = "ProductsNotPurchased";
        $products = $this->Model_product->productsNotPurchased($this->input->get());
        $this->gen_xl($products, $filename);
    }


    public function exportExcelProducts()
    {
        $filename = "Products";
        $products = $this->Model_product->getAll();
        $this->gen_xl($products, $filename);
    }

    public function gen_xl($products, $filename, $type = 'product')
    {
        $this->load->library('excel');


        $this->excel->getProperties()->setCreator("Soumya Biswas")
            ->setLastModifiedBy("Soumya Biswas")
            ->setTitle("Office 2007 XLSX Test Document")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            ->setKeywords("office 2007 openxml php")
            ->setCategory("Test result file");
        $border = array(
            'borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        $ans = array(
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            )
        );
        $arr = array(
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
            ),
            'font'  => array(
                'bold'  => true,
                "color" => array("rgb" => "903")
            )
        );
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth('20');
        $this->excel->getActiveSheet()->getStyle('A1')->applyFromArray($border);

        $this->excel->getActiveSheet()->setCellValue('A2', 'English Product Name');
        $this->excel->getActiveSheet()->getStyle('A2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('A2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth('40');
        $this->excel->getActiveSheet()->getStyle('A2')->applyFromArray($border);


        $this->excel->getActiveSheet()->setCellValue('B2', 'Arabic Product Name');
        $this->excel->getActiveSheet()->getStyle('B2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('B2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth('40');
        $this->excel->getActiveSheet()->getStyle('B2')->applyFromArray($border);

        $this->excel->getActiveSheet()->setCellValue('C2', 'Product Category');
        $this->excel->getActiveSheet()->getStyle('C2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('C2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth('40');
        $this->excel->getActiveSheet()->getStyle('C2')->applyFromArray($border);

        $this->excel->getActiveSheet()->setCellValue('D2', 'English Description');
        $this->excel->getActiveSheet()->getStyle('D2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('D2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth('50');
        $this->excel->getActiveSheet()->getStyle('D2')->applyFromArray($border);

        $this->excel->getActiveSheet()->setCellValue('E2', 'Arabic Description');
        $this->excel->getActiveSheet()->getStyle('E2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('E2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth('40');
        $this->excel->getActiveSheet()->getStyle('E2')->applyFromArray($border);

        $this->excel->getActiveSheet()->setCellValue('F2', 'Price');
        $this->excel->getActiveSheet()->getStyle('F2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('F2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth('20');
        $this->excel->getActiveSheet()->getStyle('F2')->applyFromArray($border);

        $this->excel->getActiveSheet()->setCellValue('G2', 'Quantity');
        $this->excel->getActiveSheet()->getStyle('G2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('G2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');

        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth('40');
        $this->excel->getActiveSheet()->getStyle('G2')->applyFromArray($border);

        $this->excel->getActiveSheet()->setCellValue('H2', 'English Brief Description');
        $this->excel->getActiveSheet()->getStyle('H2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('H2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth('40');
        $this->excel->getActiveSheet()->getStyle('H2')->applyFromArray($border);

        $this->excel->getActiveSheet()->setCellValue('I2', 'Arabic Brief Description');
        $this->excel->getActiveSheet()->getStyle('I2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('I2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth('40');
        $this->excel->getActiveSheet()->getStyle('I2')->applyFromArray($border);
        $this->excel->getActiveSheet()->setCellValue('J2', 'Created Date');
        $this->excel->getActiveSheet()->getStyle('J2')->applyFromArray($arr);
        $this->excel->getActiveSheet()
            ->getStyle('J2')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setRGB('ededed');
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth('20');
        $this->excel->getActiveSheet()->getStyle('J2')->applyFromArray($border);
        $n = 2;
        foreach($products as $product){


            $n++;



            $this->excel->getActiveSheet()->setCellValue('A'.$n, $product->eng_name);
            $this->excel->getActiveSheet()->getStyle('A'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('A'.$n)->applyFromArray($border);

            $this->excel->getActiveSheet()->setCellValue('B'.$n, $product->arb_name);
            $this->excel->getActiveSheet()->getStyle('B'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('B'.$n)->applyFromArray($border);

            $category = getCategoriesById($product->category_id);

            $this->excel->getActiveSheet()->setCellValue('C'.$n, $category['eng_name']);
            $this->excel->getActiveSheet()->getStyle('C'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('C'.$n)->applyFromArray($border);

            $this->excel->getActiveSheet()->setCellValue('D'.$n, strip_tags($product->eng_description));
            $this->excel->getActiveSheet()->getStyle('D'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('D'.$n)->applyFromArray($border);

            $this->excel->getActiveSheet()->setCellValue('E'.$n, strip_tags($product->arb_description));
            $this->excel->getActiveSheet()->getStyle('E'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('E'.$n)->applyFromArray($border);


            $this->excel->getActiveSheet()->setCellValue('F'.$n, $product->eng_price);
            $this->excel->getActiveSheet()->getStyle('F'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('F'.$n)->applyFromArray($border);

            $this->excel->getActiveSheet()->setCellValue('G'.$n, $product->eng_quantity);
            $this->excel->getActiveSheet()->getStyle('G'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('G'.$n)->applyFromArray($border);


            $this->excel->getActiveSheet()->setCellValue('H'.$n, strip_tags($product->eng_brief_description));
            $this->excel->getActiveSheet()->getStyle('H'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('H'.$n)->applyFromArray($border);

            $this->excel->getActiveSheet()->setCellValue('I'.$n, strip_tags($product->arb_brief_description));
            $this->excel->getActiveSheet()->getStyle('I'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('I'.$n)->applyFromArray($border);
            $this->excel->getActiveSheet()->setCellValue('J'.$n, date('d M Y',strtotime($product->created_at)));
            $this->excel->getActiveSheet()->getStyle('J'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->getStyle('J'.$n)->applyFromArray($border);
        }
        $filename=$filename.".xls";
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');

    }

}
