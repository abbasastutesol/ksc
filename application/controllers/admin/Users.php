<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
        parent::__construct();
       	checkAdminSession();
	  	$this->load->model('Model_registered_users');
		$this->load->model('Model_registered_user_address');
		$this->load->model('Model_cart_user_address');
		$this->load->model('Model_customer_loyalty');
		$this->user_rights_array = user_rights();
    }
	
	public function action()
	{
		
		if(isset($_POST['form_type']))
		{

			switch($_POST['form_type'])
			{
				case 'delete':
					
					$this->deleteData();
					$data['success'] = 'User deleted successfully.';
					$data['error'] = 'false';
					echo json_encode($data);
					exit;

				break;
				
			}
		}	
		
	}
	
	public function index()
	{	
		if($this->user_rights_array['Registered Users']['show_p'] == 1) 
		{
			$data = array();
			$data = $this->lang->line('all');
			$data['lang'] = $this->session->userdata('site_lang');
			$data['users'] = $this->Model_registered_users->getMultipleRows(array('guest' => '0'), false, 'desc');
			$data['content'] = 'admin/user/manage_front_users';
			$data['class'] = 'registered_user';
			$this->load->view('template',$data);
		} else {
			show_404();
		}	
		
	}

	public function view($id)
	{	
		if($this->user_rights_array['Registered Users']['view_p'] == 1) 
		{	
			$data = array();

			$data['user']	 = 	$this->Model_registered_users->get($id);	
			$fetch_by['user_id'] = $id;	
		
			$address	 = 	$this->Model_cart_user_address->getMultipleRows($fetch_by);
            $data['loyalty']	 = 	$this->Model_customer_loyalty->getSingleRow($fetch_by);
			$data['user_address'] = $address;
			$data['content'] = 'admin/user/view';
			$data['class'] = 'registered_user'; 
			$this->load->view('template',$data);
		} else {
			show_404();
		}
		
	}	
	
	public function exportExcelUsers()
	{
		if($this->user_rights_array['Registered Users']['export_p'] == 1) 
		{
			$filename = "Users"; 
			$users = $this->Model_registered_users->getAll();
			$this->gen_xl($users, $filename);
		} else {
			show_404();
		}
	
	}
	
	public function gen_xl($users, $filename, $type = 'users')
	{
		$this->load->library('excel');
		 
		
		$this->excel->getProperties()->setCreator("Soumya Biswas")
                             ->setLastModifiedBy("Soumya Biswas")
                             ->setTitle("Office 2007 XLSX Test Document")
                             ->setSubject("Office 2007 XLSX Test Document")
                             ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
                             ->setKeywords("office 2007 openxml php")
                             ->setCategory("Test result file");				
			$border = array(
					  'borders' => array(
						'outline' => array(
						  'style' => PHPExcel_Style_Border::BORDER_THIN
						)
					  )
			); 				
				$ans = array(
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
                              )
				);	 		 				 
				$arr = array(
						'alignment' => array(
							'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,
					         'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
										  ),
						 'font'  => array(
									'bold'  => true,
									"color" => array("rgb" => "903")
										)
						);




			$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth('20');
			$this->excel->getActiveSheet()->getStyle('A1')->applyFromArray($border);
			
			$this->excel->getActiveSheet()->setCellValue('A2', 'First Name');
			$this->excel->getActiveSheet()->getStyle('A2')->applyFromArray($arr);
			$this->excel->getActiveSheet()
				->getStyle('A2')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setRGB('ededed');
			$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth('20');
			$this->excel->getActiveSheet()->getStyle('A2')->applyFromArray($border);
			
		 
			$this->excel->getActiveSheet()->setCellValue('B2', 'Last Name');
			$this->excel->getActiveSheet()->getStyle('B2')->applyFromArray($arr);
			$this->excel->getActiveSheet()
				->getStyle('B2')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setRGB('ededed');
			$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth('20');
			$this->excel->getActiveSheet()->getStyle('B2')->applyFromArray($border);
			
			$this->excel->getActiveSheet()->setCellValue('C2', 'Email');
			$this->excel->getActiveSheet()->getStyle('C2')->applyFromArray($arr);
			$this->excel->getActiveSheet()
				->getStyle('C2')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setRGB('ededed');
			$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth('20');
			$this->excel->getActiveSheet()->getStyle('C2')->applyFromArray($border);
			
			$this->excel->getActiveSheet()->setCellValue('D2', 'Mobile Number');
			$this->excel->getActiveSheet()->getStyle('D2')->applyFromArray($arr);
			$this->excel->getActiveSheet()
				->getStyle('D2')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setRGB('ededed');
			$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth('40');
			$this->excel->getActiveSheet()->getStyle('D2')->applyFromArray($border);
			
			$this->excel->getActiveSheet()->setCellValue('E2', 'City');
			$this->excel->getActiveSheet()->getStyle('E2')->applyFromArray($arr);
			$this->excel->getActiveSheet()
				->getStyle('E2')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setRGB('ededed');
			$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth('40');
			$this->excel->getActiveSheet()->getStyle('E2')->applyFromArray($border);
			
			$this->excel->getActiveSheet()->setCellValue('F2', 'Country');
			$this->excel->getActiveSheet()->getStyle('F2')->applyFromArray($arr);
			$this->excel->getActiveSheet()
				->getStyle('F2')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setRGB('ededed');
			$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth('20');
			$this->excel->getActiveSheet()->getStyle('F2')->applyFromArray($border);
			
			$this->excel->getActiveSheet()->setCellValue('G2', 'Created Date');
			$this->excel->getActiveSheet()->getStyle('G2')->applyFromArray($arr);
			$this->excel->getActiveSheet()
				->getStyle('G2')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setRGB('ededed');
			$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth('20');
			$this->excel->getActiveSheet()->getStyle('G2')->applyFromArray($border);
			
			
			/*$this->excel->getActiveSheet()->setCellValue('H2', 'Address 1');
			$this->excel->getActiveSheet()->getStyle('H2')->applyFromArray($arr);
			$this->excel->getActiveSheet()
				->getStyle('H2')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setRGB('ededed');
			$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth('20');
			$this->excel->getActiveSheet()->getStyle('H2')->applyFromArray($border);
			
			
			$this->excel->getActiveSheet()->setCellValue('I2', 'Address 2');
			$this->excel->getActiveSheet()->getStyle('I2')->applyFromArray($arr);
			$this->excel->getActiveSheet()
				->getStyle('I2')
				->getFill()
				->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
				->getStartColor()
				->setRGB('ededed');
			$this->excel->getActiveSheet()->getColumnDimension('I')->setWidth('20');
			$this->excel->getActiveSheet()->getStyle('I2')->applyFromArray($border);*/
			
		 $n = 2;

		 foreach($users as $user){
			 
			 $fetch_by['user_id'] = $user->id;	
		 	$regsiter_address = $this->Model_registered_user_address->getMultipleRows($fetch_by); 
			if(!$regsiter_addres){
				$regsiter_addres = array();
			}
			$cart_address = $this->Model_cart_user_address->getMultipleRows($fetch_by);
			
			if(!$cart_address){
				$cart_address = array();
			}
			
			$user_addresses = array_merge($regsiter_address,$cart_address); 
			//echo "<pre>"; print_r($user_addresses); exit;
			
			$n++;
			
			                		    
			$this->excel->getActiveSheet()->setCellValue('A'.$n, $user->first_name);
			$this->excel->getActiveSheet()->getStyle('A'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
			$this->excel->getActiveSheet()->getStyle('A'.$n)->applyFromArray($border);
			
			$this->excel->getActiveSheet()->setCellValue('B'.$n, $user->last_name);
			$this->excel->getActiveSheet()->getStyle('B'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
			$this->excel->getActiveSheet()->getStyle('B'.$n)->applyFromArray($border);
			
			$this->excel->getActiveSheet()->setCellValue('C'.$n, $user->email);
			$this->excel->getActiveSheet()->getStyle('C'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
			$this->excel->getActiveSheet()->getStyle('C'.$n)->applyFromArray($border);
			
			$this->excel->getActiveSheet()->setCellValue('D'.$n, $user->mobile_no);
			$this->excel->getActiveSheet()->getStyle('D'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
			$this->excel->getActiveSheet()->getStyle('D'.$n)->applyFromArray($border);
			
			
			
			$this->excel->getActiveSheet()->setCellValue('E'.$n, getCityById($user->city));
			$this->excel->getActiveSheet()->getStyle('E'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
			$this->excel->getActiveSheet()->getStyle('E'.$n)->applyFromArray($border);
			
			$this->excel->getActiveSheet()->setCellValue('F'.$n, getCoutryByCode($user->country));
			$this->excel->getActiveSheet()->getStyle('F'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
			$this->excel->getActiveSheet()->getStyle('F'.$n)->applyFromArray($border);

             $this->excel->getActiveSheet()->setCellValue('G'.$n, date('d M Y',strtotime($user->created_at)));
             $this->excel->getActiveSheet()->getStyle('G'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
             $this->excel->getActiveSheet()->getStyle('G'.$n)->applyFromArray($border);
			
			$i = 0;
			
			/*foreach($user_addresses as $uadd)
			{	
				if($i > 0)
				{
					$this->excel->getActiveSheet()->setCellValue('G'.$n, '');
					$this->excel->getActiveSheet()->getStyle('G'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
					$this->excel->getActiveSheet()->getStyle('G'.$n)->applyFromArray($border);
												
					$this->excel->getActiveSheet()->setCellValue('H'.$n, '');
					$this->excel->getActiveSheet()->getStyle('H'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
					$this->excel->getActiveSheet()->getStyle('H'.$n)->applyFromArray($border);
					
					$this->excel->getActiveSheet()->setCellValue('I'.$n, '');
					$this->excel->getActiveSheet()->getStyle('I'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
					$this->excel->getActiveSheet()->getStyle('I'.$n)->applyFromArray($border);
				}
				
				$this->excel->getActiveSheet()->setCellValue('G'.$n, $uadd->address_title);
				$this->excel->getActiveSheet()->getStyle('G'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
				$this->excel->getActiveSheet()->getStyle('G'.$n)->applyFromArray($border);
				
				$this->excel->getActiveSheet()->setCellValue('H'.$n, $uadd->adress_1);
				$this->excel->getActiveSheet()->getStyle('H'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
				$this->excel->getActiveSheet()->getStyle('H'.$n)->applyFromArray($border);
				
				$this->excel->getActiveSheet()->setCellValue('I'.$n, $uadd->address_2);
				$this->excel->getActiveSheet()->getStyle('I'.$n)->applyFromArray($ans)->getAlignment()->setWrapText(true);
				$this->excel->getActiveSheet()->getStyle('I'.$n)->applyFromArray($border);
				
				
				$n++;
				$i++;
			}*/
			
		 } 
		 $filename=$filename.".xls";
		 header('Content-Type: application/vnd.ms-excel'); //mime type
   		 header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
   		 header('Cache-Control: max-age=0'); //no cache
	     $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
         $objWriter->save('php://output');
		
	}
	
	private function deleteData()
	{
		
		$deleted_by['id'] = $this->input->post('id');
		$delete = $this->Model_registered_users->delete($deleted_by);
		if($delete)
		{
			$fetch_by['user_id']  = $this->input->post('id'); 
			$this->Model_registered_user_address->delete($fetch_by);
			
		}
		return $delete;
		
	}
	
}
