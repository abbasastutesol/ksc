<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Projects extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */



	public function __construct()

    {

        parent::__construct();

       

	    $this->load->model('Model_career');

		$this->load->model('Model_career_image');

	    $this->load->model('Model_jobs');

		$this->load->model('Model_job_applied');

		$this->load->model('Model_projects');
		
		$this->load->library('File_upload');
		
		$this->load->model('Model_projects_gallery');

		/* checkAdminSession();
		$this->user_rights_array = user_rights(); */

		//$res = checkLevels(2);

		//checkAuth($res);

    }

	

	public function index()

	{
		/* if($this->user_rights_array['Career Content']['show_p'] == 1) 
		{ */
			$data = array();
	
				
			//$data['career'] = $this->Model_career->get('1');
	
			//$fetch_by['career_id'] = '1';
	        $data['records'] = $this->Model_projects->allProjects();
			
			$data['content'] = 'admin/Projects/manage';
	
			$data['class'] = 'projects'; 
	
			$this->load->view('template',$data);	
		/* } else {
			show_404();
		} */
		

	}
	
	public function gallery()

	{
		
			$data = array();
	
	        $data['records'] = $this->Model_projects->Projects_gallery();
			
			$data['content'] = 'admin/Projects/project_gallery_view';
	
			$data['class'] = 'Project_gallery'; 
	
			$this->load->view('template',$data);	

	}
	
	public function add_gallery_image()

	{
		
			$data['content'] = 'admin/Projects/project_gallery_add';
	
			$data['class'] = 'Project_gallery'; 
			
			$data['projects'] = $this->Model_projects->allProjects();
			
			$this->load->view('template',$data);		

	}
	

	public function add_project()

	{         
		     $data['content'] = 'admin/Projects/project_add';
	
			$data['class'] = 'add projects'; 
			
			
			$this->load->view('template',$data);	
		
	}
	
	
	public function edit_project($id)

	{         
		    $Rec = $this->Model_projects->getProjects($id);
			
			$data['imgs'] = explode(",",$Rec[0]->banner_image);
			
			$data['gallery_imgs'] = explode(",",$Rec[0]->gallery_image);
			
			$data['record'] = $Rec;
			
			$data['content'] = 'admin/Projects/project_edit';
	
			$data['class'] = 'projects'; 
			
			$this->load->view('template',$data);	
		
	}

	 public function edit_gallery_project($id)

	{         
		    $data['record'] = $this->Model_projects_gallery->getProjects($id);
			
			$data['projects'] = $this->Model_projects->allProjects();
			
			$data['content'] = 'admin/Projects/project_galler_edit';
	
			$data['class'] = 'projects'; 
			
			$this->load->view('template',$data);	
		
	}

	public function delete_project_image($id,$img)

	{       $id = $this->input->post('id');
	        $image = $this->input->post('img'); 
		    $Rec = $this->Model_projects->getProjects($id);
			
			$imgs = explode(",",$Rec[0]->banner_image);
			
			$gallery_imgs = explode(",",$Rec[0]->gallery_image);
			if (($key = array_search($image, $imgs)) !== false) {
            unset($imgs[$key]);
           }
		  
		   if (($key = array_search($image, $gallery_imgs)) !== false) {
            unset($gallery_imgs[$key]);
           }
		    
		   $update = $this->Model_projects->update(array('banner_image'=>implode(',',$imgs),'gallery_image'=>implode(',',$gallery_imgs)),array('id'=>$id));
		   
		$errors['error'] = false;

		$errors['success'] = 'deleted successfilly';

		echo json_encode($errors);

		exit;
			
	}


	public function action()

	{

		

		if(isset($_POST['form_type']))

		{



			switch($_POST['form_type'])

			{



				case 'save':

					$this->validation();

					$data['insert_id'] = $this->save();

					if($data['insert_id'] > 0)

					{

						$data['success'] = 'Record saved successfully.';

						$data['error'] = 'false';

						$data['reset'] = 1;

						echo json_encode($data);

						exit;

					}

				break;



				case 'delete':

					

					if($this->deleteData())

					{

						$data['success'] = 'Record deleted successfully.';

						$data['error'] = 'false';

						echo json_encode($data);

						exit;

					}else

					{

						$data['success'] = 'false';

						$data['error'] = 'Record not deleted successfully. Please try again.';

						echo json_encode($data);

						exit;

					}

					



				break;



				case 'update':



					$this->validation();

					if($this->update())

					{

						$data['success'] = 'Record updated successfully.';

						$data['error'] = 'false';

						echo json_encode($data);

						exit;

					}else

					{

						$data['success'] = 'false';

						$data['error'] = 'Record has not been updated successfully.Please try again';

						echo json_encode($data);

						exit;

					}

										



				break;

				case 'deletegallery';
				
				if($this->deleteGalleryImage())

					{

						$data['success'] = 'Record deleted successfully.';

						$data['error'] = 'false';

						echo json_encode($data);

						exit;

					}else

					{

						$data['success'] = 'false';

						$data['error'] = 'Record not deleted successfully. Please try again.';

						echo json_encode($data);

						exit;

					}

					



				break;


			}

		}	

		

	}

	



	

	public function edit($id)

	{
		/* if($this->user_rights_array['Jobs']['add_p'] == 1) 
		{ */
			$data = array();
	
			$data['job']	 = 	$this->Model_jobs->get($id);
	
			$data['content'] 	 =  'career/edit';
	
			$data['class'] = 'career_jobs';
	
			$this->load->view('template',$data);
		/* }else {
			show_404();
		} */

	}

	

	private function save()

	{

		$data = array();

		$post_data = $this->input->post();

		foreach($post_data as $key => $value)

		{
			if($key != 'form_type')

			{

				$data[$key] = $value;	

			}

		}

		if($_FILES['image']['name'][0] != '' )
		{
			// create configuration fields for setting
			$config_array['path']   = $file_path   = 'assets/frontend/images/';
			//$config_array['thumb_path'] = 'uploads/images/thumbs/products/';
			$config_array['height']     = '300'; //image thumb height 
			$config_array['width']      = '300'; //image thumb width 
			$config_array['field']      = 'image';//this is field name for html form
			$config_array['allowed_types']      = 'gif|png|jpg';//this is field name for html form
			
			$images = $this->file_upload->uploadFiles($config_array); 
			$data['banner_image'] = implode(',',$images);
		}
		
		
		if(isset($_FILES['gallery_image']) && $_FILES['gallery_image']['name'] != '')

				{

					$path = 'images/';

					$file_name = uploadImage2($path);
					
					$data['gallery_image'] = $file_name;
					$fullPath = 'assets/frontend/'.$path;
					
					compress_image($fullPath);

				}  
            
		$data['created_at'] = date('Y-m-d H:i:s');
		
        $tpl_name = $this->input->post('tpl_name');

		if($tpl_name != "projects_gallery"){
	    $data['created_by'] = $this->session->userdata['user']['id']; 
	    $id = save_meta_data($data,'projects');
		$value = unset_meta_date($data);
	     $value['page_id'] = $id;   
		$insert_id = $this->Model_projects->save($value);
		
		$logData = array(
            'type'=>    'add',
            'section'=> 'projects'
        );
		}else{ 
			$value = unset_meta_date($data);
			$insert_id = $this->Model_projects_gallery->save($value);
			
			$logData = array(
            'type'=>    'add',
            'section'=> 'projects gallery'
        );
		}
        // save logs here
        
		
        saveLogs($logData);

		return $insert_id;

	}

	

	private function update()

	{

		$data = array();

		$update_by = array();

		$post_data = $this->input->post();
		
		$tpl_name = $this->input->post('tpl_name');
		
		$banner_image_old = $this->input->post('banner_image_old');
		
		$gallery_image_old = $this->input->post('gallery_image_old');

		foreach($post_data as $key => $value)

		{
            if($key != 'form_type' && $key != 'id')

			{

				$data[$key] = $value;	

			}

		}

			
				if($_FILES['image']['name'][0] != '' )
		{
			// create configuration fields for setting
			$config_array['path']   = $file_path   = 'assets/frontend/images/';
			$config_array['field']      = 'image';//this is field name for html form
			$config_array['allowed_types']      = 'gif|png|jpg';//this is field name for html form
			
			$images = $this->file_upload->uploadFiles($config_array);
           $str = implode(',',$images);			
			$data['banner_image'] = $str.','.$banner_image_old;
		}
		
            if(isset($_FILES['gallery_image']) && $_FILES['gallery_image']['name'] != '')

				{

					$path = 'images/';

					$file_name = uploadImage2($path);
					
					$data['gallery_image'] = $file_name;
					$fullPath = 'assets/frontend/'.$path;
					
					compress_image($fullPath);

				}

		$data['updated_at'] = date('Y-m-d H:i:s');
		
			if($tpl_name != "projects_gallery"){
				$data['updated_by'] = $this->session->userdata['user']['id']; 
					  //unset($data['gallery_image_old']);
					  unset($data['banner_image_old']);
					$update_by['id'] = $this->input->post('id');		
					$id = save_meta_data($data,$tpl_name);
					$value = unset_meta_date($data);
					 //$value['page_id'] = $id;
					//print_r($value); exit;
					$update = $this->Model_projects->update($value,array('id'=>$this->input->post('id')));

					// save logs here
					$logData = array(
						'type'=>    'update',
						'section'=> 'clients'
					);
			}else{

					 
					$value = unset_meta_date($data);
					 //print_r($value); exit
					$update = $this->Model_projects_gallery->update($value,array('id'=>$this->input->post('id')));

					// save logs here
					$logData = array(
						'type'=>    'update',
						'section'=> 'projects'
					);
			}
        saveLogs($logData);

		return $update;

	}

	

	

	

	private function deleteData()

	{
		$delete = false;
		$deleted_by['id'] = $this->input->post('id'); 

		$delete = $this->Model_projects->delete($deleted_by);

        // save logs here
        $logData = array(
            'type'=>    'delete',
            'section'=> 'Projects'
        );
        saveLogs($logData);

		return $delete;

		

	}
	
	private function deleteGalleryImage()

	{
		$delete = false;
		$deleted_by['id'] = $this->input->post('id'); 

		$delete = $this->Model_projects_gallery->delete($deleted_by);

        // save logs here
        $logData = array(
            'type'=>    'delete',
            'section'=> 'Projects gallery'
        );
		
        saveLogs($logData);

		return $delete;

	}

	


    
	private function validation()

	{

		    $errors = array();
			$error_message = "";
			$validation = true;
			
			if(spacesValidation($this->input->post('eng_title')) == "") {
				
				$error_message .= "<p>The Eng Title field is required</p>";
				$validation = false;
			}
			if(spacesValidation($this->input->post('arb_title')) == "") {
				
				$error_message .= "<p>The Arb Title field is required</p>";
				$validation = false;
			}
			if(spacesValidation($this->input->post('eng_description')) == "") {
				
				$error_message .= "<p>The Eng Description field is required</p>";
				$validation = false;
			}
			if(spacesValidation($this->input->post('arb_description')) == "") {
				
				$error_message .= "<p>The Arb Description field is required</p>";
				$validation = false;
			}
				if(isset($_POST['project_id'])){
			if(spacesValidation($this->input->post('project_id')) == "") {
				
				$error_message .= "<p>The Project field is required</p>";
				$validation = false;
			}
				}
			if ($validation == false)

			{

				$errors['error'] = $error_message;

				$errors['success'] = 'false';

				echo json_encode($errors);

				exit;

			}else

			{
				return true;

			}

			

	}


}

