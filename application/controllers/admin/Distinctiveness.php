<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Distinctiveness extends CI_Controller {





	public function __construct()

    {

        parent::__construct();

       

	    $this->load->model('Model_Distinctiveness');

		$this->load->model('Model_page');

		checkAdminSession();
		
		$this->user_rights_array = user_rights();

		//$res = checkLevels(2);

		//checkAuth($res);

    }

	

	public function index()

	{
		if($this->user_rights_array['Our Distinctiveness']['show_p'] == 1) 
		{

			$data = array();
	
			$data['distinctiveness'] = $this->Model_Distinctiveness->get('1');
	
			$data['content'] = 'aboutus/distinctiveness_edit';
	
			$data['class'] = 'distinctiveness';
	
			$this->load->view('template',$data);	

		} else {
			show_404();
		}

	}

	

	public function action()

	{

		

		if(isset($_POST['form_type']))

		{



			switch($_POST['form_type'])

			{

				case 'update':



					$this->validation();

					if($this->update())

					{

						$data['success'] = 'Distinctiveness has been updated successfully.';

						$data['error'] = 'false';

						echo json_encode($data);

						exit;

					}else

					{

						$data['success'] = 'false';

						$data['error'] = 'Distinctiveness has not been updated successfully.Please try again';

						echo json_encode($data);

						exit;

					}

										



				break;

				

			}

		}	

		

	}

	

	private function update()

	{

		$data = array();

		$eng_values = array();

		$arb_values = array();

		$variant_value_ids = array();

		$post_data = $this->input->post();

		

		$keys[] =  'form_type';

		$keys[] =  'id';

		$keys[] =  'eng_meta_title';

		$keys[] =  'arb_meta_title';

		$keys[] =  'eng_meta_description';

		$keys[] =  'arb_meta_description';

		$keys[] =  'eng_meta_keyword';

		$keys[] =  'arb_meta_keyword';

		$keys[] =  'tpl_name';

		

		$pageCheck = $this->Model_page->getRowCount(array('id'=>$post_data['home_page_id']));

		

		$pageData['eng_meta_title'] = $post_data['eng_meta_title'];

		$pageData['eng_meta_description'] = $post_data['eng_meta_description'];

		$pageData['eng_meta_keyword'] = $post_data['eng_meta_keyword'];

		$pageData['arb_meta_title'] = $post_data['arb_meta_title'];

		$pageData['arb_meta_description'] = $post_data['arb_meta_description'];

		$pageData['arb_meta_keyword'] = $post_data['arb_meta_keyword'];

		$pageData['tpl_name'] = $post_data['tpl_name'];

		

		if($pageCheck > 0)

		{

			$page_update_by['id'] = $post_data['home_page_id'];

			$update = $this->Model_page->update($pageData,$page_update_by);

		}

		else

		{

			$post_data['home_page_id'] = $this->Model_page->save($pageData);

		}

		

		foreach($post_data as $key => $value)

		{

			

			if(!in_array($key,$keys))

			{

				$data[$key] = $value;	

			}

		}

		

		if(isset($_FILES['image']) && $_FILES['image']['name'] != '')

		{

			

			$path = 'aboutus/';

			$file_name = uploadImage($path);

			$data['banner_image'] = $file_name;	

		}  

		

		$data['created_at'] = date('Y-m-d H:i:s');

		$data['created_by'] = $this->session->userdata['user']['id']; 

		$data['updated_at'] = date('Y-m-d H:i:s');

		

				

		$update_by['id'] = $this->input->post('id');		

		$update = $this->Model_Distinctiveness->update($data,$update_by);

        // save logs here
        $logData = array(
            'type'=>    'update',
            'section'=> 'Distinctiveness'
        );
        saveLogs($logData);

		

		return $update;

	}

	

	private function validation()

	{

		    $errors = array();

			$this->form_validation->set_error_delimiters('<p>', '</p>');

           

			$this->form_validation->set_rules('eng_description', 'Eng description', 'required');

			$this->form_validation->set_rules('arb_description', 'Arb description', 'required');

			//$this->form_validation->set_rules('eng_content', 'English Content', 'required');

			//$this->form_validation->set_rules('arb_content', 'Arabic Content', 'required');

			

			

			if ($this->form_validation->run() == FALSE)

			{

				$errors['error'] = validation_errors();

				$errors['success'] = 'false';

				echo json_encode($errors);

				exit;

			}else

			{

				return true;

			}

			

	}
	

}

