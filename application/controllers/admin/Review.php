<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Review extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
        parent::__construct();
       
	   	$this->load->model('Model_review');
		checkAdminSession();
		//$res = checkLevels(2);
		//checkAuth($res);
    }
	
	public function action()
	{
		
		if(isset($_POST['form_type']))
		{

			switch($_POST['form_type'])
			{

				case 'delete':
					
					if($this->deleteData())
					{
						$data['success'] = 'Review deleted successfully.';
						$data['error'] = 'false';
						echo json_encode($data);
						exit;
					}else
					{
						$data['success'] = 'false';
						$data['error'] = 'Review not deleted successfully. Please try again.';
						echo json_encode($data);
						exit;
					}
					

				break;
				
			}
		}	
		
	}
	
	public function index()
	{
		$data = array();
		$data['reviews']	 = 	$this->Model_review->getAll(false, 'desc');
		$data['content'] = 'review/manage';
		$data['class'] = 'review'; 
		$this->load->view('template',$data);	
		
	}
	
	public function view($id)
	{
		$data = array();
		$data['review']	 = 	$this->Model_review->get($id);	
		$data['content'] = 'review/view';
		$data['class'] = 'review'; 
		$this->load->view('template',$data);
		
	}	
	
	private function deleteData()
	{
		
		$delete = false;
		
		$deleted_by['id'] = $this->input->post('id'); 
		$delete = $this->Model_review->delete($deleted_by);
		return $delete;
		
	}
	
}
