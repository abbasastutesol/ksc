<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Legal_info extends CI_Controller {





	public function __construct()

    {

        parent::__construct();

       

	    $this->load->model('Model_legal_info');

		$this->load->model('Model_page');

		/* checkAdminSession();
		$this->user_rights_array = user_rights(); */

		//$res = checkLevels(2);

		//checkAuth($res);

    }

	

	public function index()

	{
		/* if($this->user_rights_array['Why Ioud']['show_p'] == 1) 
		{ */
			$data = array();
	
			$data['record'] = $this->Model_legal_info->legal_info();
	
          	$data['content'] = 'aboutus/legal_info';
	
			$data['class'] = 'Legal_info';
	
			$this->load->view('template',$data);	

		/* } else {
			show_404();
		} */	

	}

	

	public function action()

	{

		

		if(isset($_POST['form_type']))

		{



			switch($_POST['form_type'])

			{

				case 'update':



					$this->validation();

					if($this->update())

					{

						$data['success'] = 'Record has been updated successfully.';

						$data['error'] = 'false';

						echo json_encode($data);

						exit;

					}else

					{

						$data['success'] = 'false';

						$data['error'] = 'Record has not been updated successfully.Please try again';

						echo json_encode($data);

						exit;

					}

										



				break;

				

			}

		}	

		

	}

	

	private function update()

	{

		$data = array();

		$update_by = array();

		$post_data = $this->input->post();

		foreach($post_data as $key => $value)

		{
            if($key != 'form_type' && $key != 'id')

			{

				$data[$key] = $value;	

			}

		}

			/* if(isset($_FILES['image']) && $_FILES['image']['name'] != '')

				{

					$path = 'images/';

					$file_name = uploadImage($path);
					$data['banner_image'] = $file_name;
					$fullPath = 'assets/frontend/'.$path;
					
					//compress_image($fullPath);

				}   */

		$data['updated_at'] = date('Y-m-d H:i:s');

		

		

		$data['updated_by'] = $this->session->userdata['user']['id']; 

		$update_by['id'] = $this->input->post('id');	
		
         $tpl_name = $this->input->post('tpl_name');
		
	    $id = save_meta_data($data,$tpl_name); 
		
		$value = unset_meta_date($data);
		
		$update = $this->Model_legal_info->update($value,array('id'=>$this->input->post('id')));

        // save logs here
        $logData = array(
            'type'=>    'update',
            'section'=> 'Legal_info'
        );
        saveLogs($logData);

		return $update;

	}


	

	private function validation()

	{

		    $errors = array();

			$this->form_validation->set_error_delimiters('<p>', '</p>');

           

			$this->form_validation->set_rules('eng_description', 'Eng description', 'required');

			$this->form_validation->set_rules('arb_description', 'Arb description', 'required');

			$this->form_validation->set_rules('eng_title', 'English title', 'required');

			$this->form_validation->set_rules('arb_title', 'Arabic title', 'required');

			

			

			if ($this->form_validation->run() == FALSE)

			{

				$errors['error'] = validation_errors();

				$errors['success'] = 'false';

				echo json_encode($errors);

				exit;

			}else

			{

				return true;

			}

			

	}

	

	

	

}

