<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Clients extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */



	public function __construct()

    {

        parent::__construct();

       

	    $this->load->model('Model_career');

		$this->load->model('Model_career_image');

	    $this->load->model('Model_jobs');

		$this->load->model('Model_job_applied');

		$this->load->model('Model_clients');
		
		$this->load->library('File_upload');

		checkAdminSession();
		$this->user_rights_array = user_rights();

		//$res = checkLevels(2);

		//checkAuth($res);

    }

	

	public function index()

	{
		/* if($this->user_rights_array['Career Content']['show_p'] == 1) 
		{ */
			$data = array();
	
			
	
			//$data['career'] = $this->Model_career->get('1');
	
			//$fetch_by['career_id'] = '1';
	        $data['records'] = $this->Model_clients->allClients();
			
			$data['content'] = 'admin/client/client_view';
	
			$data['class'] = 'clients'; 
	
			$this->load->view('template',$data);	
		/* } else {
			show_404();
		} */
		

	}

	public function load_add_page()

	{         
		     $data['content'] = 'admin/client/client_add';
	
			$data['class'] = 'add clients'; 
			
			
			$this->load->view('template',$data);	
		
	}
	
	
	public function edit_client($id)

	{         
		    $data['records'] = $this->Model_clients->getClient($id);
			
			$data['content'] = 'admin/client/client_edit';
	
			$data['class'] = 'edit clients'; 
			
			$this->load->view('template',$data);	
		
	}

	

	public function action()

	{

		

		if(isset($_POST['form_type']))

		{



			switch($_POST['form_type'])

			{



				case 'save':

					$this->validation();

					$data['insert_id'] = $this->save();

					if($data['insert_id'] > 0)

					{

						$data['success'] = 'Client saved successfully.';

						$data['error'] = 'false';

						$data['reset'] = 1;

						echo json_encode($data);

						exit;

					}

				break;



				case 'delete':

					

					if($this->deleteData())

					{

						$data['success'] = 'Client deleted successfully.';

						$data['error'] = 'false';

						echo json_encode($data);

						exit;

					}else

					{

						$data['success'] = 'false';

						$data['error'] = 'Client not deleted successfully. Please try again.';

						echo json_encode($data);

						exit;

					}

					



				break;



				case 'update':



					$this->validation();

					if($this->update())

					{

						$data['success'] = 'Client updated successfully.';

						$data['error'] = 'false';

						echo json_encode($data);

						exit;

					}else

					{

						$data['success'] = 'false';

						$data['error'] = 'Client has not been updated successfully.Please try again';

						echo json_encode($data);

						exit;

					}

										



				break;

				

				case 'update_career':



					$this->update_career();

										



				break;

				

				case 'deleteApplied':

					

					if($this->deleteDataApplied())

					{

						$data['success'] = 'Application deleted successfully.';

						$data['error'] = 'false';

						echo json_encode($data);

						exit;

					}else

					{

						$data['success'] = 'false';

						$data['error'] = 'Application not deleted successfully. Please try again.';

						echo json_encode($data);

						exit;

					}

					



				break;

				

			}

		}	

		

	}

	



	

	public function edit($id)

	{
		if($this->user_rights_array['Jobs']['add_p'] == 1) 
		{
			$data = array();
	
			$data['job']	 = 	$this->Model_jobs->get($id);
	
			$data['content'] 	 =  'career/edit';
	
			$data['class'] = 'career_jobs';
	
			$this->load->view('template',$data);
		}else {
			show_404();
		}

	}

	

	private function save()

	{

		$data = array();

		$post_data = $this->input->post();

		foreach($post_data as $key => $value)

		{
			if($key != 'form_type')

			{

				$data[$key] = $value;	

			}

		}

		
		if(isset($_FILES['image']) && $_FILES['image']['name'] != '')

				{

					$path = 'images/';

					$file_name = uploadImage($path);
					$data['banner_image'] = $file_name;
					$fullPath = 'assets/frontend/'.$path;
					
					//compress_image($fullPath);

				}  
		

		$data['created_at'] = date('Y-m-d H:i:s');

		

		

		$data['created_by'] = $this->session->userdata['user']['id'];
		
		$tpl_name = $this->input->post('tpl_name');
		
	    $id = save_meta_data($data,$tpl_name); 
		$value = unset_meta_date($data);
		
	     $value['page_id'] = $id;  
		 
		$insert_id = $this->Model_clients->save($value);

        // save logs here
        $logData = array(
            'type'=>    'add',
            'section'=> 'clients'
        );
		
        saveLogs($logData);

		return $insert_id;

	}

	

	private function update()

	{

		$data = array();

		$update_by = array();

		$post_data = $this->input->post();

		foreach($post_data as $key => $value)

		{
            if($key != 'form_type' && $key != 'id')

			{

				$data[$key] = $value;	

			}

		}

			if(isset($_FILES['image']) && $_FILES['image']['name'] != '')

				{

					$path = 'images/';

					$file_name = uploadImage($path);
					$data['banner_image'] = $file_name;
					$fullPath = 'assets/frontend/'.$path;
					
					//compress_image($fullPath);

				}  

		$data['updated_at'] = date('Y-m-d H:i:s');

		

		

		$data['updated_by'] = $this->session->userdata['user']['id']; 

		$update_by['id'] = $this->input->post('id');	
		
         $tpl_name = $this->input->post('tpl_name');
		
	    $id = save_meta_data($data,$tpl_name); 
		
		$value = unset_meta_date($data);
		
		$update = $this->Model_clients->update($value,array('id'=>$this->input->post('id')));

        // save logs here
        $logData = array(
            'type'=>    'update',
            'section'=> 'clients'
        );
        saveLogs($logData);

		return $update;

	}

	

	

	

	private function deleteData()

	{
		$delete = false;
		$deleted_by['id'] = $this->input->post('id'); 

		$delete = $this->Model_clients->delete($deleted_by);

        // save logs here
        $logData = array(
            'type'=>    'delete',
            'section'=> 'Career'
        );
        saveLogs($logData);

		return $delete;

		

	}

	


    
	private function validation()

	{

		    $errors = array();
			$error_message = "";
			$validation = true;
			
			if(spacesValidation($this->input->post('eng_page_title')) == "") {
				
				$error_message .= "<p>The Eng Title field is required</p>";
				$validation = false;
			}
			if(spacesValidation($this->input->post('arb_page_title')) == "") {
				
				$error_message .= "<p>The Arb Title field is required</p>";
				$validation = false;
			}
			

			if ($validation == false)

			{

				$errors['error'] = $error_message;

				$errors['success'] = 'false';

				echo json_encode($errors);

				exit;

			}else

			{
				return true;

			}

			

	}


}

