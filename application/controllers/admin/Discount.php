<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Discount extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
        parent::__construct();
       
	    $this->load->model('Model_discount');
		checkAdminSession();
		//$res = checkLevels(2);
		//checkAuth($res);
    }
	
	public function index()
	{
		
		
		$data = array();
		
		$data['discounts'] = $this->Model_discount->getAll();
		$data['content'] = 'discount/manage';
		$data['class'] = 'discount'; 
		$this->load->view('template',$data);	
		
	}
	
	public function action()
	{
		
		if(isset($_POST['form_type']))
		{

			switch($_POST['form_type'])
			{

				case 'save':
					$this->validation();
					$data['insert_id'] = $this->save();
					if($data['insert_id'] > 0)
					{
						$data['success'] = 'Discount has been created successfully.';
						$data['error'] = 'false';
						$data['reset'] = 1;
						echo json_encode($data);
						exit;
					}
				break;

				case 'delete':
					
					if($this->deleteData())
					{
						$data['success'] = 'Discount deleted successfully.';
						$data['error'] = 'false';
						echo json_encode($data);
						exit;
					}else
					{
						$data['success'] = 'false';
						$data['error'] = 'Discount not deleted successfully. Please try again.';
						echo json_encode($data);
						exit;
					}
					

				break;

				case 'update':
					$this->validation();
					if($this->update())
					{
						$data['success'] = 'Discount has been updated successfully.';
						$data['error'] = 'false';
						echo json_encode($data);
						exit;
					}else
					{
						$data['success'] = 'false';
						$data['error'] = 'Discount has not been updated successfully.Please try again';
						echo json_encode($data);
						exit;
					}
										

				break;
				
			}
		}	
		
	}
	
	
	public function add()
	{
		$data = array();
	//	$data['categories'] = $this->Model_category->getAll();
		$data['content'] = 'discount/add';
		$data['class'] = 'discount';
		$this->load->view('template',$data);
	}
	
	public function edit($id)
	{
		$data = array();
		//$data['categories']  =  $this->Model_category->getAll();
		$data['discount']	 = 	$this->Model_discount->get($id);
		$data['content'] 	 =  'discount/edit';
		$data['class'] = 'discount';
		$this->load->view('template',$data);
	}
	
	private function save()
	{
		$data = array();
		$post_data = $this->input->post();
		
		
		foreach($post_data as $key => $value)
		{
			
			if($key != 'form_type')
			{
				$data[$key] = $value;	
			}
		}
		
		if($this->Model_discount->checkDup($data['coupon_code']))
		{
			$data['success'] = 'Coupon code already exist.';
			$data['error'] = 'true';
			$data['reset'] = 0;
			echo json_encode($data);
			exit;
		}
		
		if(!isset($data['active']))
		{
			$data['active'] = '0';
		}
		else
		{
			$data['active'] = '1';			
		}
		
		$data['created_at'] = date('Y-m-d H:i:s');
		
		
		$data['created_by'] = $this->session->userdata['user']['id']; 
		
				
		$insert_id = $this->Model_discount->save($data);
		return $insert_id;
	}
	
	private function update()
	{
		$data = array();
		$update_by = array();
		$post_data = $this->input->post();
		
		
		foreach($post_data as $key => $value)
		{
			
			if($key != 'form_type' && $key != 'id' && $key != 'old_coupon_code')
			{
				$data[$key] = $value;	
			}
		}
		
		if($this->Model_discount->checkDup($data['coupon_code']) && $post_data['old_coupon_code'] != $data['coupon_code'])
		{
			$data['success'] = 'Coupon code already exist.';
			$data['error'] = 'true';
			$data['reset'] = 0;
			echo json_encode($data);
			exit;
		}
		
		if(!isset($data['active']))
		{
			$data['active'] = '0';
		}
		else
		{
			$data['active'] = '1';			
		}
		
		
		$data['updated_at'] = date('Y-m-d H:i:s');
		
		
		$data['updated_by'] = $this->session->userdata['user']['id']; 
		
		$update_by['id'] = $this->input->post('id');		
		$update = $this->Model_discount->update($data,$update_by);
		return $update;
	}
	
	private function deleteData()
	{
		
		$delete = false;		
		$deleted_by['id'] = $this->input->post('id'); 
		$delete = $this->Model_discount->delete($deleted_by);
		return $delete;
		
	}	
	
	private function validation()
	{
		    $errors = array();
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
           
			$this->form_validation->set_rules('coupon_code', 'Coupon Code', 'required');
			$this->form_validation->set_rules('expiry_date', 'Expiry Date', 'required');
			
			
			//$this->form_validation->set_rules('eng_description', 'English Description', 'required');
			//$this->form_validation->set_rules('arb_description', 'Arabic Description', 'required');
			
			if ($this->form_validation->run() == FALSE)
			{
				$errors['error'] = validation_errors();
				$errors['success'] = 'false';
				echo json_encode($errors);
				exit;
			}else
			{
				return true;
			}
			
	}
	
}
