<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faqs extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Model_faqs');
		checkAdminSession();
		$this->user_rights_array = user_rights();


    }

    public function index()
    {
		if($this->user_rights_array['Faqs']['show_p'] == 1) 
		{
			$data = array();
			$data['faqs']	 = 	$this->Model_faqs->getAll();
			$data['content'] = 'faqs/view';
			$data['class'] = 'faqs';
			$this->load->view('template',$data);
		} else {
			show_404();
		}
    }

    public function add()
    {
		if($this->user_rights_array['Faqs']['add_p'] == 1) 
		{
			$data = array();
			$data['content'] = 'faqs/add';
			$data['class'] = 'faqs';
			$this->load->view('template',$data);
		} else {
			show_404();
		}

    }


    public function action()
    {

        if(isset($_POST['form_type']))
        {

            switch($_POST['form_type'])
            {

                case 'save':
				$this->validation();
                    if($this->save()) {

                        $data['success'] = 'Faqs has been saved successfully.';
                        $data['error'] = 'false';
                        echo json_encode($data);
                        exit;
                    }else{
                        $data['success'] = 'false';
                        $data['error'] = 'Faqs has not been saved successfully.Please try again';
                        echo json_encode($data);
                        exit;

                    }

                    break;

                case 'update':

                     $this->validation();
                    if($this->update())
                    {
                        $data['success'] = 'Faqs has been updated successfully.';
                        $data['error'] = 'false';
                        echo json_encode($data);
                        exit;
                    }else
                    {
                        $data['success'] = 'false';
                        $data['error'] = 'Faqs has not been updated successfully.Please try again';
                        echo json_encode($data);
                        exit;
                    }


                    break;

                case 'delete':
                    if($this->delete()) {

                        $data['success'] = 'Faqs has been deleted successfully.';
                        $data['error'] = 'false';
                        echo json_encode($data);
                        exit;
                    }else{
                        $data['success'] = 'false';
                        $data['error'] = 'Faqs has not been deleted successfully.Please try again';
                        echo json_encode($data);
                        exit;

                    }

                    break;

            }
        }

    }

    private function save(){

        $post_data = $this->input->post();
        unset($post_data['form_type']);

        $returned_id = $this->Model_faqs->save($post_data);

        if($returned_id){

            // save logs here
            $logData = array(
                'type'=>    'add',
                'section'=> 'FAQS'
            );
            saveLogs($logData);

            return true;
        }else{
            return false;
        }
    }

    private function update()
    {
        $data = array();
        $post_data = $this->input->post();
        $keys[] =  'form_type';
        $keys[] =  'id';

        foreach($post_data as $key => $value)
        {

            if(!in_array($key,$keys))
            {
                $data[$key] = $value;
            }
        }

        $update_by['id'] = $this->input->post('id');

        $update = $this->Model_faqs->update($data,$update_by);

        // save logs here
        $logData = array(
            'type'=>    'update',
            'section'=> 'FAQS'
        );
        saveLogs($logData);

        return $update;
    }


    public function edit($id)
    {
		if($this->user_rights_array['Faqs']['edit_p'] == 1) 
		{
			$data = array();
			$data['faqs']	 = 	$this->Model_faqs->get($id);
			//echo "<pre>"; print_r($data['faqs']); exit;
			$data['content'] = 'faqs/edit';
			$data['class'] = 'faqs';
			$this->load->view('template',$data);
		} else {
			show_404();
		}
    }

    private function delete(){

        $deleted_by['id'] = $this->input->post('id');
        $delete = $this->Model_faqs->delete($deleted_by);
        if($delete){

            // save logs here
            $logData = array(
                'type'=>    'delete',
                'section'=> 'FAQS'
            );
            saveLogs($logData);

            return true;
        }else{
            return false;
        }

    }


    private function validation()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<p>', '</p>');
		
		$error_message = "";
		$success = true;
		
		if(spacesValidation($this->input->post('eng_question')) == "") {
			
			$error_message .= "<p>The Eng Question field is required</p>";
			$success = false;
		}
		if(spacesValidation($this->input->post('arb_question')) == "") {
			$error_message .= "<p>The Arb Question field is required</p>";
			$success = false;
		}
		if(spacesValidation($this->input->post('eng_ans')) == "") {
			$error_message .= "<p>The Eng answer field is required</p>";
			$success = false;
		}
		if(spacesValidation($this->input->post('arb_ans')) == "") {
			$error_message .= "<p>The Arb answer field is required</p>";
			$success = false;
		}

        if ($success == false) {
            $errors['error'] = $error_message;
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        }else
        {
            return true;
        }

    }
    
}
