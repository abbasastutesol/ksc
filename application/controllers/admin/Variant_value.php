<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Variant_value extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
        parent::__construct();
       
	    $this->load->model('Model_variant');
	    $this->load->model('Model_variant_value');
	    $this->load->model('Model_product_variant_value');
	    $this->load->model('Model_product_variant_group');
		checkAdminSession();
		//$res = checkLevels(2);
		//checkAuth($res);
    }
	
	
	
	public function action()
	{
		
		if(isset($_POST['form_type']))
		{

			switch($_POST['form_type'])
			{

				

				case 'delete':
					
					if($this->deleteData())
					{
						$data['success'] = 'Variant value deleted successfully.';
						$data['error'] = 'false';
						echo json_encode($data);
						exit;
					}else
					{
						$data['success'] = 'false';
						$data['error'] = 'Variant value not deleted successfully. Please try again.';
						echo json_encode($data);
						exit;
					}
					

				break;

				
				
			}
		}	
		
	}
	
	
	private function deleteData()
	{
		
		$fetch_by['variant_value_id'] = $this->input->post('id');
		$product_variant_value = $this->Model_product_variant_value->getMultipleRows($fetch_by);
		if($product_variant_value)
		{
			$data['success'] = 'false';
			$data['error'] = 'You can\'t delete this variant value. It is attach with some product.';
			echo json_encode($data);
			exit;
			
		}else
		{
			
			$deleted_variant_value_by['id'] = $this->input->post('id');
			$delete = $this->Model_variant_value->delete($deleted_variant_value_by);
			return $delete;
		}
		
		
	}
	
	public function deleteVariantGroup()
	{
		
		
		$deleted_values_by['product_variant_group_id'] = $this->input->post('id');
		$delete = $this->Model_product_variant_value->delete($deleted_values_by);
		if($delete)
		{
			$delete_group_by['id'] = $this->input->post('id');
			$this->Model_product_variant_group->delete($delete_group_by);
		}
		
		if($delete)
		{
			$data['success'] = 'Deleted Successfully';
			$data['error'] = 'false';
			echo json_encode($data);
			exit;
			
		}
		
		
	}
	
	
	
	
}
