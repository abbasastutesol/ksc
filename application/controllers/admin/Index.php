<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
        parent::__construct();
       
	 	$this->load->model('Model_users');
		//$res = checkLevels(2);
		//checkAuth($res);
    }
	
	public function action()
	{
		
		if(isset($_POST['form_type']))
		{

			switch($_POST['form_type'])
			{

				case 'update':
					$this->validation();
					$data['insert_id'] = $this->update();
					if($data['insert_id'] > 0)
					{
						$data['success'] = 'Profile updated successfully.';
						$data['error'] = 'false';
						$data['reset'] = 1;
						echo json_encode($data);
						exit;
					}
				break;
				
			}
		}	
		
	}
	
	public function index()
	{
		if(checkAdminSession())
        {
			 redirect($this->config->item('base_url') . 'admin/dashboard');
		}			
		
	}
	
	
	public function adminProfile()
	{
		
		checkAdminSession();
		
		$user_id = $this->session->userdata['user']['id'];
		$data['user'] = $this->Model_users->get($user_id);
		$data['content'] = 'user/admin_profile';
		$this->load->view('template',$data);
					
		
	}
	
	
	private function update()
	{
		$data = array();
		$update_by = array();
		$post_data = $this->input->post();
		
		
		foreach($post_data as $key => $value)
		{
			
			if($key != 'form_type' && $key != 'id' && $value != '')
			{
				$data[$key] = $value;	
			}
		}
		
		if(isset($_FILES['image']) && $_FILES['image']['name'] != '')
		{
			
			$path = 'user/';
			$file_name = uploadImage($path);
			$data['image_name'] = $file_name;	
		}  
		
		
		
		$data['updated_at'] = date('Y-m-d H:i:s');
		
		
		$data['updated_by'] = $this->session->userdata['user']['id']; 
		
		$update_by['id'] = $this->input->post('id');		
		$update = $this->Model_users->update($data,$update_by);
		return $update;
	}
	
	
	private function validation()
	{
		    $errors = array();
			$this->form_validation->set_error_delimiters('<p>', '</p>');
           
			$this->form_validation->set_rules('name', 'Name', 'required');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
			//$this->form_validation->set_rules('eng_content', 'English Content', 'required');
			//$this->form_validation->set_rules('arb_content', 'Arabic Content', 'required');
			
			
			if ($this->form_validation->run() == FALSE)
			{
				$errors['error'] = validation_errors();
				$errors['success'] = 'false';
				echo json_encode($errors);
				exit;
			}elseif($_POST['password'] != $_POST['confirm_password']){
				$errors['error'] = 'Password and confirm password must be same.';
				$errors['success'] = 'false';
				echo json_encode($errors);
				exit;
			}else
			{
				return true;
			}
			
	}
	
	
	
}
