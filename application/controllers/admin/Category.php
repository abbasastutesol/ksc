<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Category extends CI_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
    {
        parent::__construct();
       
	    $this->load->model('Model_category');
	    $this->load->model('Model_product');
		checkAdminSession();
		//$res = checkLevels(2);
		//checkAuth($res);
    }
	
	public function index()
	{
		
		
		$data = array();
		
		$data['categories'] = $this->Model_category->getAll();
		$data['content'] = 'admin/category/manage';
		$data['class'] = 'category'; 
		$this->load->view('template',$data);	
		
	}
	
	public function action()
	{
		
		if(isset($_POST['form_type']))
		{
			switch($_POST['form_type'])
			{
				case 'save':
					$this->validation();
					$data['insert_id'] = $this->save();
					if($data['insert_id'] > 0)
					{
						$data['success'] = 'Category has been created successfully.';
						$data['error'] = 'false';
						$data['reset'] = 1;
						echo json_encode($data);
						exit;
					}
				break;
				case 'delete':
					
					if($this->deleteData())
					{
						$data['success'] = 'Category deleted successfully.';
						$data['error'] = 'false';
						echo json_encode($data);
						exit;
					}else
					{
						$data['success'] = 'false';
						$data['error'] = 'Category not deleted successfully. Please try again.';
						echo json_encode($data);
						exit;
					}
					
				break;
				case 'update':
					//$this->validation();
					if($this->update())
					{
						$data['success'] = 'Category has been updated successfully.';
						$data['error'] = 'false';
						echo json_encode($data);
						exit;
					}else
					{
						$data['success'] = 'false';
						$data['error'] = 'Category has not been updated successfully.Please try again';
						echo json_encode($data);
						exit;
					}
										
				break;
				
			}
		}	
		
	}
	
	
	public function add()
	{
		$data = array();
	//	$data['categories'] = $this->Model_category->getAll();
		$data['content'] = 'admin/category/add';
		$data['class'] = 'category';
		$this->load->view('template',$data);
	}
	
	public function edit($id)
	{
		$data = array();
		//$data['categories']  =  $this->Model_category->getAll();
		$data['category']	 = 	$this->Model_category->get($id);
		$data['content'] 	 =  'admin/category/edit';
		$data['class'] = 'category';
		$this->load->view('template',$data);
	}
	
	private function save()
	{
		$data = array();
		$post_data = $this->input->post();
		
		foreach($post_data as $key => $value)
		{
			
			if($key != 'form_type')
			{
				$data[$key] = $value;	
			}
		}
		if(!isset($data['active']))
		{
			$data['active'] = 'Off';
		}
		
		$file_name='';
		if($_FILES['image'])
		{
			$path = 'images/';
			$file_name = uploadImage($path);	
		}  
		
		$data['eng_image'] = $file_name;
		$data['arb_image'] = $file_name;
		if($this->input->post('parent_id') != '0')
		{
			$category = $this->Model_category->get($this->input->post('parent_id'));
			$data['level'] = $category->level + 1;
		}else
		{
			$data['level'] = 1;
		}
		
		
		$data['created_at'] = date('Y-m-d H:i:s');
		
		
		$data['created_by'] = $this->session->userdata['user']['id'];

		// increment in itme_order column for new category
        $ietm_order = $this->Model_category->getMaxItemOrderVal($data);
        $item_or = $ietm_order->itme_order+1;
        $data['itme_order'] = $item_or;

		$insert_id = $this->Model_category->save($data);

		// save log here
        $logData = array(
            'type'=>    'add',
            'section'=> 'Category'
        );
        saveLogs($logData);

		return $insert_id;
	}
	
	private function update()
	{
		$data = array();
		$update_by = array();
		$post_data = $this->input->post();
		
		
		foreach($post_data as $key => $value)
		{
			
			if($key != 'form_type' && $key != 'id')
			{
				$data[$key] = $value;	
			}
		}
		
		if(!isset($data['active']))
		{
			$data['active'] = 'Off';
			$updateProd['category_status'] = '0';
		}else{
			$updateProd['category_status'] = '1';
		}
		if(isset($_FILES['image']) && $_FILES['image']['name'] != '')
		{
			$path = 'images/';
			$file_name = uploadImage($path);
			$data['eng_image'] = $file_name;
			$data['arb_image'] = $file_name;	
		}  
		
		
		if($this->input->post('parent_id') != '0')
		{
			$category = $this->Model_category->get($this->input->post('parent_id'));
			$data['level'] = $category->level + 1;
		}else
		{
			$data['level'] = 1;
		}
		$data['updated_at'] = date('Y-m-d H:i:s');
		$data['updated_by'] = $this->session->userdata['user']['id']; 
		$update_by['id'] = $updatedBy['category_id']= $this->input->post('id');		
		$update = $this->Model_category->update($data,$update_by);
		
		
		// also update product status
		$up = $this->Model_product->update($updateProd,$updatedBy);

        // update log here
        $logData = array(
            'type'=>    'update',
            'section'=> 'Category'
        );
        saveLogs($logData);

		return $update;
	}
	
	private function deleteData()
	{
		$delete = false;
		$fetch_by['parent_id'] = $this->input->post('id');
		$child_categories = $this->Model_category->getMultipleRows($fetch_by);
		if($child_categories)
		{
			$data['success'] = 'false';
			$data['error'] = 'Please first delete its child categories and try again';
			echo json_encode($data);
			exit;
			
		}else
		{
			
			$fetch_products_by['category_id'] = $this->input->post('id');
			$products = $this->Model_product->getMultipleRows($fetch_products_by);
			if($products)
			{
				$data['success'] = 'false';
				$data['error'] = 'Please first delete its products and try again';
				echo json_encode($data);
				exit;
				
			}
		}
		
		$deleted_by['id'] = $this->input->post('id'); 
		$delete = $this->Model_category->delete($deleted_by);

        // update log here
        $logData = array(
            'type'=>    'delete',
            'section'=> 'Category'
        );
        saveLogs($logData);

		return $delete;
		
	}
	
	private function validation()
	{
		    $errors = array();
			$error_message = "";
			$validation = true;
			if(spacesValidation($this->input->post('eng_name')) == "") {
				
				$error_message .= "<p>The Eng name field is required</p>";
				$validation = false;
			}
			if(spacesValidation($this->input->post('arb_name')) == "") {
				
				$error_message .= "<p>The Arb name field is required</p>";
				$validation = false;
			}
			
			
			/*if (empty($_FILES['image']['name']))
			{
				$error_message .= "<p>The image field is required</p>";
				$validation = false;
			}*/
			if ($validation == false)
			{
				$errors['error'] = $error_message;
				$errors['success'] = 'false';
				echo json_encode($errors);
				exit;
			}else
			{
				return true;
			}
			
	}
	
	
	
}
