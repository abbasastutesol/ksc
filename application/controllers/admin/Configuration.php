<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuration extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
        parent::__construct();
       
	    $this->load->model('Model_configuration');
		checkAdminSession();
		//$res = checkLevels(2);
		//checkAuth($res);
    }
	
	public function index()
	{
		$data = array();
		$data['configuration']	 = 	$this->Model_configuration->get('1');
		$data['content'] = 'configuration/edit';
		$data['class'] = 'configuration'; 
		$this->load->view('template',$data);	
		
	}
	
	public function action()
	{
		
		if(isset($_POST['form_type']))
		{

			switch($_POST['form_type'])
			{

				case 'update':

					if($this->update())
					{
						$data['success'] = 'Configuration has been updated successfully.';
						$data['error'] = 'false';
						echo json_encode($data);
						exit;
					}else
					{
						$data['success'] = 'false';
						$data['error'] = 'Configuration has not been updated successfully.Please try again';
						echo json_encode($data);
						exit;
					}
										

				break;
				
			}
		}	
		
	}
	
	private function update()
	{
		$data = array();
		$variant_value_ids = array();
		$post_data = $this->input->post();
		
		foreach($post_data as $key => $value)
		{
			
			if($key == 'form_type' || $key == 'id')
			{
				continue;	
			}
			$data[$key] = $value;
		}
		
		if(isset($_FILES['eng_size_chart']) && $_FILES['eng_size_chart']['name'] != '')
		{			
			$path = 'home/';
			$file_name = uploadImage($path, 'eng_size_chart');
			$data['eng_size_chart'] = $file_name;
		}  
		if(isset($_FILES['arb_size_chart']) && $_FILES['arb_size_chart']['name'] != '')
		{			
			$path = 'home/';
			$file_name = uploadImage($path, 'arb_size_chart');
			$data['arb_size_chart'] = $file_name;
		}  
		
		$data['created_by'] = $this->session->userdata['user']['id']; 
		$data['updated_at'] = date('Y-m-d H:i:s');		
		$data['updated_by'] = $this->session->userdata['user']['id']; 
				
		$update_by['id'] = $this->input->post('id');		
		
		if($update_by['id'] == '')
		{
			$update = $this->Model_configuration->save($data);	
		}
		else
		{
			$update = $this->Model_configuration->update($data,$update_by);
		}
		return $update;
	}
	
	
}
