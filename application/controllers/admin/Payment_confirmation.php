<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Payment_confirmation extends CI_Controller {





	public function __construct()

    {

        parent::__construct();

       

	    $this->load->model('Model_payment_confirm');

		$this->load->model('Model_page');

		checkAdminSession();
		$this->user_rights_array = user_rights();

		//$res = checkLevels(2);

		//checkAuth($res);

    }

	

	public function index()

	{
		/*if($this->user_rights_array['Be A Distributor']['show_p'] == 1)
		{*/
			$data = array();
	
			$data['payments']	 = 	$this->Model_payment_confirm->getAll();
			//echo "<pre>"; print_r($data['payments']); exit;
			$data['content'] = 'customer_service/payment_confirmation';
	
			$data['class'] = 'payment_confirm';
	
			$this->load->view('template',$data);

		/*} else {
			show_404();
		}	*/

		

	}

	

	public function action()

	{

		

		if(isset($_POST['form_type']))

		{



			switch($_POST['form_type'])

			{

				case 'update':



					$this->validation();

					if($this->update())

					{

						$data['success'] = 'Distributor has been updated successfully.';

						$data['error'] = 'false';

						echo json_encode($data);

						exit;

					}else

					{

						$data['success'] = 'false';

						$data['error'] = 'Distributor has not been updated successfully.Please try again';

						echo json_encode($data);

						exit;

					}

										



				break;

				

			}

		}	

		

	}

	

	private function update()

	{

		$data = array();

		$eng_values = array();

		$arb_values = array();

		$variant_value_ids = array();

		$post_data = $this->input->post();

		

		$keys[] =  'form_type';

		$keys[] =  'id';

		$keys[] =  'eng_meta_title';

		$keys[] =  'arb_meta_title';

		$keys[] =  'eng_meta_description';

		$keys[] =  'arb_meta_description';

		$keys[] =  'eng_meta_keyword';

		$keys[] =  'arb_meta_keyword';

		$keys[] =  'tpl_name';

		

		$pageCheck = $this->Model_page->getRowCount(array('id'=>$post_data['home_page_id']));

		

		$pageData['eng_meta_title'] = $post_data['eng_meta_title'];

		$pageData['eng_meta_description'] = $post_data['eng_meta_description'];

		$pageData['eng_meta_keyword'] = $post_data['eng_meta_keyword'];

		$pageData['arb_meta_title'] = $post_data['arb_meta_title'];

		$pageData['arb_meta_description'] = $post_data['arb_meta_description'];

		$pageData['arb_meta_keyword'] = $post_data['arb_meta_keyword'];

		$pageData['tpl_name'] = $post_data['tpl_name'];

		

		if($pageCheck > 0)

		{

			$page_update_by['id'] = $post_data['home_page_id'];

			$update = $this->Model_page->update($pageData,$page_update_by);

		}

		else

		{

			$post_data['home_page_id'] = $this->Model_page->save($pageData);

		}

		

		foreach($post_data as $key => $value)

		{

			

			if(!in_array($key,$keys))

			{

				$data[$key] = $value;	

			}

		}

		

		$data['created_at'] = date('Y-m-d H:i:s');

		$data['created_by'] = $this->session->userdata['user']['id']; 

		$data['updated_at'] = date('Y-m-d H:i:s');

		$data['updated_by'] = $this->session->userdata['user']['id']; 

		

				

		$update_by['id'] = $this->input->post('id');

		unset($data['home_page_id']);



		$update = $this->Model_payment_confirm->update($data,$update_by);

        // save logs here
        $logData = array(
            'type'=>    'update',
            'section'=> 'Payment Confirmation'
        );
        saveLogs($logData);

		

		return $update;

	}

	

	private function validation()

	{

		    $errors = array();

			$this->form_validation->set_error_delimiters('<p>', '</p>');

           

			$this->form_validation->set_rules('eng_description', 'Eng description', 'required');

			$this->form_validation->set_rules('arb_description', 'Arb description', 'required');

			

			if ($this->form_validation->run() == FALSE)

			{

				$errors['error'] = validation_errors();

				$errors['success'] = 'false';

				echo json_encode($errors);

				exit;

			}else

			{

				return true;

			}
        
	}

    public function contact_delete(){

        $delete = false;

        $deleted_by['id'] = $this->input->post('id');

        $delete = $this->Model_payment_confirm->delete($deleted_by);

        if($delete){

            // save logs here
            $logData = array(
                'type'=>    'delete',
                'section'=> 'Payment Confirmation'
            );
            saveLogs($logData);


            $data['success'] = 'Payment confirmation deleted successfully.';

            $data['error'] = 'false';

            echo json_encode($data);

            exit;

        }else{

            $data['success'] = 'false';

            $data['error'] = 'Payment confirmation not deleted successfully. Please try again.';

            echo json_encode($data);

            exit;
        }
    }


	

}

