<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Variant extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
        parent::__construct();
       
	    $this->load->model('Model_variant');
	    $this->load->model('Model_variant_value');
	    $this->load->model('Model_product_variant_value');
		checkAdminSession();
		//$res = checkLevels(2);
		//checkAuth($res);
    }
	
	public function index()
	{
		
		
		$data = array();
		
		$data['variants'] = $this->Model_variant->getAll();
		$data['content'] = 'variant/manage';
		$data['class'] = 'variant'; 
		$this->load->view('template',$data);	
		
	}
	
	public function action()
	{
		
		if(isset($_POST['form_type']))
		{

			switch($_POST['form_type'])
			{

				case 'save':
					$this->validation();
					$data['insert_id'] = $this->save();
					if($data['insert_id'] > 0)
					{
						$data['success'] = 'Variant has been created successfully.';
						$data['error'] = 'false';
						$data['reset'] = 1;
						echo json_encode($data);
						exit;
					}
				break;

				case 'delete':
					
					if($this->deleteData())
					{
						$data['success'] = 'Variant deleted successfully.';
						$data['error'] = 'false';
						echo json_encode($data);
						exit;
					}else
					{
						$data['success'] = 'false';
						$data['error'] = 'Variant not deleted successfully. Please try again.';
						echo json_encode($data);
						exit;
					}
					

				break;

				case 'update':

					$this->validation();
					if($this->update())
					{
						$data['success'] = 'Variant has been updated successfully.';
						$data['error'] = 'false';
						echo json_encode($data);
						exit;
					}else
					{
						$data['success'] = 'false';
						$data['error'] = 'Variant has not been updated successfully.Please try again';
						echo json_encode($data);
						exit;
					}
										

				break;
				
			}
		}	
		
	}
	
	
	public function add()
	{
		$data = array();
		
		$data['content'] = 'variant/add';
		$data['class'] = 'variant';
		$this->load->view('template',$data);
	}
	
	public function edit($id)
	{
		$data = array();
		$fetch_by = array();
		$fetch_by['variant_id'] = $id; //  we make this array to pass getMultipleRows as it return data by getting array.
		
		$data['variant']	 = 	$this->Model_variant->get($id);
		
		$data['variant_values']	 = 	$this->Model_variant_value->getMultipleRows($fetch_by, false, 'asc', 'eng_value');
		$data['content'] 	 =  'variant/edit';
		$data['class'] = 'variant';
		$this->load->view('template',$data);
	}
	
	private function save()
	{
		$data = array();
		$eng_values = array();
		$arb_values = array();
		$post_data = $this->input->post();
		/*$eng_values = explode(',',$this->input->post('eng_value'));
		$arb_values = explode(',',$this->input->post('arb_value'));
		if(count($eng_values) != count($arb_values))
		{
			$data['success'] = 'false';
			$data['error'] = 'Variant values for english and arabic must have same count ';
			echo json_encode($data);
			exit;
		}
		*/
		$eng_values = $this->input->post('eng_value');
		$arb_values = $this->input->post('arb_value');
		
		
		foreach($post_data as $key => $value)
		{
			
			if($key != 'form_type' && $key != 'eng_value' && $key != 'arb_value')
			{
				$data[$key] = $value;	
			}
		}
		
		
		
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['created_by'] = $this->session->userdata['user']['id']; 
		
				
		$insert_id = $this->Model_variant->save($data);
		
		$i = 0;
		$k = 1;	
		$values = array();
		$total_val = count($eng_values);
		foreach($eng_values as $value)
		{
				//$values['eng_value']  = ($total_val == $k ? $value : $value['s_'.$k.':i_0']);
				$values['eng_value']  = $value;
				if($values['eng_value'] != '')
				{
					$values['variant_id'] = $insert_id;
					//$values['eng_value']  = ($total_val == $k ? $value : $value['s_'.$k.':i_0']);
					//$values['arb_value']  = ($total_val == $k ? $arb_values[$i] : $arb_values[$i]['s_'.$k.':i_1']);
					$values['arb_value']  = $arb_values[$i];
					$values['created_at'] = date('Y-m-d H:i:s');
					$values['created_by'] = $this->session->userdata['user']['id']; 
					$this->Model_variant_value->save($values);
					
				}
				
				$i++;
				$k++;
			
		}
		
		return $insert_id;
	}
	
	private function update()
	{
		$data = array();
		$eng_values = array();
		$arb_values = array();
		$variant_value_ids = array();
		$post_data = $this->input->post();
		//$eng_values = explode(',',$this->input->post('eng_value'));
		//$arb_values = explode(',',$this->input->post('arb_value'));
		$eng_values = $this->input->post('eng_value');
		$arb_values = $this->input->post('arb_value');
		/*if(count($eng_values) != count($arb_values))
		{
			$data['success'] = 'false';
			$data['error'] = 'Variant values for english and arabic must have same count ';
			echo json_encode($data);
			exit;
		}*/
		
		foreach($post_data as $key => $value)
		{
			
			if($key != 'form_type' && $key != 'eng_value' && $key != 'arb_value' && $key != 'id' && $key != 'variant_value_ids')
			{
				$data[$key] = $value;	
			}
		}
		
		
		
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['created_by'] = $this->session->userdata['user']['id']; 
		
				
		$update_by['id'] = $this->input->post('id');		
		$update = $this->Model_variant->update($data,$update_by);
		
		$deleted_by['variant_id'] = $this->input->post('id');
		//$this->Model_variant_value->delete($deleted_by);
		$variant_value_ids = $this->input->post('variant_value_ids');
		$i = 0;
		$k = 1;	
		$values = array();
		$total_val = count($eng_values);
		
		foreach($eng_values as $val)
		{
				//$values['eng_value']  = ($total_val == $k ? $val : $val['s_'.$k.':i_0']);
				$values['eng_value']  = $val;
				if($values['eng_value'] != '')
				{
					$values['variant_id'] = $update_by['id'];
					//$values['eng_value']  = ($total_val == $k ? $val : $val['s_'.$k.':i_0']);
					//$values['arb_value']  = ($total_val == $k ? $arb_values[$i] : $arb_values[$i]['s_'.$k.':i_1']);
					$values['arb_value']  = $arb_values[$i];
					
					if(isset($variant_value_ids[$i]))
					{
						$update_variant_value_by['id'] = $variant_value_ids[$i]; 
						$values['updated_at'] = date('Y-m-d H:i:s');
					    $values['updated_by'] = $this->session->userdata['user']['id'];
						$this->Model_variant_value->update($values,$update_variant_value_by);						
					}else
					{
						$values['created_at'] = date('Y-m-d H:i:s');
						$values['created_by'] = $this->session->userdata['user']['id']; 
						$this->Model_variant_value->save($values);
					}
					
					
				}
				
				$i++;
				$k++;
			
		}
		
		
		return $update;
	}
	
	private function deleteData()
	{
		
		$fetch_by['variant_id'] = $this->input->post('id');
		$product_variant_value = $this->Model_product_variant_value->getMultipleRows($fetch_by);
		if($product_variant_value)
		{
			$data['success'] = 'false';
			$data['error'] = 'You can not delete this variant.It is attach with some product.';
			echo json_encode($data);
			exit;
			
		}else
		{
			$deleted_by['id'] = $this->input->post('id');
			$delete = $this->Model_variant->delete($deleted_by);
			$deleted_variant_value_by['variant_id'] = $this->input->post('id');
			$this->Model_variant_value->delete($deleted_variant_value_by);
			return $delete;
		}
		
		
	}
	
	private function deleteVariantValue()
	{
		
		$fetch_by['variant_id'] = $this->input->post('id');
		$product_variant_value = $this->Model_product_variant_value->getMultipleRows($fetch_by);
		if($product_variant_value)
		{
			$data['success'] = 'false';
			$data['error'] = 'You can not delete this variant.It is attach with some product.';
			echo json_encode($data);
			exit;
			
		}else
		{
			$deleted_by['id'] = $this->input->post('id');
			$delete = $this->Model_variant->delete($deleted_by);
			$deleted_variant_value_by['variant_id'] = $this->input->post('id');
			$this->Model_variant_value->delete($deleted_variant_value_by);
			return $delete;
		}
		
		
	}
	
	private function validation()
	{
		    $errors = array();
			$this->form_validation->set_error_delimiters('<p>', '</p>');
           
			$this->form_validation->set_rules('eng_name', 'Eng Title', 'required');
			$this->form_validation->set_rules('arb_name', 'Arb Title', 'required');
			//$this->form_validation->set_rules('eng_content', 'English Content', 'required');
			//$this->form_validation->set_rules('arb_content', 'Arabic Content', 'required');
			
			
			if ($this->form_validation->run() == FALSE)
			{
				$errors['error'] = validation_errors();
				$errors['success'] = 'false';
				echo json_encode($errors);
				exit;
			}else
			{
				return true;
			}
			
	}
	
	
	
}
