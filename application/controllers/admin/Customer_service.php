<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_service extends CI_Controller {



	public function __construct()

    {

        parent::__construct();



		$this->load->model('Model_page');

		$this->load->model('Model_customer_service');
		$this->load->model('Model_distributor_requests');
		$this->load->model('Model_payment_confirm');
		$this->load->model('Model_meduaf_program');


		checkAdminSession();
		$this->user_rights_array = user_rights();



    }



	public function index()

	{

		if($this->user_rights_array['Payment & Delivery']['show_p'] == 1)
		{

			$data = array();

			$data['customer_service']	 = 	$this->Model_customer_service->get('1');

			$data['content'] = 'customer_service/pay_and_delivery_edit';

			$data['class'] = 'customer_service';

			$this->load->view('template',$data);

		} else {
			show_404();
		}

	}



	public function action()

	{



		if(isset($_POST['form_type']))

		{



			switch($_POST['form_type'])

			{

				case 'update':



					$this->validation();

					if($this->update())

					{

						$data['success'] = 'Customer Service has been updated successfully.';

						$data['error'] = 'false';

						echo json_encode($data);

						exit;

					}else

					{

						$data['success'] = 'false';

						$data['error'] = 'Customer Service has not been updated successfully.Please try again';

						echo json_encode($data);

						exit;

					}

				break;


                case 'update_meduaf_program':

                    if($this->update_meduaf_program())

                    {
                        $data['success'] = 'Meduaf Program has been updated successfully.';

                        $data['error'] = 'false';

                        echo json_encode($data);

                        exit;

                    }else

                    {
                        $data['success'] = 'false';

                        $data['error'] = 'Meduaf Program has not been updated successfully.Please try again';

                        echo json_encode($data);

                        exit;

                    }

                    break;

			}

		}



	}



	private function update()

	{

		$data = array();

		$post_data = $this->input->post();



		$keys[] =  'form_type';

		$keys[] =  'id';



		foreach($post_data as $key => $value)

		{



			if(!in_array($key,$keys))

			{

				$data[$key] = $value;

			}

		}



		$data['created_at'] = date('Y-m-d H:i:s');

		$data['created_by'] = $this->session->userdata['user']['id'];

		$data['updated_at'] = date('Y-m-d H:i:s');





		$update_by['id'] = $this->input->post('id');



		$update = $this->Model_customer_service->update($data,$update_by);

		if($update) {
            // save logs here
            $logData = array(
                'type' => 'update',
                'section' => 'Payment & Delivery'
            );
            saveLogs($logData);
        }



		return $update;

	}

	private function update_meduaf_program()

	{
		$data = array();

		$post_data = $this->input->post();

		$keys[] =  'form_type';

		$keys[] =  'id';

		foreach($post_data as $key => $value)

		{
			if(!in_array($key,$keys))

			{
				$data[$key] = $value;

			}

		}

		$data['created_at'] = date('Y-m-d H:i:s');

		$data['created_by'] = $this->session->userdata['user']['id'];

		$update_by['id'] = $this->input->post('id');
		$update = $this->Model_meduaf_program->update($data,$update_by);

		if($update) {
            // save logs here
            $logData = array(
                'type' => 'update',
                'section' => 'Meduaf program'
            );
            saveLogs($logData);
        }

		return $update;

	}

	private function validation()

	{

		    $errors = array();

			$this->form_validation->set_error_delimiters('<p>', '</p>');



			$this->form_validation->set_rules('eng_description', 'Eng description', 'required');

			$this->form_validation->set_rules('arb_description', 'Arb description', 'required');



			if ($this->form_validation->run() == FALSE)

			{

				$errors['error'] = validation_errors();

				$errors['success'] = 'false';

				echo json_encode($errors);

				exit;

			}else

			{

				return true;

			}



	}



	public function distributor(){
		if($this->user_rights_array['Distributors Requests']['show_p'] == 1)
		{

			$data['distributors'] = $this->Model_distributor_requests->getAll();


			$data['content'] = 'customer_service/distributor_requests';

			$data['class'] = 'distributors';

			$this->load->view('template',$data);
		} else {
			show_404();
		}

	}

	public function payment_requests(){
		if($this->user_rights_array['Payment Requests']['show_p'] == 1)
		{
			$data['payment_requests'] = $this->Model_payment_confirm->getAll();


			$data['content'] = 'customer_service/payment_requests';

			$data['class'] = 'payment_requests';

			$this->load->view('template',$data);
		} else {
			show_404();
		}

	}


	public function delete_distributor(){

		$deleted_by['id'] = $this->input->post('id');

		$delete = $this->Model_distributor_requests->delete($deleted_by);
		 if($delete){


             // save logs here
             $logData = array(
                 'type'=>    'delete',
                 'section'=> 'Distributor'
             );
             saveLogs($logData);


			$data['success'] = 'Record deleted successfully.';

			$data['error'] = 'false';


			}else{
				$data['success'] = 'Not deleted successfully, please try again.';

				$data['error'] = 'true';
			}

			echo json_encode($data);

			exit;

	}


    public function delete_payment_request(){

        $deleted_by['id'] = $this->input->post('id');

        $delete = $this->Model_payment_confirm->delete($deleted_by);
        if($delete){

            // save logs here
            $logData = array(
                'type'=>    'delete',
                'section'=> 'Pyament Request'
            );
            saveLogs($logData);


            $data['success'] = 'Record deleted successfully.';

            $data['error'] = 'false';


        }else{
            $data['success'] = 'Not deleted successfully, please try again.';

            $data['error'] = 'true';
        }

        echo json_encode($data);

        exit;

    }

    public function meduaf_program(){
        if($this->user_rights_array['Meduaf program']['show_p'] == 1)
        {

            $data['meduaf_program'] = $this->Model_meduaf_program->get('1');


            $data['content'] = 'customer_service/meduaf_program_edit';

            $data['class'] = 'meduaf_program';

            $this->load->view('template',$data);
        } else {
            show_404();
        }

    }

}

