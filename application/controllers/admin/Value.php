<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Value extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */



	public function __construct()

    {

        parent::__construct();

       

	    $this->load->model('Model_career');

		$this->load->model('Model_career_image');

	    $this->load->model('Model_jobs');

		$this->load->model('Model_award');

		$this->load->model('Model_value');
		
		$this->load->library('File_upload');

		checkAdminSession();
		$this->user_rights_array = user_rights();

		//$res = checkLevels(2);

		//checkAuth($res);

    }

	

	public function index()

	{
		/* if($this->user_rights_array['Career Content']['show_p'] == 1) 
		{ */
			$data = array();

	        $data['record'] = $this->Model_value->getData();
			
			$data['content'] = 'aboutus/Value';
	
			$data['class'] = 'Value'; 
	
			$this->load->view('template',$data);
			
		/* } else {
			show_404();
		} */
		

	}


	

	public function action()

	{

		

		if(isset($_POST['form_type']))

		{



			switch($_POST['form_type'])

			{

				case 'update':



					$this->validation();

					if($this->update())

					{

						$data['success'] = 'Record updated successfully.';

						$data['error'] = 'false';

						echo json_encode($data);

						exit;

					}else

					{

						$data['success'] = 'false';

						$data['error'] = 'Record has not been updated successfully.Please try again';

						echo json_encode($data);

						exit;

					}

										



				break;

				

				

				

			}

		}	

		

	}

	



	

	
	

	

	

	private function update()

	{

		$data = array();

		$update_by = array();

		$post_data = $this->input->post();

		foreach($post_data as $key => $value)

		{
            if($key != 'form_type' && $key != 'id')

			{

				$data[$key] = $value;	

			}

		}

			if(isset($_FILES['image']) && $_FILES['image']['name'] != '')

				{

					$path = 'images/';

					$file_name = uploadImage($path);
					$data['banner_image'] = $file_name;
					$fullPath = 'assets/frontend/'.$path;
					
					compress_image($fullPath);

				}  

		$data['updated_at'] = date('Y-m-d H:i:s');

		$data['updated_by'] = $this->session->userdata['user']['id']; 
          
		$update_by['id'] = $this->input->post('id');		
         $id = save_meta_data($data,'value');
		$value = unset_meta_date($data);
		
		$update = $this->Model_value->update($value,array('id'=>$this->input->post('id')));

        // save logs here
        $logData = array(
            'type'=>    'update',
            'section'=> 'Value'
        );
        saveLogs($logData);

		return $update;

	}

	

	

	

	private function deleteData()

	{
		$delete = false;
		$deleted_by['id'] = $this->input->post('id'); 

		$delete = $this->Model_award->delete($deleted_by);

        // save logs here
        $logData = array(
            'type'=>    'delete',
            'section'=> 'Career'
        );
        saveLogs($logData);

		return $delete;

		

	}

	


    
	private function validation()

	{

		    $errors = array();
			$error_message = "";
			$validation = true;
			
			if(spacesValidation($this->input->post('eng_title')) == "") {
				
				$error_message .= "<p>The Eng Title field is required</p>";
				$validation = false;
			}
			if(spacesValidation($this->input->post('arb_title')) == "") {
				
				$error_message .= "<p>The Arb Title field is required</p>";
				$validation = false;
			}
			if(spacesValidation($this->input->post('eng_description')) == "") {
				
				$error_message .= "<p>The Enf Description field is required</p>";
				$validation = false;
			}
			if(spacesValidation($this->input->post('arb_description')) == "") {
				
				$error_message .= "<p>The Arb Description field is required</p>";
				$validation = false;
			}

			if ($validation == false)

			{

				$errors['error'] = $error_message;

				$errors['success'] = 'false';

				echo json_encode($errors);

				exit;

			}else

			{
				return true;

			}

			

	}


}

