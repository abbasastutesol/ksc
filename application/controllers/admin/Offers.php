<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Offers extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
        parent::__construct();
       
	    $this->load->model('Model_offers');
	    $this->load->model('Model_offer_images');
	    $this->load->model('Model_variant_value');
	    $this->load->model('Model_product_variant_value');
        $this->load->model('Model_image');
        $this->load->library('File_upload');
		checkAdminSession();
		//$res = checkLevels(2);
		//checkAuth($res);
    }
	
	public function index()
	{
		$data = array();

		$data['offers'] = $this->Model_offers->getAll();
		$data['content'] = 'offers/manage';
		$data['class'] = 'offers';
		$this->load->view('template',$data);
		
	}
	
	public function action()
	{
		
		if(isset($_POST['form_type']))
		{
			switch($_POST['form_type'])
			{

				case 'save':
					$this->validation();
					$data['insert_id'] = $this->save();
					if($data['insert_id'] > 0)
					{
						$data['success'] = 'Offer has been created successfully.';
						$data['error'] = 'false';
						$data['reset'] = 1;
						echo json_encode($data);
						exit;
					}
				break;

				case 'delete':
					
					if($this->deleteData())
					{
						$data['success'] = 'Offer deleted successfully.';
						$data['error'] = 'false';
						echo json_encode($data);
						exit;
					}else
					{
						$data['success'] = 'false';
						$data['error'] = 'Offer not deleted successfully. Please try again.';
						echo json_encode($data);
						exit;
					}
					

				break;

				case 'update':

					$this->validation();
					if($this->update())
					{
						$data['success'] = 'Offer has been updated successfully.';
						$data['error'] = 'false';
						echo json_encode($data);
						exit;
					}else
					{
						$data['success'] = 'false';
						$data['error'] = 'Offer has not been updated successfully.Please try again';
						echo json_encode($data);
						exit;
					}
										

				break;
				
			}
		}	
		
	}
	
	
	public function add()
	{
		$data = array();
		
		$data['content'] = 'offers/add';
		$data['class'] = 'offers';
		$this->load->view('template',$data);
	}
	
	public function edit($id)
	{
		$data = array();
		$fetch_by = array();
		$fetch_by['offer_id'] = $id; //  we make this array to pass getMultipleRows as it return data by getting array.
		
		$data['offers']	 = 	$this->Model_offers->get($id);
        $data['images'] = $this->Model_offer_images->getMultipleRows($fetch_by);
		$data['content'] 	 =  'offers/edit';
		$data['class'] = 'offers';

		$this->load->view('template',$data);
	}
	
	private function save()
	{
		$data = array();
		$post_data = $this->input->post();

		foreach($post_data as $key => $value)
		{
			
			if($key != 'form_type' && $key != 'eng_value' && $key != 'arb_value')
			{
				$data[$key] = $value;	
			}
		}
		
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['created_by'] = $this->session->userdata['user']['id'];

		unset($data['is_main_slider']);
		//echo "<pre>"; print_r($data); exit;
		$insert_id = $this->Model_offers->save($data);

        //image uploading

        if($_FILES['image']['name'][0] != '' )
        {
            // create configuration fields for setting
            $config_array['path']       = 'uploads/images/offers/';
            $config_array['thumb_path'] = 'uploads/images/thumbs/offers/';
            $config_array['height']     = '300'; //image thumb height
            $config_array['width']      = '300'; //image thumb width
            $config_array['field']      = 'image';//this is field name for html form
            $config_array['allowed_types']      = 'gif|png|jpg';//this is field name for html form

            $images = $this->file_upload->uploadFiles($config_array);

            if($images)
            {
                $is_main_slider = $this->input->post('is_main_slider');
                unset($is_main_slider[count($is_main_slider)-1]);
                $i=0;
                foreach($images as $image)
                {

                    $product_image['offer_id'] = $insert_id;
                    $product_image['eng_image']  = $image;
                    $product_image['arb_image']  = $image;
                    $product_image['is_main_slider']  = $is_main_slider[$i];
                    $product_image['created_at'] = date('Y-m-d H:i:s');
                    $product_image['created_by'] = $this->session->userdata['user']['id'];


                        $this->Model_offer_images->save($product_image);
                    $i++;
                }
            }
        }

		return $insert_id;
	}
	
	private function update()
	{
		$data = array();

		$post_data = $this->input->post();
		if(!isset($post_data['active']))
            $post_data['active'] = 0;

        if(!isset($post_data['display_to_home']))
            $post_data['display_to_home'] = 0;

		foreach($post_data as $key => $value)
		{
			
			if($key != 'form_type' && $key != 'id')
			{
				$data[$key] = $value;	
			}
		}
		
		
		
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['created_by'] = $this->session->userdata['user']['id']; 
		
				
		$update_by['id'] = $this->input->post('id');
        unset($data['is_main_slider']);
		$update = $this->Model_offers->update($data,$update_by);
        $image_fetch = false;
        //image uploading
        // create configuration fields for setting

        if($_FILES['image']['name'][0] != '' && isset($_FILES['image']))
        {
            $config_array['path']       = 'uploads/images/offers/';
            $config_array['thumb_path'] = 'uploads/images/thumbs/offers/';
            $config_array['height']     = '300'; //image thumb height
            $config_array['width']      = '300'; //image thumb width
            $config_array['field']      = 'image';//this is field name for html form
            $config_array['allowed_types']      = 'gif|png|jpg';//this is field name for html form
            $images = $this->file_upload->uploadFiles($config_array);
            $image_lis = '';
            $images_lis_light_boxes = '';
            if($images)
            {
                $is_main_slider = $this->input->post('is_main_slider');
                unset($is_main_slider[count($is_main_slider)-1]);
                $i=0;
                foreach($images as $image)
                {
                    $product_image['offer_id'] = $this->input->post('id');
                    $product_image['eng_image']  = $image;
                    $product_image['arb_image']  = $image;
                    $product_image['is_main_slider']  = $is_main_slider[$i];
                    $product_image['created_at'] = date('Y-m-d H:i:s');
                    $product_image['created_by'] = $this->session->userdata['user']['id'];
                    $image_insert_id = $this->Model_offer_images->save($product_image);
                    $get_image = $this->Model_offer_images->get($image_insert_id);
                    $image_lis .="<li class='uk-position-relative image-".$get_image->id."'>
                                        <button type='button' class='uk-modal-close uk-close uk-close-alt uk-position-absolute' onClick='deleteRecord(".$get_image->id.",\"admin/offers/deleteImage\",\"\");'></button><img src='".base_url().'uploads/images/thumbs/products/'.$get_image->eng_image."' alt='' class='img_small' data-uk-modal='{target:'#modal_lightbox_".$get_image->id."'}'/></li>";
                    $images_lis_light_boxes .= "<div class='uk-modal' id='modal_lightbox_".$get_image->id."' ><div class='uk-modal-dialog uk-modal-dialog-lightbox'><button type='button' class='uk-modal-close uk-close uk-close-alt'></button><img src='".base_url()."uploads/images/products/".$get_image->eng_image."' alt=''/></div></div>";
                    $i++;
                }
            }
            // end images save section
            $image_fetch = true;
        }

		
		$deleted_by['id'] = $this->input->post('id');
		
		return $update;
	}


    public function deleteImage()
    {
        $deleted_by['id'] = $this->input->post('id');
        $image = $this->Model_offer_images->get($this->input->post('id'));
        $delete = $this->Model_offer_images->delete($deleted_by);
        if($delete)
        {
            unlink('uploads/images/offers/'.$image->eng_image);
            unlink('uploads/images/thumbs/offers/'.$image->eng_image);
            $data['success'] = 'image has been deleted successfully.';
            $data['error'] = 'false';
            $data['is_image_delete'] = 'true';
            echo json_encode($data);
            exit;

        }


    }


	
	private function deleteData()
	{
		$fetch_by['id'] = $this->input->post('id');

			$deleted_by['id'] = $this->input->post('id');
			$delete = $this->Model_offers->delete($deleted_by);
			$deleted_variant_value_by['id'] = $this->input->post('id');
			return $delete;
		
		
	}
	
	private function validation()
	{
		    $errors = array();
			$this->form_validation->set_error_delimiters('<p>', '</p>');
           
			$this->form_validation->set_rules('eng_name', 'Eng Title', 'required');
			$this->form_validation->set_rules('arb_name', 'Arb Title', 'required');
			//$this->form_validation->set_rules('eng_content', 'English Content', 'required');
			//$this->form_validation->set_rules('arb_content', 'Arabic Content', 'required');
			
			
			if ($this->form_validation->run() == FALSE)
			{
				$errors['error'] = validation_errors();
				$errors['success'] = 'false';
				echo json_encode($errors);
				exit;
			}else
			{
				return true;
			}
			
	}
	
	
	
}
