<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User_rights extends CI_Controller
{
	public $user_rights_array;
	
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_user_rights');
        $this->load->model('Model_admin_roles');
        $this->load->model('Model_page_rights');
		$this->load->model('Central_model'); 
        checkAdminSession();
		$this->user_rights_array = user_rights();
    }
    public function index()
    {
		if($this->user_rights_array['User Rights']['show_p'] == 1) 
		{
			$data              = array();
			$data['UserRoles'] = $this->Central_model->select_all_array('admin_roles', array('is_admin' => 0));
			$data['content']   = 'user_rights/manage';
			$data['class']     = 'user_rights';
			$this->load->view('template', $data);
		} else {
			show_404();
		}
    }
    public function view_location()
    {
        $data                   = array();
        $id                     = $this->uri->segment(4);
        $data['shopper_driver'] = $this->Model_shopper_driver->get($id);
        $data['content']        = 'shopper_driver/view_location';
        $data['class']          = 'shopper_driver';
        $this->load->view('template', $data);
    }
    public function action()
    {
        if (isset($_POST['form_type'])) {
            switch ($_POST['form_type']) {
                case 'save':
                    $this->validation();
                    $data['insert_id'] = $this->save();
                    if ($data['insert_id'] > 0) {
                        $data['success'] = 'User has been created successfully.';
                        $data['error']   = 'false';
                        $data['reset']   = 1;
                        echo json_encode($data);
                        exit;
                    }
                    break;
                case 'delete':
                    if ($this->deleteData()) {
                        $data['success'] = 'User deleted successfully.';
                        $data['error']   = 'false';
                        echo json_encode($data);
                        exit;
                    } else {
                        $data['success'] = 'false';
                        $data['error']   = 'User not deleted successfully. Please try again.';
                        echo json_encode($data);
                        exit;
                    }
                    break;
                case 'update':
                    if ($this->update()) {
                        $data['success'] = 'User has been updated successfully.';
                        $data['error']   = 'false';
                        echo json_encode($data);
                        exit;
                    } else {
                        $data['success'] = 'false';
                        $data['error']   = 'User has not been updated successfully.Please try again';
                        echo json_encode($data);
                        exit;
                    }
                    break;
                case 'update_status':
                    if ($this->update_status()) {
                        $data['success'] = 'User has been updated successfully.';
                        $data['error']   = 'false';
                        echo json_encode($data);
                        exit;
                    } else {
                        $data['success'] = 'false';
                        $data['error']   = 'User has not been updated successfully.Please try again';
                        echo json_encode($data);
                        exit;
                    }
                    break;
            }
        }
    }
    public function add()
    {
		if($this->user_rights_array['User Rights']['add_p'] == 1) 
		{
			$data              = array();
			$data['all_pages'] = $this->Model_user_rights->getAll();
			$data['content']   = 'user_rights/add';
			$data['class']     = 'user_rights';
			$this->load->view('template', $data);
		} else {
			show_404();
		}
    }
    public function edit($id)
    {
		if($this->user_rights_array['User Rights']['edit_p'] == 1) 
		{
			$data                     = array();
			$row = $this->Central_model->select_max_field("admin_roles", array('id' => $id), 'is_admin');
			if($row->is_admin == 0) {
				$data['all_pages']        = $this->Model_user_rights->getAll();
				$data['user_role_name']   = $this->Model_admin_roles->get($id);
				$fetch_by['user_role_id'] = $id;
				$data['userRights']       = $this->Model_page_rights->allUserRights($id);
				$data['content']          = 'user_rights/edit';
				$data['class']            = 'user_rights';
				$this->load->view('template', $data);
			}
		} else {
			show_404();
		}
    }

    private function save()
    {
        $data      = array();
        $post_data = $this->input->post();
        foreach ($post_data as $key => $value) {
            if ($key != 'form_type') {
                $data[$key] = $value;
            }
        }
        $data['password'] = sha1($data['password']);
        $insert_id        = $this->Model_shopper_driver->save($data);
        return $insert_id;
    }
    private function update()
    {
        $data      = array();
        $update_by = array();
        $id        = $this->input->post('id');
        $post_data = $this->input->post();
        foreach ($post_data as $key => $value) {
            if ($key != 'form_type' && $key != 'id') {
                $data[$key] = $value;
            }
        }
        $data['password'] = sha1($data['password']);
        $update           = $this->Model_shopper_driver->update($data, $id);
        return $update;
    }
    private function update_status()
    {
        $data      = array();
        $update_by = array();
        $id        = $this->input->post('id');
        $post_data = $this->input->post();
        foreach ($post_data as $key => $value) {
            if ($key != 'form_type' && $key != 'id') {
                $data[$key] = $value;
            }
        }
        $update = $this->Model_shopper_driver->update($data, $id);
        return $update;
    }
    private function deleteData()
    {
        $delete           = false;
        $deleted_by['id'] = $this->input->post('id');
        $delete           = $this->Model_admin_roles->delete($deleted_by);

        // save logs here
        $logData = array(
            'type'=>    'delete',
            'section'=> 'Admin Roles'
        );
        saveLogs($logData);

        return $delete;
    }
    private function validation()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<p>', '</p>');
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('badge_number', 'Badge Number', 'required');
        $this->form_validation->set_rules('contact_number', 'Contact Number', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('city', 'City', 'required');
        $this->form_validation->set_rules('latitude', 'Latitude', 'required');
        $this->form_validation->set_rules('longitude', 'Longitude', 'required');
        $this->form_validation->set_rules('job_type', 'Job Type', 'required');
        $this->form_validation->set_rules('role', 'Role', 'required');
        if ($this->form_validation->run() == FALSE) {
            $errors['error']   = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }
    }

    public function saveUsersRights()
    {
        $this->user_validation();

        $user                    = $this->session->userdata('user');
        $user_id                 = $user['id'];
        $user_role               = $user['user_type'];
        $data                    = array();
        $data_new                = array();
        $data                    = $this->input->post();
        $user_rights             = array();
        $dataRole                = array();
        $post_data['role']       = $this->input->post('name');
        $post_data['updated_by'] = $user_id;
        $post_data['updated_at'] = date("Y-m-d H:i:s");
        foreach ($post_data as $key => $value) {
            if ($key != 'form_type') {
                $dataRole[$key] = $value;
            }
        }
        $insert_id                   = $this->Model_admin_roles->save($dataRole);
        $user_rights                 = $this->input->post();
        $user_rights['user_role_id'] = $insert_id;
        $user_role_id                = $user_rights['user_role_id'];
        $insert_arr                  = array();
        $insert_arr['user_role_id']  = $insert_id;
        foreach ($user_rights['page_ids'] as $page_id) {
            $insert_arr['page_id'] = $page_id;
            if ($user_rights['show_p_' . $page_id]) {
                $insert_arr['show_p'] = '1';
            } else {
                $insert_arr['show_p'] = '0';
            }
			if ($user_rights['view_p_' . $page_id]) {
                $insert_arr['view_p'] = '1';
            } else {
                $insert_arr['view_p'] = '0';
            }
            if ($user_rights['add_p_' . $page_id]) {
                $insert_arr['add_p'] = '1';
            } else {
                $insert_arr['add_p'] = '0';
            }
            if ($user_rights['edit_p_' . $page_id]) {
                $insert_arr['edit_p'] = '1';
            } else {
                $insert_arr['edit_p'] = '0';
            }
            if ($user_rights['delete_p_' . $page_id]) {
                $insert_arr['delete_p'] = '1';
            } else {
                $insert_arr['delete_p'] = '0';
            }

            /*if ($user_rights['export_p_' . $page_id]) {
                $insert_arr['export_p'] = '1';
            } else {
                $insert_arr['export_p'] = '0';
            }
			if ($user_rights['import_p_' . $page_id]) {
                $insert_arr['import_p'] = '1';
            } else {
                $insert_arr['import_p'] = '0';
            }*/
            $new_insert_id = $this->Model_user_rights->insertUserRights($insert_arr);
        }

        // save logs here
        $logData = array(
            'type'=>    'add',
            'section'=> 'User Right'
        );
        saveLogs($logData);

        if ($new_insert_id > 0) {
            $data['success'] = 'User Rights created successfully.';
            $data['error']   = 'false';
            $data['reset']   = 1;
            echo json_encode($data);
            exit;
        }
    }

    public function updateUsersRights()
    {
        $this->user_validation();
        $user                    = $this->session->userdata('user');
        $user_id                 = $user['id'];
        $user_role               = $user['user_type'];
        $data                    = array();
        $data_new                = array();
        $data                    = $this->input->post();
        $user_rights             = array();
        $dataRole                = array();
        $post_data['role']       = $this->input->post('name');
        $post_data['updated_by'] = $user_id;
        $post_data['updated_at'] = date("Y-m-d H:i:s");
        $user_right             = $this->input->post();
        unset($user_right['role_id']);
        $user_rights             = $user_right;

        $rolUpdateBy['id']       = $this->input->post('role_id');
        $this->Model_admin_roles->update($post_data,$rolUpdateBy);

        $insert_arr              = array();
        foreach ($user_rights['rights_ids'] as $user_rights_id) {
            if ($user_rights['show_p_' . $user_rights_id]) {
                $insert_arr['show_p'] = '1';
            } else {
                $insert_arr['show_p'] = '0';
            }
			if ($user_rights['view_p_' . $user_rights_id]) {
                $insert_arr['view_p'] = '1';
            } else {
                $insert_arr['view_p'] = '0';
            }
            if ($user_rights['add_p_' . $user_rights_id]) {
                $insert_arr['add_p'] = '1';
            } else {
                $insert_arr['add_p'] = '0';
            }
            if ($user_rights['edit_p_' . $user_rights_id]) {
                $insert_arr['edit_p'] = '1';
            } else {
                $insert_arr['edit_p'] = '0';
            }
            if ($user_rights['delete_p_' . $user_rights_id]) {
                $insert_arr['delete_p'] = '1';
            } else {
                $insert_arr['delete_p'] = '0';
            }
            if ($user_rights['export_p_' . $user_rights_id]) {
                $insert_arr['export_p'] = '1';
            } else {
                $insert_arr['export_p'] = '0';
            }
			if ($user_rights['import_p_' . $user_rights_id]) {
                $insert_arr['import_p'] = '1';
            } else {
                $insert_arr['import_p'] = '0';
            }
            $updateData = $this->Model_user_rights->updateUserRights($insert_arr, $user_rights_id);
        }

        // save logs here
        $logData = array(
            'type'=>    'update',
            'section'=> 'User Right'
        );
        saveLogs($logData);

        if ($updateData > 0) {
            $data['success'] = 'User Rights updated successfully.';
            $data['error']   = 'false';
            $data['reset']   = 1;
            echo json_encode($data);
            exit;
        } else {
            $data['success'] = 'User Rights updated successfully.';
            $data['error']   = 'false';
            $data['reset']   = 1;
            echo json_encode($data);
            exit;
        }
    }

    private function user_validation()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<p>', '</p>');
        $this->form_validation->set_rules('name', 'Name', 'required');

        if ($this->form_validation->run() == FALSE) {
            $errors['error']   = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }
    }
}
