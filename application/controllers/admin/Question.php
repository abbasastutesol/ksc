<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Question extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
    {
        parent::__construct();
       
	    $this->load->model('Model_question');
	    $this->load->model('Model_answer');
		checkAdminSession();
		//$res = checkLevels(2);
		//checkAuth($res);
    }
	
	public function index()
	{
		$data = array();
		$data['question'] = $this->Model_question->getAll();
		$data['content'] = 'question/manage';
		$data['class'] = 'question'; 
		$this->load->view('template',$data);	
		
	}
	
	public function action()
	{
		
		if(isset($_POST['form_type']))
		{

			switch($_POST['form_type'])
			{

				case 'save':
					$this->validation();
					$data['insert_id'] = $this->save();
					if($data['insert_id'] > 0)
					{
						$data['success'] = 'Qusetion has been saved successfully.';
						$data['error'] = 'false';
						$data['reset'] = 1;
						echo json_encode($data);
						exit;
					}
				break;

				case 'delete':
					
					if($this->deleteData())
					{
						$data['success'] = 'Qusetion deleted successfully.';
						$data['error'] = 'false';
						echo json_encode($data);
						exit;
					}else
					{
						$data['success'] = 'false';
						$data['error'] = 'Qusetion not deleted successfully. Please try again.';
						echo json_encode($data);
						exit;
					}
					

				break;

				case 'update':

					$this->validation();
					if($this->update())
					{
						$data['success'] = 'Qusetion has been updated successfully.';
						$data['error'] = 'false';
						echo json_encode($data);
						exit;
					}else
					{
						$data['success'] = 'false';
						$data['error'] = 'Qusetion has not been updated successfully.Please try again';
						echo json_encode($data);
						exit;
					}
										

				break;
				
			}
		}	
		
	}
	
	
	public function add()
	{
		$data = array();
		
		$data['content'] = 'question/add';
		$data['class'] = 'question';
		$this->load->view('template',$data);
	}
	
	public function edit($id)
	{
		$data = array();
		$fetch_by = array();
		$fetch_by['question_id'] = $id; //  we make this array to pass getMultipleRows as it return data by getting array.
		
		$data['question']	 = 	$this->Model_question->get($id);
		
		$data['answer']	 = 	$this->Model_answer->getMultipleRows($fetch_by);
		$data['content'] 	 =  'question/edit';
		$data['class'] = 'question';
		$this->load->view('template',$data);
	}
	
	private function save()
	{
		$data = array();
		$eng_values = array();
		$arb_values = array();
		$post_data = $this->input->post();
		$eng_answer = $this->input->post('eng_answer');
		$arb_answer = $this->input->post('arb_answer');
		
		
		foreach($post_data as $key => $value)
		{
			
			if($key != 'form_type' && $key != 'eng_answer' && $key != 'arb_answer')
			{
				$data[$key] = $value;	
			}
		}
		
		
		
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['created_by'] = $this->session->userdata['user']['id']; 
		
				
		$insert_id = $this->Model_question->save($data);
		
		$i = 0;
		$k = 1;	
		$values = array();
		$total_val = count($eng_answer);
		foreach($eng_answer as $value)
		{
				//$values['eng_value']  = ($total_val == $k ? $value : $value['s_'.$k.':i_0']);
				$values['eng_answer']  = $value;
				if($values['eng_answer'] != '')
				{
					$values['question_id'] = $insert_id;
					$values['arb_answer']  = $arb_answer[$i];
					$values['created_at'] = date('Y-m-d H:i:s');
					$values['created_by'] = $this->session->userdata['user']['id']; 
					$this->Model_answer->save($values);
					
				}
				
				$i++;
				$k++;
			
		}
		
		return $insert_id;
	}
	
	private function update()
	{
		$data = array();
		$eng_values = array();
		$arb_values = array();
		$variant_value_ids = array();
		$post_data = $this->input->post();
		//$eng_values = explode(',',$this->input->post('eng_value'));
		//$arb_values = explode(',',$this->input->post('arb_value'));
		$eng_answer = $this->input->post('eng_answer');
		$arb_answer = $this->input->post('arb_answer');
		/*if(count($eng_values) != count($arb_values))
		{
			$data['success'] = 'false';
			$data['error'] = 'Variant values for english and arabic must have same count ';
			echo json_encode($data);
			exit;
		}*/
		
		foreach($post_data as $key => $value)
		{
			
			if($key != 'form_type' && $key != 'eng_answer' && $key != 'arb_answer' && $key != 'id' && $key != 'ans_ids')
			{
				$data[$key] = $value;	
			}
		}
		
		
		
		$data['created_at'] = date('Y-m-d H:i:s');
		$data['created_by'] = $this->session->userdata['user']['id']; 
		
				
		$update_by['id'] = $this->input->post('id');		
		$update = $this->Model_question->update($data,$update_by);
		
		$deleted_by['question_id'] = $this->input->post('id');
		//$this->Model_variant_value->delete($deleted_by);
		$ans_ids = $this->input->post('ans_ids');
		$i = 0;
		$k = 1;	
		$values = array();
		$total_val = count($eng_answer);
		
		foreach($eng_answer as $val)
		{
				//$values['eng_value']  = ($total_val == $k ? $val : $val['s_'.$k.':i_0']);
				$values['eng_answer']  = $val;
				if($values['eng_answer'] != '')
				{
					$values['question_id'] = $update_by['id'];
					//$values['eng_value']  = ($total_val == $k ? $val : $val['s_'.$k.':i_0']);
					//$values['arb_value']  = ($total_val == $k ? $arb_values[$i] : $arb_values[$i]['s_'.$k.':i_1']);
					$values['arb_answer']  = $arb_answer[$i];
					
					if(isset($ans_ids[$i]))
					{
						$update_ans_by['id'] = $ans_ids[$i]; 
						$values['updated_at'] = date('Y-m-d H:i:s');
					    $values['updated_by'] = $this->session->userdata['user']['id'];
						$this->Model_answer->update($values,$update_ans_by);						
					}else
					{
						$values['created_at'] = date('Y-m-d H:i:s');
						$values['created_by'] = $this->session->userdata['user']['id']; 
						$this->Model_answer->save($values);
					}
					
					
				}
				
				$i++;
				$k++;
			
		}
		
		
		return $update;
	}
	
	private function deleteData()
	{
		
		/*$fetch_by['question_id'] = $this->input->post('id');
		$product_variant_value = $this->Model_product_variant_value->getMultipleRows($fetch_by);*/
		if($product_variant_value || 1==2)
		{
			$data['success'] = 'false';
			$data['error'] = 'You can not delete this variant.It is attach with some product.';
			echo json_encode($data);
			exit;
			
		}else
		{
			$deleted_by['id'] = $this->input->post('id');
			$delete = $this->Model_question->delete($deleted_by);
			$deleted_question_id_by['question_id'] = $this->input->post('id');
			$this->Model_answer->delete($deleted_question_id_by);
			return $delete;
		}
		
		
	}
	
	private function deleteVariantValue()
	{
		
		$fetch_by['variant_id'] = $this->input->post('id');
		$product_variant_value = $this->Model_product_variant_value->getMultipleRows($fetch_by);
		if($product_variant_value)
		{
			$data['success'] = 'false';
			$data['error'] = 'You can not delete this variant.It is attach with some product.';
			echo json_encode($data);
			exit;
			
		}else
		{
			$deleted_by['id'] = $this->input->post('id');
			$delete = $this->Model_variant->delete($deleted_by);
			$deleted_variant_value_by['variant_id'] = $this->input->post('id');
			$this->Model_variant_value->delete($deleted_variant_value_by);
			return $delete;
		}
		
		
	}
	
	private function validation()
	{
		    $errors = array();
			$this->form_validation->set_error_delimiters('<p>', '</p>');
           
			$this->form_validation->set_rules('eng_question', 'Eng Question', 'required');
			$this->form_validation->set_rules('arb_question', 'Arb Question', 'required');
			//$this->form_validation->set_rules('eng_content', 'English Content', 'required');
			//$this->form_validation->set_rules('arb_content', 'Arabic Content', 'required');
			
			
			if ($this->form_validation->run() == FALSE)
			{
				$errors['error'] = validation_errors();
				$errors['success'] = 'false';
				echo json_encode($errors);
				exit;
			}else
			{
				return true;
			}
			
	}
	
	
	
}
