<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Currency extends CI_Controller
{
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_currency');
        checkAdminSession();
        $this->user_rights_array = user_rights();
        //$res = checkLevels(2);
        //checkAuth($res);
    }

    public function action()
    {
        if (isset($_POST['form_type'])) {
            switch ($_POST['form_type']) {
                case 'add':
                    $this->validation();
                    if ($this->save()) {
                        $data['success'] = 'Currency has been saved successfully.';
                        $data['error'] = 'false';
                        echo json_encode($data);
                        exit;
                    } else {
                        $data['success'] = 'false';
                        $data['error'] = 'Currency has not been saved successfully.Please try again';
                        echo json_encode($data);
                        exit;
                    }
                    break;
                case 'update':
                    $this->validation();
                    if ($this->update()) {
                        $data['success'] = 'Currency has been updated successfully.';
                        $data['error'] = 'false';
                        echo json_encode($data);
                        exit;
                    } else {
                        $data['success'] = 'false';
                        $data['error'] = 'Currency has not been updated successfully.Please try again';
                        echo json_encode($data);
                        exit;
                    }
                    break;
            }
        }
    }


    public function index()
    {
        if ($this->user_rights_array['Currency']['show_p'] == 1) {
            $data = array();
            $data['currencies'] = $this->Model_currency->getAll();
            $data['content'] = 'currency/manage';
            $data['class'] = 'currency';
            $this->load->view('template', $data);
        } else {
            show_404();
        }
    }

    public function add()
    {
        if ($this->user_rights_array['Currency']['show_p'] == 1) {
            $data = array();
            $data['content'] = 'currency/add';
            $data['class'] = 'currency';
            $this->load->view('template', $data);
        } else {
            show_404();
        }
    }

    private function save($id)
    {
        $post_data = $this->input->post();
        if ($post_data['default_currency'] != NULL) {
            $post_data['default_currency'] = 1;
        } else {
            $post_data['default_currency'] = 0;
        }

        $default_curr =$this->checkDefault($post_data['default_currency']);

        if($default_curr == 1) {

            unset($post_data['form_type']);


            $save = $this->Model_currency->save($post_data);
            return $save;
        }

        if($default_curr > 1) {
            $data['success'] = 'false';
            $data['error'] = 'Default currency is already setPlease make Just one currency as default';
            echo json_encode($data);
            exit;
        }
        if($default_curr == 0) {
            $data['success'] = 'false';
            $data['error'] = 'Please make at least one currency as default';
            echo json_encode($data);
            exit;
        }
    }

    public function edit($id)
    {
        if ($this->user_rights_array['Currency']['show_p'] == 1) {
            $data = array();
            $data['currency'] = $this->Model_currency->get($id);
            $data['content'] = 'currency/edit';
            $data['class'] = 'currency';
            $this->load->view('template', $data);
        } else {
            show_404();
        }
    }

    private function update()
    {
        $data = array();
        $eng_values = array();
        $arb_values = array();
        $variant_value_ids = array();
        $post_data = $this->input->post();
        $id = $this->input->post('id');

        if($post_data['default_currency'] != NULL){
            $post_data['default_currency'] = 1;
        }else{
            $post_data['default_currency'] = 0;
        }

        foreach ($post_data as $key => $value) {
            if ($key != 'form_type') {
                $data[$key] = $value;
            }
        }

        $default_check = $this->checkDefault($post_data['default_currency'],$id);

        if($default_check == 1) {

            $data['created_at'] = date('Y-m-d H:i:s');
            $data['created_by'] = $this->session->userdata['user']['id'];
            $data['updated_at'] = date('Y-m-d H:i:s');
            $update_by['id'] = $id;
            $update = $this->Model_currency->update($data, $update_by);
            // save logs here
            $logData = array(
                'type' => 'update',
                'section' => 'Currency'
            );
            saveLogs($logData);

            return $update;
        }
        if($default_check > 1) {
            $data['success'] = 'false';
            $data['error'] = 'Default currency is already set, Please make Just one currency as default';
            echo json_encode($data);
            exit;
        }
        if($default_check == 0) {
            $data['success'] = 'false';
            $data['error'] = 'Please make at least one currency as default';
            echo json_encode($data);
            exit;
        }

    }

    private function validation()
    {
        $errors = array();
        $this->form_validation->set_error_delimiters('<p>', '</p>');
        $this->form_validation->set_rules('eng_currency', 'Eng Currency Field', 'required');
        $this->form_validation->set_rules('arb_currency', 'Arb Currency Field', 'required');
        $this->form_validation->set_rules('rate', 'Rate Fiels', 'required');
        //$this->form_validation->set_rules('arb_content', 'Arabic Content', 'required');
        if ($this->form_validation->run() == FALSE) {
            $errors['error'] = validation_errors();
            $errors['success'] = 'false';
            echo json_encode($errors);
            exit;
        } else {
            return true;
        }
    }

    private function checkDefault($default,$id){


        $count = $this->Model_currency->checkDefault($default,$id);

        return $count;
    }
}
