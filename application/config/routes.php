<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$route['search'] = 'search';
$route['default_controller'] = 'index';

$route['admin'] = 'admin/login';

$route['showrooms'] = 'showrooms';
$route['showrooms/detail/(:any)'] = 'showrooms/detail/$1';
$route['product/search'] = "product/search";

$route['product/request_qouta'] = "product/request_qouta";
$route['ajax/email_to_admin'] = "ajax/email_to_admin";
$route['product/product_page/(:any)'] = "product/product_page/$1";
$route['career/action'] = 'career/action';
//$route['product/addToCart/(:any)'] = "product/addToCart/$i";

$route['product/(:any)'] = "product/index/$i";

$route['404_override'] = '';

$route['translate_uri_dashes'] = FALSE;




//Routes for the site.



$route['en'] = "index";



$route['en/register'] = 'register';
$route['en/search'] = 'search';

$route['en/my_orders'] = 'my_orders';

$route['en/setting'] = 'setting';

$route['en/career'] = 'career';

$route['en/wish_list'] = 'wish_list';

$route['en/shopping_cart'] = 'shopping_cart';

$route['en/shopping_cart/paymentPage'] = 'shopping_cart/paymentPage';

$route['en/shopping_cart/shippingAddress'] = 'shopping_cart/shippingAddress';
$route['en/profile/myOrder'] = 'Profile/myOrder';

$route['en/shopping_cart/paytab'] = 'shopping_cart/paytab';

$route['en/product'] = 'product';
$route['en/showrooms'] = 'showrooms';
$route['en/showrooms/detail/(:any)'] = 'showrooms/detail/$1';

$route['en/my_orders/printPage/(:any)'] = 'my_orders/printPage/$1';

$route['en/product/index/(:any)'] = "product/index/$1";

$route['en/product/product_page/(:any)'] = "product/product_page/$1";

$route['en/shopping_cart/addToCart'] = "shopping_cart/addToCart";

$route['en/my_orders/returnOrder'] = "my_orders/returnOrder";

$route['en/langSwitch'] = "langSwitch";

$route['en/my_orders/returnOrderSubmit'] = "my_orders/returnOrderSubmit";

$route['en/my_orders/returnOrderConfirmation'] = "my_orders/returnOrderConfirmation";

$route['en/my_orders/lostItem'] = "my_orders/lostItem";

$route['en/my_orders/lostItemSubmit'] = "my_orders/lostItemSubmit";

$route['en/my_orders/lostItemSave'] = "my_orders/lostItemSave";

$route['en/my_orders/placeFeedback'] = "my_orders/placeFeedback";

$route['en/my_orders/placeFeedbackSubmit'] = "my_orders/placeFeedbackSubmit";

$route['en/my_orders/placeFeedbackSave'] = "my_orders/placeFeedbackSave";

$route['en/my_orders/saveConfirmation'] = "my_orders/saveConfirmation";

$route['en/my_orders/details/(:any)'] = "my_orders/details/$1";

$route['en/login/logout'] = "login/logout";

$route['en/aboutUs'] = "aboutUs";

$route['en/designWithUs'] = "designWithUs";

$route['en/designers'] = "designers";

$route['en/qualityCare'] = "qualityCare";

$route['en/termsConditions'] = "termsConditions";

$route['en/returnPolicy'] = "returnPolicy";

$route['en/giveUsReview'] = "giveUsReview";

$route['en/product/dailyOffers'] = "product/dailyOffers";

$route['en/product/search'] = "product/search";

$route['en/product/request_qouta'] = "product/request_qouta";
$route['en/ajax/email_to_admin'] = "ajax/email_to_admin";

$route['en/ajax/voucherCode'] = "ajax/voucherCode";

$route['en/Company_profile'] = "Company_profile";

$route['en/Clients/load_add_page'] = "Clients/load_add_page";

$route['en/Clients/edit_client/(:any)'] = "Clients/edit_client/$1";

$route['en/Awards/award_page'] = "Awards/award_page";

$route['en/Show_room/add_showRoom'] = "Show_room/add_showRoom";

$route['en/Show_room/edit_record/(:any)'] = "Show_room/edit_record/$1";

$route['en/Projects/add_project'] = "Projects/add_project";

$route['en/Awards/edit_award/(:any)'] = "Awards/edit_award/$1";

$route['en/product/(:any)'] = "product/index/$i";

$route['en/Projects/edit_project/(:any)'] = "Projects/edit_project/$i";

$route['en/Career/job_dep'] = "Career/job_dep";

$route['en/Career/add_dep'] = "Career/add_dep";

$route['en/Career/edit_dep/(:any)'] = "Career/edit_dep/$i";

$route['en/Career/deleteDep'] = "Career/deleteDep";
$route['en/CompanyProfile/profile'] = "CompanyProfile/profile";
$route['en/VisionMission/vision'] = "VisionMission/vision";
$route['en/ChairmanMessage/message'] = "ChairmanMessage/message";
$route['en/CeoMessage/message'] = "CeoMessage/message";
$route['en/Value/values'] = "Value/values";
$route['en/Award/data'] = "Award/data";
$route['en/Client/data'] = "Client/data";
$route['en/Projects/data'] = "Projects/data";
$route['en/Projects/detail_project/(:any)'] = "Projects/detail_project/$i";
$route['en/Projects/gallery'] = "Projects/gallery";
$route['en/Projects/add_gallery_image'] = "Projects/add_gallery_image";
$route['en/Projects/edit_gallery_project/(:any)'] = "Projects/edit_gallery_project/$i";
$route['en/Projects/deleteGalleryImage'] = "Projects/deleteGalleryImage";
$route['en/Projects/get_Project_gallery/(:any)'] = "Projects/get_Project_gallery/$i";

$route['en/Legal_info/info'] = "Legal_info/info";
$route['en/offers'] = "Offers/index";
$route['en/login/login'] = "Login/login";
$route['en/profile'] = "Profile/index";
$route['en/shopping_cart/addToCartPopup'] = "shopping_cart/addToCartPopup";
$route['en/register/action'] = 'register/action';
$route['en/shopping_cart/productAddToCart'] = 'Shopping_cart/productAddToCart';
$route['en/shopping_cart/checkout'] = 'Shopping_cart/checkOut';
$route['en/shopping_cart/saveOrder'] = 'Shopping_cart/saveOrder';
$route['en/shopping_cart/sendOrderEmail'] = 'Shopping_cart/sendOrderEmail';
$route['en/contact_us'] = 'Contact_us/index';
$route['en/contact_us/save'] = 'Contact_us/save';
$route['en/career/action'] = 'career/action';
$route['en/career/detail/(:any)'] = 'career/detail/$1';
$route['en/faqs'] = 'Faqs/index';
$route['en/locations'] = 'Locations/index';
$route['en/career/career_open/(:any)'] = "Career/career_open/$1";
$route['en/customer_service'] = 'Customer_service/index';
$route['en/customer_service/latest_news'] = 'Customer_service/latest_news';
$route['en/customer_service/payment_confirm'] = 'Customer_service/payment_confirm';
$route['en/customer_service/returns'] = 'Customer_service/returns';
$route['en/customer_service/distributor'] = 'Customer_service/distributor';
$route['en/customer_service/privacy_policy'] = 'Customer_service/privacy_policy';
$route['en/customer_service/payment_confirm_save'] = 'Customer_service/payment_confirm_save';
$route['en/customer_service/medauf_program'] = 'Customer_service/medauf_program';

$route['en/Register/profile/(:any)'] = "Register/profile/$1";

$route['en/aboutUs/mission_and_vision'] = 'AboutUs/mission_and_vision';
$route['en/aboutUs/why_ioud'] = 'AboutUs/why_ioud';
$route['en/aboutUs/distinctiveness'] = 'AboutUs/distinctiveness';
$route['en/shopping_cart/orderPlaced/(:any)'] = 'Shopping_cart/orderPlaced/$1';
$route['en/Register/check_userEmail_exist'] = 'Register/check_userEmail_exist';
$route['en/Register/check_password_exist'] = 'Register/check_password_exist';
$route['en/Register/changePassword'] = 'Register/changePassword';
$route['en/Ajax/subscribeNewsLetter'] = 'Ajax/subscribeNewsLetter';

$route['en/login/reset_password/(:any)'] = 'Login/reset_password/$1';
