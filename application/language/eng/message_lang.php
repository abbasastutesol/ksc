<?php


/*



 * English language



 */


$label['site_title'] = 'IOUD';


// new labels start here for general
$label['enter_here_palceholder']				= 'Enter here';
$label['file_upload']				= 'Upload';
$label['select_dropdown']				= 'Select';
$label['edit_label']				= 'Edit';
$label['remove_label']				= 'Remove';
$label['cancel_label']				= 'Cancel';
$label['return_label']				= 'Return';
$label['add_new_address_label']				= 'Add a new address';
$label['address_label']				= 'Address';
$label['phone_label']				= 'Phone';
$label['logout_link']				= 'Log Out';
$label['view_all']				= 'View All';
$label['read_more']				= 'Read More';
$label['read_less']				= 'Read Less';
$label['mobile_no_label']				= 'Mobile number';
$label['submit_btn_label']				= 'Submit';
$label['full_name_label']				= 'Full Name';
$label['country_label']				= 'Country';
$label['city_label']				= 'City';
$label['browse_label']				= 'Browse';
$label['email_address_label']				= 'Email Address';

$label['name_label']				= 'Name';
$label['email_label']				= 'Email';
$label['mobile_label']				= 'Mobile';
$label['dismiss_label']				= 'Dismiss';
$label['signup_label']				= 'Sign up';

$label['products_count_label']				= 'Products';
$label['grand_total_label']				= 'Grand Total';
$label['dob_label']				= 'Date of birth';

$label['cancel_order_success_msg']				= 'Your order has been cancelled successfully!';
$label['delevered_order_success_msg']				= 'Your order has been delivered.';
$label['shipped_order_success_msg']				= 'Your order has been shipped.';
$label['cancel_order_failed_msg']				= 'Your order has not been cancelled, please try again!';

$label['order_not_find']				= "Sorry! we couldn't find the order, please enter the correct order no.";

$label['return_order_success_msg']				= 'Your order return request has been submitted successfully!';
$label['return_order_failed_msg']				= 'Your order return request has not been submitted, please try again!';

$label['form_submitted_successfully']				= 'Your Form has been submitted successfully';
$label['form_submitted_failed']				= 'There was some errors to submitted your form, please try again';

$label['contact_request_received']				= 'the contact us request has been received';
$label['contact_request_submitted']				= 'Your contact request has been submitted successfully';
$label['contact_request']				= 'Contact us request';
$label['hi_label']				= 'Hi';

$label['job_application']				= 'New Job Application';
$label['job_application_received']				= 'New Job Application has been received';
$label['job_application_user']				= 'Your Job Application has been received';

$label['ok_label']				= 'OK';

$label['order_confirm_sms']				= 'Thank you for your order Order confirmation number';

$label['forgot_password_email_not_found']				= 'Sorry, we could not found any account with this email address';

$label['password_request_processed']				= 'Your password reset request has been processed.';
$label['password_request_please_click']				= 'Please Click';
$label['password_request_click_here']				= 'Here';
$label['password_request_thanks']				= 'to reset your password. Thank you';

$label['send_new_password_msg']				= 'We have sent you an email containing new password to reset you password. Kindly check you inbox';
$label['send_new_password_failed']				= 'There was some problems to send email, please try again!';
$label['password_reset_successfully']				= 'Your password has been reset successfully';
$label['url_expire']				= 'Sorry, This url is expired';
$label['type_here']				= 'TYPE HERE';


$label['return_reason']				= 'Return Reason';
$label['product_receive_mistake']				= 'Received product by mistake';
$label['product_receive_damage']				= 'Product received a damaged';
$label['wrong_product']				= 'Wrong product';
$label['received_product_bug']				= 'There is a bug, please explain';
$label['received_product_other_reason']				= 'Other, please specify';
$label['package_closed']				= 'package closed';
$label['please_explain']				= 'Please explain';
$label['type_your_message']				= 'Type your Message';
$label['yes_label']				= 'Yes';
$label['no_label']				= 'No';

$label['your_order_label']				= 'Your order#';
$label['order_changed_label']				= 'has been changed to';
$label['order_status_label']				= 'status';

$label['want_to_cancel_order']				= 'i want to cancel my order#';
$label['want_to_return_order']				= 'i want to return my order#';

$label['return_request_order']				= 'Order returned request';
$label['return_request_reason_label']				= 'under the following reason:';
$label['cancel_request_order']				= 'Order cancelled request:';

$label['order_feedback_received']				= 'The feedback has been received form';
$label['order_feedback_against_order']				= 'against order.';
$label['order_feedback_subject']				= 'Order Feedback.';

$label['order_processed_title']				= 'Order Processed';
$label['do_it_later']				= 'Do it Later';
$label['forgot_password_email']				= 'Forgot password email';

$label['go_back_to_previous']				= 'Back';

$label['shopping_car_old_price']				= 'Old Price';
$label['shopping_car_discount_price']				= 'Discount Applied';


// end labels start here for general

// new labels start here for index.php page
$label['products_label']				= 'Products';
$label['offers_label']				= 'Offers';

$label['home_page_label']				= 'Home Page';
$label['year_label']				= 'Home Page';

$label['all_fields_required']				= 'All fields are required';

$label['loyalty_discount_label']				= 'Meduaf Discount';


// end new labels for index.php page


// new labels start here for product detail page
$label['price']				= 'Price';
$label['related_products_label']				= 'Related Products';
$label['view_more_label']				= 'View More';
$label['view_less_label']				= 'View Less';
$label['add_to_my_cart_label']				= 'Add to My Cart';
$label['shopping_out_of_stock']				= 'Out of Stock';
$label['related_product_not_available_label']				= 'No Related Product Available';

$label['product_out_of_stock']				= 'This product is out of stock, please choose another';


// end new labels for product detail page

// new labels start here for product page
$label['all_products_label']				= 'All Products';
$label['no_record_founds_label']				= 'No record found!';

// end new labels for product page


// new labels start here for login popup
$label['login_register_label']				= 'Login / Register';
$label['login_btn_label']				= 'Log In';
$label['dont_have_account_label']				= "Don't have an account?";
$label['register_now_label']				= "Register Now";
$label['forgot_password_label']				= "Forgot Password";
$label['username_password_invalid_label']				= "Username and password invalid!";
$label['login_username_label']				= 'Email';
$label['login_password_label']				= 'Password';

$label['login_new_user_label']				= 'Login/New user';

// end new labels for login popup


// new labels start here for register page
$label['register_new_account_label']				= 'Register a new account';
$label['update_account_label']				= 'Update account';
$label['register_first_name_label']				= 'First Name';
$label['register_last_name_label']				= 'Last name';
$label['register_email_label']				= 'Email Address';
$label['register_mobile_number_label']				= 'Mobile No.';
$label['register_city_label']				= 'City';
$label['register_country_label']				= 'Country';
$label['register_password_label']				= 'Password';
$label['register_confirm_password_label']				= 'Confirm password';
$label['register_dob_label']				= 'Date of Birth';

$label['register_i_accept_label']				= 'I accept the';
$label['register_terms_and_conditions_label']				= 'terms and conditions.';
$label['register_btn_label']				= 'Register';
$label['update_btn_label']				= 'Update';


// end new labels for register page


// new labels start here for profile page
$label['profile_my_account_label']				= 'My Account';
$label['profile_biography_label']				= 'Biography';
$label['profile_change_password_label']				= 'Change Password';
$label['profile_dob_label']				= 'Date of Birth.';
$label['profile_password_label']				= 'Password';
$label['profile_email_label']				= 'Email Address';
$label['profile_moblie_label']				= 'Mobile No.';
$label['profile_change_data_label']				= 'Change data';
$label['my_profile_label']				= 'My Profile';

$label['my_profile_sliver_no_label']				= 'Silver Number';

$label['my_profile_loyalty_silver']				= 'Silver';
$label['my_profile_loyalty_gold']				= 'Golden';
$label['my_profile_loyalty_platinum']				= 'Platinum';


// end new labels for profile page

// new labels start here for promotion page
$label['all_promotions_label']				= 'All Promotions';

// end new labels for promotion page


// new labels start here for customer service and its inner page
$label['customer_service_label']				= 'Customer Services';
$label['payment_and_delivery_label']				= 'Payment & Delivery';
$label['latest_news_label']				= 'News';
$label['payment_confirmation_label']				= 'Payment Confirmation';
$label['returns_label']				= 'Returns';

$label['custom_returns_label']				= 'Returns';

$label['distributor_label']				= 'Be a Distributor';
$label['privacy_policy_label']				= 'Privacy Policy';

// end new labels for customer service and its inner page


// new labels start here for payment confirmation page
$label['payment_order_no_label']				= 'Order Number';
$label['payment_customer_name_label']				= 'Customer Name';
$label['payment_bank_name_label']				= 'Bank Name';
$label['payment_bank_account_no_label']				= 'Account No';
$label['payment_bank_iban_no_label']				= 'IBAN No';
$label['payment_bank_account_name_label']				= 'Bank Account Holder’s Name';
$label['payment_date_and_time_label']				= 'Date & Time of Transfer';
$label['payment_amount_transfer_label']				= 'Amount Transferred';

// end new labels for payment confirmatione page



// new labels start here for distributor page
$label['distributor_store_name_label']				= 'Store Name';
$label['distributor_street_address_label']				= 'Street Address';
$label['distributor_district_label']				= 'District';
$label['distributor_store_number_label']				= 'Store Number';
$label['distributor_pin_location_label']				= 'pin Location';
$label['distributor_store_images_label']				= 'Store Images';

$label['location_page_label']				= 'Locations';

// end new labels for payment distributor page



// new labels start here for contact us page
$label['contact_us_label']				= 'Contact Us';
$label['contact_us_details_label']				= 'Contact Details';
$label['contact_us_headoffice_label']				= 'Headoffice';
$label['contact_us_feedback_label']				= 'Feedback';
$label['contact_us_phone_label']				= 'T';
$label['contact_us_follow_us_label']				= 'Follow Us';
$label['contact_us_toll_free_label']				= 'Toll Free';

$label['feedback_detail_label']				= 'Details';

// end new labels for contact us page



// new labels start here for Faqs page
$label['faqs_label']				= 'Frequently Asked Questions';
$label['faqs_question_label']				= 'Q.';
$label['faqs_answer_label']				= 'A.';

// end new labels for Faqs page


// new labels start here for Career and career open page
$label['careers_label']				= 'Careers';
$label['careers_job_open_label']				= 'Job Openings';
$label['careers_apply_now_label']				= 'Apply now';

$label['careers_age_label']				= 'Age';
$label['careers_cv_label']				= 'CV';
$label['careers_experience_label']				= 'Years of Experience';
// end labels start here for Career and career open page



// new labels start here for Newsletter
$label['newsletter_label']				= 'Sign up out monthly Newsletter';
$label['newsletter_enter_email_label']				= 'Enter email address';

// end new labels for Newsletter


// new labels start here for Menu
$label['menu_my_account_label']				= ' My Account';
$label['menu_login_label']				= 'Login';
$label['menu_logout_label']				= 'Log Out';

$label['menu_about_us_label']				= 'About Us';

$label['menu_customer_services_label']				= 'Customer Services';
$label['menu_mission_vission_label']				= 'Mission & Vision';
$label['menu_why_ioud_label']				= 'Why IOud';

$label['menu_distinctiveness_label']				= 'Our Distinctiveness';
$label['menu_special_offers_label']				= 'Special Offers';
$label['menu_all_products_label']				= 'All Products';
$label['menu_contact_us_label']				= 'Contact Us';
$label['menu_faqs_label']				= 'FAQs';
$label['menu_careers_label']				= 'Careers';
$label['menu_newsletter_label']				= 'Newsletter';
$label['menu_trusted_partners_label']				= 'Trusted Partners';

// end new labels for  Menu



// my order page

$label['my_order_current_order_label']				= 'Current Orders';
$label['my_order_completed_label']				= 'Completed';

$label['my_order_order_no_label']				= 'Order Number';
$label['my_order_received_label']				= 'Order Received';
$label['my_order_preparing_ship_label']				= 'Preparing Shipment';

$label['my_order_shipped_label']				= 'Shipped';



$label['order_preparing_title']				= 'Order Processed';
$label['order_preparing_ship_title']				= 'Order Processed';
$label['order_shipped_title']				= 'Order Processed';
$label['order_delivered_title']				= 'Order Processed';
$label['order_returned_title']				= 'Order Processed';
$label['order_rejected_title']				= 'Order Processed';


$label['my_order_preparing_order_message']				= 'Preparing Order';
$label['my_order_preparing_ship_message']				= 'is being shipped';
$label['my_order_shipped_message']				= 'has been Shipped';
$label['my_order_delivered_message']				= 'has been delivered';
$label['my_order_rejected_message']				= 'has been rejected';
$label['my_order_returned_message']				= 'has been returned';



$label['my_order_delivered_label']				= 'Delivered';
$label['my_order_cancelled_label']				= 'Cancelled';
$label['my_order_verify_payment_label']				= 'Verify Payment';
$label['my_order_returned_label']				= 'Returned';
$label['my_order_rejected_label']				= 'Rejected';
$label['my_order_preparing_order_label']				= 'Preparing Order';
$label['my_order_reviews_label']				= 'Reviews &amp; Feedback';

$label['my_order_product_quality_label']				= 'Product Quality';
$label['my_order_packaging_label']				= 'Shipment Packaging';
$label['my_order_packing_label']				= 'Product Packing';
$label['my_order_ioud_customer_service_label']				= 'iOud Customer Service';
$label['my_order_shipment_duration_label']				= 'Shipment Duration';

$label['my_order_satisfaction_label']				= 'Overall Satisfaction';
$label['my_order_feedback_placeholder']				= 'Feedback';

$label['my_order_placed_successfully']				= 'Order Placed Successfully';
$label['my_order_print']				= 'Print';
$label['my_order_your_order_recieved']				= 'Your Order has been received';
$label['my_order_we_will_deliver']				= 'Will be delivered in ';
$label['my_order_bussiness_days']				= 'Business days';
$label['my_order_approximately_to']				= 'Approximately to';
$label['my_order_cod_label']				= 'COD Charges';


// end my order page

// registration message

$label['register_successfully_msg'] 		=	"Registered successfully!";

$label['register_successfully_to_user'] 		=	"Your Account has been created successfully";

$label['register_user_subject'] 		=			"User Registration";
$label['register_user_from'] 		=			"From";
$label['user_request_to_admin']		= 		"The user registration request has been received and here is the user detail";
// end message



// all right reserved

$label['all_right_reserved_label']				= 'All rights reserved to IOUD company';

// all right reserved end





//cart pages labels

$label['address_tittle_label'] = 'Address Title';
$label['street_address_1_label'] = 'Street address 1';

$label['apartment_building_label'] = 'Apartment / building No.';
$label['street_address_2_label'] = 'Street address 2';

$label['area_district_label'] = 'Area / District / Street';
$label['save_label'] = 'Save';

$label['atten_login_label'] = 'Attention! Login';
$label['username_label'] = 'Username';

$label['reset_password_label'] = 'Reset Password';
$label['alert_label'] = 'Alert';

$label['previous_password'] = 'Previous Password';

$label['previous_password_label'] = 'Previous Password is invalid!';
$label['password_change_label'] = 'Password changed Successfully!';
$label['new_password_label'] = 'New Password';

$label['confirm_password_label'] = 'Confirm Password';
$label['change_password_label'] = 'Change Password';

$label['registeration_success_label'] = 'Your Registration has been done successfully.';
$label['pofile_update_label'] = 'Profile Updated successfully.';

$label['email_already_label'] = 'This email is already existing.';
$label['password_change_label'] = 'Password has been changed successfully.';

$label['old_password_label'] = 'Old password is incorrect.';
$label['cannot_process_accept_label'] = 'You Cannot Process without accepting';

$label['cannot_process_upload_label'] = 'You Cannot Process without Uploading the Files.';
$label['terms_condition_label'] = 'Terms and conditions.!';

$label['do_you_want_to_register_label'] = 'Do you want to create an account?';
$label['accept_term_and_conditions_msg'] = 'You Cannot Process without accepting Terms and conditions';


$label['order_email_sent_successfully'] = 'Email sent successfully.';


$label['login_sign_in']				= 'Sign in';




$label['login_welcome']				= 'Welcome';



$label['login_all_categories']		= 'All categories';



$label['login_search_word']		= 'Search word';



$label['login_exhibits']				= 'Exhibits';



$label['login_best_seller']			= 'Best seller';



$label['login_quick_links']			= 'Quick Links';



$label['login_newsletters']			= 'Newsletters';



$label['login_send']					= 'Send';



$label['login_payment_methods']		= 'Payment Method';



$label['login_follow_us']			= 'Follow us';



$label['login_abaya']				= 'Abaya';



$label['login_blouses']				= 'Blouses';



$label['login_daily_offers']			= 'Daily Offers';



$label['login_communicate_with_customer']				= 'Communicate with customer service team.';



$label['login_get_to_know_us']				= 'Get to know us';



$label['login_about_com']				= 'About eDesign';



$label['login_investor_relations']				= 'Investor Relations';



$label['login_careers']				= 'Careers';



$label['login_cooperate_with_us']				= 'Cooperate with us';



$label['login_design_with_com']				= 'Design with eDesign';



$label['login_designers']				= 'Designers';



$label['login_terms_conditions']				= 'Terms & Conditions';



$label['login_quality_offers']				= 'Quality & Offers';



$label['login_cloth_care']				= 'Cloth Care';



$label['login_give_us_review']				= 'Give us a review';



$label['login_subscribe_to_newsletters']				= 'Subscribe to newsletters';



$label['login_returns_policy']				= 'Returns Policy';



$label['login_delivery']				= 'Delivery';



$label['login_all_rights_reserved']				= 'All Rights Reserved';



$label['login_follow_us']				= 'Follow us';



$label['login_fail']				= 'Username or password incorrect.';



$label['login_success']				= 'Login Successfully';







$label['aboutus_who_we_are']				= 'Who We Are';







$label['chat_connect_with_us']				= 'Connect with us';



$label['chat_name']				= 'Name';



$label['chat_email']				= 'Email';



$label['chat_message']				= 'Message';



$label['chat_start_chat']				= 'Start Chat';



$label['com_admin']				= 'Admin';







$label['home_display_blouse']				= 'Blouse';



$label['home_display_product_name']				= 'Product name';



$label['home_display_vote_this_product']				= 'Vote this product';



$label['home_display_sar']				= 'SAR';



$label['home_display_size']				= 'Size';



$label['home_display_select']				= 'Select';



$label['home_display_sizes_chart']				= 'Sizes Chart';



$label['home_display_add_to_shopping_cart']				= 'Add to shopping Cart';



$label['home_display_more_details']				= 'More details';



$label['home_display_add_to_wish_list']				= 'Add to wish list';







$label['products_new_arrival_label']				= 'New Arrival';



$label['products_best_seller_label']				= 'Best Seller';



$label['products_recently_viewed_products']				= 'Recently viewed products';



$label['products_price']				= 'Price';



$label['products_sar']				= 'SAR';



$label['products_color']				= 'Color';



$label['products_size']				= 'Size';



$label['products_clients_reviews']				= 'Clients Reviews';



$label['product_chart_saudi_inter']				= 'Saudi International';







$label['products_filtered_new_to_old']				= 'New to Old';





$label['products_page_similar_products_count']				= 'Products';



$label['products_page_singular_products_count']				= 'Product';



$label['products_page_similar_products']				= 'Similar Products';



$label['products_page_from_same_category']				= 'Products from the same category';



$label['products_page_your_voting']				= 'Your voting?';



$label['products_page_product_information']				= 'Product details & product care information';



$label['products_page_product_details']				= 'Product details';



$label['products_page_add_to_cart']				= 'Add to shopping cart';



$label['products_page_view_details']				= 'View details';



$label['products_page_material_use']				= 'Materials used';



$label['products_page_size_display']				= 'Size displayed in the picture';



$label['products_page_model_number']				= 'Supplier model number';



$label['products_page_project_measurement']				= 'Projector measurements / bar';



$label['products_page_length_bar']				= 'The length of the bidder / bar';



$label['products_page_washing_instruction']				= 'Washing instructions';



$label['products_page_collar_type']				= 'Collar type';



$label['products_page_brand_clothing']				= 'Brand clothing';



$label['products_page_customer_questions']				= 'Customer Questions';



$label['products_page_question']				= 'Question';



$label['products_page_answer']				= 'Answer';



$label['products_page_write_here']				= 'Write Here';



$label['products_page_customer_questions_success']				= 'Thank you. Will answer your question soon.';



$label['products_page_add_to_wish_list']				= 'Add to wish list';



$label['products_page_customer_bought_same']				= 'Customers who bought this product also bough the following products';



$label['products_page_continue_shopping']				= 'Continue Shopping';



$label['products_page_complete_shopping']				= 'Checkout';



$label['products_page_load_more']				= 'Load More';



$label['product_page_loading'] 					=  'Loading';



$label['products_largest_selection'] = 'Largest selection';



$label['products_first_time_in_kingdom'] = 'First Website in the Kingdom';







$label['size_chart_size_chart']				= 'Size Chart';



$label['size_chart_international']				= 'International';



$label['size_chart_saudi']				= 'Saudi';







$label['shopping_cart_shopping_cart']				= 'My Shopping Cart';



$label['shopping_cart_price']				= 'Price';



$label['shopping_cart_quantity']				= 'Quantity';



$label['shopping_cart_qty']				= 'Qty';



$label['shopping_cart_continue_shopping']				= 'Continue Checkout';



$label['shopping_cart_total']				= 'Total';



$label['shopping_cart_delete_product']				= 'Delete';



$label['shopping_cart_save_for_later']				= 'Save for later';



$label['shopping_cart_move_to_cart']				= 'Move to cart';


$label['shopping_cart_proceed_to_cart']				= 'Proceed to Cart';


$label['shopping_cart_actions']				= 'Actions';


$label['shopping_cart_last_piece']				= 'Last piece';


$label['shopping_cart_voucher_code_success']				= 'Voucher code accepted successfully.';


$label['shopping_cart_voucher_code_error']				= 'Voucher code is invalid or expired.';





$label['shopping_cart_voucher_code']				= 'Voucher Code';


$label['shopping_cart_discount_coupon']				= 'Discount Coupon';


$label['shopping_cart_apply']				= 'Apply';


$label['shopping_cart_total_cost']				= 'Total Cost';


$label['shopping_cart_enter_here']				= 'Enter Here';


$label['shopping_cart_coupon_enter_here']				= 'Enter the discount voucher';


$label['shopping_cart_proceed_to_checkout']				= 'Proceed to Checkout';

$label['shopping_cart_proceed_checkout']				= 'Proceed to Checkout';

$label['shopping_cart_proceed_to_checkout_btn']				= 'Proceed to Checkout';


$label['shopping_cart_added_to_cart']				= 'Added to Cart';


$label['shopping_product_added_to_cart']	= 'The product is added to your shopping cart.';



$label['shopping_product_does_not_exist']	= 'Selected product does not exist.';



$label['shopping_product_not_added_to_cart']	= 'Not added to the shopping cart.';



$label['shopping_product_no_product']	= 'There is no product.';



$label['shopping_product_thankyou']	= 'Thank you for shopping with us.';



$label['shopping_product_order_no']	= 'Here is the order number';



$label['shopping_add_payment'] = 'Add Payment';



$label['shopping_product_copy_order_no']	= 'Please save your order no and press finish.';



$label['shopping_product_finish']	= 'Finish';



$label['shopping_product_already_added_cards']	= 'Choose already registered card or add a new one';



$label['shopping_product_already_added_to_cart']	= 'The product is already added to your cart.';



$label['shopping_cart_saved_products']	= 'Saved Products';



$label['shopping_cart_no_saved_products']	= 'No Save Products';









$label['checkout_choose_mailing_address']				= 'choose mailing address';



$label['checkout_choose_payment_method']				= 'choose payment method';



$label['checkout_card_number']				= 'Card number';



$label['checkout_expiry_date']				= 'Expiry date';



$label['checkout_cvc']				= 'CVV';



$label['invalid_cvv_no']				= 'Invalid verification code';



$label['checkout_pay']				= 'Pay';



$label['checkout_submit']				= 'Submit';



$label['checkout_choose_account']				= 'Choose Account';



$label['checkout_terms_conditions']				= 'I have read and informed and accept the Terms and Conditions';



$label['checkout_continue_shopping']				= 'Continue shopping';
$label['checkout_continue_shopping_btn']				= 'Continue shopping';



$label['checkout_shopping_basket']				= 'Shopping Basket';



$label['checkout_personal_info']				= 'Personal Information';



$label['checkout_personal_info_address']				= 'Select Shipping Address';



$label['checkout_shipping_address']				= 'Shipping Address';


$label['checkout_shipping_address_normal_shipping']				= 'Normal Shipping';


$label['checkout_shipping_address_fast_shipping']				= 'Fast Shipping';


$label['checkout_shipping_address_free_shipping']				= 'Free Shipping';


$label['checkout_shipping_address_extra']				= 'extra';


$label['checkout_shipping_address_taxes']				= 'Taxes';


$label['checkout_shipping_select_this_address']		= 'Select this address';



$label['checkout_shipping_no_address_message']		= 'No Address <br />added, Click below <br />to add a <br />new address <br />';



$label['checkout_estimated_time']				= 'Estimated Delivery Time';



$label['checkout_delivery']				= 'Delivery';



$label['checkout_order_summary']				= 'Order Summary';



$label['checkout_change']				= 'Change';



$label['checkout_sadad_payment']				= 'Sadad Payment';


$label['checkout_sadad_title']				= 'Sadad';



$label['checkout_wire_transfer_title']				= 'Wire Transfer';



$label['checkout_card_payment']				= 'Card Payment';



$label['checkout_cash_on_delivery']				= 'Cash On Delivery';



$label['checkout_from']				= 'From';



$label['checkout_to']				= 'To';



$label['checkout_items']				= 'Items';



$label['checkout_shipping_handling']				= 'Shipping amount';



$label['checkout_total_before_tax']				= 'Total before tax';



$label['checkout_estimated_tax']				= 'Estimated tax to be collected';



$label['checkout_thankyou_message']     = 'Thanks for the request through our website, we will contact you soon';



$label['checkout_item_ship_to'] = 'Items will be shipped';



$label['checkout_personal_info_title_name']				= 'Title name';



$label['checkout_personal_info_company_name']				= 'Company name';



$label['checkout_personal_info_address_1']				= 'Address 1';



$label['checkout_personal_info_address_2']				= 'Address 2';



$label['checkout_personal_info_region']				= 'Region';



$label['checkout_personal_info_contact_number']				= 'Contact number';



$label['checkout_personal_info_add']				= 'Add';



$label['checkout_personal_info_address_1_placeholder'] = 'Address of the building, Street address, District';



$label['checkout_personal_info_address_2_placeholder'] = 'Building, Floor, Apartment';



$label['checkout_personal_info_edit_address'] = 'Edit';



$label['checkout_personal_info_send_shippment'] = 'Send the shipment to this address';











$label['shipping_address1_choose_already_registered']				= 'Choose already registered address or add new address';



$label['shipping_address1_edit_address']				= 'Edit address';



$label['shipping_address1_delete_address']				= 'Delete address';



$label['shipping_address_remove_address']				= 'Remove';



$label['shipping_address_phone']				= 'Phone';



$label['shipping_address1_add_another_address']				= 'Add another address';



$label['shipping_read_agreed_terms']				= 'I have read & agree to the terms and conditions';



$label['shipping_address1_cancel_editing']				= 'Cancel editing';







$label['shipping_address_street_name']				= 'Street name';



$label['shipping_address_property_number']				= 'Property number';



$label['shipping_address_city']				= 'City';



$label['shipping_address_district']				= 'District';



$label['shipping_address_po_box']				= 'P.O box';



$label['shipping_address_zip_code']				= 'Zip Code';



$label['shipping_address_more_details']				= 'More details';







$label['register_personal_information']				= 'Personal information';







$label['register_financial_information']				= 'Financial information';



$label['register_choose_payment_method']		='Choose payment method';



$label['register_year']				= 'Year';



$label['register_month']				= 'Month';



$label['register_send']				= 'Send';



$label['register_add_another_address']				= 'Add another address';



$label['register_addresses']				= 'Address';



$label['register_success']				= 'User registered successfully.';



$label['register_duplication']				= 'Email already exist.';



$label['register_email_subject']				= 'User registered.';



$label['register_email_other_info']				= 'Other Info';



$label['register_submit_button']				= 'Sign Up';



$label['register_choose_from_map']				= 'Choose from Map';







$label['after_login_Welcome']				= 'Welcome';







$label['forgot_password_password_recovery']				= 'Password recovery';



$label['forgot_password_forgot_password']				= 'Forgot your password? No problem to help you to restore it';



$label['forgot_password_restore_email']				= 'You will receive a link to restore to your email please click on it.';



$label['forgot_password_if_not_receive']				= 'If you didn’t receive it, please click here';



$label['forgot_password_no_such_user']				= 'User does not exist.';



$label['forgot_password_if_not']				= 'If you didn’t receive it, please';



$label['forgot_password_click_here']				= 'click here';







$label['password_sent_check_your_email']				= 'Please check your email';







$label['box_login']				= 'Login';



$label['box_email']				= 'Email';



$label['box_password']				= 'Password';



$label['box_forget_password']				= 'Forget password?';



$label['box_log_in']				= 'Log in';



$label['box_new_user']				= 'New user?';

$label['login_new_user_label']				= 'Login/New user';

$label['box_start_here']				= 'Start here';



$label['jobs_job']				= 'Job';



$label['jobs_send_your_resume']				= 'Send your resume';



$label['jobs_upload_your_resume']				= 'Upload your resume';



$label['jobs_send_your_resume_name']				= 'Name';



$label['jobs_upload']				= 'Upload';



$label['jobs_apply_job']				= 'Apply job';



$label['jobs_apply_success_message']				= 'Request Submitted Successfully.';

$label['jobs_apply_success_text']				= 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas suscipit, ex sed vestibulum blandit, eros mi congue ligula, non faucibus dolor odio ut lectus.';



$label['jobs_upload_cv_success_message']				= 'Request Submitted Successfully.';



$label['jobs_thank_you_message']				= 'Thank You for applying on eDesign. We will contact you soon.';







$label['my_orders_my_orders']				= 'My orders';



$label['my_orders_pending_orders']				= 'Pending Orders';



$label['my_orders_completed_orders']				= 'Completed orders';



$label['my_orders_canceled_orders']				= 'Canceled orders';



$label['my_orders_order_id']				= 'Order ID';



$label['my_orders_order_was_in']				= 'The order was in';



$label['my_orders_quantity']				= 'Quantity';



$label['my_orders_bill']				= 'Bill';



$label['my_orders_order_details']				= 'Order details';







$label['settings_settings']				= 'Settings';



$label['settings_my_account']				= 'My account';



$label['settings_personal_information']				= 'My personal information';



$label['settings_update_password']				= 'Update password';



$label['settings_my_request']				= 'Requests';



$label['settings_change_order']				= 'Change order';



$label['settings_return_order']				= 'Return order';



$label['settings_rate_order']				= 'Rate an order';



$label['settings_report_lost_order']				= 'Report a lost order';



$label['settings_financial_information']				= 'My financial information';



$label['settings_add_payment']				= 'Add payment';



$label['settings_mailing_address']				= 'Mailing address';



$label['settings_edit_mailing_address']				= 'Edit mailing address';



$label['settings_add_new_address']				= 'Add new address';



$label['settings_save_changes']				= 'Save changes';



$label['settings_cancel']				= 'Cancel';



$label['settings_success_message']				= 'Profile updated successfully.';



$label['settings_current_password']				= 'Current password';



$label['settings_new_password']				= 'New password';



$label['settings_confirm_password']				= 'New password confirmation';



$label['settings_password_success_message']				= 'Password updated successfully.';



$label['settings_invalid_password']				= 'Invalid password.';







$label['return_change_return_process']				= 'Choose an order to start the return process';



$label['return_change_previous_requests']				= 'Previous requests from two weeks ago';



$label['return_change_better_price']				= 'There is a better price';



$label['return_change_other_comments']				= 'Other comments';



$label['return_change_send']				= 'Send';







$label['return_change2_return_order']				= 'How can we help you in return order?';



$label['return_change2_amount_paid']				= 'Returns the amount paid';



$label['return_change2_via']				= 'Via';







$label['return_change_confirm_receive_order']				= 'How can we receive the order from you?';



$label['return_change_confirm_account_review']				= 'Account Review';



$label['return_change_confirm_total_return']				= 'Total return';



$label['return_change_confirm_shipping_through']				= 'Shipping through';



$label['return_change_confirm_amount_due']				= 'Amount due';



$label['return_change_confirm_aramex']				= 'Aramex';
$label['aramex_shipment']				= 'Aramex Shipping';
$label['aramex_track_no']				= 'Aramex Tracking Number';

$label['state_province']				= "State/Province";
$label['zip_code']				= "Zip Code";







$label['place_feedback_select_rating']				= 'Select order for rating';



$label['place_feedback_evaluate_order']				= 'Evaluate an order';



$label['place_feedback_success']				= 'Your feedback is saved successfully.';







$label['place_feedback2_order_review']				= 'Order review';



$label['place_feedback2_order_number']				= 'Order number';



$label['place_feedback2_order_date']				= 'Order date';



$label['place_feedback2_positive']				= 'Positive';



$label['place_feedback2_negative']				= 'Negative';



$label['place_feedback2_other_comments']				= 'Do you have other comments?';







$label['lost_item_orders_lost']				= 'Select which of your orders was lost';



$label['lost_item_lost']				= 'Lost';



$label['lostitem_success']				= 'The product have been reported successfully';











$label['logout_menu_my_wish_list']				= 'My wish list';



$label['logout_menu_sign_out']				= 'Sign out';







$label['designers_design']				= 'Design';







$label['design_with_us_upload_portfolio']				= 'Upload portfolio';



$label['design_with_us_Website']				= 'Website';



$label['design_with_us_upload_cv'] = 'Upload CV';



$label['design_thank_you_email'] = 'Thank You for applying on eDesign.';



$label['design_email'] = 'You recieved a designer resume.';



$label['design_cv_title'] = 'CV';



$label['designers_designer']				= 'Designer';



$label['design_visit_our_store']				= 'Visit our online store';



$label['design_with_us_portfolio']				= 'Portfolio';







$label['validation_mandatory']				= 'Mandatory field';



$label['validation_mandatory_size']				= 'Please choose size';



$label['validation_invalid_email']				= 'Invalid email address';



$label['validation_password_mismatch']				= 'Password and confirm password does not match';







$label['email_thank_you'] = 'Thank You for registering with eDesign.';



$label['email_registration_subject'] = 'Confirmation of registration on eDesign site';







$label['email_thank_you_career'] = 'Thank you for applying on Rastakka.';



$label['email_career_subject'] = 'New Application Recieved.';



$label['email_career_detail_message'] = 'Following are the details of applicant.';







$label['email_template_footer_need_more_help'] = 'Need More Help?';



$label['email_template_footer_service_team'] = 'Contact our friendly customer service team on';



$label['email_template_or'] = 'or';



$label['email_template_thank_you_for_shopping'] = 'Thanks for shopping with us';



$label['email_template_hi'] = 'Hi';



$label['email_template_hello'] = 'Hello';







$label['request_message'] = 'The request was in';







$label['home_our_mission'] = 'Our mission';



$label['home_or'] = 'OR';



$label['home_our_vision'] = 'Our vision';



$label['home_our_values'] = 'Our Values';



$label['home_banner_text'] = 'You want to be designed';



$label['home_banner_button_text'] = 'With eDesign';







$label['career_working_with'] = 'Working with eDesign';



$label['career_invalid_file'] = 'Invalid file type.';











$label['no_records'] = 'No Records';







$label['newsletter_success_msg'] = 'Thank you for subscribing to the newsletter';



$label['newsletter_already_subscribed_msg'] = 'You are already subscribed.';



$label['newsletter_error_msg'] = 'Some problem occurred, please try again.';







$label['wishlist_title'] = 'Wish List';



$label['wishlist_already_in'] = 'The product is already in your wishlist.';



$label['wishlist_added_to'] = 'Product is added to wishlist.';







$label['not_saved'] = 'Not saved';



$label['already_rated'] = 'You have already rated this product.';



$label['give_us_your_review'] = 'Thank You for giving your review.';
$label['rating_validation_msg'] = 'Please select the star rating and enter the feedback!';



$label['give_us_review']  = 'Review';



$label['give_us_review_email_subject']	= 'Review';



$label['login_to_rate_product']	= 'Please register first to rate this product.';







$label['daily_offer_start_date'] = 'Start Date';



$label['daily_offer_end_date'] = 'End Date';







$label['no_results_found'] = 'No Results Found';







$label['delete_confirm_message'] = 'Are you sure you want to delete this product?';



$label['delete_confirm_general'] = 'Are you sure you want to delete this?';
$label['cancel_confirm_general'] = 'Are you sure you want to cancel this?';











$lang['all'] = $label;