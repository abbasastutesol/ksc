<?php

/*



 * Arabic language



 */


 $label['site_title'] = 'IOUD';


// new labels start here for general
$label['enter_here_palceholder']				= 'أدخل هنا';
$label['file_upload']				= 'تحميل';
$label['select_dropdown']				= 'اختر';
$label['edit_label']				= 'تصحيح';
$label['remove_label']				= 'إزالة';
$label['cancel_label']				= 'إلغاء';
$label['return_label']				= 'المرتجعات';
$label['add_new_address_label']				= 'أضف عنواناً جديداً';
$label['address_label']				= 'العنوان';
$label['phone_label']				= 'الهاتف';
$label['logout_link']				= 'تسجيل الخروج';
$label['view_all']				= 'عرض الكل';
$label['read_more']				= 'قراءة المزيد';
$label['read_less']				= 'قراءة أقل';
$label['mobile_no_label']				= 'رقم الجوال';
$label['submit_btn_label']				= 'قدم';
$label['full_name_label']				= 'الأسم الكامل';
$label['country_label']				= 'البلد';
$label['city_label']				= 'المدينة';
$label['browse_label']				= 'تصفح';
$label['email_address_label']				= 'البريد الإلكتروني';

$label['name_label']				= 'الإسم';
$label['email_label']				= 'البريد الإلكتروني';
$label['mobile_label']				= 'رقم الجوال';
$label['dismiss_label']				= 'إغلاق‎';
$label['signup_label']				= 'سجل';

$label['products_count_label']				= 'المنتجات';
$label['grand_total_label']				= 'المبلغ الإجمالي';
$label['dob_label']				= 'تاريخ الميلاد';
$label['cancel_order_success_msg']				= 'تم إلغاء طلبك بنجاح.';
$label['shipped_order_success_msg']				= 'تم شحن طلبك.';
$label['delevered_order_success_msg']				= 'تم تسليم طلبك.';
$label['cancel_order_failed_msg']				= 'لم يتم إلغاء طلبك، يرجى إعادة المحاولة!';

$label['order_not_find']				= "Sorry! we couldn't find the order, please enter the correct order no.";


$label['return_order_success_msg']				= '"تم إرسال طلب إعادة الطلب."';
$label['return_order_failed_msg']				= 'لم يتم إرسال طلب إعادة الطلب، يرجى إعادة المحاولة!';


$label['form_submitted_successfully']				= 'تم تقديم إستمارتك بنجاح';
$label['form_submitted_failed']				= 'بعض الأخطاء حدثت عند إرسال النموذج الخاص بك, يرجى إعادة المحاولة.';


$label['contact_request_received']				= 'تم إستلام طلب الإتصال بنا';
$label['contact_request_submitted']				= 'تم إرسال طلب الإتصال بنجاح';
$label['contact_request']				= 'طلب الإتصال ';
$label['hi_label']				= 'أهلا';

$label['job_application']				= 'طلب وظيفة جديدة';
$label['job_application_received']				= 'تم إستقبال طلب وظيفة جديد';
$label['job_application_user']				= 'تم استلام طلب الوظيفة الخاص بك';

$label['ok_label']				= 'حسنا';

$label['order_confirm_sms']				= 'شكراً لتنفيذ طلبك رقم الطلب';

$label['forgot_password_email_not_found']				= 'عذرا، لم نتمكن من العثور على أي حساب باستخدام هذا البريد الإلكتروني';


$label['password_request_processed']				= 'لقد تم تجهيز طلب إعادة تعيين كلمة المرور الخاصة بك.';
$label['password_request_please_click']				= 'الرجاء الضغط';
$label['password_request_click_here']				= 'هنا';
$label['password_request_thanks']				= 'لإعادة تعيين كلمة المرور';

$label['send_new_password_msg']				= 'لقد أرسلنا لك رسالة إلكترونية تحتوي على كلمة مرور جديدة لإعادة تعيين كلمة المرور الخاصة بك يرجى التحقق من البريد الوارد';
$label['send_new_password_failed']				= 'هناك بعض المشاكل لإرسال البريد الإلكتروني، يرجى إعادة المحاولة مرة أخرى!';
$label['password_reset_successfully']				= 'تم إعادة تعيين كلمة المرور بنجاح';
$label['url_expire']				= 'عذرا ً , لقد أنتهت صلاحية هذا الرابط';
$label['type_here']				= 'أكتب هنا';


$label['return_reason']				= 'سبب الإرجاع';
$label['product_receive_mistake']				= 'إستلام المنتج عن طريق الخطأ';
$label['product_receive_damage']				= 'إستلام المنتج تالفا ً ';
$label['wrong_product']				= 'المنتج غير صحيح';
$label['received_product_bug']				= 'يوجد خطأ، يرجى التوضيح';
$label['received_product_other_reason']				= 'أخرى, يرجى التوضيح';
$label['package_closed']				= 'تم إغلاق الشحنة';
$label['please_explain']				= 'يرجى التوضيح';
$label['type_your_message']				= 'اكتب رسالتك';
$label['yes_label']				= 'نعم';
$label['no_label']				= 'لا';

$label['your_order_label']				= 'طلبك # ';
$label['order_changed_label']				= 'تم تغيير إلى ';
$label['order_status_label']				= 'الحالة';

$label['want_to_cancel_order']				= 'أريد إلغاء طلبي # ';
$label['want_to_return_order']				= 'أريد إرجاع طلبي #  ';

$label['return_request_order']				= 'طلب إعادة الطلب:';
$label['return_request_reason_label']				= 'وفقا ً  للسبب التالي:';
$label['cancel_request_order']				= 'طلب إلغاء الطلب:';

$label['order_feedback_received']				= 'تم استقبال نموذج تقييم من ';
$label['order_feedback_against_order']				= 'عن الطلب.';
$label['order_feedback_subject']				= 'تقييم الطلب';

$label['order_processed_title']				= 'جاري تجهيز الطلب';
$label['do_it_later']				= 'لاحقا ً';
$label['forgot_password_email']				= 'إعادة تعيين كلمة المرور';

$label['shopping_car_old_price']				= 'السعر قبل التخفيض';
$label['shopping_car_discount_price']				= 'قيمة التخفيض';

$label['loyalty_discount_label']				= 'خصم عملاء مضياف';


// end labels start here for general

// new labels start here for index.php page
$label['products_label']				= 'المنتجات';
$label['offers_label']				= 'العروض';
$label['view_all']				= 'عرض الكل';
$label['home_page_label']				= 'الصفحة الرئيسية';

$label['all_fields_required']				= 'جميع الحقول مطلوبة';

$label['go_back_to_previous']				= 'الرجوع';



// end new labels for index.php page


// new labels start here for product detail page
$label['price']				= 'السعر';
$label['related_products_label']				= 'منتجات ذات صلة';
$label['view_more_label']				= 'عرض المزيد';
$label['view_less_label']				= 'عرض أقل';
$label['add_to_my_cart_label']				= 'أضف إلى سلتي';
$label['shopping_out_of_stock']				= 'نفذ من المخزن';
$label['related_product_not_available_label']				= 'لا منتجات ذات صلة متاحة';

$label['product_out_of_stock']				= 'هذا المنتج نفذ من المتجر, يرجى إختيار منتج آخر';

// end new labels for product detail page


// new labels start here for product page
$label['all_products_label']				= 'جميع المنتجات';
$label['no_record_founds_label']				= 'لم يتم العثور على سجل';

// end new labels for product page

// new labels start here for login popup
$label['login_register_label']				= 'تسجيل الدخول / التسجيل';
$label['login_btn_label']				= 'تسجيل الدخول';
$label['dont_have_account_label']				= "ليس لديك حساب؟";
$label['register_now_label']				= "سجل الآن";
$label['forgot_password_label']				= "هل نسيت كلمة المرور؟";
$label['username_password_invalid_label']				= "!اسم المستخدم وكلمة المرور غير صالحة";
$label['login_username_label']				= 'البريد الإلكتروني';
$label['login_password_label']				= 'كلمة السر';
// end new labels for login popup


// new labels start here for register page
$label['register_new_account_label']				= 'تسجيل حساب جديد';
$label['update_account_label']				= 'تحديث الحساب';
$label['register_first_name_label']				= 'الأسم الأول';
$label['register_last_name_label']				= 'الأسم الثاني';
$label['register_email_label']				= 'البريد الإلكتروني';
$label['register_mobile_number_label']				= 'رقم الجوال';
$label['register_city_label']				= 'المدينة';
$label['register_country_label']				= 'البلد';
$label['register_password_label']				= 'كلمة السر';
$label['register_confirm_password_label']				= 'إعادة كلمة السر';
$label['register_dob_label']				= 'تاريخ الميلاد';

$label['register_i_accept_label']				= 'أوافق على';
$label['register_terms_and_conditions_label']				= 'الشروط و الأحكام';
$label['register_btn_label']				= 'تسجيل';
$label['update_btn_label']				= 'معدل';

// end new labels for register page


// new labels start here for profile page
$label['profile_my_account_label']				= 'حسابي';
$label['profile_biography_label']				= 'الملف الشخصي';
$label['profile_change_password_label']				= 'تغيير كلمة المرور';
$label['profile_dob_label']				= 'تاريخ الميلاد';
$label['profile_password_label']				= 'كلمة السر';
$label['profile_email_label']				= 'البريد الإلكتروني';
$label['profile_moblie_label']				= 'رقم الجوال';
$label['profile_change_data_label']				= 'تغيير البيانات';
$label['my_profile_label']				= 'حسابي';

$label['my_profile_sliver_no_label']				= 'الرقم الفضي';


$label['my_profile_loyalty_silver']				= 'فضي';
$label['my_profile_loyalty_gold']				= 'ذهبي';
$label['my_profile_loyalty_platinum']				= 'بلاتينيوم';

// end new labels for profile page

// new labels start here for promotion page
$label['all_promotions_label']				= 'جميع العروض';

// end new labels for promotion page


// new labels start here for customer service and inner page
$label['customer_service_label']				= 'خدمة العملاء';
$label['payment_and_delivery_label']				= 'الدفع والتسليم';
$label['latest_news_label']				= 'الأخبار';
$label['payment_confirmation_label']				= 'تأكيد الدفع';
$label['returns_label']				= 'المرتجعات';

$label['custom_returns_label']				= 'الإستبدال والإسترجاع';

$label['distributor_label']				= 'كن موزعاً';
$label['privacy_policy_label']				= 'سياسة الخصوصية';

// end new labels for customer service and inner page



// new labels start here for payment confirmation page
$label['payment_order_no_label']				= 'رقم الطلب';
$label['payment_customer_name_label']				= 'اسم العميل';
$label['payment_bank_name_label']				= 'اسم البنك';
$label['payment_bank_account_no_label']				= 'رقم الحساب';
$label['payment_bank_iban_no_label']				= 'رقم الايبان';
$label['payment_bank_account_name_label']				= 'اسم صاحب حساب البنك';
$label['payment_date_and_time_label']				= 'تاريخ و وقت التحويل';
$label['payment_amount_transfer_label']				= 'المبلغ المحول';

// end new labels for payment confirmatione page


// new labels start here for distributor page
$label['distributor_store_name_label']				= 'اسم المتجر';
$label['distributor_street_address_label']				= 'عنوان الشارع';
$label['distributor_district_label']				= 'المنطقة';
$label['distributor_store_number_label']				= 'رقم المتجر';
$label['distributor_pin_location_label']				= 'حدد الموقع';
$label['distributor_store_images_label']				= 'صور المتجر';

$label['location_page_label']				= 'مواقعنا';

// end new labels for payment distributor page


// new labels start here for contact us page
$label['contact_us_label']				= 'اتصل بنا';
$label['contact_us_details_label']				= 'تفاصيل الإتصال';
$label['contact_us_headoffice_label']				= 'المكتب الرئيسي';

$label['contact_us_feedback_label']				= 'أستفسار';
$label['contact_us_phone_label']				= 'الهاتف';
$label['contact_us_follow_us_label']				= 'تابعنا';
$label['contact_us_toll_free_label']				= 'رقم الهاتف المجاني';

$label['feedback_detail_label']				= 'التفاصيل';

// end new labels for contact us page



// new labels start here for FAQS page
$label['faqs_label']				= 'الأسئلة الشائعة';
$label['faqs_question_label']				= 'س.';
$label['faqs_answer_label']				= 'ج.';

// end new labels for FAQS page



// new labels start here for Career and career open page
$label['careers_label']				= 'الوظائف';
$label['careers_job_open_label']				= 'فرص العمل';
$label['careers_apply_now_label']				= 'قدم الآن';

$label['careers_age_label']				= 'العمر';
$label['careers_cv_label']				= 'السيرة الذاتية';
$label['careers_experience_label']				= 'سنوات الخبرة';

// end new labels for Career and career open page

// new labels start here for Newsletter
$label['newsletter_label']				= 'الاشتراك في النشرة الشهرية';
$label['newsletter_enter_email_label']				= 'أدخل البريد الإلكتروني';

// end new labels for Newsletter



// new labels start here for Menu
$label['menu_my_account_label']				= ' حسابي';
$label['menu_login_label']				= 'تسجيل الدخول';
$label['menu_logout_label']				= 'تسجيل الخروج';
$label['menu_about_us_label']				= 'من نحن ';

$label['menu_customer_services_label']				= 'خدمة العملاء';
$label['menu_mission_vission_label']				= 'الرسالة والرؤية';
$label['menu_why_ioud_label']				= 'لماذا البخور الذكي';

$label['menu_distinctiveness_label']				= 'تميزنا';
$label['menu_special_offers_label']				= 'عروض خاصة';
$label['menu_all_products_label']				= 'جميع المنتجات';
$label['menu_contact_us_label']				= 'اتصل بنا';
$label['menu_faqs_label']				= 'الأسئلة الشائعة';
$label['menu_careers_label']				= 'الوظائف';
$label['menu_newsletter_label']				= 'النشرة الإخبارية';
$label['menu_trusted_partners_label']				= 'الشركاء الموثوقين';


// end new labels for  Menu


// my order page

$label['my_order_current_order_label']				= 'الطلبات الحالية';
$label['my_order_completed_label']				= 'طلبات سابقة';

$label['my_order_order_no_label']				= 'رقم الطلب';
$label['my_order_received_label']				= 'تم استلام الطلب';
$label['my_order_preparing_ship_label']				= 'إعداد الشحنة';



$label['order_preparing_title']				= 'جاري تجهيز الطلب ';
$label['order_preparing_ship_title']				= 'جاري تجهيز الشحن';
$label['order_shipped_title']				= 'تم الشحن';
$label['order_delivered_title']				= 'تم التوصيل';
$label['order_returned_title']				= 'تم إرجاع';
$label['order_rejected_title']				= 'تم رفض الطلب';




$label['my_order_preparing_order_message']				= 'جاري تجهيز';
$label['my_order_preparing_ship_message']				= 'جاري تجهيز شحن';
$label['my_order_shipped_message']				= 'تم شحن';
$label['my_order_delivered_message']				= 'تم توصيل';
$label['my_order_rejected_message']				= 'تم رفض';
$label['my_order_returned_message']				= 'تم إرجاع';



$label['my_order_shipped_label']				= 'تم الشحن';
$label['my_order_delivered_label']				= 'تم التوصيل';
$label['my_order_cancelled_label']				= 'تم الالغاء';
$label['my_order_verify_payment_label']				= 'التحقق من الدفع';
$label['my_order_returned_label']				= 'عاد';
$label['my_order_rejected_label']				= 'مرفوض';
$label['my_order_preparing_order_label']				= 'إعداد النظام';
$label['my_order_reviews_label']				= 'آراء و ملاحظات';

$label['my_order_product_quality_label']				= 'جودة المنتج';
$label['my_order_packaging_label']				= 'تعبئة و تغليف الشحنة';
$label['my_order_packing_label']				= 'تغليف المنتج';
$label['my_order_ioud_customer_service_label']				= 'خدمة عملاء البخور الذكي';
$label['my_order_shipment_duration_label']				= 'مدة الشحن';

$label['my_order_satisfaction_label']				= 'الرضا العام';
$label['my_order_feedback_placeholder']				= 'أستفسار';

$label['my_order_placed_successfully']				= 'تم تنفيذ الطلب بنجاح';
$label['my_order_print']				= 'طباعة';
$label['my_order_your_order_recieved']				= 'تم استلام الطلب';
$label['my_order_we_will_deliver']				= 'سيتم تسليمها في ';
$label['my_order_bussiness_days']				= 'أيام عمل ';
$label['my_order_approximately_to']				= 'إلى';
$label['my_order_cod_label']				= 'رسوم الدفع عن الإستلام';


// end my order page


// registration message

$label['register_successfully_msg'] 		=	"تم التسجيل بنجاح!";

$label['register_successfully_to_user'] 		=	"تم إنشاء حسابك بنجاح";

$label['register_user_subject'] 		=			"تسجيل المستخدم";
$label['register_user_from'] 		=			"من عند";
$label['user_request_to_admin']		= 		"تم استلام طلب تسجيل المستخدم فيما يلى تفاصيل المستخدم";
// end message



// all right reserved

$label['all_right_reserved_label']				= 'جميع الحقوق محفوظة لشركة البخور الذكي';

// all right reserved end


//cart pages labels

$label['address_tittle_label'] = 'العنوان';
$label['street_address_1_label'] = 'عنوان الشارع 1 ';

$label['apartment_building_label'] = 'الشقة / رقم المبنى';
$label['street_address_2_label'] = 'عنوان الشارع 2';

$label['area_district_label'] = 'المنطقة / الحي / الشارع';
$label['save_label'] = 'حفظ';

$label['atten_login_label'] = 'إنتباه ! تسجيل الدخول';
$label['username_label'] = 'اسم المستخدم';

$label['reset_password_label'] = 'إعادة تعيين كلمة المرور';
$label['alert_label'] = 'تنبيه';
$label['previous_password'] = 'كلمة المرور السابقة';

$label['previous_password_label'] = 'كلمة المرور السابقة غير صالحة';
$label['password_change_label'] = 'تم تغيير كلمة المرور بنجاح';
$label['new_password_label'] = 'كلمة المرور الجديدة';

$label['confirm_password_label'] = 'تأكيد كلمة المرور';
$label['change_password_label'] = 'تغيير كلمة المرور';

$label['registeration_success_label'] = 'تم التسجيل بنجاح';
$label['pofile_update_label'] = 'تم تحديث الملف الشخصي بنجاح';

$label['email_already_label'] = 'هذا البريد الإلكتروني مسجل بالفعل.';
$label['password_change_label'] = 'تم تغيير كلمة المرور بنجاح';

$label['old_password_label'] = 'كلمة المرور القديمة غير صحيحة';
$label['cannot_process_accept_label'] = 'لا يمكنك الإنتقال دون الموافقة';
$label['terms_condition_label'] = 'الشروط و الأحكام';

$label['cannot_process_upload_label'] = 'لا يمكنك الإنتقال دون تحميل الملفات';


$label['do_you_want_to_register_label'] = 'هل تريد إنشاء حساب؟';

$label['accept_term_and_conditions_msg'] = 'لا يمكنك الإنتقال دون الموافقة على الشروط و الأحكام';


$label['order_email_sent_successfully'] = 'تم إرسال البريد بنجاح';












$label['login_sign_in']				= 'تسجيل دخول';



$label['login_welcome']				= 'أهلا ً بك';



$label['login_all_categories']		= 'جميع الفئات';



$label['login_search_word']		= 'بحث بكلمة';



$label['login_exhibits']				= 'المعروضات';



$label['login_best_seller']			= 'الأكثر مبيعا';



$label['login_quick_links']			= 'روابط سريعة';



$label['login_newsletters']			= 'النشرة الإخبارية';



$label['login_send']					= 'رسال';



$label['login_payment_methods']		= 'طرق السداد';



$label['login_follow_us']			= 'تابعنا';



$label['login_abaya']				= 'عبايات';



$label['login_blouses']				= 'بلوزة';



$label['login_daily_offers']			= 'عروض يومية';



$label['login_communicate_with_customer']				= 'تواصل مع فريق خدمة العملاء';



$label['login_get_to_know_us']				= 'تعرف علينا';



$label['login_about_com']				= 'عن eDesign';



$label['login_investor_relations']				= 'علاقات المستثمرين';



$label['login_careers']				= 'الوظائف';



$label['login_cooperate_with_us']				= 'تعاون معنا';



$label['login_design_with_com']				= 'صمم مع eDesign';



$label['login_designers']				= 'المصممين';



$label['login_terms_conditions']				= 'الشروط و الأحكام';



$label['login_quality_offers']				= 'الجودة و العروض';



$label['login_cloth_care']				= 'العناية بالقماش';



$label['login_give_us_review']				= 'رأيك يهمنا';



$label['login_subscribe_to_newsletters']				= 'أشترك في النشرة الإخبارية';



$label['login_returns_policy']				= 'سياسة الإسترجاع';



$label['login_delivery']				= 'توصيل الطلبات';



$label['login_all_rights_reserved']				= 'جميع الحقوق محفوظة ';



$label['login_follow_us']				= 'تابعنا';



$label['login_fail']				= 'اسم المستخدم او الرقم السري غير صحيح.';



$label['login_success']				= 'تسجيل الدخول بنجاح';







$label['aboutus_who_we_are']				= 'من نحن';







$label['chat_connect_with_us']				= 'تواصل معنا';



$label['chat_name']				= 'الإسم';



$label['chat_email']				= 'البريد الإلكتروني';



$label['chat_message']				= 'الرسالة';



$label['chat_start_chat']				= 'ابدأ المحادثة';



$label['com_admin']				= 'مشرف';







$label['home_display_blouse']				= 'بلوزة';



$label['home_display_product_name']				= 'إسم المنتج';



$label['home_display_vote_this_product']				= 'قيم هذا المنتج';



$label['home_display_sar']				= 'ريال سعودى';



$label['home_display_size']				= 'المقاس';



$label['home_display_select']				= 'إختر';



$label['home_display_sizes_chart']				= 'جدول القياسات';



$label['home_display_add_to_shopping_cart']				= 'أضف إلى حقيبة التسوق';



$label['home_display_more_details']				= 'عرض التفاصيل';



$label['home_display_add_to_wish_list']				= 'أضف للقائمة';









$label['products_new_arrival_label']				= 'وصل حديثا';



$label['products_best_seller_label']				= 'الأكثر مبيعا';



$label['products_recently_viewed_products']				= 'تم التصفح من قبلكم';



$label['products_price']				= 'السعر';



$label['products_sar']				= 'ريال';



$label['products_color']				= 'اللون';



$label['products_size']				= 'المقاس';



$label['products_clients_reviews']				= 'تقييم العملاء';



$label['product_chart_saudi_inter']				= 'دولى سعودى';







$label['products_filtered_new_to_old']				= 'من الأحدث إلى الأقدم';





$label['products_page_similar_products_count']				= 'منتجات';



$label['products_page_singular_products_count']				= 'منتج';



$label['products_page_similar_products']				= 'منتجات مشابهة';



$label['products_page_from_same_category']				= 'منتجات من نفس الفئة';



$label['products_page_your_voting']				= 'كم تقييمك ؟';



$label['products_page_product_information']				= 'تفاصيل و معلومات العناية بالمنتج';



$label['products_page_product_details']				= 'تفاصيل المنتج';



$label['products_page_add_to_cart']				= 'ااضف للحقيبة';



$label['products_page_view_details']				= 'عرض التفاصيل';



$label['products_page_material_use']				= 'المواد المستخدمة';



$label['products_page_size_display']				= 'المقاس المعروض في الصورة';



$label['products_page_model_number']				= 'رقم الموديل من المورد';



$label['products_page_project_measurement']				= 'قياسات العارض/العارضة';



$label['products_page_length_bar']				= 'طول العارض/العارضة';



$label['products_page_washing_instruction']				= 'ارشادات الغسيل';



$label['products_page_collar_type']				= 'نوع الياقة';



$label['products_page_brand_clothing']				= 'نوع الملابس';



$label['products_page_customer_questions']				= 'أسئلة العملاء';



$label['products_page_question']				= 'سؤال';



$label['products_page_answer']				= 'جواب';



$label['products_page_write_here']				= 'اكتب هنا';



$label['products_page_customer_questions_success']				= 'شكرا. سنقوم بالرد على سؤالك قريبا';



$label['products_page_add_to_wish_list']				= 'إضافة الى القائمة المفضلة';



$label['products_page_customer_bought_same']				= 'العملاء الذين اشتروا المنتجات في حقيبة تسوقك، اشتروا أيضا';



$label['products_page_continue_shopping']				= 'استمر بالتسوق';



$label['products_page_complete_shopping']				= 'ااكمال الشراء';



$label['products_page_load_more']				= 'تحميل المزيد';



$label['product_page_loading'] 					=  'جار التحميل';



$label['products_largest_selection'] = 'أكبر تشكيلة';



$label['products_first_time_in_kingdom'] = 'الموقع الأول فى المملكة';











$label['size_chart_size_chart']				= 'جدول المقاسات';



$label['size_chart_international']				= 'دولى';



$label['size_chart_saudi']				= 'سعودى';







$label['shopping_cart_shopping_cart']				= 'سلة المشتريات';



$label['shopping_cart_price']				= 'السعر';



$label['shopping_cart_quantity']				= 'الكمية';



$label['shopping_cart_qty']				= 'الكمية';



$label['shopping_cart_continue_shopping']				= 'إكمال عملية الشراء';



$label['shopping_cart_total']				= 'الإجمالى';



$label['shopping_cart_delete_product']				= 'حذف';



$label['shopping_cart_save_for_later']				= 'احفظ لوقت لاحق';



$label['shopping_cart_move_to_cart']				= 'إضافة في حقيبة التسوق';



$label['shopping_cart_proceed_to_cart']				= 'انتقل إلى السلة';



$label['shopping_cart_actions']				= 'خيارات‎';



$label['shopping_cart_last_piece']				= 'أخر قطعة';


$label['shopping_cart_voucher_code_success']				= 'تم قبول رقم القسيمة بنجاح';


$label['shopping_cart_voucher_code_error']				= 'رقم القسيمة غير صالح أو منتهى الصلاحية';






$label['shopping_cart_voucher_code']				= 'رقم القسيمة';


$label['shopping_cart_discount_coupon']				= 'قسيمة التخفيض';


$label['shopping_cart_apply']				= 'تقديم';


$label['shopping_cart_total_cost']				= 'التكلفة الإجمالية';


$label['shopping_cart_enter_here']				= 'أدخل هناe';


$label['shopping_cart_coupon_enter_here']				= 'ادخل قسيمة التخفيض';


$label['shopping_cart_proceed_to_checkout']				= 'الإنتقال للدفع';
$label['shopping_cart_proceed_checkout']				= 'تأكيد الطلب';

$label['shopping_cart_proceed_to_checkout_btn']				= 'تابع عملية الشراء';


$label['shopping_cart_added_to_cart']				= 'تمت الإضافة إلى السلة';



$label['shopping_product_added_to_cart']	= 'لقد تم إضافة المنتج الى سلة التسوق.';



$label['shopping_product_does_not_exist']	= 'المنتج مع اختيار غير موجود.';



$label['shopping_product_not_added_to_cart']	= 'تم إضافة هذا المنتج إلى سلة التسوق';



$label['shopping_product_no_product']	= 'لا وجود المنتج.';



$label['shopping_product_thankyou']	= 'شكرا للتسوق معنا';



$label['shopping_product_order_no']	= 'هنا هو رقم الطلب';



$label['shopping_add_payment'] = 'تستمر عملية الشحن';



$label['shopping_product_copy_order_no']	= 'يرجي حفظ رقم طلبك ثم انقر الزر إنهاء';



$label['shopping_product_finish']	= 'إنهاء';



$label['shopping_product_already_added_cards']	= 'يرجى اختيار البطاقة الائتمانية المسجلة مسبقاً أو يرجى إضافة واحدة أخري.';



$label['shopping_product_already_added_to_cart']	= 'هذا المنتج مضاف بالفعل في سلة مشترياتكم.';



$label['shopping_cart_saved_products']	= 'حفظ المنتجات';



$label['shopping_cart_no_saved_products']	= 'لا حفظ المنتجات';







$label['checkout_choose_mailing_address']				= 'أختيار عنوان المراسلات';



$label['checkout_choose_payment_method']				= 'أختارى طريقة الدفع';



$label['checkout_card_number']				= 'رقم البطاقة';



$label['checkout_expiry_date']				= 'تاريخ الإنتهاء';



$label['checkout_cvc']				= 'رمز التحقق';



$label['invalid_cvv_no']				= 'رمز التحقق غير صالح';



$label['checkout_pay']				= 'دفع';



$label['checkout_submit']				= 'قدم';



$label['checkout_choose_account']				= 'اختيار الحساب';



$label['checkout_terms_conditions']				= 'لقد قرأت و أطلعت و أوافق على الشروط و الأحكام';



$label['checkout_continue_shopping']				= 'اكمال عملية الشراء';
$label['checkout_continue_shopping_btn']				= 'إكمال عملية التسوق';



$label['checkout_shopping_basket']				= 'سلة التسوق';



$label['checkout_personal_info']				= 'المعلومات الشخصية';



$label['checkout_personal_info_address']				= 'اختيار عنوان الشحن';



$label['checkout_shipping_address']				= 'عنوان الشحن';


$label['checkout_shipping_address_normal_shipping']				= 'الشحن العادي';


$label['checkout_shipping_address_fast_shipping']				= 'الشحن السريع';


$label['checkout_shipping_address_free_shipping']				= 'الشحن المجاني';


$label['checkout_shipping_address_extra']				= 'إضافية';



$label['checkout_shipping_address_taxes']				= 'الضرائب';


$label['checkout_shipping_address']				= 'عنوان الشحن';


$label['checkout_shipping_select_this_address']				= 'إختر هذا العنوان';


$label['checkout_shipping_no_address_message']		= 'لم يتم إضافة عنوان <br />، انقر أدناه <br /> لإضافة <br /> عنوان جديد <br />';



$label['checkout_estimated_time']				= 'الوقت المتوقع للتسليم';



$label['checkout_delivery']				= 'الطلبات';



$label['checkout_order_summary']				= 'ملخص الطلب';



$label['checkout_change']				= 'يتغيرون';



$label['checkout_sadad_payment']				= 'سداد الدفع';


$label['checkout_sadad_title']				= 'سداد';


$label['checkout_wire_transfer_title']				= 'تحويل بنكي';


$label['checkout_card_payment']				= 'بطاقه ائتمان';



$label['checkout_cash_on_delivery']				= 'الدفع عن الاستلام';



$label['checkout_from']				= 'من عند';



$label['checkout_to']				= 'إلى';



$label['checkout_items']				= 'المنتجات';



$label['checkout_shipping_handling']				= 'سعر الشحن';



$label['checkout_total_before_tax']				= 'المجموع قبل الضريبة';



$label['checkout_estimated_tax']				= 'الضريبة المتوقعة';



$label['checkout_thankyou_message']     = 'شكرا للطلب من خلال موقعنا، سنتواصل معكم قريبا';



$label['checkout_item_ship_to'] = 'وسيتم شحن البنود';



$label['checkout_personal_info_title_name']				= 'اسم العنوان';



$label['checkout_personal_info_company_name']				= 'أسم الشركة العنوان';



$label['checkout_personal_info_address_1']				= 'العنوان 1';



$label['checkout_personal_info_address_2']				= 'العنوان 2';



$label['checkout_personal_info_region']				= 'المنطقة';



$label['checkout_personal_info_contact_number']				= 'رقم التواصل';



$label['checkout_personal_info_add']				= 'اضف';



$label['checkout_personal_info_address_1_placeholder'] = 'عنوان المبنى، عنوان الشارع، حي';



$label['checkout_personal_info_address_2_placeholder'] = 'عدد المبنى، الطابق، شقة';



$label['checkout_personal_info_edit_address'] = 'تصحيح';



$label['checkout_personal_info_send_shippment'] = 'أرسل الشحنة لهذا العنوان';











$label['shipping_address1_choose_already_registered']				= 'اختيار العنوان المسجل بالفعل او اضافة عنوان جديد';



$label['shipping_address1_edit_address']				= 'تعديل العنوان';



$label['shipping_address1_delete_address']				= 'حذف عنوان';


$label['shipping_address_remove_address']				= 'إزالة';



$label['shipping_address_phone']				= 'الهاتف';



$label['shipping_address1_add_another_address']				= 'أضف عنوان آخر';



$label['shipping_read_agreed_terms']				= 'لقد قرأت واطلعت وأوافق على الشروط والأحكام';



$label['shipping_address1_cancel_editing']				= 'إلغاء التحرير';







$label['shipping_address_street_name']				= 'اسم الشارع';



$label['shipping_address_property_number']				= 'رقم المبنى';



$label['shipping_address_city']				= 'المدينة';



$label['shipping_address_district']				= 'الحي';



$label['shipping_address_po_box']				= 'صندوق البريد';



$label['shipping_address_zip_code']				= 'الرمز البريدي';



$label['shipping_address_more_details']				= 'تفاصيل أخرى';







$label['register_personal_information']				= 'تسجيل البيانات';



$label['register_financial_information']				= 'بيانات مالية';



$label['register_choose_payment_method']		='اختارى طريقة الدفع';



$label['register_year']				= 'السنة';



$label['register_month']				= 'الشهر';



$label['register_send']				= 'ارسال';



$label['register_add_another_address']				= 'أضف عنوان آخر';



$label['register_addresses']				= 'العنوان';



$label['register_success']				= 'آخر عضو مسجل بنجاح.';



$label['register_duplication']				= 'البريد الالكتروني موجود بالفعل.';



$label['register_email_subject']				= 'آخر عضو مسجل.';



$label['register_email_other_info']				= 'معلومات اخرى';



$label['register_submit_button']				= 'تسجيل';



$label['register_choose_from_map']				= 'اختار من الخريطة';







$label['after_login_Welcome']				= 'أهلا بك';







$label['forgot_password_password_recovery']				= 'استعادة كلمة السر';



$label['forgot_password_forgot_password']				= 'نسيتي كلمة السر ؟ ليست مشكلة نساعدك على استعادتها';



$label['forgot_password_restore_email']				= 'سيصلك رابط للاستعادة على بريدك الإلكتروني قومي بالضغط عليه .';



$label['forgot_password_if_not_receive']				= 'إن لم يصلك رجاء إضغطى هنا';



$label['forgot_password_no_such_user']				= 'اسم المستخدم الذي أدخلته غير موجود';



$label['forgot_password_if_not']				= 'إن لم يصلك. يرجي';



$label['forgot_password_click_here']				= 'الضغط هنا';







$label['password_sent_check_your_email']				= 'تفقد بريدك الإلكترونى من فضلك';







$label['box_login']				= 'تسجيل الدخول';



$label['box_email']				= 'البريد الإلكتروني';



$label['box_password']				= 'كلمة السر';



$label['box_forget_password']				= 'نسيتي كلمة السر ؟';



$label['box_log_in']				= 'دخول';



$label['box_new_user']				= 'مستخدم جديد';

$label['login_new_user_label']				= 'مستخدمة جديدة/دخول';

$label['box_start_here']				= 'ابدئي هنا';




$label['jobs_job']				= 'للوظيفة';



$label['jobs_send_your_resume']				= 'ارسلي سيرتك الذاتية';



$label['jobs_upload_your_resume']				= 'تحميل السيرة الذاتية';



$label['jobs_send_your_resume_name']				= 'الأسم';



$label['jobs_upload']				= 'تحميل';



$label['jobs_apply_job']				= 'تقدم للوظيفة';



$label['jobs_apply_success_message']				= 'تم التقديم على هذه الوظيفة بنجاح.';

$label['jobs_apply_success_text']				= 'لوريم إيبسوم دولور سيت أميت، كونسكتيتور أديبيسشينغ إليت. ميسيناس سوسسيبيت، إكس سيد فيستيبولوم بلانديت، إيروس مي كونغ ليغولا، نون فوسيبوس دولور أوديو أوت ليكتوس';

$label['jobs_upload_cv_success_message']				= 'تم إرسال السيرة الذاتية بنجاح.';



$label['jobs_thank_you_message']				= 'شكراً للتقديم في eDesign, و سوف نتصل بك قريبا';







$label['my_orders_my_orders']				= 'طلباتي';



$label['my_orders_pending_orders']				= 'في انتظار';



$label['my_orders_completed_orders']				= 'أوامر الانتهاء';



$label['my_orders_canceled_orders']				= 'إلغاء أوامر';



$label['my_orders_order_id']				= 'طلب رقم';



$label['my_orders_order_was_in']				= 'تم الطلب في';



$label['my_orders_quantity']				= 'الكمية';



$label['my_orders_bill']				= 'الفاتورة';



$label['my_orders_order_details']				= 'تفاصيل الطلب';







$label['settings_settings']				= 'إعدادات';



$label['settings_my_account']				= 'حسابي الشخصي';



$label['settings_personal_information']				= 'بياناتي الشخصية';



$label['settings_update_password']				= 'تعديل كلمة السر';



$label['settings_my_request']				= 'طلباتى';



$label['settings_change_order']				= 'استبدال طلب';



$label['settings_return_order']				= 'ارجاع طلب';



$label['settings_rate_order']				= 'تقييم طلب';



$label['settings_report_lost_order']				= 'ابلاغ عن طلب مفقود';



$label['settings_financial_information']				= 'بياناتي المالية';



$label['settings_add_payment']				= 'اضافة طرق الدفع';



$label['settings_mailing_address']				= 'عنوان المراسلات';



$label['settings_edit_mailing_address']				= 'تعديل عنوان المراسلات';



$label['settings_add_new_address']				= 'اضافة عنوان جديد';



$label['settings_save_changes']				= 'حفظ التغييرات';



$label['settings_cancel']				= 'إلغاء';



$label['settings_success_message']				= 'تم تحديث الملف الشخصي بنجاح.';



$label['settings_current_password']				= 'كلمة السر الحالية';



$label['settings_new_password']				= 'كلمة السر الجديدة';



$label['settings_confirm_password']				= 'تأكيد كلمة السر الجديدة';



$label['settings_password_success_message']				= 'تم تحديث كلمة السر بنجاح';



$label['settings_invalid_password']				= 'كلمة المرور غير صالحة.';











$label['return_change_return_process']				= 'اختارى طلب لبدء عملية الإرجاع';



$label['return_change_previous_requests']				= 'طلبات سابقة منذ اسبوعين';



$label['return_change_better_price']				= 'يوجد سعر أفضل';



$label['return_change_other_comments']				= 'تعليقات أخري';



$label['return_change_send']				= 'إرسال';







$label['return_change2_return_order']				= 'كيف نساعدك على إرجاع الطلب ؟';



$label['return_change2_amount_paid']				= 'إرجاع المبلغ المدفوع';



$label['return_change2_via']				= 'عن طريق';







$label['return_change_confirm_receive_order']				= 'كيف نستطيع استلام الطلب منك ؟';



$label['return_change_confirm_account_review']				= 'مراجعة الحساب';



$label['return_change_confirm_total_return']				= 'ارجاع الاجمالى';



$label['return_change_confirm_shipping_through']				= 'شحن عن طريق';



$label['return_change_confirm_amount_due']				= 'المبلغ المستحق';



$label['return_change_confirm_aramex']				= 'أرامكس';
$label['aramex_shipment']				= 'شحن عن طريق ارامكس';
$label['aramex_track_no']				= 'رقم تتبع الشحنة من ارامكس';

$label['state_province']				= "المحافظة";
$label['zip_code']				= "الرمز البريدي";






$label['place_feedback_select_rating']				= 'أختاري طلب للتقييم';



$label['place_feedback_evaluate_order']				= 'تقييم الطلب';



$label['place_feedback_success']				= 'تم حفظ تقييمك بنجاح';







$label['place_feedback2_order_review']				= 'مراجعة الطلب';



$label['place_feedback2_order_number']				= 'رقم الطلب';



$label['place_feedback2_order_date']				= 'تاريخ الطلب';



$label['place_feedback2_positive']				= 'إيجابي';



$label['place_feedback2_negative']				= 'سلبي';



$label['place_feedback2_other_comments']				= 'هل لديك تعليقات أخرى ؟';







$label['lost_item_orders_lost']				= 'أختارى من طلباتك ايهم مفقود';



$label['lost_item_lost']				= 'مفقود';



$label['lostitem_success']				= 'تم الإبلاغ عن المنتج بنجاح';







$label['logout_menu_my_wish_list']				= 'مفضلة';



$label['logout_menu_sign_out']				= 'تسجيل الخروج';







$label['designers_design']				= 'تصميم';







$label['design_with_us_upload_portfolio']				= 'تحميل الكتالوجات';



$label['design_with_us_Website']				= 'موقع الكتروني';



$label['design_with_us_upload_cv'] = 'تحميل السيرة الذاتية';



$label['design_thank_you_email'] = 'شكرا لتقديمكم في eDesign';



$label['design_email'] = 'تم إستلام السيرة الذاتية لمصممة';



$label['design_cv_title'] = 'السيرة الذاتي';



$label['designers_designer']				= 'مصمم';



$label['design_visit_our_store']				= 'قم بزيارة متجرنا عبر الإنترنت';



$label['design_with_us_portfolio']				= 'الكتالوجات';







$label['validation_mandatory']				= 'حقل إجباري';



$label['validation_mandatory_size']				= 'الرجاء اختيار المقاس';



$label['validation_invalid_email']				= 'البريد الإلكتروني غير صحيح';



$label['validation_password_mismatch']				= 'الرقم السري و تأكيد الرقم السري غير مطابقين';







$label['email_thank_you'] = 'شكرا لتسجيلكم في موقع eDesign';



$label['email_registration_subject'] = 'تأكيد التسجيل في موقع eDesign';







$label['email_thank_you_career'] = 'شكراً للتقديم في eDesign';



$label['email_career_subject'] = 'تم استقبال أستمارة توظيف جديدة';



$label['email_career_detail_message'] = 'فيما يلي تفاصيل المتقدم';







$label['email_template_footer_need_more_help'] = 'تحتاج إلى المزيد من المساعدة؟';



$label['email_template_footer_service_team'] = 'أتصل بفريق خدمة العملاء المميز على';



$label['email_template_or'] = 'أو';



$label['email_template_thank_you_for_shopping'] = 'شكرا ً للتسوق معنا';



$label['email_template_hi'] = 'مرحبا';



$label['email_template_hello'] = 'أهلا ً ';







$label['request_message'] = 'تم الطلب فى';







$label['home_our_mission'] = 'مهمتنا';



$label['home_or'] = 'و';



$label['home_our_vision'] = 'رؤيـــتـنا';



$label['home_our_values'] = 'قيمـــــنا';



$label['home_banner_text'] = 'تريدين ان تكونى مصممة';



$label['home_banner_button_text'] = 'مع eDesign ';







$label['career_working_with'] = 'العمل مع eDesign';



$label['career_invalid_file'] = 'نوع الملف غير صالح.';







$label['no_records'] = 'لا تسجيلات';







$label['newsletter_success_msg'] = 'شكرا للإشتراكك بالنشرة الاخبارية';



$label['newsletter_already_subscribed_msg'] = 'هذا البريد الالكتروني مسجل مسبقا';



$label['newsletter_error_msg'] = 'حدث خطأ، نرجو المحاولة مرة أخرى.';







$label['wishlist_title'] = 'قائمة الرغبات';



$label['wishlist_already_in'] = 'المنتج هو بالفعل لسلة المفضلة.';



$label['wishlist_added_to'] = 'يضاف المنتج إلى سلة.';







$label['not_saved'] = 'يتم حفظ';



$label['already_rated'] = 'لقد قمت بتقييم المنتج هذا من قبل';
$label['rating_validation_msg'] = 'يرجى تحديد تصنيف النجوم وإدخال رأيك!';



$label['give_us_your_review'] = 'شكرا ً على مشاركتك في التقييم';



$label['give_us_review']				= 'مراجعة';



$label['give_us_review_email_subject']	= 'تقييم';



$label['login_to_rate_product']	= 'الرجاء تسجيل الدخول لتقييم هذا المنتج';









$label['daily_offer_start_date'] = 'تاريخ البدء';



$label['daily_offer_end_date'] = 'تاريخ الإنتهاء';







$label['no_results_found'] = 'لا يوجد نتائج';







$label['delete_confirm_message'] = 'هل أنت متأكد من أنك تريد حذف هذا المنتج ؟';



$label['delete_confirm_general'] = 'هل أنت متأكد من أنك تريد حذف هذا ؟';
$label['cancel_confirm_general'] = 'هل أنت متأكد أنك تريد إلغاء هذا؟';







$lang['all'] = $label;