<?php
if (!defined('BASEPATH'))
exit('No direct script access allowed');
function checkAdminSession()
   {
	   $CI = & get_Instance();
	   $check = $CI->session->userdata('user');
		if ($CI->input->is_ajax_request()) {
			if($check)
			{
				return true;
			}else
			{
				$data['session_out'] = 'true';
				echo json_encode($data);
				exit();
			}
		}else
		{
			if($check)
			   {
				   return true;
			   }else
			   {
				   redirect($CI->config->item('base_url') . 'admin/login');
			   }
		}
   }
function uploadImage($path, $field_name = 'image')
   {
		$file_name = date('Ymdhsi').$_FILES[$field_name]['name'];
		$file_size = $_FILES[$field_name]['size'];
		$file_tmp  = $_FILES[$field_name]['tmp_name'];
		$file_type = $_FILES[$field_name]['type'];
		//echo $file_tmp,"/frontend/".$path.$file_name; exit;
		if(move_uploaded_file($file_tmp,"assets/frontend/".$path.$file_name))
		{
			return $file_name;
		}else
		{
			return '';
		}
   }
    function uploadImageNew($path, $field_name = 'image')
   {
		$file_name = date('Ymdhsi').$_FILES[$field_name]['name'];
		$file_size = $_FILES[$field_name]['size'];
		$file_tmp  = $_FILES[$field_name]['tmp_name'];
		$file_type = $_FILES[$field_name]['type'];
		//echo $file_tmp,"/frontend/".$path.$file_name; exit;
		if(move_uploaded_file($file_tmp,"uploads/images/".$path.$file_name))
		{
			return $file_name;
		}else
		{
			return '';
		}
   }
   function uploadImage2($path, $field_name = 'gallery_image')
   {
		$file_name = date('Ymdhsi').$_FILES[$field_name]['name'];
		$file_size = $_FILES[$field_name]['size'];
		$file_tmp  = $_FILES[$field_name]['tmp_name'];
		$file_type = $_FILES[$field_name]['type'];
		//echo $file_tmp,"/frontend/".$path.$file_name; exit;
		if(move_uploaded_file($file_tmp,"assets/frontend/".$path.$file_name))
		{
			return $file_name;
		}else
		{
			return '';
		}
   }
function uploadVideo($field_name = 'product_video')
{
    $file_name = date('Ymdhsi').$_FILES[$field_name]['name'];
    $file_size = $_FILES[$field_name]['size'];
    $file_tmp  = $_FILES[$field_name]['tmp_name'];
    $file_type = $_FILES[$field_name]['type'];
    if(move_uploaded_file($file_tmp,"uploads/videos/".$file_name))
    {
        return $file_name;
    }else
    {
        return '';
    }
}
function uploadMultipleImage($path, $field_name, $index)
   {
		$file_name = date('Ymdhsi').$field_name['name'][$index];
		$file_size = $field_name['size'][$index];
		$file_tmp  = $field_name['tmp_name'][$index];
		$file_type = $field_name['type'][$index];
		if(move_uploaded_file($file_tmp,"uploads/images/".$path.$file_name))
		{
			return $file_name;
		}else
		{
			return '';
		}
   }
function uploadFile($field_name = '', $path = 'uploads/cv/')
{
	$file_name = date('Ymdhsi').$_FILES[$field_name]['name'];
	$file_size = $_FILES[$field_name]['size'];
	$file_tmp  = $_FILES[$field_name]['tmp_name'];
	$file_type = $_FILES[$field_name]['type'];
	if(move_uploaded_file($file_tmp,$path.$file_name))
	{
		return $file_name;
	}else
	{
		return '';
	}
}
function uploadMultipleFile($field_name = '', $path = 'uploads/cv/')
{
	$count = count($_FILES[$field_name]['name']);
	for($i=0;$i<$count;$i++)
	{
		$file_name = date('Ymdhsi').$_FILES[$field_name]['name'][$i];
		$file_size = $_FILES[$field_name]['size'][$i];
		$file_tmp  = $_FILES[$field_name]['tmp_name'][$i];
		$file_type = $_FILES[$field_name]['type'][$i];
		if(move_uploaded_file($file_tmp,$path.$file_name))
		{
			$file_arr[] = $file_name;
		}
	}
	return $file_arr;
}
$sr = 0;
function getCategories($p_cid=0,$space='')
    {
        global $sr;
        $CI = & get_Instance();
        $CI->load->model('Model_category');
        $fetch_by['parent_id'] = $p_cid;
        $rows = $CI->Model_category->getMultipleRows($fetch_by,true,'asc', 'itme_order');
        $count=count($rows);
        if($p_cid==0)
		{
               $space='';
        }else
		{
               $space .='&nbsp &nbsp &nbsp &nbsp &nbsp';
        }
        if($count > 0)
		{
			if(!empty($rows))
			{
				foreach($rows as $row)
				{
				    $imgPath = base_url().'assets/admin/assets/images/drag.png';
					$active = '';
					if($row['active'] == 'On')
					{
						$active = '<i class="material-icons md-color-light-blue-600 md-24">&#xE86C;</i>';
					}
				    echo '<tr class="'.$row["id"].'" id="'.$row["id"].'">
				   
						 
							<td class="drag_row">'.$space.ucfirst($row['eng_name']).'</td>
							
						   
							<td>
							'.$active;
                    if(viewEditDeleteRights('Category','edit') || viewEditDeleteRights('Category','delete')) {
                        echo '</td>';
                        if (viewEditDeleteRights('Category', 'edit')) {
                            echo '<td><a href="' . base_url() . 'admin/category/edit/' . $row['id'] . '" title="Edit Category">
							 <i class="md-icon material-icons">&#xE254;</i>
							</a>';
                        }
                        if (viewEditDeleteRights('Category', 'delete')) {
                            echo '<a href="javascript:void(0);" class="uk-margin-left delete" onclick="deleteRecord(' . $row['id'] . ',\'admin/category/action\',\'\');" title="Delete Category"> <i class="material-icons md-24 delete">&#xE872;</i>
							</a>';
                        }
                        echo '</td>';
                    }
						   
						  echo '</tr>';
					getCategories($row['id'],$space);
				}
			}
		}
    }
function getCategoriesOptionListing($p_cid=0,$space='',$category_id=0)
{
	//$category_id is use for selected category in edit screen
	$CI = & get_Instance();
	$CI->load->model('Model_category');
	$fetch_by['parent_id'] = $p_cid;
	$rows = $CI->Model_category->getMultipleRows($fetch_by,true);
	$count=count($rows);
	if($p_cid==0)
	{
		   $space='';
	}else
	{
		   $space .='&nbsp &nbsp &nbsp &nbsp &nbsp';
	}
	if($count > 0)
	{
		if(!empty($rows))
		{
			foreach($rows as $row)
			{
				if($category_id == $row['id'])
				{
					$selected = 'selected';
				}elseif(is_array($category_id) && in_array($row['id'], $category_id)){
					$selected = 'selected';
				}else
				{
					$selected = '';
				}
				echo '<option value="'.$row['id'].'" '.$selected.'>'.$space.ucfirst($row['eng_name']).'</option>';
				getCategoriesOptionListing($row['id'],$space,$category_id);
			}
		}
	}
}
function getCategoriesNames($cat_array,$seprated_by=' - ')
{
	//$category_id is use for selected category in edit screen
	$CI = & get_Instance();
	$lang = $CI->session->userdata('site_lang');
	$CI->load->model('Model_category');
	$result = array();
	foreach($cat_array as $cat)
	{
		$row = $CI->Model_category->get($cat, true);
		$result[] = $row[$lang.'_name'];
	}
	return implode($seprated_by, $result);
}
function getColorOptionListing($p_cid=0,$space='',$color_id=0)
{
	//$category_id is use for selected category in edit screen
	$CI = & get_Instance();
	$CI->load->model('Model_variant');
	$fetch_by['eng_name'] = 'Color';
	$rows = $CI->Model_variant->getSingleRow($fetch_by,true);
	$CI->load->model('Model_variant_value');
	$fetch_value_by['variant_id'] = $rows['id'];
	$result = $CI->Model_variant_value->getMultipleRows($fetch_value_by,true);
	$count=count($result);
	if($p_cid==0)
	{
		   $space='';
	}else
	{
		   $space .='&nbsp &nbsp &nbsp &nbsp &nbsp';
	}
	if($count > 0)
	{
		if(!empty($result))
		{
			foreach($result as $res)
			{
				if($color_id == $res['id'])
				{
					$selected = 'selected';
				}else
				{
					$selected = '';
				}
				echo '<option value="'.$res['id'].'" '.$selected.'>'.$space.ucfirst($res['eng_value']).'</option>';
			}
		}
	}
}
function getCategoriesProductsOptionListing($product_id,$category_id)
{
	$result_prod = array();
	$CI = & get_Instance();
    $CI->load->model('Model_product');
	$CI->load->model('Model_related_product');
	$fetch_by['category_id'] = $category_id;
	$results = $CI->Model_product->getMultipleRows($fetch_by);
	$fetch_product_by['product_id'] = $product_id;
	$result_products = $CI->Model_related_product->getMultipleRows($fetch_product_by);
	foreach($result_products as $res_prod)
	{
		$result_prod[] = $res_prod->related_product_id;
	}
	foreach($results as $res)
	{
		if(in_array($res->id, $result_prod))
		{
			$selected = 'selected="selected"';
		}
		else
		{
			$selected = '';
		}
		$result_option .= '<option '.$selected.' value="'.$res->id.'">'.$res->eng_name.'</option>';
	}
	echo $result_option;
}
function getVariantValue($variant_id)
{
	$data = array();
	$results = array();
	$CI = & get_Instance();
    $CI->load->model('Model_variant_value');
	$fetch_by['variant_id'] = $variant_id;
	$results = $CI->Model_variant_value->getMultipleRows($fetch_by);
	return $results;
}
function getVariantById($variant_id)
{
	$data = array();
	$results = array();
	$CI = & get_Instance();
    $CI->load->model('Model_variant');
	$results = $CI->Model_variant->get($variant_id);
	return $results;
}
function getVariantValueById($variant_value_id)
{
	$data = array();
	$results = array();
	$CI = & get_Instance();
    $CI->load->model('Model_variant_value');
	$results = $CI->Model_variant_value->get($variant_value_id);
	return $results;
}
function getTableDataById($id, $table_name, $as_array = false)
{
	$data = array();
	$results = array();
	$CI = & get_Instance();
    $CI->load->model('Model_general');
	$results = $CI->Model_general->getRow($id, $table_name, $as_array);
	return $results;
}
function getVariantsOfGroups($group_id)
{
	$data = array();
	$results = array();
	$CI = & get_Instance();
    $CI->load->model('Model_product_variant_value');
	$fetch_by['product_variant_group_id'] = $group_id;
	$results = $CI->Model_product_variant_value->getMultipleRows($fetch_by);
	if($results)
	{
		foreach($results as $result)
		{
			$data[] = $result->variant_value_id;
		}
		return $data;
	}else
	{
		 return false;
	}
}
function getCartData($id)
{
	$data = array();
	$results = array();
	$CI = & get_Instance();
    $CI->load->model('Model_temp_orders');
	$results = $CI->Model_temp_orders->getRowCount(array('user'=>$id));
	return $results;
}
function getOrderCount($fetch_by)
{
	$data = array();
	$results = array();
	$CI = & get_Instance();
    $CI->load->model('Model_orders');
	$results = $CI->Model_orders->getRowCount($fetch_by);
	return $results;
}
function getCartAddress($address_id)
{
	$CI = & get_Instance();
	$CI->load->model('Model_cart_user_address');
	$result = $CI->Model_cart_user_address->get($address_id);

	return $result;
}
function getAverageRating($id)
{
	$data = array();
	$results = array();
	$CI = & get_Instance();
    $CI->load->model('Model_product_ratings');
	$result = $CI->Model_product_ratings->getAverageRating($id);
	return round($result->rating);
}
function getRatingPercentage($id, $val)
{
	$data = array();
	$results = array();
	$CI = & get_Instance();
    $CI->load->model('Model_product_rating');
	$total = $CI->Model_product_rating->getRowCount(array('product_id'=>$id));
	$val_total = $CI->Model_product_rating->getRowCount(array('product_id'=>$id, 'rating'=>$val));
	$percent = (($val_total/$total)*100);
	return round($percent).'%';
}
function getCconfiguration($arr = false)
{
	$data = array();
	$results = array();
	$CI = & get_Instance();
    $CI->load->model('Model_configuration');
	$results = $CI->Model_configuration->get(1, $arr);
	return $results;
}
function getSettings($arr = false)
{
	$data = array();
	$results = array();
	$CI = & get_Instance();
    $CI->load->model('Model_settings');
	$results = $CI->Model_settings->get(1, $arr);
	return $results;
}
function getCoutries($country = '')
{
	$data = array();
	$results = array();
	$CI = & get_Instance();
	$lang = $CI->session->userdata('site_lang');
    $CI->load->model('Model_general');
	$results = $CI->Model_general->getAll('countries', true);
	$html = '';
    $result = sort_by_alphabet($results,'country');
	foreach($result as $res)
	{
		if($country == $res[$lang.'_country_name'])
		{
			$select = 'selected="selected"';
		}
		else
		{
			$select = '';
		}
        $countryId = str_replace(array(')','(',' '),'',$res[$lang.'_country_name']);
		$html .= '<option id="'.$countryId.'" '.$select.' value="'.$res[$lang.'_country_name'].'">'.$res[$lang.'_country_name'].'</option>';
	}
	return $html;
}
function getCountries_html($country_code = ''){
	$data = array();
	$results = array();
	$CI = & get_Instance();
	$lang = $CI->session->userdata('site_lang');
    $CI->load->model('Model_general');
	$results = $CI->Model_general->getAll('countries', true);
	$html = '';
    $result = sort_by_alphabet($results,'country');
	foreach($result as $res){
		if($country_code == $res['code']){
			$select = 'selected="selected"';
		}else{
			$select = '';
		}
		$html .= '<option '.$select.' value="'.$res['code'].'">'.$res[$lang.'_country_name'].'</option>';
	}
	return $html;
}
function getCountriesAddress_html($country_code = ''){
	$data = array();
	$results = array();
	$CI = & get_Instance();
	$lang = $CI->session->userdata('site_lang');
    $CI->load->model('Model_general');
	$CI->load->model('Model_shipment_group_country');
	$results = $CI->Model_general->getAll('countries', true);
	
	$resultGroups = $CI->Model_shipment_group_country->getActiveGroupCountries();
	$countryArr = array();
	
	foreach($resultGroups as $resgr)
	{
		if(!in_array($resgr->country_id, $countryArr)){
			$countryArr[] = $resgr->country_id;
		}
	}
	$html = '';
    $result = sort_by_alphabet($results,'country');
	foreach($result as $res){
		
		$country = $CI->Model_general->getSingleRow('countries', array(
            'code' => $res['code']
        ));
		
		if(!in_array($country->id, $countryArr)){
			continue;
		}
		
		if($country_code == $res['code']){
			$select = 'selected="selected"';
		}else{
			$select = '';
		}
		$html .= '<option '.$select.' value="'.$res['code'].'">'.$res[$lang.'_country_name'].'</option>';
	}
	return $html;
}
function getCountriesBackend($country_code = ''){
	$data = array();
	$results = array();
	$CI = & get_Instance();
	$lang = $CI->session->userdata('site_lang');
    $CI->load->model('Model_general');
	$results = $CI->Model_general->getAll('countries', true);
	$html = '';
    $result = sort_by_alphabet($results,'country');
	foreach($result as $res){
		if($country_code == $res['code']){
			$select = 'selected="selected"';
		}else{
			$select = '';
		}
		$html .= '<option '.$select.' value="'.$res['code'].'">'.$res['eng_country_name'].'</option>';
	}
	return $html;
}
function getCountriesBackendArr($country_ids = array()){
	$data = array();
	$results = array();
	$CI = & get_Instance();
	$lang = $CI->session->userdata('site_lang');
    $CI->load->model('Model_general');
	$results = $CI->Model_general->getAll('countries', true);
	$html = '';
    $result = sort_by_alphabet($results,'country');
	foreach($result as $res){
		if(in_array($res['id'], $country_ids)){
			$select = 'selected="selected"';
		}else{
			$select = '';
		}
		$html .= '<option '.$select.' value="'.$res['code'].'">'.$res['eng_country_name'].'</option>';
	}
	return $html;
}
function getCoutryByName($country)
{
    $data    = array();
    $results = array();
    $CI =& get_Instance();
    $lang = $CI->session->userdata('site_lang');
    $CI->load->model('Model_general');
	$fetch_by['eng_country_name'] = $country;
    $result = $CI->Model_general->getSingleRow('countries', $fetch_by);
	if(!$result)
	{
		$fetch_by['arb_country_name'] = $country;
    	$result = $CI->Model_general->getSingleRow('countries', $fetch_by);
	}
    return $result;
}
function getCoutryByCode($country_code, $fetchArr=false,$default_lang = '')
{
    $data    = array();
    $results = array();
    $CI =& get_Instance();

    if($default_lang == '') {
        $lang = $CI->session->userdata('site_lang');
    }else{
    $lang = $default_lang;
    }

    $CI->load->model('Model_general');
	$fetch_by['code'] = $country_code;
    $result = $CI->Model_general->getSingleRow('countries', $fetch_by);
	if($fetchArr)
	{
		return $result;
	}
	else
	{
		if($lang == 'eng')
		{
			return $result->eng_country_name;
		}else{
			return $result->arb_country_name;
		}
	}
}
function getCity($country = '', $city = '')
{
	$data = array();
	$results = array();
	$CI = & get_Instance();
	$lang = $CI->session->userdata('site_lang');
	if($lang == "")
	{
	$lang = "eng";	
	}
	
    $CI->load->model('Model_general');
	if($country == '')
	{
		$results = $CI->Model_general->getAll('city', true);
	}
	else
	{
		$country = $CI->Model_general->getSingleRow('countries', array($lang.'_country_name'=>$country));
		$results = $CI->Model_general->getMultipleRows('city', array('countrycode'=>$country->code), true);
	}
	$html = '';
    $result = sort_by_alphabet($results,'city');
	foreach($result as $res)
	{
		if($city == $res['city'])
		{
			$select = 'selected="selected"';
		}
		else
		{
			$select = '';
		}
		$cityId = str_replace(array(')','(',' '),'',$res[$lang.'_name']);
		$html .= '<option id="'.$cityId.'" '.$select.' value="'.$res[$lang.'_name'].'">'.$res[$lang.'_name'].'</option>';
	}
	return $html;
}
function getCity_html($country_code = '', $city_id = ''){
    $data = array();
    $results = array();
    $CI = & get_Instance();
    $lang = $CI->session->userdata('site_lang');
    $CI->load->model('Model_general');
    if($country_code == ''){
        $results = $CI->Model_general->getAll('city', true);
    }else{
        //$country = $CI->Model_general->getSingleRow('countries', array($lang.'_country_name'=>$country));
        $results = $CI->Model_general->getMultipleRows('city', array('countrycode'=>$country_code), true);
    }
    $html = '';
    $result = sort_by_alphabet($results,'city');
    foreach($result as $res){
        if($city_id == $res['id']){
            $select = 'selected="selected"';
        }else{
            $select = '';
        }
        $html .= '<option '.$select.' value="'.$res['id'].'">'.$res[$lang.'_name'].'</option>';
    }
    return $html;
}
function get_cities_html_address($country_code = '', $city_id = ''){
	$data = array();
	$results = array();
	$CI = & get_Instance();
	$lang = $CI->session->userdata('site_lang');
    $CI->load->model('Model_general');
    $CI->load->model('Model_shipment_group_country');
	if($country_code == ''){
		$results = $CI->Model_general->getAll('city', true);
	}else{
		$results = $CI->Model_shipment_group_country->getCitiesByCountryGroup($country_code);
	    if(count($results) == 0){
	        $results = $CI->Model_general->getMultipleRows('city', array('countryCode'=>$country_code), true);
        }
		if(count($results) == 0){
	        $results = $CI->Model_general->getAll('city', true);
        }
	}
	$html = '';
	// cities by alphabet
    $result = sort_by_alphabet($results,'city');
	foreach($result as $res){
		if($city_id == $res['id']){
			$select = 'selected="selected"';
		}else{
			$select = '';
		}
		$html .= '<option '.$select.' value="'.$res['id'].'">'.$res[$lang.'_name'].'</option>';
	}
	return $html;
}
function getCityAdmin($country_code = '', $city_id = ''){
	$data = array();
	$results = array();
	$CI = & get_Instance();
	$lang = $CI->session->userdata('site_lang');
    $CI->load->model('Model_general');
	$html = '';
	if($country_code == ''){
		$results = $CI->Model_general->getAll('city', true);
	}else{
		$country = $CI->Model_general->getSingleRow('countries', array('code'=>$country_code));
		$results = $CI->Model_general->getMultipleRows('city', array('countrycode'=>$country_code), true);
	}
    $result = sort_by_alphabet($results,'city');
	foreach($result as $res){
		if($city_id == $res['id']){
			$select = 'selected="selected"';
		}else{
			$select = '';
		}
		$html .= '<option '.$select.' value="'.$res['id'].'">'.$res['eng_name'].'</option>';
	}
	return $html;
}
function getCityAdminArr($country_codes = array(), $city_ids = array()){
	$data = array();
	$results = array();
	$CI = & get_Instance();
	$lang = $CI->session->userdata('site_lang');
    $CI->load->model('Model_general');
	$html = '';
	foreach($country_codes as $country_code)
	{
		if($country_code == ''){
			$results = $CI->Model_general->getAll('city', true);
		}else{
			$country = $CI->Model_general->getSingleRow('countries', array('code'=>$country_code));
			$results = $CI->Model_general->getMultipleRows('city', array('countrycode'=>$country_code), true);
		}
        $result = sort_by_alphabet($results,'city');
		foreach($result as $res){
			if(in_array($res['id'], $city_ids)){
				$select = 'selected="selected"';
			}else{
				$select = '';
			}
			$html .= '<option '.$select.' value="'.$country->id.'|'.$res['id'].'">'.$res['eng_name'].'</option>';
		}
	}
	return $html;
}
function getCityAdminArrIds($country_ids = array(), $city_ids = array()){
	$data = array();
	$results = array();
	$CI = & get_Instance();
	$lang = $CI->session->userdata('site_lang');
    $CI->load->model('Model_general');
	$html = '';
	foreach($country_ids as $country_id)
	{
		if($country_id == ''){
			$results = $CI->Model_general->getAll('city', true);
		}else{
			$country = $CI->Model_general->getSingleRow('countries', array('id'=>$country_id));
			$results = $CI->Model_general->getMultipleRows('city', array('countrycode'=>$country->code), true);
		}
        $result = sort_by_alphabet($results,'city');
		foreach($result as $res){
			if(in_array($res['id'], $city_ids)){
				$select = 'selected="selected"';
			}else{
				$select = '';
			}
			$html .= '<option '.$select.' value="'.$res['id'].'">'.$res['eng_name'].'</option>';
		}
	}
	return $html;
}
function getCityLang($country = '', $city = '', $lang = 'eng')
{
	$data = array();
	$results = array();
	$CI = & get_Instance();
    $CI->load->model('Model_general');
	if($country == '')
	{
		$results = $CI->Model_general->getAll('city', true);
	}
	else
	{
		$country = $CI->Model_general->getSingleRow('countries', array($lang.'_country_name'=>$country));
		$results = $CI->Model_general->getMultipleRows('city', array('countrycode'=>$country->code), true);
	}
	$html = '';
	foreach($results as $res)
	{
		if($city == $res['id'])
		{
			$select = 'selected="selected"';
		}
		else
		{
			$select = '';
		}
		$html .= '<option '.$select.' value="'.$res['id'].'">'.$res[$lang.'_name'].'</option>';
	}
	return $html;
}
function getCityById($id,$default_lang = ''){
	$results = array();
	$CI = & get_Instance();
	if($default_lang == '') {
        $lang = $CI->session->userdata('site_lang');
    }else{
        $lang = $default_lang;
    }
    $CI->load->model('Model_general');
	$results = $CI->Model_general->getRow($id, 'city', true);
	if($lang == 'eng'){
		return $results['eng_name'];
	}else{
		return $results['arb_name'];
	}
}
function getUserById($id){
	$results = array();
	$CI = & get_Instance();
   	$CI->load->model('Model_registered_users');
	$getBy['id'] = $id;
	$results = $CI->Model_registered_users->getSingleRow($getBy, true);
	return $results;
}
function getUserFromCartById($id){
	$results = array();
	$CI = & get_Instance();
   	$CI->load->model('Model_cart_user_address');
	$getBy['user_id'] = $id;
	$results = $CI->Model_cart_user_address->getSingleRow($getBy, true);
	return $results;
}
function getShippmentDetailByCityId($city_id)
{
	$data = array();
	$results = array();
	$CI = & get_Instance();
    $CI->load->model('Model_general');
	$result = $CI->Model_general->getSingleRow('shipping_details', array('city_id' => $city_id));
	return $result;
}
function getIdByCityName($city_name)
{
	$data = array();
	$results = array();
	$CI = & get_Instance();
    $CI->load->model('Model_general');
	$result = $CI->Model_general->getCityName($city_name, true);
	return $result;
}
function getProductImages($id)
{
	$data = array();
	$results = array();
	$CI = & get_Instance();
    $CI->load->model('Model_image');
	$results = $CI->Model_image->getMultipleRows(array('product_id'=>$id), true);
	return $results;
}

function GetProductColor($prod_id)
{
	$results = array();
	$CI = & get_Instance();
    $CI->load->model('Model_product');
	$results = $CI->Model_product->GetProductColor($prod_id,true);

	return $results;
}
function getProductBrand($brand_id)
{
	$results = array();
	$CI = & get_Instance();
    $CI->load->model('Model_brand');
	$results = $CI->Model_brand->get($brand_id,true);
	return $results;
}

function getOfferImages($id)
{
	$data = array();
	$results = array();
	$CI = & get_Instance();
    $CI->load->model('Model_offer_images');
	$results = $CI->Model_offer_images->getMultipleRows(array('offer_id'=>$id), true);
	return $results;
}
function getCurrency($lang = 'eng')
{
	$data = array();
	$results = array();
	$CI = & get_Instance();
    $CI->load->model('Model_currency');
    $currency_id = $CI->session->userdata('currency_id');
    if($currency_id != NULL) {
        $results = $CI->Model_currency->get($currency_id);
    }else{
        $results = $CI->Model_currency->getSingleRow(array('default_currency'=>1));
    }
	if($lang == 'arb')
	{
		return $results->arb_currency;
	}
	else
	{
		return $results->eng_currency;
	}
}
function getPriceCur($price, $onlyCur = false)
{
	$data = array();
	$results = array();
	$CI = & get_Instance();
	$lang = $CI->session->userdata('site_lang');
    $CI->load->model('Model_currency');
	$results = $CI->Model_currency->get('1', true);
	if($results['position'] == 'B')
	{
		if($onlyCur)
		{
			$response = $results[$lang.'_currency'];
		}
		else
		{
			$response = $results[$lang.'_currency'].$price;
		}
	}
	else
	{
		if($onlyCur)
		{
			$response = '<span>'.$results[$lang.'_currency'].'</span>';
		}
		else
		{
			$response = $price.' <span>'.$results[$lang.'_currency'].'</span>';
		}
	}
	return $response;
}
function checkUser($id)
{
	$data = array();
	$results = array();
	$CI = & get_Instance();
    $CI->load->model('Model_registered_users');
	$results = $CI->Model_registered_users->getRowCount(array('id'=>$id));
	return $results;
}
function getUser($id)
{
	$data = array();
	$results = array();
	$CI = & get_Instance();
    $CI->load->model('Model_registered_users');
	$results = $CI->Model_registered_users->get($id);
	return $results;
}
function getCategoriesById($category_id)
{
	//$category_id is use for selected category in edit screen
	$CI = & get_Instance();
	$CI->load->model('Model_category');
	$row = $CI->Model_category->get($category_id,true);
	return $row;
}
function menu()
{
	$CI = & get_Instance();
	$lang = $CI->session->userdata('site_lang');
    $CI->load->model('Model_category');
	$fetch_by['active'] = 'On';
	$results = $CI->Model_category->getMultipleRows($fetch_by, true);
	if($lang == 'eng')
	{
		$category_id = $CI->uri->segment(3);
	}
	else
	{
		$category_id = $CI->uri->segment(2);
	}
	$html = '';
	$active = '';
	foreach($results as $row)
	{
		if(trim($category_id) == trim(str_replace(' ', '-', $row['eng_name'])))
		{
			$active = 'class="active"';
		}
		else
		{
			$active = '';
		}
		$html .= '<a href="'.lang_base_url().'product/'.str_replace(' ', '-', $row['eng_name']).'" '.$active.'>'.$row[$lang.'_name'].'</a>';
	}
	echo $html;
}
function getAllCategories()
{
	$CI = & get_Instance();
	$lang = $CI->session->userdata('site_lang');
    $CI->load->model('Model_category');
	$results = $CI->Model_category->getAll(true);
	return $results;
}
function getAllCategoriesFront()
{
	$CI = & get_Instance();
	$lang = $CI->session->userdata('site_lang');
    $CI->load->model('Model_category');
	$fetch_by['active'] = 'On';
	$results = $CI->Model_category->getMultipleRows($fetch_by, true);
	return $results;
}
function getPageData($page_id, $arr=false)
{
	$CI = & get_Instance();
	$lang = $CI->session->userdata('site_lang');
    $CI->load->model('Model_page');
	$results = $CI->Model_page->get($page_id, $arr);
	return $results;
}
function getProdCategory($id)
{
	$CI = & get_Instance();
	$lang = $CI->session->userdata('site_lang');
	$CI->load->model('Model_category');
	$results = $CI->Model_category->get($id, true);
	return $results[$lang.'_name'];
}
function getRecentViewed()
{
	$CI = & get_Instance();
    $CI->load->model('Model_product');
	$product_ids = unserialize(get_cookie('mv_p_id'));
	$products = array();
	foreach($product_ids as $p_id)
	{
		$product = $CI->Model_product->get($p_id, true);
		if($product)
		{
			$products[] = $product;
		}
	}
	return $products;
}
function getGiftCarTotal()
{
	$giftIds = unserialize(get_cookie('giftIds'));
	$CI = & get_Instance();
	$CI->load->model('Model_gifts');
	$totalAmount = 0;
	foreach($giftIds as $giftId)
	{
		$gift = $CI->Model_gifts->get($giftId);
		$totalAmount += $gift->amount;
	}
	return $totalAmount;
}
function getGiftCardDetail()
{
	$giftIds = unserialize(get_cookie('giftIds'));
	$CI = & get_Instance();
	$CI->load->model('Model_gifts');
	$giftArray = array();
	//$totalAmount = 0;
	foreach($giftIds as $giftId)
	{
		$giftArray[] = $CI->Model_gifts->get($giftId, true);
		
	}
	return $giftArray;
}
function getGiftCarTotalDb($order_id)
{
	$CI = & get_Instance();
	$CI->load->model('Model_gifts');
	$CI->load->model('Model_order_gifts');
	$giftIds = $CI->Model_order_gifts->getMultipleRows(array('order_id'=>$order_id));
	$totalAmount = 0;
	foreach($giftIds as $giftId)
	{
		$gift = $CI->Model_gifts->get($giftId->gift_id);
		$totalAmount += $gift->amount;
	}
	return $totalAmount;
}
function getGiftCarDetailDb($order_id)
{
	$CI = & get_Instance();
	$CI->load->model('Model_gifts');
	$CI->load->model('Model_order_gifts');
	$giftIds = $CI->Model_order_gifts->getMultipleRows(array('order_id'=>$order_id));
	$giftArray = array();
	if($giftIds)
	{
		foreach($giftIds as $giftId)
		{
			$giftArray[] = $CI->Model_gifts->get($giftId->gift_id, true);
		}
		return $giftArray;
	}
	else
	{
		return $giftIds; 
	}
}
function getCouponTotal($cart_total)
{
	$CI = & get_Instance();
	$CI->load->model('Model_coupons');
	if(get_cookie('voucher_code'))
	{
		$voucher_code = get_cookie('voucher_code');
		$fetch_by['code'] = $voucher_code;
		$coupon = $CI->Model_coupons->getSingleRow($fetch_by);
		if($coupon->type == 1)
		 {
			$discount = ($cart_total*$coupon->discount)/100; 
		 }
		 else
		 {
			 $discount = $coupon->discount; 
		 }
						 
		return ($cart_total - $discount);
	}
	else
	{
		return $cart_total;
	}
}
function getCouponPrice($cart_total)
{
    $CI = & get_Instance();
    $CI->load->model('Model_coupons');
    $discount = 0;
    if(get_cookie('voucher_code'))
    {
        $voucher_code = get_cookie('voucher_code');
        $fetch_by['code'] = $voucher_code;
        $coupon = $CI->Model_coupons->getSingleRow($fetch_by);
        if($coupon->type == 1)
        {
            $discount = ($cart_total*$coupon->discount)/100;
        }
        else
        {
            $discount = $coupon->discount;
        }
    }
    return $discount;
}
function getCouponDiscount()
{
	$CI = & get_Instance();
	$lang = $CI->session->userdata('site_lang');
	$CI->load->model('Model_coupons');
	$voucher_code = get_cookie('voucher_code');
	$fetch_by['code'] = $voucher_code;
	$coupon = $CI->Model_coupons->getSingleRow($fetch_by);
	if($coupon->type == 1)
	 {
		$discount = $coupon->discount.'%';
	 }
	 else
	 {
		 $discount = $coupon->discount.' '.getCurrency($lang);
	 }
					 
	return $discount;
}
function getCouponDiscountDb($coupon_id)
{
	$CI = & get_Instance();
	$CI->load->model('Model_coupons');
	
	$fetch_by['id'] = $coupon_id;
	$coupon = $CI->Model_coupons->getSingleRow($fetch_by);
	return $coupon;
}
function getCouponTotalDb($cart_total, $coupon_id)
{
	$CI = & get_Instance();
	$CI->load->model('Model_coupons');
	if($coupon_id > 0)
	{
		$fetch_by['id'] = $coupon_id;
		$coupon = $CI->Model_coupons->getSingleRow($fetch_by);
		if($coupon->type == 1)
		 {
			$discount = ($cart_total*$coupon->discount)/100; 
		 }
		 else
		 {
			 $discount = $coupon->discount; 
		 }
						 
		return ($cart_total - $discount);
	}
	else
	{
		return $cart_total;
	}
}
function getMaxMinPrices()
{
	$CI = & get_Instance();
    $CI->load->model('Model_product');
	$price = $CI->Model_product->fetcMaxMinPrice();
	return $price;
}
function getProductUnreadCount($id)
{
	$CI = & get_Instance();
    $CI->load->model('Model_customer_questions');
	$count = $CI->Model_customer_questions->getRowCount(array('product_id'=>$id, 'answered'=>'0'));
	return $count;
}
function getAllProductUnreadCount()
{
	$CI = & get_Instance();
    $CI->load->model('Model_customer_questions');
	$count = $CI->Model_customer_questions->getRowCount(array('answered'=>'0'));
	return $count;
}
function getFilters($filter)
{
	$CI = & get_Instance();
	$CI->load->model('Model_variant');
    $CI->load->model('Model_variant_value');
	$fetch_by['eng_name'] = $filter;
	$results = $CI->Model_variant->getMultipleRows($fetch_by);
	$fetch_value['variant_id'] = $results[0]->id;
	$varients = $CI->Model_variant_value->getMultipleRows($fetch_value, true);
	return $varients;
}
function varientValue($group_id)
{
	$data = array();
	$results = array();
	$CI = & get_Instance();
    $CI->load->model('Model_product_variant_value');
	$fetch_by['product_variant_group_id'] = $group_id;
	$results = $CI->Model_product_variant_value->getMultipleRows($fetch_by);
	if($results)
	{
		foreach($results as $result)
		{
			$data[] = $result->variant_id.'|'.$result->variant_value_id;
		}
		return implode(',',$data);
	}else
	{
		 return false;
	}
}
function getShipmentMethod($id)
{
	$data = array();
	$results = array();
	$CI = & get_Instance();
	$lang = $CI->session->userdata('site_lang');
    $CI->load->model('Model_shipment_methods');
	$result = $CI->Model_shipment_methods->get($id, true);
	return $result[$lang.'_name'];
}
function getAnswers($id)
{
	$data = array();
	$results = array();
	$CI = & get_Instance();
	$lang = $CI->session->userdata('site_lang');
    $CI->load->model('Model_answer');
	$fetch_by['question_id'] = $id;
	$result = $CI->Model_answer->getMultipleRows($fetch_by, true);
	return $result;
}
function getAdminProfileImage($id)
{
	$CI = & get_Instance();
	$lang = $CI->session->userdata('site_lang');
    $CI->load->model('Model_users');
	$result = $CI->Model_users->get($id);
	return $result->image_name;
}
function dateFormat($date)
{
	$CI = & get_Instance();
	$lang = $CI->session->userdata('site_lang');
	if($lang == 'arb')
	{
		$month = array('يناير', 'فبراير', 'مارس', 'أبريل', 'مايو', 'يونيو', 'يوليو', 'أغسطس', 'سبتمبر', 'أكتوبر', 'نوفمبر', 'ديسمبر');
	}
	else
	{
		$month = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
	}
	$dateArr = explode('-', $date);
	return $dateArr[2].' '.$month[((int)$dateArr[1]-1)].' '.$dateArr[0];
}
function emailTemplateRegister($message)
{
	$CI = & get_Instance();
	$lang = $CI->session->userdata('site_lang');
	$CI->load->model('Model_registered_users');
	$CI->load->model('Model_registered_user_address');
	$user_id = $CI->session->userdata('user_id');
	$config = getCconfiguration();
	if($lang == 'arb')
	{
		$body_style = 'style="direction:rtl"';
		$left = 'right';
		$right = 'left';
		$td_phone_style = ' unicode-bidi: plaintext;';
	}
	else
	{
		$body_style = '';
		$left = 'left';
		$right = 'right';
		$td_phone_style = '';
	}
	$fetch_by['user_id'] = $user_id;
	$data = $CI->Model_registered_users->get($user_id, true);
	$streets = $CI->Model_registered_user_address->getMultipleRows($fetch_by, true);
	foreach($streets as $street)
	{
		if($street != '')
		{
			$address_html .= '<tr>
								<td>
									<table width="400" cellpadding="0" cellspacing="0" border="0" style="border-bottom: 1px solid #eee; margin: auto; padding: 20px 0 18px;">
										<tr>
											<td colspan="2" align="'.$left.'" valign="middle" style="font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; padding-bottom:15px;"><strong>'.$message['register_addresses'].': </strong></td>
										</tr>
										<tr>
											<td colspan="2"  align="'.$left.'" valign="middle" style="font-size:12px;font-weight: normal; color:#E00513; text-transform:uppercase; padding-bottom:3px;">'.$data['first_name'].' '.$data['last_name'].'</td>
										</tr>
										<tr>
											<td colspan="2"  align="'.$left.'" valign="middle" style="font-size:12px;font-weight: normal; color:#777; text-transform:uppercase;">'.$street['property_no'].' '.$street['street_name'].' '.$street['address_city'].', '.$street['address_country'].', '.$street['address_district'].' <strong>'.$message['register_email_other_info'].':</strong> '.$street['address_po_box'].', '.$street['address_zip_code'].'</td>
										</tr>
										<tr>
											<td colspan="2"  align="'.$left.'" valign="middle" style="font-size:12px;font-weight: normal; color:#777; text-transform:uppercase;">'.$street['more_details'].'</td>
										</tr>
									</table>
								</td>
							</tr>';
		}
		$i++;
	}
	$html .= '
				<html>
				<head>
					<style type="text/css">
						body {
							color: #777;
							padding:0;
							margin: 0;
						}
						body, table, td, p, a, li, blockquote {
							-webkit-text-size-adjust: none !important;
							font-style: normal;
							font-weight: 400;
							font-family: \'Roboto\', sans-serif;
						}
						@import \'https://fonts.googleapis.com/css?family=Roboto\';
						@import \'url(http://fonts.googleapis.com/earlyaccess/notosanskannada.css)\';
						button {
							width: 90%;
						}
						p {
							font-size: 13px !important;
							color: #777 !important;
							line-height: normal !important;
							padding-bottom: 14px !important;
							margin: 0px !important;
						}
						@media screen and (max-width:600px) {
						/*styling for objects with screen size less than 600px; */
						body, table, td, p, a, li, blockquote {
							-webkit-text-size-adjust: none!important;
						}
						table {
							/* All tables are 100% width */
							width: 100%;
						}
						.footer {
							/* Footer has 2 columns each of 48% width */
							height: auto !important;
							max-width: 48% !important;
							width: 48% !important;
						}
						table.responsiveImage {
							/* Container for images in catalog */
							height: auto !important;
							max-width: 30% !important;
							width: 30% !important;
						}
						table.responsiveContent {
							/* Content that accompanies the content in the catalog */
							height: auto !important;
							max-width: 66% !important;
							width: 66% !important;
						}
						.top {
							/* Each Columnar table in the header */
							height: auto !important;
							max-width: 48% !important;
							width: 48% !important;
						}
						.catalog {
							margin-left: 0%!important;
						}
						}
						
						@media screen and (max-width:480px) {
						/*styling for objects with screen size less than 480px; */
						body, table, td, p, a, li, blockquote {
						}
						table {
							/* All tables are 100% width */
							width: 100% !important;
							border-style: none !important;
						}
						.footer {
							/* Each footer column in this case should occupy 96% width  and 4% is allowed for email client padding*/
							height: auto !important;
							max-width: 96% !important;
							width: 96% !important;
						}
						.table.responsiveImage {
							/* Container for each image now specifying full width */
							height: auto !important;
							max-width: 96% !important;
							width: 96% !important;
						}
						.table.responsiveContent {
							/* Content in catalog  occupying full width of cell */
							height: auto !important;
							max-width: 96% !important;
							width: 96% !important;
						}
						.top {
							/* Header columns occupying full width */
							height: auto !important;
							max-width: 100% !important;
							width: 100% !important;
						}
						.catalog {
							margin-left: 0%!important;
						}
						button {
							width: 90%!important;
						}
						}
						table a {color:#d80000 !important;text-decoration:none !important;}
						</style>
						</head>
						<body '.$body_style.'>
						<table width="100%" cellspacing="0" cellpadding="0">
						  <tbody>
							<tr>
							  <td>
								<table width="500"  align="center" cellpadding="0" cellspacing="0" style="background:url(images/header-top.jpg) repeat-x top left; width=100%;max-width:500px; ">
								  <!-- Main Wrapper Table with initial width set to 60opx -->
								  <tbody>
									<tr>
									  <td>
											<table class="top" width="100%"  align="left" cellpadding="0" cellspacing="0" style="padding:35px 0 53px;">
											  <!-- First header column with Logo -->
											  <tbody>
												<tr>
												  <td align="center" valign="middle"><a href="'.base_url().'" target="_blank"><img src="'.base_url().'assets/frontend/images/logo.png" alt="eDesign"></a></td>
												</tr>
											  </tbody>
										  </table>
						
									  </td>
									</tr>
									<tr>
									  <td>
											<table width="100%" align="left"  cellpadding="0" cellspacing="0" >
										  <tr>
											<td align="center" valign="middle" style="font-size:13px; font-weight: 500; padding-bottom: 22px;">'.$message['email_template_hi'].' {hi_name},</td>
										  </tr>
										  <tr>
											<td align="center" valign="middle" style="font-family: \'Roboto\', sans-serif; font-weight: 100; font-size: 22px; color: #E00513; padding-bottom: 16px;">
												'.$message['email_thank_you'].'
											</td>
										  </tr>
										</table>
									  </td>
									</tr>
									<tr>
										<td>
											<table width="400" cellpadding="0" cellspacing="0" border="1" style="border:0px; border-bottom: 1px solid rgba(119, 119, 119, 0.51); margin:  0 auto 30px; padding: 35px 0 0;">
												<tr>
													<td align="'.$left.'" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; "><strong>'.$message['register_full_name'].'</strong></td>
													<td align="'.$right.'" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-'.$left.':0; padding: 10px 5px;font-size:12px;font-weight: normal; color:#D80000; text-transform:uppercase; ">'.$data['first_name'].'</td>
												</tr>
												
												<tr>
													<td align="'.$left.'" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; "><strong>'.$message['register_email'].'</strong></td>
													<td align="'.$right.'" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-'.$left.':0; padding: 10px 5px;font-size:12px;font-weight: normal; color:#D80000; text-transform:uppercase; ">'.$data['email'].'</td>
												</tr>';
												/*<tr>
													<td align="'.$left.'" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; "><strong>'.$message['register_last_name'].'</strong></td>
													<td align="'.$right.'" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-'.$left.':0; padding: 10px 5px;font-size:12px;font-weight: normal; color:#D80000; text-transform:uppercase; ">'.$data['last_name'].'</td>
												</tr>
												<tr>
													<td align="'.$left.'" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; "><strong>'.$message['register_mobile_number'].'</strong></td>
													<td align="'.$right.'" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-'.$left.':0; padding: 10px 5px;font-size:12px;font-weight: normal; color:#D80000; text-transform:uppercase;'.$td_phone_style .'">'.$data['mobile_no'].'</td>
												</tr>
												<tr>
													<td align="'.$left.'" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; "><strong>'.$message['register_phone_number'].'</strong></td>
													<td align="'.$right.'" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-'.$left.':0; padding: 10px 5px;font-size:12px;font-weight: normal; color:#D80000; text-transform:uppercase;'.$td_phone_style .'">'.$data['phone_no'].'</td>
												</tr>
												<tr>
													<td align="'.$left.'" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; "><strong>'.$message['register_city'].'</strong></td>
													<td align="'.$right.'" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-'.$left.':0; padding: 10px 5px;font-size:12px;font-weight: normal; color:#D80000; text-transform:uppercase; ">'.$data['city'].'</td>
												</tr>
												<tr>
													<td align="'.$left.'" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; "><strong>'.$message['register_country'].'</strong></td>
													<td align="'.$right.'" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-'.$left.':0; padding: 10px 5px;font-size:12px;font-weight: normal; color:#D80000; text-transform:uppercase; ">'.$data['country'].'</td>
												</tr>*/
											$html .= '</table>
										</td>
									</tr>
									'.$address_html.'
									<tr>
										<td align="center" valign="middle" style="font-size:13px; padding: 46px 0 18px;">'.$message['email_template_footer_need_more_help'].'</td>
									</tr>
									<tr>
										<td align="center" valign="middle" style="font-size:11px; padding: 0px 0 15px;">'.$message['email_template_footer_service_team'].' <span style="direction: ltr; unicode-bidi: bidi-override;" >'.$config->phone.'</span> '.$message['email_template_or'].'<br>'.$message['chat_email'].' '.$config->email.'</td>
									</tr>
									<tr>
										<td align="center" valign="middle" style="font-size:11px; padding: 0px 0 50px;">'.$message['email_template_thank_you_for_shopping'].'</td>
									</tr>
								  </tbody>
								  <tfoot bgcolor="#333">
									<tr>
										<td colspan="2">
											<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-top: 1px solid #f1f1f1; padding: 20px 30px 46px;">
												<tr>
													<td style="direction: '.($lang == 'arb' ? 'ltr' : 'rtl').';" align="center">
														
														<span style="font-size:11px; color:#fff;">'.$message['login_follow_us'].'</span>														
														<a href="'.$config->facebook_link.'" target="_blank" style="display:inline-block; margin-right:14px;"><img src="'.base_url().'assets/frontend/images/facebook-icon.png" alt=""></a>
														<a href="'.$config->twitter_link.'" target="_blank" style="display:inline-block; margin-right:14px;"><img src="'.base_url().'assets/frontend/images/twitter-icon.png" alt=""></a>
														<a href="'.$config->instagram_link.'" target="_blank" style="display:inline-block; margin-right:14px;"><img src="'.base_url().'assets/frontend/images/instagram-icon.png" alt=""></a>
														<a href="'.$config->snapchat_link.'" target="_blank" style="display:inline-block; margin-right:14px;"><img src="'.base_url().'assets/frontend/images/snapchat-icon.png" alt=""></a>
														<a href="'.$config->youtube_link.'" target="_blank" style="display:inline-block; margin-right:16px;"><img src="'.base_url().'assets/frontend/images/youtube-icon.png" alt=""></a>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td>
											<table width="100%" cellpadding="0" cellspacing="0" border="0" style="padding: 0px 30px 24px;">
												<tr>
													<td align="'.$left.'" valign="middle" style="font-family: \'Noto Sans Kannada\', sans-serif; font-size:10px; color: #fff;">'.($lang == 'arb' ? 'جميع الحقوق محفوظة I eDesign '.date('Y') : 'All rights reserved '.date('Y').' | eDesign').'</td>
													<td align="'.$right.'" valign="middle"><a href="'.base_url().'" target="_balnk" style="font-size:10px; color: #ffffff; text-decoration: none;">'.$message['design_visit_our_store'].'</a></td>
												</tr>
											</table>
										</td>
									</tr>
								  </tfoot>
								</table>
								</td>
							</tr>
						  </tbody>
						</table>
				</body>
				</html>';
			$user = str_replace('{hi_name}', $data['first_name'].' '.$data['last_name'], $html);
			htmlmail($data['email'], $message['email_registration_subject'], $user, 'developer@edesign.com.sa');
			$admin = str_replace('{hi_name}','Admin', $html);
			$admin = str_replace($message['email_thank_you'], $message['register_email_subject'], $admin);
			htmlmail($config->admin_email, $message['register_email_subject'], $admin, 'developer@edesign.com.sa');
}
function emailTemplateEng($order_id, $messages)
{
	$CI = & get_Instance();
    $CI->load->model('Model_orders');
	$CI->load->model('Model_cart_user_address');
	$result = $CI->Model_orders->get($order_id);
	$address = $CI->Model_cart_user_address->get($result->address_id);
	$products = $CI->Model_orders->getOrderProdLang($order_id);
	$config = getCconfiguration();
	$total_amount=0;
	$prod = '';
	foreach($products as $product)
	{
		$images = getProductImages($product->id);
		$prod .= '<tr>
              <td><table width="400" cellpadding="0" cellspacing="0" border="0" style="border-bottom: 1px solid #eee; margin: auto;">
                  <tr>
                      <td align="left" valign="middle" width="80%"><table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                          <td align="left" valign="middle" style="font-size:12px; color:#777; padding-bottom:3px"><strong>Description:</strong> '.$product->eng_description.'</td>
                        </tr>
                        <tr>
                          <td align="left" valign="middle" style="font-size:12px; color:#777; padding-bottom:3px"><strong>Quantity:</strong> '.$product->quantity.'</td>
                        </tr>';
		 if($product->varient_id != '')
		  {
			  $var_arr = explode('|', $product->varient_id);
			  $var_val_arr = explode('|', $product->varient_value_id);
			  $i = 0;
			  foreach($var_arr as $varid)
			  {
				  $var = getVariantById($varid);
				  $var_val = getVariantValueById($var_val_arr[$i]);
				  $prod .= '<tr>
                          <td align="left" valign="middle" style="font-size:12px; color:#777; padding-bottom:3px"><strong>'.$var->eng_name.':</strong> '.$var_val->eng_value.'</td>
                        </tr>';
			  $i++;
			  }
		  }
		 $prod .= '<tr>
                          <td align="left" valign="middle" style="font-size:12px; color:#777;"><strong>Price:</strong> <span style="color: #D80000;">'.getPriceCur($product->quantity*$product->price).'</span></td>
                        </tr>
                      </table></td>
					  <td align="center" valign="top" width="20%"><img src="'.base_url().'uploads/images/products/'.$images[0]['eng_image'].'" alt="" width="76" height="88" style="display:inherit;"></td>
                  </tr>
                </table></td>
            </tr>';
		$total_amount += ($product->quantity*$product->price);
	}
	$html = '<!doctype html>
					<!--Quite a few clients strip your Doctype out, and some even apply their own. Many clients do honor your doctype and it can make things much easier if you can validate constantly against a Doctype.-->
					<html>
					<head>
					<meta charset="utf-8">
					<meta name="viewport" content="width=device-width, initial-scale=1.0">
					
					<!-- Please use an inliner tool to convert all CSS to inline as inpage or external CSS is removed by email clients -->
					<!-- When use in Email please remove all comments as it is removed by Email clients-->
					<!-- important in CSS is used to prevent the styles of currently inline CSS from overriding the ones mentioned in media queries when corresponding screen sizes are encountered -->
					
					<style type="text/css">
					body {
						color: #777;
						padding: 0;
						margin: 0;
					}
					body, table, td, p, a, li, blockquote {
						-webkit-text-size-adjust: none !important;
						font-style: normal;
						font-weight: 400;
						font-family: \'Roboto\', sans-serif;
					}
					@import \'https://fonts.googleapis.com/css?family=Roboto\';
					@import \'url(http://fonts.googleapis.com/earlyaccess/notosanskannada.css)\';
					button {
						width: 90%;
					}
					p {
						font-size: 13px !important;
						color: #777 !important;
						line-height: normal !important;
						padding-bottom: 14px !important;
						margin: 0px !important;
					}
					
					@media screen and (max-width:600px) {
					/*styling for objects with screen size less than 600px; */
					body, table, td, p, a, li, blockquote {
						-webkit-text-size-adjust: none!important;
					}
					table {
						/* All tables are 100% width */
						width: 100%;
					}
					.footer {
						/* Footer has 2 columns each of 48% width */
						height: auto !important;
						max-width: 48% !important;
						width: 48% !important;
					}
					table.responsiveImage {
						/* Container for images in catalog */
						height: auto !important;
						max-width: 30% !important;
						width: 30% !important;
					}
					table.responsiveContent {
						/* Content that accompanies the content in the catalog */
						height: auto !important;
						max-width: 66% !important;
						width: 66% !important;
					}
					.top {
						/* Each Columnar table in the header */
						height: auto !important;
						max-width: 48% !important;
						width: 48% !important;
					}
					.catalog {
						margin-left: 0%!important;
					}
					}
					
					@media screen and (max-width:480px) {
					/*styling for objects with screen size less than 480px; */
					body, table, td, p, a, li, blockquote {
					}
					table {
						/* All tables are 100% width */
						width: 100% !important;
						border-style: none !important;
					}
					.footer {
						/* Each footer column in this case should occupy 96% width  and 4% is allowed for email client padding*/
						height: auto !important;
						max-width: 96% !important;
						width: 96% !important;
					}
					.table.responsiveImage {
						/* Container for each image now specifying full width */
						height: auto !important;
						max-width: 96% !important;
						width: 96% !important;
					}
					.table.responsiveContent {
						/* Content in catalog  occupying full width of cell */
						height: auto !important;
						max-width: 96% !important;
						width: 96% !important;
					}
					.top {
						/* Header columns occupying full width */
						height: auto !important;
						max-width: 100% !important;
						width: 100% !important;
					}
					.catalog {
						margin-left: 0%!important;
					}
					button {
						width: 90%!important;
					}
					}
					table a {color:#d80000 !important;text-decoration:none !important;}
					</style>
					</head>
					<body>
					<table width="100%" cellspacing="0" cellpadding="0">
					  <tbody>
						<tr>
						  <td><table width="500"  align="center" cellpadding="0" cellspacing="0" style="background:url(images/header-top.jpg) repeat-x top left; width=100%;max-width:500px;">
							  <!-- Main Wrapper Table with initial width set to 60opx -->
							  <tbody>
								<tr>
								  <td><table class="top" width="100%"  align="left" cellpadding="0" cellspacing="0" style="padding:35px 0 53px;">
									  <!-- First header column with Logo -->
									  <tbody>
										<tr>
										  <td align="center" valign="middle"><a href="'.base_url().'"><img src="'.base_url().'assets/frontend/images/logo.png" alt="eDesign"></a></td>
										</tr>
									  </tbody>
									</table></td>
								</tr>
								<tr>
								  <td><table width="100%" align="left"  cellpadding="0" cellspacing="0" >
									  <tr>
										<td align="center" valign="middle" style="font-size:13px; font-weight: 500; padding-bottom: 22px;">Hi {name},</td>
									  </tr>
									 {thankyou_msg}
									  <tr> 
										<!-- HTML Spacer row -->
										<td><table width="100%" align="left"  cellpadding="0" cellspacing="0">
											<tr>
											  <td align="center" valign="middle" style="font-size: 13px; font-weight: normal; color:#E00513; padding: 35px 0 18px;">'.$config->eng_email_success_message.' </td>
											</tr>
										  </table></td>
									  </tr>
									  <tr>
										<td align="center" valign="middle" style="font-size:13px; color: #777; padding-bottom: 38px;">Order No - '.str_pad($order_id, 10, "0", STR_PAD_LEFT).' on '.date("F jS Y h:i a", strtotime($result->created_at)).'.</td>
									  </tr>
									</table></td>
								</tr>
								'.$prod.'
								<tr>
								  <td><table width="400" cellpadding="0" cellspacing="0" border="0" style="border-bottom: 1px solid #eee; margin: auto; padding-top: 35px;">
									  <tr>
										<td></td>
									  </tr>
									</table></td>
								</tr>
								<tr>
								  <td><table width="400" cellpadding="0" cellspacing="0" border="1" style="border:0px; border-bottom: 1px solid rgba(119, 119, 119, 0.51); margin:  0 auto 30px; padding: 35px 0 0;">
									  <tr>
										<td align="left" valign="top" style=" border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px; font-weight: normal; color:#777; text-transform:uppercase; padding-bottom:3px;width: 111px;">PAYMENT METHOD:</td>
										<td align="right" valign="top" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-left:0; padding: 10px 5px;font-size:12px; font-weight: normal; color:#777; text-transform:uppercase; padding-bottom:3px;">'.$result->payment_method.'</td>
									  </tr>
									  <tr>
										<td align="left" valign="top" style=" border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px; font-weight: normal; color:#777; text-transform:uppercase; padding-bottom:3px;width: 111px;">TOTAL ITEMS PRICE:</td>
										<td align="right" valign="top" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-left:0; padding: 10px 5px;font-size:12px; font-weight: normal; color:#777; text-transform:uppercase; padding-bottom:3px;">'.getPriceCur($total_amount).'</td>
									  </tr>
									  <tr>
										<td align="left" valign="top" style=" border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px; font-weight: normal; color:#777; text-transform:uppercase; padding-bottom:3px;width: 111px;">STORE CREDIT:</td>
										<td align="right" valign="top" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-left:0; padding: 10px 5px;font-size:12px; font-weight: normal; color:#777; text-transform:uppercase; padding-bottom:3px;">'.getPriceCur('0').'</td>
									  </tr>
									  
									  <tr>
										<td align="left" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; width: 111px;"><strong>ORDER TOTAL:</strong></td>
										<td align="right" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-left:0; padding: 10px 5px;font-size:12px;font-weight: normal; color:#D80000; text-transform:uppercase; ">'.getPriceCur($total_amount).'</td>
									  </tr>
									  
									</table></td>
								</tr>
								
								<tr>
								  <td><table width="400" cellpadding="0" cellspacing="0" border="1" style="border:0px; border-bottom: 1px solid rgba(119, 119, 119, 0.51); margin:  0 auto 30px; padding: 35px 0 0;">
								  
								  	
									  <tr>
										<td align="left" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; padding-bottom:3px;width: 111px;"><strong>Email:</strong></td>
										<td align="right" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-left:0; padding: 10px 5px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; padding-bottom:3px;">'.$address->email.'</td>
									  </tr>
									  <tr>
									  
										  <tr>
											<td align="left" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; padding-bottom:3px;width: 111px;"><strong>City:</strong></td>
	
											<td align="right" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-left:0; padding: 10px 5px;font-size:12px;font-weight: normal; color:#777; text-transform: inherit; padding-bottom:3px;">'.$address->city.'</td>
	
										  </tr>
										  <tr>
											<td align="left" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; padding-bottom:3px;width: 111px;"><strong>Country:</strong></td>
	
											<td align="right" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-left:0; padding: 10px 5px;font-size:12px;font-weight: normal; color:#777; text-transform: inherit; padding-bottom:3px;">'.$address->country.'</td>
	
										  </tr>
										  <tr>
											<td align="left" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; padding-bottom:3px;width: 111px;"><strong>Region:</strong></td>
	
											<td align="right" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-left:0; padding: 10px 5px;font-size:12px;font-weight: normal; color:#777; text-transform: inherit; padding-bottom:3px;">'.$address->region.'</td>
	
										  </tr>
										  
										  <tr>
											<td align="left" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; padding-bottom:3px;width: 111px;"><strong>Zip Code:</strong></td>
	
											<td align="right" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-left:0; padding: 10px 5px;font-size:12px;font-weight: normal; color:#777; text-transform: inherit; padding-bottom:3px;">'.$address->zip_code.'</td>
	
										  </tr>
										  <tr>
											<td align="left" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; padding-bottom:3px;width: 111px;"><strong>Contact Number:</strong></td>
											<td align="right" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-left:0; padding: 10px 5px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; padding-bottom:3px; unicode-bidi: plaintext;">'.$address->phone_no.'</td>
										  </tr>
								  
								</table></td>
								</tr>
								<tr>
								  <td><table width="400" cellpadding="0" cellspacing="0" border="0" style="border-bottom: 1px solid #eee; margin: auto; padding: 20px 0 18px;">
									  <tr>
										<td colspan="2" align="left" valign="middle" style="font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; padding-bottom:15px;"><strong>BILLING ADDRESS: </strong></td>
									  </tr>
									  <tr>
										<td colspan="2"  align="left" valign="middle" style="font-size:12px;font-weight: bold; color:#E00513; text-transform:inherit; padding-bottom:3px;">'.$address->full_name.'</td>
									  </tr>
									  <tr>
										<td colspan="2"  align="left" valign="middle" style="font-size:12px;font-weight: normal; color:#777; text-transform:inherit;">'.$address->address_1.'</td>
									  </tr>
									  
									</table></td>
								</tr>
								
								<tr>
									<td align="center" valign="middle" style="font-size:13px; padding: 46px 0 18px;">'.$messages['email_template_footer_need_more_help'].'</td>
								</tr>
								<tr>
									<td align="center" valign="middle" style="font-size:11px; padding: 0px 0 15px;">'.$messages['email_template_footer_service_team'].'<span style="direction: ltr; unicode-bidi: bidi-override;" > '.$config->phone.'</span> '.$messages['email_template_or'].'<br>'.$messages['chat_email'].' '.$config->email.'</td>
								</tr>
								<tr>
									<td align="center" valign="middle" style="font-size:11px; padding: 0px 0 50px;">'.$messages['email_template_thank_you_for_shopping'].'</td>
								</tr>
							  </tbody>
							  <tfoot bgcolor="#333">
								<tr>
									<td colspan="2">
										<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-top: 1px solid #f1f1f1; padding: 20px 30px 46px;">
											<tr>
												<td align="center">
													
														<span style="font-size:11px; color:#fff;">'.$messages['login_follow_us'].'</span>
														<a href="'.$config->facebook_link.'" target="_blank" style="display:inline-block; margin-right:14px;"><img src="'.base_url().'assets/frontend/images/facebook-icon.png" alt=""></a>
														<a href="'.$config->twitter_link.'" target="_blank" style="display:inline-block; margin-right:14px;"><img src="'.base_url().'assets/frontend/images/twitter-icon.png" alt=""></a>
														<a href="'.$config->instagram_link.'" target="_blank" style="display:inline-block; margin-right:14px;"><img src="'.base_url().'assets/frontend/images/instagram-icon.png" alt=""></a>
														<a href="'.$config->snapchat_link.'" target="_blank" style="display:inline-block; margin-right:14px;"><img src="'.base_url().'assets/frontend/images/snapchat-icon.png" alt=""></a>
														<a href="'.$config->youtube_link.'" target="_blank" style="display:inline-block; margin-right:16px;"><img src="'.base_url().'assets/frontend/images/youtube-icon.png" alt=""></a>
													</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table width="100%" cellpadding="0" cellspacing="0" border="0" style="padding: 0px 30px 24px;">
											<tr>
												<td align="left" valign="middle" style="font-family: \'Noto Sans Kannada\', sans-serif; font-size:10px; color: #fff;">All rights reserved '.date('Y').' | eDesign</td>
												<td align="right" valign="middle"><a href="'.base_url().'" target="_balnk" style="font-size:10px; color: #ffffff; text-decoration: none;">'.$messages['design_visit_our_store'].'</a></td>
											</tr>
										</table>
									</td>
								</tr>
							  </tfoot>
							</table></td>
						</tr>
					  </tbody>
					</table>
					</body>
					</html>
					';
					$thankyou_msg = '<tr>
											<td align="center" valign="middle" style="font-family: \'Roboto\', sans-serif; font-weight: 100; font-size: 22px; color: #E00513; padding-bottom: 16px;"> '.$config->eng_email_title.' </td>
										  </tr>
										  <tr>
											<td align="center" valign="middle" style="">'.$config->eng_email_description.'</td>
										  </tr>';
					$customer = str_replace('{name}',$address->full_name, $html);
					$customer = str_replace('{thankyou_msg}', $thankyou_msg, $customer);
					htmlmail($result->email, 'order', $customer, 'developer@edesign.com.sa');
					$admin_email = str_replace('{name}','Admin', $html);
					$admin_email = str_replace('{thankyou_msg}','', $admin_email);
					htmlmail($config->admin_email, 'order', $admin_email, 'developer@edesign.com.sa');
}
function emailTemplateArb($order_id, $messages)
{
	$CI = & get_Instance();
    $CI->load->model('Model_orders');
	$result = $CI->Model_orders->get($order_id);
	$address = $CI->Model_cart_user_address->get($result->address_id);
	$products = $CI->Model_orders->getOrderProdLang($order_id, 'arb');
	$config = getCconfiguration();
	$total_amount=0;
	$prod = '';
	foreach($products as $product)
	{
		$images = getProductImages($product->id);
		$prod .= '<tr>
              <td><table width="400" cellpadding="0" cellspacing="0" border="0" style="border-bottom: 1px solid #eee; margin: auto;">
                  <tr>
                    <td align="left" valign="middle" width="80%">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr>
							  <td align="right" valign="middle" style="font-size:12px; color:#777; padding-bottom:3px"><strong>وصف:</strong> '.$product->arb_description.'</td>
							</tr>
							<tr>
							  <td align="right" valign="middle" style="font-size:12px; color:#777; padding-bottom:3px"><strong>الكمية:</strong> '.$product->quantity.'</td>
							</tr>';
		if($product->varient_id != '')
		  {
			  $var_arr = explode('|', $product->varient_id);
			  $var_val_arr = explode('|', $product->varient_value_id);
			  $i = 0;
			  foreach($var_arr as $varid)
			  {
				  $var = getVariantById($varid);
				  $var_val = getVariantValueById($var_val_arr[$i]);
				  $prod .= '<tr>
                          <td align="right" valign="middle" style="font-size:12px; color:#777; padding-bottom:3px"><strong>'.$var->arb_name.':</strong> '.$var_val->arb_value.'</td>
                        </tr>';
			  $i++;
			  }
		  }
		$prod .= '<tr>
							  <td align="right" valign="middle" style="font-size:12px; color:#777;"><strong>السعر:</strong> <span style="color: #D80000;">'.getPriceCur($product->quantity*$product->price).'</span></td>
							</tr>
                      	</table>
					 </td>
					  <td align="center" valign="top" width="20%"><img src="'.base_url().'uploads/images/products/'.$images[0]['arb_image'].'" alt="" width="76" height="88" style="display:inherit;"></td>
                  </tr>
                </table></td>
            </tr>';
		$total_amount += ($product->quantity*$product->price);
	}
	$html = '<!doctype html>
						<!--Quite a few clients strip your Doctype out, and some even apply their own. Many clients do honor your doctype and it can make things much easier if you can validate constantly against a Doctype.-->
						<html>
						<head>
						<meta charset="utf-8">
						<meta name="viewport" content="width=device-width, initial-scale=1.0">
						
						<!-- Please use an inliner tool to convert all CSS to inline as inpage or external CSS is removed by email clients -->
						<!-- When use in Email please remove all comments as it is removed by Email clients-->
						<!-- important in CSS is used to prevent the styles of currently inline CSS from overriding the ones mentioned in media queries when corresponding screen sizes are encountered -->
						
						<style type="text/css">
						body {
							color: #777;
							padding: 0;
							margin: 0;
						}
						body, table, td, p, a, li, blockquote {
							-webkit-text-size-adjust: none !important;
							font-style: normal;
							font-weight: 400;
							font-family: \'Roboto\', sans-serif;
						}
						@import \'https://fonts.googleapis.com/css?family=Roboto\';
						@import \'url(http://fonts.googleapis.com/earlyaccess/notosanskannada.css)\';
						button {
							width: 90%;
						}
						p {
							font-size: 13px !important;
							color: #777 !important;
							line-height: normal !important;
							padding-bottom: 14px !important;
							margin: 0px !important;
						}
						
						@media screen and (max-width:600px) {
						/*styling for objects with screen size less than 600px; */
						body, table, td, p, a, li, blockquote {
							-webkit-text-size-adjust: none!important;
						}
						table {
							/* All tables are 100% width */
							width: 100%;
						}
						.footer {
							/* Footer has 2 columns each of 48% width */
							height: auto !important;
							max-width: 48% !important;
							width: 48% !important;
						}
						table.responsiveImage {
							/* Container for images in catalog */
							height: auto !important;
							max-width: 30% !important;
							width: 30% !important;
						}
						table.responsiveContent {
							/* Content that accompanies the content in the catalog */
							height: auto !important;
							max-width: 66% !important;
							width: 66% !important;
						}
						.top {
							/* Each Columnar table in the header */
							height: auto !important;
							max-width: 48% !important;
							width: 48% !important;
						}
						.catalog {
							margin-left: 0%!important;
						}
						}
						
						@media screen and (max-width:480px) {
						/*styling for objects with screen size less than 480px; */
						body, table, td, p, a, li, blockquote {
						}
						table {
							/* All tables are 100% width */
							width: 100% !important;
							border-style: none !important;
						}
						.footer {
							/* Each footer column in this case should occupy 96% width  and 4% is allowed for email client padding*/
							height: auto !important;
							max-width: 96% !important;
							width: 96% !important;
						}
						.table.responsiveImage {
							/* Container for each image now specifying full width */
							height: auto !important;
							max-width: 96% !important;
							width: 96% !important;
						}
						.table.responsiveContent {
							/* Content in catalog  occupying full width of cell */
							height: auto !important;
							max-width: 96% !important;
							width: 96% !important;
						}
						.top {
							/* Header columns occupying full width */
							height: auto !important;
							max-width: 100% !important;
							width: 100% !important;
						}
						.catalog {
							margin-left: 0%!important;
						}
						button {
							width: 90%!important;
						}
						}
						
						table a {color:#d80000 !important;text-decoration:none !important;}
						</style>
						</head>
						<body style="direction:rtl">
						<table width="100%" cellspacing="0" cellpadding="0">
						  <tbody>
							<tr>
							  <td><table width="500"  align="center" cellpadding="0" cellspacing="0" style="background:url(images/header-top.jpg) repeat-x top left; width=100%;max-width:500px;">
								  <!-- Main Wrapper Table with initial width set to 60opx -->
								  <tbody>
									<tr>
									  <td><table class="top" width="100%"  align="left" cellpadding="0" cellspacing="0" style="padding:35px 0 53px;">
										  <!-- First header column with Logo -->
										  <tbody>
											<tr>
											  <td align="center" valign="middle"><a href="'.base_url().'"><img src="'.base_url().'assets/frontend/images/logo.png" alt="eDesign"></a></td>
											</tr>
										  </tbody>
										</table></td>
									</tr>
									<tr>
									  <td><table width="100%" align="left"  cellpadding="0" cellspacing="0" >
										  <tr>
											<td align="center" valign="middle" style="font-size:13px; font-weight: 500; padding-bottom: 22px;">مرحبا {name},</td>
										  </tr>
										  {thankyou_msg}
										  <tr> 
											<!-- HTML Spacer row -->
											<td><table width="100%" align="left"  cellpadding="0" cellspacing="0">
												<tr>
												  <td align="center" valign="middle" style="font-size: 13px; font-weight: normal; color:#E00513; padding: 35px 0 18px;">'.$config->arb_email_success_message.' </td>
												</tr>
											  </table></td>
										  </tr>
										  <tr>
											<td align="center" valign="middle" style="font-size:13px; color: #777; padding-bottom: 38px;">طلب رقم - '.str_pad($order_id, 10, "0", STR_PAD_LEFT).' في '.date("F jS Y h:i a", strtotime($result->created_at)).'.</td>
										  </tr>
										</table></td>
									</tr>
									'.$prod.'
									<tr>
									  <td><table width="400" cellpadding="0" cellspacing="0" border="0" style="border-bottom: 1px solid #eee; margin: auto; padding-top: 35px;">
										  <tr>
											<td></td>
										  </tr>
										</table></td>
									</tr>
									<tr>
									  <td><table width="400" cellpadding="0" cellspacing="0" border="1" style="border:0px; border-bottom: 1px solid rgba(119, 119, 119, 0.51); margin:  0 auto 30px; padding: 35px 0 0;">
										  <tr>
											<td align="right" valign="top" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px; font-weight: normal; color:#777; text-transform:uppercase; padding-bottom:3px;width: 111px;">طريقة الدفع:</td>
											<td align="left" valign="top" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-right:0; padding: 10px 5px;font-size:12px; font-weight: normal; color:#d80000; text-transform:uppercase; padding-bottom:3px;">';
											if($result->payment_method == 'VISA')
											{
												$html .= 'VISA';
											}
											elseif($result->payment_method == 'SADAD')
											{
												$html .= 'سداد';
											}
											else
											{
												$html .= 'الدفع عند التوصيل';
											}
											$html .= '</td>
										  </tr>
										  <tr>
											<td align="right" valign="top" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px; font-weight: normal; color:#777; text-transform:uppercase; padding-bottom:3px;width: 111px;">مجموع السعر:</td>
											<td align="left" valign="top" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-right:0; padding: 10px 5px;font-size:12px; font-weight: normal; color:#d80000; text-transform:uppercase; padding-bottom:3px;">'.getPriceCur($total_amount).'</td>
										  </tr>
										  <tr>
											<td align="right" valign="top" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px; font-weight: normal; color:#777; text-transform:uppercase; padding-bottom:3px;width: 111px;"> رسوم المتجر :</td>
											<td align="left" valign="top" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-right:0; padding: 10px 5px;font-size:12px; font-weight: normal; color:#d80000; text-transform:uppercase; padding-bottom:3px;">'.getPriceCur('0').'</td>
										  </tr>
										  
										  <tr>
											<td align="right" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; width: 111px;"><strong>مجموع الطلب الكلي:</strong></td>
											<td align="left" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-right:0; padding: 10px 5px;font-size:12px;font-weight: normal; color:#D80000; text-transform:uppercase; ">'.getPriceCur($total_amount).'</td>
										  </tr>
										  
										</table></td>
									</tr>
									
									<tr>
									  <td><table width="400" cellpadding="0" cellspacing="0" border="1" style="border:0px; border-bottom: 1px solid rgba(119, 119, 119, 0.51); margin:  0 auto 30px; padding: 35px 0 0;">
									  
									  
										  <tr>
											<td align="right" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; padding-bottom:3px;width: 111px;"><strong>البريد الإلكتروني:</strong></td>
											<td align="left" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-right:0; padding: 10px 5px;font-size:12px;font-weight: normal; color:#d80000; text-transform:uppercase; padding-bottom:3px; unicode-bidi: plaintext;">'.$address->email.'</td>
										  </tr>
										  
										  <tr>
											<td align="right" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; padding-bottom:3px;width: 111px;"><strong>المدينة:</strong></td>
											<td align="left" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-right:0; padding: 10px 5px;font-size:12px;font-weight: normal; color:#d80000; text-transform:uppercase; padding-bottom:3px; unicode-bidi: plaintext;">'.$address->city.'</td>
										  </tr>
										  
										  <tr>
											<td align="right" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; padding-bottom:3px;width: 111px;"><strong>البلد:</strong></td>
											<td align="left" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-right:0; padding: 10px 5px;font-size:12px;font-weight: normal; color:#d80000; text-transform:uppercase; padding-bottom:3px; unicode-bidi: plaintext;">'.$address->country.'</td>
										  </tr>
										  
										  <tr>
											<td align="right" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; padding-bottom:3px;width: 111px;"><strong>المنطقة:</strong></td>
											<td align="left" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-right:0; padding: 10px 5px;font-size:12px;font-weight: normal; color:#d80000; text-transform:uppercase; padding-bottom:3px; unicode-bidi: plaintext;">'.$address->region.'</td>
										  </tr>
										  
										  <tr>
											<td align="right" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; padding-bottom:3px;width: 111px;"><strong>الرمز البريدي:</strong></td>
											<td align="left" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-right:0; padding: 10px 5px;font-size:12px;font-weight: normal; color:#d80000; text-transform:uppercase; padding-bottom:3px; unicode-bidi: plaintext;">'.$address->zip_code.'</td>
										  </tr>
										  
										  <tr>
												<td align="right" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; padding-bottom:3px;width: 111px;"><strong>رقم التواصل:</strong></td>
												<td align="left" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-right:0; padding: 10px 5px;font-size:12px;font-weight: normal; color:#d80000; text-transform:uppercase; padding-bottom:3px; unicode-bidi: plaintext;">'.$address->phone_no.'</td>
											  </tr>
											
									  
									</table></td>
									</tr>
									<tr>
									  <td><table width="400" cellpadding="0" cellspacing="0" border="0" style="border-bottom: 1px solid #eee; margin: auto; padding: 20px 0 18px;">
										  <tr>
											<td colspan="2" align="right" valign="middle" style="font-size:12px;font-weight: normal; color:#777; text-transform:uppercase; padding-bottom:15px;"><strong>العنوان: </strong></td>
										  </tr>
										  <tr>
											<td colspan="2"  align="right" valign="middle" style="font-size:12px;font-weight: bold; color:#E00513; text-transform:inherit; padding-bottom:3px;">'.$address->full_name.'</td>
										  </tr>
										  <tr>
											<td colspan="2"  align="right" valign="middle" style="font-size:12px;font-weight: normal; color:#777; text-transform:inherit;">'.$address->address_1.'</td>
										  </tr>
										  
										</table></td>
									</tr>
									
									
									<tr>
										<td align="center" valign="middle" style="font-size:13px; padding: 46px 0 18px;">'.$messages['email_template_footer_need_more_help'].'</td>
									</tr>
									<tr>
										<td align="center" valign="middle" style="font-size:11px; padding: 0px 0 15px;">'.$messages['email_template_footer_service_team'].' <span style="direction: ltr; unicode-bidi: bidi-override;" > '.$config->phone.'</span> '.$messages['email_template_or'].'<br>'.$messages['chat_email'].' '.$config->email.'</td>
									</tr>
									<tr>
										<td align="center" valign="middle" style="font-size:11px; padding: 0px 0 50px;">'.$messages['email_template_thank_you_for_shopping'].'</td>
									</tr>
								  </tbody>
								  <tfoot bgcolor="#333">
									<tr>
										<td colspan="2">
											<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-top: 1px solid #f1f1f1; padding: 20px 30px 46px;">
												<tr>
													<td align="center">
													
														<span style="font-size:11px; color:#fff;">'.$messages['login_follow_us'].'</span>
														<a href="'.$config->facebook_link.'" target="_blank" style="display:inline-block; margin-right:14px;"><img src="'.base_url().'assets/frontend/images/facebook-icon.png" alt=""></a>
														<a href="'.$config->twitter_link.'" target="_blank" style="display:inline-block; margin-right:14px;"><img src="'.base_url().'assets/frontend/images/twitter-icon.png" alt=""></a>
														<a href="'.$config->instagram_link.'" target="_blank" style="display:inline-block; margin-right:14px;"><img src="'.base_url().'assets/frontend/images/instagram-icon.png" alt=""></a>
														<a href="'.$config->snapchat_link.'" target="_blank" style="display:inline-block; margin-right:14px;"><img src="'.base_url().'assets/frontend/images/snapchat-icon.png" alt=""></a>
														<a href="'.$config->youtube_link.'" target="_blank" style="display:inline-block; margin-right:16px;"><img src="'.base_url().'assets/frontend/images/youtube-icon.png" alt=""></a>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td>
											<table width="100%" cellpadding="0" cellspacing="0" border="0" style="padding: 0px 30px 24px;">
												<tr>
													<td align="right" valign="middle" style="font-family: \'Noto Sans Kannada\', sans-serif; font-size:10px; color: #fff;">جميع الحقوق محفوظة I eDesign '.date('Y').'</td>
													<td align="left" valign="middle"><a href="'.base_url().'" target="_balnk" style="font-size:10px; color: #ffffff; text-decoration: none;">'.$messages['design_visit_our_store'].'</a></td>
												</tr>
											</table>
										</td>
									</tr>
								  </tfoot>
								</table></td>
							</tr>
						  </tbody>
						</table>
						</body>
						</html>
					';
					$thankyou_msg = '<tr>
											<td align="center" valign="middle" style="font-family: \'Roboto\', sans-serif; font-weight: 100; font-size: 22px; color: #E00513; padding-bottom: 16px;"> '.$config->arb_email_title.' </td>
										  </tr>
										  <tr>
											<td align="center" valign="middle" style="">'.$config->arb_email_description.'</td>
										  </tr>';
					$customer = str_replace('{name}',$address->full_name, $html);
					$customer = str_replace('{thankyou_msg}', $thankyou_msg, $customer);
					htmlmail($address->email, 'طلب', $customer, 'developer@edesign.com.sa');
					$admin_email = str_replace('{name}','مشرف', $html);
					$admin_email = str_replace('{thankyou_msg}','', $admin_email);
					htmlmail($config->admin_email, 'طلب', $admin_email, 'developer@edesign.com.sa');
}
function emailOrderStatus($order_id, $status)
{
	$CI = & get_Instance();
    $CI->load->model('Model_orders');
	$result = $CI->Model_orders->get($order_id);
	$config = getCconfiguration();
	$total_amount=0;
	if($status == 0)
	{
		$ord_status = '<td align="center" valign="middle" style="font-family: \'Roboto\', sans-serif; font-weight: 100; font-size: 18px; color: #E00513; padding-bottom: 16px;">Sorry your Order No - '.str_pad($order_id, 6, "0", STR_PAD_LEFT).' on '.date("F jS Y h:i a", strtotime($result->created_at)).' is cancelled. Please try again later.</td>';
	}
	else if($status == 2)
	{
		$ord_status = '<td align="center" valign="middle" style="font-family: \'Roboto\', sans-serif; font-weight: 100; font-size: 18px; color: #E00513; padding-bottom: 16px;"> Your Order No - '.str_pad($order_id, 6, "0", STR_PAD_LEFT).' on '.date("F jS Y h:i a", strtotime($result->created_at)).' has been delivered. Thank you for shopping with us. </td>';
	}
	$prod = '';
	$html = '<!doctype html>
					<!--Quite a few clients strip your Doctype out, and some even apply their own. Many clients do honor your doctype and it can make things much easier if you can validate constantly against a Doctype.-->
					<html>
					<head>
					<meta charset="utf-8">
					<meta name="viewport" content="width=device-width, initial-scale=1.0">
					
					<!-- Please use an inliner tool to convert all CSS to inline as inpage or external CSS is removed by email clients -->
					<!-- When use in Email please remove all comments as it is removed by Email clients-->
					<!-- important in CSS is used to prevent the styles of currently inline CSS from overriding the ones mentioned in media queries when corresponding screen sizes are encountered -->
					
					<style type="text/css">
					body {
						color: #777;
						padding: 0;
						margin: 0;
					}
					body, table, td, p, a, li, blockquote {
						-webkit-text-size-adjust: none !important;
						font-style: normal;
						font-weight: 400;
						font-family: \'Roboto\', sans-serif;
					}
					@import \'https://fonts.googleapis.com/css?family=Roboto\';
					@import \'url(http://fonts.googleapis.com/earlyaccess/notosanskannada.css)\';
					button {
						width: 90%;
					}
					p {
						font-size: 13px !important;
						color: #777 !important;
						line-height: normal !important;
						padding-bottom: 14px !important;
						margin: 0px !important;
					}
					
					@media screen and (max-width:600px) {
					/*styling for objects with screen size less than 600px; */
					body, table, td, p, a, li, blockquote {
						-webkit-text-size-adjust: none!important;
					}
					table {
						/* All tables are 100% width */
						width: 100%;
					}
					.footer {
						/* Footer has 2 columns each of 48% width */
						height: auto !important;
						max-width: 48% !important;
						width: 48% !important;
					}
					table.responsiveImage {
						/* Container for images in catalog */
						height: auto !important;
						max-width: 30% !important;
						width: 30% !important;
					}
					table.responsiveContent {
						/* Content that accompanies the content in the catalog */
						height: auto !important;
						max-width: 66% !important;
						width: 66% !important;
					}
					.top {
						/* Each Columnar table in the header */
						height: auto !important;
						max-width: 48% !important;
						width: 48% !important;
					}
					.catalog {
						margin-left: 0%!important;
					}
					}
					
					@media screen and (max-width:480px) {
					/*styling for objects with screen size less than 480px; */
					body, table, td, p, a, li, blockquote {
					}
					table {
						/* All tables are 100% width */
						width: 100% !important;
						border-style: none !important;
					}
					.footer {
						/* Each footer column in this case should occupy 96% width  and 4% is allowed for email client padding*/
						height: auto !important;
						max-width: 96% !important;
						width: 96% !important;
					}
					.table.responsiveImage {
						/* Container for each image now specifying full width */
						height: auto !important;
						max-width: 96% !important;
						width: 96% !important;
					}
					.table.responsiveContent {
						/* Content in catalog  occupying full width of cell */
						height: auto !important;
						max-width: 96% !important;
						width: 96% !important;
					}
					.top {
						/* Header columns occupying full width */
						height: auto !important;
						max-width: 100% !important;
						width: 100% !important;
					}
					.catalog {
						margin-left: 0%!important;
					}
					button {
						width: 90%!important;
					}
					}
					</style>
					</head>
					<body>
					<table width="100%" cellspacing="0" cellpadding="0">
					  <tbody>
						<tr>
						  <td><table width="500"  align="center" cellpadding="0" cellspacing="0" style="background:url(images/header-top.jpg) repeat-x top left; width=100%;max-width:500px;">
							  <!-- Main Wrapper Table with initial width set to 60opx -->
							  <tbody>
								<tr>
								  <td><table class="top" width="100%"  align="left" cellpadding="0" cellspacing="0" style="padding:35px 0 53px;">
									  <!-- First header column with Logo -->
									  <tbody>
										<tr>
										  <td align="center" valign="middle"><a href="'.base_url().'"><img src="'.base_url().'assets/frontend/images/logo.png" alt="eDesign"></a></td>
										</tr>
									  </tbody>
									</table></td>
								</tr>
								<tr>
								  <td><table width="100%" align="left"  cellpadding="0" cellspacing="0" >
									  <tr>
										<td align="center" valign="middle" style="font-size:13px; font-weight: 500; padding-bottom: 22px;">Hi '.$result->first_name.' '.$result->last_name.',</td>
									  </tr>
									  <tr>
										'.$ord_status.'
									  </tr>
									  
									</table></td>
								</tr>
								
							  </tbody>
							  <tfoot bgcolor="#333">
								<tr>
									<td colspan="2">
										<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-top: 1px solid #f1f1f1; padding: 20px 30px 46px;">
											<tr>
												<td>
													<a href="'.$config->facebook_link.'" target="_blank" style="display:inline-block; margin-right:14px;"><img src="'.base_url().'assets/frontend/images/facebook-icon.png" alt=""></a>
													<a href="'.$config->twitter_link.'" target="_blank" style="display:inline-block; margin-right:14px;"><img src="'.base_url().'assets/frontend/images/twitter-icon.png" alt=""></a>
													<a href="'.$config->instagram_link.'" target="_blank" style="display:inline-block; margin-right:14px;"><img src="'.base_url().'assets/frontend/images/instagram-icon.png" alt=""></a>
													<a href="'.$config->snapchat_link.'" target="_blank" style="display:inline-block; margin-right:14px;"><img src="'.base_url().'assets/frontend/images/snapchat-icon.png" alt=""></a>
													<a href="'.$config->youtube_link.'" target="_blank" style="display:inline-block; margin-right:16px;"><img src="'.base_url().'assets/frontend/images/youtube-icon.png" alt=""></a>
													<span style="font-size:11px; color:#fff;">Follow us</span>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<table width="100%" cellpadding="0" cellspacing="0" border="0" style="padding: 0px 30px 24px;">
											<tr>
												<td align="left" valign="middle" style="font-family: \'Noto Sans Kannada\', sans-serif; font-size:10px; color: #fff;">All rights reserved '.date('Y').' | eDesign</td>
												<td align="right" valign="middle"><a href="'.base_url().'" target="_balnk" style="font-size:10px; color: #ffffff; text-decoration: none;">Visit our online store</a></td>
											</tr>
										</table>
									</td>
								</tr>
							  </tfoot>
							</table></td>
						</tr>
					  </tbody>
					</table>
					</body>
					</html>';
					htmlmail($result->email, 'Order Status', $html, 'developer@edesign.com.sa');
}
function questionEmail($question, $product_id, $quetion_id)
{
	$CI = & get_Instance();
	$lang = $CI->session->userdata('site_lang');
	$messages = $CI->lang->line('all');
	$config = getCconfiguration();
	if($lang == 'arb')
	{
		$body_style = 'style="direction:rtl"';
		$left = 'right';
		$right = 'left';
		$td_phone_style = ' unicode-bidi: plaintext;';
	}
	else
	{
		$body_style = '';
		$left = 'left';
		$right = 'right';
		$td_phone_style = '';
	}
	$CI->load->model('Model_product');
	$product = $CI->Model_product->get($product_id, true);
	$images = getProductImages($product['id']);
	$prod .= '<tr>
		  <td><table width="400" cellpadding="0" cellspacing="0" border="0" style="border-bottom: 1px solid #eee; margin: auto;">
			  <tr>
				  <td align="left" valign="middle" width="80%"><table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
					  <td align="left" valign="middle" style="font-size:12px; color:#777; padding-bottom:3px">'.$product[$lang.'_description'].'</td>
					</tr>
				  </table></td>
				  <td align="center" valign="top" width="20%"><img src="'.base_url().'uploads/images/products/'.$images[0]['eng_image'].'" alt="" width="76" height="88" style="display:inherit;"></td>
			  </tr>
			</table></td>
		</tr>';
	$html = '<html>
					<head>
					<!-- Please use an inliner tool to convert all CSS to inline as inpage or external CSS is removed by email clients -->
					<!-- When use in Email please remove all comments as it is removed by Email clients-->
					<!-- important in CSS is used to prevent the styles of currently inline CSS from overriding the ones mentioned in media queries when corresponding screen sizes are encountered -->
					
					<style type="text/css">
					body {
						color: #777;
						padding:0;
						margin: 0;
					}
					body, table, td, p, a, li, blockquote {
						-webkit-text-size-adjust: none !important;
						font-style: normal;
						font-weight: 400;
						font-family: \'Roboto\', sans-serif;
					}
					@import \'https://fonts.googleapis.com/css?family=Roboto\';
					@import \'url(http://fonts.googleapis.com/earlyaccess/notosanskannada.css)\';
					button {
						width: 90%;
					}
					p {
						font-size: 13px !important;
						color: #777 !important;
						line-height: normal !important;
						padding-bottom: 14px !important;
						margin: 0px !important;
					}
					@media screen and (max-width:600px) {
					/*styling for objects with screen size less than 600px; */
					body, table, td, p, a, li, blockquote {
						-webkit-text-size-adjust: none!important;
					}
					table {
						/* All tables are 100% width */
						width: 100%;
					}
					.footer {
						/* Footer has 2 columns each of 48% width */
						height: auto !important;
						max-width: 48% !important;
						width: 48% !important;
					}
					table.responsiveImage {
						/* Container for images in catalog */
						height: auto !important;
						max-width: 30% !important;
						width: 30% !important;
					}
					table.responsiveContent {
						/* Content that accompanies the content in the catalog */
						height: auto !important;
						max-width: 66% !important;
						width: 66% !important;
					}
					.top {
						/* Each Columnar table in the header */
						height: auto !important;
						max-width: 48% !important;
						width: 48% !important;
					}
					.catalog {
						margin-left: 0%!important;
					}
					}
					
					@media screen and (max-width:480px) {
					/*styling for objects with screen size less than 480px; */
					body, table, td, p, a, li, blockquote {
					}
					table {
						/* All tables are 100% width */
						width: 100% !important;
						border-style: none !important;
					}
					.footer {
						/* Each footer column in this case should occupy 96% width  and 4% is allowed for email client padding*/
						height: auto !important;
						max-width: 96% !important;
						width: 96% !important;
					}
					.table.responsiveImage {
						/* Container for each image now specifying full width */
						height: auto !important;
						max-width: 96% !important;
						width: 96% !important;
					}
					.table.responsiveContent {
						/* Content in catalog  occupying full width of cell */
						height: auto !important;
						max-width: 96% !important;
						width: 96% !important;
					}
					.top {
						/* Header columns occupying full width */
						height: auto !important;
						max-width: 100% !important;
						width: 100% !important;
					}
					.catalog {
						margin-left: 0%!important;
					}
					button {
						width: 90%!important;
					}
					}
					table a {color:#d80000 !important;text-decoration:none !important;}
					</style>
					</head>
					<body '.$body_style.'>
					<table width="100%" cellspacing="0" cellpadding="0">
					  <tbody>
						<tr>
						  <td>
							<table width="500"  align="center" cellpadding="0" cellspacing="0" style="background:url(images/header-top.jpg) repeat-x top left;">
							  <!-- Main Wrapper Table with initial width set to 60opx -->
							  <tbody>
								<tr>
								  <td>
										<table class="top" width="100%"  align="left" cellpadding="0" cellspacing="0" style="padding:35px 0 53px;">
										  <!-- First header column with Logo -->
										  <tbody>
											<tr>
											  <td align="center" valign="middle"><a href="'.base_url().'" target="_blank"><img src="'.base_url().'assets/frontend/images/logo.png" alt="eDesign"></a></td>
											</tr>
										  </tbody>
									  </table>
					
								  </td>
								</tr>
								<tr>
								  <td>
										<table width="100%" align="left"  cellpadding="0" cellspacing="0" >
									  <tr>
										<td align="center" valign="middle" style="font-size:13px; font-weight: 500; padding-bottom: 22px;">'.$messages['email_template_hi'].' '.$messages['com_admin'].',</td>
									  </tr>
									  <tr>
										<td align="center" valign="middle" style="font-family: \'Roboto\', sans-serif; font-weight: 100; font-size: 22px; color: #E00513; padding-bottom: 16px;">
											'.$messages['products_page_customer_questions'].'
										</td>
									  </tr>
									</table>
								  </td>
								</tr>
								'.$prod.'
								<tr>
									<td>
										<table width="400" cellpadding="0" cellspacing="0" border="1" style="border:0px; border-bottom: 1px solid rgba(119, 119, 119, 0.51); margin: 0 auto 30px; padding: 35px 0 0; ">
											<tr>
												<td align="'.$left.'" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase;"><strong>'.$messages['products_page_question'].'</strong></td>
												<td align="'.$right.'" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-'.$left.':0; padding: 10px 5px;font-size:12px;font-weight: normal; color:#777; ">'.$question.'</td>
											</tr>
											<tr>
												<td align="'.$left.'" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0;padding: 10px 5px;font-size:12px;font-weight: normal; color:#777; text-transform:uppercase;"><strong>'.$messages['products_page_answer'].'</strong></td>
												<td align="'.$right.'" valign="middle" style="border: 1px solid rgba(119, 119, 119, 0.51);border-bottom: 0; border-'.$left.':0; padding: 10px 5px;font-size:12px;font-weight: normal; color:#777; ">'.base_url().'admin/customerQuestions/answer/'.$quetion_id.'</td>
											</tr>											
										</table>
									</td>
								</tr>
								<tr>
									<td align="center" valign="middle" style="font-size:13px; padding: 46px 0 18px;">'.$messages['email_template_footer_need_more_help'].'</td>
								</tr>
								<tr>
									<td align="center" valign="middle" style="font-size:11px; padding: 0px 0 15px;">'.$messages['email_template_footer_service_team'].' <span style="'.$td_phone_style.'"> '.$config->phone.' </span> '.$messages['email_template_or'].'<br>'.$messages['chat_email'].' '.$config->email.'</td>
								</tr>
								<tr>
									<td align="center" valign="middle" style="font-size:11px; padding: 0px 0 50px;">'.$messages['email_template_thank_you_for_shopping'].'</td>
								</tr>
							  </tbody>
							  <tfoot bgcolor="#333">
									<tr>
										<td colspan="2">
											<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-top: 1px solid #f1f1f1; padding: 20px 55px 46px;">
												<tr>
													<td style="direction: '.($lang == 'arb' ? 'ltr' : 'rtl').';" align="center">
														<span style="font-size:11px; color:#fff;">'.$messages['login_follow_us'].'</span>
														<a href="'.$config->facebook_link.'" target="_blank" style="display:inline-block; margin-right:14px;"><img src="'.base_url().'assets/frontend/images/facebook-icon.png" alt=""></a>
														<a href="'.$config->twitter_link.'" target="_blank" style="display:inline-block; margin-right:14px;"><img src="'.base_url().'assets/frontend/images/twitter-icon.png" alt=""></a>
														<a href="'.$config->instagram_link.'" target="_blank" style="display:inline-block; margin-right:14px;"><img src="'.base_url().'assets/frontend/images/instagram-icon.png" alt=""></a>
														<a href="'.$config->snapchat_link.'" target="_blank" style="display:inline-block; margin-right:14px;"><img src="'.base_url().'assets/frontend/images/snapchat-icon.png" alt=""></a>
														<a href="'.$config->youtube_link.'" target="_blank" style="display:inline-block; margin-right:16px;"><img src="'.base_url().'assets/frontend/images/youtube-icon.png" alt=""></a>
														
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td>
											<table width="100%" cellpadding="0" cellspacing="0" border="0" style="padding: 0px 55px 24px;">
												<tr>
													<td align="'.$left.'" valign="middle" style="font-family: \'Noto Sans Kannada\', sans-serif; font-size:11px; color: #fff;">'.($lang == 'arb' ? 'جميع الحقوق محفوظة I eDesign '.date('Y').'' : 'All rights reserved '.date('Y').' | eDesign').'</td>
													<td align="'.$right.'" valign="middle"><a href="'.base_url().'" target="_balnk" style="font-size:11px; color: #ffffff; text-decoration: none;">'.$messages['design_visit_our_store'].'</a></td>
												</tr>
											</table>
										</td>
									</tr>
								  </tfoot>
							</table>
							</td>
						</tr>
					  </tbody>
					</table>
					</body>
					</html>';
			htmlmail($config->email, $messages['products_page_customer_questions'], $html, 'developer@edesign.com.sa');
}
function htmlmail($to, $subject, $body, $from = ''){
    require_once (APPPATH.'libraries/PHPMailer/class.phpmailer.php');
    $CI = & get_Instance();
    $CI->load->model('Model_settings');
    $smtp = $CI->Model_settings->get(1);
    $from = $smtp->smtp_email;

    $mail = new PHPMailer(true);
    //$mail->SMTPDebug = 1;
    $mail->CharSet = 'UTF-8';
    $mail->isSMTP();

    $mail->Host = $smtp->smtp_host;
    $mail->SMTPAuth = true;
    $mail->Username = $smtp->smtp_email;
    $mail->Password = $smtp->smtp_password;
    $mail->SMTPSecure = 'ssl';
    $mail->Port = $smtp->smtp_port;

    $mail->setFrom($from, 'KSC');
    if (strpos($to, ',') !== false) {
        $addresses = explode(',', $to);
        foreach ($addresses as $address) {
            $mail->AddAddress($address);
        }
    }else {
       $mail->addAddress($to);
    }
    $mail->isHTML(true);
    $mail->Subject = $subject;
    $mail->Body = $body;
    $mail->send();
    return true;
}
function randomPassword($size = '8') {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < $size; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}
function convertToArabic($string) {
    $persian = array('۰', '۱', '۲', '۳', '٤', '۵', '٦', '۷', '۸', '۹');
    $num = range(0, 9);
    return str_replace($num, $persian, $string);
    return $string;
}
function lang_base_url(){
	$CI = & get_Instance();
	$lang = $CI->session->userdata('site_lang');
	$url = base_url();
	switch($lang){
		case "eng":
			$url = base_url().'en/';
		break;
	}
	return $url;
}
function countryDropdown($name, $selected = '')
{
	$CI = & get_Instance();
	$CI->load->model('Model_country');
	$fetch_prefered['prefered_country'] = 1;
	$result_prefered = $CI->Model_country->getMultipleRows($fetch_prefered);
	$fetch_by['prefered_country'] = 0;
	$result = $CI->Model_country->getMultipleRows($fetch_by);
	$html = '<select name="'.$name.'" id="countryCode">';
	foreach($result_prefered as $rp)
	{
		$html .= '<option data-countryCode="'.$rp->iso.'" '.($selected == $rp->phonecode ? 'selected' : '').' value="'.$rp->phonecode.'">'.$rp->nicename.' (+'.$rp->phonecode.')</option>';
	}
	$html .= '<optgroup label="Other countries">';
	foreach($result as $rs)
	{
		$html .= '<option data-countryCode="'.$rs->iso.'" '.($selected == $rs->phonecode ? 'selected' : '').' value="'.$rs->phonecode.'">'.$rs->nicename.' (+'.$rs->phonecode.')</option>';
	}
    $html .= '</optgroup></select>';
	return $html;
}
function getCurrentLatLong()
{
	$ipAddress = !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
    $ip_key = "9af6c695961fb8ccce5ae99cc4be5873f27302833282de5b89c30beb2d76a28b";
    $query = "http://api.ipinfodb.com/v3/ip-city/?key=" . $ip_key . "&ip=" . $ipAddress . "&format=json";
    $json = file_get_contents($query);
    $data = json_decode($json, true);
    if ($data['statusCode'] == "OK") {
        /*echo '<pre>';
        echo "IP Address: " . $ipAddress;
        echo "Country: " . $data['countryName'];
        echo "Region: " . $data['regionName'];
        echo "City: " . $data['cityName'];
        echo "Latitude: " . $data['latitude'];
        echo "Longitude: " . $data['longitude'];
        echo '</pre>';*/
		return 'latitude: '.$data['latitude'].',longitude: '.$data['longitude'].'';
    } else {
        //echo $data['statusCode']." ".$data['statusMessage'];
		return 'latitude: 21.2854,longitude: 39.2376';
    }
	/*$ip  = !empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
	$url = "http://freegeoip.net/json/$ip";
	$ch  = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
	return $data = curl_exec($ch);
	curl_close($ch);
	if ($data) {
		$location = json_decode($data);
		$lat = $location->latitude;
		$lon = $location->longitude;
		return 'latitude: '.$lat.',longitude: '.$lon.'';
		//$sun_info = date_sun_info(time(), $lat, $lon);
		//print_r($sun_info);
	}
	else
	{
		return 'latitude: 21.2854,longitude: 39.2376';
	}*/
}
function cartCount(){
    $CI = & get_Instance();
    $CI->load->model('Model_temp_orders');
	/*if ($CI->session->userdata('login') == false && !$CI->session->userdata('user_id'))
	{
		$user_id = get_cookie('u_id');
	}
	else
	{
		$user_id = $CI->session->userdata('user_id');
	}*/
	$user_id = get_cookie('u_id');
    $get_by = array();
    $get_by['user'] = $user_id;
    $get_by['saved'] = '0';
    $rows = $CI->Model_temp_orders->getRowCount($get_by);
    if($rows > 0) {
        $count = $rows;
    }else{
        $count = "";
    }
    return $count;
}
function checkProdQuantity($prod_id){
    $fetch_by = array();
    $CI = & get_Instance();
    $fetch_by['id'] = $prod_id;
    $quantity = $CI->Model_product->getSingleRow($fetch_by,true);
    return $quantity['eng_quantity'];
}
function getAllBranchesLocations(){
    $fetch_by = array();
    $CI = & get_Instance();
    $locations = $CI->Model_branches->getAll(true);
    return $locations;
}
    function getCategoriesForHeader(){
        $CI = & get_Instance();
        $CI->load->model('Model_category');
		$filtch_by['active'] = 'On';
        $result = $CI->Model_category->getMultipleRows($filtch_by,true,'asc','itme_order');
        return $result;
    }
    function social_links(){
        $CI = & get_Instance();
        $CI->load->model('Model_social_media');
        $result = $CI->Model_social_media->getAll();
        return $result;
    }
    function payment_methods(){
        $CI = & get_Instance();
        $CI->load->model('Model_payment_types');
        $result = $CI->Model_payment_types->get(1);
        return $result;
    }
	function getProductCategory($cat_id){
        $CI = & get_Instance();
        $CI->load->model('Model_category');
        $result = $CI->Model_category->get($cat_id,true);
        return $result;
    }


	function get_x_words($string, $x = 25)
	{
    $parts = explode(' ', $string);
    if (sizeof($parts) > $x) {
        $parts = array_slice($parts, 0, $x);
    }
    return implode(' ', $parts);
}
 function getOrderProducts($order_id){
		$CI =& get_Instance();
		$result = $CI->db->query('select p.id, p.eng_name, p.arb_name, op.quantity, op.price, op.user_id, op.guest from order_product op, products p where op.product_id = p.id and op.order_id = '.$order_id);
		if($result->num_rows() > 0){
		    return $result->result_array();
		}else{
			return false;
		}
	}
	function getUserAddress($address_id,$user_id){
		$CI =& get_Instance();
		$get_by['user_id'] = $user_id;
		$get_by['id'] = $address_id;
		$address = $CI->Model_cart_user_address->getSingleRow($get_by,true);
		if($address){
		    return $address;
		}else{
			return false;
		}
	}
function getUserRoleName($id)
{
    $CI =& get_Instance();
    $CI->load->model('Model_admin_roles');
    $row = $CI->Model_admin_roles->get($id);
    return $row->role;
}
function userRolesRights($page_id, $user_role_id)
{
    $data    = array();
    $results = array();
    $CI =& get_Instance();
    $CI->load->model('Model_page_rights');
    $results = $CI->Model_page_rights->fetchUserRightsRecord($page_id, $user_role_id);
    return $results;
}
function checkUserRights($user_role_id)
{
    $data    = array();
    $results = array();
    $CI =& get_Instance();
    $CI->load->model('Model_page_rights');
    $results = $CI->Model_page_rights->fetchUserPageExcess($user_role_id);
    return $results;
}
function admin_user($id)
{
    $CI =& get_Instance();
    $CI->load->model('Central_model');
    $result = $CI->Central_model->first("users", 'id', $id);
    return $result;
}
function user_rights()
{
	$CI =& get_Instance();
	$user_rights_array = array();
	$arrUser = $CI->session->userdata('user');
	$user_data = admin_user($arrUser['id']);
	$user_role_id = $user_data->user_type;
	$user_rights = checkUserRights($user_role_id);
	foreach($user_rights as $row)
	{
		$user_rights_array[$row['page_name']]['show_p'] = $row['show_p'];
		$user_rights_array[$row['page_name']]['add_p'] = $row['add_p'];
		$user_rights_array[$row['page_name']]['edit_p'] = $row['edit_p'];
		$user_rights_array[$row['page_name']]['delete_p'] = $row['delete_p'];
		$user_rights_array[$row['page_name']]['export_p'] = $row['export_p'];
		$user_rights_array[$row['page_name']]['block_p'] = $row['block_p'];
		$user_rights_array[$row['page_name']]['view_p'] = $row['view_p'];
		$user_rights_array[$row['page_name']]['import_p'] = $row['import_p'];
	}
	return $user_rights_array;
}
function getRatingByOrder($order_id,$user_id){
	$CI =& get_Instance();
	$get_by = array();
	$get_by['order_id'] = $order_id;
	$get_by['user_id'] = $user_id;
	$CI->load->model('Model_product_rating');
	$rating = $CI->Model_product_rating->getSingleRow($get_by,true);
	return $rating;
}
function changeNameToArb($title){
	$CI =& get_Instance();
	$lang = $CI->session->userdata('site_lang');
	if ($lang == "eng") {
            $title = $title;
        } else {
            if ($title == "full_name") {
                $title = "اسم";
            }
            if ($title == "name") {
                $title = "اسم";
            }
            if ($title == "country") {
                $title = "البلد";
            }
			if($title == "city" ) {
				$title = "المدينة";
			}
            if ($title == "email") {
                $title = "البريد الإلكتروني";
            }
			if ($title == "mobile") {
                $title = "رقم الجوال";
            }
			if ($title == "message") {
                $title = "رسالة";
            }
			if($title == "age") {
				$title = "العمر";
			}
			if($title == "experience") {
				$title = "سنوات الخبرة";
			}
			if($title == "cv") {
				$title = "السيرة الذاتية";
			}
        }
        return ucfirst(str_replace('_',' ',$title));
    }
	function spacesValidation($post){
		$data = strip_tags($post);
		$result = str_replace(array(' ','&nbsp;'), '', $data);
		//echo "<pre>"; var_dump(trim($eng_q)); exit;
		return trim($result);
	}
	function send_sms($order_id,$otherMsg = ""){
        require_once(APPPATH.'libraries/sms/api.php');
        require_once(APPPATH.'libraries/libphonenumber/vendor/autoload.php');
        $CI =& get_Instance();
        $CI->load->model('Model_registered_users');
        $CI->load->model('Model_cart_user_address');
        $CI->load->model('Model_orders');
        $CI->load->model('Model_settings');
        $order = $CI->Model_orders->get($order_id);
        if(!$CI->session->userdata('user_id') && $CI->session->userdata('login') == false)
        {
            $user = $CI->Model_registered_users->getSingleRow(array('id'=>$order->user_id));
            $user_id = $user->id;
        }
        else
        {
            $user_id = $CI->session->userdata('user_id');
            $reg_user = $CI->Model_registered_users->getSingleRow(array('id'=>$user_id));
            $mobile = $reg_user->mobile_no;
        }
        $user = $CI->Model_cart_user_address->get($order->address_id);
        if($mobile == '')
        {
            $mobile = $user->phone_no;
        }
        $country = getCodeByCountry($mobile);
        $mobile_no = str_replace(array('+',' '),'',$mobile);
        $orderNo = str_pad($order_id, 6, "0", STR_PAD_LEFT);
        $settings = $CI->Model_settings->get(1);
        $user_name = $settings->jawal_user_name;
        $password = $settings->jawal_password;
        $Originator = $settings->jawal_originator;
        $message = $CI->lang->line('all');
        // if message is not related to order placed then this snipped will be worked
       if($otherMsg == "") {
           $msg = $message['order_confirm_sms'] . " " . $orderNo." ".$message['aramex_track_no']." ".$order->tracking_id;
       }else{
           $msg = $otherMsg." ".$message['aramex_track_no']." ".$order->tracking_id;
       }
       //----end-------
        $Ucode = 'U';
        $m = $mobile_no;
        $check_number = check_number_country($m,$country);
        //echo($msg); exit;
        if ($check_number) {
             SendSms($user_name, $password, $mobile_no, $Originator, $msg, $Ucode);
            $s = "success";
        } else {
            $s = 'fail';
        }
        return $s;
    }
    function getCodeByCountry($mobile){
        // mobile number codes
        if (strpos($mobile, '+966') !== false){
            $country = "SA";
        }
        elseif(strpos($mobile, '+971') !== false){
            $country = "AE";
        }
        elseif(strpos($mobile, '+973') !== false){
            $country = "BH";
        }
        elseif(strpos($mobile, '+965') !== false){
            $country = "KW";
        }
        elseif(strpos($mobile, '+968') !== false){
            $country = "OM";
        }
        elseif(strpos($mobile, '+974') !== false){
            $country = "QA";
        }
        elseif(strpos($mobile, '+92') !== false){
            $country = "PK";
        }else{
            $country = "SA";
        }
        return $country;
    }
    function check_number_country($number,$country){
        require_once(APPPATH.'libraries/sms/api.php');
        require_once(APPPATH.'libraries/libphonenumber/vendor/autoload.php');
        $phoneNumberUtil = \libphonenumber\PhoneNumberUtil::getInstance();
        $phoneNumberObject = $phoneNumberUtil->parse($number, $country);
        return $phoneNumberUtil->isValidNumber($phoneNumberObject);
    }
    
    function getPriceByOffer($product_id){
        $CI =& get_Instance();
        $CI->load->model('Model_product');
        $product = $CI->Model_product->get($product_id);
        /*if($product->daily_offer == '1'){
            $currentData = date('Y-m-d');
            $start_date = date('Y-m-d',strtotime($product->start_date));
            $end_date = date('Y-m-d',strtotime($product->end_date));*/
        $discount_amount = $product->discount_amount;
        $orgPrice = $product->eng_price;
        $discount_type = $product->discount_type;
        if( $discount_type != ""){
                if($discount_type == 'amount'){
                    $price = $orgPrice - $discount_amount ;
                }else{
                    $discPrice = ($discount_amount / 100) * $orgPrice;
                    //$discPrice =   ($orgPrice*$discount_amount)/100;
                    $price = $orgPrice - $discPrice;
                }
        }else{
           $price = $product->eng_price;
        }
        return $price;
    }
    function sort_by_alphabet($array,$convert){
        $name = array();
        foreach ($array as $key => $row)
        {
            if($convert == 'city') {
                $name[$key] = $row['eng_name'];
            }else{
                $name[$key] = $row['eng_country_name'];
            }
        }
        array_multisort($name, SORT_ASC, $array);
        return $array;
    }
    function getShipmentDays($shipment_group,$shipment_type){
        $CI =& get_Instance();
        $lang = $CI->session->userdata('site_lang');
        $CI->load->model('Model_shipment_groups');
        $days = $CI->Model_shipment_groups->get($shipment_group, true);
        
        if($shipment_type == "normal"){
            if($lang != NULL){
                $day = $days[$lang.'_normal_shipment_days'];
            }else{
                $day = $days['eng_normal_shipment_days'];
            }
        }else{
            if($lang != NULL) {
                $day = $days[$lang.'_fast_shipment_days'];
            }else{
                $day = $days['eng_fast_shipment_days'];
            }
        }
        return $day;
    }
    function getOrderProductCount($order_id){
        $CI = & get_Instance();
        $CI->load->model('Model_order_product');
        $count = $CI->Model_order_product->getRowCount(array('order_id'=>$order_id));
        return $count;
    }
    function getProductsByOrder($order_id){
        $CI = & get_Instance();
        $CI->load->model('Model_orders');
        $products = $CI->Model_orders->getOrderProd($order_id);
        return $products;
    }
    function getPaymentStatus($status_id){
        $CI = & get_Instance();
        $CI->load->model('Model_order_status');
        $get_by['status_id'] = $status_id;
        $status = $CI->Model_order_status->getSingleRow($get_by);
        return $status->status;
    }
    
    function orderPagination($path,$total_rows){
        $CI = & get_Instance();
        $CI->load->library("pagination");
        $config['base_url'] = base_url().$path;
        $config['total_rows'] = $total_rows;
        //this is for filtering
        if (isset($_COOKIE['order_limits']) && $_COOKIE['order_limits'] != NULL) {
            $config['per_page'] = $_COOKIE['order_limits'];
        }else{
            $config['per_page'] = 10;
        }
        $config['page_query_string'] = TRUE;
        $config['full_tag_open'] = '<div class="uk-width-medium-5-10"><ul class="uk-pagination uk-text-right">';
        $config['full_tag_close'] = '</ul ></div>';
        $config['next_link'] = '<span>Next</span>';
        $config['prev_link'] = '<span>Previous</span>';
        $config['prev_tag_open'] = '<li class="prev page">';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li class="next page">';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="uk-active"><a href="">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page">';
        $config['num_tag_close'] = '</li>';
        $CI->pagination->initialize($config);
    return $config;
}
function productPagination($path,$total_rows){
    $CI = & get_Instance();
    $CI->load->library("pagination");
    $config['base_url'] = base_url().$path;
    $config['total_rows'] = $total_rows;
    //this is for filtering
    if (isset($_COOKIE['product_limits']) && $_COOKIE['product_limits'] != NULL) {
        $config['per_page'] = $_COOKIE['product_limits'];
    } else {
        $config['per_page'] = 8;
    }

    $config['page_query_string'] = TRUE;
    $config['full_tag_open'] = '<div class="uk-width-medium-5-10"><ul class="uk-pagination uk-text-right">';
    $config['full_tag_close'] = '</ul ></div>';
    $config['next_link'] = '<span>Next</span>';
    $config['prev_link'] = '<span>Previous</span>';
    $config['prev_tag_open'] = '<li class="prev page">';
    $config['prev_tag_close'] = '</li>';
    $config['next_tag_open'] = '<li class="next page">';
    $config['next_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="uk-active"><a href="">';
    $config['cur_tag_close'] = '</a></li>';
    $config['num_tag_open'] = '<li class="page">';
    $config['num_tag_close'] = '</li>';
    $CI->pagination->initialize($config);
    return $config;
}


function productFrontPagination($path,$total_rows,$limit=12){
    $CI = & get_Instance();
    $CI->load->library("pagination");
    $config['base_url'] = lang_base_url().$path;
    $config['total_rows'] = $total_rows;
    //this is for filtering

    $config['per_page'] = $limit;

    $config['page_query_string'] = TRUE;
    $config['full_tag_open'] = '<div class="page-navigation"><ul class="pagination">';
    $config['full_tag_close'] = '</ul></div>';
    $config['next_link'] = '<li class="next page">Next →</li>';
    $config['prev_link'] = '<li class="next page">← Previous</li>';
    $config['prev_tag_open'] = '<li class="page">';
    $config['prev_tag_close'] = '</li>';
    $config['next_tag_open'] = '<li class="page">';
    $config['next_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="active"><a href="">';
    $config['cur_tag_close'] = '</a></li>';
    $config['num_tag_open'] = '<li class="page">';
    $config['num_tag_close'] = '</li>';
    $CI->pagination->initialize($config);
    return $config;
}


function saveProductViewedStatus($id){
    $CI = & get_Instance();
    $CI->load->model('Model_product');
    $oldViews = $CI->Model_product->get($id);
    $viewed['viewed'] = $oldViews->viewed+1;
    $update_view['id'] = $id;
    $CI->Model_product->update($viewed,$update_view);
}
function redirectToProducts(){
    $CI = & get_Instance();
    $CI->load->model('Model_temp_orders');
    if(get_cookie('u_id'))
    {
        $user_id = get_cookie('u_id');
    }
    else
    {
        redirect(lang_base_url() . 'product');
    }
    $cart_data = $CI->Model_temp_orders->getMultipleRows(array('user'=>$user_id, 'saved'=>'0'),true);
    if(!$cart_data)
    {
        redirect(lang_base_url() . 'product');
    }
}
function getMenu($header,$sub_header = '',$footer_menu_type = 0){
    $CI = & get_Instance();
    $CI->load->model('Model_menu_bar');
    $getType['menu_type'] = $header;
    $getType['is_active'] = 1;

    /*for header menu*/
    if($header == 'header') {
        $getType['header_sub_menu'] = $sub_header;
        $getType['footer_menu_type'] = 0;
        if ($sub_header == 'yes') {
            $menu = $CI->Model_menu_bar->getMultipleRows($getType, true);
            /*$menu = array();
            foreach ($menus as $men) {
                $menu['menu']['sub'][] = $men;
            }
            $menu['menu']['sub_menu'] = 'sub_menu';*/
        }  if ($sub_header == 'no') {
            $getType['footer_menu_type'] = 0;
            $menu = $CI->Model_menu_bar->getMultipleRows($getType, true);
            //$menu['menu']['sub_menu'] = '';
        }
    }else{
        /*for footer menu*/
        $getType['footer_menu_type'] = $footer_menu_type;
        $menu = $CI->Model_menu_bar->getMultipleRows($getType, true);
    }

    return $menu;
}
// on off maintenance mode
function maintenanceMode($redirect = ''){
    $CI = & get_Instance();
    $CI->load->model('Model_maintenance_mode');
    $mode = $CI->Model_maintenance_mode->get('1');
    if($mode->active == 'on') {
        if ($redirect != '') {
            redirect(lang_base_url());
        }else{
            return $mode;
        }
    }else{
        return '';
    }
}
function getLeftMenu($type){
    $CI = & get_Instance();
    $lang = $CI->session->userdata('site_lang');
    $CI->load->model('Model_'.$type);
    $model = 'Model_'.$type;
    $menu = $CI->$model->get('1');
    if($lang == 'eng'){
        $title = $menu->eng_title;
    }else{
        $title = $menu->arb_title;
    }
    return $title;
}
function saveLogs($data){
    $CI = & get_Instance();
    $CI->load->model('Model_logs');
    $user = $CI->session->userdata('user');
    $data['user_id'] = $user['id'];
    $data['user_name'] = $user['name'];
    
    if($data['type'] == 'add'){
        $data['message'] = 'Added a Record';
    }
    if($data['type'] == 'update'){
        $data['message'] = 'Updated a Record';
    }
    if($data['type'] == 'delete'){
        $data['message'] = 'Deleted a Record';
    }
    if($data['type'] == 'export'){
        $data['message'] = 'Export a Record';
    }
    if($data['type'] == 'import'){
        $data['message'] = 'Import a Record';
    }
    $data['action_date'] = date('Y-m-d H:i:s');
    $CI->Model_logs->save($data);
    
}
function getAramexRate($address_id){
    /*error_reporting(E_ALL);
    ini_set('display_errors', '1');*/

    $CI = & get_Instance();
    $CI->load->model('Model_cart_user_address');
    $CI->load->model('Model_temp_orders');
    $CI->load->model('Model_product');
    $CI->load->model('Model_general');
    $CI->load->model('Model_settings');
    $lang = $CI->session->userdata('site_lang');
    $aramex_detail = $CI->Model_settings->get(1);

    $destAddr = $CI->Model_cart_user_address->getSingleRow(array('id'=>$address_id));
    $user_id = get_cookie('u_id');
    $cart_data = $CI->Model_temp_orders->getMultipleRows(array('user'=>$user_id, 'saved'=>'0'),true);
    $kg = 0;
    $lb = 0;
    $g = 0;
    $oz = 0;
    $totalQuan  = 0;
    foreach ($cart_data as $key => $cart){
        $getBy['id'] = $cart['p_id'];
        $prods = $CI->Model_product->getSingleRow($getBy);
        if($prods->weight_unit == 'kg') {
            $kg += $prods->weight_value;
        }if($prods->weight_unit == 'lb'){
            $lb += $prods->weight_value;
        }if($prods->weight_unit == 'g'){
            $g += $prods->weight_value;
        }if($prods->weight_unit == 'oz'){
            $oz += $prods->weight_value;
        }
        $totalQuan += $cart['quantity'];
    }
    $weight = weightConvertToKg($kg,$lb,$g,$oz);
    $city = getCityById($destAddr->city,'eng');
    $country = $CI->Model_general->getSingleRow('countries', array(
        'code' => $destAddr->country
    ));
    if($country->country_code == 'SA'){
        $ProductGroup = 'DOM';
        $ProductType = 'ONP';
    }else{
        $ProductGroup = 'EXP';
        $ProductType = 'PPX';
    }
    $params = array(
        'ClientInfo'  			=> array(
            'AccountCountryCode'	=> $aramex_detail->aramex_country_code,
            'AccountEntity'		 	=>  $aramex_detail->aramex_account_entity,
            'AccountNumber'		 	=> $aramex_detail->aramex_account,
            'AccountPin'		 	=> $aramex_detail->aramex_pin,
            'UserName'			 	=> $aramex_detail->aramex_email,
            'Password'			 	=> $aramex_detail->aramex_password,
            'Version'			 	=> 'v1.0'
        ),
        'Transaction' 	=> array(
            'Reference1'    => '001'
        ),
        'OriginAddress' => array(
            'City'	    => 'Jeddah',
            'CountryCode'	=> 'SA'
        ),
        'DestinationAddress' 	=> array(
            'City'			=> $city,
            'CountryCode'	=> $country->country_code,
            'State' => $destAddr->region,
            'PostCode' => $destAddr->zip_code
        ),
        'ShipmentDetails'	=> array(
            'PaymentType'	    => 'P',
            'ProductGroup'		=> $ProductGroup,
            'ProductType'		=> $ProductType,
            'ActualWeight' 		=> array('Value' => $weight, 'Unit' => 'KG'),
            'NumberOfPieces'    => $totalQuan
        )
    );

    try {
        $soapClient = new SoapClient($aramex_detail->aramex_rate_wsdl, array('trace' => 1));
        $results = $soapClient->CalculateRate($params);
        $data = array();
        $data['error'] = '';
        
        if(isset($results->Notifications->Notification->Message)){
             //$error = $results->Notifications->Notification->Message;
             $error = $results->Notifications->Notification->Code;
            if ($error == "ERR52" || $error == "ERR06") {
                if($lang == "eng") {
                    $error = "Your Zip code / State is invalid, please correct the zip code / State for checkout!";
                }else{
                    $error = "الرمز البريدي/الولاية غير صحيحة، نرجو إدخال رمز بريدي/ولاية بالشكل الصحيح";
                }
            }

            $data['error'] = $error;
            return $data;
            //sendAramexFailedEmail();
        }

        $data['data'] = $results;
        return $data;
    } catch (SoapFault $fault) {
        die('Error : ' . $fault->faultstring);
        sendAramexFailedEmail();
    }
}

function sendAramexFailedEmail(){
    $CI = & get_Instance();
    $CI->load->model('Model_settings');
    $settings = $CI->Model_settings->get(1);
    $subject = "Aramex Shipment API has unexpected error!";
    $message = "Hi IOUD, <br> Aramex Shipment API has unexpected error, you can enable manual shipment from admin panel.";

    $data['title'] = $subject;
    $data['message'] = $message;
    $data['info'] = array();

    $view = $CI->load->view('layouts/emails/eng_general_email', $data, TRUE);

    $body = $view;

    htmlmail($settings->admin_email, $subject, $body);
}

function getUserAddresses($order){

    $obj =  new stdClass();
    $addressCheck = false;
    $address = getUserAddressByOrder($order->id);
    if ($address) {
        $obj->countryCode = $address->country;
        $obj->cityId = $address->city;
        $addressCheck = true;
    } else {

        $address = getCartAddress($order->address_id);
        if($address){

            $obj->cityId = $address->city;
            $obj->countryCode = $address->country;
            $addressCheck = true;

        }
    }
    $obj->full_address =$address->address_1.' '.$address->address_2;

    $user = getUserById($order->user_id);

    if (!$user) {
        $obj->fullName = $address->full_name;
        $obj->email = $address->email;
        if(!$addressCheck) {
            $obj->countryCode = $address->country;
            $obj->cityId = $address->city;
        }
        $obj->mobile_no = $address->phone_no;
        $obj->userId = $address->user_id;
    } else {
        $obj->fullName = $user['first_name'] . " " . $user['last_name'];
        $obj->email = $user['email'];
        if(!$addressCheck) {
            $obj->countryCode = $user['country'];
            $obj->cityId = $user['city'];
        }
        $obj->mobile_no = $user['mobile_no'];
        $obj->userId = $user['id'];
    }
    return $obj;
}

function getAramexTrackingId($totalAmount,$extra_charges,$order,$address_id,$payment_type){
    /*error_reporting(E_ALL);
    ini_set('display_errors', '1');*/

    $CI = & get_Instance();

    if($order->shipment_method == 1) {
        return false;
    }else{
        $CI->load->model('Model_cart_user_address');
        $CI->load->model('Model_temp_orders');
        $CI->load->model('Model_order_product');
        $CI->load->model('Model_general');
        $CI->load->model('Model_currency');
        $CI->load->model('Model_settings');

        $aramex_detail = $CI->Model_settings->get(1);

        $destAddr = $CI->Model_cart_user_address->getSingleRow(array('id' => $address_id));
        $products = $CI->Model_order_product->getMultipleRows(array('order_id' => $order->id), true);
        $kg = 0;
        $lb = 0;
        $g = 0;
        $oz = 0;
        $totalQuan = 0;

        foreach ($products as $key => $prod) {
            $getBy['id'] = $prod['product_id'];
            $prods = $CI->Model_product->getSingleRow($getBy);

            if ($prods->weight_unit == 'kg') {
                $kg += $prods->weight_value;
            }
            if ($prods->weight_unit == 'lb') {
                $lb += $prods->weight_value;
            }
            if ($prods->weight_unit == 'g') {
                $g += $prods->weight_value;
            }
            if ($prods->weight_unit == 'oz') {
                $oz += $prods->weight_value;
            }
            $totalQuan += $prod['quantity'];
        }
        $weight = weightConvertToKg($kg, $lb, $g, $oz);
        $city = getCityById($destAddr->city,'eng');
        $country = $CI->Model_general->getSingleRow('countries', array(
            'code' => $destAddr->country
        ));
        $address = getUserAddresses($order);

        if ($country->country_code == 'SA') {
            $ProductGroup = 'DOM';
            $ProductType = 'ONP';
            $currency = 'SAR';

        } else {
            $ProductGroup = 'EXP';
            $ProductType = 'PPX';
            $currency = 'USD';

            $currency_rate = '';
            $get_currency = $CI->Model_currency->getAll();
            foreach($get_currency as $curr){
                if(strtoupper($curr->eng_currency) == "USD"){
                    $currency_rate = $curr->rate;
                    break;
                }
            }

            $tAmount = $totalAmount*$currency_rate;
            $totalAmount    =   $tAmount;

            $extra_currency = "";
            $extra_charges = "";
        }


        $service = '';
        if ($payment_type == 'visa' || $payment_type == 'mastercard') {
            $paymentType = 'P';
            $sipper_acc_no = $aramex_detail->aramex_account;
            $consignee_acc_no = "";
            $extra_charges = "";
            $extra_currency = "";
        } else {
            $paymentType = 'P';

            $sipper_acc_no = $aramex_detail->aramex_account;
            $consignee_acc_no = "";
        }

        if($country->country_code == 'SA' && $payment_type != 'visa' || $payment_type != 'mastercard'){
            $service = 'CODS';
            $extra_charges = $extra_charges;
            $extra_currency = "SAR";
        }if($country->country_code == 'SA' && $payment_type == 'visa' || $payment_type == 'mastercard'){
            $service = '';
            $extra_charges = '';
            $extra_currency = '';
        }if($country->country_code != 'SA'){
            $service = '';
            $extra_charges = '';
            $extra_currency = '';
        }

        $referenceId = str_pad($order->id, 6, "0", STR_PAD_LEFT);
        $params = array(
            'Shipments' => array(
                'Shipment' => array(
                    'Shipper' => array(
                        'Reference1' => 'Ref ' . $referenceId,
                        'AccountNumber' => $sipper_acc_no,
                        'PartyAddress' => array(
                            'Line1' => 'IOUD',
                            /*'Line2' 				=> '',
                            'Line3' 				=> '',*/
                            'City' => 'Riyadh',
                            'StateOrProvinceCode' => '',
                            'PostCode' => '',
                            'CountryCode' => 'SA'
                        ),
                        'Contact' => array(
                            'Department' => '',
                            'PersonName' => 'IOUD',
                            'Title' => '',
                            'CompanyName' => 'IOUD',
                            'PhoneNumber1' => '000000000',
                            'PhoneNumber1Ext' => '',
                            'PhoneNumber2' => '',
                            'PhoneNumber2Ext' => '',
                            'FaxNumber' => '',
                            'CellPhone' => '000000000',
                            'EmailAddress' => 'Intelligentoud@gmail.com',
                            'Type' => ''
                        ),
                    ),

                    'Consignee' => array(
                        'Reference1' => 'Ref ' . $referenceId,
                        'Reference2' => '',
                        'AccountNumber' => $consignee_acc_no,
                        'PartyAddress' => array(
                            'Line1' => $address->full_address,
                            'Line2' => '',
                            'Line3' => '',
                            'City' => $city,
                            'StateOrProvinceCode' => $destAddr->region,
                            'PostCode' => $destAddr->zip_code,
                            'CountryCode' => $country->country_code
                        ),

                        'Contact' => array(
                            'Department' => '',
                            'PersonName' => $address->fullName,
                            'Title' => $address->fullName,
                            'CompanyName' => $address->fullName,
                            'PhoneNumber1' => $address->mobile_no,
                            'PhoneNumber1Ext' => '',
                            'PhoneNumber2' => '',
                            'PhoneNumber2Ext' => '',
                            'FaxNumber' => '',
                            'CellPhone' => $address->mobile_no,
                            'EmailAddress' => $address->email,
                            'Type' => ''
                        ),
                    ),

                    // conditional based on PaymentType = "3"
                    'ThirdParty' => array(
                        'Reference1' => '',
                        'Reference2' => '',
                        'AccountNumber' => $aramex_detail->aramex_account,
                        'PartyAddress' => array(
                            'Line1' => '',
                            'Line2' => '',
                            'Line3' => '',
                            'City' => '',
                            'StateOrProvinceCode' => '',
                            'PostCode' => '',
                            'CountryCode' => ''
                        ),
                        'Contact' => array(
                            'Department' => '',
                            'PersonName' => '',
                            'Title' => '',
                            'CompanyName' => '',
                            'PhoneNumber1' => '',
                            'PhoneNumber1Ext' => '',
                            'PhoneNumber2' => '',
                            'PhoneNumber2Ext' => '',
                            'FaxNumber' => '',
                            'CellPhone' => '',
                            'EmailAddress' => '',
                            'Type' => ''
                        ),
                    ),

                    'Reference1' => '', // Optional
                    'Reference2' => '', // Optional
                    'Reference3' => '',
                    'ForeignHAWB' => uniqid(), // must be unique changed by kashif
                    'TransportType' => 0,
                    'ShippingDateTime' => time(),
                    'DueDate' => time(), // this is optional
                    'PickupLocation' => 'Reception', // Optional
                    'PickupGUID' => '', // Optional
                    'Comments' => 'Shpt 0001', // Optional
                    'AccountingInstrcutions' => '', // Optional
                    'OperationsInstructions' => '', // Optional

                    'Details' => array(

                        'ActualWeight' => array(
                            'Value' => $weight,
                            'Unit' => 'Kg'
                        ),

                        'ProductGroup' => $ProductGroup,
                        'ProductType' => $ProductType,
                        'PaymentType' => $paymentType,
                        'PaymentOptions' => '',
                        'Services' => $service, // if cods then must enter cod amount
                        'NumberOfPieces' => $totalQuan,
                        'DescriptionOfGoods' => 'Parcels',
                        'GoodsOriginCountry' => 'SA',

                        'CashOnDeliveryAmount' => array(
                            'Value' => $extra_charges,
                            'CurrencyCode' => $extra_currency
                        ),

                        // optional
                        'InsuranceAmount' => array(
                            'Value' => 0,
                            'CurrencyCode' => ''
                        ),

                        // conditional base on PaymentType = "C"
                        'CollectAmount' => array(
                            'Value' => 0,
                            'CurrencyCode' => ''
                        ),

                        // conditional base on PaymentType = "3"
                        'CashAdditionalAmount' => array(
                            'Value' => 0,
                            'CurrencyCode' => ''
                        ),

                        // conditional base on PaymentType = "3"
                        'CashAdditionalAmountDescription' => '',

                        //order_total amount
                        'CustomsValueAmount' => array(
                            'Value' => $totalAmount,
                            'CurrencyCode' => $currency
                        ),

                        'Items' => array()
                    ),
                ),
            ),

            // client's Aramex test account credentials
            'ClientInfo'  			=> array(
                'AccountCountryCode'	=> $aramex_detail->aramex_country_code,
                'AccountEntity'		 	=>  $aramex_detail->aramex_account_entity,
                'AccountNumber'		 	=> $aramex_detail->aramex_account,
                'AccountPin'		 	=> $aramex_detail->aramex_pin,
                'UserName'			 	=> $aramex_detail->aramex_email,
                'Password'			 	=> $aramex_detail->aramex_password,
                'Version'			 	=> 'v1.0'
            ),


            // all fields are optional
            'Transaction' => array(
                'Reference1' => '',
                'Reference2' => '',
                'Reference3' => '',
                'Reference4' => '',
                'Reference5' => '',
            ),
            // for creating the report link in pdf
            'LabelInfo'				=> array(
                'ReportID' 				=> 9201,
                'ReportType'			=> "URL",
            ),
        );

        // item detail
        $params['Shipments']['Shipment']['Details']['Items'][] = array(
            'PackageType' => 'Box',
            'Quantity' => $totalQuan,
            'Weight' => array(
                'Value' => $weight,
                'Unit' => 'Kg',
            ),
            'Comments' => 'Parcels',
            'Reference' => ''
        );

        $soapClient = new SoapClient($aramex_detail->aramex_ship_wsdl, array('trace' => 1));

        $results = $soapClient->CreateShipments($params);
        $results->reference_id = 'Ref ' . $referenceId;
        $results->payment_type = $paymentType;
        $results->totalAmount = $totalAmount;
        //echo "<pre>"; print_r($results); exit;
        return $results;
    }
}

function aramexPrintLabel($order_id,$tracking_id,$total_amount,$address_id){
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
    $CI = & get_Instance();
    $CI->load->model('Model_settings');
    $aramex_detail = $CI->Model_settings->get(1);
    $destAddr = $CI->Model_cart_user_address->getSingleRow(array('id' => $address_id));
    $city = getCityById($destAddr->city,'eng');
    $country = $CI->Model_general->getSingleRow('countries', array(
        'code' => $destAddr->country
    ));

    if ($country->country_code == 'SA') {
        $ProductGroup = 'DOM';

    } else {
        $ProductGroup = 'EXP';
    }

    $params = array(
        'ClientInfo'  			=> array(
            'AccountCountryCode'	=> $aramex_detail->aramex_country_code,
            'AccountEntity'		 	=>  $aramex_detail->aramex_account_entity,
            'AccountNumber'		 	=> $aramex_detail->aramex_account,
            'AccountPin'		 	=> $aramex_detail->aramex_pin,
            'UserName'			 	=> $aramex_detail->aramex_email,
            'Password'			 	=> $aramex_detail->aramex_password,
            'Version'			 	=> 'v1.0'
        ),

        'Transaction' => array(
            'Reference1' => $total_amount,
            'Reference2' => '',
            'Reference3' => '',
            'Reference4' => '',
            'Reference5' => '',
        ),


        'LabelInfo'                             => array(
            'ReportID'                              => 9729,
            'ReportType'                    => 'URL',

        ),
        'ShipmentNumber' =>$tracking_id,
        'ProductGroup' => $ProductGroup,
        'OriginEntity' => $aramex_detail->aramex_account_entity

    );


    $soapClient = new SoapClient($aramex_detail->aramex_ship_wsdl, array('trace' => 1));

    $results = $soapClient->PrintLabel($params);
    return $results;

}

function weightConvertToKg($kg,$lb,$g,$oz) {
    $g_weight = number_format($g / 1000, 2);
    $kg_weight = $kg+$g_weight;
    $oz_weight = number_format($oz/16,2);
    $lb_weight = $lb+$oz_weight;
    $lb_to_kg = number_format($lb_weight / 0.4536, 2);
    $weight = $kg_weight+$lb_to_kg;
    return $weight;
}

function saveTrackingId($order_id,$track_detail){
    $CI = & get_Instance();
    $CI->load->model('Model_orders');
    $update_by['id'] = $order_id;
    $update = $CI->Model_orders->update($track_detail,$update_by);
    return $update;
}
function getTrackingId($order_id){
    $CI = & get_Instance();
    $CI->load->model('Model_orders');
    $tracking = $CI->Model_orders->get($order_id);
    if($tracking->tracking_id != ''){
        $tracking = $tracking->tracking_id;
    }else{
        $tracking = false;
    }
    return $tracking;
}

function currencies() {
    $CI = & get_Instance();
    $CI->load->model('Model_currency');
    $currency = $CI->Model_currency->getAll(true);
    return $currency;
}
function currencyRatesCalculate($amount,$currency_rate = '',$id = ''){
    $CI = & get_Instance();
    $lang = $CI->session->userdata('site_lang');
    $CI->load->model('Model_currency');
    $CI->load->model('Model_orders');
    //echo 'price'.$amount.'c_rate'.$currency_rate.'c_id'.$curr_id.'o_id'.$id;
    $currency_id = $CI->session->userdata('currency_id');
    if ($currency_id == NULL) {
        $getCurr['default_currency'] = 1;
        $currency = $CI->Model_currency->getSingleRow($getCurr);
    } else {
        $currency = $CI->Model_currency->get($currency_id);
    }
    if($currency_rate == '') {
        // set currency detail for saving with orders
        $currency_detail['currency_id'] = $currency->id;
        $currency_detail['currency_unit'] = $currency->eng_currency;
        $currency_detail['currency_rate'] = $currency->rate;
        $CI->session->set_userdata('currency_detail', $currency_detail);
        //============================================================
        $convertAmount = number_format((float)$amount * $currency->rate, 2, '.', '');
    }else{
        $prodPrice = $amount /$currency_rate;
        $convertAmount = number_format((float)$prodPrice*$currency->rate, 2, '.', '');
        //echo "<pre>"; print_r($currency); exit;
        if($id != '') {
            $orderUpdate['currency_rate'] = $currency_rate;
            $orderUpdateBy['id'] = $id;
            $CI->Model_orders->update($orderUpdate,$orderUpdateBy);
        }
    }
    if($lang == 'eng'){
        $data['unit'] = $currency->eng_currency;
    }else{
        $data['unit'] = $currency->arb_currency;
    }
    $data['rate']  = $convertAmount;
    $data['currency_rate']  = $currency->rate;
    return $data;
}
function afterSaveCurrencyChange($amount,$currency_rate){
    $CI = & get_Instance();
    $currency_id = $CI->session->userdata('currency_id');
    $currency = $this->Model_currency->get($currency_id);
    $prodPrice = $amount /$currency_rate;
    $newAmount = $prodPrice*$currency->rate;
    if($lang == 'eng'){
        $data['unit'] = $currency->eng_currency;
    }else{
        $data['unit'] = $currency->arb_currency;
    }
    $data['rate']  = $newAmount;
    $data['currency_rate']  = $currency->rate;
    return $data;
}
function getProductDetailById($id){
    $CI = & get_Instance();
    $CI->load->model('Model_product');
    $product = $CI->Model_product->get($id,true);
    return $product;
}
function viewEditDeleteRights($page,$right_type){
    $rights_array = user_rights();
    if($rights_array[$page][$right_type.'_p'] == 1)
    {
        $right = true;
    }else{
        $right = false;
    }
    return $right;
}
function saveLoyaltyPoints($amount,$currency_rate,$user_id){
    $CI = & get_Instance();
    $CI->load->model('Model_loyalty');
    $CI->load->model('Model_customer_loyalty');
    $amount = $amount/$currency_rate;
    $loyalty = $CI->Model_loyalty->get('1');
    $getByUser['user_id'] = $user_id;
    $customer_loyalty = $CI->Model_customer_loyalty->getSingleRow($getByUser);
    if($customer_loyalty){
        // check what is the user current level
        /*$level = getUserLevel($customer_loyalty,$loyalty);
        // for level 1 silver
        if($level == 1){*/
            $points = $amount*$loyalty->silver_points;
            $total_points = $customer_loyalty->loyalty_points+$points;
            if($total_points <= $loyalty->sliver_end_rang){
                $update_data['loyalty_points'] = round($total_points);
                $update_data['loyalty_level'] = '1';
            }
            else{
                $update_data['loyalty_points'] = round($total_points);
                $update_data['loyalty_level'] = '2';
            }
        /*}
        // for level 2 golden
        if($level == 2){*/
            $points = $amount*$loyalty->golden_points;
            $total_points = $customer_loyalty->loyalty_points+$points;
            if($total_points > $loyalty->sliver_end_rang
                && $total_points <= $loyalty->golden_end_rang){
                $update_data['loyalty_points'] = round($total_points);
                $update_data['loyalty_level'] = '2';
            }
            else{
                $update_data['loyalty_points'] = round($total_points);
                $update_data['loyalty_level'] = '3';
            }
        /*}
        // for level 3 platinum
        if($level == 3){*/
            $points = $amount*$loyalty->platinum_points;
            $total_points = $customer_loyalty->loyalty_points+$points;
            if($total_points > $loyalty->golden_end_rang){
                $update_data['loyalty_points'] = round($total_points);
                $update_data['loyalty_level'] = '3';
            }
            else{
                $update_data['loyalty_points'] = round($total_points);
                $update_data['loyalty_level'] = '3';
            }
        //}

        $update_by['user_id'] = $user_id;
        $CI->Model_customer_loyalty->update($update_data,$update_by);
    }else{
        $total_points = $amount*$loyalty->silver_points;
        $saveDate['user_id'] = $user_id;
        $saveDate['loyalty_points'] = round($total_points);
        $saveDate['loyalty_level'] = '1';
       $CI->Model_customer_loyalty->save($saveDate);
    }
}
/*function getUserLevel($customer_loyalty,$loyalty){
    if($customer_loyalty->loyalty_points >= $loyalty->sliver_start_rang
        && $customer_loyalty->loyalty_points <= $loyalty->sliver_end_rang){
        $level = 1;
    }
    if($customer_loyalty->loyalty_points > $loyalty->sliver_end_rang
        && $customer_loyalty->loyalty_points <= $loyalty->golden_end_rang){
        $level = 2;
    }
    if($customer_loyalty->loyalty_points > $loyalty->golden_end_rang
        && $customer_loyalty->loyalty_points <= $loyalty->platinum_start_rang){
        $level = 3;
    }
    return $level;
}*/

function loyaltyDiscount($tAmount,$coupon_discount = 0){

    if ($coupon_discount != 0) {

        return 0;
    }

    $data = array();
    $CI = &get_Instance();
    $CI->load->model('Model_loyalty');
    $CI->load->model('Model_customer_loyalty');
    $user_id = $CI->session->userdata('user_id');
    $loyalty = $CI->Model_loyalty->get('1');
    // if user is guest then no loyalty implement
    if ($user_id == NULL) {
        return 0;
    }
    if ($loyalty->active_discount == 1) {
        $getByUser['user_id'] = $user_id;
        $cust_loyalty = $CI->Model_customer_loyalty->getSingleRow($getByUser);

        if ($cust_loyalty->loyalty_points <= $loyalty->sliver_end_rang
        ) {

            if ($loyalty->silver_discount_type == 1) {
                $discounts = $loyalty->silver_discount_amount;
                $discount = currencyRatesCalculate($discounts);
                $data['paytabs'] = $discounts;
                $data['rate'] = $discount['rate'];
                $data['type'] = $loyalty->silver_discount_amount . ' ' . $discount['unit'];
            } else {
                $discount_amount = ($loyalty->silver_discount_amount / 100);
                $discounts = $tAmount * $discount_amount;
                $discount = currencyRatesCalculate($discounts);
                $data['paytabs'] = $discounts;
                $data['rate'] = $discount['rate'];
                $data['type'] = $loyalty->silver_discount_amount . '%';
            }
        }

        if ($cust_loyalty->loyalty_points > $loyalty->sliver_end_rang
            && $cust_loyalty->loyalty_points <= $loyalty->golden_end_rang
        ) {
            if ($loyalty->golden_discount_type == 1) {
                $discounts = $loyalty->golden_discount_amount;
                $discount = currencyRatesCalculate($discounts);
                $data['paytabs'] = $discounts;
                $data['rate'] = $discount['rate'];
                $data['type'] = $loyalty->golden_discount_amount . ' ' . $discount['unit'];
            } else {
                $discount_amount = ($loyalty->golden_discount_amount / 100);
                $discounts = $tAmount * $discount_amount;
                $discount = currencyRatesCalculate($discounts);
                $data['paytabs'] = $discounts;
                $data['rate'] = $discount['rate'];
                $data['type'] = $loyalty->golden_discount_amount . '%';
            }
        }
        if ($cust_loyalty->loyalty_points > $loyalty->golden_end_rang) {
            if ($loyalty->platinum_discount_type == 1) {
                $discount = $loyalty->platinum_discount_amount;
                $discount = currencyRatesCalculate($discount);
                $data['paytabs'] = $discounts;
                $data['rate'] = $discount['rate'];
                $data['type'] = $loyalty->platinum_discount_amount . ' ' . $discount['unit'];
            } else {
                $discount_amount = ($loyalty->platinum_discount_amount / 100);
                $discounts = $tAmount * $discount_amount;
                $discount = currencyRatesCalculate($discounts);
                $data['paytabs'] = $discounts;
                $data['rate'] = $discount['rate'];
                $data['type'] = $loyalty->platinum_discount_amount . '%';
            }
        }
    } else {
        return 0;
    }
    // set cookie for using database
    $cookie = array(
        'name' => 'loyalty_discount',
        'value' => $data['rate'],
        'expire' => time() + (10 * 365 * 24 * 60 * 60)
    );
    $CI->input->set_cookie($cookie);

    $cookie = array(
        'name' => 'discount_type',
        'value' => $data['type'],
        'expire' => time() + (10 * 365 * 24 * 60 * 60)
    );
    $CI->input->set_cookie($cookie);
    return $data;
}


function excludeLoyaltyPoints($amount,$currency_rate,$user_id){
    $CI = & get_Instance();
    $CI->load->model('Model_loyalty');
    $CI->load->model('Model_customer_loyalty');
    $amount = $amount/$currency_rate;
    $loyalty = $CI->Model_loyalty->get('1');
    $getByUser['user_id'] = $user_id;
    $customer_loyalty = $CI->Model_customer_loyalty->getSingleRow($getByUser);
    if($customer_loyalty){

        // for level 1 silver
            $points = $amount*$loyalty->silver_points;
            $total_points = $customer_loyalty->loyalty_points-$points;
            if($total_points <= $loyalty->sliver_end_rang){
                $update_data['loyalty_points'] = round($total_points);
                $update_data['loyalty_level'] = '1';
            }
            else{
                $update_data['loyalty_points'] = round($total_points);
                $update_data['loyalty_level'] = '1';
            }

        // for level 2 golden
            $points = $amount*$loyalty->golden_points;
            $total_points = $customer_loyalty->loyalty_points-$points;

            if($total_points > $loyalty->sliver_end_rang
                && $total_points <= $loyalty->golden_end_rang){
                $update_data['loyalty_points'] = round($total_points);
                $update_data['loyalty_level'] = '2';
            }
            if($total_points >= $loyalty->sliver_start_rang
                && $total_points <= $loyalty->sliver_end_rang){
                $update_data['loyalty_points'] = round($total_points);
                $update_data['loyalty_level'] = '1';
            }

        // for level 3 platinum
            $points = $amount*$loyalty->platinum_points;
            $total_points = $customer_loyalty->loyalty_points-$points;
            if($total_points > $loyalty->golden_end_rang){
                $update_data['loyalty_points'] = round($total_points);
                $update_data['loyalty_level'] = '3';
            }
            if($total_points > $loyalty->sliver_end_rang
                && $total_points <= $loyalty->golden_end_rang){
                $update_data['loyalty_points'] = round($total_points);
                $update_data['loyalty_level'] = '2';
            }

        $update_by['user_id'] = $user_id;
        $CI->Model_customer_loyalty->update($update_data,$update_by);
    }
}

function checkLevel($user_points){
    $CI = & get_Instance();
    $CI->load->model('Model_loyalty');
    $loyalty = $CI->Model_loyalty->get('1');

    if($user_points >= $loyalty->sliver_start_rang
        && $user_points <= $loyalty->sliver_end_rang){
        return 1;
    }
    if($user_points > $loyalty->sliver_end_rang
        && $user_points <= $loyalty->golden_end_rang){
        return 2;
    }
    if($user_points > $loyalty->golden_end_rang){
        return 3;
    }


}


// limited size not currently used
function optimizeImage($path, $file)
{
    $CI = & get_Instance();
    $CI->load->library("Optimus");
    $optimus = new Optimus('1AUBDV468PD7KI0RUUBVRQP9');
    $img_name = $path . $file;
    $opt_res = $optimus->optimize($img_name);
    file_put_contents($img_name, $opt_res);
}

function compress_image($path)
{
    $destination_url = $path;
    $info = getimagesize($path);
    if ($info['mime'] == 'image/jpeg')
    {
        $image = imagecreatefromjpeg($path);
        imagejpeg($image, $destination_url);
    }elseif ($info['mime'] == 'image/gif')
    {
        $image = imagecreatefromgif($path);
        imagegif($image, $destination_url);
    }elseif ($info['mime'] == 'image/png')
    {
        $image = imagecreatefrompng($path);
        imagepng($image, $destination_url);
    }
}

function existImgOptimus()
{
    error_reporting(-1);
    ini_set('error_reporting', E_ALL);
    $CI = & get_Instance();
    $CI->load->model('Model_home_slider');
    $CI->load->model('Model_image');
    $sliders = $CI->Model_home_slider->getAll();
    //$CI->Model_image->getAll();
    foreach($sliders as $key => $slider) {
        $path = "uploads/images/home/".$slider->arb_image;

        $CI->load->library("Optimus");
        $optimus = new Optimus('1AUBDV468PD7KI0RUUBVRQP9');
        $opt_res = $optimus->optimize($path);
        $r = file_put_contents($path, $opt_res);

        /*$destination_url = $path;
        $info = getimagesize($path);
        if ($info['mime'] == 'image/jpeg')
        {
            $image = imagecreatefromjpeg($path);
            $r = imagejpeg($image, $destination_url);
        }elseif ($info['mime'] == 'image/gif')
        {
            $image = imagecreatefromgif($path);
            $r = imagegif($image, $destination_url);
        }elseif ($info['mime'] == 'image/png')
        {
            $image = imagecreatefrompng($path);
            $r = imagepng($image, $destination_url);
        }*/

        echo $path;
        echo "<br>";
        echo $key;
        var_dump($r);
        echo "<br>";
    }
}

function getImageSizeDimension($image,$imgName){

    $data = array();
    list($width, $height) = getimagesize($image['tmp_name']);
    $size = $image['size'];
    $data['size'] =  $size;
    $data['height'] =  $height;
    $data['width'] =  $width;
    if($height > 1402 && $width > 688){
        $data['message'] = "The ".$imgName." image height and width is too large recommended dimension is 1402px x 688px";
        $data['success'] = false;
        return $data;
    }else{
        $data['success'] = true;
        return $data;
    }
    if($size > 1000000){
        $data['message'] = "The ".$imgName." image size is too large recommended size is 1 mb";
        $data['success'] = false;
        return $data;
    }else{
        $data['success'] = true;
        return $data;
    }
}

function getUserAddressByOrder($order_id){
    $CI = & get_Instance();
    $CI->load->model('Model_cart_user_address');
    $fetch_by['order_id'] = $order_id;
    $result = $CI->Model_cart_user_address->getSingleRow($fetch_by);
    return $result;
}

function popupForLoyalty($total){

    $ifcDiscount = getCouponPrice($total);

    $loyalty_discount = loyaltyDiscount($total,$ifcDiscount);

    if($loyalty_discount['rate'] > 0){
        return $loyalty_discount['type'];
    }else{
        return false;
    }
}

function checkVourcherIsExpire(){
    $CI = & get_Instance();
    $CI->load->model('Model_coupons');
    $voucher_code = get_cookie('voucher_code');

    if($voucher_code) {
        $isExpire = $CI->Model_coupons->couponIsExpire($voucher_code);
        if (!$isExpire) {
            delete_cookie('voucher_code');
            return false;
        } else {
            return true;
        }
    }else{
        return true;
    }

}

function debug($array,$dump = false,$query = false){
    $CI = & get_Instance();

    if($dump){
        echo "<pre>";
        var_dump($array);
        echo "</pre>";
    }else{
        echo "<pre>";
        print_r($array);
        echo "</pre>";


    }
    if($query){
        echo $CI->db->last_query();
    }
    exit;


}

function unlink_image($path,$image_name){
    $file = $path.$image_name;
    if (!unlink($file))
    {
        $delete = false;
    }
    else
    {
        $delete = true;
    }
    return $delete;
}

function display_errors($query =false){
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    $CI = & get_Instance();
    if($query) {
        echo $CI->db->last_query();
        exit;
    }

}

function save_meta_data($data,$tpl_name){

    $CI = & get_Instance();
    $CI->load->model('Model_page');
    $array = array();
    $array['eng_meta_title'] = $data['eng_meta_title'];
    $array['arb_meta_title'] = $data['arb_meta_title'];
    $array['eng_meta_description'] = $data['eng_meta_description'];
    $array['arb_meta_description'] = $data['arb_meta_description'];
    $array['eng_meta_keyword'] = $data['eng_meta_keyword'];
    $array['arb_meta_keyword'] = $data['arb_meta_keyword'];
    $array['tpl_name'] = $tpl_name;

    $pageCheck = $CI->Model_page->getRowCount2(array('tpl_name'=>$tpl_name));

    if(count($pageCheck) > 0) {
        $array['created_at'] = date('Y-m-d H:i:s');
        $page_update_by['tpl_name'] = $tpl_name;
        $CI->Model_page->update($array,$page_update_by);
        $id = $pageCheck[0]->id;

    }else {
        $id = $CI->Model_page->save($array);
    }

    return $id;

}

function unset_meta_date($post_data){
    $data = array();
    $keys[] =  'form_type';
    $keys[] =  'eng_meta_title';
    $keys[] =  'arb_meta_title';
    $keys[] =  'eng_meta_description';
    $keys[] =  'arb_meta_description';
    $keys[] =  'eng_meta_keyword';
    $keys[] =  'arb_meta_keyword';
    $keys[] =  'tpl_name';

    foreach($post_data as $key => $value)
    {
        if(!in_array($key,$keys))
        {
            $data[$key] = $value;
        }
    }
    return $data;
}

function getCartProducts($u_id = null){

    $CI = & get_Instance();
    $CI->load->model('Model_temp_orders');
    $CI->load->model('Model_product');
    $data = array();
    $data = $CI->lang->line('all');
    $data['lang'] = $CI->session->userdata('site_lang');

    if($u_id == null) {
        $get_by['user'] = get_cookie('u_id');
        $data['user'] = get_cookie('u_id');
    }else{
        $get_by['user'] = $u_id;
        $data['user'] = $u_id;
    }
    $get_by['saved'] = 0;
    $data['carts'] = $CI->Model_temp_orders->getMultipleRows($get_by,true);
    $products = array();
    foreach ($data['carts'] as $cart){
        $getBy['id'] = $cart['p_id'];
        $prods = $CI->Model_product->getSingleRow($getBy,true);
        $products[] = $prods;
    }

    $get_by['saved'] = 1;
    $data['saved_carts'] = $CI->Model_temp_orders->getMultipleRows($get_by,true);

    $saved_products = array();
    foreach ($data['saved_carts'] as $cart){
        $getBy['id'] = $cart['p_id'];
        $prods = $CI->Model_product->getSingleRow($getBy,true);
        $saved_products[] = $prods;
    }
    $data['products'] = $products;

    $data['saved_products'] = $saved_products;

    return $data;
}

function getProductIds(){
    $productIds = array();
    $cart = getCartProducts();
    foreach ($cart['products'] as $car){
        $productIds[] = $car['id'];
    }
    return $productIds;
}

function getShowroomsLocation($showroom_id,$associative=true){

    $CI = & get_Instance();
    $CI->load->model('Model_general');
    $get_by['showroom_id'] = $showroom_id;

    if($associative){
        $showrooms = $CI->Model_general->getMultipleRows("showrooms_location",$get_by,true);

    }else{
        $showrooms = $CI->Model_general->getSingleRow("showrooms_location",$get_by,true);

    }
    return $showrooms;

}

function getPageNameFromUrl($url,$lang){

    $uri_segments = explode('/', $url);

    if($lang == 'eng'){

        return strtolower($uri_segments[3]);
    }else{
        return strtolower($uri_segments[2]);
    }

}

function getPageClassFromUrl($url,$lang){

    $uri_segments = explode('/', $url);

    if($lang == 'eng'){

        return strtolower($uri_segments[5]);
    }else{
        return strtolower($uri_segments[4]);
    }

}

function activeMenuClass($url,$page_link,$lang){
    $p_name = getPageNameFromUrl($url,$lang);
    $p_class = getPageClassFromUrl($page_link,$lang);
    if($p_class == $p_name){
        $active = 'active';
    }else{
        $active = '';
    }
    return $active;
}

function reCaptcha($siteKey) {

    $url = 'https://www.google.com/recaptcha/api/siteverify';
    $data = array(
        'secret' => '6Ldi3DsUAAAAAO698AqbQMob_wd8tHXHGpCEkYN9',
        'response' => $siteKey,
        'remoteip' => $_SERVER['REMOTE_ADDR']
    );

    $options = array(
        'http' => array(
            'header' => "Content-type: application/x-www-form-urlencoded\r\n",
            'method' => 'POST',
            'content' => http_build_query($data)
        )
    );
    $context = stream_context_create($options);
    $verify = @file_get_contents($url, false, $context);
    $captcha_success = json_decode($verify);

    // return response true or false
    return $captcha_success->success;

}

function userGeneralEmail($post_data,$subject,$msg){

    $CI = & get_Instance();
    $message = $CI->lang->line('all');
    $lang = $CI->session->userdata('site_lang');
    if(isset($post_data['name']))
        $name = $post_data['name'];
    else
        $name = $post_data['full_name'];
    $emailData['message'] = $message['hi_label']." ".$name.", <br><br> ".$msg;
    $emailData['title'] = $subject;
    $emailData['info'] = $post_data;

    if($lang == "eng") {
        $body = $CI->load->view('layouts/emails/eng_general_email',$emailData,TRUE);
    }else{
        $body = $CI->load->view('layouts/emails/arb_general_email',$emailData,TRUE);
    }

    $sent = htmlmail($post_data['email'],$subject,$body);

    return $sent;

}

function adminGeneralEmail($post_data,$subject,$msg){
    $CI = & get_Instance();
    $message = $CI->lang->line('all');
    $lang = $CI->session->userdata('site_lang');
    $setting = getSettings();
    $to = $setting->admin_email;

    $emailData['message'] = $message['hi_label']." Admin, <br><br> ".$msg;
    $emailData['title'] = $subject;
    $emailData['info'] = $post_data;

    if($lang == "eng") {
        $body = $CI->load->view('layouts/emails/eng_general_email',$emailData,TRUE);
    }else{
        $body = $CI->load->view('layouts/emails/arb_general_email',$emailData,TRUE);
    }

    $sent = htmlmail($to,$subject,$body);

    return $sent;

}

function getMetaTags($pag_id){
    $CI = & get_Instance();
    $CI->load->model('Model_general');
    $get_by['id'] = $pag_id;
    $meta_tags =  $CI->Model_general->getSingleRow("page",$get_by,true);
    return $meta_tags;
}