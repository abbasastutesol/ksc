<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit712b0c467374530dbc2b4a2991d6ebfb
{
    public static $prefixLengthsPsr4 = array (
        'G' => 
        array (
            'Giggsey\\Locale\\' => 15,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Giggsey\\Locale\\' => 
        array (
            0 => __DIR__ . '/..' . '/giggsey/locale/src',
        ),
    );

    public static $prefixesPsr0 = array (
        'l' => 
        array (
            'libphonenumber' => 
            array (
                0 => __DIR__ . '/..' . '/giggsey/libphonenumber-for-php/src',
            ),
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit712b0c467374530dbc2b4a2991d6ebfb::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit712b0c467374530dbc2b4a2991d6ebfb::$prefixDirsPsr4;
            $loader->prefixesPsr0 = ComposerStaticInit712b0c467374530dbc2b4a2991d6ebfb::$prefixesPsr0;

        }, null, ClassLoader::class);
    }
}
