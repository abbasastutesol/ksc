<?php
Class Model_ChairManmessage extends Base_Model
{
	public function __construct()
	{
		parent::__construct("chairman_message");		
	}
	
		function getmessage()
		{
			
			
			$query = $this->db->select('chairman_message.*,page.eng_meta_title,page.eng_meta_description,page.	    eng_meta_keyword,page.arb_meta_title,page.arb_meta_description,page.arb_meta_keyword')
			 ->from('chairman_message')
			 ->join('page', 'chairman_message.page_id = page.id');
			 
			 //echo $this->db->last_query(); exit;
			 $result = $this->db->get();

				return $result->result();
				
			
			
		}
	
	
}