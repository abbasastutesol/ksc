<?php
Class Model_showroom extends Base_Model
{
	public function __construct()
	{
		parent::__construct("showroom");		
	}
	
		public function getAllData(){
			
			$data = $this->db->get('showroom');
			if($data->num_rows() >0)
			{
				
				return $data->result();
			}else{
				
				return false;
			}
			
		}
		
		public function getSpecficData($id){
			
		 $this->db->select('showroom.*,page.eng_meta_title,page.eng_meta_description,page.	    eng_meta_keyword,page.arb_meta_title,page.arb_meta_description,page.arb_meta_keyword')
         ->from('showroom')->join('page', 'showroom.page_id = page.id')
		 ->where('showroom.id',$id);
		//echo $this->db->last_query(); exit;
         $result = $this->db->get();

		return $result->result();
			
		}
	
	
}