<?php
Class Model_orders extends Base_Model
{
	public function __construct()
	{
		parent::__construct("orders");		
	}
	
	public function getAllOrder($orderBy = 'asc', $orderByField = 'id')
	{
		$this->db->select('*');
		$this->db->from('orders');
		$this->db->order_by($orderByField,$orderBy);
		$result = $this->db->get();
		if($result->num_rows() > 0)
		{	
		    return $result->result_array();
		}
		
		else
		{
			return false;
		}
	}
		
	public function getAllOrders($user_id, $lang)
	{
		$result = $this->db->query('select op.product_id, op.quantity, op.price, o.order_status, op.order_id, op.created_at, p.category_id, p.'.$lang.'_name from order_product op, orders o, products p where op.order_id = o.id and op.product_id = p.id and o.user_id = '.$user_id.' group by op.user_id, op.order_id');
		
		if($result->num_rows() > 0)
		{	
		    return $result->result_array();
		}
		
		else
		{
			return false;
		}
	}
	
	public function getAllReturn()
	{
		$result = $this->db->query('select ro.product_id, ro.order_id, ro.reason, ro.other_comments, p.eng_name, o.first_name, o.last_name, o.email from return_order ro, products p, orders o where ro.order_id = o.id and ro.product_id = p.id group by ro.order_id');
		
		if($result->num_rows() > 0)
		{	
		    return $result->result();
		}
		
		else
		{
			return false;
		} 
	}

	public function getAllLostItem()
	{
		$result = $this->db->query('select li.id, li.product_id, li.order_id, li.order_date, li.other_comments, p.eng_name, o.first_name, o.last_name, o.email from lostitem li, products p, orders o where li.order_id = o.id and li.product_id = p.id group by li.order_id');
		
		if($result->num_rows() > 0)
		{	
		    return $result->result();
		}
		
		else
		{
			return false;
		} 
	}
	
	public function getAllOrderProd($user_id, $order_id, $lang)
	{
		$result = $this->db->query('select op.product_id, op.quantity, op.price, o.order_status, op.order_id, op.created_at, p.category_id, p.'.$lang.'_name from order_product op, orders o, products p where op.order_id = o.id and op.product_id = p.id and o.user_id = '.$user_id.' and op.order_id = '.$order_id);
		
		if($result->num_rows() > 0)
		{	
		    return $result->result_array();
		}
		
		else
		{
			return false;
		} 
	}

	
	public function getOrderProd($order_id)
	{
		$result = $this->db->query('select p.id, p.eng_name, p.weight_value, 
      p.weight_unit, p.category_id, op.quantity, op.price, op.user_id, op.guest,
      op.varient_id, op.varient_value_id from order_product op, products p 
      where op.product_id = p.id and op.order_id = '.$order_id);
		
		if($result->num_rows() > 0)
		{	
		    return $result->result();
		}
		
		else
		{
			return false;
		} 
	}
	
	public function getOrderProdLang($order_id, $lang = 'eng')
	{
		$result = $this->db->query('select p.id, p.'.$lang.'_name, p.'.$lang.'_description, op.quantity, op.price, op.user_id, op.guest, op.varient_id, op.varient_value_id from order_product op, products p where op.product_id = p.id and op.order_id = '.$order_id);
		
		if($result->num_rows() > 0)
		{	
		    return $result->result();
		}
		
		else
		{
			return false;
		} 
	}
	
	public function getAllProcessedOrders($user_id, $lang, $interval = '')
	{
		$result = $this->db->query('select op.product_id, op.quantity, op.price, o.order_status, op.order_id, op.created_at, p.category_id, p.'.$lang.'_name from order_product op, orders o, products p where op.order_id = o.id and op.product_id = p.id and o.user_id = '.$user_id.' and o.order_status != 0 '.$interval.'');
		
		if($result->num_rows() > 0)
		{	
		    return $result->result_array();
		}
		
		else
		{
			return false;
		} 
	}
	
	public function getOrderById($order_id, $lang)
	{
		$result = $this->db->query('select op.product_id, op.quantity, op.price, o.order_status, o.shipment_method, o.shipment_price, DATE(o.created_at) as order_date, o.user_id, op.order_id, op.created_at, p.category_id, p.'.$lang.'_name from order_product op, orders o, products p where op.order_id = o.id and op.product_id = p.id and o.id = '.$order_id.' and o.order_status != 0 group by op.user_id, op.order_id');
		
		if($result->num_rows() > 0)
		{	
		    return $result->row_array();
		}
		
		else
		{
			return false;
		} 
	}
	
	public function getFeedbackDetails($f_id)
	{
		$result = $this->db->query('select pfb.product_id, pfb.order_id, pfb.order_date, pfb.other_comments, p.eng_name, o.first_name, o.last_name, o.email from placefeedback pfb, products p, orders o where pfb.order_id = o.id and pfb.product_id = p.id and pfb.id = '.$f_id.' group by pfb.order_id');
		
		if($result->num_rows() > 0)
		{	
		    return $result->row();
		}
		
		else
		{
			return false;
		}
	}
	
	public function getFeedbackAnswers($f_id)
	{
		$result = $this->db->query('select q.eng_question, a.eng_answer, pfba.id from placefeedback_answer pfba, question q, answer a where pfba.question_id = q.id and pfba.answer_id = a.id and pfba.feedback_id = '.$f_id.' group by pfba.id');
		
		if($result->num_rows() > 0)
		{	
		    return $result->result();
		}
		
		else
		{
			return false;
		}
	}
	
	public function getLostitemDetails($l_id)
	{
		$result = $this->db->query('select li.product_id, li.order_id, li.order_date, li.other_comments, p.eng_name, o.first_name, o.last_name, o.email from lostitem li, products p, orders o where li.order_id = o.id and li.product_id = p.id and li.id = '.$l_id.' group by li.order_id');
		
		if($result->num_rows() > 0)
		{	
		    return $result->row();
		}
		
		else
		{
			return false;
		}
	}
	
	public function getLostitemAnswers($l_id)
	{
		$result = $this->db->query('select q.eng_question, a.eng_answer, lia.id from lostitem_answer lia, question q, answer a where lia.question_id = q.id and lia.answer_id = a.id and lia.lostitem_id = '.$l_id.' group by lia.id');
		
		if($result->num_rows() > 0)
		{	
		    return $result->result();
		}
		
		else
		{
			return false;
		}
	}
	
	public function getCurrentCart()
	{
		$result = $this->db->query('SELECT t.id as id, ru.first_name, ru.last_name, COUNT( t.p_id ) AS no_of_prod, t.user, t.guest FROM temp_orders t, registered_users ru WHERE t.user = ru.id GROUP BY t.user, t.p_id');
		
		if($result->num_rows() > 0)
		{	
		    return $result->result_array();
		}
		
		else
		{
			return false;
		}
	}
	
	public function getCurrentGuestCart()
	{
		$result = $this->db->query('SELECT id, COUNT( p_id ) AS no_of_prod, guest, user FROM temp_orders WHERE guest =1 GROUP BY user');
		
		if($result->num_rows() > 0)
		{	
		    return $result->result_array();
		}
		
		else
		{
			return false;
		}
	}
	
	public function ordersTotalAllData()
	{
		$result = $this->db->query("SELECT 'Today' AS period, SUM(o.total_amount) AS total, o.order_status
									FROM orders o
									WHERE DATE(o.created_at) = CURRENT_DATE AND o.payment_order_status = 1
									GROUP BY o.order_status
									UNION ALL 
									SELECT  'This Week' AS period, SUM(o.total_amount) AS total, o.order_status
									FROM orders o
									WHERE o.created_at > DATE_SUB( NOW( ) , INTERVAL 1 WEEK ) AND o.payment_order_status = 1
									GROUP BY o.order_status
									UNION ALL 
									SELECT  'This Month' AS period, SUM(o.total_amount) AS total, o.order_status
									FROM orders o
									WHERE o.created_at > DATE_SUB( NOW( ) , INTERVAL 1 
									MONTH ) AND o.payment_order_status = 1
									GROUP BY o.order_status
									UNION ALL 
									SELECT  'This Year' AS period, SUM(o.total_amount) AS total, o.order_status
									FROM orders o
									WHERE o.created_at > DATE_SUB( NOW( ) , INTERVAL 1 YEAR ) AND o.payment_order_status = 1
									GROUP BY o.order_status
									UNION ALL 
									SELECT  'All Total' AS period, SUM(o.total_amount) AS total, o.order_status
									FROM orders o Where o.payment_order_status = 1
									GROUP BY o.order_status");
		if($result->num_rows() > 0)
		{	
		    return $result->result_array();
		}
		
		else
		{
			return false;
		}

	}
	
	public function weeklyOrdersChart()
	{
		$result = $this->db->query("SELECT DATE(a.created_at) as created_date, count(a.id) as 'all', sum(case a.order_status when 1 then 1 else 0 end) as 'pending', sum(case a.order_status when 0 then 1 else 0 end) as 'cancelled', sum(case when a.order_status = 2 then 1 else 0 end) as 'completed' FROM orders a WHERE a.created_at >= DATE(NOW()) - INTERVAL 7 DAY 
GROUP BY DATE(a.created_at)");
		if($result->num_rows() > 0)
		{	
		    return $result->result_array();
		}
		
		else
		{
			return false;
		}
		
	}
	
	public function monthlyOrdersChart()
	{
		$result = $this->db->query("SELECT MONTH(a.created_at) as created_date, count(a.id) as 'all', sum(case a.order_status when 1 then 1 else 0 end) as 'pending', sum(case a.order_status when 0 then 1 else 0 end) as 'cancelled', sum(case when a.order_status = 2 then 1 else 0 end) as 'completed' FROM orders a WHERE a.created_at >= DATE(NOW()) - INTERVAL 7 DAY 
GROUP BY MONTH(a.created_at)");
		if($result->num_rows() > 0)
		{	
		    return $result->result_array();
		}
		
		else
		{
			return false;
		}
		
	}
	
	public function yearlyOrdersChart()
	{
		$result = $this->db->query("SELECT YEAR(a.created_at) as created_date, count(a.id) as 'all', sum(case a.order_status when 1 then 1 else 0 end) as 'pending', sum(case a.order_status when 0 then 1 else 0 end) as 'cancelled', sum(case when a.order_status = 2 then 1 else 0 end) as 'completed' FROM orders a WHERE a.created_at >= DATE(NOW()) - INTERVAL 7 DAY 
GROUP BY YEAR(a.created_at)");
		if($result->num_rows() > 0)
		{	
		    return $result->result_array();
		}
		
		else
		{
			return false;
		}
		
	}
	
	
	
	public function getCurrentOrdres($user_id,$as_array=false, $idOrderBy='asc', $orderBy='id'){
	    $query = "SELECT * FROM `orders` WHERE `user_id` = '".$user_id."' AND payment_order_status = 1 AND 
        (`order_status` = '1' 
        OR `order_status` = '2' OR `order_status` = '3' OR `order_status` = '4' OR `order_status` = '5')
         ORDER BY `id` DESC";
        $result = $this->db->query($query);
		if($result->num_rows() > 0){
		if($as_array){
			return $result->result_array();
		}
		    return $result->result();
		}else{
			return false;
		} 
	}
	
    public function getAllOrdersPaginate($limit,$start,$array){

        $this->db->limit($limit, $start);
        $this->db->where('payment_order_status', '1');
        if($array['order_status'] != "") {
            $this->db->where('order_status', $array['order_status']);
        }
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get("orders");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }



    public function getExportOrders()
    {
        $result = $this->db->query("select * from orders WHERE payment_order_status = '1' AND 
        order_status != '6' AND order_status != '0' AND order_status != '5' AND order_status != '10' ");

        if($result->num_rows() > 0)
        {
            return $result->result();
        }

        else
        {
            return false;
        }
    }
    
}

