<?php
Class Model_category extends Base_Model
{
	public function __construct()
	{
		parent::__construct("categories");		
	}
	

	public function getMaxItemOrderVal(){

        $result = $this->db->query('SELECT MAX(itme_order) as itme_order FROM categories ');

        if($result->num_rows() > 0)
        {
            return $result->row();
        }
        else
        {
            return false;
        }
    }
	
}