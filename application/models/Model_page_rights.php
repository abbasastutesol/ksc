<?php
Class Model_page_rights extends Base_Model
{
    public function __construct()
    {
        parent::__construct("user_rights");
    }
    function allUserRights($id)
    {
        $this->db->select('*');
        $this->db->from('user_rights');
        $this->db->where('user_role_id', $id);
        $this->db->order_by('page_id', 'ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return $query->result();
        }
    }
    function fetchUserRightsRecord($page_id, $user_role_id)
    {
        $this->db->select('*');
        $this->db->from('user_rights');
        $this->db->where('user_role_id', $user_role_id);
        $this->db->where('page_id', $page_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return $query->result();
        }
    }
    public function fetchUserPageExcess($user_role_id)
    {
        $this->db->select('u_r.*,p.page_name');
        $this->db->from('pages p');
        $this->db->join('user_rights u_r', 'u_r.page_id = p.id', 'LEFT OUTER');
        $this->db->where('u_r.user_role_id', $user_role_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
}
