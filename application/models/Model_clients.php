<?php

Class Model_clients extends Base_Model

{

	public function __construct()

	{

		parent::__construct("clients");		

	}

	
       public function allClients()
	   
	   {
		   $query = $this->db->get('clients');
		    if( $query->num_rows() > 0)
			{
				
				return $query->result();
			}else{
				
				return false;
			}
		   
	   }
	   
	   public function getClient($id)
	   
	   {
		   $query = $this->db->select('clients.*,page.eng_meta_title,page.eng_meta_description,page.	    eng_meta_keyword,page.arb_meta_title,page.arb_meta_description,page.arb_meta_keyword')
         ->from('clients')
         ->join('page', 'clients.page_id = page.id')
		 ->where('clients.id',$id);
		 
         $result = $this->db->get();

		return $result->result();
		
		  /*  $query = $this->db->get_where('clients',array('id' => $id));
		    if( $query->num_rows() > 0)
				
			{
				
				return $query->result();
				
			}else{
				
				return false;
			} */
		   
	   }
	   
	/* public function validateLogin($email,$password)

	{

		$this->db->select('*');

		$this->db->from('users');

		$this->db->where('email',$email);

		$this->db->where('password',$password);
        
		$query = $this->db->get();

		//echo $this->db->last_query();exit();

		

		if (!empty($query->result())) {

			   return $query->result_array();

		}else{

			   return false;

		} 

	}	 */

	

	

}