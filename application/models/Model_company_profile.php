<?php
Class Model_company_profile extends Base_Model
{
	public function __construct()
	{
		parent::__construct("company_profile");
	}
	
		public function getProfile()
	{
		
		
		$query = $this->db->select('company_profile.*,page.eng_meta_title,page.eng_meta_description,page.	    eng_meta_keyword,page.arb_meta_title,page.arb_meta_description,page.arb_meta_keyword')
         ->from('company_profile')
         ->join('page', 'company_profile.page_id = page.id');
         $result = $this->db->get();

		return $result->result();
	}
	
	
}