<?php

Class Model_CeoMessage extends Base_Model

{

	public function __construct()

	{

		parent::__construct("ceomessage");		

	}

	public function getmessage()
	{
		
		
		$query = $this->db->select('ceomessage.*,page.eng_meta_title,page.eng_meta_description,page.	    eng_meta_keyword,page.arb_meta_title,page.arb_meta_description,page.arb_meta_keyword')
         ->from('ceomessage')
         ->join('page', 'ceomessage.page_id = page.id');
         $result = $this->db->get();

		return $result->result();
	}

	public function validateLogin($email,$password)

	{

		$this->db->select('*');

		$this->db->from('users');

		$this->db->where('email',$email);

		$this->db->where('password',$password);
        
		$query = $this->db->get();

		//echo $this->db->last_query();exit();

		

		if (!empty($query->result())) {

			   return $query->result_array();

		}else{

			   return false;

		} 

	}	

	

	

}