<?php
Class Model_job_applied extends Base_Model
{
	public function __construct()
	{
		parent::__construct("job_applied");		
	}
		
	public function getAllAppliedJobs()
	{
		$query = $this->db->query("SELECT ja.id, ja.name, ja.email,ja.mobile,ja.cv, ja.lang, ja.created_at as created_at, j.id as job_id, j.eng_title as en_title, j.arb_title as ar_title FROM `job_applied` ja, `jobs` j WHERE ja.job_id = j.id AND ja.job_id!=0 ORDER BY id DESC;");
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	public function getAllCV()
	{
		$query = $this->db->query("SELECT * FROM `job_applied` WHERE job_id=0 ORDER BY id DESC;");
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
}