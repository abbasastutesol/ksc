<?php
Class Model_product_variant_group extends Base_Model
{
	public function __construct()
	{
		parent::__construct("product_variant_groups");		
	}
	
	public function fetchPrice($product_id, $option)
	{
		$from = '';
		$where = '';
		$i=1;
		$lastelement = sizeof($option);
		foreach($option as $opt)
		{
			$from .= ' product_variant_values pvv'.$i.',';
			$opt_arr = explode('|', $opt);
			$where .= ' pvv'.$i.'.variant_id = '.$opt_arr[0].' and pvv'.$i.'.variant_value_id = '.$opt_arr[1];
			if($lastelement > $i)
			{
				$where .= ' and pvv'.$i.'.product_variant_group_id = pvv'.($i+1).'.product_variant_group_id and';
			}
			$i++;
		}
		$from = rtrim($from, ',');
		$result = $this->db->query('select * from product_variant_groups where id in ( select pvv1.product_variant_group_id from '.$from.' where '.$where.') and `product_id` = '.$product_id);
		if($result->num_rows() > 0)
		{	
		    return $result->row();
		}
		
		else
		{
			return false;
		} 
	}
	
	public function updateQuantity($product_id, $option, $quantity)
	{
		$from = '';
		$where = '';
		$i=1;
		$lastelement = sizeof($option);
		foreach($option as $opt)
		{
			$from .= ' product_variant_values pvv'.$i.',';
			$opt_arr = explode('|', $opt);
			$where .= ' pvv'.$i.'.variant_id = '.$opt_arr[0].' and pvv'.$i.'.variant_value_id = '.$opt_arr[1];
			if($lastelement > $i)
			{
				$where .= ' and pvv'.$i.'.product_variant_group_id = pvv'.($i+1).'.product_variant_group_id and';
			}
			$i++;
		}
		$from = rtrim($from, ',');
		$result = $this->db->query('update product_variant_groups set eng_quantity = (eng_quantity-'.$quantity.'), arb_quantity = (arb_quantity-'.$quantity.')  where id in ( select pvv1.product_variant_group_id from '.$from.' where '.$where.') and `product_id` = '.$product_id);
		
		return true;
	}
		
	
	
}