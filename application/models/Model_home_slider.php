<?php

Class Model_home_slider extends Base_Model

{

    public function __construct()

    {

        parent::__construct("home_slider");

    }

    public function getWithOrder(){
        $sql = "SELECT * FROM home_slider ORDER BY slider_order ASC ";
        $result = $this->db->query($sql);

        if($result->num_rows() > 0)
        {
            return $result->result();
        }

        else
        {
            return false;
        }
    }
    

}