<?php
Class Model_coupons extends Base_Model
{
	public function __construct()
	{
		parent::__construct("coupons");		
	}
	
	
	public function getCouponOnFilter($data)
	{
		$query = $this->db->query("SELECT * FROM `coupons` WHERE start_date >= '".$data['start_date']."' AND end_date <= '".$data['end_date']."'");
	
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	public function checkVoucherCode($voucher_code, $cart_total = '',$uses = 0)
	{
		$sql = "SELECT c.* FROM coupons c
        WHERE c.code = '".$voucher_code."' 
        AND c.start_date <= '".date('Y-m-d')."' 
        AND c.end_date >= '".date('Y-m-d')."' ";

		if($cart_total != '') {
            $sql .= " AND c.status = 1 AND c.total_amount <= '" . $cart_total . "' ";
        }
        if($uses != 0) {
            $sql .= " AND c.uses_per_coupon > (SELECT count(*) from coupon_use cu 
        where cu.coupon_id = c.id) LIMIT 1";
        }

        //echo $sql; exit;
	    $query = $this->db->query($sql);

		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}

	}

	public function couponIsExpire($voucher_code){
        $sql = "SELECT c.* FROM coupons c
        WHERE c.code = '".$voucher_code."'
        AND status = '1'
        AND c.start_date <= '".date('Y-m-d')."' 
        AND c.end_date >= '".date('Y-m-d')."' ";

        $query = $this->db->query($sql);

        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return false;
        }
    }

	
	public function checkVoucherCodeProduct($product_id, $coupon_id)
	{
		$query = $this->db->query("SELECT * FROM coupon_products WHERE coupon_id = ".$coupon_id." AND product_id = ".$product_id.";");
	
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
		
	}
	
	public function checkVoucherCodeCategory($category_id, $coupon_id)
	{
		$query = $this->db->query("SELECT * FROM `coupon_categories` WHERE coupon_id = ".$coupon_id." AND category_id = ".$category_id.";");
	
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
		
	}

	public function checkCouponProdExist($code){

	    $sql =  "SELECT * FROM `coupons` c JOIN coupon_products cc ON (c.id = cc.coupon_id) WHERE c.code = '".$code."' ";

        $query = $this->db->query($sql);

        return $query->num_rows();
    }

    public function checkCouponCatExist($code){

        $sql =  "SELECT * FROM `coupons` c JOIN coupon_categories cp ON (c.id = cp.coupon_id) WHERE c.code = '".$code."' ";

        $query = $this->db->query($sql);

        return $query->num_rows();

    }
	
}

