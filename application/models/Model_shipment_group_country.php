<?php

Class Model_shipment_group_country extends Base_Model

{

	public function __construct()

	{

		parent::__construct("shipment_group_country");

	}

	
	public function getActiveGroupCountries()
	{
		$query = $this->db->query("select sgc.* from shipment_group_country sgc, shipment_groups sg WHERE sg.id = sgc.group_id AND sg.active_status =1;");
		
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return array();
		}
	}
    public function getCitiesByCountryGroup($countryCode)
    {

        $this->db->select('c.*');
        $this->db->from('city as c');
        $this->db->join('shipment_group_country as sgc', 'c.id = sgc.city_id');
        $this->db->where('c.countrycode', $countryCode);
        $query = $this->db->get();

        if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
    }

	

}