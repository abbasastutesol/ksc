<?php
Class Model_product_rating extends Base_Model
{
	public function __construct()
	{
		parent::__construct("product_rating");		
	}
	
	public function getAverageRating($id)
	{
		$query = $this->db->query("SELECT round(avg(rating)) as rating FROM `product_rating` where product_id = '".$id."'");
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}
		
	
	
}