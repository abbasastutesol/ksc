<?php
Class Model_user_rights extends Base_Model
{
    public function __construct()
    {
        parent::__construct("pages");
    }
    function update($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('pages', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
    function getShoppersOnMarketLocation($id)
    {
        $this->db->select('*');
        $this->db->from('shopper_driver');
        $this->db->where('market_location', $id);
        $this->db->where('active_status', '1');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    function getActiveShoppersOnMarketLocation($id)
    {
        $this->db->select('*');
        $this->db->from('shopper_driver');
        $this->db->where('market_location', $id);
        $this->db->where('active_status', '1');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    function insertUserRights($data_new)
    {
        $this->db->insert('user_rights', $data_new);
        return $this->db->insert_id();
    }
    function updateUserRights($insert_arr, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('user_rights', $insert_arr);
        if ($this->db->affected_rows() > 0) {
            return $this->db->affected_rows();
        } else {
            return false;
        }
    }
}

