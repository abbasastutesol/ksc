<?php
Class Model_value extends Base_Model
{
	public function __construct()
	{
		parent::__construct("value");		
	}
	
		public function getData()
	{
		
		$query = $this->db->select('value.*,page.eng_meta_title,page.eng_meta_description,page.	    eng_meta_keyword,page.arb_meta_title,page.arb_meta_description,page.arb_meta_keyword')
         ->from('value')
         ->join('page', 'value.page_id = page.id');
         $result = $this->db->get();

		return $result->result();
	}
	
	
}