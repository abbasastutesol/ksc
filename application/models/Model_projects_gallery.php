<?php

Class Model_projects_gallery extends Base_Model

{

	public function __construct()

	{

		parent::__construct("project_gallery_list");		

	}

	
       public function allProjects()
	   
	   {
		   $query = $this->db->get('projects');
		    if( $query->num_rows() > 0)
			{
				
				return $query->result();
			}else{
				
				return false;
			}
		   
	   }
	   
	   public function getProjects($id)
	   
	   {
		$result = $this->db->get_where('project_gallery_list', array('id' => $id));
           
		return $result->result();
		   
	   }
	   
	   public function Projects_gallery()
	   
	   {
		    
		    $this->db->select('project_gallery_list.*,projects.eng_title as eng_parent,projects.arb_title as arb_parent')
           ->from('project_gallery_list')->join('projects', 'project_gallery_list.project_id = projects.id');
           $result = $this->db->get();
		   //echo $this->db->last_query(); exit;
		   return $result->result();
		   
	   }
	   
	   public function get_Project_gallery($id)

	   {     //echo $id; exit;
	   $query= $this->db->query('SELECT project_gallery_list.*, projects.eng_title as eng_parent,projects.arb_title as arb_parent,projects.arb_description as arb_des,projects.eng_description as eng_des,projects.banner_image FROM project_gallery_list, projects WHERE project_gallery_list.project_id = projects.id AND projects.id = '.$id.'');
	   
          if ($query->num_rows() > 0) {
		   return $query->result_array();
			
        } else {

            return array();

        }
	
		    /* $this->db->select('project_gallery_list.*,projects.eng_title as eng_parent,projects.arb_title as arb_parent,projects.banner_image')->from('project_gallery_list')->join('projects', 'project_gallery_list.project_id = projects.id')->where('projects.id',$id);
			echo $this->db->last_query(); exit;
		    $result = $this->db->get(); */
           
		 
		   /* return $result->result(); */
		   
	   }
	   
	/* public function validateLogin($email,$password)

	{

		$this->db->select('*');

		$this->db->from('users');

		$this->db->where('email',$email);

		$this->db->where('password',$password);
        
		$query = $this->db->get();

		//echo $this->db->last_query();exit();

		

		if (!empty($query->result())) {

			   return $query->result_array();

		}else{

			   return false;

		} 

	}	 */

	

	

}