<?php

Class Model_award extends Base_Model

{

	public function __construct()

	{

		parent::__construct("awards");		

	}

	
       public function allAwards()
	   
	   {
		   $query = $this->db->get('awards');
		    if( $query->num_rows() > 0)
			{
				
				return $query->result();
			}else{
				
				return false;
			}
		   
	   }
	   
	   public function getAward($id)
	   
	   { 
	  
			$query = $this->db->select('awards.*,page.eng_meta_title,page.eng_meta_description,page.	    eng_meta_keyword,page.arb_meta_title,page.arb_meta_description,page.arb_meta_keyword')
			 ->from('awards')
			 ->join('page', 'awards.page_id = page.id')
			 ->where('awards.id',$id);
			 //echo $this->db->last_query(); exit;
			 $result = $this->db->get();

			return $result->result();
		   
	   }
	   
	/* public function validateLogin($email,$password)

	{

		$this->db->select('*');

		$this->db->from('users');

		$this->db->where('email',$email);

		$this->db->where('password',$password);
        
		$query = $this->db->get();

		//echo $this->db->last_query();exit();

		

		if (!empty($query->result())) {

			   return $query->result_array();

		}else{

			   return false;

		} 

	}	 */

	

	

}