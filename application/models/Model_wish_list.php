<?php
Class Model_wish_list extends Base_Model
{
	public function __construct()
	{
		parent::__construct("wish_list");		
	}
	
	public function getWishListProducts($user_id)
	{
		$result = $this->db->query("select p.*, c.eng_name as eng_category_name, c.arb_name as arb_category_name from products p, wish_list wl, categories c where p.id = wl.product_id and c.id = p.category_id and wl.user_id = ".$user_id);
		if($result->num_rows() > 0)
		{	
		    return $result->result_array();
		}
		
		else
		{
			return false;
		} 
	}
	
	
}