<?php
Class Model_general extends CI_Model
{
	public function __construct()
	{
		parent::__construct();		
	}
	
	public function getRow($id, $table_name, $as_array=false)
	{
		$result = $this->db->get_where($table_name,array('id'=>$id));
		//echo $this->db->last_query(); exit();
		if($result->num_rows() > 0)
		{
			$row = $result->row_array();
			
			if($as_array)
			{
				return $row;
			}
			
			foreach ($row as $col=>$val)
			{
				$this->$col = $val;
			}
			
			return $this;
		}
		
		else
		{
			return false;
		} 
	}
	
	public function getSingleRow($table_name,$field,$as_array=false)
	{
		$result = $this->db->get_where($table_name,$field);
		
		if($result->num_rows() > 0)
		{
			$row = $result->row_array();
			
			if($as_array)
			{
				return $row;
			}
			
			foreach ($row as $col=>$val)
			{
				$this->$col = $val;
			}
			
			return $this;
		}
		
		else
		{
			return false;
		} 
	}
	
	public function getAll($table_name, $as_array=false, $idOrderBy='asc')
	{
		if($idOrderBy=='desc')
			$this->db->order_by('id','desc');
		
		$result = $this->db->get($table_name);
		
		if($as_array)
		{
			return $result->result_array();
		}
		
		return $result->result();
	}
	
	public function getMultipleRows($table_name, $fields,$as_array=false, $idOrderBy='asc', $orderBy='id')
	{
		
		
		$this->db->order_by($orderBy,$idOrderBy);
	
		$result = $this->db->get_where($table_name,$fields);
		
		//echo $this->db->last_query(); exit();
		
		if($result->num_rows() > 0)
		{
			
			
			if($as_array)
		    {
			 return $result->result_array();
		    }
		
		    return $result->result();
		}
		
		else
		{
			return false;
		} 
	}
	
	public function getCityName($city_name)
	{
		$result = $this->db->query("select * from city where eng_name = '".$city_name."' OR arb_name = '".$city_name."'");
		//echo $this->db->last_query(); exit();
		if($result->num_rows() > 0)
		{
			return $result->row();
		}
		
		else
		{
			return false;
		} 
	}
	
	public function updateOutOfStock()
	{
		$this->db->query("UPDATE `products` SET `active`=0 WHERE `out_of_stock_date` < DATE_SUB(NOW(), INTERVAL 30 DAY) AND `out_of_stock_date` != '0000-00-00' AND `out_of_stock` = 1");
		if ($this->db->affected_rows() == '1') {
			return true;
		} else {
			// any trans error?
			if ($this->db->trans_status() === FALSE) {
				return false;
			}
		}
		return true;
	}
	
	
}