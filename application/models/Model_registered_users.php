<?php
Class Model_registered_users extends Base_Model
{
	public function __construct()
	{
		parent::__construct("registered_users");		
	}
	
		
	public function registeredUsersChartWeek()
	{
		$result = $this->db->query("SELECT count(a.id) as count, DATE(a.created_at) as created_date FROM registered_users a WHERE a.created_at >= DATE(NOW()) - INTERVAL 7 DAY GROUP BY DATE(a.created_at) Order BY DATE(a.created_at)");
		if($result->num_rows() > 0)
		{	
		    return $result->result_array();
		}
		
		else
		{
			return false;
		}
		
	}
	
	public function registeredUsersChartMonth()
	{
		$result = $this->db->query("SELECT count(a.id) as count, MONTH(a.created_at) as created_date FROM registered_users a GROUP BY MONTH(a.created_at) Order BY MONTH(a.created_at)");
		if($result->num_rows() > 0)
		{	
		    return $result->result_array();
		}
		
		else
		{
			return false;
		}
		
	}
	
	public function registeredUsersChartYear()
	{
		$result = $this->db->query("SELECT count(a.id) as count, YEAR(a.created_at) as created_date FROM registered_users a GROUP BY YEAR(a.created_at) Order BY YEAR(a.created_at)");
		if($result->num_rows() > 0)
		{	
		    return $result->result_array();
		}
		
		else
		{
			return false;
		}
		
	}	
	
	public function check_userEmail_exist($email){	
		$result = $this->db->query("SELECT * from registered_users WHERE email='".$email."'");		if($result->num_rows() > 0)		{
	
			return $result->result_array();	
			
		}else{	
			
		return false;
		
		}
	}
	
}