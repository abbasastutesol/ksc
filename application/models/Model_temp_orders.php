<?php
Class Model_temp_orders extends Base_Model
{
	public function __construct()
	{
		parent::__construct("temp_orders");		
	}
		
	public function getProductDetail($id, $lang)
	{
		$result = $this->db->query('SELECT c.'.$lang.'_name as category_name, p.'.$lang.'_name as product_name, p.'.$lang.'_price as product_price, p.has_discount_price, p.'.$lang.'_discount_price as product_discount_price FROM products p, categories c WHERE p.category_id=c.id and p.id='.$id);
		
		if($result->num_rows() > 0)
		{	
		    return $result->row();
		}
		
		else
		{
			return false;
		} 
	}
	
}