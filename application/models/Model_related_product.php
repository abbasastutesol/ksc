<?php
Class Model_related_product extends Base_Model
{
	public function __construct()
	{
		parent::__construct("related_product");		
	}
	
		
	public function getRelatedProducts($id)
	{
		$result = $this->db->query('SELECT p.* FROM products p, related_product rp WHERE p.id = rp.related_product_id and rp.product_id='.$id);
		
		if($result->num_rows() > 0)
		{	
		    return $result->result_array();
		}
		
		else
		{
			return false;
		} 
	}
	
}