<?php

Class Model_projects extends Base_Model

{

	public function __construct()

	{

		parent::__construct("projects");		

	}

	
       public function allProjects()
	   
	   {
		   $query = $this->db->get('projects');
		    if( $query->num_rows() > 0)
			{
				
				return $query->result();
			}else{
				
				return false;
			}
		   
	   }
	   
	   public function getProjects($id)
	   
	   {
			$query = $this->db->select('projects.*,page.eng_meta_title,page.eng_meta_description,page.	    eng_meta_keyword,page.arb_meta_title,page.arb_meta_description,page.arb_meta_keyword')
           ->from('projects')->join('page', 'projects.page_id = page.id')->where('projects.id',$id);
           $result = $this->db->get();
		   //echo $this->db->last_query(); exit;
		return $result->result();
		   
	   }
	   
	   
	   
	   public function Projects_gallery()
	   
	   {
		    
		    $this->db->select('project_gallery_list.*,projects.eng_title as eng_parent,projects.arb_title as arb_parent')
           ->from('project_gallery_list')->join('projects', 'project_gallery_list.project_id = projects.id');
           $result = $this->db->get();
		   //echo $this->db->last_query(); exit;
		   return $result->result();
		   
	   }
	   
	   
	   
	/* public function validateLogin($email,$password)

	{

		$this->db->select('*');

		$this->db->from('users');

		$this->db->where('email',$email);

		$this->db->where('password',$password);
        
		$query = $this->db->get();

		//echo $this->db->last_query();exit();

		

		if (!empty($query->result())) {

			   return $query->result_array();

		}else{

			   return false;

		} 

	}	 */

	

	

}