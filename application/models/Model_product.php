<?php
Class Model_product extends Base_Model
{
	public function __construct()
	{
		parent::__construct("products");		
	}
	
	public function fetcMaxMinPrice()
	{
		$query = $this->db->query("SELECT MIN(`eng_price`) as min_price , MAX(`eng_price`) as max_price
FROM  `products`");
		if($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return false;
		}
	}
    
	public function searchProduct($input,$array = true,$lang = 'eng')
	{
        $word = trim($input['search']);
        $category_id = $input['category_id'];
        $brand_id = $input['brand_id'];

		$sql = "SELECT p.*, c.eng_name as eng_cat_name, c.arb_name as arb_cat_name,
        b.eng_name as eng_brand_name,b.eng_name as arb_brand_name FROM products as p 
		JOIN categories as c ON c.id = p.category_id 
		JOIN brands as b ON b.id = p.brand_id 
		WHERE p.archive = 0 AND p.active = 1 ";

		if($category_id != ''){
		    $sql .= " AND p.category_id = '$category_id' ";
        }
        if($brand_id != ''){
            $sql .= "AND p.brand_id = '$brand_id' ";
        }
		
		if ($word !='') {
			$sql .= " AND ( p.".$lang."_name LIKE '%".$word."%' OR
			 p.".$lang."_description LIKE '%".$word."%'
			  OR c.".$lang."_name LIKE '%".$word."%' OR b.".$lang."_name LIKE '%".$word."%') ";
		}
        $sql .= " ORDER BY p.id DESC";
		$query = $this->db->query($sql);

		if($query->num_rows() > 0)
		{
		    if($array) {
                return $query->result_array();
            }else{
                return $query->result();
            }
		}
		else
		{
			return false;
		}
	}

	public function availableProduct($available)
    {
        $sql = "SELECT p.*, c.eng_name as eng_cat_name, c.arb_name as arb_cat_name FROM products as p 
		JOIN categories as c ON c.id = p.category_id ";
        if($available == 1){
            $sql .= " WHERE p.eng_quantity > 0 ";
        }else{
            $sql .= " WHERE p.eng_quantity = 0 ";
        }
        $sql .= " AND category_status = 1 ORDER BY id DESC";
        $query = $this->db->query($sql);
        if($query->num_rows() > 0)
        {
                return $query->result();
        }
        else
        {
            return false;
        }
    }

    public function search($search, $lang)
	{
		$min_price = $search['min_price'];
		$max_price = $search['max_price'];
		$min_range = $search['min_range'];
		$max_range = $search['max_range'];
		$categoryCheck = $search['categoryCheck'];
		$colorCheck = $search['colorCheck'];
		$sizeCheck = $search['sizeCheck'];
		$reviewCheck = $search['reviewCheck'];
		$category_id = $search['category_id'];
		$keyword = $search['keyword'];
		
		$sql = "SELECT p.* FROM products p LEFT OUTER JOIN product_variant_groups pvg ON p.id = pvg.product_id LEFT OUTER JOIN product_variant_values pvv ON pvg.id = pvv.product_variant_group_id LEFT OUTER JOIN product_rating pr ON p.id = pr.product_id WHERE p.active = 1";
		if ($category_id != '' && !$categoryCheck) {
			$sql .= " AND p.category_id = '".$category_id."'";
		}
		if ($keyword !='') {
			$sql .= " AND ( p.".$lang."_name LIKE '%".$keyword."%' OR p.".$lang."_description LIKE '%".$keyword."%' OR p.".$lang."_product_manufacturer LIKE '%".$keyword."%' ) ";
		}
		/*if ($min_range != '' && $max_range != '') {
			$sql .= " AND (p.eng_price <= '".$max_range."' AND p.eng_price >= '".$min_range."') ";
		}*/
		if($categoryCheck)
		{
			$sql .= " AND (";
			foreach($categoryCheck as $category)
			{
				$sql .= "p.category_id = '".$category."' OR ";
			}
			$sql = rtrim($sql, 'OR ');
			$sql .= ")";
		}
		if($colorCheck)
		{
			$sql .= " AND (";
			foreach($colorCheck as $color)
			{
				$sql .= "p.eng_color = '".$color."' OR pvv.variant_value_id >= '".$color."' OR ";
			}
			$sql = rtrim($sql, 'OR ');
			$sql .= ")";
		}
		if($sizeCheck)
		{
			$sql .= " AND (";
			foreach($sizeCheck as $size)
			{
				$sql .= "pvv.variant_value_id = '".$size."' OR ";
				/*if($size == '25')
				{
					$sql .= "p.eng_size_in_image = 'S' OR ";
				}elseif($size == '26')
				{
					$sql .= "p.eng_size_in_image = 'M' OR ";
				}elseif($size == '27')
				{
					$sql .= "p.eng_size_in_image = 'L' OR ";
				}elseif($size == '28')
				{
					$sql .= "p.eng_size_in_image = 'X' OR ";
				}*/
			}
			$sql = rtrim($sql, 'OR ');
			$sql .= ")";
		}
		if($reviewCheck)
		{
			$sql .= " AND (";
			foreach($reviewCheck as $review)
			{
				$sql .= "p.rating = '".$review."' OR ";
			}
			$sql = rtrim($sql, 'OR ');
			$sql .= ")";
		}
		
		$sql .= ' GROUP BY p.id';
		
		/*echo $sql;
		exit;*/
		
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}
	
	public function productsNotPurchased($data)
	{
		$where = '';
		if($data['start_date'] != '' && $data['end_date'] != '')
		{
			$where .= ' where DATE(created_at) >= '.date('Y-m-d', strtotime($data['start_date'])).' AND DATE(created_at) <= '.date('Y-m-d', strtotime($data['end_date']));
		}
		$query = $this->db->query("SELECT * FROM `products` WHERE `id` not in (select product_id from order_product ".$where.")");
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	public function lowStockProducts()
	{
		
		$query = $this->db->query("SELECT * FROM `products` WHERE eng_quantity < 5");
		return $query->num_rows();
		
	}
	
	public function sameCustomerProduct($id)
	{
		$query = $this->db->query("SELECT p.* FROM products p left join order_product op on op.product_id = p.id where op.order_id IN (select op.order_id from order_product op where op.product_id = ".$id.") and p.id != ".$id." GROUP BY p.id");
		return $query->result_array();
	}
	
	public function lowStockProductDetails()
	{
		
		$query = $this->db->query("SELECT * FROM `products` WHERE eng_quantity < 5");
		if($query->num_rows() > 0)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
		
	}
	
	public function fetchSelectedVariants($prod_id)
	{
		$sql = "SELECT v.eng_name, pvv.variant_id, pvv.variant_value_id FROM product_variant_values pvv, product_variant_groups pvg, variants v where pvg.id = pvv.product_variant_group_id and v.id = pvv.variant_id and pvg.product_id = ".$prod_id." ORDER BY pvv.variant_id";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}
	public function getProductWithCatBrand($field,$per_page=12,$start=0,$as_array=false){

        $this->db->select($this->table.'.*,brands.eng_name as eng_brand_name,brands.arb_name as arb_brand_name ,categories.eng_name as eng_cat_name,categories.arb_name as arb_cat_name');
        $this->db->from($this->table);
        $this->db->join('categories', $this->table.'.category_id = categories.id');
        $this->db->join('brands', $this->table.'.brand_id = brands.id');
        if(isset($field['active'])) {
            $this->db->where($this->table . '.active', $field['active']);
        }
        if(isset($field['display_to_home_product'])) {
            $this->db->where($this->table . '.display_to_home_product', $field['display_to_home_product']);
        }
        if(isset($field['category_name'])) {
            $this->db->where('categories.eng_name', $field['category_name']);
        }
		
		if(isset($field['category_id'])) {
            $this->db->where('categories.id', $field['category_id']);
        }
		if(isset($field['category_status'])) {
            $this->db->where($this->table .'.category_status', $field['category_status']);
        }
        if(isset($field['id'])) {
            $this->db->where($this->table .'.id', $field['id']);
        }if(isset($field['daily_offer'])) {
            $this->db->where($this->table .'.daily_offer', $field['daily_offer']);
        }
        $this->db->order_by('categories.itme_order', 'asc');
        $this->db->limit($per_page, $start);
        $query = $this->db->get();
		if($query->num_rows() > 0) {
			if($as_array) {
				return $query->result_array();
			}else {
				return $query->result();
			}
		}else{
		
			return false;
		}
    }

    public function getProductColor($prod_id,$as_array=false){

        $this->db->select('product_color.*,colors.eng_name as eng_color_name,colors.arb_name as arb_color_name');
        $this->db->from('colors');
        $this->db->join('product_color', 'colors.id = product_color.color_id');

        $this->db->where('product_color.product_id', $prod_id);


        $query = $this->db->get();

		if($query->num_rows() > 0) {
			if($as_array) {
				return $query->result_array();
			}else {
				return $query->result();
			}
		}else{

			return false;
		}
    }

	
	public function getOfferByDates($is_home_offer = 0, $daily_offer = 1,$category_status = 1)
	{	
		$sql = "SELECT * FROM products WHERE  DATE(end_date) >= DATE(NOW())
		And DATE(start_date) <= DATE(NOW()) And active = 1"; 
		if($is_home_offer == 1){
			$sql .= " AND display_to_home_offer = '".$is_home_offer."'";
		}
		if($category_status == 1){
			$sql .= " AND category_status = '".$category_status."'";
		}
		$sql .= " AND daily_offer = '".$daily_offer."' ";
        $sql .= " ORDER BY id DESC";
		$query = $this->db->query($sql);
		if($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}
    public function getAllProductPaginate($limit,$start,$available = ''){
        if( $available != '' && $available == 1){
            $this->db->where('eng_quantity >', '0');
        }if($available != '' && $available == 0){
            $this->db->where('eng_quantity', '0');
        }
        $this->db->where('archive', '0');
	    $this->db->limit($limit, $start);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get("products");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
	
	public function get_products(){
		
		$sql = "SELECT products.id,products.eng_name,products.arb_name,products.eng_description,products.arb_description ,products.arb_short_des,products.eng_short_des FROM products ORDER BY products.created_at desc Limit 8";
		
        $query = $this->db->query($sql);
		//echo $this->db->last_query(); exit;
		return $query->result();
		
	}

    public function product_count() {
        $sql = "SELECT COUNT(id) as count FROM products WHERE active = 1";
        $query = $this->db->query($sql);
        return $query->row()->count;
    }
	
}