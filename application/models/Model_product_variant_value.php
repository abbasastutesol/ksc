<?php
Class Model_product_variant_value extends Base_Model
{
	public function __construct()
	{
		parent::__construct("product_variant_values");		
	}
	
		
	public function checkVarient($g_id)
	{
		$result = $this->db->query('SELECT pvv.variant_value_id, pvv.variant_id FROM product_variant_groups pvg, product_variant_values pvv WHERE pvv.product_variant_group_id=pvg.id and pvv.product_variant_group_id='.$g_id.' order by variant_id');
		
		if($result->num_rows() > 0)
		{	
		    return $result->result();
		}
		
		else
		{
			return false;
		} 
	}
	
}