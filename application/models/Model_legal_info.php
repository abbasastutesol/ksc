<?php

Class Model_legal_info extends Base_Model
{
	public function __construct()
	{
		parent::__construct("legal_info");
	}
	
		public function legal_info()
	{
		 $query = $this->db->query('SELECT lg.*,pg.eng_meta_title,pg.eng_meta_description,pg.eng_meta_keyword,pg.arb_meta_title,pg.arb_meta_description,pg.arb_meta_keyword FROM `legal_info` lg LEFT JOIN `page` pg ON lg.page_id = pg.id');
		
        if ($query->num_rows() > 0) {
			
            return  $query->result();
        } else {
            return false;
        }
		
			 
	}
	
	
}