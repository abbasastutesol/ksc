<?php
Class Model_jobs extends Base_Model
{
	public function __construct()
	{
		parent::__construct("jobs");		
	}
	
		
	public function get_job($id)
	{
		 $query = $this->db->query('SELECT j.*,pg.eng_meta_title,pg.eng_meta_description,pg.eng_meta_keyword,pg.arb_meta_title,pg.arb_meta_description,pg.arb_meta_keyword FROM `jobs` j LEFT JOIN `page` pg ON j.page_id = pg.id 
        WHERE j.id = '.$id.' ');
		
        if ($query->num_rows() > 0) {
			
            return  $query->result();
        } else {
            return false;
        }
		
			 
	}
}