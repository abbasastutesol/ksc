<?php
Class Model_report extends Base_Model
{
	public function __construct()
	{
		parent::__construct("orders");
	}
    
    public function getSalesOrders($data = array()) {

        $sql = "SELECT MIN(o.created_at) AS date_start, MAX(o.created_at) AS date_end, 
        COUNT(DISTINCT o.id) AS orders,
        SUM((SELECT SUM(op.quantity) as quantity FROM order_product op 
        WHERE op.order_id = o.id 
        GROUP BY op.order_id)) AS products, 
         SUM(DISTINCT o.total_amount/o.currency_rate) AS total FROM orders o 
         WHERE o.order_status != '' AND o.payment_order_status = '1' ";

        if ($data['status'] != '') {
            $sql .= " AND o.order_status = '" . (int)$data['status'] . "' ";
        }
        if($data['filter_date_start'] != '') {
            $sql .= " AND DATE(o.created_at) >= '" . $data['filter_date_start'] . "' ";
        }
        if($data['filter_date_end']) {
            $sql .= " AND DATE(o.created_at) <= '" . $data['filter_date_end'] . "' ";
        }

        $group = $data['group'];

        switch($group) {
            case 'day';
                $sql .= " GROUP BY YEAR(o.created_at), MONTH(o.created_at), DAY(o.created_at)";
                break;
            default:
            case 'week':
                $sql .= " GROUP BY YEAR(o.created_at), WEEK(o.created_at)";
                break;
            case 'month':
                $sql .= " GROUP BY YEAR(o.created_at), MONTH(o.created_at)";
                break;
            case 'year':
                $sql .= " GROUP BY YEAR(o.created_at)";
                break;
        }
        $sql .= " ORDER BY o.created_at DESC";

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getShippingOrders($data = array(),$type) {

        $group = $data['group'];

        $sql = "SELECT MIN(o.created_at) AS date_start,
         MAX(o.created_at) AS date_end, sm.eng_name AS shipment_method, 
         o.shipment_type AS shipment_type,
         COUNT(o.id) AS orders,SUM(o.total_amount/o.currency_rate) AS total FROM orders o 
         LEFT JOIN shipment_methods sm ON (o.shipment_method = sm.id) 
         WHERE o.shipment_type != '' AND o.payment_order_status = '1' ";

        if ($data['status'] != '') {
            $sql .= " AND o.order_status = '" . (int)$data['status'] . "' ";
        }
        if ($data['filter_date_start'] != '') {
            $sql .= " AND DATE(o.created_at) >= '" . $data['filter_date_start'] . "'";
        }
        if ($data['filter_date_end'] != '') {
            $sql .= " AND DATE(o.created_at) <= '" . $data['filter_date_end'] . "'";
        }

        // switch cases for group
        switch($group) {
            case 'day';
                $sql .= " GROUP BY YEAR(o.created_at), MONTH(o.created_at), DAY(o.created_at)";
                break;
            default:
            case 'week':
                $sql .= " GROUP BY YEAR(o.created_at), WEEK(o.created_at)";
                break;
            case 'month':
                $sql .= " GROUP BY YEAR(o.created_at), MONTH(o.created_at)";
                break;
            case 'year':
                $sql .= " GROUP BY YEAR(o.created_at)";
                break;
        }

        //$sql .= " GROUP BY o.shipment_type ";
        $sql .= " ORDER BY o.created_at DESC ";
       // echo $sql; exit;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getSalesReturns($data = array()) {

        $group = $data['group'];

        $sql = "SELECT MIN(o.created_at) AS date_start,
         MAX(o.created_at) AS date_end,
         COUNT(o.id) AS returns FROM orders o 
         WHERE o.order_status = '8' AND o.payment_order_status = '1' ";

        if($data['filter_date_start'] != '') {
            $sql .= " AND DATE(o.created_at) >= '" . $data['filter_date_start'] . "'";
        }
        if($data['filter_date_end'] != '') {
            $sql .= " AND DATE(o.created_at) <= '" . $data['filter_date_end'] . "'";
        }

        // switch cases for group
        switch($group) {
            case 'day';
                $sql .= " GROUP BY YEAR(o.created_at), MONTH(o.created_at), DAY(o.created_at)";
                break;
            default:
            case 'week':
                $sql .= " GROUP BY YEAR(o.created_at), WEEK(o.created_at)";
                break;
            case 'month':
                $sql .= " GROUP BY YEAR(o.created_at), MONTH(o.created_at)";
                break;
            case 'year':
                $sql .= " GROUP BY YEAR(o.created_at)";
                break;
        }
        $sql .= " ORDER BY o.created_at DESC";
        //echo $sql; exit;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getTotalViews(){
	    $sql = "SELECT SUM(viewed) AS total FROM products ";

	    $query = $this->db->query($sql);

        return $query->row()->total;
    }

    public function getProductViewed(){

        $sql = "SELECT p.eng_name as product_name,p.viewed,c.eng_name as cat_name 
        FROM products p LEFT JOIN categories c ON 
        (p.category_id = c.id) WHERE p.viewed > 0 ";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function getPurchasedProduct($data = array()) {

        $sql = "SELECT p.eng_name as product,p.category_id,
       SUM(op.quantity) as quantity, SUM(o.total_amount/o.currency_rate) AS total
        FROM order_product op 
        left JOIN orders o ON (op.order_id = o.id)
        left JOIN products p ON (op.product_id = p.id)
        WHERE o.order_status = '6' AND o.payment_order_status = '1' ";

        if($data['filter_date_start'] != '') {
            $sql .= " AND DATE(o.created_at) >= '" . $data['filter_date_start'] . "'";
        }
        if($data['filter_date_end'] != '') {
            $sql .= " AND DATE(o.created_at) <= '" . $data['filter_date_end'] . "'";
        }

        $sql .=" GROUP BY op.product_id, p.eng_name ";

        //echo $sql; exit;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getCustomerOrders($data = array()) {

        $sql = "SELECT * FROM (SELECT  ru.id,ru.first_name,ru.last_name,
               ru.email, COUNT(DISTINCT  o.id) AS orders,
                SUM(op.quantity) AS products,
               SUM(DISTINCT o.total_amount/o.currency_rate) AS total,o.id as order_id,o.created_at,o.order_status
               FROM orders o 
               JOIN registered_users ru ON (o.user_id = ru.id)
               JOIN order_product op ON (o.id = op.order_id)
               WHERE o.payment_order_status = '1' ";

        if ($data['status'] != '') {
            $sql .= " AND o.order_status = '" . (int)$data['status'] . "' ";
        }

        $sql .= " GROUP BY ru.id ";

        $sql .= " UNION

               SELECT  cua.id,cua.full_name as first_name, '' as last_name,cua.email,
               COUNT(DISTINCT o.id) AS orders,
               SUM(op.quantity) AS products,
              SUM(DISTINCT o.total_amount/o.currency_rate) AS total,o.id as order_id,o.created_at,o.order_status
               FROM orders o 
               JOIN cart_user_address cua ON (o.guest_user_id = cua.id)
               JOIN order_product op ON (o.guest_user_id = op.guest_user_id)
               WHERE o.payment_order_status = '1' ";

        if ($data['status'] != '') {
            $sql .= " AND o.order_status = '" . (int)$data['status'] . "' ";
        }

        $sql .= " GROUP BY cua.id ) t1 where t1.id != 0 ";

        /*if ($data['status'] != '') {
            $sql .= " AND t1.order_status = '" . (int)$data['status'] . "' ";
        }*/
        if ($data['customer'] != '') {
            $sql .= " AND t1.id = '" . (int)$data['customer'] . "' ";
        }
        if ($data['filter_date_start'] != '') {
            $sql .= " AND  DATE(t1.created_at) >= '" . $data['filter_date_start'] . "' ";
        }
        if ($data['filter_date_end'] != '') {
            $sql .= " AND  DATE(t1.created_at) <= '" . $data['filter_date_end'] . "'";
        }


        $sql .= " ORDER BY t1.id DESC";

        //echo $sql; exit;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getCoupons($data = array()) {

        $sql = "SELECT c.coupon_name,c.code,COUNT(o.id) as orders,
        SUM(o.total_amount/o.currency_rate) as total FROM orders o 
        JOIN coupons c ON (o.coupon_id = c.id) WHERE o.payment_order_status = '1' ";

        if($data['filter_date_start'] != '') {
            $sql .= " AND DATE(o.created_at) >= '" . $data['filter_date_start'] . "' ";

        }
        if($data['filter_date_end'] != '') {
            $sql .= " AND DATE(o.created_at) <= '" . $data['filter_date_end'] . "'";
        }

        $sql .=" GROUP BY c.id ";

        //echo $sql; exit;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

}
