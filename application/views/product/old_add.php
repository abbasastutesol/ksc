<div id="page_content">

        <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">

            <h1 id="product_edit_name">Add a product</h1>

            <span class="uk-text-muted uk-text-upper uk-text-small" id="product_edit_sn"></span>

        </div>

        <div id="page_content_inner">

            <form action="<?php echo base_url(); ?>admin/product/action" method="post" onsubmit="return false" class="uk-form-stacked ajax_form" id="product_edit_form" enctype="multipart/form-data">

				<input type="hidden" name="form_type" value="save">

                <div class="uk-grid uk-grid-medium" data-uk-grid-margin>

                    

                    <div class="uk-width-xLarge-10-10  uk-width-large-10-10">

                        <div class="md-card">

                            <div class="md-card-toolbar">

                                <h3 class="md-card-toolbar-heading-text">

                                    Details

                                </h3>

                            </div>

                            <div class="md-card-content large-padding">

                                <div class="uk-grid uk-grid-divider uk-grid-medium" data-uk-grid-margin>

                                	<div class="uk-width-large-1-2">

                                     </div>

                                     <div class="uk-width-large-1-2">

                                        <div class="uk-form-row">

                                            <input type="hidden" class="md-input" id="" name="" value=""/>

                                        </div>

                                     </div>

                                    <div class="uk-width-large-1-2">



                                        <div class="md-input-wrapper md-input-filled">

                                            <label for="tpl_name">Template Name</label>

                                            <input type="text" class="md-input" id="tpl_name" name="tpl_name" value="">

                                            <span class="md-input-bar "></span>

                                        </div>



                                        <div class="uk-form-row">

                                            <label for="eng_name">English Product Name</label>

                                            <input type="text" class="md-input" id="eng_name" name="eng_name" value=""/>

                                            <span class="md-input-bar "></span>

                                        </div>

                                        <label for="eng_description">English Description</label>

										<div class="uk-form-row">

                                            

                                            <textarea class="md-input eng_text" name="eng_description" id="eng_description" cols="30" rows="4"></textarea>

                                        </div>

										

										<div class="uk-form-row">

										<select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}" title="Select with tooltip" name="category_id" onchange="getProduct(this.value);">

											<option value="">Select Category</option>

											

											  <?php

												getCategoriesOptionListing();

											  ?>

											  

										</select>

										</div>

                                        <div class="uk-form-row" id="select_main_div" style="display:none;">

                                        	<label for="select_demo_6">Similar Products </label>

                                            <select id="select_demo_6" class="md-input" title="Select with tooltip" name="related_product[]" multiple="multiple">                                              

                                            </select>

										</div>



										<div class="uk-form-row">

                                            

											<label for="eng_price">Price</label>

                                            <input type="text" class="md-input" id="eng_price" name="eng_price" value=""/>

											

                                        </div>

                                        <div class="uk-form-row">

                                            

											<label for="eng_quantity">Quantity</label>

                                            <input type="text" class="md-input" id="eng_quantity" name="eng_quantity" value=""/>

											

                                        </div>



                                        <div class="uk-form-row daily_offer_date display-none">

                                            <label for="end_date">Discount Price</label>
                                            <input type="text" class="md-input" id="discount_price" name="discount_price" value="" />

                                        </div>

                                                                                

                                    </div>

                                    <div class="uk-width-large-1-2">

                                        <div class="uk-form-row">

                                            <label for="arb_name">Arabic Product Name</label>

                                            <input type="text" class="md-input" id="arb_name" name="arb_name" value=""/>

                                            <span class="md-input-bar "></span>

                                        </div>

                                        <label for="arb_description">Arabic Description</label>

										<div class="uk-form-row">

                                            

                                            <textarea class="md-input arb_text" name="arb_description" id="arb_description" cols="30" rows="4"></textarea>

                                        </div>



										<div class="uk-form-row">

                                            

											<input type="checkbox" data-switchery data-switchery-size="large" checked id="active" name="active" value="1" />

											<label for="active" class="inline-label">Active</label>

                                        </div>



										<div class="uk-form-row">

                                            

											<input type="checkbox" data-switchery data-switchery-size="large" id="display_to_home_product" name="display_to_home_product"  value="1"/>

											<label for="display_to_home_product" class="inline-label">Display to home under product section</label>

            

                                        </div>



                                        <div class="uk-form-row">



                                            <input type="checkbox" data-switchery data-switchery-size="large" id="display_to_home_offer" name="display_to_home_offer" value="1" />

                                            <label for="display_to_home_offer" class="inline-label">Display to home under offer section</label>



                                        </div>



                                        <div class="uk-form-row daily_offer">



                                            <input type="checkbox" data-switchery data-switchery-size="large" id="daily_offer" name="daily_offer" value="1" />

                                            <label for="daily_offer" class="inline-label">Offer</label>



                                        </div>



                                        <div class="uk-form-row daily_offer_date display-none">



                                            <label for="start_date">Offer Start Date </label>

                                            <input type="text" class="md-input" id="start_date" name="start_date" data-uk-datepicker="{format:'DD/MM/YYYY'}" value=""/>





                                        </div>



                                        <div class="uk-form-row daily_offer_date display-none">



                                            <label for="end_date">Offer End Date </label>

                                            <input type="text" class="md-input" id="end_date" name="end_date" value="" data-uk-datepicker="{format:'DD/MM/YYYY'}" onChange="validateDates();"/>



                                        </div>

                                        <div class="uk-form-row">

                                        <select name="weight_unit" data-md-selectize data-md-selectize-bottom>
                                            <option value="">Weight Unit</option>
                                            <option value="kg">KG</option>
                                            <option value="g">G</option>
                                            <option value="lb">LB</option>
                                            <option value="oz">OZ</option>
                                        </select>

                                        </div>

                                        <div class="uk-form-row">
                                            <label for="eng_quantity">Weight Value</label>
                                            <input type="text" class="md-input" id="weight_value" name="weight_value" value=""/>


                                        </div>

                                    </div>


<div class="uk-width-large-1-1">

                    <h3 class="heading_a">

                        Upload Thumbnail

                       

                    </h3>

                    <span style="color:red">(200 * 300 pixels)</span>

                    <div class="uk-grid">

                        <div class="uk-width-1-1">

                            <div id="file_upload-drop" class="uk-file-upload">

                              	<a class="uk-form-file md-btn">choose file<input id="file_upload-select" class="images_upload" type="file" name="thumbnail[]"></a>

                            </div>

                            <div id="file_upload-progressbar" class="uk-progress uk-hidden">

                                <div class="uk-progress-bar" style="width:0">0%</div>

                            </div> 

                        </div>

                    </div>

                </div>
                                        

                                      <div class="uk-width-large-1-1">

										<h3 class="heading_a">

											Upload Product Image(s)

                                            <p style="color:red; font-size:12px">(182 x 398 pixels)</p>

										   

										</h3>

                                            <div class="uk-grid form_section" id="im_form_row">

                                                <div class="uk-width-1-2">

                                                    <div id="file_upload-drop" class="uk-file-upload">

                                                        <a class="uk-form-file md-btn" id="showCheck">choose file<input id="file_upload-select" type="file" class="images_upload" name="image[]" multiple></a>

                                                    </div>

                                                    <div id="file_upload-progressbar" class="uk-progress uk-hidden">

                                                        <div class="uk-progress-bar" style="width:0">0%</div>

                                                    </div>

                                                </div>

                                            	

                                                <span class="uk-input-group-addon">

                                                    <a href="#" class="btnSectionClone" data-section-clone="#im_form_row"><i class="material-icons md-24">&#xE146;</i></a>

                                                </span>

                                                 

                                            </div>

                                        <br><span class="uk-input-group-addon">Click + to add images

                                            <a href="javascript:void(0);" class="btnSectionClone" data-section-clone="#im_form_row"><i class="material-icons md-24"> &#xE146;</i></a>

            </span>

									</div>

                                </div>

                            </div>

                        </div>

                        <div class="md-card">

                            <div class="md-card-toolbar">

                                <h3 class="md-card-toolbar-heading-text">

                                    More Details and Product Information

                                </h3>

                            </div>

                            <div class="md-card-content large-padding">

                                <div class="uk-grid uk-grid-divider uk-grid-medium" data-uk-grid-margin>



                                    <div class="uk-width-large-1-2">



                                        <label for="eng_washing_instruction">English Product Details </label>

                                        <div class="uk-form-row">



                                            <textarea class="md-input eng_text" name="eng_brief_description" id="eng_washing_instruction" cols="30" rows="4"></textarea>

                                        </div>



                                        <label for="eng_more_detail">English More Details </label>

                                        <div class="uk-form-row">



                                            <textarea class="md-input eng_text" name="eng_more_detail" id="eng_more_detail" cols="30" rows="4"></textarea>

                                        </div>



                                    </div>

                                    <div class="uk-width-large-1-2">



                                        <label for="eng_washing_instruction">Arabic Product Details </label>

                                        <div class="uk-form-row">



                                            <textarea class="md-input arb_text" name="arb_brief_description" id="arb_washing_instruction" cols="30" rows="4"></textarea>

                                        </div>





                                        <label for="arb_washing_instruction">Arabic More Details </label>

                                        <div class="uk-form-row">



                                            <textarea class="md-input arb_text" name="arb_more_detail" id="arb_more_detail" cols="30" rows="4"></textarea>

                                        </div>

										

                                    </div>

                                </div>

                            </div>

                        </div>





                    </div>

                </div>

            </form>



        </div>

    </div>



    <div class="md-fab-wrapper">

        <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">

            <i class="material-icons">&#xE161;</i>

        </a>

    </div>

    

    <div class="md-fab-wrapper" style="right:95px;">

        <a class="md-fab md-fab-primary" href="<?php echo base_url();?>admin/product" id="">

            <i class="material-icons">keyboard_backspace</i>

        </a>

    </div>

    