<section class="productPage">

        <div class="container">

            <div class="productPHeading">

                <div class="row">

                    <div class="col-sm-3">

                        <h1><?php echo $all_products_label; ?></h1>

                    </div>

                    <div class="col-sm-9 text-right">

                        <ul class="prod_cat_ckBox">

                           <?php
						  $categories =  getCategoriesForHeader();
							$k = 1;
                           foreach($categories as $category) {

                               /*if($index == "1"){

                                   $checked = 'checked="checked"';

                               }else{

                                   $checked = '';

                               }*/

                               ?>

                                <li>
                                    
                                    <input class="searchByCat" id="prodCat_<?php echo str_replace(' ','',$category['eng_name']); ?>" type="checkbox" name="Products_checkBox" value="<?php echo $category['eng_name']; ?>" >

                                    <label for="prodCat_<?php echo $category['id']; ?>"><span>&nbsp;</span></label><?php echo $category[$lang.'_name']; ?>

                                </li>

                           <?php
							 $k++;
                           } ?>



                        </ul>

                    </div>

                </div>

            </div>

            <div class="productsListing">

                <ul class="getProductByCat">



                   <?php

                   if($products) {



                       foreach ($products as $product) {

                           $product_image = getProductImages($product['id']);
						   
						   foreach($product_image as $key => $product_img){
								if($product_img['is_thumbnail'] == 1){
									$thumbnail = $product_img['thumbnail'];
								}
							}

                           ?>



                           <li>

                               <div class="whiteBox">

                                   <a href="<?php echo lang_base_url() . 'product/product_page/' . $product['id']; ?>">

										
                                       <div class="imgBox">
									   <img src="<?php echo base_url(); ?>uploads/images/thumbs/products/<?php echo $thumbnail; ?>"  alt="Product" width="111" height="144">
									   </div>
									

                                   </a>

                                   <a href="<?php echo lang_base_url() . 'product/product_page/' . $product['id']; ?>">

                                       <h3><?php echo $product[$lang . '_name']; ?></h3>

                                   </a>

                                   <p><?php echo $product[$lang . '_cat_name']; ?></p>

                                   <h4>
                                       <?php

                                       $mMode = maintenanceMode('');
                                       if($mMode != ''){
                                           $onClick = 'maintenanceMode();';
                                       }else{
                                           $onClick = 'addToCart('.$product["id"].','.$product["total_quantity"].');';
                                       }

                                       $price = getPriceByOffer($product['id']);
                                       $calculated_price = currencyRatesCalculate($price);
                                       echo $calculated_price['rate']; ?> <?php echo $calculated_price['unit']; ?></h4>

                                   <input type="hidden" class="selQty" name="selQty" value="1">

                                   <a href="javascript:void(0);" onclick="<?php echo $onClick; ?>">

                                       <button class="btn btn-success" type="button">

                                           <i class="sprite_ioud ioudSppluscart"></i>

                                       </button>

                                   </a>

                               </div>

                           </li>



                       <?php }

                   }else { ?>
                       <div><li style="color: red; font-size: large; text-align: center;">

                               <?php echo $no_record_founds_label; ?>

                           </li>

                       </div>

                    <?php } ?>

                </ul>

                <div class="clearfix"></div>

            </div>

        </div>

    </section>





<script type="text/javascript">

var cat = '<?php echo str_replace(' ','',$_GET['cat']); ?>';

    $('#prodCat_'+cat).attr('checked',true);
	
	
	var cat = '<?php echo str_replace(' ','',$_GET['cat']); ?>';

    $('#product_'+cat).addClass('active');




</script>