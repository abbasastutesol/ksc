 <!-- Content Wrapper. Contains page content -->
      <div id="page_content">
        <div id="page_content_inner">
        <a href="<?php echo base_url();?>admin/product/exportExcelProductsNotPurchased/?start_date=<?php echo $this->input->post('start_date');?>&end_date=<?php echo $this->input->post('end_date');?>" target="_blank" class="md-btn"><img src="<?php echo base_url();?>assets/admin/assets/images/excelIcon.png" alt="excel" /></a>
			<div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                        	<div class="uk-overflow-container"> 
                            	<div class="uk-width-1-4" style="float: none;display: block;margin: 15px auto 0;">
                                	<form action="<?php echo base_url().'admin/product/productNotPurchased';?>" method="post" id="purchased_form">
                                        <div class="uk-form-row">
                                            <label for="eng_name">Start Date</label>
                                            <input type="text" class="md-input" id="start_date" name="start_date" value="<?php echo $this->input->post('start_date');?>" data-uk-datepicker="{format:'DD/MM/YYYY'}"/>
                                        </div>
                                        <div class="uk-form-row">
                                            <label for="eng_name">End Date</label>
                                            <input type="text" class="md-input" id="end_date" name="end_date" value="<?php echo $this->input->post('end_date');?>" data-uk-datepicker="{format:'DD/MM/YYYY'}"/>
                                        </div>
                                        
                                    </form>
                                    <div class="uk-form-row">
	                                        <button class="md-btn md-btn-wave waves-effect waves-button" onclick="validateDates();" type="submit">Search</button>										
                                        </div>
                                </div>
                                <table class="uk-table uk-table-align-vertical listing dt_default">
                                    <thead>
                                        <tr>
                                             <th>Sr#</th>

                                              <th>Product</th>
                                              
                                              <th class="nosort">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
									<?php
									if($products)
									{
										$i=0;
										foreach($products as $product)
										{
										?>
											<tr class="<?php echo $product->id ;?> cat_<?php echo $product->category_id ;?> allproducts">
													<td><?php echo ++$i;?></td>
													<td><?php echo ucfirst($product->eng_name);?></td>
													<td><a href="<?php echo base_url().'admin/product/edit/'.$product->id;?>" title="Edit product">
													<i class="md-icon material-icons">visibility</i>
													</a>
													</td>

													  
											</tr>
										<?php
										}
										
									}
									
									?>
                                           
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
       <!-- /.content-wrapper --> 