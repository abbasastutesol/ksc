<script>

        $(document).ready(function(){

            //	Add class to footer border

            $('footer .footerLinks').addClass('border4ProDtl');

        });

    </script>
    <?php 
		/*if($_SERVER['REMOTE_ADDR'] == "45.55.49.75") {
		
			echo "<pre>"; print_r($product); exit;
		}*/
	?>

    <section class="productPage">

        <div class="container">

            <div class="prod_dtl_box">

                <div class="breadcrumbs">

                    <ul>

                        <li class="Back_pro">

                            <a href="<?php echo lang_base_url().'product' ?>"><i class="sprite_ioud ioudSparrowright"></i></a>

                        </li>

                        <li>

							<a><?php echo $home_page_label; ?></a>

                        </li>

                        <li>

                            <i class="fa fa-angle-<?php echo ($lang=='arb' ? 'left' : 'right'); ?>" ></i>

                        </li>

				<?php $cat = getProductCategory($product['category_id']); ?>
                        <li>

                            <a><?php echo $cat[$lang.'_name']; ?></a>

                        </li>

                        <li>

                            <i class="fa fa-angle-<?php echo ($lang=='arb' ? 'left' : 'right'); ?>" ></i>

                        </li>

                        <li>
                            <a><?php echo $product[$lang.'_name']; ?></a>
                        </li>

                    </ul>

                </div>

                <div class="clearfix"></div>

                <div class="proDtlSlider">

                    <div id="productSlider" class="carousel slide" data-ride="carousel"  data-interval="false">

                        <div class="carousel-inner" role="listbox">



                          <?php $i = 0; $images = getProductImages($product['id']);

                          foreach ($images as $image) {

                              if ($i == 0) {

                                  $active = "active";

                              } else {

                                  $active = "";

                              }
                              if ($image['is_thumbnail'] == 0) {

                                  ?>

                                  <div class="item <?php echo $active; ?>">

                                      <?php if($image[$lang.'_image'] != '') { ?>
                                      <a class="imgBox html5lightbox" data-group="set1"
                                         href="<?php echo base_url(); ?>uploads/images/products/<?php echo $image[$lang . '_image']; ?>">

                                          <img src="<?php echo base_url(); ?>uploads/images/products/<?php echo $image[$lang . '_image']; ?>"
                                               data-origin="<?php echo base_url(); ?>uploads/images/products/<?php echo $image[$lang . '_image']; ?>">

                                      </a>

                                      <?php } ?>
                                      <?php if($image['gif'] != '') { ?>
                                          <a class="imgBox html5lightbox" data-group="set1"
                                             href="<?php echo base_url(); ?>uploads/images/products/<?php echo $image['gif']; ?>">

                                              <img src="<?php echo base_url(); ?>uploads/images/products/<?php echo $image['gif']; ?>"
                                                   data-origin="<?php echo base_url(); ?>uploads/images/products/<?php echo $image['gif']; ?>">

                                          </a>

                                      <?php } ?>
                                      <?php if($image['video'] != '') { ?>


                                          <a class="html5lightbox video_thumbnail" href="<?php echo base_url(); ?>uploads/videos/<?php echo $image['video']; ?>?autoplay=1" data-group="set1">
                                            <img src="<?php echo base_url(); ?>assets/frontend/images/video.jpg">
                                          </a>


                                      <?php } ?>

                                  </div>


                                  <?php $i++;
                              }
                          }?>


                        </div>

                        <div class="smallImages">

                            <ol class="carousel-indicators">

                                <?php $c = 0;
                                $images = getProductImages($product['id']);

                                foreach ($images as $image) {

                                    if($c == 0){

                                        $active = "active";

                                    }else{

                                        $active = "";

                                    }
									if($image['is_thumbnail'] == 0) {
                                    ?>

                                    <li data-target="#productSlider" data-slide-to="<?php echo $c; ?>" class="<?php echo $active; ?>">
                                        <?php if($image[$lang.'_image'] != '') { ?>
                                        <img src="<?php echo base_url(); ?>uploads/images/products/<?php echo $image[$lang.'_image']; ?>" alt="Thumb" height="51" width="74" />
                                        <?php }
                                        if($image['is_thumbnail'] == 0 && $image['video'] != '') { ?>

                                        <img src="<?php echo base_url(); ?>assets/frontend/images/video-thumbnail.png" alt="Video" height="51" width="74" />

                                        <?php } ?>
                                    <?php
                                    if($image['is_thumbnail'] == 0 && $image['gif'] != '') { ?>

                                        <img src="<?php echo base_url(); ?>uploads/images/products/<?php echo $image['gif']; ?>" alt="Gif" height="51" width="74" />

                                    <?php } ?>

                                    </li>



                                    <?php $c++; }
								}?>

                            </ol>

                        </div>

                        <div class="clearfix"></div>

                    </div>

                </div>

                <div class="proDtlText">

                    <h2 class="makeShadow"><?php echo $product[$lang.'_name'];?></h2>

                    <?php echo $product[$lang.'_description'];?>

                    <br />

                    <?php echo $product[$lang.'_brief_description'];?>

                    <p><strong class="priceTitle"><?php echo $price; ?></strong></p>

                    <h3 class="price"><?php
                        $proPrice = getPriceByOffer($product['id']);
                        $calculated_rate = currencyRatesCalculate($proPrice);
                        echo $calculated_rate['rate']; ?> <span class="curr"><?php echo $calculated_rate['unit'];?></span> </h3>

                    <div class="clearfix"></div>

                    <?php
                    // for maintenance mode
                    $mMode = maintenanceMode('');
                    if($mMode != ''){
                        $onClick = 'maintenanceMode();';
                    }else{
                        $onClick = 'addToCart('.$product["id"].');';
                    }

                    if(intval($product[$lang.'_quantity']) > 0) { ?>

                    <a href="javascript:void(0);" onclick="<?php echo $onClick; ?>">

                        <button class="btn btn-success" type="button">

                            <i class="sprite_ioud ioudSppluscart"></i>

                            <?php echo $add_to_my_cart_label; ?>

                        </button>

                    </a>
                    <?php }
					$count = intval($product[$lang.'_quantity']);
                    if($count > 0) { ?>

                    <select class="selQty">
                    
                        <?php for($i = 1; $i<= 100; $i++){ ?>

                            <option value="<?php echo $i; ?>"><?php echo $shopping_cart_qty; ?> <?php echo $i; ?></option>

                        <?php } ?>

                    </select>

                    <?php }else{ ?>

                        <div class="btn btn-danger">

                           <?php echo $shopping_out_of_stock; ?>

                        </div>

                    <?php } ?>

                    <div class="clearfix"></div>

                </div>

                <div class="clearfix"></div>
				
				<?php if($product[$lang.'_more_detail'] != "") { ?>
                <div class="divViewMore"><?php echo $view_more_label; ?></div>
                <?php } ?>

            </div>

            <!-- Product detail Box Bellow Section -->

            <div class="proDtlBlow">
				<?php if($product[$lang.'_more_detail'] != "") { ?>
                <div class="prodMoreDtl">

                    <div class="pMD_inner">

                        <div class="divViewLess">

                            <a href="javascript:void(0);"><?php echo $view_less_label; ?></a>

                        </div>

                        <?php echo $product[$lang.'_more_detail']; ?>

                    </div>

                </div>
                
                <?php } ?>

                <div class="productPHeading">

                    <div class="row">

                        <div class="col-sm-12">

                            <h1 class="makeShadow"><?php echo $related_products_label; ?></h1>

                        </div>

                    </div>

                </div>

                <div class="productPage">

                    <div class="productsListing">



                        <ul>
                        

                          <?php
						 	$count = 0;
						  if(sizeof($related_products) > 0 && $related_products != false) {
						  	foreach ($related_products as $related_product) {
								
                              if($product['id'] != $related_product['id']) {
								  $count++; 


                                  $images = getProductImages($related_product['id']);
								  
								  foreach($images as $key => $image){
								if($image['is_thumbnail'] == 1){
									$thumbnail = $image['thumbnail'];
								}
							}

                                  ?>



                                  <li>

                                      <div class="whiteBox">

                                          <a href="<?php echo lang_base_url() . 'product/product_page/' . $related_product['id']; ?>">

                                              <div class="imgBox"><img

                                                          src="<?php echo base_url(); ?>uploads/images/thumbs/products/<?php echo $thumbnail; ?>" alt="Product" width="111" height="144"></div>

                                          </a>

                                          <a href="<?php echo lang_base_url() . 'product/product_page/' . $related_product['id']; ?>">

                                              <h3><?php echo $related_product[$lang . '_name']; ?></h3>

                                          </a>

                                          <p><?php echo $related_product[$lang . '_cat_name']; ?></p>

                                          <h4>
                                              <?php

                                              $mMode = maintenanceMode('');
                                              if($mMode != ''){
                                                  $onClick = 'maintenanceMode();';
                                              }else{
                                                  $onClick = 'addToCart('.$related_product["id"].');';
                                              }

                                              $proPrice = getPriceByOffer($related_product['id']);
                                              $calculated_rate =currencyRatesCalculate($proPrice);
                                              echo $calculated_rate['rate']; ?>

                                              <?php echo $calculated_rate['unit'];?></h4>

                                          <a href="javascript:void(0);" onclick="<?php echo $onClick; ?>">

                                              <button class="btn btn-success" type="button">

                                                  <img src="<?php echo base_url(); ?>assets/frontend/images/plusCart.png"

                                                       alt="Add Cart" width="34" height="16">

                                              </button>

                                          </a>

                                      </div>

                                  </li>

                              <?php }
							  ?>
                             <?php 
							  }
						  }
							 ?>
                             
                             <?php if($count == 0) { ?>
                             <li style="width: 100% !important;"><div style="text-align: center;"><?php echo $related_product_not_available_label; ?></div></li>
                             <?php } ?>


                        </ul>

                        <div class="clearfix"></div>

                    </div>

                </div>

            </div>



        </div>

    </section>
    
    <!-- do not remove this script from here -->
    <script type="text/javascript">
    
    $('.pMD_inner > h1').addClass('makeShadow');
	
    </script>