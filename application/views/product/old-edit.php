<div id="page_content">

        <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">

            <h1 id="product_edit_name">Edit a product</h1>

            <span class="uk-text-muted uk-text-upper uk-text-small" id="product_edit_sn"></span>

        </div>

        <div id="page_content_inner">

            <form action="<?php echo base_url(); ?>admin/product/action" method="post" onsubmit="return false" class="uk-form-stacked ajax_form" id="product_edit_form">

				<input type="hidden" name="form_type" value="update">

				<input type="hidden" name="id" value="<?php echo $product->id;?>">

                <div class="uk-grid uk-grid-medium" data-uk-grid-margin>

                    

                    <div class="uk-width-xLarge-10-10  uk-width-large-10-10">

                        <div class="md-card">

                            <div class="md-card-toolbar">

                                <h3 class="md-card-toolbar-heading-text">

                                    Details

                                </h3>

                            </div>

                            <div class="md-card-content large-padding">

                                <div class="uk-grid uk-grid-divider uk-grid-medium" data-uk-grid-margin>

                                	<div class="uk-width-large-1-2">



                                     </div>

                                     <div class="uk-width-large-1-2">

                                        <div class="uk-form-row">

                                            <input type="hidden" class="md-input" id="" name="" value=""/>

                                        </div>

                                     </div>

                                    <div class="uk-width-large-1-2">



                                        <div class="md-input-wrapper md-input-filled">

                                            <label for="tpl_name">Template Name</label>

                                            <input type="text" class="md-input" id="tpl_name" name="tpl_name" value="<?php echo $product->tpl_name;?>">

                                            <span class="md-input-bar "></span>

                                        </div>



                                        <div class="uk-form-row">

                                            <label for="eng_name">English Product Name</label>

                                            <input type="text" class="md-input" id="eng_name" name="eng_name" value="<?php echo $product->eng_name;?>"/>

                                        </div>

                                        <label for="eng_description">English Description</label>

										<div class="uk-form-row">

                                            

                                            <textarea class="md-input eng_text" name="eng_description" id="eng_description" cols="30" rows="4"><?php echo $product->eng_description;?></textarea>

                                        </div>



										<div class="uk-form-row">

										<select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}" title="Select with tooltip" name="category_id" onchange="getProduct(this.value);">

											<option value="">Select Category</option>

											

											  <?php

												getCategoriesOptionListing('0','',$product->category_id);

											  ?>

											  

										</select>

										</div>

                                        <div class="uk-form-row" id="select_main_div" style="display:<?php echo ($product->category_id != '0' ? 'block' : 'none');?>;">

                                        	<label for="select_demo_6">Similar Products </label>

                                            <select id="select_demo_6" class="md-input" title="Select with tooltip" name="related_product[]" multiple="multiple">

                                            <?php

												getCategoriesProductsOptionListing($product->id,$product->category_id);

											  ?>                                              

                                            </select>

										</div>



										<div class="uk-form-row">

                                            

											<label for="eng_price">Price</label>

                                            <input type="text" class="md-input" id="eng_price" name="eng_price" value="<?php echo $product->eng_price;?>"/>

											

                                        </div>

                                        <div class="uk-form-row">

											<label for="eng_quantity">Quantity</label>

                                            <input type="text" class="md-input" id="eng_quantity" name="eng_quantity" value="<?php echo $product->eng_quantity;?>"/>

                                        </div>

                                        <div class="uk-form-row daily_offer_date <?php if($product->daily_offer == '1') { echo 'display-block'; } else { echo 'display-none'; } ?>">

                                            <label for="end_date">Discount Price</label>
                                            <input type="text" class="md-input" id="discount_price" name="discount_price" value="<?php echo $product->discount_price; ?>" />

                                        </div>


                                    </div>

                                    <div class="uk-width-large-1-2">

                                        <div class="uk-form-row">

                                            <label for="arb_name">Arabic Product Name</label>

                                            <input type="text" class="md-input" id="arb_name" name="arb_name" value="<?php echo $product->arb_name;?>"/>

                                        </div>

                                        <label for="arb_description">Arabic Description</label>

										<div class="uk-form-row">

                                            

                                            <textarea class="md-input arb_text" name="arb_description" id="arb_description" cols="30" rows="4"><?php echo $product->arb_description;?></textarea>

                                        </div>



										<div class="uk-form-row">

                                            

											<input type="checkbox" data-switchery data-switchery-size="large"  id="active" name="active" value="1" <?php echo ($product->active == '1' ? 'checked' : ''); ?> />

											<label for="active" class="inline-label">Active</label>

                                        </div>

										

										<div class="uk-form-row">

											<input type="checkbox" data-switchery data-switchery-size="large" id="display_to_home_product" name="display_to_home_product"  value="1" <?php echo ($product->display_to_home_product == '1' ? 'checked' : ''); ?>/>

											<label for="display_to_home_product" class="inline-label">Display to home under product section</label>

                                        </div>

                                        <div class="uk-form-row">

                                            <input type="checkbox" data-switchery data-switchery-size="large" id="display_to_home_offer" name="display_to_home_offer"  value="1" <?php echo ($product->display_to_home_offer == '1' ? 'checked' : ''); ?>/>

                                            <label for="display_to_home_offer" class="inline-label">Display to home under offer section</label>

                                        </div>

                                        <div class="uk-form-row daily_offer">

                                            <input type="checkbox" data-switchery data-switchery-size="large" id="daily_offer" name="daily_offer" value="1" <?php echo ($product->daily_offer == '1' ? 'checked' : ''); ?>/>

                                            <label for="daily_offer" class="inline-label">Offer</label>

                                        </div>

                                        <div class="uk-form-row daily_offer_date <?php if($product->daily_offer == '1') { echo 'display-block'; } else { echo 'display-none'; } ?>">



                                            <label for="start_date">Offer Start Date </label>

                                            <input type="text" class="md-input" id="start_date" name="start_date" data-uk-datepicker="{format:'DD/MM/YYYY'}" value="<?php if($product->daily_offer == '1') { echo date('d/m/Y',strtotime($product->start_date)); } else { echo ''; }; ?>"/>





                                        </div>



                                        <div class="uk-form-row daily_offer_date <?php if($product->daily_offer == '1') { echo 'display-block'; } else { echo 'display-none'; } ?>">



                                            <label for="end_date">Offer End Date </label>

                                            <input type="text" class="md-input" id="end_date" name="end_date" value="<?php if($product->daily_offer == '1') { echo date('d/m/Y',strtotime($product->end_date)); } else { echo ''; }; ?>" data-uk-datepicker="{format:'DD/MM/YYYY'}"/>



                                        </div>


                                        <div class="uk-form-row">

                                            <select name="weight_unit" data-md-selectize data-md-selectize-bottom>
                                                <option value="">Weight Unit</option>
                                                <option value="kg" <?php if($product->weight_unit == "kg") { echo 'selected'; } ?>>KG</option>
                                                <option value="g" <?php if($product->weight_unit == "g") { echo 'selected'; } ?>>G</option>
                                                <option value="lb" <?php if($product->weight_unit == "lb") { echo 'selected'; } ?>>LB</option>
                                                <option value="oz" <?php if($product->weight_unit == "oz") { echo 'selected'; } ?>>OZ</option>
                                            </select>

                                        </div>

                                        <div class="uk-form-row">
                                            <label for="eng_quantity">Weight Value</label>
                                            <input type="text" class="md-input" id="weight_value" name="weight_value" value="<?php echo $product->weight_value; ?>"/>


                                        </div>



                                    </div>


                    
                    <!-- tumbnail-->
                    
                    
                    <div class="uk-width-large-1-1">

									<div class="md-card-toolbar">

                                <div class="md-card-toolbar-actions">

                                
                                </div>

                                <h3 class="md-card-toolbar-heading-text">

                                    Thumbnail

                                </h3>

                            </div>

                            <div class="md-card-content">

                                <?php 


								if($images){

									

									?>

                                <ul class="uk-grid uk-grid-width-small-1-6 uk-text-center" data-uk-grid-margin data-uk-observe id="images-light-box">

								<?php foreach($thumbs as $thumb){

								?>

									<li class="uk-position-relative image-<?php echo $thumb->id;?>">

                                        <button type="button" class="uk-modal-close uk-close uk-close-alt uk-position-absolute" onClick="deleteRecord(<?php echo $thumb->id;?>,'admin/product/deleteImage','');"></button>

                                        <img src="<?php echo base_url().'uploads/images/thumbs/products/'.$thumb->thumbnail;?>" alt="" class="img_small" data-uk-modal="{target:'#modal_lightbox_<?php echo $thumb->id;?>'}"/>

                                        <div class="uk-form-row">

                                            

											
                                        </div>

                                    </li>


								<?php

								
								}

								?>

                                    

                                    

                                </ul>

								<?php } ?>

                            </div>

							</div>
                    
                    <!-- thumbnail end-->
                    
                    
                    
                    
                    
                                    
                                    
                   <div class="uk-width-large-1-1">

                    <h3 class="heading_a">

                        Upload Thumbnail

                       

                    </h3>

                    <span style="color:red">(200 * 300 pixels)</span>

                    <div class="uk-grid">

                        <div class="uk-width-1-1">

                            <div id="file_upload-drop" class="uk-file-upload">

                              	<a class="uk-form-file md-btn">choose file<input id="file_upload-select" class="images_upload" type="file" name="thumbnail[]"></a>

                            </div>

                            <div id="file_upload-progressbar" class="uk-progress uk-hidden">

                                <div class="uk-progress-bar" style="width:0">0%</div>

                            </div> 

                        </div>

                    </div>

                </div>
                                    
                                    
                                    

									<div class="uk-width-large-1-1">

									<div class="md-card-toolbar">

                                <div class="md-card-toolbar-actions">

                                    <!--<i class="md-icon material-icons">&#xE146;</i>-->

                                </div>

                                <h3 class="md-card-toolbar-heading-text">

                                    Photos

                                </h3>

                            </div>

                            <div class="md-card-content">

                                <?php 

								$light_boxes = '';

								if($images){

									

									?>

                                <ul class="uk-grid uk-grid-width-small-1-6 uk-text-center" data-uk-grid-margin data-uk-observe id="images-light-box">

								<?php foreach($images as $image)

								{

								?>

									<li class="uk-position-relative image-<?php echo $image->id;?>">

                                        <button type="button" class="uk-modal-close uk-close uk-close-alt uk-position-absolute" onClick="deleteRecord(<?php echo $image->id;?>,'admin/product/deleteImage','');"></button>

                                        <img src="<?php echo base_url().'uploads/images/thumbs/products/'.$image->eng_image;?>" alt="" class="img_small" data-uk-modal="{target:'#modal_lightbox_<?php echo $image->id;?>'}"/>

                                        <div class="uk-form-row">

                                            

											
                                        </div>

                                    </li>

									

			

								<?php

								// this is used to avoid extra loop in php when this loop executes we at the bootom echo this variable

								

									$light_boxes .="<div class='uk-modal' id='modal_lightbox_".$image->id."'><div class='uk-modal-dialog uk-modal-dialog-lightbox'><button type='button' class='uk-modal-close uk-close uk-close-alt'></button><img src='".base_url()."uploads/images/products/".$image->eng_image."' alt=''/></div></div>"; 	

								}

								?>

                                    

                                    

                                </ul>

								<?php } ?>

                            </div>

							</div>
                            
                            

                                    <div class="uk-width-large-1-1">

										<h3 class="heading_a">

											Upload Product Image(s)

                                            <p style="color:red; font-size:12px">(182 x 398 pixels)</p>

										   

										</h3>

										 <div class="uk-grid form_section" id="im_form_row">

                                                <div class="uk-width-1-2">

                                                    <div id="file_upload-drop" class="uk-file-upload">

                                                        <a class="uk-form-file md-btn" id="showCheck">choose file<input id="file_upload-select" type="file" class="images_upload" name="image[]" multiple></a>

                                                    </div>

                                                    <div id="file_upload-progressbar" class="uk-progress uk-hidden">

                                                        <div class="uk-progress-bar" style="width:0">0%</div>

                                                    </div>

                                                </div>

                                            	

                                                <span class="uk-input-group-addon">

                                                    <a href="#" class="btnSectionClone" data-section-clone="#im_form_row"><i class="material-icons md-24">&#xE146;</i></a>

                                                </span>

                                                 

                                            </div>

                                        <br><span class="uk-input-group-addon">Click + to add images

                                            <a href="javascript:void(0);" class="btnSectionClone" data-section-clone="#im_form_row"><i class="material-icons md-24"> &#xE146;</i></a>

            </span>

									</div>

                                  

									

                                </div>

                            </div>

                        </div>

                        <div class="md-card">

                            <div class="md-card-toolbar">

                                <h3 class="md-card-toolbar-heading-text">

                                    More Details and Product Information

                                </h3>

                            </div>

                            <div class="md-card-content large-padding">

                                <div class="uk-grid uk-grid-divider uk-grid-medium" data-uk-grid-margin>

                                    <div class="uk-width-large-1-2">



                                        <label for="eng_washing_instruction">English Product Details </label>

                                        <div class="uk-form-row">



                                            <textarea class="md-input eng_text" name="eng_brief_description" id="eng_washing_instruction" cols="30" rows="4"><?php echo $product->eng_brief_description;?></textarea>

                                        </div>



                                        <label for="eng_more_detail">English More Details </label>

                                        <div class="uk-form-row">



                                            <textarea class="md-input eng_text" name="eng_more_detail" id="eng_more_detail" cols="30" rows="4"><?php echo $product->eng_more_detail; ?></textarea>

                                        </div>

                                                                                

                                    </div>

                                    <div class="uk-width-large-1-2">



                                        <label for="arb_washing_instruction">Arabic Product Detail </label>

                                        <div class="uk-form-row">

                                            

                                            <textarea class="md-input arb_text" name="arb_brief_description" id="arb_washing_instruction" cols="30" rows="4"><?php echo $product->arb_brief_description;?></textarea>

                                        </div>



                                        <label for="arb_washing_instruction">Arabic More Details </label>

                                        <div class="uk-form-row">



                                            <textarea class="md-input arb_text" name="arb_more_detail" id="arb_more_detail" cols="30" rows="4"><?php echo $product->arb_more_detail; ?></textarea>

                                        </div>



										

                                    </div>

                                </div>

                            </div>

                        </div>





                    </div>

                </div>

            </form>



        </div>

    </div>

<!-- light box for image -->



<div id="light_box_of_images" data-uk-observe>

</div>

								

			<!-- end light box for image --> 

    <div class="md-fab-wrapper">

        <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">

            <i class="material-icons">&#xE161;</i>

        </a>

    </div>

    <div class="md-fab-wrapper" style="right:95px;">

        <a class="md-fab md-fab-primary" href="<?php echo base_url();?>admin/product" id="">

            <i class="material-icons">keyboard_backspace</i>

        </a>

    </div>

<?php if($light_boxes != ''){

    echo '<script>

		light_box_images = "'.$light_boxes.'";

	</script>';

} ?>

	