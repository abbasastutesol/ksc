<section>
	<div class="productsListSec">
		<div class="container">
			<?php $this->load->view('layouts/sidebar');?>
            <input type="hidden" id="page_type" value="search_page" />
            <input type="hidden" id="category_id" value="<?php echo ($this->input->get('category') != 'all' ? $this->input->get('category') : '');?>" />
            <input type="hidden" id="keyword" value="<?php echo $this->input->get('keyword');?>" />
			<div class="proDtlLftSec">
				<div class="searchText">
                <?php $config = getCconfiguration(true);
					echo $config[$lang.'_search_description']
				?>	
			</div><?php if($search_products){?>
				<div class="productSecFive showProInGrid four">
                	
					<ul id="search_results">
                    	<?php foreach($search_products as $product){
							$image_arr = getProductImages($product['id']);
                                    ?>
                                            <li>
                                                <div class="imgBox">
													<?php if(sizeof($image_arr) > 1){?>						
                                                        <div id="Product_id_<?php echo $main;?>" class="carousel slide cntCenter" data-ride="carousel"> 															
                                                            <!-- Wrapper for slides -->
                                                            <div class="carousel-inner" role="listbox">
                                                                <?php 
                                                                $im=0;
                                                                foreach($image_arr as $image){?>
                                                                    <div class="item<?php if($im==0){ echo ' active';}?>"> <a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"><img src="<?php echo base_url();?>uploads/images/products/<?php echo $image[$lang.'_image']?>" alt="Product" height="199" width="129" ></a>
                                                                    </div>
                                                                <?php
                                                                $im++; 
                                                                }?>                                                   
                                                            </div>
                                                        </div>
                                                    <?php }else{ ?>
                                                        <?php if($image_arr){
                                                            foreach($image_arr as $image){?>
                                                            
                                                            <a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"><img src="<?php echo base_url();?>uploads/images/products/<?php echo $image[$lang.'_image']?>" alt="Product" height="199" width="129" /></a>
                                                        <?php 
                                                            }
                                                        } else
                                                        { ?>
                                                           <a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"> <img src="<?php echo base_url();?>uploads/images/products/no_imge.png" alt="Product" height="199" width="129" /></a>
                                                        <?php }?>
                                                    <?php } ?>
                                                </div>
                                                <!-- Indicators -->
                                                <?php if(sizeof($image_arr) > 1){?>
                                                	<ol class="carousel-indicators">
														<?php 
                                                        $imo=0;
                                                        foreach($image_arr as $image){?>
                                                            <li data-target="#Product_id_<?php echo $main;?>" data-slide-to="<?php echo $imo;?>"<?php if($imo==0){ echo ' class="active"';}?>></li>
                                                        <?php 
                                                        $imo++; 
                                                        }
                                                        ?>
                                                    </ol>	
                                                <?php }?>
                                                <h3><a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"><?php echo $product[$lang.'_name']?></a></h3>
                                                <?php  
												if($product['has_discount_price'])
												{
													  echo '<h4><span class="discount">'.$product[$lang.'_price'].'</span>'.getPriceCur($product[$lang.'_discount_price']).'</h4>';
												}
												else
												{
													echo '<h4>'.getPriceCur($product[$lang.'_price']).'</h4>';
												}
												
												?>
                                                <div class="starAddToCart">
                                                    <div class="addToCart"><a class="fancybox fancybox.ajax" href="<?php echo base_url();?>ajax/getProduct/<?php echo $product['id']?>"><img src="<?php echo base_url();?>assets/frontend/images/addToCartGreen.png" alt="Cart" height="15" width="17" /></a></div>
                                                    <div class="openInNewWindow"><a href="<?php echo lang_base_url();?>product/product_page/<?php echo ($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"><img src="<?php echo base_url();?>assets/frontend/images/openLinkSep.png" alt="Open" height="15" width="17" /></a></div>
                                                    <div class="stars stars-example-bootstrap">
														<?php $rate = getAverageRating($product['id']);?>
                                                        <select class="example-bootstrap" name="rating" autocomplete="off" onchange="rateProduct(this.value, '<?php echo $product['id']; ?>');">
                                                          <option <?php echo ($rate == 1 ? 'selected' : '');?> value="1">1</option>
                                                          <option <?php echo ($rate == 2 ? 'selected' : '');?> value="2">2</option>
                                                          <option <?php echo ($rate == 3 ? 'selected' : '');?> value="3">3</option>
                                                          <option <?php echo ($rate == 4 ? 'selected' : '');?> value="4">4</option>
                                                          <option <?php echo ($rate == 5 ? 'selected' : '');?> value="5">5</option>
                                                        </select>
                                                      </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </li>
                                <?php
                                    $main++; }?>
					</ul>
                    
				</div>
                <?php }else{
						echo '<strong>'.$no_records.'</strong>';
						 }?>
			</div>
		</div>
	</div>
	<div class="container">
    <?php if($products){?>
		<div class="productSecFive  playSlide">
			<h2><?php echo $login_exhibits; ?></h2>
			<div class="row">
				<ul>
					<?php foreach($products as $product){
									$image_arr = getProductImages($product['id']);
                                          ?>
                                <li>
                                    <div class="imgBox">
                                    	<?php if(sizeof($image_arr) > 1){?>						
                                        	<div id="Product_id_<?php echo $main;?>" class="carousel slide cntCenter" data-ride="carousel"> 															
                                                <!-- Wrapper for slides -->
                                                <div class="carousel-inner" role="listbox">
                                                    <?php 
													$im=0;
													foreach($image_arr as $image){?>
                                                        <div class="item<?php if($im==0){ echo ' active';}?>"> <a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"><img src="<?php echo base_url();?>uploads/images/products/<?php echo $image[$lang.'_image']?>" alt="Product" height="199" width="129" ></a>
                                                        </div>
                                                    <?php
													$im++; 
													}?>                                                   
                                                </div>
                                            </div>
                                        <?php }else{ ?>
                                       		<?php if($image_arr){
												foreach($image_arr as $image){?>
                                            	
                                        		<a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"><img src="<?php echo base_url();?>uploads/images/products/<?php echo $image[$lang.'_image']?>" alt="Product" height="199" width="129" /></a>
                                            <?php 
												}
											} else
											{ ?>
                                            	<a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"><img src="<?php echo base_url();?>uploads/images/products/no_imge.png" alt="Product" height="199" width="129" /></a>
                                            <?php }?>
                                        <?php } ?>
                                    </div>
                                    <!-- Indicators -->
                                    <?php if(sizeof($image_arr) > 1){?>
                                        <ol class="carousel-indicators">
                                        <?php 
                                        $imo=0;
                                        foreach($image_arr as $image){?>
                                            <li data-target="#Product_id_<?php echo $main;?>" data-slide-to="<?php echo $imo;?>"<?php if($imo==0){ echo ' class="active"';}?>></li>
                                        <?php 
                                        $imo++; 
                                        }
                                        ?>
                                    </ol>	
                                    <?php }?>	
                                    <h3><a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"><?php echo $product[$lang.'_name']?></a></h3>
                                   <?php  
												if($product['has_discount_price'])
												{
													  echo '<h4><span class="discount">'.$product[$lang.'_price'].'</span>'.getPriceCur($product[$lang.'_discount_price']).'</h4>';
												}
												else
												{
													echo '<h4>'.getPriceCur($product[$lang.'_price']).'</h4>';
												}
												
												?>
                                    <div class="starAddToCart">
                                        <div class="addToCart"><a class="fancybox fancybox.ajax" href="<?php echo base_url();?>ajax/getProduct/<?php echo $product['id']?>"><img src="<?php echo base_url();?>assets/frontend/images/addToCartGreen.png" alt="Cart" height="15" width="17" /></a></div>
                                        <div class="openInNewWindow"><a href="<?php echo lang_base_url();?>product/product_page/<?php echo ($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"><img src="<?php echo base_url();?>assets/frontend/images/openLinkSep.png" alt="Open" height="15" width="17" /></a></div>
                                        <div class="stars stars-example-bootstrap">
											<?php $rate = getAverageRating($product['id']);?>
                                            <select class="example-bootstrap" name="rating" autocomplete="off" onchange="rateProduct(this.value, '<?php echo $product['id']; ?>');">
                                              <option <?php echo ($rate == 1 ? 'selected' : '');?> value="1">1</option>
                                              <option <?php echo ($rate == 2 ? 'selected' : '');?> value="2">2</option>
                                              <option <?php echo ($rate == 3 ? 'selected' : '');?> value="3">3</option>
                                              <option <?php echo ($rate == 4 ? 'selected' : '');?> value="4">4</option>
                                              <option <?php echo ($rate == 5 ? 'selected' : '');?> value="5">5</option>
                                            </select>
                                          </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                            <?php 
                            $main++;
                            }?>
				</ul>
			</div>
		</div>
    <?php }?>
	</div>
</section>