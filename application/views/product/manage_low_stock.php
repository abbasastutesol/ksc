 <!-- Content Wrapper. Contains page content -->
      <div id="page_content">
        <div id="page_content_inner">
		<a href="<?php echo base_url();?>admin/product/" class="md-btn"> All Products</a>
			<div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                        	<div class="uk-overflow-container"> 
                            
                                <table class="uk-table uk-table-align-vertical listing dt_default">
                                    <thead>
                                        <tr>
                                             <th>Sr#</th>

                                              <th>Product</th>
											  <th>Serial Number(SKU)</th>
                                              
                                              <th>Featured</th>
                      
                                              <th>Active</th>
                                              
                                              <th class="nosort">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
									<?php
									if($products)
									{
										$i=0;
										foreach($products as $product)
										{
										?>
											<tr class="<?php echo $product->id ;?> cat_<?php echo $product->category_id ;?> allproducts">
													<td><?php echo ++$i;?></td>
													<td><?php echo ucfirst($product->eng_name);?></td>
													<td><?php echo $product->eng_sku;?></td>
                                                    <td><?php echo ($product->is_feature == 1 ? 'Yes' : 'No');?></td>
													<td><?php echo ($product->active == '1' ? '<i class="material-icons md-color-light-blue-600 md-24">&#xE86C;</i>': '');?></td>
													<td><a href="<?php echo base_url().'admin/product/edit/'.$product->id;?>" title="Edit product">
													<i class="md-icon material-icons">&#xE254;</i>
													</a>
													<a href="javascript:void(0);" class="uk-margin-left delete" onclick="deleteRecord(<?php echo $product->id;?>,'admin/product/action','');" title="Delete Product"> <i class="material-icons md-24 delete">&#xE872;</i></a></td>

													  
											</tr>
										<?php
										}
										
									}
									
									?>
                                           
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
       <!-- /.content-wrapper --> 