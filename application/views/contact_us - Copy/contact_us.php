<section class="">
        <div class="container">
            <div class="cartHeading fontWhtReg">
                <h1 class=""><?php echo $contact_us_label; ?></h1>
                <div class="clearfix"></div>
            </div>
            <div class="bannerImg wBox_w_Shadow">
                <img src="<?php echo base_url().'uploads/images/contactus/'.$contact_us['banner_image']; ?>" alt="Banner" height="370" width="1401" />
            </div>
            <div class="contactPage">
                <div class="row">
                    <div class="col-md-6 col-xs-12">
                        <h2><?php echo $contact_us_details_label; ?></h2>
                        <br/>
                        <?php echo $contact_us[$lang.'_contact_detail']; ?>
                        <br/>
                        <!--<p> <strong><?php /*echo $contact_us_headoffice_label; */?>,</strong></p>-->
                        
                            <?php echo $contact_us[$lang.'_head_office_address']; ?>
                            <span><?php echo $contact_us_phone_label; ?>: </span> 
                            <bdi><?php echo $contact_us[$lang.'_telephone']; ?></bdi>
                       <div class="clearfix"></div>
                        <br/>
                        <p><a href="mailto:<?php echo $contact_us['email']; ?>"><?php echo $contact_us['email']; ?></a> </p>
                        <div class="clearfix"></div>
                        <div class="row social_tellNo">
                            <div class="col-md-6">
                                <p><span><?php echo $contact_us_follow_us_label; ?>:</span></p>
                                <ul class="">
                                <?php
                                    $social_media = social_links();
                                    foreach ($social_media as $social) {
                                        ?>
                                        <?php if ($social->instagram_link != "") { ?>
                                            <li><a target="_blank" href="<?php echo $social->instagram_link; ?>" title="Instagram"><i
                                                            class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                        <?php }
                                        if ($social->twitter_link != "") {
                                            ?>
                                            <li><a target="_blank" href="<?php echo $social->twitter_link; ?>" title="Twitter"><i
                                                            class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                        <?php }
                                        if ($social->facebook_link != "") {
                                            ?>
                                            <li><a target="_blank" href="<?php echo $social->facebook_link; ?>" title="Facebook"><i
                                                            class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <?php }
                                    }?>
                                </ul>
                            </div>
                            <!--<div class="col-md-6">
                                <p><span><?php /*echo $contact_us_toll_free_label; */?>:</span></p>
                                <h3><?php /*echo $contact_us[$lang.'_toll_no']; */?></h3>
                            </div>-->
                        </div>
                    </div>
                    
                    <div class="col-md-6 col-xs-12">
                        <h2><?php echo $contact_us_feedback_label; ?></h2>
                        <form action="<?php echo lang_base_url().'contact_us/save'; ?>" method="post" name="contactForm" id="contactUs_id" onsubmit="return false;">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <label><?php echo $name_label; ?></label>
                                    <input type="text" name="full_name" id="name_id" placeholder="<?php echo $enter_here_palceholder; ?>" />
                                </div>
                                
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <label><?php echo $country_label; ?></label>
                                    <select name="country" id="country" class="countryClassCart">
                                        <option value=""><?php echo $select_dropdown; ?></option>
                                        <?php echo getCoutries();?>
                                        
                                    </select>
                                </div>
                                
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <label><?php echo $city_label; ?></label>
                                    <select name="city" id="city" class="cityClassCart">
                                        <option selected value=""><?php echo $select_dropdown; ?></option>
                                        
                                    </select>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <label><?php echo $email_label; ?></label>
                                    <input type="text" name="email" id="email_id" placeholder="<?php echo $enter_here_palceholder; ?>" />
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label><?php echo $mobile_label; ?></label>
                                    <input type="text" name="mobile" id="mobile_id" placeholder="<?php echo $enter_here_palceholder; ?>" class="" value="+966 "/>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label><?php echo $feedback_detail_label; ?></label>
                                    <textarea placeholder="<?php echo $enter_here_palceholder; ?>" id="message_id" name="message"></textarea>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12 text-right" >
                                    <button type="submit" id="submit_id" class="btn btn-black"><?php echo $submit_btn_label; ?></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>