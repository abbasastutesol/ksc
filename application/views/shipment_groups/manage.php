 <!-- Content Wrapper. Contains page content -->

      <div id="page_content">

        <div id="page_content_inner">
        <?php if(viewEditDeleteRights('Shipment Groups','add')) { ?>
            <a href="<?php echo base_url();?>admin/shipment_groups/add" class="md-btn" > Add</a>
        <?php } ?>
			<div class="md-card">

                <div class="md-card-content">

                    <div class="uk-grid" data-uk-grid-margin>

                        <div class="uk-width-1-1">

                            <div class="uk-overflow-container"> 

                                <table class="uk-table uk-table-align-vertical listing dt_default">

                                    <thead>

                                        <tr>

                                             <th>Sr#</th>



                                              <th class="nosort">Group Name</th>

                      

                                             <th class="nosort">Normal Shipment Amount</th>
                                             
                                             
                                             <th class="nosort">Fast Shipment Amount</th>



                                              <th>Active</th>


                                            <?php if(viewEditDeleteRights('Shipment Groups','edit') || viewEditDeleteRights('Shipment Groups','delete')) { ?>
                                              <th class="nosort">Action</th>
                                            <?php } ?>

                                        </tr>

                                    </thead>

                                    <tbody>

                                    <?php 

									$i=1;

									foreach($groups as $group)

									{?>

                                    	<tr id="<?php echo $group->id;?>">

                                       		<td class="uk-text-center"><?php echo $i;?></td>

                                        	<td><?php echo $group->eng_group_name;?></td>
                                            
                                            <td><?php echo $group->normal_shipment_amount;?></td>
                                            
                                            <td><?php echo $group->fast_shipment_amount;?></td>

                                            <td><?php echo ($group->active_status == '1' ? '<i class="material-icons md-color-light-blue-600 md-24">&#xE86C;</i>' : '<i class="material-icons">highlight_off</i>');?></td>

                                            <?php if(viewEditDeleteRights('Shipment Groups','edit') || viewEditDeleteRights('Shipment Groups','delete')) { ?>
                                            <td>
                                                <?php if(viewEditDeleteRights('Shipment Groups','edit') ) { ?>
                                                <a href="<?php echo base_url().'admin/shipment_groups/edit/'.$group->id;?>" title="Edit Groups"><i class="md-icon material-icons">&#xE254;</i>
                                                </a>
                                                <?php } ?>
                                                <?php if(viewEditDeleteRights('Shipment Groups','delete')) { ?>
                                                <a href="javascript:void(0);" class="uk-margin-left delete" onclick="deleteRecord('<?php echo $group->id;?>','admin/shipment_groups/action','');" title="Delete Jobs"> <i class="material-icons md-24 delete">&#xE872;</i>
                                                </a>
                                                <?php } ?>
                                            </td>
                                            <?php } ?>

                                    	</tr>

                                    <?php 

										$i++;

									}?>

                                    </tbody>

                                </table>

                            </div>

                            

                        </div>

                    </div>

                </div>

            </div>



        </div>

    </div>

       <!-- /.content-wrapper --> 