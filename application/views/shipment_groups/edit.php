<div id="page_content">
  <div id="page_content_inner">
    <div class="md-card">
      <div class="md-card-content">
        <h3 class="heading_a">Edit a group</h3><br /><br />
        <form action="<?php echo base_url(); ?>admin/shipment_groups/action" method="post" onsubmit="return false" class="ajax_form">
		<div class="uk-grid" data-uk-grid-margin>
		
		<input type="hidden" name="form_type" value="update">
		<input type="hidden" name="id" value="<?php echo $group->id;?>"> 
        <?php foreach($cities as $city){?>
        	<input type="hidden" class="city_ids" value="<?php echo $city;?>"> 
        <?php }?>  
          <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
              <label>English Group Name</label>
              <input type="text" class="md-input" value="<?php echo $group->eng_group_name;?>" name="eng_group_name" />
            </div>
			
          </div>
          <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
              <label>Arabic Group Name</label>
              <input type="text" class="md-input" value="<?php echo $group->arb_group_name;?>" name="arb_group_name" />
            </div>
			
          </div>
          <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
              <label>Normal Shipment Amount</label>
              <input type="text" class="md-input" value="<?php echo $group->normal_shipment_amount;?>" name="normal_shipment_amount" />
            </div>
            
          </div>
          <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
              <label>Fast Shipment Amount</label>
              <input type="text" class="md-input" value="<?php echo $group->fast_shipment_amount;?>" name="fast_shipment_amount" />
            </div>
            
          </div>
          <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
              <label>English Normal Shipment Days</label>
              <input type="text" class="md-input" value="<?php echo $group->eng_normal_shipment_days;?>" name="eng_normal_shipment_days" />
            </div>
            
          </div>
          <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
              <label>Arabic Normal Shipment Days</label>
              <input type="text" class="md-input" value="<?php echo $group->arb_normal_shipment_days;?>" name="arb_normal_shipment_days" />
            </div>
            
          </div>
          <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
              <label>English Fast Shipment Days</label>
              <input type="text" class="md-input" value="<?php echo $group->eng_fast_shipment_days;?>" name="eng_fast_shipment_days" />
            </div>
            
          </div>
          <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
              <label>Arabic Fast Shipment Days</label>
              <input type="text" class="md-input" value="<?php echo $group->arb_fast_shipment_days;?>" name="arb_fast_shipment_days" />
            </div>
            
          </div>
          <div class="uk-form-row uk-width-1-1">
              <div class="uk-grid" data-uk-grid-margin="">
                  <h5>Shipment Types</h5>
                  <div class="uk-width-medium-1-1 uk-row-first">
                      <div class=" uk-width-1-1">
                        <span class="icheck-inline">
                            <input type="radio" name="based_on" <?php echo ($group->based_on == 1 ? 'checked="checked"' : '')?> class="showDropDowns" value="1"/>
                            <label for="shipment_methods" class="inline-label">Country Based</label>
                            <input type="radio" name="based_on" <?php echo ($group->based_on == 2 ? 'checked="checked"' : '')?> value="2" class="showDropDowns"/>
                            <label for="shipment_methods" class="inline-label">City Based</label>
                        </span>
                                     
                      </div>
                  </div>
              </div>
          </div>
          	<div id="country_base" class="uk-width-medium-1-1 hideDropdown" <?php echo ($group->based_on == 2 ? 'style="display:none;"' : '')?>>
                <label>Country</label>
                <div class="uk-form-row">
                    <select class="md-input" name="country[]" id="country" data-md-selectize data-md-selectize-bottom                            data-uk-tooltip="{pos:'top'}" title="Select Country" multiple="multiple">
                        <option value="">Select Country</option>
						
                        <?php 
						
						if($group->based_on == 1)
						{
							echo getCountriesBackendArr(array_unique($countries));
						}
                        else
                        {
							echo getCountriesBackend();
                        }
						?>
                    </select>
                </div>
            </div>
            <div id="city_base" class="uk-width-medium-1-2 hideDropdown" <?php echo ($group->based_on == 1 ? 'style="display:none;"' : '')?>>
                <label>Country</label>
                <div class="uk-form-row">
                    <select class="md-input reg_country_selectize" name="scountry" id="country">
                        <option value="">Select Country</option>

                        <?php 
						
						if($group->based_on == 2)
						{
							echo getCountriesBackendArr(array_unique($countries));
						}
                        else
                        {
							echo getCountriesBackend();
                        }
						?>

                    </select>
                </div>
            </div>
            <div id="city_base2" class="uk-width-medium-1-2 hideDropdown" <?php echo ($group->based_on == 1 ? 'style="display:none;"' : '')?>>
                <label>City</label>
                <div class="uk-form-row">

                    <select class="md-input" id="city"  name="city[]" data-md-selectize data-md-selectize-bottom                            data-uk-tooltip="{pos:'top'}" title="Select City" multiple="multiple">
                        
						<?php 
						
						if($group->based_on == 2)
						{
							echo getCityAdminArrIds(array_unique($countries), $cities);
						}
                        else
                        {
							echo '<option value="">Select City</option>';
                        }
						?>
						
                    </select>
                </div>
            </div>

          
          <div class="uk-width-medium-1-4">
            <input type="checkbox" data-switchery data-switchery-size="large" <?php echo ($group->active_status == 1 ? 'checked' : '')?> id="switch_demo_large" name="active_status" />
            <label for="switch_demo_large" class="inline-label">Active</label>
           
			</div>
			
			<div class="uk-form-row uk-width-1-1">
            	<div class="uk-grid" data-uk-grid-margin="">
                	<h5>Payment Types</h5>
                    <div class="uk-width-medium-1-1 uk-row-first">
                    	<div class=" uk-width-1-1">
                            <span class="icheck-inline">
                                <input type="checkbox" id="visa" name="visa" value="1" <?php echo (in_array('1', $payment_method) ? 'checked' : '')?> data-md-icheck/>
                                <label for="visa" class="inline-label">Visa</label>
                            </span>
                        	<span class="icheck-inline">
                                <input type="checkbox" name="sadad" id="sadad" value="1" <?php echo (in_array('2', $payment_method) ? 'checked' : '')?> data-md-icheck/>
                                <label for="sadad" class="inline-label">Sadad</label>
                        	</span>
                       		<span class="icheck-inline">
                                <input type="checkbox" name="cash_on_delivery" id="cash_on_delivery" value="1" <?php echo (in_array('3', $payment_method) ? 'checked' : '')?> data-md-icheck/>
                                <label for="cash_on_delivery" class="inline-label">Cash</label>
                        	</span>
	                        <span class="icheck-inline">
                                <input type="checkbox" name="transfer" id="transfer" value="1" <?php echo (in_array('4', $payment_method) ? 'checked' : '')?> data-md-icheck/>
                                <label for="transfer" class="inline-label">Transfer</label>
    	                    </span>
                        </div>
                    </div>
                </div>
            </div>
	
        </div>
       </form> 
      </div>
    </div>
   
    
    
    
    
  </div>
</div>
<div class="md-fab-wrapper">
        <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">
            <i class="material-icons">&#xE161;</i>
        </a>
    </div>
<div class="md-fab-wrapper" style="right:95px;">
        <a class="md-fab md-fab-primary" href="<?php echo base_url();?>admin/shipment_groups" id="">
            <i class="material-icons">keyboard_backspace</i>
        </a>
    </div>