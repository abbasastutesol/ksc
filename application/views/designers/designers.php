<section>
	<div class="defaultPageSty">
		<div class="container">
			<h1><?php echo $login_designers;?> </h1>
		</div>
		<div class="line"></div>
		<div class="container">
			<div class="designersSec">
				<ul class="row">
                	<?php foreach($designers as $designer){?>
						<li class="col-md-2 col-sm-4 col-xs-6">
                            <div class="imgBox"><img src="<?php echo base_url().'uploads/images/designer/'.$designer['image'];?>" alt="designers" height="113" width="113" /></div>
                            <h2><?php echo $designer[$lang.'_name']?></h2>
                            <h3><?php echo $designers_design.': '.$designer['no_of_designs'];?></h3>
                            <h4><?php echo getCategoriesNames(explode(',', $designer['categories']));?></h4>
                        </li>
                    <?php }?>
				</ul>
			</div>
		</div>
	</div>
	
</section>