 <!-- Content Wrapper. Contains page content -->
      <div id="page_content">
        <div id="page_content_inner">
		<a href="<?php echo base_url();?>admin/designer/add" class="md-btn" > Add</a>
			<div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <div class="uk-overflow-container"> 
                                <table class="uk-table uk-table-align-vertical listing dt_default">
                                    <thead>
                                        <tr>
                                             <th>Sr#</th>

                                              <th class="nosort">Name</th>
                      
                                              <th class="nosort"> Image</th>
                                              
                                              <th class="nosort"> No Of Designs</th>
                                              
                                              <th class="nosort">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
										  <?php 
                                          $i=1;
                                          foreach($designers as $designer)
                                          {?>
                                              <tr id="<?php echo $designer->id;?>">
                                                  <td class="uk-text-center"><?php echo $i;?></td>
                                                  <td><?php echo $designer->eng_name;?></td>
                                                  <td><img src="<?php echo base_url().'uploads/images/designer/'.$designer->image;?>" alt="no image" width="100" /></td>
                                                  <td><?php echo $designer->no_of_designs;?></td>
                                                  <td><a href="<?php echo base_url().'admin/designer/edit/'.$designer->id;?>" title="Edit designer">
													<i class="md-icon material-icons">&#xE254;</i>
													</a>
													<a href="javascript:void(0);" class="uk-margin-left delete" onclick="deleteRecord(<?php echo $designer->id;?>,'admin/designer/action','');" title="Delete Designer"> <i class="material-icons md-24 delete">&#xE872;</i></a></td>
                                              </tr>
                                          <?php 
                                              $i++;
                                          }?>   
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
       <!-- /.content-wrapper --> 