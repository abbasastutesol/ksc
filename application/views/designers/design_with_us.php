<section>
	<div class="defaultPageSty">
		<div class="container">
			<h1><?php echo $login_design_with_com;?> </h1>
		</div>
		<div class="line"></div>
		<form action="<?php echo base_url();?>designWithUs/submit" method="post" onsubmit="return false;" id="designWith_form" class="submit_design_with" enctype="multipart/form-data">
			<div class="container max960_cont">
				<div class="row">
					<div class="col-md-4 col-sm-4 col-xs-12">
						<label><?php echo $jobs_send_your_resume_name;?><span class="redStar">*</span></label>
						<input type="text" name="name" id="name" data-toggle="tooltip" data-placement="top" title="" />
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<label><?php echo $chat_email;?><span class="redStar">*</span></label>
						<input type="text" name="email" id="email" data-toggle="tooltip" data-placement="top" title="" />
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12" style="padding-bottom:10px;">
						<label><?php echo $register_mobile_number;?><span class="redStar">*</span></label>
						<input type="text" name="mobile_no" id="mobile_no" data-toggle="tooltip" data-placement="top" title="" value="" />
                        <input type="hidden" name="mobile_ios2_code" id="mobile_ios2_code" value="sa|966" />
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12 btnBootStlEd">
						<label><?php echo $design_with_us_upload_cv;?><span class="redStar">*</span> <?php echo ' (pdf, doc, docx)';?></label>
						<div class="fileUploader">
							<input type="text" class="showFileName" readonly="readonly">
							<div class="edBtn showFileType"><?php echo $jobs_upload;?></div>
							<input type="file" class="edBtn attachCV" name="cv" id="cv" data-toggle="tooltip" data-placement="top" title="" onchange="hideToolTip('cv');">
						</div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12 btnBootStlEd">
						<label><?php echo $design_with_us_upload_portfolio;?><span class="redStar">*</span> <?php echo ' (pdf, doc, docx)';?></label>
						<div class="fileUploader">
							<input type="text" class="showFileName" readonly="readonly">
							<div class="edBtn showFileType"><?php echo $jobs_upload;?></div>
							<input type="file" class="edBtn attachCV" name="portfolio[]" multiple="multiple" id="portfolio" data-toggle="tooltip" data-placement="top" title="" onchange="hideToolTip('portfolio');">
						</div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<label><?php echo $design_with_us_Website?></label>
						<input type="text" name="website" id="website"/>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<label><?php echo $register_country;?><span class="redStar">*</span></label>
                        <select name="country" id="country" data-toggle="tooltip" data-placement="top" title="">
                        	<option value=""><?php echo $register_country;?></option>
                            <?php echo getCoutries();?>
                        </select>
					</div>
					<div class="col-md-8 col-sm-8 col-xs-12 design_with_usBTN">
						<label>&nbsp;</label>
						<input class="edBtn gray" type="button" onclick="document.getElementById('designWith_form').reset();" value="<?php echo $settings_cancel;?>">
						<input type="submit" class="edBtn red" value="<?php echo $register_send;?>">
					</div>
				</div>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
			</div>
		</form>
	</div>
</section>