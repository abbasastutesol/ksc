<div id="page_content">
  <div id="page_content_inner">
    <div class="md-card">
      <div class="md-card-content">
        <h3 class="heading_a">Add a Designer</h3>
        <form action="<?php echo base_url(); ?>admin/designer/action" method="post" onsubmit="return false" class="ajax_form">
		<div class="uk-grid" data-uk-grid-margin>
		
		<input type="hidden" name="form_type" value="save">
		<div class="uk-width-medium-1-1">
            <select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}" title="Select with tooltip" name="category_id[]" multiple="multiple">
              <option value="">Select Category</option>
			  <?php
			  getCategoriesOptionListing();
			  ?>
              
            </select>
        </div> 
          <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
              <label>English Name</label>
              <input type="text" class="md-input" value="" name="eng_name" />
            </div>
			
          </div>
          <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
              <label>Arabic Name</label>
              <input type="text" class="md-input" value="" name="arb_name" />
            </div>
            
          </div>
		  
		  <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
              <label>No of Designs</label>
              <input type="number" class="md-input" value="" name="no_of_designs" />
            </div>
            
          </div>
		  
		   
			 
			
			  <div class="uk-width-large-1-1">
                    <h3 class="heading_a">
                        Upload Designer Image
                       
                    </h3>
                    <span style="color:red">(113 * 113 pixels)</span>
                    <div class="uk-grid">
                        <div class="uk-width-1-1">
                            <div id="file_upload-drop" class="uk-file-upload">
                                <a class="uk-form-file md-btn">choose file<input id="file_upload-select" class="images_upload" type="file" name="image"></a>
                            </div>
                            <div id="file_upload-progressbar" class="uk-progress uk-hidden">
                                <div class="uk-progress-bar" style="width:0">0%</div>
                            </div>
                        </div>
                    </div>
                </div>
			
			
	
        </div>
       </form> 
      </div>
    </div>
   
    
    
    
    
  </div>
</div>

<div class="md-fab-wrapper">
        <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">
            <i class="material-icons">&#xE161;</i>
        </a>
    </div>
<div class="md-fab-wrapper" style="right:95px;">
        <a class="md-fab md-fab-primary" href="<?php echo base_url();?>admin/designer" id="">
            <i class="material-icons">keyboard_backspace</i>
        </a>
    </div>