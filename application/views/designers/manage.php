 <!-- Content Wrapper. Contains page content -->
      <div id="page_content">
        <div id="page_content_inner">
			<div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <div class="uk-overflow-container"> 
                                <table class="uk-table uk-table-align-vertical listing dt_default">
                                    <thead>
                                        <tr>
                                             <th>Sr#</th>

                                              <th class="nosort">Name</th>
                      
                                              <th class="nosort"> Email</th>
                                              
                                              <th class="nosort"> Mobile No</th>
                                              
                                              <th class="nosort">Applied On</th>
                      
                                              <th class="nosort">Action</th>
                                              
                                        </tr>
                                    </thead>
                                    <tbody>
                                       	<?php 
											$i=1;
											foreach($designers as $designer)
											{
												$mobile_no = $designer->mobile_no;
												if($designer->mobile_ios2_code != '')
												{
													$mobile_code = explode('|', $designer->mobile_ios2_code);
													$mobile_no = '+'.$mobile_code[1].' '.$designer->mobile_no;
												}
												?>
												<tr id="<?php echo $designer->id;?>">
													<td class="uk-text-center"><?php echo $i;?></td>
													<td><?php echo $designer->name;?></td>
                                                    <td><?php echo $designer->email;?></td>
                                                    <td><?php echo $mobile_no;?></td>
                                                    <td><?php echo date('d M Y - h:i a', strtotime($designer->created_at))?></td>
													<td><a href="<?php echo base_url().'admin/designer/view/'.$designer->id;?>" title="View Designer"><i class="md-icon material-icons">visibility</i></a><a href="javascript:void(0);" class="uk-margin-left delete" onclick="deleteRecordAll('<?php echo $designer->id;?>','admin/designer/action','','deleteDesigner');" title="Delete Jobs"> <i class="material-icons md-24 delete">&#xE872;</i></a></td>
												</tr>
											<?php 
												$i++;
											}?>   
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
       <!-- /.content-wrapper --> 