<div id="page_content">
  <div id="page_content_inner">
    <div class="md-card">
      <div class="md-card-content">
        <h3 class="heading_a">Designer Detail</h3><br>
        
		<div class="uk-grid" data-uk-grid-margin>
		
          <div class="uk-width-medium-1-1">
            
            <div class="uk-form-row">
              <label><strong>Name: </strong></label>
              <?php echo $designer->name?>
            </div>
          </div>
          
          <div class="uk-width-medium-1-1">
            
            <div class="uk-form-row">
              <label><strong>Email: </strong></label>
              <?php echo $designer->email?>
            </div>
          </div>
          <?php 
		  	$mobile_no = $designer->mobile_no;
			if($designer->mobile_ios2_code != '')
			{
				$mobile_code = explode('|', $designer->mobile_ios2_code);
				$mobile_no = '+'.$mobile_code[1].' '.$designer->mobile_no;
			}
		  ?>
          <div class="uk-width-medium-1-1">
            
            <div class="uk-form-row">
              <label><strong>Mobile No: </strong></label>
              <?php echo $mobile_no?>
            </div>
          </div>
          <div class="uk-width-medium-1-1">
            
            <div class="uk-form-row">
              <label><strong>Website: </strong></label>
              <?php echo $designer->website?>
            </div>
          </div>
          <div class="uk-width-medium-1-1">
            
            <div class="uk-form-row">
              <label><strong>Country: </strong></label>
              <?php echo $designer->country?>
            </div>
          </div>
          
          <div class="uk-width-medium-1-1">
            
            <div class="uk-form-row">
              <label><strong>Applied On: </strong></label>
              <?php echo date('d M Y - h:i a', strtotime($designer->created_at))?>
            </div>
          </div>
           
			<div class="uk-width-medium-1-1">
            
            <div class="uk-form-row">
              <label><strong>Designer CV: </strong></label>
              	<a href="<?php echo base_url();?>uploads/images/designer/cv/<?php echo $designer->cv?>"> <i class="md-icon material-icons">insert_drive_file</i></a>
            </div>
          </div>
          
          
          <div class="uk-width-medium-1-1">
            
            <div class="uk-form-row">
              <label><strong>Designer Portfolio: </strong></label>
              	<?php foreach($designer_portfolio as $des_port){?>
              		<a href="<?php echo base_url();?>uploads/images/designer/portfolio/<?php echo $des_port->portfolio?>"> <i class="md-icon material-icons">insert_drive_file</i><?php echo $des_port->portfolio?></a>
                <?php }?>
            </div>
          </div>
		  
			
                
         
        </div>
       
      </div>
    </div>
   
   <!-- light box for image -->
		   <div class="uk-modal" id="modal_lightbox">
              <div class="uk-modal-dialog uk-modal-dialog-lightbox">
                  <button type="button" class="uk-modal-close uk-close uk-close-alt"></button>
                  <img src="<?php echo base_url().'uploads/images/categories/'.$variant->eng_image; ?>" alt=""/>
                  
              </div>
           </div>
			<!-- end light box for image -->   
    
    
    
  </div>
</div>

<div class="md-fab-wrapper" style="right:95px;">
        <a class="md-fab md-fab-primary" href="<?php echo base_url();?>admin/designer/applications" id="">
            <i class="material-icons">keyboard_backspace</i>
        </a>
    </div>