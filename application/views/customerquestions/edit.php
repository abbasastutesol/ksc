<div id="page_content">
  <div id="page_content_inner">
    <div class="md-card">
      <div class="md-card-content">
        <h3 class="heading_a">Answer a Question</h3><br /><br />
        <form action="<?php echo base_url(); ?>admin/customerQuestions/action" method="post" onsubmit="return false" class="ajax_form">
		<div class="uk-grid" data-uk-grid-margin>
		
		<input type="hidden" name="form_type" value="update">
        <input type="hidden" name="id" value="<?php echo $question->id;?>">
		<br />
		<br />	 
          <div class="uk-width-medium-1-1">
            <div class="uk-form-row">
              <label style="color:#727272;">Question</label>
              <input type="text" class="md-input" value="<?php echo $question->question;?>" disabled="disabled" readonly="readonly"/>
            </div>
			
          </div>
          <div class="uk-width-medium-1-1">
            <div class="uk-form-row">
              <label>Answer</label>
              <input type="text" class="md-input" value="<?php echo $question->answer;?>" name="answer" />
            </div>
            
          </div>		
			
	
        </div>
       </form> 
      </div>
    </div>
  
    
    
    
  </div>
</div>
<div class="md-fab-wrapper">
        <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">
            <i class="material-icons">&#xE161;</i>
        </a>
    </div>
<div class="md-fab-wrapper" style="right:95px;">
        <a class="md-fab md-fab-primary" href="<?php echo base_url();?>admin/customerQuestions/manage/<?php echo $question->product_id;?>" id="">
            <i class="material-icons">keyboard_backspace</i>
        </a>
    </div>