 <!-- Content Wrapper. Contains page content -->
      <div id="page_content">
        <div id="page_content_inner">
			<div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <div class="uk-overflow-container"> 
                                <table class="uk-table uk-table-align-vertical listing dt_default">
                                    <thead>
                                        <tr>
                                             <th>Sr#</th>

                                              <th class="nosort">Question</th>
                                              
                                              <th class="nosort">Answered</th>                  
                                             
                                              <th class="nosort">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php
											 $index = 1;
											 foreach($questions as $question){?>
												<tr id="<?php echo $question->id;?>">
													 <td><?php echo $index;?></td>
							  
													  <td><?php echo $question->question;?></td>
													  
													  <td><?php echo ($question->answered == '1' ? '<i class="material-icons md-color-light-blue-600 md-24">&#xE86C;</i>': '');?></td>
                                                                                                            
                                                      <td><a href="<?php echo base_url().'admin/customerQuestions/answer/'.$question->id;?>" title="Edit Discount">
													<i class="md-icon material-icons">&#xE254;</i>
													</a>
													</td>
												</tr>
											 <?php 
											 $index++;
											 }?> 
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
       <!-- /.content-wrapper --> 
       <div class="md-fab-wrapper" style="right:95px;">
        <a class="md-fab md-fab-primary" href="<?php echo base_url();?>admin/product" id="">
            <i class="material-icons">keyboard_backspace</i>
        </a>
    </div>