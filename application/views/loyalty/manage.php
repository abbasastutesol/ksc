<div id="page_content">
    <div id="page_content_inner">
        <div class="md-card">
            <div class="md-card-content"><h3 class="heading_a">Edit Loyalty</h3><br>
                <form action="<?php echo base_url(); ?>admin/loyalty/update" method="post" onsubmit="return false"
                      class="ajax_form">
                    <div class="uk-grid" data-uk-grid-margin>
                        <input type="hidden" name="id" value="<?php echo $loyalty->id; ?>">


                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row">
                                <label>Silver Loyalty Point (For 1 SR)</label>
                                <input type="text" class="md-input" value="<?php echo $loyalty->silver_points; ?>" name="silver_points"/></div>
                        </div>

                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row">
                                <label>Golden Loyalty Point (For 1 SR)</label>
                                <input type="text" class="md-input" value="<?php echo $loyalty->golden_points; ?>" name="golden_points"/></div>
                        </div>

                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row">
                                <label>Platinum Loyalty Point (For 1 SR)</label>
                                <input type="text" class="md-input" value="<?php echo $loyalty->platinum_points; ?>" name="platinum_points"/></div>
                        </div>

                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row">
                                <label>Sliver Loyalty Start</label>
                                <input type="text" class="md-input" value="<?php echo $loyalty->sliver_start_rang; ?>" name="sliver_start_rang"/></div>
                        </div>


                    <div class="uk-width-medium-1-2">
                        <div class="uk-form-row">
                            <label>Sliver Loyalty End</label>
                            <input type="text" class="md-input" value="<?php echo $loyalty->sliver_end_rang; ?>" name="sliver_end_rang"/></div>
                    </div>

                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row">
                                <label>Golden Loyalty Start</label>
                                <input type="text" class="md-input" value="<?php echo $loyalty->golden_start_rang; ?>" name="golden_start_rang"/></div>
                        </div>

                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row">
                                <label>Golden Loyalty End</label>
                                <input type="text" class="md-input" value="<?php echo $loyalty->golden_end_rang; ?>" name="golden_end_rang"/></div>
                        </div>

                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row">
                                <label>Platinum Loyalty Start</label>
                                <input type="text" class="md-input" value="<?php echo $loyalty->platinum_start_rang; ?>" name="platinum_start_rang"/></div>
                        </div>

                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row" style="margin-top: 20px;">
                                <label>Platinum Loyalty End</label>
                                <input type="text" class="md-input" value="<?php echo $loyalty->platinum_end_rang; ?>" name="platinum_end_rang"/></div>
                        </div>


                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row" style="margin-top: 20px;">
                                <label>Silver Discount</label>
                                <input type="text" class="md-input" value="<?php echo $loyalty->silver_discount_amount; ?>" name="silver_discount_amount"/></div>
                        </div>


                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row">
                                <label>Silver Discount Type</label>
                                <select id="silver_discount_type" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}" title="Select with tooltip" name="silver_discount_type">

                                    <option <?php if ($loyalty->silver_discount_type == '1') { ?> selected="selected" <?php } ?> value="1">Amount
                                    </option>

                                    <option <?php if ($loyalty->silver_discount_type == '2') { ?> selected="selected" <?php } ?> value="2">Percentage
                                    </option>


                                </select>
                            </div>
                        </div>


                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row" style="margin-top: 20px;">
                                <label>Golden Discount</label>
                                <input type="text" class="md-input" value="<?php echo $loyalty->golden_discount_amount; ?>" name="golden_discount_amount"/></div>
                        </div>

                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row">
                                <label>Golden Discount Type</label>
                                <select id="golden_discount_type" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}" title="Select with tooltip" name="golden_discount_type">

                                    <option <?php if ($loyalty->golden_discount_type == '1') { ?> selected="selected" <?php } ?> value="1">Amount
                                    </option>

                                    <option <?php if ($loyalty->golden_discount_type == '2') { ?> selected="selected" <?php } ?> value="2">Percentage
                                    </option>


                                </select>
                            </div>
                        </div>



                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row" style="margin-top: 20px;">
                                <label>Platinum Discount</label>
                                <input type="text" class="md-input" value="<?php echo $loyalty->platinum_discount_amount; ?>" name="platinum_discount_amount"/></div>
                        </div>


                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row">
                                <label>Platinum Discount Type</label>
                                <select id="platinum_discount_type" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}" title="Select with tooltip" name="platinum_discount_type">

                                    <option <?php if ($loyalty->platinum_discount_type == '1') { ?> selected="selected" <?php } ?> value="1">Amount
                                    </option>

                                    <option <?php if ($loyalty->platinum_discount_type == '2') { ?> selected="selected" <?php } ?> value="2">Percentage
                                    </option>


                                </select>
                            </div>
                        </div>



                        <div class="uk-width-medium-1-2">
                            <input type="checkbox" value="1" data-switchery data-switchery-size="large" <?php echo($loyalty->active_discount == 1 ? 'checked' : ''); ?>
                                                                id="active_discount" name="active_discount"/> <label
                                    for="active_discount" class="inline-label">Enable Discount ?</label>
                        </div>


                    </div>



                </form>
            </div>
        </div>
    </div>
</div>
<div class="md-fab-wrapper"><a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id=""> <i
                class="material-icons">&#xE161;</i>
    </a>
</div>
