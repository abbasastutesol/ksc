 <!-- Content Wrapper. Contains page content -->
      <div id="page_content">
        <div id="page_content_inner">
            <?php if(viewEditDeleteRights('Jobs','add')) { ?>
		<a href="<?php echo base_url();?>admin/career/add" class="md-btn" > Add</a>
            <?php } ?>
			<div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <div class="uk-overflow-container"> 
                                <table class="uk-table uk-table-align-vertical listing dt_default">
                                    <thead>
                                        <tr>
                                             <th>Sr#</th>

                                              <th class="nosort">Title</th>
                      
                                             
                      
                                              <th>Active</th>
                                            <?php if(viewEditDeleteRights('Jobs','edit') || viewEditDeleteRights('Jobs','delete')) { ?>
                                              <th class="nosort">Action</th>
                                            <?php } ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
									$i=1;
									foreach($jobs as $job)
									{?>
                                    	<tr id="<?php echo $job->id;?>">
                                       		<td class="uk-text-center"><?php echo $i;?></td>
                                        	<td><?php echo $job->eng_title;?></td>
                                            <td><?php echo ($job->active == '1' ? '<i class="material-icons md-color-light-blue-600 md-24">&#xE86C;</i>' : '<i class="material-icons">highlight_off</i>');?></td>

                                            <?php if(viewEditDeleteRights('Jobs','edit') || viewEditDeleteRights('Jobs','delete')) { ?>
                                            <td>
                                                <?php if(viewEditDeleteRights('Jobs','edit')) { ?>
                                                <a href="<?php echo base_url().'admin/career/edit/'.$job->id;?>" title="Edit Jobs">
                                                    <i class="md-icon material-icons">&#xE254;</i>
                                                </a>
                                                <?php } ?>
                                                <?php if(viewEditDeleteRights('Jobs','delete')) { ?>
                                            <a href="javascript:void(0);" class="uk-margin-left delete" onclick="deleteRecord('<?php echo $job->id;?>','admin/career/action','');" title="Delete Jobs">
                                                <i class="material-icons md-24 delete">&#xE872;</i>
                                            </a>
                                                <?php } ?>
                                            </td>
                                            <?php } ?>
                                    	</tr>
                                    <?php 
										$i++;
									}?>
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
       <!-- /.content-wrapper --> 