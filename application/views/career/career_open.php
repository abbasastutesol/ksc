
    <section class="">
        <div class="container">
            <div class="cartHeading fontWhtReg with paragraph">
                <h1 class=""><?php echo $careers_label; ?></h1>
                <?php echo $career[$lang.'_description']; ?>
                <div class="clearfix"></div>
            </div>
            <div class="openingJobSec">
                <div class="contactPage">
                    <div class="row">
                        <div class="col-md-6 col-xs-12 jobDescription">
                            <h2>
                                <?php echo $contact_us_details_label; ?>
							<span><?php echo getCityById($jobs['city']);?>, <?php echo getCoutryByCode($jobs['country']); ?></span>
                            </h2>
                            <ul>
                                <li><?php echo $jobs[$lang.'_title']; ?></li>
                            </ul>
                            <?php echo $jobs[$lang.'_description']; ?>
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-md-6 col-xs-12 jobDesForm">
                            <h2><?php echo $careers_apply_now_label; ?></h2>
                            <form action="<?php echo base_url().'career/action'; ?>" class="applied_jobs" method="post" enctype="multipart/form-data" onsubmit="return false;" id="apply_job" name="apply_job">
                                <input type="hidden" name="lang" value="<?php echo $lang;?>">
                                <input type="hidden" name="form_type" value="save">
                                <input type="hidden" name="job_id" value="<?php echo $jobs['id']; ?>">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label><?php echo $name_label; ?></label>
                                        <input type="text" name="name" id="name_id" placeholder="<?php echo $enter_here_palceholder; ?>"/>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label><?php echo $careers_age_label; ?></label>
                                        <select name="age" id="age_id">
                                            <option disabled selected><?php echo $select_dropdown; ?></option>
                                            <option value="20-30">20-30</option>
                                            <option value="30-40">30-40</option>
                                            <option value="40-50">40-50</option>
                                            <option value="50-60">50-60</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label><?php echo $email_label; ?></label>
                                        <input type="text" name="email" id="email_id" placeholder="<?php echo $enter_here_palceholder; ?>"/>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label><?php echo $mobile_label; ?></label>
                                        <input type="text" name="mobile" id="mobile_id" placeholder="<?php echo $enter_here_palceholder; ?>" class="" value="+966 "/>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label><?php echo $careers_experience_label; ?> </label>
                                        <select name="experience" id="experience_id">
                                            <option disabled selected><?php echo $select_dropdown; ?></option>
                                            <option value="1">1 <?php echo $register_year; ?></option>
                                            <option value="2">2 <?php echo $register_year; ?></option>
                                            <option value="3">3 <?php echo $register_year; ?></option>
                                            <option value="4">4 <?php echo $register_year; ?></option>
                                            <option value="5">5 <?php echo $register_year; ?></option>
                                            <option value="6">6 <?php echo $register_year; ?></option>
                                            <option value="7">7 <?php echo $register_year; ?></option>
                                            <option value="8">8 <?php echo $register_year; ?></option>
                                            <option value="9">9 <?php echo $register_year; ?></option>
                                            <option value="10">10 <?php echo $register_year; ?></option>
                                        </select>
                                    </div>
                                    
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label><?php echo $careers_cv_label; ?></label>
                                        <div class="fileUploader">
                                            <input class="showFileName" type="text">
                                            <div class="btn btn-black showFileType"><?php echo $browse_label; ?></div>
                                            <input class="edBtn attachCV fileTypeValidation" id="cv_id" name="cv" type="file" accept=".xlsx,.xls,.doc, .docx,.ppt, .pptx,.txt,.pdf">
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 col-xs-12 text-right" >
                                        <button type="submit" id="submit_id" class="btn btn-black"><?php echo $careers_apply_now_label; ?></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- popup -->
    <div class="modal fade iOudModel" id="applyNowPU" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel"><?php echo $career['popup_'.$lang.'_title']; ?></h4>
                </div>
                <div class="modal-body">
                    <div class="text-only-popup text-center" >
                        <p><?php echo $career['popup_'.$lang.'_desc']; ?></p>
                        <button class="btn btn-golden"  data-dismiss="modal"><?php echo $dismiss_label; ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>