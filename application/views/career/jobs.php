<section class="">
	<div class="container">
		<div class="cartHeading fontWhtReg with paragraph">
			<h1 class=""><?php echo $careers_label; ?></h1>
			<?php echo $career[$lang.'_description']; ?>
			<div class="clearfix"></div>
		</div>
		<div class="openingJobSec">
			<h2><?php echo $careers_job_open_label; ?></h2>
			<ul>
			  <?php foreach ($jobs as $job) { 
			 
			  $country_detail = getCoutryByCode($job['country']);
			  $city_detail = getCityById($job['city']);
			   
			  ?>
				<li class="haveEqHeight">
					<a href="<?php echo lang_base_url().'career/career_open/'.$job['id']; ?>"><h3><?php echo $job[$lang.'_title']; ?></h3></a>
					<p><?php echo $city_detail; ?>, <?php echo $country_detail; ?></p>
				</li>
			  <?php } ?>


			</ul>
			<div class="clearfix"></div>
		</div>
	</div>
</section>