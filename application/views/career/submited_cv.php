 <!-- Content Wrapper. Contains page content -->
      <div id="page_content">
        <div id="page_content_inner">
			<div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <div class="uk-overflow-container"> 
                                <table class="uk-table uk-table-align-vertical listing dt_default">
                                    <thead>
                                        <tr>
                                             <th>Sr#</th>
                                             <th class="nosort">Name</th>
                                             <th class="nosort">Email</th>
                                             <th class="nosort">Date</th>
                                             <th class="nosort">CV</th>
                                             <th class="nosort">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
									$i=1;
									foreach($applicants as $app)
									{?>
                                    	<tr id="<?php echo $app->id;?>">
                                       		<td class="uk-text-center"><?php echo $i;?></td>
                                        	<td><?php echo $app->name;?></td>
                                            <td><?php echo $app->email;?></td>
                                            <td><?php echo date('d M Y - h:i a', strtotime($app->created_at));?></td>
                                            <td><a href="<?php echo base_url().'uploads/cv/'.$app->cv;?>" target="_blank"><i class="material-icons md-color-light-blue-600 md-24">get_app</i></a></td>
                                            <td><a href="javascript:void(0);" class="uk-margin-left delete" onclick="deleteRecordAll('<?php echo $app->id;?>','admin/career/action','','deleteApplied');" title="Delete Jobs"> <i class="material-icons md-24 delete">&#xE872;</i></a></td>
                                    	</tr>
                                    <?php 
										$i++;
									}?>
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
       <!-- /.content-wrapper --> 