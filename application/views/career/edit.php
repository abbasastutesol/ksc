<div id="page_content">
  <div id="page_content_inner">
    <div class="md-card">
      <div class="md-card-content">
        <h3 class="heading_a">Edit a Job</h3><br /><br />
        <form action="<?php echo base_url(); ?>admin/career/action" method="post" onsubmit="return false" class="ajax_form">
		<div class="uk-grid" data-uk-grid-margin>
		
		<input type="hidden" name="form_type" value="update">
		<input type="hidden" name="id" value="<?php echo $job[0]->id;?>"> 
		<input type="hidden" name="tpl_name" value="jobs">
          <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
              <label>English Name</label>
              <input type="text" class="md-input" value="<?php echo $job[0]->eng_title;?>" name="eng_title" />
            </div>
			
          </div>
          <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
              <label>Arabic Name</label>
              <input type="text" class="md-input" value="<?php echo $job[0]->arb_title;?>" name="arb_title" />
            </div>
            
          </div>
		  
		  <div class="uk-width-medium-1-2">
            <label>English Description</label>
            <div class="uk-form-row">
              
              <textarea cols="30" rows="4" class="md-input eng_text" id="eng_description" name="eng_description"><?php echo $job[0]->eng_description;?></textarea>
            </div>
          </div>
          <div class="uk-width-medium-1-2">
            <label>Arabic Description</label>
            <div class="uk-form-row">
              
              <textarea cols="30" rows="4" class="md-input arb_text" id="arb_description" name="arb_description"><?php echo $job[0]->arb_description;?></textarea>
            </div>
          </div>


            <div class="uk-width-medium-1-2">
                <label>Country</label>
                <div class="uk-form-row">
                    <select class="md-input reg_country" name="country" id="country">
                        <option value="">Select Country</option>

                        <?php echo getCountriesBackend($job[0]->country);?>

                    </select>
                </div>
            </div>
            <div class="uk-width-medium-1-2">
                <label>City</label>
                <div class="uk-form-row">

                    <select class="md-input reg_city" id="city"  name="city">
                        <option value="<?php echo $job[0]->city;?>"><?php echo getCityById($job[0]->city, $lang);?></option>

                    </select>
                </div>
            </div>



          
          <div class="uk-width-medium-1-4">
            <label>Deparment</label>
                <div class="uk-form-row">

                    <select class="md-input " id="Deparment"  name="Deparment">
                        <option value="">Select Deparment</option>
						<?php  
						foreach($dep as $depr){  
						if($depr->id == $job[0]->deparment){ ?>
                                <option value="<?php echo $depr->id;?>" selected="selected"><?php echo $depr->eng_title; ?></option>
						<?php }else{ ?>
						 <option value="<?php echo $depr->id;?>"><?php echo $depr->eng_title; ?></option>
						<?php } }?>
                    </select>
                </div>
           
			
           
			</div>
			
			
	
        </div>
		<div class="md-card">
                <div class="md-card-toolbar">
                    <h3 class="md-card-toolbar-heading-text">              Keyword          </h3> </div>
                <!----<input type="hidden" name="home_page_id" value="<?php //echo $about_us->home_page_id?>" />--->
                
                <?php //$keyword = getPageData($about_us->home_page_id);?>
                    <div class="md-card-content large-padding">
                        <div class="uk-grid uk-grid-divider uk-grid-medium" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2">
                                <div class="uk-form-row">
                                    <label>English Meta Title</label>
                                    <input type="text" class="md-input" value="<?php echo trim($job[0]->eng_meta_title);?>" name="eng_meta_title" /> </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <div class="uk-form-row">
                                    <label>Arabic Meta Title</label>
                                    <input type="text" class="md-input" value="<?php echo trim($job[0]->arb_meta_title);?>" name="arb_meta_title" /> </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <div class="uk-form-row">
                                    <label>Englishn Meta Description</label>
                                    <textarea cols="30" rows="4" class="md-input" name="eng_meta_description">
                                        <?php echo trim($job[0]->eng_meta_description);?>
                                    </textarea>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <div class="uk-form-row">
                                    <label>Arabic Meta Description</label>
                                    <textarea cols="30" rows="4" class="md-input" name="arb_meta_description">
                                        <?php echo trim($job[0]->arb_meta_description);?>
                                    </textarea>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <div class="uk-form-row">
                                    <label>Englishn Meta Keyword</label>
                                    <textarea cols="30" rows="4" class="md-input" name="eng_meta_keyword">
                                        <?php echo trim($job[0]->eng_meta_keyword);?>
                                    </textarea>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <div class="uk-form-row">
                                    <label>Arabic Meta Keyword</label>
                                    <textarea cols="30" rows="4" class="md-input" name="arb_meta_keyword">
                                        <?php echo trim($job[0]->arb_meta_keyword);?>
                                    </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
       </form> 
      </div>
    </div>
     
  </div>
</div>
<div class="md-fab-wrapper">
        <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">
            <i class="material-icons">&#xE161;</i>
        </a>
    </div>
<div class="md-fab-wrapper" style="right:95px;">
        <a class="md-fab md-fab-primary" href="<?php echo base_url();?>admin/career/jobs" id="">
            <i class="material-icons">keyboard_backspace</i>
        </a>
    </div>

<!--these variable used in footer dont remove them-->
<script type="text/javascript">

    var country ="<?php echo $job->country;?>";
    var replacedCountry = country.replace('(', '').replace(')', '').replace(' ', '');

    var city ="<?php echo $job->city;?>";
    var replacedCity = city.replace('(', '').replace(')', '').replace(' ', '');

</script>