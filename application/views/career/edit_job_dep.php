<div id="page_content">
  <div id="page_content_inner">
    <div class="md-card">
      <div class="md-card-content">
        <h3 class="heading_a">Add Departments</h3><br /><br />
        <form action="<?php echo base_url(); ?>admin/career/action" method="post" onsubmit="return false" class="ajax_form">
		<div class="uk-grid" data-uk-grid-margin>
		
		<input type="hidden" name="form_type" value="update">
		<input type="hidden" name="tpl_name" value="deparments"> 
		<input type="hidden" name="id" value="<?php echo $dep[0]->id;?>"> 
		
          <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
              <label>English Name</label>
              <input type="text" class="md-input" value="<?php echo $dep[0]->eng_title;?>" name="eng_title" />
            </div>
			
          </div>
		  
          <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
              <label>Arabic Name</label>
              <input type="text" class="md-input" value="<?php echo $dep[0]->arb_title;?>" name="arb_title" />
            </div>
            
          </div>
		  
		  
        </div>
		
		<div class="md-card">
                <div class="md-card-toolbar">
                    <h3 class="md-card-toolbar-heading-text">              Keyword          </h3> </div>
                <!----<input type="hidden" name="home_page_id" value="<?php //echo $about_us->home_page_id?>" />--->
                
                <?php //$keyword = getPageData($about_us->home_page_id);?>
                    <div class="md-card-content large-padding">
                        <div class="uk-grid uk-grid-divider uk-grid-medium" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2">
                                <div class="uk-form-row">
                                    <label>English Meta Title</label>
                                    <input type="text" class="md-input" value="<?php echo $dep[0]->eng_meta_title;?>" name="eng_meta_title" /> </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <div class="uk-form-row">
                                    <label>Arabic Meta Title</label>
                                    <input type="text" class="md-input" value="<?php echo $dep[0]->arb_meta_title;?>" name="arb_meta_title" /> </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <div class="uk-form-row">
                                    <label>Englishn Meta Description</label>
                                    <textarea cols="30" rows="4" class="md-input" name="eng_meta_description">
                                        <?php echo $dep[0]->eng_meta_description;?>
                                    </textarea>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <div class="uk-form-row">
                                    <label>Arabic Meta Description</label>
                                    <textarea cols="30" rows="4" class="md-input" name="arb_meta_description">
                                        <?php echo $dep[0]->arb_meta_description;?>
                                    </textarea>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <div class="uk-form-row">
                                    <label>Englishn Meta Keyword</label>
                                    <textarea cols="30" rows="4" class="md-input" name="eng_meta_keyword">
                                        <?php echo $dep[0]->eng_meta_keyword;?>
                                    </textarea>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <div class="uk-form-row">
                                    <label>Arabic Meta Keyword</label>
                                    <textarea cols="30" rows="4" class="md-input" name="arb_meta_keyword">
                                        <?php echo $dep[0]->arb_meta_keyword;?>
                                    </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
       </form> 
      </div>
    </div>
   
    
    
    
    
  </div>
</div>
<div class="md-fab-wrapper">
        <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">
            <i class="material-icons">&#xE161;</i>
        </a>
    </div>
<div class="md-fab-wrapper" style="right:95px;">
        <a class="md-fab md-fab-primary" href="<?php echo base_url();?>admin/career/jobs" id="">
            <i class="material-icons">keyboard_backspace</i>
        </a>
    </div>