<div id="page_content">
  <div id="page_content_inner">
    <div class="md-card">
      <div class="md-card-content">
        <h3 class="heading_a">Edit Career</h3><br /><br />
        <form action="<?php echo base_url(); ?>admin/career/action" method="post" onsubmit="return false" class="ajax_form">
		<div class="uk-grid" data-uk-grid-margin>
		
		<input type="hidden" name="form_type" value="update_career">
		<input type="hidden" name="id" value="<?php echo $career->id;?>"> 		  
		  <div class="uk-width-medium-1-2">
			<label>English Description</label>
            <div class="uk-form-row">
              
              <textarea cols="30" rows="4" class="md-input eng_text" id="eng_description" name="eng_description"><?php echo $career->eng_description;?></textarea>
            </div>
          </div>
          <div class="uk-width-medium-1-2">
		  
            <label>Arabic Description</label>
            <div class="uk-form-row">
              
              <textarea cols="30" rows="4" class="md-input arb_text" id="arb_description" name="arb_description"><?php echo $career->arb_description;?></textarea>
            </div>
          </div>
		  
		  <div class="uk-width-medium-1-2"> 	 
			<label>Popup English Title</label>
				<div class="uk-form-row">
				<input type="text" class="md-input" name="popup_eng_title" value="<?php echo $career->popup_eng_title;?>">
			</div>		  
		  <label>Popup English Description</label>   
		  <div class="uk-form-row">          
		  <textarea cols="30" rows="4" class="md-input eng_text" id="popup_eng_desc" name="popup_eng_desc"><?php echo $career->popup_eng_desc;?></textarea>     
		  </div>       
		  </div>    
		  <div class="uk-width-medium-1-2">    
		  <label>Popup Arabic Title</label>
			<div class="uk-form-row">
			<input type="text" class="md-input" name="popup_arb_title" value="<?php echo $career->popup_arb_title;?>">
		  </div>
		  <label>Popup Arabic Description</label>   
		  <div class="uk-form-row">        
		  <textarea cols="30" rows="4" class="md-input arb_text" id="popup_arb_desc" name="popup_arb_desc"><?php echo $career->popup_arb_desc;?></textarea>  
          </div>         
		  </div>

	
        </div>
       </form> 
      </div>
    </div>

    
  </div>
</div>
<?php// if(viewEditDeleteRights('Career Content','edit')) { ?>
<div class="md-fab-wrapper">
        <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">
            <i class="material-icons">&#xE161;</i>
        </a>
    </div>
    <?php //} ?>

<?php if(isset($light_boxes) && $light_boxes != ''){
echo '<script>
		light_box_images = "'.$light_boxes.'";
	</script>'; 
} ?>