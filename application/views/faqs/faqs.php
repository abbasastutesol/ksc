

    <section class="">

        <div class="container">

            <div class="cartHeading fontWhtReg">

                <h1 class=""><?php echo $faqs_label; ?></h1>

                <div class="clearfix"></div>

            </div>

            <div class="faqs_Sec">

            <?php foreach ($faqs as $faq) {  ?>

                <div class="qusAnsBox">

                    <div class="qusBox">

                        <div class="listing"><?php echo $faqs_question_label; ?></div>

                        <?php echo $faq[$lang.'_question']; ?>

                    </div>

                    <div class="ansBox">

                        <div class="listing"><?php echo $faqs_answer_label; ?></div>

                        <?php echo  strip_tags($faq[$lang.'_ans']); ?>

                        <div class="clearfix"></div>

                        <a href="javascript:void(0);" class="faqLess">

                            <img src="<?php echo base_url().'assets/frontend/images/upGolden.png'; ?>" alt="Less" height="43" width="31" />

                        </a>

                    </div>

                </div>



            <?php } ?>

            </div>

        </div>

    </section>