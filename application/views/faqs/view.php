 <!-- Content Wrapper. Contains page content -->
      <div id="page_content">
        <div id="page_content_inner">
            <?php if(viewEditDeleteRights('Faqs','add')) { ?>
		<a href="<?php echo base_url();?>admin/faqs/add" class="md-btn"> Add</a>
            <?php } ?>
			<div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                        	<div class="uk-overflow-container"> 

                                <table class="uk-table uk-table-align-vertical listing dt_default">
                                    <thead>
                                        <tr>
                                             <th>Sr#</th>

                                              <th>English Question</th>
											  <th>Arabic Question</th>
                                              
                                              <th>English Answer</th>
                      
                                              <th>Arabic Answer</th>

                                            <?php if(viewEditDeleteRights('Faqs','edit') || viewEditDeleteRights('Faqs','delete')) { ?>
                                              <th class="nosort">Action</th>
                                            <?php } ?>
                                        </tr>
                                    </thead>
                                    <tbody>
									<?php
									if($faqs)
									{
										$i=0;
										foreach($faqs as $faq)
										{
										?>
											<tr class="<?php echo $faq->id;?>">
													<td><?php echo ++$i;?></td>
													<td><?php echo get_x_words($faq->eng_question, '5').'..';?></td>
													<td><?php echo get_x_words($faq->arb_question,'5').'..';?></td>
                                                    <td><?php echo get_x_words($faq->eng_ans,'5').'..';?></td>
                                                    <td><?php echo get_x_words($faq->arb_ans,'5').'..';?></td>

                                            <?php if(viewEditDeleteRights('Faqs','edit') || viewEditDeleteRights('Faqs','delete')) { ?>
													<td>
                                                        <?php if(viewEditDeleteRights('Faqs','edit')) { ?>
                                                        <a href="<?php echo base_url().'admin/faqs/edit/'.$faq->id;?>" title="Edit Faqs">
													<i class="md-icon material-icons">&#xE254;</i>
													</a>
                                                <?php } ?>
                                            <?php if(viewEditDeleteRights('Faqs','delete')) { ?>
													<a href="javascript:void(0);" class="uk-margin-left delete" onclick="deleteRecord(<?php echo $faq->id;?>,'admin/faqs/action','');" title="Delete Faqs">
                                                        <i class="material-icons md-24 delete">&#xE872;</i>
                                                    </a>
                                                <?php } ?>

                                                    </td>
                                            <?php } ?>

													  
											</tr>
										<?php
										}
										
									}
									
									?>
                                           
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
       <!-- /.content-wrapper --> 