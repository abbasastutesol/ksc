<div id="page_content">
        <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
            <h1 id="product_edit_name">Edit Faqs</h1>
            <span class="uk-text-muted uk-text-upper uk-text-small" id="product_edit_sn"></span>
        </div>
        <div id="page_content_inner">
            <form action="<?php echo base_url(); ?>admin/faqs/action" method="post" onsubmit="return false" class="uk-form-stacked ajax_form" id="product_edit_form">
				<input type="hidden" name="form_type" value="update">				<input type="hidden" name="id" value="<?php echo $faqs->id; ?>">
                <div class="uk-grid uk-grid-medium" data-uk-grid-margin>

                    <div class="uk-width-xLarge-10-10  uk-width-large-10-10">
                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text">
                                    Edit Faqs
                                </h3>
                            </div>
                            <div class="md-card-content large-padding">
                                <div class="uk-grid uk-grid-divider uk-grid-medium" data-uk-grid-margin>
                                	<div class="uk-width-large-1-2">
                                     </div>
                                     <div class="uk-width-large-1-2">
                                        <div class="uk-form-row">
                                            <input type="hidden" class="md-input" id="" name="" value=""/>
                                        </div>
                                     </div>
                                    <div class="uk-width-large-1-2">

                                        <label for="eng_description">English Question</label>
										<div class="uk-form-row">

                                            <textarea class="md-input eng_text" name="eng_question" id="eng_question" cols="30" rows="4"><?php echo $faqs->eng_question; ?></textarea>
                                        </div>

                                    </div>


                                    <div class="uk-width-large-1-2">

                                        <label for="eng_description">English Answer</label>
                                        <div class="uk-form-row">

                                            <textarea class="md-input eng_text" name="eng_ans" id="eng_ans" cols="30" rows="4"><?php echo $faqs->eng_ans; ?></textarea>
                                        </div>

                                    </div>

                                    <div class="uk-width-large-1-2">

                                        <label for="arb_description">Arabic Question</label>
                                        <div class="uk-form-row">

                                            <textarea class="md-input arb_text" name="arb_question" id="arb_question" cols="30" rows="4"><?php echo $faqs->arb_question; ?></textarea>
                                        </div>



                                    </div>

                                    <div class="uk-width-large-1-2">

                                        <label for="arb_description">Arabic Answer</label>
                                        <div class="uk-form-row">

                                            <textarea class="md-input arb_text" name="arb_ans" id="arb_ans" cols="30" rows="4"><?php echo $faqs->arb_ans; ?></textarea>
                                        </div>



                                    </div>


                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </form>

        </div>
    </div>

    <div class="md-fab-wrapper">
        <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">
            <i class="material-icons">&#xE161;</i>
        </a>
    </div>

    <div class="md-fab-wrapper" style="right:95px;">
        <a class="md-fab md-fab-primary" href="<?php echo base_url();?>admin/faqs" id="">
            <i class="material-icons">keyboard_backspace</i>
        </a>
    </div>
