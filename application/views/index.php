<?php //echo $show_room[0]->$lang . '_head_office'; exit; ?>
    <section class="banner-sec scrlFX">
        <div class="container">
            <div id="carousel-1" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <?php
                    if(count($sliders) > 0) {
                        foreach ($sliders as $key => $slider) {
                            if ($slider[$lang . '_image'] == '') {
                                continue;
                            }
                            if ($key == 0) {
                                $class = 'active';
                            } else {
                                $class = '';
                            }

                            ?>

                            <div class="item <?php echo $class; ?>">
                                <img src="<?php echo base_url(); ?>uploads/images/home/<?php echo $slider[$lang . '_image'] ?>"
                                     alt="">
                                <div class="caption-wrap">
                                    <div class="caption">
                                        <h1><?php echo $slider[$lang.'_title']; ?></h1>
                                        <?php echo $slider[$lang.'_desc']; ?>
                                        <div class="banner-readmore">
                                            <a href="<?php echo ($slider[$lang.'_slider_link'] !== '' ? $slider[$lang.'_slider_link'] : 'javascript:void(0);' ); ?>">READ MORE</a></div>
                                    </div>
                                </div>
                            </div>

                        <?php }
                    }?>
                    

                </div>
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <?php foreach ($sliders as $key => $slider) {
                        if($key == 0){
                          $active = 'active';  
                        }else{
                            $active = '';
                        }
                        ?>
                    <li data-target="#carousel-1" data-slide-to="<?php echo $key; ?>" class="<?php echo $active; ?>"></li>
                    
                    <?php } ?>
                </ol>
                <div class="raq-box">
                    <ul>
                        <li><i class="fa fa-arrow-<?php echo ($lang=='eng' ? 'left' : 'right'); ?>" aria-hidden="true"></i> <a href="#." data-toggle="modal" data-target="#Modal_2">REQUEST A QUOTATION</a></li>
                        <li><span class="or">or</span> <a href="#." data-toggle="modal" data-target="#Modal_1">EMAIL US</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="hm-content scrlFX">
        <div class="container">
            <div class="about-sec">
                <div class="about-vid">Video Tour</div>
                <div class="about-left">
                    <div class="hmVideo">
                        <img src="<?php echo base_url(); ?>assets/frontend/images/video-poster.png" alt=""><img src="<?php echo base_url(); ?>assets/frontend/images/play-icon.png" alt="" id="video-btn">
                    </div>
                </div>
                <div class="about-right">
                    <h1><?php echo $company[$lang.'_page_title'];?><span><?php echo ( $lang == 'eng' ? 'OVERVIEW' : 'نظرة عامة'); ?></span></h1>
                    <?php echo $company[$lang.'_short_description'];?>
                    <p><a href="<?php echo base_url();?>CompanyProfile/profile"><?php echo ( $lang == 'eng' ? 'READ MORE' : 'اقرأ أكثر'); ?></a></p>
                </div>
            </div>
        </div>
    </section>
    <section class="hm-pro-sec scrlFX">
        <div class="container">
            <div class="hm-heading-row">
                <h1>PRODUCT <span>RANGE</span></h1>
            </div>
            <div class="product-sec">
		<?php $i = 1;
		 foreach($products as $product) {   
			$prod_image = getProductImages($product['id']); 
			foreach($prod_image as $key => $prod_img){
                                        if($prod_img['is_thumbnail'] == 1){
                                            $thumbnail = $prod_img['thumbnail'];
                                        }
                                    }?>
                <div class="product-box" id="pro-box-<?php echo $i;?>">
                    <div class="product-thumb"><img src="<?php echo base_url(); ?>uploads/images/thumbs/products/<?php echo $thumbnail;?>" alt=""></div>
                    <div class="product-title">
                        <h2><?php echo $product[$lang.'_name'];?><span></span></h2>
                    </div>
                    <div class="product-desc">
                        <h2><?php echo $product[$lang.'_name'];?></span></h2>
						<?php $small_des = substr($product[$lang.'_short_des'], 0, strrpos(substr($product[$lang.'_short_des'], 0, 150), ' ')); echo $small_des;?>
                        <!---<p>Curabitur faucibus, lorem in rhoncus interdum, felis eros porta enim, nec condimentum lorem est ac lacus.</p>-->
                        <div class="prodesc-btn"><a href="<?php echo lang_base_url();?>product">VIEW ALL</a></div>
                    </div>
                </div> <?php $i++;} ?>
               
            </div>
        </div>
    </section>
    <section class="hm-partners scrlFX">
        <div class="container">
            <div class="hm-heading-row">
                <h1>OUR <span>PARTNERS</span></h1>
            </div>
            <div class="partners-wrap client-logos">
                <ul>
				<?php foreach($clients_partners as $client){?>
                    <li><a href="#"><img src="<?php echo base_url(); ?>assets/frontend/images/<?php  echo $client->banner_image;?>" alt=""></a></li>
				<?php } ?>
                
                </ul>
                <div class="more-logos"><a href="<?php echo lang_base_url();?>Client/data">Read More</a></div>
            </div>
        </div>
    </section>
    <section class="hm-showrooms scrlFX">
        <div class="container">
            <div class="hm-heading-row">
                <h1>OUR <span>SHOWROOMS</span></h1>
            </div>
            <div class="showrooms-wrap">
                <div class="showrooms-left"><img src="<?php echo base_url(); ?>assets/frontend/images/<?php echo $show_room['banner_image'];?>" alt=""></div>
                <div class="showrooms-right">
				<?php echo $show_room[$lang . '_head_office']; ?>
                    
                    <div class="view-map"><a href="<?php echo lang_base_url().'showrooms';?>" class="red-btn">VIEW MAP</a></div>
                </div>
            </div>
        </div>
    </section>

<?php
    $video_link = $company['video_link'];
        $html == '';
    if($video_link != "") {
    $link_id = explode( "embed/", $video_link);

    $link_id = $link_id[ 1 ]; ?>
   <?php  $html = '<iframe width="100%" height="100%" src="https://www.youtube.com/embed/'.$link_id.'/5Peo-ivmupE?rel=0&amp;controls=1&amp;showinfo=1&amp;autoplay=1&wmode=opaque&amp;wmode=transparent" frameborder="0" allowfullscreen></iframe>'; ?>
<?php }?>

    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery("#video-btn").click(function(){
                jQuery(".hmVideo").html('<?php echo $html; ?>');
            });
        });
    </script>
