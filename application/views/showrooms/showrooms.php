
<main class="main">
	<div class="container">
		<section class="content-wrap showrooms-sec">
			<div class="page-heading">
				<h1 class="page-title">SHOWROOMS</h1>
			</div>
			<div class="showrooms-wrapper">
				<div class="locations-list">
					<ul>
                        <?php
                        $title = '';
                        $office_address = '';
                        $office_type = '';
                        $showroom_id = 0;

                        foreach($showrooms as $key=> $showroom) {

                            if($key == 0){
                                $showroom_id = $showroom['id'];
                                $title = $showroom[$lang.'_title'];
                                $office_address = $showroom[$lang.'_head_office'];
                                $office_type = $showroom[$lang.'_office_type'];
                                $banner_image = $showroom['banner_image'];
                                $active = 'active';
                            }else{
                                $active = '';
                            }
                            ?>
						<li class="<?php echo $active; ?>"><a href="<?php echo lang_base_url().'showrooms/detail/'.$showroom['id']; ?>"><?php echo $showroom[$lang.'_title']; ?></a></li>

                        <?php } ?>
					</ul>
				</div>
                <div class="showrooms-map" id="map"></div>
				<section class="hm-showrooms scrlFX">
					<div class="showrooms-wrap">
						<div class="showrooms-left"><img src="<?php echo base_url().'assets/frontend/images/'.$banner_image; ?>" alt=""></div>
						<div class="showrooms-right">
							<!--<h1><?php /*echo $title; */?> <span><?php /*echo $office_type; */?></span></h1>-->
                            <?php echo $office_address; ?>
                            <!--<p>BALADIYAH STREET, JEDDAH, KINGDOM OF SAUDI ARABIA. <span>T: <a href="tel:+966 12 1234567">+966 12 1234567</a></span></p>-->
							<!--<div class="view-map"><a href="#" class="red-btn">VIEW MAP</a></div>-->
						</div>
					</div>
				</section>
			</div>
		</section>
	</div>
</main>

<?php
$marker_array = array();
    $locations = getShowroomsLocation($showroom_id);

    foreach ($locations as $location) {
        $marker_array[] = $location['lat_long'];
    }
?>

<script>
    var markericons = [];
    function initialize() {
        var map;
        var bounds = new google.maps.LatLngBounds();
        var mapOptions = {
            mapTypeId: 'roadmap'
        };

        // Display a map on the page
        map = new google.maps.Map(document.getElementById("map"), mapOptions);
        map.setTilt(45);

        // Multiple Markers
        var markers = [
            <?php $i=0;
            foreach($marker_array as $marker_loc) { ?>
            ['', '<?php echo $marker_loc; ?>','http://www.google.com/maps/place/<?php echo $marker_loc; ?>'],
            <?php $i++; } ?>
        ];

        // Display multiple markers on a map
        var infoWindow = new google.maps.InfoWindow(), marker, i;
        // Loop through our array of markers & place each one on the map

        for( i = 0; i < markers.length; i++ ) {
            var partsOfStr = markers[i][1].split(',');
            var position = new google.maps.LatLng(partsOfStr[0],partsOfStr[1]);
            bounds.extend(position);
            marker = new google.maps.Marker({
                position: position,
                map: map,
                icon: base_url+'assets/frontend/images/location-small-icon.png',
                url: markers[i][2]
            });
            // Allow each marker to open location on click
            /*google.maps.event.addListener(marker, 'click', function() {
             alert(markers[i][2]);
             //window.open(marker.url, '_blank');
             });*/
            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function(){

                    for (var j = 0; j < markers.length; j++) {
                        window.open(markers[i][2], '_blank');
                    }

                }

            })(marker, i));
            // Automatically center the map fitting all markers on the screen
            map.fitBounds(bounds);
        }

        // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
        var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
            this.setZoom(11);
            google.maps.event.removeListener(boundsListener);
        });
    }
</script>