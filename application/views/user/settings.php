<section>
	<div class="defaultPageSty">
		<div class="container max960_cont">
			<h1><?php echo $settings_settings;?></h1>
		</div>
		<div class="container max960_cont">
			<div class="settingPgLinks">
				<?php $this->load->view('user/settinglinks'); ?>
			</div>
			<h1><?php echo $settings_personal_information;?></h1>
		</div>
        <?php if($this->session->flashdata('message'))
				{
					echo $this->session->flashdata('message');
					
				}?>
		<form action="<?php echo base_url();?>setting/action" class="ajax_register_user" method="post" onsubmit="return false;" id="setting_form">
        	<input type="hidden" name="form_type" value="update" />
            <input type="hidden" id="settings" value="1" />
            <input type="hidden" id="old_email" name="old_email" value="<?php echo $user->email;?>" />
            <input type="hidden" name="update_id" value="<?php echo $user->id;?>" />
            <input type="hidden" name="lang" value="<?php echo $lang;?>" />
			<div class="line"></div>
			<div class="container max960_cont">
				<div class="row" id="myPersonal">
					<div class="col-md-4 col-sm-4 col-xs-12">
						<label><?php echo $register_full_name;?><span class="redStar">*</span></label>
						<input type="text" name="first_name" id="first_name" data-toggle="tooltip" data-placement="top" title="" value="<?php echo $user->first_name;?>" />
					</div>
					<!--<div class="col-md-4 col-sm-4 col-xs-12">
						<label><?php //echo $register_last_name;?><span class="redStar">*</span></label>
						<input type="text" name="last_name" id="last_name" data-toggle="tooltip" data-placement="top" title="" value="<?php //echo $user->last_name;?>" />
					</div>-->
					
					
					<!--<div class="col-md-4 col-sm-4 col-xs-12 paddingBtm10px">
						<label><?php //echo $register_mobile_number;?><span class="redStar">*</span></label>
						<input type="text" name="mobile_no" id="mobile_no" data-toggle="tooltip" data-placement="top" title="" value="<?php //echo $user->mobile_no;?>" />
                        <input type="hidden" name="mobile_ios2_code" id="mobile_ios2_code" value="<?php //echo $user->mobile_ios2_code;?>" />
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<label><?php //echo $register_phone_number;?></label>
						<input type="text" name="phone_no" id="phone_no" data-toggle="tooltip" data-placement="top" title="" value="<?php //echo $user->phone_no;?>" />
                        <input type="hidden" name="phone_ios2_code" id="phone_ios2_code" value="<?php //echo ($user->phone_ios2_code != '' ? $user->phone_ios2_code :'sa|966');?>" />
					</div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
						<label><?php //echo $register_country;?><span class="redStar">*</span></label>
						<select name="country" id="country" class="countryClass" data-toggle="tooltip" data-placement="top" title="">
                        	<option value=""><?php //echo $register_country;?></option>
                            <?php //echo getCoutries($user->country);?>
                        </select>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<label><?php //echo $register_city;?><span class="redStar">*</span></label>
                        <select name="city" id="city" class="cityClass" data-toggle="tooltip" data-placement="top" title="">
                        	<?php //echo getCity($user->country, $user->city);?>
                        </select>
					</div>-->					               
                    <div class="col-md-4 col-sm-4 col-xs-12">
						<label><?php echo $register_email;?><span class="redStar">*</span></label>
						<input type="text" name="email" id="email" data-toggle="tooltip" data-placement="top" title=""  value="<?php echo $user->email;?>" readonly="readonly"/>
					</div>
                    <div class="clearfix"></div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<label><?php echo $register_password;?><span class="redStar">*</span></label>
						<input type="password" name="password" id="password" data-toggle="tooltip" data-placement="top" title="" value="" />
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<label><?php echo $register_confirm_password;?><span class="redStar">*</span></label>
						<input type="password" name="confirm_password" id="confirm_password" data-toggle="tooltip" data-placement="top" title="" value="" />
					</div>
				</div>
			</div>
            <input type="hidden" name="payment_type" id="payment_type_setting" value="COD" />
			 <!-- Uncomment when apply payment method-->
			<!--<div class="container max960_cont">
				<h1><?php //echo $settings_financial_information;?></h1>
			</div>
			<div class="line"></div>
           <div class="container max960_cont">
				<label><?php //echo $register_choose_payment_method;?></label>
                <?php //foreach($cards as $card){?>
                    <div class="settingPgLinks margin_btm_20px" style="width: 35%;" id="editMailing_<?php //echo $card->id;?>">
                        <p><?php //echo $checkout_card_number;?>: <?php //echo $card->card_number;?></p>
                        <p><?php //echo $chat_name;?>: <?php //echo $card->customer_name;?></p>
                    </div>
                <?php //}?>
                <div class="radioPayMeth addBtnActiveSetting">
                	<p>
                    	<input id="checkbox_1" type="radio" name="order_idsED" value="cc_merchantpage" <?php //echo ($user->payment_type == 'cc_merchantpage' ? 'checked="checked"' : '');?>>
                        <label for="checkbox_1"><span></span><img src="<?php //echo base_url()?>assets/frontend/images/visaMasterImg.png" alt="master Card" id="cc_merchantpage" height="57" width="125" /></label>
                    </p>
                    <p>
                    	<input id="checkbox_2" type="radio" name="order_idsED" value="sadad" <?php //echo ($user->payment_type == 'sadad' ? 'checked="checked"' : '');?>>
                        <label for="checkbox_2"><span></span><img src="<?php //echo base_url()?>assets/frontend/images/sadasImage.png" alt="sadad" id="sadad" height="57" width="97" /></label>
                    </p>
                    <p>
                    	<input id="checkbox_3" type="radio" name="order_idsED" value="COD" <?php //echo ($user->payment_type == 'COD' ? 'checked="checked"' : '');?>>
                        <label for="checkbox_3"><span></span><img src="<?php //echo base_url()?>assets/frontend/images/cashDlivImg.png" alt="Cash on Deliver" id="COD" height="57" width="76" /></label>
                    </p>
                </div>
                
				<p>&nbsp;</p>
                
                   <section class="merchant-page-iframe">
                        <?php
                            /*$merchantPageData = $objFort->getMerchantPageData();
                            $postData = $merchantPageData['params'];
                            $gatewayUrl = $merchantPageData['url'];*/
                        ?>
                        <div class="cc-iframe-display">
                            <div id="div-pf-iframe" style="display:none">
                                <div class="pf-iframe-container">
                                    <div class="pf-iframe" id="pf_iframe_content">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
			
			</div>-->
            
			<!--<div class="container max960_cont">
				<h1><?php //echo $register_addresses;?></h1>
			</div>
			<div class="line"></div>-->
			<!--<div class="container max960_cont" id="editingAddress">
            	<?php foreach($addresses as $address){?>
                    <div class="settingPgLinks margin_btm_20px" id="editMailing_<?php echo $address->id;?>">
                        <p><?php echo $address->street_name.' - '.$address->property_no.' - '.$address->address_city;?>	<span><a href="javascript:void(0);" onclick="editAddress('<?php echo $address->id;?>', true);"><?php echo $shipping_address1_edit_address;?></a></span></p>
                    </div>
                    <input type="hidden" id="compAddress_<?php echo $address->id;?>" value="<?php echo $address->street_name.'|'.$address->property_no.'|'.$address->address_city.'|'.$address->more_details.'|'. $address->address_country.'|'.$address->address_district.'|'.$address->address_po_box.'|'.$address->address_zip_code;?>" />
                    <div class="row" style="display:none;" id="showAddress_<?php echo $address->id;?>">
                    	<div class="col-md-4 col-sm-4 col-xs-12">
                            <label><?php echo $register_country;?></label>
                            <select name="address_country[]" id="address_country_<?php echo $address->id;?>" class="countryClass">
                                <option value=""><?php echo $register_country;?></option>
                                <?php echo getCoutries($address->address_country);?>
                            </select>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <label><?php echo $shipping_address_city;?></label>
                            <select name="address_city[]" id="address_city_<?php echo $address->id;?>" class="cityClass">
                            	<?php echo getCity($address->address_country, $address->address_city);?>
                            </select>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12 mapPopUp">
                            <label><?php echo $shipping_address_district;?></label>
                            <input type="text" name="address_district[]" id="address_district_<?php echo $address->id;?>" value="<?php echo $address->address_district;?>" class="address_district"/>
                            <a href="javascript:void(0);" class="openMap"><?php echo $register_choose_from_map;?></a>
            				<div class="us3" style="display:none"></div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <label><?php echo $shipping_address_street_name;?></label>
                            <input type="text" name="street_name[]" id="street_name_<?php echo $address->id;?>" value="<?php echo $address->street_name;?>"/>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <label><?php echo $shipping_address_property_number;?></label>
                            <input type="text" name="property_no[]" id="property_no_<?php echo $address->id;?>" value="<?php echo $address->property_no;?>" />
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <label><?php echo $shipping_address_zip_code;?></label>
                            <input type="text" name="address_zip_code[]" id="address_zip_code_<?php echo $address->id;?>" value="<?php echo $address->address_zip_code;?>"/>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <label><?php echo $shipping_address_po_box;?></label>
                            <input type="text" name="address_po_box[]" id="address_po_box_<?php echo $address->id;?>" value="<?php echo $address->address_po_box;?>"/>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <label><?php echo $shipping_address_more_details;?></label>
                            <input type="text" name="more_details[]" id="more_details_<?php echo $address->id;?>" value="<?php echo $address->more_details;?>"/>
                        </div>
                       
                       		<span <?php echo ($lang == 'eng' ? 'style="margin: 33px 20px 0 0;float: right;color: #FF0000; cursor:pointer;"' : 'style="margin: 33px 0 0 20px;float: left;color: #FF0000;cursor:pointer;'); ?>><a href="javascript:void(0);" onclick="editAddress('<?php echo $address->id;?>', false);"><?php echo $shipping_address1_cancel_editing;?></a></span>
                    </div>
                <?php }?>
                <div class="row" id="addMoreButtonParent">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 addMoreButton" id="addNewAddress">
						<button type="button" class="btn addButtonTwo">+</button><?php echo $shipping_address1_add_another_address;?>
					</div>
				</div>
			</div>-->
			<p>&nbsp;</p>
			<p>&nbsp;</p>
			<div class="container max960_cont">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 text-left">
						<input type="button" onclick="document.getElementById('setting_form').reset();" class="edBtn gray" value="<?php echo $settings_cancel;?>" />
						<input type="submit" class="edBtn red" value="<?php echo $settings_save_changes;?>" />
					</div>
				</div>
			</div>
		</form>
        <a href="#registerUser" class="fancybox" id="success_pop"></a>
	</div>
</section>
<div class="hide" id="optionTemplateTwo">
    <div class="row">
    	<div class="col-md-4 col-sm-4 col-xs-12">
            <label><?php echo $register_country;?></label>
            <select name="address_country[]" class="countryClass">
                <option value=""><?php echo $register_country;?></option>
                <?php echo getCoutries();?>
            </select>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <label><?php echo $shipping_address_city;?></label>
            <select name="address_city[]" class="cityClass">
                <option value=""><?php echo $register_city;?></option>
            </select>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12 mapPopUp">
            <label><?php echo $shipping_address_district;?></label>
            <input type="text" name="address_district[]" class="address_district" />
            <a href="javascript:void(0);" class="openMap"><?php echo $register_choose_from_map;?></a>
            <div class="us3" style="display:none"></div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <label><?php echo $shipping_address_street_name;?></label>
            <input type="text" name="street_name[]" />
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <label><?php echo $shipping_address_property_number;?></label>
            <input type="text" name="property_no[]" />
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <label><?php echo $shipping_address_zip_code;?></label>
            <input type="text" name="address_zip_code[]"/>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <label><?php echo $shipping_address_po_box;?></label>
            <input type="text" name="address_po_box[]"/>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <label><?php echo $shipping_address_more_details;?></label>
            <input type="text" name="more_details[]" />
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12 addMoreButton" style="margin-top:40px;">
            <button type="button" class="btn deleteAddress">-</button><?php echo $shipping_address1_delete_address;?>
        </div>
    </div>
</div>