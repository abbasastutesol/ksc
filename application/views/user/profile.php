<?php
if($user->id != ''){?>
    <section class="">
        <div class="container">
            <div class="cartHeading fontWhtReg">
                <h1 class=""><?php echo $profile_my_account_label; ?></h1>
                <div class="clearfix"></div>
            </div>
            <div class="profilePage">
                <!-- Don't add top Bottom Padding -->
                <div class="leftMenu haveEqHeight">
                    <ul>
                        <li class="active"><a href="<?php echo lang_base_url().'profile'; ?>"><?php echo $my_profile_label; ?></a></li>
                        <li><a href="<?php echo lang_base_url().'profile/myOrder'; ?>"><?php echo $my_orders_my_orders; ?></a></li>
                    </ul>
                    <a href="<?php echo lang_base_url().'login/logout'; ?>" class="logOut"><?php echo $logout_link; ?></a>
                </div>
                <!-- Don't add top Bottom Padding -->
                <div class="rhtProfSec haveEqHeight">
                    <div class="profDtlSec">
                        <h1><?php echo $profile_biography_label; ?></h1>
                        <div class="sandGrayBox userInfo">
                            <div class="menuBtn">
                                <div class="dropdown">
                                    <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="sprite_ioud ioudSpdropdown"></i>
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                                        <li><a href="<?php echo lang_base_url().'Register/profile/'.$user->id;?>"><?php echo $edit_label; ?></a> </li>
                                        <!--<li><a href="javascript:void(0);">Remove</a> </li>-->
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <p><?php echo $user->first_name." ".$user->last_name; ?></p>
                                    <p><strong><?php echo getCoutryByCode($user->country); ?>, <?php echo getCityById($user->city);?></strong></p>
                                </div>

                                <?php

                                if(checkLevel($loyalty->loyalty_points) == 1){
                                    $loyalty_img = base_url().'assets/frontend/images/silverStar.png';
                                    $profileLoyalty = $my_profile_loyalty_silver;
                                }
                                elseif(checkLevel($loyalty->loyalty_points) == 2){
                                    $loyalty_img = base_url().'assets/frontend/images/golden.PNG';
                                    $profileLoyalty = $my_profile_loyalty_gold;
                                }
                                else{
                                    $loyalty_img = base_url().'assets/frontend/images/platinum.PNG';
                                    $profileLoyalty = $my_profile_loyalty_platinum;
                                }
                                ?>
                                <?php if($loyalty){ ?>
                                <div class="col-md-6 text-right">
                                    <img src="<?php echo $loyalty_img; ?>" alt="Star" height="40" width="42"  class="displayInlineTop" />
                                    <p class="displayInlineTop text-left">
                                        <strong><?php echo $profileLoyalty; ?></strong>
                                        <br />
                                        <?php echo $loyalty->loyalty_points; ?>
                                    </p>
                                </div>
                                <?php } ?>

                            </div>
                            <br />
                            <br />
                            <div class="row">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <p><strong> <?php echo $profile_dob_label; ?></strong></p>
                                    <p><?php echo $user->dob; ?></p>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <p><strong><?php echo $profile_email_label; ?></strong></p>
                                    <p><?php echo $user->email; ?></p>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <p><strong><?php echo $profile_moblie_label; ?></strong></p>
                                    <p class="numberArb"><?php echo $user->mobile_no; ?></p>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <p><strong><?php echo $profile_password_label; ?></strong></p>
                                    <p><a href="javascript:void(0);" data-toggle="modal" data-target="#changePassword"> <?php echo $profile_change_password_label; ?></a></p>
                                </div>
                            </div>
                        </div>
                        <?php if($addresses){ ?>
                        <h1><?php echo $address_label; ?></h1>
                        <?php } ?>
                        <div class="sandGrayBox withAddBox">
                            <div class="addressBoxes">

                                <?php foreach($addresses as $address) { ?>

                                    <div class="AddBox">
                                        <div class="whiteBox">
                                            <h1><?php echo $address['address_title']; ?></h1>
                                            <div class="menuBtn">
                                                <div class="dropdown">
                                                    <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="sprite_ioud ioudSpdropdown"></i>
                                                    </button>
                                                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                        <li><a href="javascript:void(0);" onclick="showEdit('<?php echo $address['id']; ?>','<?php echo $address['address_type']; ?>'); $('#address_type').val('<?php echo $address['address_type']; ?>');"><?php echo $edit_label; ?></a> </li>
                                                        <li><a href="javascript:void(0);" onclick="deleteCartAddress('<?php echo $address['id']; ?>','<?php echo $address['address_type']; ?>', '<?php echo $delete_confirm_general; ?>');"><?php echo $remove_label; ?></a> </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            
                                            <div class="sandGrayBox">
                                                <p><?php echo $address['address_1']; ?></p>
                                                <p><?php echo $address['address_2']; ?></p>
                                                <?php echo getCoutryByCode($address['country']).", ".getCityById($address['city']); ?> <br />
                                                <?php echo $phone_label; ?>: <span class="numberArb"><?php echo $address['phone_no']; ?></span>
                                            </div>

                                        </div>
                                    </div>

                                <?php } ?>
                                <form action="<?php echo lang_base_url().'shopping_cart/checkout'; ?>" method="post" id="address_form">
                                    <input type="hidden" name="address_id" id="address_id_val" value="">
                                    <input type="hidden" name="addressType" id="addressType" value="register_address">
                                </form>

                                <div class="clearfix"></div>
                            </div>
                            <div class="AddMoreAddressBtn">
                                <a href="Javascript:void(0);" class="addAddressReset" id="profile_address" data-toggle="modal" data-target="#addAddress">
                                    <i class="sprite_ioud ioudSpaddplus"></i>
                                    <?php echo $add_new_address_label; ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </section>
	<?php }else {
		if($lang == 'eng'){
		redirect('en/register');
		}else{
		redirect('register');
		}
	}?>

