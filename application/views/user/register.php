<section class="">
    <div class="container">
        <div class="cartHeading">
            <h1 class="pull-left"><?php echo ($user->id == ''?$register_new_account_label:$update_account_label); ?></span></h1>
            <div class="clearfix"></div>
        </div>
        <div class="registerNewUser wBox_w_Shadow">
            
            <form action="<?php echo lang_base_url().'register/action'; ?>" method="post" id="resgisterForm_id" name="resgisterForm" onsubmit="return false;" autocomplete="off">

                <div style="text-align: center; color: red;"><?php echo $this->session->flashdata('message'); ?></div>
                
                <input type="hidden" name="form_type" value="<?php echo ($user->id != ''?"update":"register");?>">
				
                <input type="hidden" id="user_id" name="id" value="<?php echo $user->id;?>">
				
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="col-xs-12">
                            <label><?php echo $register_first_name_label; ?></label>
                            <input type="text" id="fname_id" placeholder="<?php echo $enter_here_palceholder; ?>" name="first_name" value="<?php echo $user->first_name;?>"/>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="col-xs-12">
                            <label><?php echo $register_last_name_label; ?></label>
                            <input type="text" id="lname_id" placeholder="<?php echo $enter_here_palceholder; ?>" name="last_name" value="<?php echo $user->last_name;?>"/>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="col-xs-12">
                            <label><?php echo $register_mobile_number_label; ?></label>
                            <input type="text" id="mobile_id" placeholder="<?php echo $enter_here_palceholder; ?>" name="mobile_no" value="<?php echo ($user->mobile_no != '' ? $user->mobile_no : '+966 ');?>" />
                        </div>
                    </div>
					<?php if($user->id == ''){?>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="col-xs-12">
                            <label><?php echo $register_email_label; ?></label>
                            <input type="text" id="reg_email_id" placeholder="<?php echo $enter_here_palceholder; ?>" name="email" value="<?php echo $user->email;?>" />
                        </div>
                    </div>
					<?php } ?>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="col-xs-12">
                            <label><?php echo $register_country_label; ?></label>
                            <select name="country" id="country" class="reg_country">
                                <option disabled selected><?php echo $select_dropdown; ?></option>
                                <?php echo getCountries_html($user->country);?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="col-xs-12">
                            <label><?php echo $register_city_label; ?></label>
                            <select name="city" id="city" class="reg_city">
                            	<option value=""><?php echo $select_dropdown;?></option>
                                <?php if($user->id != ''){ echo getCity_html($user->country, $user->city);} ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="col-xs-12">
                            <label for="dob_id"><?php echo $register_dob_label; ?></label>				
							<input type="text" id="dob_id" placeholder="<?php echo $select_dropdown; ?>" name="dob" value="<?php echo $user->dob;?>" autocomplete="off" >
                            
                            <!-- fake field are a workaround for chrome autofill getting the wrong field -->
						<input class="hideByShowing" type="text"/>


                        </div>
                    </div>
					<?php if($user->id == ''){?>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="col-xs-12">
                            <label><?php echo $register_password_label; ?></label>
                            <input type="password" id="password_id" placeholder="<?php echo $enter_here_palceholder; ?>" name="password" value="<?php echo $user->password;?>"/>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="col-xs-12">
                            <label><?php echo $register_confirm_password_label; ?></label>
                            <input type="password" id="re_password_id" placeholder="<?php echo $enter_here_palceholder; ?>" name="confirm_password" value="<?php echo $user->password;?>"/>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 checkBox">
                       <div class="row">
                       	 <div class="col-xs-12">
                            <input id="checkbox1" type="checkbox" name="checkbox" value="1" style="display: none;" ><label for="checkbox1"><span>&nbsp;</span></label>
                            <?php echo $register_i_accept_label; ?> <a  href="#termsCond" data-toggle="modal"><?php echo $register_terms_and_conditions_label; ?></a>
                        </div>
                       </div>
                    </div>
					<?php } ?>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="col-xs-12">
                            <button type="submit" id="submit_id" class="btn btn-success"><?php echo ($user-id == ''?$register_btn_label:$update_btn_label); ?></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<div class="modal fade iOudModel" id="termsCond" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel"><?php echo $register_terms_and_conditions_label; ?></h4>
            </div>
            <div class="modal-body">
                <?php echo ($lang == 'eng'?$terms_text->eng_description:$terms_text->arb_description); ?>
            </div>
        </div>
    </div>
</div>