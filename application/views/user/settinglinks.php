<div class="row">
	<div class="col-md-3 col-sm-6 col-xs-6">
		<div class="center-block">
			<h2><?php echo $settings_my_account; ?></h2>
			<ul>
				<li><a href="<?php echo lang_base_url();?>setting#myPersonal"><?php echo $settings_personal_information;?></a></li>
				<li><a href="#updatePasword" class="fancybox"><?php echo $settings_update_password;?></a></li>
			</ul>
		</div>
	</div>
	<div class="col-md-3 col-sm-6 col-xs-6">
		<div class="center-block">
			<h2><?php echo $settings_my_request;?></h2>
			<ul>
				<li><a href="<?php echo lang_base_url();?>product"><?php echo $settings_change_order;?></a></li>
				<li><a href="<?php echo lang_base_url();?>my_orders/returnOrder"><?php echo $settings_return_order;?></a></li>
				<li><a href="<?php echo lang_base_url();?>my_orders/placeFeedback"><?php echo $settings_rate_order;?></a></li>
				<li><a href="<?php echo lang_base_url();?>my_orders/lostItem"><?php echo $settings_report_lost_order;?></a></li>
			</ul>
		</div>
	</div>
	<!--<div class="col-md-3 col-sm-6 col-xs-6">
		<div class="center-block">
			<h2><?php echo $settings_financial_information;?></h2>
			<ul>
				<li><a href="<?php echo lang_base_url();?>setting#myPersonal"><?php echo $settings_add_payment;?></a></li>
			</ul>
		</div>
	</div>-->
	<div class="col-md-3 col-sm-6 col-xs-6">
		<div class="center-block">
			<h2><?php echo $settings_mailing_address;?></h2>
			<ul>
				<li><a href="<?php echo lang_base_url();?>setting#editingAddress"><?php echo $settings_edit_mailing_address;?></a></li>
				<li><a href="<?php echo lang_base_url();?>setting#addNewAddress" class="addButtonTwo"><?php echo $settings_add_new_address;?></a></li>
			</ul>
		</div>
	</div>
</div>