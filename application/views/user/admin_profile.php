<div id="page_content">
  <div id="page_content_inner">
    <div class="md-card">
      <div class="md-card-content">
        <h3 class="heading_a">Edit a Category</h3>
        <form action="<?php echo base_url(); ?>admin/index/action" method="post" onsubmit="return false" class="ajax_form">
		<div class="uk-grid" data-uk-grid-margin>
		
		<input type="hidden" name="form_type" value="update">
		<input type="hidden" name="id" value="<?php echo $this->session->userdata['user']['id'];?>"> 
          <div class="uk-width-medium-1-1">
            <div class="uk-form-row">
              <label>Name</label>
              <input type="text" class="md-input" value="<?php echo $user->name;?>" name="name" />
            </div>
			
          </div>
          <div class="uk-width-medium-1-1">
            <div class="uk-form-row">
              <label>Email</label>
              <input type="text" class="md-input" value="<?php echo $user->email;?>" name="email" />
            </div>
            
          </div>
		  
		  <div class="uk-width-medium-1-1">
            
            <div class="uk-form-row">
              <label>Password</label>
              <input type="text" class="md-input" value="" name="password" />
            </div>
          </div>
          
          
		  <div class="uk-width-medium-1-1">
            
            <div class="uk-form-row">
              <label>Confirm Password</label>
              <input type="text" class="md-input" value="" name="confirm_password" />
            </div>
          </div>
          
          <div class="uk-width-medium-1-1">
            
            <div class="uk-form-row">
              <label>Phone</label>
              <input type="text" class="md-input" value="<?php echo $user->phone;?>" name="phone" />
            </div>
          </div>
          <div class="uk-width-medium-1-1">
            
            <div class="uk-form-row">
              <label>Designation</label>
              <input type="text" class="md-input" value="<?php echo $user->designation;?>" name="designation" />
            </div>
          </div>
          
		  <div class="uk-width-medium-1-1">
            
            <div class="uk-form-row">
              <label>Profile Image</label>
               <img src="<?php echo base_url().'uploads/images/user/'.($user->image_name != '' ? $user->image_name : 'no_imge.png'); ?>" alt="Profile Image" data-uk-modal="{target:'#modal_lightbox'}" width="100">
            </div>
          </div>
		  
			
			  <div class="uk-width-large-1-1">
                    <h3 class="heading_a">
                        Upload Profile Image
                       
                    </h3>
                    <div class="uk-grid">
                        <div class="uk-width-1-1">
                            <div id="file_upload-drop" class="uk-file-upload">
                              	<a class="uk-form-file md-btn">choose file<input id="file_upload-select" class="images_upload" type="file" name="image"></a>
                            </div>
                            <div id="file_upload-progressbar" class="uk-progress uk-hidden">
                                <div class="uk-progress-bar" style="width:0">0%</div>
                            </div>
                        </div>
                    </div>
                </div>
			
			<div class="uk-width-large-1-1">
           
              <span class="uk-input-group-addon"><a class="md-btn submit_ajax_form" href="javascript:void(0);">Update</a></span> 
          </div>
	
        </div>
       </form> 
      </div>
    </div>
   
   <!-- light box for image -->
		   <div class="uk-modal" id="modal_lightbox">
                                <div class="uk-modal-dialog uk-modal-dialog-lightbox">
                                    <button type="button" class="uk-modal-close uk-close uk-close-alt"></button>
                                    <img src="<?php echo base_url().'uploads/images/user/'.$category->image_name; ?>" alt=""/>
                                    
                                </div>
           </div>
			<!-- end light box for image -->   
    
    
    
  </div>
</div>