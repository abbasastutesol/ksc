<section class="">
    <div class="container">
        <div class="cartHeading">
            <h1 class="pull-left"><?php echo $reset_password; ?><?php echo $reset_password_label; ?></span></h1>
            <div class="clearfix"></div>
        </div>
        <div class="registerNewUser wBox_w_Shadow">
            
            <form action="<?php echo base_url().'login/reset_password_request'; ?>" method="post" id="reset_password" class="reset_password">
            	<input type="hidden" name="reset_token" value="<?php echo $reset_token;?>"/>

                <div style="text-align: center; color: red;"><?php echo $this->session->flashdata('message'); ?></div>
                              
                <div class="row">

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="col-xs-12">
                            <label><?php echo $register_password_label; ?></label>
                            <input type="password" id="password_id" placeholder="<?php echo $enter_here_palceholder; ?>" name="password" value=""/>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="col-xs-12">
                            <label><?php echo $register_confirm_password_label; ?></label>
                            <input type="password" id="re_password_id" placeholder="<?php echo $enter_here_palceholder; ?>" name="confirm_password" value=""/>
                        </div>
                    </div>
                   
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="col-xs-12">
                            <button type="submit" id="submit_id" class="btn btn-success"><?php echo $reset_password_label; ?></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
