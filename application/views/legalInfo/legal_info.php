
<main class="main">
	<div class="container">
		<section class="content-wrap legalinfo-wrap">
            <div class="page-heading">
                <h1 class="page-title"><?php echo $legal_info[0][$lang.'_title'];?></h1>
            </div>
            <div class="legalinfo-content">
				<h1><?php echo $legal_info[0][$lang.'_title'];?></h1>
				
				<?php echo $legal_info[0][$lang.'_description'];?>
            </div>
		</section>
	</div>
</main>
