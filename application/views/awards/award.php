<main class="main">
	<div class="container">
		<section class="content-wrap">
			<div class="left-sidebar">
			
                <ul>
                    
                    <?php
					$class = '';
                    $getMenu = getMenu('footer','',1);
                    foreach($getMenu as $getMe){
                        $active = activeMenuClass($_SERVER['REQUEST_URI'],$getMe[$lang.'_link'],$lang);

                        ?>

                    <li class="<?php echo $active; ?>"><a href="<?php echo $getMe[$lang.'_link']?>"><?php echo $getMe[$lang.'_menu_title']; ?></a></li>

                    <?php } ?>
                </ul>
				
			</div>
			<div class="right-content">
				<h1>Awards</h1>
				<ul class="awards-list">
				<?php $i = 0;
				foreach($awards as $award) {?>
					<li><a href="#." data-toggle="modal" data-target="#Modal_<?php echo $i;?>"><span class="year"><?php echo $award['award_year']; ?></span><img src="<?php echo base_url();?>assets/frontend/images/<?php echo $award['award_image'];?>" alt="" width="198" height="248"></a></li> <?php $i++; } ?>
			
				</ul>
			</div>
		</section>
	</div>
</main>
<?php $i = 0; 
foreach($awards as $award) {?>
<div id="Modal_<?php echo $i;?>" class="modal fade awards-lightbox" role="dialog">

    <div class="modal-dialog">        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="modal-body"><img src="<?php echo base_url();?>assets/frontend/images/<?php echo $award['award_image'];?>" alt="Image"></div>
        </div>
    </div>
</div>
<?php $i++; }?>