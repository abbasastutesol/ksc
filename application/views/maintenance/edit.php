
    <div id="page_content">
        <div id="page_content_inner">
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-1">
                    <div class="md-card">
                        <form action="<?php echo base_url(); ?>/admin/maintenanceMode/action" method="post" class="ajax_form" onsubmit="return false;">
                        	<input type="hidden" name="form_type" value="update"  />
                            <input type="hidden" name="id" value="<?php echo $maintenance->id; ?>" />
                            <div class="md-card-content">
                                <h3 class="heading_a">Maintenance Mode</h3>
                                <div class="uk-grid" data-uk-grid-margin>


                                    <div class="uk-form-row uk-width-1-2">
                                        <div class="md-input-wrapper md-input-filled">
                                            <label>English Title</label>
                                            <input type="text" class="md-input" name="eng_title" value="<?php echo $maintenance->eng_title; ?>">
                                            <span class="md-input-bar "></span>
                                        </div>
                                    </div>

                                    <div class="uk-form-row uk-width-1-2" style="margin-top: 0;">
                                        <div class="md-input-wrapper md-input-filled">
                                            <label>Arabic Title</label>
                                            <input type="text" class="md-input" name="arb_title" value="<?php echo $maintenance->arb_title; ?>">
                                            <span class="md-input-bar "></span>
                                        </div>
                                    </div>
                                </div>
                                
                                    <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-form-row uk-width-1-2">
                                        <div class="md-input-wrapper md-input-filled">
                                            <label>English Content</label>
                                            <textarea cols="30" rows="8" class="md-input no_autosize" name="eng_content"><?php echo $maintenance->eng_content; ?></textarea>
                                            <span class="md-input-bar "></span>
                                        </div>
                                    </div>
                                    <div class="uk-form-row uk-width-1-2" style="margin-top: 0;">
                                        <div class="md-input-wrapper md-input-filled">
                                            <label>Arabic Content</label>
                                            <textarea cols="30" rows="8" class="md-input no_autosize" name="arb_content"><?php echo $maintenance->arb_content; ?></textarea>
<span class="md-input-bar "></span>
                                        </div>
                                    </div>
                                    <?php
                                    if ($maintenance->active == 'on')
                                    {
                                        $maintenanceChecked = 'checked';
                                    }else{
                                        $maintenanceChecked = '';
                                    } ?>
                                    <div class="uk-form-row uk-width-1-1">
                                        <div class="uk-grid" data-uk-grid-margin>
                                            <div class="uk-width-medium-1-2 uk-row-first">
                                                <input type="checkbox" data-switchery data-switchery-color="#d32f2f" name="active" value="on" data-switchery-size="large" id="maintenance_mode" <?php echo $maintenanceChecked; ?>/>
                                                <label for="maintenance_mode" class="inline-label">Maintenance Mode</label>
                                            </div>
                                        </div>
                                    </div>

                                     <?php if(viewEditDeleteRights('Maintenance Mode','edit')) { ?>
                                        <div class="uk-width-1-1">
                                        <button type="submit" class="md-btn md-btn-primary">Update</button>
                                    </div>
                                        <?php } ?>

                                </div>
                            </div>
                        </form>
                    </div>

                </div>

            </div>
        </div>

    </div>