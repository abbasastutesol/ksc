<div id="page_content">

    <div id="page_content_inner">

        <div class="md-card">

            <div class="md-card-content">

                <h3 class="heading_a">Add Currency</h3><br>

                <form action="<?php echo base_url(); ?>admin/currency/action" method="post" onsubmit="return false" class="ajax_form">

                    <div class="uk-grid" data-uk-grid-margin>



                        <input type="hidden" name="form_type" value="add">

                        <div class="uk-width-medium-1-2">

                            <div class="uk-form-row">

                                <label>English Currency</label>

                                <input type="text" class="md-input" value="" name="eng_currency" />

                            </div>



                        </div>

                        <div class="uk-width-medium-1-2">

                            <div class="uk-form-row">

                                <label>Arabic Currency</label>

                                <input type="text" class="md-input" value="" name="arb_currency" />

                            </div>



                        </div>

                        <div class="uk-width-medium-1-2">

                            <div class="uk-form-row" >

                                <label>Position</label>

                                <select id="position" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}" title="Select with tooltip" name="position">

                                    <option value="A">After</option>

                                    <option value="B">Before</option>

                                </select>

                            </div>

                        </div>



                        <div class="uk-width-medium-1-2">

                            <div class="uk-form-row" style="margin-top: 20px;">

                                <label>Currency Rate(1 SAR equal to)</label>

                                <input type="text" class="md-input" value="" name="rate" />

                            </div>



                        </div>

                        <div class="uk-width-medium-1-2">
                            <input type="checkbox" value="1" data-switchery data-switchery-size="large" checked id="default_currency" name="default_currency" />
                            <label for="default_currency" class="inline-label">Default</label>

                        </div>

                    </div>

                </form>

            </div>

        </div>



    </div>

</div>

<div class="md-fab-wrapper">

    <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">

        <i class="material-icons">&#xE161;</i>

    </a>

</div>