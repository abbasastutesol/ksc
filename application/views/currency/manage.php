 <!-- Content Wrapper. Contains page content -->
      <div id="page_content">
        <div id="page_content_inner">
            <?php if(viewEditDeleteRights('Currency','add')) {  ?>
		    <a href="<?php echo base_url();?>admin/currency/add" class="md-btn" > Add</a>
            <?php } ?>
			<div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <div class="uk-overflow-container"> 
                                <table class="uk-table uk-table-align-vertical listing dt_default">
                                    <thead>
                                        <tr>
                                             <th>Sr#</th>

                                              <th class="nosort">English Currency</th>
                                              <th class="nosort">Arabic Currency</th>

                                              <th>Position</th>
                                              <th>Currency Rate</th>
                                            <?php if(viewEditDeleteRights('Currency','edit') || viewEditDeleteRights('Currency','delete')) {  ?>
                                              <th class="nosort">Action</th>
                                            <?php } ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $count = 1;
                                    foreach($currencies as $currency){ ?>
                                       <tr>
                                           <td><?php echo $count; ?></td>
                                           <td><?php echo $currency->eng_currency; ?></td>
                                           <td><?php echo $currency->arb_currency; ?></td>
                                           <td><?php echo ($currency->position == 'A' ? 'After' : 'Before'); ?></td>
                                           <td><?php echo $currency->rate; ?></td>
                                           <?php if(viewEditDeleteRights('Currency','edit') || viewEditDeleteRights('Currency','delete')) {  ?>
                                           <td>
                                               <?php if(viewEditDeleteRights('Currency','edit') ) {  ?>
                                               <a href="<?php echo base_url().'admin/currency/edit/'.$currency->id;?>" title="Edit Currency">

                                                   <i class="md-icon material-icons">&#xE254;</i>

                                               </a>
                                               <?php } ?>
                                               <?php if(viewEditDeleteRights('Currency','delete')) {  ?>

                                               <a href="javascript:void(0);" class="uk-margin-left delete" onclick="deleteRecord(<?php echo $currency->id;?>,'admin/branches/action','');" title="Delete branches">
                                                   <i class="material-icons md-24 delete">&#xE872;</i>
                                               </a>
                                               <?php } ?>

                                           </td>
                                           <?php } ?>

                                       </tr>
                                    <?php $count++;
                                    } ?>
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
       <!-- /.content-wrapper --> 