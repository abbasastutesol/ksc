<div id="page_content">
    <div id="page_content_inner">
        <div class="md-card">
            <div class="md-card-content"><h3 class="heading_a">Edit Currency</h3><br>
                <form action="<?php echo base_url(); ?>admin/currency/action" method="post" onsubmit="return false"
                      class="ajax_form">
                    <div class="uk-grid" data-uk-grid-margin><input type="hidden" name="form_type" value="update">
                        <input type="hidden" name="id" value="<?php echo $currency->id; ?>">
                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row"><label>English Currency</label> <input type="text" class="md-input"
                                                                                            value="<?php echo $currency->eng_currency; ?>"
                                                                                            name="eng_currency"/></div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row"><label>Arabic Currency</label> <input type="text" class="md-input"
                                                                                           value="<?php echo $currency->arb_currency; ?>"
                                                                                           name="arb_currency"/></div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row"><label>Position</label> <select id="position" data-md-selectize
                                                                                     data-md-selectize-bottom
                                                                                     data-uk-tooltip="{pos:'top'}"
                                                                                     title="Select with tooltip"
                                                                                     name="position">
                                    <option <?php if ($currency->position == 'A') { ?> selected="selected" <?php } ?>
                                            value="A">After
                                    </option>
                                    <option <?php if ($currency->position == 'B') { ?> selected="selected" <?php } ?>
                                            value="B">Before
                                    </option>
                                </select></div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row" style="margin-top: 20px;"><label>Currency Rate(1 SAR equal to)</label> <input
                                        type="text" class="md-input" value="<?php echo $currency->rate; ?>"
                                        name="rate"/></div>
                        </div>
                        <div class="uk-width-medium-1-2"><input type="checkbox" value="1" data-switchery
                                                                data-switchery-size="large" <?php echo($currency->default_currency == 1 ? 'checked' : ''); ?>
                                                                id="default_currency" name="default_currency"/> <label
                                    for="default_currency" class="inline-label">Default</label></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="md-fab-wrapper"><a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id=""> <i
                class="material-icons">&#xE161;</i> </a></div>
<div class="md-fab-wrapper" style="right:95px;"><a class="md-fab md-fab-primary"
                                                   href="<?php echo base_url(); ?>admin/currency" id=""> <i
                class="material-icons">keyboard_backspace</i> </a></div>