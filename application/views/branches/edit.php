<div id="page_content">
    <div id="page_content_inner">
        <form action="<?php echo base_url(); ?>admin/branches/action" method="post" onsubmit="return false" class="ajax_form" enctype="multipart/form-data">
            <div class="md-card">
                <div class="md-card-content">
                    <h3 class="heading_a">Edit Branch</h3><br>
                    <div class="uk-grid" data-uk-grid-margin>

                        <input type="hidden" name="form_type" value="update">
                        <input type="hidden" name="id" value="<?php echo $branch->id; ?>">

                        <div class="uk-width-medium-1-2">
                            <label>English Name</label>
                            <div class="uk-form-row">

                                <input class="md-input" value="<?php echo $branch->eng_name; ?>" id="eng_name" name="eng_name" />
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <label>Arabic Name</label>
                            <div class="uk-form-row">

                                <input class="md-input" value="<?php echo $branch->arb_name; ?>" id="arb_name" name="arb_name" />
                            </div>
                        </div>


                        <div class="uk-width-medium-1-2">
                            <label>English Description</label>
                            <div class="uk-form-row">

                                <textarea cols="30" rows="4" class="md-input eng_text" id="eng_description" name="eng_description"><?php echo $branch->eng_description; ?></textarea>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <label>Arabic description</label>
                            <div class="uk-form-row">

                                <textarea cols="30" rows="4" class="md-input arb_text" id="arb_description" name="arb_description"><?php echo $branch->arb_description; ?></textarea>
                            </div>
                        </div>

                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row">
                                <label>Phone</label>
                                <input type="text" class="md-input" value="<?php echo $branch->phone; ?>" name="phone" />
                            </div>

                        </div>

                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row">
                                <label>Fax</label>
                                <input type="text" class="md-input" value="<?php echo $branch->fax; ?>" name="fax" />
                            </div>

                        </div>

                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row">
                                <label>Email</label>
                                <input type="text" class="md-input" value="<?php echo $branch->email; ?>" name="email" />
                            </div>

                        </div>
                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row">
                                <label>Lat Long</label>
                                <input type="text" class="md-input" value="<?php echo $branch->lat_long; ?>" name="lat_long" />
                            </div>

                        </div>



                    </div>

                </div>
            </div>


        </form>


        <div class="md-fab-wrapper">
            <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">
                <i class="material-icons">&#xE161;</i>
            </a>
        </div>

        <div class="md-fab-wrapper" style="right:95px;">
            <a class="md-fab md-fab-primary" href="<?php echo base_url();?>admin/branches" id="">
                <i class="material-icons">keyboard_backspace</i>
            </a>
        </div>



    </div>
</div>