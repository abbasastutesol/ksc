<!-- Content Wrapper. Contains page content -->
<div id="page_content">
    <div id="page_content_inner">
        <?php if(viewEditDeleteRights('Branches','add')) { ?>
        <a href="<?php echo base_url(); ?>admin/branches/add" class="md-btn"> Add</a>
        <?php } ?>
        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-1-1">
                        <div class="uk-overflow-container">
                            <table class="uk-table uk-table-align-vertical listing dt_default">
                                <thead>
                                <tr>
                                    <th>Sr#</th>
                                    <th>English Name</th>
                                    <th>Arabic Name</th>
                                    <th>Phone</th>
                                    <th>Fax</th>
                                    <th>Email</th>
                                    <?php if(viewEditDeleteRights('Branches','edit') || viewEditDeleteRights('Branches','delete')) { ?>
                                    <th class="nosort">Action</th>
                                    <?php } ?>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if ($branches) {
                                    $i = 0;
                                    foreach ($branches as $branch) { ?>
                                        <tr class="<?php echo $branch->id; ?>">
                                            <td><?php echo ++$i; ?></td>
                                            <td><?php echo $branch->eng_name; ?></td>
                                            <td><?php echo $branch->arb_name; ?></td>
                                            <td><?php echo $branch->phone; ?></td>
                                            <td><?php echo $branch->fax; ?></td>
                                            <td><?php echo $branch->email; ?></td>

                                        <?php if(viewEditDeleteRights('Branches','edit') || viewEditDeleteRights('Branches','delete')) { ?>
                                            <td>
                                            <?php if(viewEditDeleteRights('Branches','edit') ) { ?>
                                                <a href="<?php echo base_url() . 'admin/branches/edit/' . $branch->id; ?>"
                                                   title="Edit Branches"> <i class="md-icon material-icons">&#xE254;</i>
                                                </a>
                                                <?php } ?>
                                            <?php if(viewEditDeleteRights('Branches','delete')) { ?>
                                                <a href="javascript:void(0);" class="uk-margin-left delete"
                                                        onclick="deleteRecord(<?php echo $branch->id; ?>,'admin/branches/action','');"
                                                        title="Delete branches"> <i class="material-icons md-24 delete">
                                                        &#xE872;</i>
                                                </a>
                                                <?php } ?>

                                            </td>
                                            <?php } ?>

                                        </tr>

                                    <?php }
                                } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>       <!-- /.content-wrapper -->