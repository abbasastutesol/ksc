<section class="">

        <div class="container">

            <div class="cartHeading fontWhtReg">

                <h1 class=""><?php echo $location_page_label; ?></h1>

                <div class="clearfix"></div>

            </div>

            <div class="locationSec">

                <div class="mapSec">

                    <div id="map" style="height: 450px;"></div>

                </div>

                <div class="LocAddSec">

                    <ul>



                     <?php

                     $i = 1;

                     foreach($locations as $location) { ?>



                        <li>

                            <div class="whiteBox haveEqHeight">

                                <h1 class="makeShadow"><?php echo str_pad($i, 2, '0', STR_PAD_LEFT); ; ?></h1>

                                <a href="javascript:void(0);" class="branchLocation" data-lat_long="<?php echo $location['lat_long']; ?>" data-locid="<?php echo $i; ?>"><h2><?php echo $location[$lang.'_name'] ?></h2></a>

                                <?php echo get_x_words($location[$lang.'_description'],12); ?>

                                <br/>

								<?php if($location['phone'] != ''){?>
                                <p><strong>T: </strong> <bdi><?php echo $location['phone']; ?></bdi></p>
								<?php } if($location['fax'] != ''){ ?>
                                <p><strong>F: </strong> <bdi><?php echo $location['fax']; ?></bdi></p>
								<?php } if($location['email'] != ''){?>
                                <p><strong>E: </strong> <?php echo $location['email']; ?></p>
								<?php } ?>
                            </div>

                        </li>

                     <?php $i++;

                     } ?>



                    </ul>

                    <div class="clearfix"></div>

                </div>

            </div>

        </div>

    </section>
    
    
    <script type="text/javascript">
    // When the window has finished loading create our google map below
    google.maps.event.addDomListener(window, 'load', init);

    var locations = [
    <?php
        $locations = getAllBranchesLocations();
        foreach ($locations as $location) {
    ?>
        ['<?php echo $location[$lang."_name"]; ?>', <?php echo $location["lat_long"]; ?>],

    <?php } ?>
    ];


    function init() {

		var markers = new Array();
		
		
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 7,
            center: new google.maps.LatLng(24.774265,46.738586),
            styles: mapStyle,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var infowindow = new google.maps.InfoWindow();
		

        var marker, i;

        for (i = 0, index = 1; i < locations.length; i++,index++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map,
                //label: '0'+index,
				/*label: {
					text: '0'+index,
					color: '#000'
				},*/
                icon: pinSymbol("#000")
            });
			
			
            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));
            
			//display location on map on clicking the each branch box.
			marker.locid = index;
			markers[markers.length] = marker;
			
			$(document).on("click",".branchLocation",function() {
				var thisloc = $(this).data("locid");
				for(var i=0; i<markers.length; i++) {
					if(markers[i].locid == thisloc) {
						
						map.panTo(markers[i].getPosition());
						map.setZoom(9);
						infowindow.setContent(locations[i][0]);
						infowindow.open(map, markers[i]);
					}
				}

				  $("html, body").animate({ scrollTop: 70 }, "fast");
			}); // end this script..
        }
    }

    // this function to make custom marker
    function pinSymbol(color) {
        return {
            path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z M -2,-30 a 2,2 0 1,1 4,0 2,2 0 1,1 -4,0',
            fillColor: color,
            fillOpacity: 1,
            strokeColor: '#FFF',
            strokeWeight: 2,
            scale: 1.5
        };
    }

</script>