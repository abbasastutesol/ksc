 <!-- Content Wrapper. Contains page content -->
      <div id="page_content">
        <div id="page_content_inner">
			<div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <div class="uk-overflow-container"> 
                                <table class="uk-table uk-table-align-vertical listing dt_default">
                                    <thead>
                                        <tr>
                                             <th>Sr#</th>

                                              <th class="nosort">Name</th>
                      
                                              <th class="nosort"> Email</th>
                                              
                                              <th class="nosort"> Mobile No</th>
                      
                                              <th class="nosort">Action</th>
                                              
                                        </tr>
                                    </thead>
                                    <tbody>
                                       	<?php 
											$i=1;
											foreach($reviews as $review)
											{
												$mobile_no = $review->mobile_no;
												if($review->mobile_ios2_code != '')
												{
													$mobile_code = explode('|', $review->mobile_ios2_code);
													$mobile_no = '+'.$mobile_code[1].' '.$review->mobile_no;
												}
												?>
												<tr id="<?php echo $review->id;?>">
													<td class="uk-text-center"><?php echo $i;?></td>
													<td><?php echo $review->name;?></td>
                                                    <td><?php echo $review->email;?></td>
                                                    <td><?php echo $mobile_no;?></td>
													<td><a href="<?php echo base_url().'admin/review/view/'.$review->id;?>" title="View Designer"><i class="md-icon material-icons">visibility</i></a><a href="javascript:void(0);" class="uk-margin-left delete" onclick="deleteRecordAll('<?php echo $review->id;?>','admin/review/action','','delete');" title="Delete Jobs"> <i class="material-icons md-24 delete">&#xE872;</i></a></td>
												</tr>
											<?php 
												$i++;
											}?>   
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
       <!-- /.content-wrapper --> 