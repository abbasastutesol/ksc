<div id="page_content">
  <div id="page_content_inner">
    <div class="md-card">
      <div class="md-card-content">
        <h3 class="heading_a">Designer Detail</h3><br>
        
		<div class="uk-grid" data-uk-grid-margin>
		
          <div class="uk-width-medium-1-1">
            
            <div class="uk-form-row">
              <label><strong>Name: </strong></label>
              <?php echo $review->name?>
            </div>
          </div>
          
          <div class="uk-width-medium-1-1">
            
            <div class="uk-form-row">
              <label><strong>Email: </strong></label>
              <?php echo $review->email?>
            </div>
          </div>
          <?php 
		  	$mobile_no = $review->mobile_no;
			if($review->mobile_ios2_code != '')
			{
				$mobile_code = explode('|', $review->mobile_ios2_code);
				$mobile_no = '+'.$mobile_code[1].' '.$review->mobile_no;
			}
		  ?>
          <div class="uk-width-medium-1-1">
            
            <div class="uk-form-row">
              <label><strong>Mobile No: </strong></label>
              <?php echo $review->mobile_no?>
            </div>
          </div>
          <div class="uk-width-medium-1-1">
            
            <div class="uk-form-row">
              <label><strong>Message: </strong></label>
              <?php echo $review->message?>
            </div>
          </div>
        </div>
       
      </div>
    </div>
      
  </div>
</div>

<div class="md-fab-wrapper" style="right:95px;">
        <a class="md-fab md-fab-primary" href="<?php echo base_url();?>admin/review" id="">
            <i class="material-icons">keyboard_backspace</i>
        </a>
    </div>