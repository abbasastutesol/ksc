<section>
	<div class="defaultPageSty">
		<div class="container">
			<h1><?php echo $login_give_us_review;?> </h1>
		</div>
		<div class="line"></div>
		<form action="<?php echo base_url();?>giveUsReview/submit" method="post" onsubmit="return false;" id="review_form" class="submit_review" enctype="multipart/form-data">
			<div class="container max960_cont">
				<div class="row">
					<div class="col-md-4 col-sm-4 col-xs-12">
						<label><?php echo $jobs_send_your_resume_name;?><span class="redStar">*</span></label>
						<input type="text" name="name" id="name" data-toggle="tooltip" data-placement="top" title="" />
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<label><?php echo $chat_email;?><span class="redStar">*</span></label>
						<input type="text" name="email" id="email" data-toggle="tooltip" data-placement="top" title="" />
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<label><?php echo $register_mobile_number;?><span class="redStar">*</span></label>
						<input type="text" name="mobile_no" value="" id="mobile_no" data-toggle="tooltip" data-placement="top" title="" />
                        <input type="hidden" name="mobile_ios2_code" id="mobile_ios2_code" value="sa|966" />
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<label><?php echo $chat_message;?><span class="redStar">*</span></label>
						<textarea name="message" id="message" data-toggle="tooltip" data-placement="top" title=""></textarea>
					</div>
					<div class="col-md-8 col-sm-8 col-xs-12 design_with_usBTN">
						<label>&nbsp;</label>
						<input class="edBtn gray" type="button" onclick="document.getElementById('review_form').reset();" value="<?php echo $settings_cancel;?>">
						<input type="submit" class="edBtn red" value="<?php echo $register_send;?>">
					</div>
				</div>
				<p>&nbsp;</p>
				<p>&nbsp;</p>
			</div>
		</form>
	</div>
</section>