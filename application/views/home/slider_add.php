<div id="page_content">

  <div id="page_content_inner">

    <form action="<?php echo base_url(); ?>admin/home/action" method="post" onsubmit="return false" class="ajax_form">

    <div class="md-card">

      <div class="md-card-content">

        <h3 class="heading_a">Add Slider</h3><br>

		<div class="uk-grid" data-uk-grid-margin>

		

		<input type="hidden" name="form_type" value="save_slider">

		

          <div class="uk-width-medium-1-2">



              <label>English Title</label>

              <div class="uk-form-row">

              <input type="text" class="md-input" name="eng_title">

              </div>





              <label>English Description</label>

            <div class="uk-form-row">

              

              <textarea cols="30" rows="4" id="eng_desc" class="md-input eng_text" name="eng_desc"></textarea>

            </div>
           <!-- <br>
            
           <label>Slider for Home Page</label>

               <div class="uk-width-medium-1-2">

                   <input type="checkbox" id="home_page_slider" data-switchery="true" data-switchery-size="large" checked="" id="active" name="is_home" value="1" checked>

               </div> -->
			   

          </div>

           <div class="uk-width-medium-1-2">



               <label>Arabic Title</label>

               <div class="uk-form-row">

                   <input type="text" class="md-input" name="arb_title" >

               </div>



            <label>Arabic Description</label>

            <div class="uk-form-row">

              

              <textarea cols="30" rows="4" id="arb_desc" class="md-input arb_text" name="arb_desc"></textarea>

            </div>
			
			<br>
			<label>Active</label>

              <div class="uk-form-row">
              
              <input type="checkbox" id="is_active" data-switchery="true" data-switchery-size="large" id="is_active" name="is_active" value="1" checked>
              

              </div>
           <!--<label>Slider for Promotion Page</label>

               <div class="uk-width-medium-1-2">

                   <input type="checkbox" id="promotion_page_slider" data-switchery="true" data-switchery-size="large" id="active" name="is_promotion" value="1" >

               </div>-->
            
            

          </div>

			<div class="uk-width-medium-1-2">

            
			<label>English Slider Link</label>

              <div class="uk-form-row">

              <input type="text" class="md-input" name="eng_slider_link">

              </div>


          </div>
          
          
		  

			

			  <div class="uk-width-large-1-1">

                    <h3 class="heading_a">

                        Upload English Slider Image

                       

                    </h3>

                    <span style="color:red">(1402 * 688 pixels)</span>

                    <div class="uk-grid">

                        <div class="uk-width-1-1">

                            <div id="file_upload-drop" class="uk-file-upload">

                              	<a class="uk-form-file md-btn">choose file<input id="file_upload-select" class="images_upload" type="file" name="eng_slider_image"></a>

                            </div>

                            <div id="file_upload-progressbar" class="uk-progress uk-hidden">

                                <div class="uk-progress-bar" style="width:0">0%</div>

                            </div> 

                        </div>

                    </div>

                </div>
          <div class="uk-width-medium-1-2">

            
			<label>Arabic Slider Link</label>

              <div class="uk-form-row">

              <input type="text" class="md-input" name="arb_slider_link">

              </div>


          </div>
                
                <div class="uk-width-large-1-1">

                    <h3 class="heading_a">

                        Upload Arabic Slider Image

                       

                    </h3>

                    <span style="color:red">(1402 * 688 pixels)</span>

                    <div class="uk-grid">

                        <div class="uk-width-1-1">

                            <div id="file_upload-drop" class="uk-file-upload">

                              	<a class="uk-form-file md-btn">choose file<input id="file_upload-select" class="images_upload" type="file" name="arb_slider_image"></a>

                            </div>

                            <div id="file_upload-progressbar" class="uk-progress uk-hidden">

                                <div class="uk-progress-bar" style="width:0">0%</div>

                            </div> 

                        </div>

                    </div>

                </div>

                

          

        </div>

        

      </div>

    </div>

    



  

   </form>

   

   

   <div class="md-fab-wrapper">

        <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">

            <i class="material-icons">&#xE161;</i>

        </a>

    </div>



      <div class="md-fab-wrapper" style="right:95px;">

          <a class="md-fab md-fab-primary" href="<?php echo base_url();?>admin/home/slider" id="">

              <i class="material-icons">keyboard_backspace</i>

          </a>

      </div>

    

    

  </div>

</div>