<div id="page_content">   
 <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">   
 <h1 id="product_edit_name">Edit Home</h1>      
 <span class="uk-text-muted uk-text-upper uk-text-small" id="product_edit_sn"></span>   
 </div>   
 <div id="page_content_inner"> 
 <form action="<?php echo base_url(); ?>admin/home/action" method="post" onsubmit="return false" class="ajax_form" enctype="multipart/form-data">      
 <input type="hidden" name="form_type" value="update_content">      
 <input type="hidden" name="id" value="<?php echo $home_content['id'];?>">    
 <div class="uk-grid uk-grid-medium" data-uk-grid-margin>          
 <div class="uk-width-xLarge-10-10  uk-width-large-10-10">  
 <div class="md-card">                        
 <div class="md-card-toolbar">                  
 <h3 class="md-card-toolbar-heading-text">  
 Home page more detail                     
 </h3>                        
 </div>          
 <div class="md-card-content large-padding">     
 <div class="md-card">      
 <div class="uk-width-1-2" style="float:left; margin:0;">  
 <label for="eng_more_title">English Discover More title </label>   
 <div class="uk-form-row">                          
 <input class="md-input" name="eng_more_title" id="eng_more_title" value="<?php echo $home_content['eng_more_title']; ?>"/>
 </div>
 <label for="eng_more_discover">English Discover More Detail </label>  
 <div class="uk-form-row">                
 <textarea class="md-input eng_text" name="eng_more_discover" id="eng_more_discover" cols="30" rows="4"><?php echo $home_content['eng_more_discover']; ?></textarea>       
 </div>               
 </div>                  
 <div class="uk-width-1-2" style="float:right; margin:0;">          
 <label for="arb_more_title">Arabic Discover More title </label>      
 <div class="uk-form-row">          
 <input class="md-input" name="arb_more_title" id="arb_more_title" value="<?php echo $home_content['arb_more_title']; ?>" />   
 </div>                    
 <label for="arb_washing_instruction">Arabic Discover More Detail</label>   
 <div class="uk-form-row">                           
 <textarea class="md-input arb_text" name="arb_more_discover" id="arb_more_discover" cols="30" rows="4"><?php echo $home_content['arb_more_discover']; ?></textarea>    
 </div>             
 </div>              
 <div class="uk-width-large-1-1">  
 <?php if($home_content['discover_more_image'] != ''){?>                
 <div class="uk-width-medium-1-2">                          
 <div class="uk-form-row">                                 
 <label>Image</label>                                 
 <img style="margin-top: 30px; margin-bottom: 30px;" src="<?php echo base_url().'uploads/images/home/'.$home_content['discover_more_image']; ?>" alt="Slider Image" data-uk-modal="{target:'#modal_lightbox'}" width="100">   
 </div>                
 </div>                     
 <?php }?>              
 <h3 class="heading_a">    
 Upload Image            
 <p style="color:red; font-size:12px">(1016 x 608 pixels)</p>             
 </h3>        
 <div class="uk-grid">         
 <div class="uk-width-1-1">      
 <div id="file_upload-drop" class="uk-file-upload">   
 <a class="uk-form-file md-btn">choose file<input id="file_upload-select" type="file" class="images_upload" name="discover_more_image"></a>     
 </div>                      
 <div id="file_upload-progressbar" class="uk-progress uk-hidden">         
 <div class="uk-progress-bar" style="width:0">0%</div>    
 </div>                      
 </div>               
 </div>                  
 </div>                
 <label for="more_video">Video Link </label>    
 <div class="uk-form-row">       
 <input class="md-input" value="<?php echo $home_content['more_video']; ?>" name="more_video" id="more_video" />        
 </div></div>             
 </div>         
 </div>             
 </div>          
 </div>      
 </form>  
 </div></div><?php if(viewEditDeleteRights('Home','edit')) { ?><div class="md-fab-wrapper">
 <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">     
 <i class="material-icons">&#xE161;</i>  
 </a></div><?php } ?><!-- light box for image --><div class="uk-modal" id="modal_lightbox"> 
 <div class="uk-modal-dialog uk-modal-dialog-lightbox">      
 <button type="button" class="uk-modal-close uk-close uk-close-alt"></button>  
 <img src="<?php echo base_url().'uploads/images/home/'.$home_content['discover_more_image']; ?>" alt=""/>  
 </div></div><!-- end light box for image -->