 <!-- Content Wrapper. Contains page content -->

      <div id="page_content">

        <div id="page_content_inner">
            <?php if(viewEditDeleteRights('Sliders','add')) { ?>
		    <a href="<?php echo base_url();?>admin/home/slider_add" class="md-btn"> Add</a>
            <?php } ?>

			<div class="md-card">

                <div class="md-card-content">

                    <div class="uk-grid" data-uk-grid-margin>

                        <div class="uk-width-1-1">

                        	<div class="uk-overflow-container"> 



                                <table class="uk-table uk-table-align-vertical listing dt_default_slider ">

                                    <thead>

                                        <tr>

                                             <!--<th>Sr#</th>-->



                                              <th>Slider</th>



                                              <th>Slider Description</th>

                      

                                              <th>Created At</th>

                                            <?php if(viewEditDeleteRights('Sliders','edit') || viewEditDeleteRights('Sliders','delete')) { ?>

                                              <th class="nosort">Action</th>
                                            <?php } ?>

                                        </tr>

                                    </thead>

                                    <tbody>

									<?php

									if($sliders)

									{

										$i=0;

										foreach($sliders as $slider)

										{

										?>

											<tr class="<?php echo $slider->id;?>" id="<?php echo $slider->id;?>">

													<!--<td class="drag_row"><?php /*echo ++$i;*/?></td>-->
                                                    
                                                    <td class="drag_row"><?php echo $slider->eng_title;?></td>

													<td><?php echo substr($slider->eng_desc,0,20).'...';?></td>

                                                    <td><?php echo $slider->created_at;?></td>


                                            <?php if(viewEditDeleteRights('Sliders','edit') || viewEditDeleteRights('Sliders','delete')) { ?>
													<td>


                                                    <?php if(viewEditDeleteRights('Sliders','edit')) { ?>
                                                        <a href="<?php echo base_url().'admin/home/slider_edit/'.$slider->id;?>" title="Edit Branches">

													<i class="md-icon material-icons">&#xE254;</i>

													</a>
                                                <?php } ?>
                                            <?php if(viewEditDeleteRights('Sliders','delete')) { ?>
													<a href="javascript:void(0);" class="uk-margin-left delete" onclick="deleteRecord(<?php echo $slider->id;?>,'admin/home/action','');" title="Delete Slider">
                                                        <i class="material-icons md-24 delete">&#xE872;</i>
                                                    </a>
                                                <?php } ?>

                                                    </td>
                                            <?php } ?>


													  

											</tr>

										<?php

										}

										

									}

									

									?>

                                           

                                    </tbody>

                                </table>

                            </div>

                            

                        </div>

                    </div>

                </div>

            </div>



        </div>

    </div>

       <!-- /.content-wrapper -->





 <!-- light box for image -->

 <div class="uk-modal" id="modal_lightbox">

     <div class="uk-modal-dialog uk-modal-dialog-lightbox">

         <button type="button" class="uk-modal-close uk-close uk-close-alt"></button>

         <img src="<?php echo base_url().'uploads/images/home/'.($slider->images != '' ? $slider->images : 'no_imge.png'); ?>" alt=""/>



     </div>

 </div>

 <!-- end light box for image -->

 <style>
     .drag_row{
         cursor: move; /* fallback if grab cursor is unsupported */
         cursor: grab;
         cursor: -moz-grab;
         cursor: -webkit-grab;
     }
 </style>