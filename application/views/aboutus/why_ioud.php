
    <section class="aboutUs">
        <div class="container">
            <div class="bannerImg">
                <img src="<?php echo base_url().'uploads/images/aboutus/'.$why_ioud['banner_image']; ?>" alt="Banner" width="1402" height="373">
            </div>
            <div class="textPageSty">
                <!-- Don't add top Bottom Padding -->
                <?php $this->load->view('layouts/about_us_left_sidebar'); ?>
                <!-- Don't add top Bottom Padding -->
                <div class="rhtTextDetails">
                    <h1 class="makeShadow"><?php echo $why_ioud[$lang.'_title']; ?></h1>
                    <?php echo $why_ioud[$lang.'_description']; ?>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </section>