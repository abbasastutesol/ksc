<div id="page_content">
   <div id="page_content_inner">
      <form action="<?php echo base_url(); ?>admin/legal_info/action" method="post" onsubmit="return false" class="ajax_form">
         <div class="md-card">
            <div class="md-card-content">
               <h3 class="heading_a">Legal Information</h3>
               <br>		
               <div class="uk-grid" data-uk-grid-margin>
                  <input type="hidden" name="form_type" value="update">		<input type="hidden" name="id" value="<?php echo $record[0]->id;?>">		        <input type="hidden" name="tpl_name" value="legal_info">
				  <input type="hidden" name="page_id" value="<?php echo $record[0]->page_id;?>">	
                  <div class="uk-width-medium-1-2">
                     <label>English Title</label>              
                     <div class="uk-form-row">              <input type="text" class="md-input" name="eng_title" value="<?php echo $record[0]->eng_title;?>">              </div>
                     <label>English Description</label>            
                     <div class="uk-form-row">                            <textarea cols="30" rows="4" id="eng_description" class="md-input eng_text" name="eng_description"><?php echo $record[0]->eng_description;?></textarea>            </div>
                  </div>
                  <div class="uk-width-medium-1-2">
                     <label>Arabic Title</label>               
                     <div class="uk-form-row">                   <input type="text" class="md-input" name="arb_title" value="<?php echo $record[0]->arb_title;?>" >               </div>
                     <label>Arabic Description</label>            
                     <div class="uk-form-row">                            <textarea cols="30" rows="4" id="arb_description" class="md-input arb_text" name="arb_description"><?php echo $record[0]->arb_description;?></textarea>            </div>
                  </div>
				   
				  
                  
               </div>
            </div>
         </div>
         <div class="md-card">
            <div class="md-card-toolbar">
               <h3 class="md-card-toolbar-heading-text">              Keyword          </h3>
            </div>
             
            <div class="md-card-content large-padding">
               <div class="uk-grid uk-grid-divider uk-grid-medium" data-uk-grid-margin>
                  <div class="uk-width-medium-1-2">
                     <div class="uk-form-row">                    <label>English Meta Title</label>                    <input type="text" class="md-input" value="<?php echo $record[0]->eng_meta_title;?>" name="eng_meta_title" />                  </div>
                  </div>
                  <div class="uk-width-medium-1-2">
                     <div class="uk-form-row">                    <label>Arabic Meta Title</label>                    <input type="text" class="md-input" value="<?php echo $record[0]->arb_meta_title;?>" name="arb_meta_title" />                  </div>
                  </div>
                  <div class="uk-width-medium-1-2">
                     <div class="uk-form-row">                    <label>Englishn Meta Description</label>                    <textarea cols="30" rows="4" class="md-input" name="eng_meta_description"><?php echo $record[0]->eng_meta_description;?></textarea>                  </div>
                  </div>
                  <div class="uk-width-medium-1-2">
                     <div class="uk-form-row">                    <label>Arabic Meta Description</label>                    <textarea cols="30" rows="4" class="md-input" name="arb_meta_description"><?php echo $record[0]->arb_meta_description;?></textarea>                  </div>
                  </div>
                  <div class="uk-width-medium-1-2">
                     <div class="uk-form-row">                    <label>Englishn Meta Keyword</label>                    <textarea cols="30" rows="4" class="md-input" name="eng_meta_keyword"><?php echo $record[0]->eng_meta_keyword;?></textarea>                  </div>
                  </div>
                  <div class="uk-width-medium-1-2">
                     <div class="uk-form-row">                    <label>Arabic Meta Keyword</label>                    <textarea cols="30" rows="4" class="md-input" name="arb_meta_keyword"><?php echo $record[0]->arb_meta_keyword;?></textarea>                  </div>
                  </div>
               </div>
            </div>
         </div>
      </form>
      <?php //if(viewEditDeleteRights('About Us','edit')) { ?>   
      <div class="md-fab-wrapper">        <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">            <i class="material-icons">&#xE161;</i>        </a>    </div>
      <?php //} ?><!-- light box for image -->
      
      <!-- end light box for image -->                 
   </div>
</div>
