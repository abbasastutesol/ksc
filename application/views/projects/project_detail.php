<main class="main">
	<div class="container">
		<section class="content-wrap projects-dtl-sec">
			<div class="page-heading">
				<h1 class="page-title">Projects</h1>
			</div>
			<div class="projects-dtl-wrap">
				<div class="project-lrg-img">
					<div id="projectBannerCarousel" class="carousel slide" data-ride="carousel">
						<!-- Wrapper for slides -->
						<div class="carousel-inner">
						<?php  $i = 1;
						
						$images = explode(',',$project_detail[0]['banner_image']); 
						foreach($images as $img){ 
						if($i == 1)
						{$class = "active";}else {$class = "";}?>
							<div class="item <?php echo $class;?>">
								<img src="<?php echo base_url();?>assets/frontend/images/<?php echo $img;?>" alt="">
							</div>
							
						<?php $i++;} ?>
						</div>
						<!-- Left and right controls -->
						<a class="project-left carousel-control" href="#projectBannerCarousel" data-slide="prev"><img src="<?php echo base_url();?>assets/frontend/images/project-prev-arrow.png" alt=""></a>
						<a class="project-right carousel-control" href="#projectBannerCarousel" data-slide="next"><img src="<?php echo base_url();?>assets/frontend/images/project-next-arrow.png" alt=""></a>
					</div>
				</div>
				<?php echo $project_detail[0][$lang.'_des']; ?>
				<div class="project-gal">
					<ul>
					<?php
					
						foreach($project_detail as $gall_img){ ?>
						<li><a href="#." data-toggle="modal" data-target="#Modal_3"><img src="<?php echo base_url();?>assets/frontend/images/<?php echo $gall_img['gallery_image'];?>" alt=""></a></li><?php } ?>
						
					</ul>
				</div>
				</div>
		</section>
	</div>
</main>
<div id="Modal_3" class="modal fade project-carousel" role="dialog">
    <div class="modal-dialog">        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="modal-body">
                <div id="projectCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
					<?php  $i = 1;
						foreach($project_detail as $gall_img){ if($i == 1)
						{$class = "active";}else {$class = "";}?>
                        <div class="item <?php echo $class;?>"><img src="<?php echo base_url(); ?>assets/frontend/images/<?php echo $gall_img['gallery_image'];?>" alt="">
                           <div class="caption"><?php echo $gall_img[$lang.'_description'];?></div>
                        </div> <?php $i++;} ?>
                        
                    </div>                    <!-- Left and right controls --> <a class="project-left carousel-control"
                                                                                  href="#projectCarousel"
                                                                                  data-slide="prev"><img
                                src="<?php echo base_url(); ?>assets/frontend/images/project-prev-arrow.png" alt=""></a> <a
                            class="project-right carousel-control" href="#projectCarousel" data-slide="next"><img
                                src="<?php echo base_url(); ?>assets/frontend/images/project-next-arrow.png" alt=""></a></div>
            </div>
        </div>
    </div>
</div>