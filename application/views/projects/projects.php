
<main class="main">
	<div class="container">
		<section class="content-wrap projects-sec">
			<div class="page-heading">
				<h1 class="page-title">Projects</h1>
			</div>
			<div class="projects-wrap">
			<?php foreach($projects as $project){  $arr = explode(',',$project['banner_image']);?>
				<div class="project-col">
					<a href="<?php echo lang_base_url();?>Projects/detail_project/<?php echo $project['id'];?>"><img src="<?php echo base_url();?>assets/frontend/images/<?php echo $arr[0];?>" alt=""></a>
					<h1><a href="<?php echo lang_base_url();?>Projects/get_Project_gallery/<?php echo $project['id'];?>"><?php $project[$lang.'_title']?></a></h1>
				</div>
				<?php } ?>
				<!---<div class="project-col">
					<a href="project-detail.php"><img src="<?php echo base_url();?>assets/frontend/images/project-img-2.jpg" alt=""></a>
					<h1><a href="project-detail.php">MADINA AIRPORT</a></h1>
				</div>
				<div class="project-col">
					<a href="project-detail.php"><img src="<?php echo base_url();?>assets/frontend/images/project-img-3.jpg" alt=""></a>
					<h1><a href="project-detail.php">MAKKAH (HARAM EXPANSION)</a></h1>
				</div>
				<div class="project-col">
					<a href="project-detail.php"><img src="<?php echo base_url();?>assets/frontend/images/project-img-4.jpg" alt=""></a>
					<h1><a href="project-detail.php">KINGDOM TOWER</a></h1>
				</div>--->
			</div>
		</section>
	</div>
</main>
