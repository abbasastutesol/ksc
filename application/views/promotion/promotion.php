<section class="productPage">

        <div class="container">

            <div class="productPHeading">

                <div class="row">

                    <div class="col-sm-12">

                        <h1><?php echo $all_promotions_label; ?></h1>

                    </div>

                </div>

            </div>





            <div class="bannerImg">

                <img src="<?php echo base_url().'uploads/images/promotion/'.$promotion_slider->banner_image; ?>" alt="Banner" height="" width="" />

            </div>

            <div class="offersSec">

                <div class="offProList">

                    <ul>



                      <?php foreach ($promotions as $promotion) {

                          $prom_image =  getProductImages($promotion['id']);
						  
						  foreach($prom_image as $key => $prom_img){
								if($prom_img['is_thumbnail'] == 1){
									$thumbnail = $prom_img['thumbnail'];
								}
							}

                          ?>

                        <li>

                            <div class="whiteBox">

                                <a href="<?php echo lang_base_url().'product/product_page/'.$promotion['id']; ?>">

                                <div class="imgBox"><img src="<?php echo base_url(); ?>uploads/images/thumbs/products/<?php echo $thumbnail; ?>" alt="Offer" height="192" width="212" /></div>

                                </a>

                                <a href="<?php echo lang_base_url().'product/product_page/'.$promotion['id']; ?>">



                                <h3><?php echo $promotion[$lang.'_name']; ?></h3>

                                </a>

                                    <?php echo substr($promotion[$lang . '_description'],0,100).'...'; ?>

                                <h4>
                                    <?php

                                    $mMode = maintenanceMode('');
                                    if($mMode != ''){
                                        $onClick = 'maintenanceMode();';
                                    }else{
                                        $onClick = 'addToCart('.$promotion["id"].');';
                                    }

                                    $price = getPriceByOffer($promotion['id']);
                                    $calculated_rate = currencyRatesCalculate($price);
                                    echo $calculated_rate['rate']; ?> <?php echo $calculated_rate['unit']; ?></h4>

                                <input type="hidden" class="selQty" name="selQty" value="1">

                                <a href="javascript:void(0);" onclick="<?php echo $onClick; ?>">

                                    <button class="btn btn-success" type="button">

                                        <i class="sprite_ioud ioudSppluscart"></i>

                                    </button>

                                </a>

                            </div>

                        </li>

                      <?php } ?>



                    </ul>

                    <div class="clearfix"></div>

                </div>

            </div>

        </div>

    </section>