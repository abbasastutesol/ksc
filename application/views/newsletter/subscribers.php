 <!-- Content Wrapper. Contains page content -->

<div id="page_content">

  <div id="page_content_inner">
<?php if($result) { ?>
  <a href="<?php echo base_url();?>admin/newsletter/exportExcel" target="_blank" class="md-btn"><img src="<?php echo base_url();?>assets/admin/assets/images/excelIcon.png" alt="excel" /></a>
  <?php } ?>

      <div class="md-card">

          <div class="md-card-content">

              <div class="uk-grid" data-uk-grid-margin>

                  <div class="uk-width-1-1">

                      <div class="uk-overflow-container"> 

                          <table class="uk-table uk-table-align-vertical listing dt_default">

                              <thead>

                                  <tr>

                                       <th>Sr#</th>

                                       <th class="nosort">Email</th>
                                       <th class="nosort">Subscription Date</th>
                                      <?php if(viewEditDeleteRights('Newsletter Subscribers','delete')) { ?>
                                       <th class="nosort">Action</th>
                                      <?php } ?>

                                  </tr>

                              </thead>

                              <tbody>

                              <?php 

                              $i=1;

                              foreach($result as $res)

                              {?>

                                  <tr id="<?php echo $i;?>">

                                      <td class="uk-text-center"><?php echo $i;?></td>

                                      <td><?php echo $res['email'];?></td>
                                      <td><?php echo date('d M Y h:i a',strtotime($res['timestamp']));?></td>
                                      <?php if(viewEditDeleteRights('Newsletter Subscribers','delete')) { ?>
                                      <td>
                                          <a href="javascript:void(0);" class="uk-margin-left delete" onclick="deleteRecordByString('<?php echo $res['email'];?>','admin/newsletter/action','', <?php echo $i;?>);" title="Delete Subscriber">
                                              <i class="material-icons md-24 delete"></i>
                                          </a>
                                      </td>
                                      <?php } ?>

                                  </tr>

                              <?php 

                                  $i++;

                              }?>

                              </tbody>

                          </table>

                      </div>

                      

                  </div>

              </div>

          </div>

      </div>



  </div>

</div>

       <!-- /.content-wrapper --> 