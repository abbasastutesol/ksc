<!-- Content Wrapper. Contains page content -->
<div id="page_content">
    <div id="page_content_inner">

        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-1-1">
                        <div class="uk-overflow-container">
                            <table class="uk-table uk-table-align-vertical listing dt_default">
                                <thead>
                                <tr>
                                    <th>Order Id</th>
                                    <th class="nosort"> User Name</th>
                                    <th class="nosort"> Email</th>
                                    <!--<th class="nosort">Order Date</th>-->
                                    <!--<th class="nosort">Product Rating</th>
                                    <th class="nosort">Packaging Rating</th>                                              <th class="nosort">Packing Rating</th>                                              <th class="nosort">Service Rating</th>                                              <th class="nosort">Shipment Rating</th>                                              <th class="nosort">Satisfaction Rating</th>-->
                                    <th class="nosort">Feedback Comment</th>
                                    <?php if(viewEditDeleteRights('Order Feedback','view')) { ?>
                                    <th class="nosort">Detail</th>
                                    <?php } ?>
                                </tr>
                                </thead>
                                <tbody>
                            <?php $index = 1;
                            foreach ($feedback as $feed) {
                                $user = getUserById($feed->user_id);
                                if ($user) { ?>
                                    <tr>
                                        <td><?php echo str_pad($feed->order_id, 6, "0", STR_PAD_LEFT); ?></td>
                                        <td>                                                     <?php echo $user['first_name'] . " " . $user['last_name']; ?>                                                 </td>
                                        <td><?php echo $user['email']; ?></td>
                                        <!--<td><?php /*echo $feed->product_rating; */ ?></td>                                                 <td><?php /*echo $feed->packaging_rating; */ ?></td>                                                 <td><?php /*echo $feed->packing_rating; */ ?></td>                                                 <td><?php /*echo $feed->service_rating; */ ?></td>                                                 <td><?php /*echo $feed->shipment_rating; */ ?></td>                                                 <td><?php /*echo $feed->satisfaction_rating; */ ?></td>-->
                                        <td><?php echo substr($feed->feedback, '0', '30'); ?></td>

                                    <?php if(viewEditDeleteRights('Order Feedback','view')) { ?>
                                        <td>
                                            <a href="<?php echo base_url(); ?>admin/orders/feedBackDetails/<?php echo $feed->id; ?>"><i
                                                        title="Order Detail" class="material-icons md-icon dp48">description</i></a>
                                            <!--<a href="<?php /*echo base_url(); */ ?>admin/product/edit/<?php /*echo $order->product_id; */ ?>"><i                                                                 title="Product Detail"                                                                 class="material-icons md-icon dp48">redeem</i></a>                                                     <a href="<?php /*echo base_url(); */ ?>admin/orders/feedBackDetails/<?php /*echo $order->id; */ ?>"><i                                                                 title="Feedback Detail"                                                                 class="material-icons md-icon dp48">chat</i>                                                     </a>-->
                                        </td>
                                        <?php } ?>
                                    </tr>
                                <?php }
                            } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>       <!-- /.content-wrapper -->