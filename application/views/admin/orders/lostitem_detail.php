<div id="page_content">
        <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
            <h1 id="product_edit_name">Edit a product</h1>
            <span class="uk-text-muted uk-text-upper uk-text-small" id="product_edit_sn"></span>
        </div>
        <div id="page_content_inner">
                <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
                    
                    <div class="uk-width-xLarge-10-10  uk-width-large-10-10">
                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text">
                                    Detail
                                </h3>
                            </div>
                            <div class="md-card-content large-padding">
                                <div class="uk-grid uk-grid-divider uk-grid-medium" data-uk-grid-margin>
                                    <div class="uk-width-large-1-1">
                                        <div class="uk-form-row">
                                            <label for="eng_name">Product Name</label>
                                            <input type="text" class="md-input" value="<?php echo $lostitem->eng_name;?>" readonly="readonly"/>
                                        </div>
                                        <div class="uk-form-row">
                                            <label for="eng_name">Name</label>
                                            <input type="text" class="md-input" value="<?php echo $lostitem->first_name.' '.$lostitem->last_name;?>" readonly="readonly"/>
                                        </div>
                                        <div class="uk-form-row">
                                            <label for="eng_name">Email</label>
                                            <input type="text" class="md-input" value="<?php echo $lostitem->email;?>" readonly="readonly"/>
                                        </div>
                                        <div class="uk-form-row">
                                            <label for="eng_name">Order Date</label>
                                            <input type="text" class="md-input" value="<?php echo date('d/m/Y',strtotime($lostitem->order_date));?>" readonly="readonly"/>
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text">
                                    Question Answers
                                </h3>
                            </div>
                            <div class="md-card-content large-padding">
                                <div class="uk-grid uk-grid-divider uk-grid-medium" data-uk-grid-margin>
                                    <div class="uk-width-large-1-1">
                                    <?php foreach($lostitemAns as $ans){?>
                                        <div class="uk-form-row">
                                            <label><?php echo $ans->eng_question;?></label>
                                            <input type="text" class="md-input" value="<?php echo $ans->eng_answer;?>" readonly="readonly"/>
                                            
                                        </div>                                 
                                    <?php }?>  
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

        </div>
    </div>
	<div class="md-fab-wrapper" style="right:95px;">
        <a class="md-fab md-fab-primary" href="<?php echo base_url();?>admin/lostItems" id="">
            <i class="material-icons">keyboard_backspace</i>
        </a>
    </div>