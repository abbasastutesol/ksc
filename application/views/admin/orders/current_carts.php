<!-- Content Wrapper. Contains page content -->
      <div id="page_content">
        <div id="page_content_inner">
         <!--<a href="<?php echo base_url();?>admin/orders/exportExcelOrder" target="_blank" class="md-btn"><img src="<?php //echo base_url();?>assets/admin/assets/images/excelIcon.png" alt="excel" /></a>-->
			<div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <div class="uk-overflow-container"> 
                                <table class="uk-table uk-table-align-vertical listing dt_default">
                                    <thead>
                                        <tr>
                                             <th>Sr#</th>
                      
                                              <th class="nosort"> Name</th>
                                              
                                              <th class="nosort"> No of Products</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
									 $index = 1;
									 foreach($carts as $cart) {?>
               							<tr>
                                             <td><?php echo $index;?></td>
                                            
                                              <td><?php echo ($cart['guest'] == 0 ? $cart['first_name'].' '.$cart['last_name'] : 'Guest'); ?></td>
                      
                                              <td><?php echo $cart['no_of_prod']; ?></td>
                                              
                                           
                                        </tr>
                                     <?php $index++; }?>
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
       <!-- /.content-wrapper --> 