 <!-- Content Wrapper. Contains page content -->
      <div id="page_content">
        <div id="page_content_inner">
         <a href="<?php echo base_url();?>admin/orders/exportExcelOrder" target="_blank" class="md-btn"><img src="<?php echo base_url();?>assets/admin/assets/images/excelIcon.png" alt="excel" /></a>
			<div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <div class="uk-overflow-container"> 
                                <table class="uk-table uk-table-align-vertical listing dt_default">
                                    <thead>
                                        <tr>
                                             <th>Sr#</th>

                                              <th>Order Id</th>
                      
                                              <th class="nosort"> Name</th>
                                              
                                              <th class="nosort"> Email</th>
                                              
                                              <th class="nosort"> Address</th>
                                              
                                              <th class="nosort"> Order Status</th>
                                              
                                              <th class="nosort">Order Detail</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
									 $index = 1;
									 foreach($orders as $order){?>
               							<tr>
                                             <td><?php echo $index;?></td>
                                             
                                             <td><?php echo str_pad($order->id, 10, "0", STR_PAD_LEFT);?></td>

                                              <td><?php echo $order->first_name.' '.$order->last_name;?></td>
                      
                                              <td><?php echo $order->email;?></td>
                                              
                                              <td><?php echo $order->property_no.' '.$order->streat_name.' '.$order->address_city;?></td>
                                              <td><div class="uk-form-row">
                                                    <select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}" title="Select with tooltip" name="status" onchange="changeOrderStatus('<?php echo $order->id;?>', this.value);">
                                                        <option <?php echo ($order->order_status == '0' ? 'selected' : '');?> value="0">Cancelled</option>
                                                        <option <?php echo ($order->order_status == '1' ? 'selected' : '');?> value="1">Pending</option>
                                                        <option <?php echo ($order->order_status == '2' ? 'selected' : '');?> value="2">Completed</option>
                                                    </select>
										</div></td>
                                              <td><a href="<?php echo base_url();?>admin/orders/order_details/<?php echo $order->id;?>"><i title="Detail" class="material-icons md-icon dp48">description</i></a></td>
                                        </tr>
                                     <?php }?>
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
       <!-- /.content-wrapper --> 