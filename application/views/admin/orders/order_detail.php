<?php if(!$order) { redirect(base_url('admin/orders')); } ?>

<body class=" sidebar_main_open sidebar_main_swipe">
<!-- main header -->


    <?php

    if($order->order_status == 1){
        $statusClass = 'red';
        $order_status = 'Prepare Order';
        $circle_1 = "";
        $circle_2 = "black";
        $circle_3 = "black";
        $circle_4 = "black";

    }elseif($order->order_status == 2){
        $statusClass = 'blue';
        $order_status = 'Verify Payment';
        $circle_1 = "";
        $circle_2 = "";
        $circle_3 = "black";
        $circle_4 = "black";

    }elseif($order->order_status == 3){
        $statusClass = 'green';
        $order_status = 'Preparing Shipment';
        $circle_1 = "";
        $circle_2 = "";
        $circle_3 = "";
        $circle_4 = "black";

    }elseif($order->order_status == 4){
        $statusClass = 'black';
        $order_status = 'Shipped';
        $circle_1 = "";
        $circle_2 = "";
        $circle_3 = "";
        $circle_4 = "";

    }
    elseif($order->order_status == 0){
        $statusClass = 'orange';
        $order_status = 'Cancelled ';
        $circle_1 = "black";
        $circle_2 = "black";
        $circle_3 = "black";
        $circle_4 = "black";
    }
    elseif($order->order_status == 8){
        $statusClass = 'orange';
        $order_status = 'Returned';
        $circle_1 = "black";
        $circle_2 = "black";
        $circle_3 = "black";
        $circle_4 = "black";
    }
    elseif($order->order_status == 9){
        $statusClass = 'orange';
        $order_status = 'Returned';
        $circle_1 = "black";
        $circle_2 = "black";
        $circle_3 = "black";
        $circle_4 = "black";
    }
    elseif($order->order_status == 10){
        $statusClass = 'orange';
        $order_status = 'Cancelled ';
        $circle_1 = "black";
        $circle_2 = "black";
        $circle_3 = "black";
        $circle_4 = "black";
    }

    $addressCheck = false;
    $address = getUserAddressByOrder($order->id);
    if ($address) {
        $countryCode = $address->country;
        $cityId = $address->city;
        $addressCheck = true;
    } else {

        $address = getCartAddress($order->address_id);
        if($address){

            $cityId = $address->city;
            $countryCode = $address->country;
            $addressCheck = true;
        }
    }


    $user = getUserById($order->user_id);

    if (!$user) {
        $fullName = $address->full_name;
        $email = $address->email;
        if(!$addressCheck) {
            $countryCode = $address->country;
            $cityId = $address->city;
        }
        $mobile_no = $address->phone_no;
        $userId = $address->user_id;
    } else {
        $fullName = $user['first_name'] . " " . $user['last_name'];
        $email = $user['email'];
        if(!$addressCheck) {
            $countryCode = $user['country'];
            $cityId = $user['city'];
        }
        $mobile_no = $user['mobile_no'];
        $userId = $user['id'];
    
    }
    ?>

<div id="page_content">

    <div id="page_content_inner">
        <div class="md-card">
            <div class="md-card-content orderPage ordDtlPgEd">
                <div class="orderListBox">
                    <div class="singleOrder <?php echo $statusClass; ?>">
                        <div class="orderExpand uk-child"  style="display: block;" >
                            <div class="uk-grid ordSingDtlEdHead" data-uk-grid-margin data-uk-grid-match="{target:'.md-card'}">
                                <div class="uk-width-medium-5-10">
                                    <a href="<?php echo base_url().'admin/orders'; ?>">
                                        <i class="material-icons">keyboard_arrow_left</i>
                                        Go back
                                    </a>
                                </div>
                                <div class="uk-width-medium-5-10">
                                    <ul class="topIcons uk-float-right">

                                        <!--<li><a href="javascript:void(0);"><i class="material-icons">done_all</i></a></li>
                                        <li class="red"><a href="javascript:void(0);"><i class="material-icons">pan_tool</i></a></li>
                                        -->
                                        <li><a href="javascript:void(0);" onClick="window.open('<?php echo base_url(); ?>/en/my_orders/printPage/<?php echo $order->id; ?>')"><i class="material-icons">print</i></a></li>
                                        <li><a href="javascript:void(0);" onClick="deleteRecord(<?php echo $order->id;?>,'admin/orders/action','');"><i class="material-icons">&#xE872;</i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="boxes">
                                <div class="uk-grid uk-grid-small" data-uk-grid-margin data-uk-grid-match="{target:'.md-card'}">
                                    <div class="uk-width-medium-1-3">
                                        <div class="md-card">

                                            <div class="md-card-content">
                                                <h1>Order Information <span><?php echo getOrderProductCount($order->id); ?> Items</span></h1>
                                                <h2>Order # <?php echo str_pad($order->id, 6, "0", STR_PAD_LEFT);?></h2>
                                                <p><strong class="big"><?php echo getCityById($cityId);?>, <?php echo getCoutryByCode($countryCode); ?></strong> </p>
                                                <br />
                                                <h2>Order Date</h2>
                                                <p><strong class="big"><?php echo date('d M y, h:i a', strtotime($order->created_at));?></strong> </p>
                                                <br />
                                                <?php /*?><h2>Delivered on</h2>
                                                <p><strong class="big"><?php echo getShipmentDays($order->shipment_group,$order->shipment_type); ?></strong> </p><?php */?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="uk-width-medium-1-3">
                                        <div class="md-card">
                                            <div class="md-card-content">
                                                <h1>Customer Information</h1>
                                                <h2>Customer Name</h2>
                                                <p><strong class="big"><?php echo $fullName; ?></strong> </p>
                                                <br />
                                                <h2>Email</h2>
                                                <p><strong class="big"><?php echo $email; ?></strong> </p>
                                                <br />
                                                <h2>Mobile</h2>
                                                <p><strong class="big"><?php echo $mobile_no; ?></strong> </p>
                                                <br />
                                                <h2>Customer Type</h2>
                                                <p><strong class="big"><?php echo ($order->user_type == '0' ? 'Guest' : 'Registered');?></strong> </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="uk-width-medium-1-3">
                                        <div class="md-card">
                                            <div class="md-card-content">
                                                <h1>Shipment Information</h1>
                                                <p><strong class="big"><?php echo $fullName; ?></strong></p>
                                                <p>
                                                    <?php
                                                    $print = str_split($address->address_1, 30);
                                                    echo implode("<br>", $print);
                                                    ?>
                                                    <br/>
                                                    <?php if(isset($address->address_title) && $address->address_title != ''){ ?>
                                                <p><?php echo $address->address_title;?></p>
                                                <?php } ?>
                                                <?php echo getCityById($cityId); ?>
                                                , <?php echo getCoutryByCode($countryCode); ?>
                                                </p>
                                                <p>
                                                    <!--T: +966 12 6030111<br>-->
                                                    M: <?php echo $mobile_no; ?><br>
                                                    E: <?php echo $email; ?><br>
                                                    <!--P.O. Box 11412<br>
                                                    Zip code 22411<br>-->
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="uk-grid uk-grid-small" data-uk-grid-margin data-uk-grid-match="{target:'.md-card'}">
                                    <div class="uk-width-medium-1-3">
                                        <div class="md-card">
                                            <div class="md-card-content">
                                                <h1>Shipment Method</h1>
                                                <h6><?php echo ($order->shipment_method == "1" ? "Manual Shipment" : "Aramex"); ?></h6>
                                                <p>
                                                   <?php if($order->shipment_type == 'normal'){ echo "Normal Shipping";}elseif($order->shipment_type == 'fast'){ echo "Fast Shipping";}elseif($order->shipment_type == 'aramex'){ echo 'Aramex Shipping';} else{ echo "Free Shipping";} ?>, Charges + <?php echo number_format($order->shipment_price,'2','.',''); ?> <?php echo $order->currency_unit; ?><br />
                                                    <?php echo getShipmentDays($order->shipment_group,$order->shipment_type); ?>
                                                </p>

                                                <p style="margin-bottom: 0">
                                                    <br />
                                                    <?php if($order->shipment_method == 2){ ?>
                                                    <strong>Tracking ID: <?php echo $order->tracking_id;?></strong>
                                                        <br>
                                                        <br>
                                                        <strong>Aramex Invoice: <a href="javascript:void(0);" onclick="getAramexInvoice('<?php echo $order->id; ?>','<?php echo $order->tracking_id ?>','<?php echo $order->total_amount ?>','<?php echo $order->address_id; ?>')">View</a></strong>
                                                    <?php } ?>
                                                </p>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="uk-width-medium-2-3">
                                        <div class="md-card">
                                            <div class="md-card-content">
                                                <h1>Shipment Status</h1>
                                                <div class="uk-grid uk-grid-small uk-text-center" data-uk-grid-margin data-uk-grid-match="{target:'.md-card'}">
                                                    <div class="ordStaStyle">
                                                        <div class="ordStaStyle uk-row-first">
                                                            <ul>
                                                                <li class="<?php echo $circle_1; ?>">
                                                                    <div class="wrapper">
                                                                        <div class="circle"></div>
                                                                        <p>Preparing Order</p>
                                                                    </div>
                                                                </li>
                                                                <li class="<?php echo $circle_2; ?>">
                                                                    <div class="wrapper">
                                                                        <div class="circle"></div>
                                                                        <p>Verify Payment</p>
                                                                    </div>
                                                                </li>
                                                                <li class="<?php echo $circle_3; ?>">
                                                                    <div class="wrapper">
                                                                        <div class="circle"></div>
                                                                        <p>Preparing Shipment</p>
                                                                    </div>
                                                                </li>
                                                                <li class="<?php echo $circle_4; ?>">
                                                                    <div class="wrapper">
                                                                        <div class="circle"></div>
                                                                        <p>Shipped</p>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="uk-grid uk-grid-small uk-text-left" data-uk-grid-margin data-uk-grid-match="{target:'.md-card'}">
                                                    <div class="doubleBtns uk-row-first">


                                                        <?php

                                                        $text = '';
                                                        $btn = '';
                                                        if($order->order_status == 0) {
                                                            $text = 'Cancelled';
                                                            $btn = 'button';
                                                        }
                                                        elseif($order->order_status == 10) {
                                                            $text = 'Cancelled Request';
                                                            $btn = 'button';
                                                        } elseif($order->order_status == 7) {
                                                            $text = 'Returned Request';
                                                            $btn = 'button';
                                                        } elseif($order->order_status == 8) {
                                                            $text = 'Returned';
                                                            $btn = 'button';
                                                        }
                                                        else {

                                                            $text = '<i class="material-icons">done</i> Verify Payment';
                                                            $btn = 'submit';

                                                        }

                                                        if (strtolower($order->payment_method) == 'cash on delivery') {
                                                            $paymentStatus = false;
                                                            $payTabNo = '';
                                                            $btn = 'button';
                                                        }
                                                        if(strtolower($order->payment_method) == 'visa'){
                                                            $payTabNo = '<strong>Transaction ID: </strong>'.$order->card_id;
                                                            $paymentStatus = true;
                                                            $btn = 'button';
                                                        }
                                                        if(strtolower($order->payment_method) == 'mastercard'){
                                                            $payTabNo = '<strong>Transaction ID: </strong>'.$order->card_id;
                                                            $paymentStatus = true;
                                                            $btn = 'button';
                                                        }
                                                        if (strtolower($order->payment_method) != 'cash on delivery' && strtolower($order->payment_method) != 'visa' &&
                                                            strtolower($order->payment_method) != 'mastercard') {
                                                            $payTabNo = '<div class="uk-form-file btn ioudBtn blue"><i class="material-icons">crop_original</i>
                                                                             Upload Receipt
                                                                             <input id="form-file" type="file"
                                                                                    name="order_receipt"></div>';
                                                            $paymentStatus = true;
                                                        }

                                                        ?>


                                                      <?php if($order->receipt == "" && $paymentStatus){ ?>
                                                        <form class="ajax_form" onSubmit="return false;" action="<?php echo base_url().'admin/orders/action'; ?>" method="post" enctype="multipart/form-data">
                                                       <?php } ?>
                                                            <input type="hidden" name="form_type" value="upload">
                                                           <input type="hidden" name="order_id" value="<?php echo $order->id; ?>">
                                                            <?php if($order->order_status != 0 || $order->order_status != 7 || $order->order_status != 8) { ?>
                                                                <?php if ($order->receipt == "" && $order->order_status != 0 && $order->order_status != 10) { ?>
                                                                    <?php echo $payTabNo; ?>
                                                                    
                                                                <?php } else {
                                                                    if ($order->order_status < 4 && $order->order_status != 0 && !$paymentStatus) {
                                                                        ?>
                                                                        <button class=" btn ioudBtn blue btnNotBtn">
                                                                        <?php if($order->receipt !== ''){ ?>
                                                                        <i class="material-icons">crop_original</i>

                                                                            <a href="<?php echo base_url() . 'uploads/receipt/' . $order->receipt; ?>"
                                                                    download><?php echo $order->receipt; ?></a>

                                                                    <i class="material-icons"
                                                                       onclick="removeReceipt('<?php echo $order->id; ?>');">clear</i>

                                                                        <?php }else{
                                                                            echo $payTabNo;
                                                                        }?>
                                                                            </button>

                                                                <?php }
                                                            }
                                                            }if (strtolower($order->payment_method) != 'cash on delivery' && strtolower($order->payment_method) != 'visa' &&
                                                                strtolower($order->payment_method) != 'mastercard') {?>
														
                                                        <button class="btn ioudBtn green" type="<?php echo $btn; ?>" <?php echo ($order->receipt != "" ? "disabled" : ""); ?>><?php echo $text; ?> </button>
                                                       <?php }if($order->receipt == ""){ ?>
                                                       </form>

                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="uk-grid uk-grid-small" data-uk-grid-margin data-uk-grid-match="{target:'.md-card'}">
                                    <div class="uk-width-medium-1-1">
                                        <div class="md-card">
                                            <div class="md-card-content">
                                                <h1>Item Information <span><?php echo getOrderProductCount($order->id); ?> Items</span></h1>
                                                <div class="uk-overflow-container">
                                                    <table class="uk-table uk-text-nowrap">
                                                        <thead>
                                                        <tr>
                                                            <th>Product</th>
                                                            <th>Weight</th>
                                                            <!--<th>Color</th>-->
                                                            <th>Quantity</th>
                                                            <th>Unit Price</th>
                                                            <th>total</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        <?php
                                                        if (strtolower($order->payment_method) == "visa") {
                                                            $payment_img = base_url() . 'assets/frontend/images/visa_payment.png';
                                                        } elseif (strtolower($order->payment_method) == "cash on delivery") {
                                                            $payment_img = base_url() . 'assets/frontend/images/cash_payment.png';
                                                        } elseif (strtolower($order->payment_method) == "bank_transfer") {
                                                            $payment_img = base_url() . 'assets/admin/assets/img/wire.png';
                                                        } elseif (strtolower($order->payment_method) == "sadad") {
                                                            $payment_img = base_url() . 'assets/frontend/images/sadad_payment.png';
                                                        } else {
                                                            $payment_img = base_url() . 'assets/admin/assets/img/master.png';
                                                        }

                                                            $productPrice = 0;
                                                            foreach($order_products as $product) {
                                                                $prod_image =  getProductImages($product->id);

                                                                foreach($prod_image as $prod_img){
                                                                    if($prod_img['is_thumbnail'] == 1){
                                                                        $thumbnail = $prod_img['thumbnail'];
                                                                    }
                                                                }
                                                                $total = (int)$product->price*(int)$product->quantity;
                                                                $productPrice += (float)$product->price*(float)$product->quantity;
                                                                $grandTotal = (float)$order->shipment_price +(float)$productPrice;
                                                                $cat = getCategoriesById($product->category_id);

                                                                ?>

                                                        <tr>
                                                            <td>
                                                                <img src="<?php echo base_url(); ?>uploads/images/thumbs/products/<?php echo $thumbnail; ?>" alt="Img" height="57" width="57" />
                                                                <h3><?php echo $product->eng_name; ?></h3>
                                                                <p><?php echo $cat['eng_name']?></p>
                                                            </td>
                                                            <td><?php echo $product->weight_value; ?> <?php echo $product->weight_unit; ?></td>

                                                            <td><?php echo $product->quantity; ?></td>
                                                            <td><?php echo number_format($product->price,'2','.',''); ?></td>
                                                            <td><?php echo number_format($total,'2','.',''); ?> <?php echo $order->currency_unit; ?></td>
                                                        </tr>

                                                        <?php } ?>


                                                        </tbody>
                                                        <tfoot>
                                                        <tr>
                                                            <th colspan="6" align="right" class="uk-text-right">
                                                                <strong>Total Products Cost:</strong> <?php echo number_format($productPrice,'2','.','').' '.$order->currency_unit; ?>
                                                                <br />
                                                                Shipment: <?php echo number_format($order->shipment_price,'2','.',''); ?> <?php echo $order->currency_unit; ?>
                                                                
                                                                <?php if($order->coupon_id > 0){
																		$coupon = getCouponDiscountDb($order->coupon_id);
																		?>
                                                                        <br />
																		<p>Voucher Code:<?php if($coupon->type == 1)
																				{
																					echo $coupon->discount.'%'; 
																				}
																				else
																				{
																					echo number_format($coupon->discount,'2','.','').' '.$order->currency_unit;
																				}?>
                                                                        </p>
																	<?php }?>



                                                                <?php if($order->loyalty_discount != '' && $order->loyalty_discount > 0){

                                                                    ?>
                                                                    <p>Loyalty Discount:
                                                                        <?php

                                                                            echo $order->loyalty_discount.' '.$order->currency_unit;
                                                                        ?>
                                                                    </p>
                                                                <?php }?>


																	<?php $gifts = getGiftCarDetailDb($order->id);
																	
																		if($gifts){
																			
																			foreach($gifts as $gift)
																			{
																		?>
                                                                        <br />
																		<p><?php echo $gift['eng_name'];?>:<?php echo number_format($gift['amount'],'2','.','');?> <?php echo $order->currency_unit; ?></p>
																	<?php 	}
																		  }
																			?>
                                                                
                                                                <?php if($order->payment_method == 'Cash On Delivery'){
																	?>
                                                                    <br />
																	<p>COD Charges:<?php echo number_format($order->extra_charges, '2', '.', '');?> <?php echo $order->currency_unit; ?></p>
																<?php }?>

                                                                <!--<i class="material-icons">card_giftcard</i> Voucher: -50.00 SR-->
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <th colspan="4" class="red">
                                                                Order
                                                                <img src="<?php echo $payment_img; ?>" alt="Wire" height="15" width="33" />
                                                            </th>
                                                            <th colspan="4" class="uk-text-right">
                                                                <h1>Grand Total: <span><?php echo number_format($order->total_amount,'2','.',''); ?> <?php echo $order->currency_unit; ?></span></h1>
                                                            </th>
                                                        </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="uk-width-medium-1-1">
                                        <div class="md-card">
                                            <div class="md-card-content">
                                                <h1>Order History</h1>
                                                <div class="uk-overflow-container">
                                                    <table class="uk-table uk-text-nowrap">
                                                        <thead>
                                                        <tr>
                                                            <th>Date added</th>
                                                            <th>Comment</th>
                                                            <th>Status</th>
                                                            <!--<th>Override</th>-->
                                                            <th>Customer Notified</th>
                                                            <th>User</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        <?php foreach($order_history as $history) {


                                                            ?>
                                                        <tr>
                                                            <td><?php echo date('d/m/Y',strtotime($history->created_at)); ?></td>
                                                            <td><?php echo $history->comment; ?></td>
                                                            <td><?php echo getPaymentStatus($history->order_status); ?></td>
                                                            <td><?php echo $history->notify_customer; ?></td>
                                                            <td><?php echo $history->user; ?></td>
                                                        </tr>
                                                       <?php } ?>

                                                        </tbody>
                                                       <?php //if($order->order_status != 0  && $order->order_status != 8 && $order->order_status != 9) { ?>
                                                        <tfoot>
                                                        <tr>
                                                            <th colspan="5">
                                                                <button data-uk-modal="{target:'#addA_history'}" class="btn ioudBtn green fullWidth OrdBtnMng">Add Order History</button>
                                                            </th>
                                                        </tr>
                                                        </tfoot>
                                                      <?php //} ?>

                                                    </table>

                                                    <div class="uk-modal" id="addA_history">
                                                        <form class="ajax_form" action="<?php echo base_url().'admin/orders/order_history'; ?>" method="post" onSubmit="return false;">
                                                        <div class="uk-modal-dialog">
                                                            <div class="uk-modal-header">
                                                                <h3 class="uk-modal-title">Add a History</h3>
                                                                <button class="uk-modal-close uk-close closeBtnEd" type="button">&nbsp;</button>
                                                            </div>
                                                            <div class="uk-grid " data-uk-grid-margin>
                                                                <div class="uk-width-medium-3-10">
                                                                    <label>Order Status</label>
                                                                </div>
                                                                <div class="uk-width-medium-4-10">
                                                                    <!--<div class="selectDD">
                                                                        <select>
                                                                            <option value="">Select Status</option>
                                                                            <option value="Shipped">Shipped</option>
                                                                            <option value="verified">verified</option>
                                                                        </select>
                                                                    </div>-->
                                                                    <select class="uk-form-width-medium" name="order_status" id="task_assignee" data-md-selectize-inline>
                                                                        <option value="">Select Status</option>

                                                                       <?php
                                                                       /*$returnReject = array('0','1','2','3','4','5','6','7','8');
                                                                       if($order->order_status <= 4){
                                                                           $returnReject = array('0','1','2','3','4','5','6');
                                                                       }*/
                                                                        foreach ($order_statuses as $order_s) {
                                                                          // if($order_s->status_id != 7) {

                                                                               //if ($order_s->status_id > $order->order_status || $order_s->status_id == "0") {
                                                                                  // if ($returnReject[$order_s->status_id] == $order_s->status_id) {
                                                                            if ($order_s->status_id != 7  && $order_s->status_id != 10)  {
                                                                            ?>
                                                                                       <option value="<?php echo $order_s->status_id . '|' . $order_s->status; ?>"><?php echo $order_s->status; ?></option>
                                                                                   <?php
                                                                            }
                                                                               //}

                                                                           //}
                                                                        } ?>

                                                                    </select>
                                                                </div>
                                                            </div>
<!--                                                            <div class="uk-grid " data-uk-grid-margin>-->
<!--                                                                <div class="uk-width-medium-3-10">-->
<!--                                                                    <label>Override</label>-->
<!--                                                                </div>-->
<!--                                                                <div class="uk-width-medium-7-10">-->
<!--                                                                    <p>-->
<!--                                                                        <input type="checkbox" name="order_override" value="yes" id="override" data-md-icheck />-->
<!--                                                                        <label for="override" class="inline-label">&nbsp;</label>-->
<!--                                                                    </p>-->
<!--                                                                </div>-->
<!--                                                            </div>-->
                                                            <div class="uk-grid " data-uk-grid-margin>
                                                                <div class="uk-width-medium-3-10">
                                                                    <label>Notify Customer</label>
                                                                </div>
                                                                <div class="uk-width-medium-7-10">
                                                                        <span class="icheck-inline">
                                                                            <input type="checkbox" value="sms" name="notify_customer_sms" id="notCus_1" data-md-icheck />
                                                                            <label for="notCus_1" class="">SMS</label>
                                                                        </span>
                                                                    <span class="icheck-inline">
                                                                            <input type="checkbox" value="email" name="notify_customer_email" id="notCus_2" data-md-icheck />
                                                                            <label for="notCus_2" class="">Email</label>
                                                                        </span>
                                                                </div>
                                                            </div>
                                                            <div class="uk-grid " data-uk-grid-margin>
                                                                <div class="uk-width-medium-3-10">
                                                                    <label>Comment</label>
                                                                </div>
                                                                <div class="uk-width-medium-7-10">
                                                                    <textarea cols="30" rows="1" class="md-input" placeholder="Write" name="comment"></textarea>
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="order_id" value="<?php echo $order->id; ?>">
                                                            <input type="hidden" name="user_id" value="<?php echo $userId; ?>">
                                                            <div class="uk-modal-footer uk-text-center">
                                                                <button type="submit" class="btn ioudBtn green">Add</button>
                                                            </div>
                                                        </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                            <!--<div class="uk-grid uk-grid-small proFootListing" data-uk-grid-margin data-uk-grid-match="{target:'.md-card'}">
                                <div class="uk-width-medium-5-10">
                                    Show entries
                                    <select id="select_demo_4" data-md-selectize>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                    </select>
                                </div>
                                <div class="uk-width-medium-5-10">
                                    <ul class="uk-pagination uk-text-right">
                                        <li class="uk-disabled"><span>Previous</span></li>
                                        <li class="uk-active"><span>1</span></li>
                                        <li><span>2</span></li>
                                        <li><a href="#">Next</a></li>
                                    </ul>
                                </div>
                            </div>-->
                        </div>
                    </div><!--      Single Order Row END Here       -->
                </div>
            </div>
        </div>
    </div>

</div>


</body>
</html>