 <!-- Content Wrapper. Contains page content -->
      <div id="page_content">
        <div id="page_content_inner">
        <a href="<?php echo base_url();?>admin/orders/exportExcelReturnItems" target="_blank" class="md-btn"><img src="<?php echo base_url();?>assets/admin/assets/images/excelIcon.png" alt="excel" /></a>
			<div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <div class="uk-overflow-container"> 
                                <table class="uk-table uk-table-align-vertical listing dt_default">
                                    <thead>
                                        <tr>
                                             <th>Sr#</th>

                                              <th>Order Id</th>
                      
                                              <th class="nosort"> Name</th>
                                              
                                              <th class="nosort"> Email</th>
                                              
                                              <th class="nosort"> Product Name</th>
                                              
                                              <th class="nosort">Reason</th>
                                              
                                              <th class="nosort">Other Comments</th>
                                              
                                              <th class="nosort">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
									 $index = 1;
									 foreach($orders as $order){?>
               							<tr>
                                             <td><?php echo $index;?></td>
                                             
                                             <td><?php echo str_pad($order->order_id, 10, "0", STR_PAD_LEFT);;?></td>

                                              <td><?php echo $order->first_name.' '.$order->last_name;?></td>
                      
                                              <td><?php echo $order->email;?></td>
                                              
                                              <td><?php echo $order->eng_name;?></td>
                                              <td><?php echo $order->reason;?></td>
                                              <td><?php echo $order->other_comments;?></td>
                                              <td><a href="<?php echo base_url();?>admin/orders/order_details/<?php echo $order->order_id;?>"><i title="Order Detail" class="material-icons md-icon dp48">description</i></a><a href="<?php echo base_url();?>admin/product/edit/<?php echo $order->product_id;?>"><i title="Product Detail" class="material-icons md-icon dp48">redeem</i></a></td>
                                        </tr>
                                     <?php $index++;}?>
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
       <!-- /.content-wrapper --> 