<div id="page_content">

    <div id="page_content_inner">
        <div class="md-card">
            <div class="md-card-content orderPage">
                <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
                    <div class="heading_actions">
                        <ul id="menu_top" class="uk-width-2-6 uk-clearfix">
                            <li data-uk-dropdown class="">
                                <span class="uk-text-upper uk-text-small"><a href="#">Filter</a></span>
                                <h1>Order Status <i class="material-icons">&#xE5C5;</i></h1>
                                <div class="uk-dropdown">
                                    <ul class="parentCatEd uk-nav uk-nav-dropdown">

                                        <li class="hasDropDown">
                                            <a href="javascript:void(0);" class="order_status_filter" data-cat="">All</a>

                                            <?php foreach($order_statuses as $key => $order_s) {
                                                if ($key == 1) { ?>
                                                    <a href="javascript:void(0);" class="order_status_filter"
                                                       data-cat="<?php echo '10'; ?>">Cancelled Request</a>

                                                <?php }
                                                if ($order_s->status_id != 10) { ?>
                                                    <a href="javascript:void(0);" class="order_status_filter"
                                                       data-cat="<?php echo $order_s->status_id; ?>"><?php echo $order_s->status; ?></a>

                                                <?php }

                                            }?>

                                        </li>

                                    </ul>

                                </div>
                            </li>
                            <!--<li data-uk-dropdown class="">
                                <span class="uk-text-upper uk-text-small"><a href="#">Show</a></span>
                                <h1>Available <i class="material-icons">&#xE5C5;</i></h1>
                                <div class="uk-dropdown">
                                    <ul class="parentCatEd uk-nav uk-nav-dropdown">

                                            <li class="hasDropDown">
                                            <a href="javascript:void(0);"><?php /*echo $cat['eng_name']; */?></a>

                                        </li>


                                    </ul>

                                </div>
                            </li>-->
                            
                        </ul>
                        <div class="uk-width-2-3  ">
                            <ul class="ColorDis">
                                <li class="borderLeft red"><span>Not</span>Started</li>
                                <li class="borderLeft blue"><span>Under</span>Process</li>
                                <li class="borderLeft green"><span>Order</span>Shipped</li>
                                <li class="borderLeft black"><span>Order</span>Delivered</li>
                            </ul>
                            <div class="srchDDED">
                                <div data-uk-dropdown class="dropDownED">
                                    Order Id
                                    <i class="md-icon material-icons">&#xE5C5;</i>
                                    <!--<div class="uk-dropdown uk-dropdown-small">
                                        <ul class="uk-nav">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Other Action</a></li>
                                            <li><a href="#">Other Action</a></li>
                                        </ul>
                                    </div>-->
                                </div>
                                <div class="serchInpEd uk-width-medium-1-2">
                                    <label for="contact_list_search">Search</label>
                                    <input class="md-input order_search" type="text" name="search" value="" id="contact_list_search"/>
                                </div>


                                <form class="search_order_form" action="<?php echo base_url().'admin/orders'; ?>" method="post" style="display: none;">
                                <input type="hidden" name="order_search_value" class="order_search_value">
                                    <button type="submit"></button>
                                </form>

                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="orderListBox">

                 <?php if($orders) {
                     foreach ($orders as $order) {
                         if ($order->order_status == 1) {
                             $statusClass = 'red';
                             $order_status = 'Prepare Order';
                             $circle_1 = "";
                             $circle_2 = "black";
                             $circle_3 = "black";
                             $circle_4 = "black";

                         } elseif ($order->order_status == 2) {
                             $statusClass = 'blue';
                             $order_status = 'Verify Payment';
                             $circle_1 = "";
                             $circle_2 = "";
                             $circle_3 = "black";
                             $circle_4 = "black";

                         } elseif ($order->order_status == 3) {
                             $statusClass = 'blue';
                             $order_status = 'Prepare Shipment';
                             $circle_1 = "";
                             $circle_2 = "";
                             $circle_3 = "";
                             $circle_4 = "black";

                         } elseif ($order->order_status == 4) {
                             $statusClass = 'green';
                             $order_status = 'Shipped';
                             $circle_1 = "";
                             $circle_2 = "";
                             $circle_3 = "";
                             $circle_4 = "";

                         }elseif ($order->order_status == 5) {
                             $statusClass = 'black';
                             $order_status = 'Delivered ';
                             $circle_1 = "";
                             $circle_2 = "";
                             $circle_3 = "";
                             $circle_4 = "";
                         }elseif ($order->order_status == 6) {
                             $statusClass = 'black';
                             $order_status = 'Completed ';
                             $circle_1 = "";
                             $circle_2 = "";
                             $circle_3 = "";
                             $circle_4 = "";
                         }elseif ($order->order_status == 7) {
                             $statusClass = 'black';
                             $order_status = 'Return Request ';
                             $circle_1 = "";
                             $circle_2 = "";
                             $circle_3 = "";
                             $circle_4 = "";
                         }elseif ($order->order_status == 8) {
                             $statusClass = 'black';
                             $order_status = 'Returned ';
                             $circle_1 = "";
                             $circle_2 = "";
                             $circle_3 = "";
                             $circle_4 = "";
                         }elseif ($order->order_status == 9) {
                             $statusClass = 'black';
                             $order_status = 'Rejected ';
                             $circle_1 = "";
                             $circle_2 = "";
                             $circle_3 = "";
                             $circle_4 = "";
                         }elseif ($order->order_status == 10) {
                             $statusClass = 'black';
                             $order_status = 'Cancelled Request ';
                             $circle_1 = "black";
                             $circle_2 = "black";
                             $circle_3 = "black";
                             $circle_4 = "black";
                         }
                         else {
                             $statusClass = 'black';
                             $order_status = 'Cancelled ';
                             $circle_1 = "black";
                             $circle_2 = "black";
                             $circle_3 = "black";
                             $circle_4 = "black";
                         }


                         $address = getCartAddress($order->address_id);
                         $user = getUserById($order->user_id);


                         if (!$user) {
                             $fullName = $address->full_name;
                             $email = $address->email;
                             $countryCode = $address->country;
                             $cityId = $address->city;
                             $mobile_no = $address->phone_no;
                             $userId = $address->user_id;
                         } else {
                             $fullName = $user['first_name'] . " " . $user['last_name'];
                             $email = $user['email'];
                             $countryCode = $user['country'];
                             $cityId = $user['city'];
                             $mobile_no = $user['mobile_no'];
                             $userId = $user['id'];
                         }
                         ?>
                         <div class="singleOrder <?php echo $statusClass; ?>" id="<?php echo $order->id; ?>">
                             <div class="orderRow uk-parent" style="">
                                 <div class="colEd">
                                     <h3>Order # <?php echo str_pad($order->id, 6, "0", STR_PAD_LEFT); ?></h3>
                                     <p><?php echo getCityById($cityId); ?>
                                         , <?php echo getCoutryByCode($countryCode); ?></p>
                                 </div>
                                 <div class="colEd">
                                     <h3>Order Date</h3>
                                     <p><?php echo date('d M y, h:i a', strtotime($order->created_at)); ?></p>
                                 </div>
                                 <div class="colEd">
                                     <h3>Items</h3>
                                     <p><?php echo getOrderProductCount($order->id); ?></p>
                                 </div>
                                 <div class="colEd">
                                     <h3>Order Cost</h3>
                                     <p>
                                         <?php echo number_format($order->total_amount, '2', '.', ''); ?> SR | Paid

                                         <?php
                                         if (strtolower($order->payment_method) == "visa") {
                                             $payment_img = base_url() . 'assets/frontend/images/visa_payment.png';
                                         } elseif (strtolower($order->payment_method) == "cash on delivery") {
                                             $payment_img = base_url() . 'assets/frontend/images/cash_payment.png';
                                         } elseif (strtolower($order->payment_method) == "bank_transfer") {
                                             $payment_img = base_url() . 'assets/admin/assets/img/wire.png';
                                         } elseif (strtolower($order->payment_method) == "mastercard") {
                                             $payment_img = base_url() . 'assets/admin/assets/img/master.png';
                                         } else {
                                             $payment_img = base_url() . 'assets/frontend/images/sadad_payment.png';
                                         }
                                         ?>
                                         <span class="small">via</span>
                                         <img src="<?php echo $payment_img; ?>"
                                              alt="<?php echo $order->payment_method; ?>" height="15" width="33"/>
                                     </p>
                                 </div>
                                 <div class="colEd">
                                     <h3>Delivered on</h3>
                                     <p><?php echo getShipmentDays($order->shipment_group, $order->shipment_type); ?></p>
                                 </div>
                                 <div class="colEd">
                                     <h3>Status</h3>
                                     <p class="blue"><?php echo($order->payment_status == "1" ? "Paid" : "Pending"); ?></p>
                                 </div>
                                 <div class="colEd">
                                     <button type="button"
                                             class="btn ioudBtn green"><?php echo $order_status; ?></button>
                                 </div>
                                 <div class="clearfix"></div>
                             </div>
                             <div class="orderExpand uk-child">
                                 <div class="orderRow">
                                     <div class="colEd">
                                         <h3>Order # <?php echo str_pad($order->id, 6, "0", STR_PAD_LEFT); ?></h3>
                                         <p><?php echo getCityById($cityId); ?>
                                             , <?php echo getCoutryByCode($countryCode); ?></p>
                                     </div>
                                     <div class="colEd">
                                         <h3>Order Date</h3>
                                         <p><?php echo date('d M y, h:i a', strtotime($order->created_at)); ?></p>
                                     </div>
                                     <div class="colEd">
                                         <h3>Delivered on</h3>
                                         <p>
                                             <!--6 Jun 17, approx--> <?php echo getShipmentDays($order->shipment_group, $order->shipment_type); ?></p>
                                     </div>
                                     <div class="colEd uk-float-right align-right">
                                         <ul class="topIcons">
                                             <li>
                                                 <a href="<?php echo base_url(); ?>admin/orders/order_details/<?php echo $order->id; ?>"><i
                                                             class="material-icons">description</i></a></li>
                                             <li><a href="javascript:void(0);"
                                                    onclick="deleteRecord(<?php echo $order->id; ?>,'admin/orders/action','');"><i
                                                             class="material-icons">&#xE872;</i></a></li>
                                         </ul>
                                     </div>
                                     <div class="clearfix"></div>
                                 </div>
                                 <div class="boxes">
                                     <div class="uk-grid uk-grid-small" data-uk-grid-margin
                                          data-uk-grid-match="{target:'.md-card'}">
                                         <div class="uk-width-medium-1-3">
                                             <div class="md-card">
                                                 <div class="md-card-content">
                                                     <h2>Customer Name</h2>
                                                     <p><strong class="big"><?php echo $fullName; ?></strong></p>
                                                     <br/>
                                                     <h2>Email</h2>
                                                     <p><strong class="big"><?php echo $email; ?></strong></p>
                                                     <br/>
                                                     <h2>Customer Type</h2>
                                                     <p><strong class="big"><?php echo ($order->user_type == '0' ? 'Guest' : 'Registered');?></strong></p>
                                                 </div>
                                             </div>

                                         </div>
                                         <div class="uk-width-medium-1-3">
                                             <div class="md-card">
                                                 <div class="md-card-content">
                                                     <h1>Shipment Information</h1>
                                                     <p><strong class="big"><?php echo $fullName; ?></strong></p>
                                                     <p>
                                                         <?php
                                                         $print = str_split($address->address_1, 30);
                                                         echo implode("<br>", $print);
                                                         ?>
                                                         <br/>

                                                         <?php echo getCityById($cityId); ?>
                                                         , <?php echo getCoutryByCode($countryCode); ?>
                                                     </p>
                                                     <p>
                                                         <!--T: +966 12 6030111<br/>-->
                                                         M: <?php echo $mobile_no; ?><br/>
                                                         E: <?php echo $email; ?><br/>
                                                         <!--P.O. Box 11412<br/>
                                                         Zip code 22411<br/>-->
                                                     </p>
                                                 </div>
                                             </div>
                                             <div class="md-card">
                                                 <div class="md-card-content">
                                                     <h1>Shipment Method</h1>
                                                     <h6><?php echo($order->shipment_method == "1" ? "Manual Shipment" : "Aramex"); ?></h6>
                                                     <p>
                                                         <?php echo($order->shipment_type == 'normal' ? "Normal Shipping" : "Fast Shipping") ?>
                                                         , Charges
                                                         + <?php echo number_format($order->shipment_price, '2', '.', ''); ?>
                                                         SR<br/>
                                                         <?php echo getShipmentDays($order->shipment_group, $order->shipment_type); ?>
                                                     </p>

                                                     <p style="margin-bottom: 0">
                                                         <br/>
                                                         <?php if($order->shipment_method == "2"){ ?>
                                                            <strong>Tracking ID: 66603030303666</strong>
                                                         <?php } ?>
                                                     </p>
                                                     <br>
                                                 </div>
                                             </div>
                                         </div>
                                         <div class="uk-width-medium-1-3">
                                             <div class="md-card">
                                                 <div class="md-card-content">
                                                     <h1>Shipment Status</h1>

                                                     <div class="uk-grid uk-grid-small uk-text-center"
                                                          data-uk-grid-margin data-uk-grid-match="{target:'.md-card'}">
                                                         <div class="ordStaStyle">
                                                             <ul>
                                                                 <li class="<?php echo $circle_1; ?>">
                                                                     <div class="wrapper">
                                                                         <div class="circle"></div>
                                                                         <p>
                                                                             Preparing<br/>
                                                                             Order
                                                                         </p>
                                                                     </div>
                                                                 </li>
                                                                 <li class="<?php echo $circle_2; ?>">
                                                                     <div class="wrapper">
                                                                         <div class="circle"></div>
                                                                         <p>
                                                                             Verifying<br/>
                                                                             Payment
                                                                         </p>
                                                                     </div>
                                                                 </li>
                                                                 <li class="<?php echo $circle_3; ?>">
                                                                     <div class="wrapper">
                                                                         <div class="circle"></div>
                                                                         <p>
                                                                             Preparing<br/>
                                                                             Shipment
                                                                         </p>
                                                                     </div>
                                                                 </li>
                                                                 <li class="<?php echo $circle_4; ?>">
                                                                     <div class="wrapper">
                                                                         <div class="circle"></div>
                                                                         <p>
                                                                             Shipped
                                                                         </p>
                                                                     </div>
                                                                 </li>
                                                             </ul>
                                                         </div>
                                                     </div>

                                                     <div class="uk-grid uk-grid-small uk-text-center"
                                                          data-uk-grid-margin data-uk-grid-match="{target:'.md-card'}">
                                                         <div class="doubleBtns uk-row-first">

                                                             <?php if($order->receipt == ""){ ?>
                                                             <form class="ajax_form" onsubmit="return false;" action="<?php echo base_url().'admin/orders/action'; ?>" method="post" enctype="multipart/form-data">
                                                                 <?php } ?>
                                                                 <input type="hidden" name="form_type" value="upload">
                                                                 <input type="hidden" name="order_id" value="<?php echo $order->id; ?>">
                                                                 <?php if($order->order_status != 0 || $order->order_status != 7 || $order->order_status != 8) { ?>
                                                                     <?php if ($order->receipt == "" && $order->order_status != 0 && $order->order_status != 10) { ?>
                                                                         <div class="uk-form-file btn ioudBtn blue">
                                                                             <i class="material-icons">crop_original</i>
                                                                             Upload Receipt
                                                                             <input id="form-file" type="file"
                                                                                    name="order_receipt">
                                                                         </div>
                                                                     <?php } else {
                                                                         if ($order->order_status < 4 && $order->order_status != 0) {
                                                                             ?>
                                                                             <button class=" btn ioudBtn blue btnNotBtn">
                                                                                 <i class="material-icons">crop_original</i>

                                                                                 <a href="<?php echo base_url() . 'uploads/receipt/' . $order->receipt; ?>"
                                                                                    download><?php echo $order->receipt; ?></a>

                                                                                 <i class="material-icons"
                                                                                    onclick="removeReceipt('<?php echo $order->id; ?>');">clear</i>
                                                                             </button>

                                                                         <?php }
                                                                     }
                                                                 }?>
                                                                 <button class="btn ioudBtn green" type="submit" <?php echo ($order->receipt != "" ? "disabled" : ""); ?>> <?php if($order->order_status == 0) { echo 'Cancelled'; } elseif($order->order_status == 10) { echo 'Cancelled Request'; } elseif($order->order_status == 7) { echo 'Returned Request'; } elseif($order->order_status == 8) { echo 'Returned'; } else { ?><i class="material-icons">done</i> Verified Payment <?php } ?></button>
                                                                 <?php if($order->receipt == ""){ ?>
                                                             </form>
                                                         <?php } ?>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div>

                                             <?php
                                             
                                             $products = getProductsByOrder($order->id);
                                             $productPrice = 0;
                                             foreach ($products as $product) {
                                                 $productPrice += (float)$product->price * (float)$product->quantity;
                                             }
                                             $grandTotal = (float)$order->shipment_price + (float)$productPrice;
                                             ?>

                                             <div class="md-card">
                                                 <div class="md-card-content">
                                                     <h1>Payment</h1>
                                                     <p>Total Products
                                                         Cost: <?php echo number_format($productPrice, '2', '.', ''); ?></p>
                                                     <p>
                                                         Shipment: <?php echo number_format($order->shipment_price, '2', '.', ''); ?>
                                                         SR</p>
                                                     <p>Tax: 0.00 SR</p>
                                                     <!--<p>Voucher: -50.00 SR</p>-->
                                                     <div class="totalMethod">
                                                         <h1>Total:
                                                             <strong><?php echo number_format($order->total_amount, '2', '.', ''); ?>
                                                                 SR</strong></h1>
                                                         <h5 class="green">
                                                             Order
                                                             <span>Via</span>
                                                             <img src="<?php echo $payment_img; ?>"
                                                                  alt="<?php echo $order->payment_method; ?>"
                                                                  height="15" width="33"/>
                                                         </h5>
                                                         <div class="clearfix"></div>
                                                     </div>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>

                                     <div class="clearfix"></div>
                                 </div>
                             </div>
                         </div><!--      Single Order Row END Here       -->

                     <?php }
                 }else{ ?>
                     <div style="color: red; font-size:large; text-align: center;"><span >No record found!</span></div>
               <?php } ?>
                    <?php if($orders && $count > 10) { ?>

                    <div class="uk-grid uk-grid-small proFootListing" data-uk-grid-margin="" data-uk-grid-match="{target:'.md-card'}">


                        <div class="uk-width-medium-5-10 uk-row-first">
                            Show entries
                            <select class="order_limit" id="select_demo_4" data-md-selectize>

                                <option value="10" <?php if($limit == "10") { echo 'selected'; } ?>>10</option>
                                <option value="20" <?php if($limit == "20") { echo 'selected'; } ?>>20</option>
                                <option value="30" <?php if($limit == "30") { echo 'selected'; } ?>>30</option>
                                <option value="40" <?php if($limit == "40") { echo 'selected'; } ?>>40</option>
                                <option value="50" <?php if($limit == "50") { echo 'selected'; } ?>>50</option>
                                <option value="60" <?php if($limit == "60") { echo 'selected'; } ?>>60</option>
                                <option value="70" <?php if($limit == "70") { echo 'selected'; } ?>>70</option>
                                <option value="80" <?php if($limit == "80") { echo 'selected'; } ?>>80</option>
                                <option value="90" <?php if($limit == "90") { echo 'selected'; } ?>>90</option>
                                <option value="100" <?php if($limit == "100") { echo 'selected'; } ?>>100</option>
                            </select>
                        </div>

                        <?php echo $links; ?>
                        <!--<div class="uk-width-medium-5-10">
                            <ul class="uk-pagination uk-text-right">
                                <li class="uk-disabled"><span>Previous</span></li>
                                <li class="uk-active"><span>1</span></li>
                                <li><span>2</span></li>
                                <li><a href="#">Next</a></li>
                            </ul>
                        </div>-->
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

</div>
