<div id="page_content">
    <div id="page_content_inner">
        <form action="<?php echo base_url(); ?>admin/show_room/action" method="post" onsubmit="return false" class="ajax_form">
            <div class="md-card">
                <div class="md-card-content">
                    <h3 class="heading_a">Show Room</h3>
                    <br>
                    <div class="uk-grid" data-uk-grid-margin>
                        <input type="hidden" name="form_type" value="update">
						<input type="hidden" name="tpl_name" value="showRoom">
                        <input type="hidden" name="id" value="<?php echo $record[0]->id;?>">
                        <div class="uk-width-medium-1-2">
                            <label>English Title</label>
                            <div class="uk-form-row">
                                <input type="text" class="md-input" name="eng_title" value="<?php echo $record[0]->eng_title;?>">
                            </div>

                            <br>
                            <label>English Office Type</label>
                            <div class="uk-form-row">
                                <input type="text" class="md-input" name="eng_office_type" value="<?php echo $record[0]->eng_office_type;?>">
                            </div>
                            <label>English Office Address</label>
                            <div class="uk-form-row">
                                <textarea cols="30" rows="4" id="eng_head_office" class="md-input eng_text" name="eng_head_office">
                                    <?php echo $record[0]->eng_head_office;?>
                                </textarea>
                            </div>
							
                        </div>
                        <div class="uk-width-medium-1-2">
                            <label>Arabic Title</label>
                            <div class="uk-form-row">
                                <input type="text" class="md-input" name="arb_title" value="<?php echo $record[0]->arb_title;?>">
                            </div>

                            <br>
                            <label>Arabic Office Type</label>
                            <div class="uk-form-row">
                                <input type="text" class="md-input" name="arb_office_type" value="<?php echo $record[0]->arb_office_type;?>">
                            </div>
                            <label>Arabic Office Address</label>
                            <div class="uk-form-row">
                                <textarea cols="30" rows="4" id="arb_head_office" class="md-input arb_text" name="arb_head_office">
                                    <?php echo $record[0]->arb_head_office;?>
                                </textarea>
                            </div>
							
                        </div>




                        <div class="uk-width-medium-1-1">
                            <?php foreach($locations as $location) { ?>
                                <div class="uk-grid form_section" id="im_form_row" style="display: block !important;">
                                    <div class="uk-width-1-2">

                                        <div id="file_upload-drop" class="uk-file-upload">
                                            <input id="lot_longs" value="<?php echo $location->lat_long; ?>" type="text" class="md-input location" data-exist="1" name="location[]" placeholder="location">
                                        </div>
                                    </div>
                                    <span class="uk-input-group-addon">
                                <a href="#" class="btnSectionRemove"><i class="material-icons md-24"></i></a>
                                </span>

                                </div>
                            <?php } ?>

                            <div class="uk-grid form_section" id="im_form_row">
                                <div class="uk-width-1-2">

                                    <div id="file_upload-drop" class="uk-file-upload">
                                        <input id="lot_longs" type="text" class="md-input location" data-exist="1" name="location[]" placeholder="location">
                                    </div>
                                </div>
                                <span class="uk-input-group-addon">
                                <a href="#" class="btnSectionClone" data-section-clone="#im_form_row">
                                <i class="material-icons md-24">&#xE146;</i>
                                </a>
                                </span>

                            </div>
                            <br><span class="uk-input-group-addon">Click + to add Locations
                                            <a href="javascript:void(0);" class="btnSectionClone clone_prepend" data-section-clone="#im_form_row">
                                                <i class="material-icons md-24"> &#xE146;</i></a>
                            </span>
                        </div>

                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row">
                            <select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}" title="Select with tooltip" name="city">
                                <option value="">Select City</option>
                                <option value="0">Top</option>
                                <?php

                                echo $cities;



                                ?>

                            </select>
                            </div>

                        </div>



                    </div>
                    <br>
                    <br>
                    <br>
                    <div class="uk-width-medium-1-2">
                        <label>Image</label>
                        <img src="<?php echo base_url().'assets/frontend/images/'.($record[0]->banner_image != '' ? $record[0]->banner_image : 'no_imge.png'); ?>" alt="Image" data-uk-modal="{target:'#modal_lightbox'}" width="100">
                    </div>
                        <div class="uk-width-large-1-1">
                            <h3 class="heading_a">
                                Upload Image
                            </h3> <span style="color:red">(1400 * 365 pixels)</span>
                            <div class="uk-grid">
                                <div class="uk-width-1-1">
                                    <div id="file_upload-drop" class="uk-file-upload"> <a class="uk-form-file md-btn">choose file<input id="file_upload-select" class="images_upload" type="file" name="image"></a> </div>
                                    <div id="file_upload-progressbar" class="uk-progress uk-hidden">
                                        <div class="uk-progress-bar" style="width:0">0%</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="md-card">
                <div class="md-card-toolbar">
                    <h3 class="md-card-toolbar-heading-text">              Keyword          </h3> </div>
                <!----<input type="hidden" name="home_page_id" value="<?php //echo $about_us->home_page_id?>" />--->
                <input type="hidden" name="tpl_name" value="showroom" />
                <?php //$keyword = getPageData($about_us->home_page_id);?>
                    <div class="md-card-content large-padding">
                        <div class="uk-grid uk-grid-divider uk-grid-medium" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2">
                                <div class="uk-form-row">
                                    <label>English Meta Title</label>
                                    <input type="text" class="md-input" value="<?php echo $record[0]->eng_meta_title;?>" name="eng_meta_title" /> </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <div class="uk-form-row">
                                    <label>Arabic Meta Title</label>
                                    <input type="text" class="md-input" value="<?php echo $record[0]->arb_meta_title;?>" name="arb_meta_title" /> </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <div class="uk-form-row">
                                    <label>Englishn Meta Description</label>
                                    <textarea cols="30" rows="4" class="md-input" name="eng_meta_description">
                                        <?php echo $record[0]->eng_meta_description;?>
                                    </textarea>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <div class="uk-form-row">
                                    <label>Arabic Meta Description</label>
                                    <textarea cols="30" rows="4" class="md-input" name="arb_meta_description">
                                        <?php echo $record[0]->arb_meta_description;?>
                                    </textarea>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <div class="uk-form-row">
                                    <label>Englishn Meta Keyword</label>
                                    <textarea cols="30" rows="4" class="md-input" name="eng_meta_keyword">
                                        <?php echo $record[0]->eng_meta_keyword;?>
                                    </textarea>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <div class="uk-form-row">
                                    <label>Arabic Meta Keyword</label>
                                    <textarea cols="30" rows="4" class="md-input" name="arb_meta_keyword">
                                        <?php echo $record[0]->arb_meta_keyword;?>
                                    </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </form>
        <?php //if(viewEditDeleteRights('About Us','edit')) { ?>
            <div class="md-fab-wrapper">
                <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id=""> <i class="material-icons">&#xE161;</i> </a>
            </div>
    <div class="md-fab-wrapper" style="right:95px;">

        <a class="md-fab md-fab-primary" href="<?php echo base_url();?>admin/Show_room" id="">

            <i class="material-icons">keyboard_backspace</i>

        </a>

    </div>

            <?php //} ?>
                <!-- light box for image -->
                <div class="uk-modal" id="modal_lightbox">
                    <div class="uk-modal-dialog uk-modal-dialog-lightbox">
                        <button type="button" class="uk-modal-close uk-close uk-close-alt"></button> <img src="<?php echo base_url().'assets/frontend/images/'.($record[0]->banner_image != '' ? $record[0]->banner_image : 'no_imge.png'); ?>" alt="" /> </div>
                </div>
                <!-- end light box for image -->
    </div>
</div>
<script>

</script>