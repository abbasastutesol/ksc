<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="<?php echo base_url();?>assets/frontend/images/favicon.ico" sizes="16x16">
    <link rel="icon" type="image/png" href="<?php echo base_url();?>assets/frontend/images/favicon.ico" sizes="32x32">
    <title>Admin | Login</title>

    <!--<link rel="stylesheet" href="<?php echo base_url();?>assets/admin/plugins/uikit/css/uikit.almost-flat.min.css" media="all">-->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/assets/css/login_page.css" media="all">
  <script> var base_url =  '<?php echo base_url();?>';</script>
</head>

  <body class="login_page">
  <div class="page_loader"></div>
<div class="login_page_wrapper">
        <div class="md-card" id="login_card">
       
            <div class="md-card-content large-padding" id="login_form">
                <div class="login_heading">
				<h2>IOUD Admin</h2>
                    <!--<div class="user_avatar"></div>-->
                </div>
        <form action="<?php echo base_url(); ?>admin/login/validate" method="post" id="login" class="" onsubmit="return false;">					
		<div class="login_msg"></div>
                    <div class="uk-form-row">
                        <label for="login_email">Email</label>
                        <input class="md-input" type="email" id="email" name="email" autocomplete="off" value=""  required/>
                    </div>
                    <div class="uk-form-row">
                        <label for="login_password">Password</label>
                        <input class="md-input" type="password" id="pass" name="password"  autocomplete="off" value=""  required/>

                    </div>
                    <div class="uk-margin-medium-top">
                       <button type="submit" id="btn_login" class="md-btn md-btn-primary md-btn-block md-btn-large">Sign In</button>
                    </div>
                    <!--<div class="uk-grid uk-grid-width-1-3 uk-grid-small uk-margin-top">
                        <div><a href="#" class="md-btn md-btn-block md-btn-facebook" data-uk-tooltip="{pos:'bottom'}" title="Sign in with Facebook"><i class="uk-icon-facebook uk-margin-remove"></i></a></div>
                        <div><a href="#" class="md-btn md-btn-block md-btn-twitter" data-uk-tooltip="{pos:'bottom'}" title="Sign in with Twitter"><i class="uk-icon-twitter uk-margin-remove"></i></a></div>
                        <div><a href="#" class="md-btn md-btn-block md-btn-gplus" data-uk-tooltip="{pos:'bottom'}" title="Sign in with Google+"><i class="uk-icon-google-plus uk-margin-remove"></i></a></div>
                    </div>-->
                    <div class="uk-margin-top">
                       <!-- <a href="#" id="login_help_show" class="uk-float-right">Fogot password</a>
                        <span class="icheck-inline">
                            <input type="checkbox" name="remember" id="login_page_stay_signed" value="1" data-md-icheck />
                            <label for="login_page_stay_signed" class="inline-label">Remember Me</label>
                        </span> -->
                    </div>
                </form>
            </div>
            <div class="md-card-content large-padding uk-position-relative" id="login_help" style="display: none">
                <button type="button" class="uk-position-top-right uk-close uk-margin-right uk-margin-top back_to_login"></button>
                <h2 class="heading_b uk-text-success">Can't log in?</h2>
                <p>Here’s the info to get you back in to your account as quickly as possible.</p>
                <p>First, try the easiest thing: if you remember your password but it isn’t working, make sure that Caps Lock is turned off, and that your username is spelled correctly, and then try again.</p>
                <p>If your password still isn’t working, it’s time to <a href="#" id="password_reset_show">reset your password</a>.</p>
            </div>
            <div class="md-card-content large-padding" id="login_password_reset" style="display: none">
                <button type="button" class="uk-position-top-right uk-close uk-margin-right uk-margin-top back_to_login"></button>
                <h2 class="heading_a uk-margin-large-bottom">Reset password</h2>
                <form>
                    <div class="uk-form-row">
                        <label for="login_email_reset">Your email address</label>
                        <input class="md-input" type="text" id="login_email_reset" name="login_email_reset" />
                    </div>
                    <div class="uk-margin-medium-top">
                        <a href="index.html" class="md-btn md-btn-primary md-btn-block">Reset password</a>
                    </div>
                </form>
            </div>
			 
                            
                        
            
        </div>
      
    </div>

    <script src="<?php echo base_url();?>assets/admin/assets/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="<?php echo base_url();?>assets/admin/assets/js/uikit_custom.min.js"></script>
    <!-- altair common functions/helpers -->
    <script src="<?php echo base_url();?>assets/admin/assets/js/altair_admin_common.min.js"></script>
    
    <script src="<?php echo base_url();?>assets/admin/bower_components/ckeditor/ckeditor.js"></script>
    
    <script type="text/javascript">CKEDITOR_BASEPATH = '<?php echo base_url();?>assets/admin/bower_components/ckeditor/';</script>
      <!-- altair login page functions -->
    <script src="<?php echo base_url();?>assets/admin/assets/js/pages/login.min.js"></script>
	<script src="<?php echo base_url();?>assets/admin/assets/js/script.js"></script>

  </body>
</html>