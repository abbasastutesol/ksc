<div id="page_content">
   <div id="page_content_inner">
      <!-- statistics (small charts) -->            
      <div class="uk-grid uk-grid-width-large-1-4 uk-grid-width-medium-1-2 uk-grid-medium uk-sortable sortable-handler hierarchical_show" data-uk-sortable data-uk-grid-margin>
         <div>
            <div class="md-card">
               <div class="md-card-content">
                  <div class="uk-float-right uk-margin-top uk-margin-small-right">                             
				  <a href="<?php echo base_url();?>admin/orders/">                                  
				  <span class="menu_icon"><i class="material-icons"></i></span>                
				  </a>                         
				  </div>
                  <span class="uk-text-muted uk-text-small">Orders</span>                            
                  <h2 class="uk-margin-remove">
                     <span class="countUpMe">

                        <noscript><?php echo sizeof($total_orders);?></noscript>
                     </span>
                  </h2>
               </div>
            </div>
         </div>
         <div>
            <div class="md-card">
               <div class="md-card-content">
                  <div class="uk-float-right uk-margin-top uk-margin-small-right">
                      <a href="<?php echo base_url();?>admin/orders">                                   
					  <span class="menu_icon"><i class="material-icons">autorenew</i></span>                
					  </a>                        
					  </div>
                  <span class="uk-text-muted uk-text-small">Pending Return Request</span>                            
                  <h2 class="uk-margin-remove">
                     <span class="countUpMe">

                        <noscript><?php if(!$return_orders) echo 0; else echo count($return_orders); ?></noscript>
                     </span>
                  </h2>
               </div>
            </div>
         </div>
         <div>
            <div class="md-card">
               <div class="md-card-content">
                  <div class="uk-float-right uk-margin-top uk-margin-small-right">                   
				  <a href="<?php echo base_url();?>admin/users/">                            
				  <span class="menu_icon"><i class="material-icons">people</i></span>        
				  </a>               
				  </div>
                  <span class="uk-text-muted uk-text-small">Registered Customers</span>                            
                  <h2 class="uk-margin-remove">
                     <span class="countUpMe">
                        <noscript><?php echo count($customers);?></noscript>
                     </span>
                  </h2>
               </div>
            </div>
         </div>
         <div>
            <div class="md-card">
               <div class="md-card-content">
                  <div class="uk-float-right uk-margin-top uk-margin-small-right">                 
				  <a href="<?php echo base_url();?>admin/product/lowStockProducts/">          
				  <span class="menu_icon"><i class="material-icons">pie_chart</i></span>       
				  </a>                    
				  </div>
                  <span class="uk-text-muted uk-text-small">Low Stock Products</span>                            
                  <h2 class="uk-margin-remove">
                     <span class="countUpMe">
                        <noscript><?php echo $lowStock;?></noscript>
                     </span>
                  </h2>
               </div>
            </div>
         </div>
      </div>
      <div class="uk-grid" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
         <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>				
         <div class="uk-width-medium-1-2">
            <div class="md-card">
               <div class="md-card-content">
                  <h3 class="heading_a uk-margin-bottom">Orders</h3>
                  <a href="javascript:void(0);" class="active all_types_orders" dir="chart_div_order_weekly"><span class="uk-badge">Weekly</span></a>                   
				  <a href="javascript:void(0);" class="all_types_orders" dir="chart_div_order_monthly"><span class="uk-badge">Monthly</span></a>                            <a href="javascript:void(0);" class="all_types_orders" dir="chart_div_order_yearly"><span class="uk-badge">Yearly</span></a>                            
                  <div id="chart_div_order_weekly" class="hide_all_order" style="display:block;"></div>
                  <script type="text/javascript">		
				  google.charts.load('current', {'packages':['corechart']});						
				  google.charts.setOnLoadCallback(drawChartWeekOrder);						
				  function drawChartWeekOrder() {							
				  var data = google.visualization.arrayToDataTable([		
				  ['Date', 'All', 'Pending', 'Completed', 'Cancelled'],		
				  <?php echo $weeklyOrderChart;?>						
				  ]);													
				  var options = {									
				  hAxis: {										
				  title: 'Date'									
				  },									
				  vAxis: {								
				  title: 'Status'						
				  },									
				  legend: { position: 'top' },				
				  pointSize: 5							
				  };												
				  var chart = new google.visualization.LineChart(document.getElementById('chart_div_order_weekly'));	
				  chart.draw(data, options);				
				  }							
				  </script>                                                                
                  <div id="chart_div_order_monthly" class="hide_all_order" style="display:block;"></div>
                  <script type="text/javascript">								
				  google.charts.load('current', {'packages':['corechart']});	
				  google.charts.setOnLoadCallback(drawChartWeekOrder);		
				  function drawChartWeekOrder() {							
				  var data = google.visualization.arrayToDataTable([		
				  ['Date', 'All', 'Pending', 'Completed', 'Cancelled'],	
				  <?php echo $monthlyOrderChart;?>						
				  ]);												
				  var options = {									
				  hAxis: {											
				  title: 'Date'										
				  },											
				  vAxis: {										
				  title: 'Status'								
				  },											
				  legend: { position: 'top' },					
				  pointSize: 5								
				  };										
				  var chart = new google.visualization.LineChart(document.getElementById('chart_div_order_monthly'));	
				  chart.draw(data, options);					
				  document.getElementById("chart_div_order_monthly").style.display = "none";		
				  }							
				  </script>                                                                                                
                  <div id="chart_div_order_yearly" class="hide_all_order" style="display:block;"></div>
                  <script type="text/javascript">						
				  google.charts.load('current', {'packages':['corechart']});	
				  google.charts.setOnLoadCallback(drawChartWeekOrder);		
				  function drawChartWeekOrder() {							
				  var data = google.visualization.arrayToDataTable([		
				  ['Date', 'All', 'Pending', 'Completed', 'Cancelled'],		
				  <?php echo $yearlyOrderChart;?>					
				  
				  ]);												
				  var options = {										
				  hAxis: {											
				  title: 'Date'								
				  },										
				  vAxis: {							
				  title: 'Status'					
				  },								
				  legend: { position: 'top' },		
				  pointSize: 5						
				  };									
				  var chart = new google.visualization.LineChart(document.getElementById('chart_div_order_yearly'));			
				  chart.draw(data, options);												
				  document.getElementById("chart_div_order_yearly").style.display = "none";		
				  }							
				  </script>                                                                              
               </div>
            </div>
         </div>
         <div class="uk-width-medium-1-2">
            <div class="md-card">
               <div class="md-card-content">
                  <h3 class="heading_a uk-margin-bottom">Registered Users</h3>
                  <a href="javascript:void(0);" class="active all_types" dir="chart_div_week"><span class="uk-badge">Weekly</span></a>                            <a href="javascript:void(0);" class="all_types" dir="chart_div_month"><span class="uk-badge">Monthly</span></a>                            <a href="javascript:void(0);" class="all_types" dir="chart_div_year"><span class="uk-badge">Yearly</span></a>                             
                  <div id="chart_div_week" class="hide_all" style="display:block;"></div>
                  <script type="text/javascript">								  google.charts.load('current', {'packages':['corechart']});								  google.charts.setOnLoadCallback(drawChartWeek);															  function drawChartWeek() {									var data = google.visualization.arrayToDataTable([									  ['Date', 'Users'],									  <?php echo $weeklyChart;?>									]);																var options = {										hAxis: {												  title: 'Date'												},												vAxis: {												  title: 'Users'												},												legend: { position: 'top' },												pointSize: 5									};																var chart = new google.visualization.LineChart(document.getElementById('chart_div_week'));																chart.draw(data, options);								  }								</script>                                                                
                  <div id="chart_div_month" class="hide_all" style="display:none;"></div>
                  <script type="text/javascript">								  google.charts.load('current', {'packages':['corechart']});								  google.charts.setOnLoadCallback(drawChartMonth);															  function drawChartMonth() {									var data = google.visualization.arrayToDataTable([									  ['Date', 'Users'],									  <?php echo $monthlyChart;?>									]);																var options = {										hAxis: {												  title: 'Date'												},												vAxis: {												  title: 'Users'												},												legend: { position: 'top' },												pointSize: 5									};																var chart = new google.visualization.LineChart(document.getElementById('chart_div_month'));																chart.draw(data, options);									document.getElementById("chart_div_month").style.display = "none";								  }								</script>                                                                
                  <div id="chart_div_year" class="hide_all" style="display:none;"></div>
                  <script type="text/javascript">								  google.charts.load('current', {'packages':['corechart']});								  google.charts.setOnLoadCallback(drawChartYear);															  function drawChartYear() {									var data = google.visualization.arrayToDataTable([									  ['Date', 'Users'],									  <?php echo $yearlyChart;?>									]);																var options = {										hAxis: {												  title: 'Date'												},												vAxis: {												  title: 'Users'												},												legend: { position: 'top' },												pointSize: 5									};																var chart = new google.visualization.LineChart(document.getElementById('chart_div_year'));																chart.draw(data, options);									document.getElementById("chart_div_year").style.display = "none";								  }								</script>                        
               </div>
            </div>
         </div>
      </div>
      <!-- tasks -->            
      <div class="uk-grid" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
         <div class="uk-width-medium-1-2">
            <div class="md-card">
               <div class="md-card-content">
                  <h3 class="heading_a uk-margin-bottom">Orders</h3>
                  <div class="uk-overflow-container">
                     <table class="uk-table">
                        <thead>
                           <tr>
                              <th class="uk-text-nowrap">Order Status</th>
                              <th class="uk-text-nowrap">Today</th>
                              <th class="uk-text-nowrap">This Week</th>
                              <th class="uk-text-nowrap">This Month</th>
                              <th class="uk-text-nowrap">This Year</th>
                              <th class="uk-text-nowrap">All Time</th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr class="uk-table-middle">
                              <td class="uk-width-2-10 uk-text-nowrap"><span class="uk-badge uk-badge-warning">Pending</span></td>
                              <?php
                              $today = 0;
                              $this_week = 0;
                              $this_month = 0;
                              $this_year = 0;
                              $all_total = 0;
                              foreach($orders_total as $ord)
                              {
                                 if($ord['order_status'] > 0 && $ord['order_status'] <= 4)
                                 {
                                    if($ord['period'] == 'Today')
                                    {
                                       $today += $ord['total'];
                                    }
                                    if($ord['period'] == 'This Week')
                                    {
                                       $this_week += $ord['total'];
                                    }
                                    if($ord['period'] == 'This Month')
                                    {
                                       $this_month += $ord['total'];
                                    }
                                    if($ord['period'] == 'This Year')
                                    {
                                       $this_year += $ord['total'];
                                    }
                                    if($ord['period'] == 'All Total')
                                    {
                                       $all_total += $ord['total'];
                                    }
                                 }
                              }

                              ?>

                              <td><?php echo $today.' '.getCurrency();?></td>
                              <td><?php echo $this_week.' '.getCurrency();?></td>
                              <td><?php echo $this_month.' '.getCurrency();?></td>
                              <td><?php echo $this_year.' '.getCurrency();?></td>
                              <td><?php echo $all_total.' '.getCurrency();?></td>
                           </tr>
                           <tr class="uk-table-middle">
                              <td class="uk-width-2-10 uk-text-nowrap"><span class="uk-badge uk-badge-success">Completed</span></td>
                              <?php
                              $today = 0;
                              $this_week = 0;
                              $this_month = 0;
                              $this_year = 0;
                              $all_total = 0;
                              foreach($orders_total as $ord)
                              {
                                 if($ord['order_status'] == 6)
                                 {
                                    if($ord['period'] == 'Today')
                                    {
                                       $today += $ord['total'];
                                    }
                                    if($ord['period'] == 'This Week')
                                    {
                                       $this_week += $ord['total'];
                                    }
                                    if($ord['period'] == 'This Month')
                                    {
                                       $this_month += $ord['total'];
                                    }
                                    if($ord['period'] == 'This Year')
                                    {
                                       $this_year += $ord['total'];
                                    }
                                    if($ord['period'] == 'All Total')
                                    {
                                       $all_total += $ord['total'];
                                    }
                                 }
                              }
                              ?>

                              <td><?php echo $today.' '.getCurrency();?></td>

                              <td><?php echo $this_week.' '.getCurrency();?></td>
                              <td><?php echo $this_month.' '.getCurrency();?></td>
                              <td><?php echo $this_year.' '.getCurrency();?></td>
                              <td><?php echo $all_total.' '.getCurrency();?></td>
                           </tr>
                           <tr class="uk-table-middle">
                              <td class="uk-width-2-10 uk-text-nowrap"><span class="uk-badge uk-badge-danger">Cancelled</span></td>
                              <?php
                              $today = 0;
                              $this_week = 0;
                              $this_month = 0;
                              $this_year = 0;
                              $all_total = 0;
                              foreach($orders_total as $ord)
                              {
                                 if($ord['order_status'] == 0)
                                 {
                                    if($ord['period'] == 'Today')
                                    {
                                       $today += $ord['total'];
                                    }
                                    if($ord['period'] == 'This Week')
                                    {
                                       $this_week += $ord['total'];
                                    }
                                    if($ord['period'] == 'This Month')
                                    {
                                       $this_month += $ord['total'];
                                    }
                                    if($ord['period'] == 'This Year')
                                    {
                                       $this_year += $ord['total'];
                                    }
                                    if($ord['period'] == 'All Total')
                                    {
                                       $all_total += $ord['total'];
                                    }
                                 }
                              }
                              ?>

                              <td><?php echo $today.' '.getCurrency();?></td>


                              <td><?php echo $this_week.' '.getCurrency();?></td>
                              <td><?php echo $this_month.' '.getCurrency();?></td>
                              <td><?php echo $this_year.' '.getCurrency();?></td>
                              <td><?php echo $all_total.' '.getCurrency();?></td>
                           </tr>
                           <tr class="uk-table-middle">
                              <td class="uk-width-2-10 uk-text-nowrap"><span class="uk-badge uk-badge-info">Returned</span></td>

                               <?php
                               $today = 0;
                               $this_week = 0;
                               $this_month = 0;
                               $this_year = 0;
                               $all_total = 0;
                               foreach($orders_total as $ord)
                               {
                                   if($ord['order_status'] == 8)
                                   {
                                       if($ord['period'] == 'Today')
                                       {
                                           $today += $ord['total'];
                                       }
                                       if($ord['period'] == 'This Week')
                                       {
                                           $this_week += $ord['total'];
                                       }
                                       if($ord['period'] == 'This Month')
                                       {
                                           $this_month += $ord['total'];
                                       }
                                       if($ord['period'] == 'This Year')
                                       {
                                           $this_year += $ord['total'];
                                       }
                                       if($ord['period'] == 'All Total')
                                       {
                                           $all_total += $ord['total'];
                                       }
                                   }
                               }
                               ?>

                              <td><?php echo $today.' '.getCurrency();?></td>
                              <td><?php echo $this_week.' '.getCurrency();?></td>
                              <td><?php echo $this_month.' '.getCurrency();?></td>
                              <td><?php echo $this_year.' '.getCurrency();?></td>
                              <td><?php echo $all_total.' '.getCurrency();?></td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="uk-grid" data-uk-grid-margin data-uk-grid-match="{target:'.md-card-content'}">
         <div class="uk-width-medium-1-1">
            <div class="md-card">
               <div class="md-card-content">
                  <h3 class="heading_a uk-margin-bottom">Latest Orders</h3>
                  <a href="<?php echo base_url();?>admin/orders/"><span class="uk-badge">List Orders</span></a>                            
                  <div class="uk-overflow-container">
                     <table class="uk-table uk-table-align-vertical listing dt_default">
                        <thead>
                           <tr>
                              <th class="uk-text-nowrap">Order Id</th>
                              <th class="uk-text-nowrap">Order Status</th>
                              <th class="uk-text-nowrap">Customer</th>
                              <th class="uk-text-nowrap">Created On</th>
                              <th class="uk-text-nowrap nosort">View</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php foreach($latestOrder as $order){?>                                        	
                           <tr class="uk-table-middle">
                              <td><?php echo str_pad($order['id'], 6, "0", STR_PAD_LEFT);?></td>
                              <td class="uk-width-2-10 uk-text-nowrap">
                                  <?php

                                  if ($order['order_status'] == 1) {
                                      $order_status = 'Prepare Order';
                                      $badge = 'uk-badge-info';

                                  } elseif ($order['order_status'] == 2) {
                                      $order_status = 'Verify Payment';
                                      $badge = 'uk-badge-info';

                                  } elseif ($order['order_status'] == 3) {
                                      $order_status = 'Prepare Shipment';
                                      $badge = 'uk-badge-info';

                                  } elseif ($order['order_status'] == 4) {
                                      $order_status = 'Shipped';
                                      $badge = 'uk-badge-success';

                                  }elseif ($order['order_status'] == 5) {
                                      $order_status = 'Delivered ';
                                      $badge = 'uk-badge-success';

                                  }elseif ($order['order_status'] == 6) {
                                      $order_status = 'Completed ';
                                      $badge = 'uk-badge-success';

                                  }elseif ($order['order_status'] == 7) {
                                      $order_status = 'Return Request ';
                                      $badge = 'uk-badge-warning';


                                  }elseif ($order['order_status'] == 8) {
                                      $order_status = 'Returned ';
                                      $badge = 'uk-badge-warning';

                                  }elseif ($order['order_status'] == 9) {
                                      $order_status = 'Rejected ';
                                      $badge = 'uk-badge-danger';

                                  }elseif ($order['order_status'] == 10) {
                                      $order_status = 'Cancelled Request ';
                                      $badge = 'uk-badge-danger';


                                  }
                                  else {
                                      $order_status = 'Cancelled ';
                                      $badge = 'uk-badge-danger';

                                  }
                                  ?>



                                      <span class="uk-badge <?php echo $badge; ?>"><?php echo $order_status; ?></span>

                              </td>
                              <td>
                                  <?php

                                  $address = getCartAddress($order['address_id']);
                                  $user = getUserById($order['user_id']);

                                  if (!$user) {
                                      $fullName = $address->full_name;
                                      $email = $address->email;
                                  } else {
                                      $fullName = $user['first_name'] . " " . $user['last_name'];
                                      $email = $user['email'];
                                  }

                                  echo $fullName.'<br />('.$email.')';?></td>
                              <td><?php echo date('d M Y', strtotime($order['created_at']));?></td>
                              <td><a href="<?php echo base_url();?>admin/orders/order_details/<?php echo $order['id']?>"><i class="md-icon material-icons">visibility</i></a></td>
                           </tr>
                           <?php }?>                                    
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>