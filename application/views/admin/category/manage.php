 <!-- Content Wrapper. Contains page content -->
      <div id="page_content">
        <div id="page_content_inner">

            <?php if(viewEditDeleteRights('Category','add')) { ?>
            <a href="<?php echo base_url();?>admin/category/add" class="md-btn" > Add</a>
            <?php } ?>
			<div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <div class="uk-overflow-container"> 
                                <table class="uk-table uk-table-align-vertical listing dt_default_cat">
                                    <thead>
                                        <tr>

                                              <th class="nosort">Category</th>
                      
                                              <th>Active</th>
                                            <?php if(viewEditDeleteRights('Category','edit') || viewEditDeleteRights('Category','delete')) { ?>
                                              <th class="nosort">Action</th>
                                            <?php } ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php getCategories();?>    
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
       <!-- /.content-wrapper -->

 <style>
     .drag_row{
         cursor: move; /* fallback if grab cursor is unsupported */
         cursor: grab;
         cursor: -moz-grab;
         cursor: -webkit-grab;
     }
 </style>