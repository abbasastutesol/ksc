<div id="page_content">
  <div id="page_content_inner">
    <div class="md-card">
      <div class="md-card-content">
        <h3 class="heading_a">Add a Category</h3>
        <form action="<?php echo base_url(); ?>admin/category/action" method="post" onsubmit="return false" class="ajax_form">
		<div class="uk-grid" data-uk-grid-margin>
		
		<input type="hidden" name="form_type" value="save">
		<div class="uk-width-medium-1-2">
            <select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}" title="Select with tooltip" name="parent_id">
              <option value="">Select Parent Category</option>
			  <option value="0">Top</option>
			  <?php
			  getCategoriesOptionListing();
			  /*if($categories)
			  {
				  foreach($categories as $category)
				  {
					 echo '<option value="'.$category->id.'">'.$category->eng_name.'</option>'; 
				  }
			  }*/
			  ?>
              
            </select>
        </div>
			<div class="uk-width-medium-1-4">
            <input type="checkbox" data-switchery data-switchery-size="large" checked id="switch_demo_large" name="active" />
            <label for="switch_demo_large" class="inline-label">Active</label>
           
			</div> 
          <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
              <label>English Name</label>
              <input type="text" class="md-input" value="" name="eng_name" />
            </div>
			
          </div>
          <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
              <label>Arabic Name</label>
              <input type="text" class="md-input" value="" name="arb_name" />
            </div>
            
          </div>
			 
			
			  <div class="uk-width-large-1-1">
                    <h3 class="heading_a">
                        Upload Category Image
                       
                    </h3><span style="color:red">(330 * 300 pixels)</span>
                    <div class="uk-grid">
                        <div class="uk-width-1-1">
                            <div id="file_upload-drop" class="uk-file-upload">
                                <a class="uk-form-file md-btn">choose file<input id="file_upload-select" class="images_upload" type="file" name="image"></a>
                            </div>
                            <div id="file_upload-progressbar" class="uk-progress uk-hidden">
                                <div class="uk-progress-bar" style="width:0">0%</div>
                            </div>
                        </div>
                    </div>
                </div>
                
                
            <div class="uk-width-medium-1-2">
                <div class="uk-form-row">
                  <label>English Meta Title</label>
                  <input type="text" class="md-input" value="" name="eng_meta_title" />
                </div>
                
              </div>
              <div class="uk-width-medium-1-2">
                <div class="uk-form-row">
                  <label>Arabic Meta Title</label>
                  <input type="text" class="md-input" value="" name="arb_meta_title" />
                </div>
                
              </div>
              
              <div class="uk-width-medium-1-2">
                
                <div class="uk-form-row">
                  <label>Englishn Meta Description</label>
                  <textarea cols="30" rows="4" class="md-input" name="eng_meta_description"></textarea>
                </div>
              </div>
              <div class="uk-width-medium-1-2">
                
                <div class="uk-form-row">
                  <label>Arabic Meta Description</label>
                  <textarea cols="30" rows="4" class="md-input" name="arb_meta_description"></textarea>
                </div>
              </div>
              
              <div class="uk-width-medium-1-2">
                
                <div class="uk-form-row">
                  <label>Englishn Meta Keyword</label>
                  <textarea cols="30" rows="4" class="md-input" name="eng_meta_keyword"></textarea>
                </div>
              </div>
              <div class="uk-width-medium-1-2">
                
                <div class="uk-form-row">
                  <label>Arabic Meta Keyword</label>
                  <textarea cols="30" rows="4" class="md-input" name="arb_meta_keyword"></textarea>
                </div>
              </div>
			
			
	
        </div>
       </form> 
      </div>
    </div>
   
    
    
    
    
  </div>
</div>
<div class="md-fab-wrapper">
        <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">
            <i class="material-icons">&#xE161;</i>
        </a>
    </div>
<div class="md-fab-wrapper" style="right:95px;">
        <a class="md-fab md-fab-primary" href="<?php echo base_url();?>admin/category" id="">
            <i class="material-icons">keyboard_backspace</i>
        </a>
    </div>