<div id="page_content">

  <div id="page_content_inner">

    <div class="md-card">

      <div class="md-card-content">

        <h3 class="heading_a">Edit a Category</h3>

        <form action="<?php echo base_url(); ?>admin/category/action" method="post" onsubmit="return false" class="ajax_form">

		<div class="uk-grid" data-uk-grid-margin>

		

		<input type="hidden" name="form_type" value="update">

		<input type="hidden" name="id" value="<?php echo $category->id;?>">


          <div class="uk-width-medium-1-2">

            <div class="uk-form-row">

              <label>English Name</label>

              <input type="text" class="md-input" value="<?php echo $category->eng_name;?>" name="eng_name" />

            </div>

			

          </div>

          <div class="uk-width-medium-1-2">

            <div class="uk-form-row">

              <label>Arabic Name</label>

              <input type="text" class="md-input" value="<?php echo $category->arb_name;?>" name="arb_name" />

            </div>

            

          </div>
		  <div class="uk-width-medium-1-1">
                     <div class="uk-form-row">              <label>Category Image</label>               <img src="<?php echo base_url().'assets/frontend/images/'.($category->eng_image != '' ? $category->arb_image : 'no_imge.png'); ?>" alt="Category Image" data-uk-modal="{target:'#modal_lightbox'}" width="100">            </div>
                  </div>
<div class="uk-width-large-1-1">
                    <h3 class="heading_a">
                        Upload Category Image
                       
                    </h3><span style="color:red">(330 * 300 pixels)</span>
                    <div class="uk-grid">
                        <div class="uk-width-1-1">
                            <div id="file_upload-drop" class="uk-file-upload">
                                <a class="uk-form-file md-btn">choose file<input id="file_upload-select" class="images_upload" type="file" name="image"></a>
                            </div>
                            <div id="file_upload-progressbar" class="uk-progress uk-hidden">
                                <div class="uk-progress-bar" style="width:0">0%</div>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="uk-width-medium-1-4">

                <?php

                $active_check = '';

                if($category->active == 'On')

                {

                    $active_check = 'checked';

                }

                ?>

                <input type="checkbox" data-switchery data-switchery-size="large" <?php echo $active_check ;?> id="switch_demo_large" name="active" />

                <label for="switch_demo_large" class="inline-label">Active</label>



            </div>

        </div>

       </form> 

      </div>

    </div>

   

   <!-- light box for image -->

		   <div class="uk-modal" id="modal_lightbox">

                                <div class="uk-modal-dialog uk-modal-dialog-lightbox">

                                    <button type="button" class="uk-modal-close uk-close uk-close-alt"></button>

                                    <img src="<?php echo base_url().'assets/frontend/images/'.$category->eng_image; ?>" alt=""/>

                                    

                                </div>

           </div>

			<!-- end light box for image -->   

    

    

    

  </div>

</div>

<div class="md-fab-wrapper">

        <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">

            <i class="material-icons">&#xE161;</i>

        </a>

    </div>

<div class="md-fab-wrapper" style="right:95px;">

        <a class="md-fab md-fab-primary" href="<?php echo base_url();?>admin/category" id="">

            <i class="material-icons">keyboard_backspace</i>

        </a>

    </div>