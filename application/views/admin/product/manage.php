<div id="page_content">

    <div id="page_content_inner">
        <div class="md-card">
            <div class="md-card-content orderPage">
                <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
                    <div class="heading_actions">
                        <ul id="menu_top" class="uk-width-2-6 uk-clearfix">
                            <li data-uk-dropdown class="">
                                <span class="uk-text-upper uk-text-small"><a href="#">Categories</a></span>
                                <h1>All Categories <i class="material-icons">&#xE5C5;</i></h1>
                                <div class="uk-dropdown">
                                    <ul class="parentCatEd uk-nav uk-nav-dropdown">
                                      <?php foreach($categories as $category) { ?>
                                        <li class="hasDropDown">
                                            <a href="javascript:void(0);" onclick="getProductByCat('<?php echo $category->id; ?>');"><?php echo $category->eng_name; ?></a>

                                        </li>
                                      <?php } ?>

                                    </ul>
                                </div>
                            </li>
                            <li data-uk-dropdown class="">
                                <span class="uk-text-upper uk-text-small"><a href="#">Show</a></span>
                                <h1>Available <i class="material-icons">&#xE5C5;</i></h1>
                                <div class="uk-dropdown">
                                    <ul class="parentCatEd uk-nav uk-nav-dropdown">
                                        <li class="hasDropDown">
                                            <a href="javascript:void(0);"onclick="getProductByAvail('1');">Available</a>

                                        </li>
                                        <li class="hasDropDown">
                                            <a href="javascript:void(0);"onclick="getProductByAvail('0');">Not Available</a>

                                        </li>

                                    </ul>

                                </div>
                            </li>
                            </li>
                        </ul>
                        <div class="uk-width-2-3  ">
                            <div class="srchDDED">
                                <div data-uk-dropdown class="dropDownED">
                                    <div class="uk-dropdown uk-dropdown-small">
                                        <ul class="uk-nav">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Other Action</a></li>
                                            <li><a href="#">Other Action</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="serchInpEd uk-width-medium-1-2">
                                    <label for="contact_list_search">Search</label>
                                    <input class="md-input product_search" type="text" id="contact_list_search" name="search"/>
                                </div>

                                <form class="product_search_form" action="<?php echo base_url().'admin/product'; ?>" method="post" style="display: none;">
                                    <input type="hidden" name="product_search_value" class="product_search_value">
                                    <button type="submit"></button>
                                </form>

                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="proTopBtns">
                    <div class="md-card-content">
                        <a href="javascript:void(0);" class="gridBtn" id="gridBtn">
                            <i class="material-icons">apps</i>
                            Switch to Grid View
                        </a>
                        <a href="javascript:void(0);" class="listBtn" id="listBtn">
                            <i class="material-icons">menu</i>
                            Switch to List View
                        </a>
                        <?php if(viewEditDeleteRights('Products','add')) { ?>
                            <a href="<?php echo base_url().'admin/product/add'; ?>" class="add_product_btn"><button class="btn ioudBtn green uk-float-right">Add</button></a>
                        <?php } ?>
                        <button class="btn ioudBtn red uk-float-right" id="cancel">Cancel</button>
                        <!--<button class="btn ioudBtn green uk-float-right" id="massDisBtn">Mass Discount</button>-->
                        <button class="btn ioudBtn green uk-float-right" id="applyBtn">Apply</button>
                        <button class="btn ioudBtn green uk-float-right" id="massDiscountBtn" data-uk-modal="{target:'#applyDiscount'}" style="display: none;">Apply</button>
                        <div class="clearfix"></div>
                        <div class="uk-modal" id="applyDiscount">
                            <div class="uk-modal-dialog">
                                <div class="uk-modal-header">
                                    <h3 class="uk-modal-title">Mass Discount</h3>
                                    <button class="uk-modal-close uk-close closeBtnEd" type="button">&nbsp;</button>
                                </div>
                                <form action="<?php echo base_url().'admin/product/action'; ?>" method="post" class="discount_form" onsubmit="return false;">
                                <input type="hidden" name="form_type" value="discount">
                                <input type="hidden" name="product_ids" value="" id="product_ids">
                                    <div class="uk-grid " data-uk-grid-margin>
                                    <div class="uk-width-medium-4-10">
                                        <label>Discount Type</label>
                                    </div>
                                    <div class="uk-width-medium-6-10">
                                        <select class="uk-form-width-full" name="discount_type" id="task_assignee" data-md-selectize-inline>
                                            <option value="">Select</option>
                                            <option value="percentage">Percentage</option>
                                            <option value="amount">Amount</option>
                                        </select>
                                    </div>
                                </div>
                                    
                                <div class="uk-grid " data-uk-grid-margin>
                                    <div class="uk-width-medium-4-10">
                                        <label>Discount figure</label>
                                    </div>
                                    <div class="uk-width-medium-6-10">
                                        <input class="md-input mass_discount_field" name="discount_amount" type="text" placeholder="Write" />
                                    </div>
                                </div>
                                <div class="uk-grid " data-uk-grid-margin>                                    
                                    <div class="uk-width-medium-10-10 uk-text-center">
                                        <button type="submit" class="btn ioudBtn green">Apply</button>
                                    </div>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="proGridView">
                    <div class="md-card-content">
                        <div class="uk-grid-width-small-1-2 uk-grid-width-medium-1-3 uk-grid-width-large-1-4 uk-margin-large-bottom hierarchical_show" data-uk-grid="{gutter: 20}">

                            <?php
                            $no_record = false;

                            if($products) {
                                $no_record = true;

                                foreach ($products as $product) {

                                    $prod_image =  getProductImages($product->id);
                                    //echo "<pre>"; print_r($product);
                                    foreach($prod_image as $key => $prod_img){
                                        if($prod_img['is_thumbnail'] == 1){
                                            $thumbnail = $prod_img['thumbnail'];
                                        }
                                    }

                                    ?>
                                    <div class="forIndex">
                                        <div class="md-card">
                                            <div class="singProDisp md-card md-card-hover">
                                                <div class="gallery_grid_item md-card-content">
                                                    <div class="gallery_grid_image_caption">

                                                        <?php if(viewEditDeleteRights('Products','edit') || viewEditDeleteRights('Products','delete')) { ?>
                                                        <div class="gallery_grid_image_menu"
                                                             data-uk-dropdown="{pos:'bottom-right'}">
                                                            <i class="md-icon material-icons">&#xE5D4;</i>
                                                            <div class="uk-dropdown uk-dropdown-small">


                                                                <ul class="uk-nav">
                                                                    <?php if(viewEditDeleteRights('Products','edit')) { ?>
                                                                    <li><a href="<?php echo base_url().'admin/product/edit/'.$product->id;?>"><i
                                                                                    class="material-icons uk-margin-small-right">
                                                                                &#xE150;</i> Edit</a>
                                                                    </li>
                                            <?php } ?>
                                            <?php if(viewEditDeleteRights('Products','delete')) { ?>
                                                                    <li><a href="#" onclick="deleteRecord(<?php echo $product->id;?>,'admin/product/action','');"><i
                                                                                    class="material-icons uk-margin-small-right">
                                                                                &#xE872;</i> Remove</a>
                                                                    </li>
                                            <?php } ?>
                                                                </ul>


                                                            </div>

                                                        </div>

                                                        <?php } ?>
                                            <?php if(viewEditDeleteRights('Products','view')) {
                                                $href = base_url().'admin/product/detail/'.$product->id;
                                            }else{
                                                $href = 'javascript:void(0);';
                                            } ?>


                                                        <a href="<?php echo $href; ?>"><span class="gallery_image_title big uk-text-truncate"
                                                                                                       title="<?php echo ucfirst($product->eng_name); ?>"><?php echo ucfirst($product->eng_name); ?></span></a>
                                                        <span class="uk-text-small"><?php
                                                            $cats = getCategoriesById($product->category_id);

                                                            echo $cats['eng_name'];
                                                            ?></span>
                                                    </div>
                                                    <a class="proImg">
                                                        <img src="<?php echo base_url(); ?>uploads/images/thumbs/products/<?php echo $thumbnail; ?>" alt="">
                                                    </a>
                                                    <div class="md-card-toolbar">
                                                        <div class="md-card-toolbar-actions">
                                                            <!--<i class="md-icon material-icons green">question_answer</i>
                                                            <i class="md-icon material-icons red">pan_tool</i>-->
                                                    <?php if(viewEditDeleteRights('Products','delete')) { ?>
                                                            <i class="md-icon material-icons black" onclick="deleteRecord(<?php echo $product->id;?>,'admin/product/action','');">delete</i>
                                                    <?php } ?>
                                                        </div>
                                                        <h3 class="md-card-toolbar-heading-text">
                                                            <?php if($product->active == 0){ ?>
                                                                <i class="md-icon material-icons black">highlight_off</i>

                                                            <?php }else{ ?>
                                                                <i class="md-icon material-icons green">check_circle</i>
                                                            <?php } ?>
                                                        </h3>
                                                    </div>
                                                    <div class="gallery_grid_image_caption">
                                                       <!-- <span class="gallery_image_title uk-text-truncate">Sub products: <span
                                                                    class="green">13</span></span>-->
                                                        <span class="gallery_image_title uk-text-truncate">Availability: <span
                                                                    class="green"><?php echo $product->eng_quantity;?> Pcs</span></span>
                                                    </div>
                                                    <div class="proPriceFoot">
                                                        <h3><?php echo number_format($product->eng_price,'2','.',''); ?> SR.</h3>
                                                       <!-- <h4>Featured</h4>-->
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class="proSelection get_product_id" data-prod_id="" id="<?php echo $product->id; ?>"><a href="javascript:void(0);" class=""></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                <?php }
                            }?>
                            
                        </div>
                        <?php if(!$no_record) { ?>
                        <div style="color: red; font-size: large; text-align: center;"><span>Record not found!</span></div>
                        <?php } ?>
                    </div>

                    <?php if($products && $count > 8) { ?>

                        <div class="uk-grid uk-grid-small proFootListing" data-uk-grid-margin="" data-uk-grid-match="{target:'.md-card'}">


                            <div class="uk-width-medium-5-10 uk-row-first">
                                Show entries
                                <select class="product_limit" id="select_demo_4" data-md-selectize>

                                    <option value="8" <?php if($limit == "8") { echo 'selected'; } ?>>8</option>
                                    <option value="16" <?php if($limit == "16") { echo 'selected'; } ?>>16</option>
                                    <option value="24" <?php if($limit == "24") { echo 'selected'; } ?>>24</option>
                                    <option value="32" <?php if($limit == "32") { echo 'selected'; } ?>>32</option>
                                    <option value="40" <?php if($limit == "40") { echo 'selected'; } ?>>40</option>
                                    <option value="48" <?php if($limit == "48") { echo 'selected'; } ?>>48</option>
                                    <option value="56" <?php if($limit == "56") { echo 'selected'; } ?>>56</option>
                                    <option value="64" <?php if($limit == "64") { echo 'selected'; } ?>>64</option>
                                    <option value="72" <?php if($limit == "72") { echo 'selected'; } ?>>72</option>
                                    <option value="80" <?php if($limit == "80") { echo 'selected'; } ?>>80</option>
                                </select>
                            </div>

                            <?php echo $links; ?>
                            <!--<div class="uk-width-medium-5-10">
                                <ul class="uk-pagination uk-text-right">
                                    <li class="uk-disabled"><span>Previous</span></li>
                                    <li class="uk-active"><span>1</span></li>
                                    <li><span>2</span></li>
                                    <li><a href="#">Next</a></li>
                                </ul>
                            </div>-->
                        </div>
                    <?php } ?>


                </div >
                <div class="proListView">
                    <div class="md-card-content">
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-1-1">
                                <div class="uk-overflow-container">
                                    <table class="uk-table uk-table-align-vertical">
                                        <thead>
                                        <tr>
                                            <th>Image</th>
                                            <th>Product Name</th>
                                            <th>Price</th>
                                            <!--<th>Sub products</th>-->
                                            <th>Availability</th>
                                            <th>Active</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                      <?php
                                      foreach ($products as $product) {

                                          $prod_image =  getProductImages($product->id);

                                          foreach($prod_image as $key => $prod_img){
                                              if($prod_img['is_thumbnail'] == 1){
                                                  $thumbnail = $prod_img['thumbnail'];
                                              }
                                          }
                                          ?>

                                        <tr>
                                            <td>
                                                <img class="img_thumb" src="<?php echo base_url(); ?>uploads/images/thumbs/products/<?php echo $thumbnail; ?>" alt="">
                                                <div class="proSelection get_product_id" data-prod_id="" id="<?php echo $product->id; ?>"><a href="javascript:void(0);" class=""></a></div>
                                            </td>
                                            <td class="uk-text-large uk-text-nowrap">
                                                <a href="ecommerce_product_details.html" class="title"><?php echo $product->eng_name; ?></a>
                                                <a href="ecommerce_product_details.html" class="Category"><?php
                                                    $cats = getCategoriesById($product->category_id);

                                                    echo $cats['eng_name'];
                                                ?></a>
                                                <div class="proSelection"><a href="javascript:void(0);" class=""></a></div>
                                            </td>
                                            <td class="uk-text-nowrap">
                                                SR <?php echo number_format($product->eng_price,'2','.',''); ?>
                                                <div class="proSelection"><a href="javascript:void(0);" class=""></a></div>
                                            </td>
                                            <!--<td>
                                                <?php /*echo $product->total_quantity; */?>
                                                <div class="proSelection"><a href="javascript:void(0);" class=""></a></div>
                                            </td>-->
                                            <td class="uk-text-nowrap">
                                                <?php echo $product->total_quantity; ?> Pcs
                                                <div class="proSelection"><a href="javascript:void(0);" class=""></a></div>
                                            </td>
                                            <td>
                                                <?php if($product->active == 0){ ?>
                                                    <i class="md-icon material-icons black">highlight_off</i>
                                                <?php }else{ ?>
                                                    <i class="md-icon material-icons green">check_circle</i>
                                                <?php } ?>
                                                <div class="proSelection"><a href="javascript:void(0);" class=""></a></div>
                                            </td>
                                            <td class="uk-text-nowrap">
                                                <a href="<?php echo base_url().'admin/product/edit/'.$product->id;?>">
                                                <i class="md-icon material-icons green">mode_edit</i>
                                                </a>
                                                <i class="md-icon material-icons black" onclick="deleteRecord(<?php echo $product->id;?>,'admin/product/action','');">delete</i>
                                                <div class="proSelection"><a href="javascript:void(0);" class=""></a></div>

                                            </td>
                                        </tr>


                                        <?php } ?>


                                        </tbody>
                                    </table>
                                </div>
                                <?php if($products && $count > 8) { ?>

                                    <div class="uk-grid uk-grid-small proFootListing" data-uk-grid-margin="" data-uk-grid-match="{target:'.md-card'}">


                                        <div class="uk-width-medium-5-10 uk-row-first">
                                            Show entries
                                            <select class="product_limit" id="select_demo_4" data-md-selectize>

                                                <option value="8" <?php if($limit == "8") { echo 'selected'; } ?>>8</option>
                                                <option value="16" <?php if($limit == "16") { echo 'selected'; } ?>>16</option>
                                                <option value="24" <?php if($limit == "24") { echo 'selected'; } ?>>24</option>
                                                <option value="32" <?php if($limit == "32") { echo 'selected'; } ?>>32</option>
                                                <option value="40" <?php if($limit == "40") { echo 'selected'; } ?>>40</option>
                                                <option value="48" <?php if($limit == "48") { echo 'selected'; } ?>>48</option>
                                                <option value="56" <?php if($limit == "56") { echo 'selected'; } ?>>56</option>
                                                <option value="64" <?php if($limit == "64") { echo 'selected'; } ?>>64</option>
                                                <option value="72" <?php if($limit == "72") { echo 'selected'; } ?>>72</option>
                                                <option value="80" <?php if($limit == "80") { echo 'selected'; } ?>>80</option>
                                            </select>
                                        </div>

                                        <?php echo $links; ?>
                                        <!--<div class="uk-width-medium-5-10">
                                            <ul class="uk-pagination uk-text-right">
                                                <li class="uk-disabled"><span>Previous</span></li>
                                                <li class="uk-active"><span>1</span></li>
                                                <li><span>2</span></li>
                                                <li><a href="#">Next</a></li>
                                            </ul>
                                        </div>-->
                                    </div>
                                <?php } ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
