<div id="page_content">
    <form action="<?php echo base_url(); ?>admin/product/action" method="post" onsubmit="return false" class="uk-form-stacked ajax_form" id="product_edit_form">
        <input type="hidden" name="form_type" value="save">

        <div id="page_content_inner">

            <div class="md-card-content orderPage ordDtlPgEd">
                <div class="orderListBox">
                    <div class="singleOrder noBorder">
                        <div class="orderExpand uk-child"  style="display: block;" >
                            <div class="uk-grid ordSingDtlEdHead" data-uk-grid-margin data-uk-grid-match="{target:'.md-card'}">
                                <div class="uk-width-medium-5-10">
                                    <a href="<?php echo base_url().'admin/product'; ?>">
                                        <i class="material-icons">keyboard_arrow_left</i>
                                        Go back
                                    </a>
                                </div>

                            </div>
                            <div class="productDtlSec">
                                <div class=" uk-grid uk-grid-medium" data-uk-grid-margin>
                                    <div class="uk-width-xLarge-2-10 uk-width-large-3-10">
                                        <div class="uk-margin-bottom uk-text-center">
                                            <div class="uk-slidenav-position proSliderBox" data-uk-slideshow="{animation:'scale'}">

                                                <div class="uk-form-file uploadImages">
                                                    <button class="btn ioudBtn green" type="button">
                                                        <i class="material-icons">crop_original</i> Upload images
                                                    </button>
                                                    <input id="form-file" type="file" name="image[]" multiple>
                                                </div>

                                            </div>

                                            <button class="uk-form-file btn ioudBtn blue">
                                                <i class="material-icons">crop_original</i> Upload Thumbnail
                                                <input id="form-file" name="thumbnail[]" type="file" >
                                            </button>

                                            <button class="uk-form-file btn ioudBtn blue">
                                                <i class="material-icons">crop_original</i> Upload English PDF
                                                <input id="form-file" name="eng_pdf" type="file" accept="application/pdf">
                                            </button>

                                            <button class="uk-form-file btn ioudBtn blue">
                                                <i class="material-icons">crop_original</i> Upload arabic PDF
                                                <input id="form-file" name="arb_pdf" type="file"  accept="application/pdf" >
                                            </button>


                                        </div>
                                    </div>
                                    <div class="uk-width-xLarge-8-10  uk-width-large-7-10">
                                        <div class="uk-grid uk-grid-medium">
                                            <div class="uk-width-10-10">
                                                <div class="inlineBlock">
                                                    <label for="title_eng">Title goes here</label>
                                                    <input type="text" class="md-input" id="title_eng" name="eng_name" value=""/>
                                                </div>
                                                <h1 class="inlineBlock sepr">|</h1>
                                                <div class="inlineBlock">
                                                    <label for="title_arb">العنوان يذهب هنا</label>
                                                    <input type="text" class="md-input" id="title_arb" name="arb_name" value=""/>
                                                </div>

                                                <div class="inlineBlock">
                                                    <label for="priceTxt">Price</label>
                                                    <input type="text" class="md-input" id="priceTxt" name="eng_price" value=""/>
                                                </div>

                                                <div class="inlineBlock">
                                                    <label for="eng_quantity">Quantity</label>
                                                    <input type="text" class="md-input" id="eng_quantity" name="eng_quantity" value=""/>
                                                </div>
                                                <div class="clearfix"></div>

                                                <div>
                                                    <br />

                                                    <select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}" title="Select with tooltip" name="category_id">

                                                        <option value="">Select Category</option>

                                                        <?php
                                                        foreach ($categories as $category){ ?>

                                                            <option value="<?php echo $category->id; ?>"><?php echo $category->eng_name; ?></option>
                                                        <?php }

                                                        ?>

                                                    </select>
                                                </div>

                                                <div>
                                                    <br />


                                                    <select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}" title="Select with tooltip" name="brand_id">

                                                        <option value="">Select Brand</option>

                                                        <?php
                                                        foreach ($brands as $brand){ ?>

                                                            <option value="<?php echo $brand->id; ?>"><?php echo $brand->eng_name; ?></option>
                                                        <?php }

                                                        ?>

                                                    </select>
                                                </div>


                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class=" uk-grid uk-grid-medium" data-uk-grid-margin>
                                    <div class="uk-width-xLarge-10-10">
                                        <div class="md-card">
                                            <div class="uk-grid  uk-grid-medium">

                                                <div class="uk-width-5-10">
                                                    <label>English Short Description(150 word only)</label>
                                                    <br>
                                                    <textarea name="eng_short_des" cols="30" rows="4" class="md-input eng_text" id="eng_short_des"></textarea>
                                                </div>
                                                <div class="uk-width-5-10 rtl">
                                                    <label>Arbic Short Description(150 word only)</label>
                                                    <br>
                                                    <textarea name="arb_short_des" cols="30" rows="4" class="md-input arb_text" id="arb_short_des"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class=" uk-grid uk-grid-medium" data-uk-grid-margin>
                                    <div class="uk-width-xLarge-10-10">
                                        <div class="md-card">
                                            <div class="uk-grid  uk-grid-medium">

                                            <div class="uk-width-5-10">
                                                <label>English Overview & Usage</label>
                                                <br>
                                                <textarea name="eng_more_detail" cols="30" rows="4" class="md-input eng_text" id="eng_more_detail"></textarea>
                                            </div>
                                            <div class="uk-width-5-10 rtl">
                                                <label>Arbic Overview & Usage</label>
                                                <br>
                                                <textarea name="arb_more_detail" cols="30" rows="4" class="md-input arb_text" id="arb_more_detail"></textarea>
                                            </div>
                                    </div>
                                    </div>
                                </div>
                            </div>


                                <div class=" uk-grid uk-grid-medium" data-uk-grid-margin>
                                    <div class="uk-width-xLarge-10-10">
                                        <div class="md-card">
                                            <div class="uk-width-10-10 inlineBlock multi_selector">

                                                <select id="kerWordsTag" class="color_id" multiple>
                                                    <option value="">Select Color</option>

                                                    <?php
                                                    foreach ($colors as $color){ ?>

                                                        <option value="<?php echo $color->id; ?>"><?php echo $color->eng_name; ?></option>
                                                    <?php }

                                                    ?>

                                                </select>

                                                <input id="color_id" type="hidden" name="color_id" value="">


                                            </div>
                                            <div class="md-card-content uk-width-10-10">

                                                <div class="inlineBlock">
                                                    <label for="eng_quantity">Weight Value</label>
                                                    <input type="text" class="md-input" id="weight_value" name="weight_value"/>

                                                </div>
                                                <div class="inlineBlock">
                                                    <label for="eng_quantity">Length Value</label>
                                                    <input type="text" class="md-input" id="length_value" name="length_value"/>
                                                </div>
                                                <div class="inlineBlock">
                                                    <label for="eng_quantity">Width Value</label>
                                                    <input type="text" class="md-input" id="width_value" name="width_value"/>

                                                </div>
                                                <div class="inlineBlock">
                                                    <label for="eng_quantity">Height Value</label>
                                                    <input type="text" class="md-input" id="height_value" name="height_value" />


                                                </div>



                                            </div>

                                        </div>

                                    </div>

                                </div>

                                <div class=" uk-grid uk-grid-small smiVarActSec" data-uk-grid-margin="">

                                    <div class="uk-width-large-5-10">
                                        <div class="md-card">
                                            <div class="md-card-content">
                                                <h1>Actions</h1>
                                                <ul class="actionSwitcher">
                                                    <li>
                                                        <input name="active" checked id="switch_Active" value="1" type="checkbox" data-switchery data-switchery-size="large" data-switchery-color="#85b102" />
                                                        <label for="switch_Active" class="inline-label">Active</label>
                                                    </li>

                                                </ul>
                                            </div>


                                        </div>


                                    </div>

                                    <div class="uk-width-large-5-10">
                                        <div class="md-card">
                                            <div class="md-card-content">
                                                <h1>Actions</h1>
                                                <ul class="actionSwitcher">

                                                    <li class="daily_offer">
                                                        <input name="daily_offer" id="daily_offer" type="checkbox" data-switchery data-switchery-size="large" data-switchery-color="#85b102" />
                                                        <label for="switch_Daily_Offer" class="inline-label">Offer</label>
                                                    </li>
                                                </ul>
                                            </div>

                                            <br>

                                            <div class="uk-form-row daily_offer_date display-none">

                                                <label for="start_date">Offer Start Date </label>

                                                <input type="text" class="md-input" id="start_date" name="start_date" data-uk-datepicker="{format:'DD/MM/YYYY'}" value=""/>

                                            </div>

                                            <div class="uk-form-row daily_offer_date display-none">

                                                <label for="end_date">Offer End Date </label>

                                                <input type="text" class="md-input" id="end_date" name="end_date" value="" data-uk-datepicker="{format:'DD/MM/YYYY'}"/>

                                            </div>
                                        </div>


                                    </div>



                                </div>

                            </div>


                            </div>
                        </div><!--      Single Order Row END Here       -->
                    </div>
                </div>
            <div class="md-card">
                <div class="md-card-toolbar">
                    <h3 class="md-card-toolbar-heading-text"> Keyword </h3>
                </div>

                <div class="md-card-content large-padding">
                    <div class="uk-grid uk-grid-divider uk-grid-medium" data-uk-grid-margin>
                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row"><label>English Meta Title</label> <input type="text"
                                                                                              class="md-input"
                                                                                              value="<?php //echo $record[0]->eng_meta_title;?>"
                                                                                              name="eng_meta_title"/>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row"><label>Arabic Meta Title</label> <input type="text"
                                                                                             class="md-input"
                                                                                             value="<?php //echo $record[0]->arb_meta_title;?>"
                                                                                             name="arb_meta_title"/>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row"><label>Englishn Meta Description</label> <textarea cols="30"
                                                                                                        rows="4"
                                                                                                        class="md-input"
                                                                                                        name="eng_meta_description"><?php //echo $record[0]->eng_meta_description;?></textarea>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row"><label>Arabic Meta Description</label> <textarea cols="30" rows="4"
                                                                                                      class="md-input"
                                                                                                      name="arb_meta_description"><?php //echo $record[0]->arb_meta_description;?></textarea>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row"><label>Englishn Meta Keyword</label> <textarea cols="30" rows="4"
                                                                                                    class="md-input"
                                                                                                    name="eng_meta_keyword"><?php //echo $record[0]->eng_meta_keyword;?></textarea>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row"><label>Arabic Meta Keyword</label> <textarea cols="30" rows="4"
                                                                                                  class="md-input"
                                                                                                  name="arb_meta_keyword"><?php //echo $record[0]->arb_meta_keyword;?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            </div>
        </div>






        <div class="md-fab-wrapper">
            <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);">
                <i class="material-icons">&#xE161;</i>
            </a>
        </div>
    </form>

</div>
</div>