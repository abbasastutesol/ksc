<div id="page_content">

    <div id="page_content_inner">

        <div class="md-card-content orderPage ordDtlPgEd">
            <div class="orderListBox">
                <div class="singleOrder noBorder">
                    <div class="orderExpand uk-child"  style="display: block;" >
                        <div class="uk-grid ordSingDtlEdHead" data-uk-grid-margin data-uk-grid-match="{target:'.md-card'}">
                            <div class="uk-width-medium-5-10">
                                <a href="<?php echo base_url().'admin/product'; ?>">
                                    <i class="material-icons">keyboard_arrow_left</i>
                                    Go back
                                </a>
                            </div>
                            <div class="uk-width-medium-5-10">
                                <div class="gallery_grid_image_menu" data-uk-dropdown="{pos:'bottom-right'}">
                                    <i class="md-icon material-icons">&#xE5D4;</i>
                                    <div class="uk-dropdown uk-dropdown-small">
                                        <ul class="uk-nav">
                                            <li><a href="<?php echo base_url().'admin/product/edit/'.$product->id; ?>"><i class="material-icons uk-margin-small-right">&#xE150;</i> Edit</a></li>
                                            <li><a href="#" onclick="deleteRecord(<?php echo $product->id;?>,'admin/product/action','<?php echo base_url()."admin/product"; ?>');"><i class="material-icons uk-margin-small-right">&#xE872;</i> Remove</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="productDtlSec">
                            <div class=" uk-grid uk-grid-medium" data-uk-grid-margin>
                                <div class="uk-width-xLarge-2-10 uk-width-large-3-10">
                                    <div class="uk-margin-bottom uk-text-center">
                                        <div class="uk-slidenav-position proSliderBox" data-uk-slideshow="{animation:'scale'}">
                                            <ul class="uk-slideshow">


                                                <?php
                                                $count = 1;
                                                $images = getProductImages($product->id);
                                                $total = array_count_values(
                                                    array_column($images, 'eng_image')
                                                );
                                                foreach($images as $img){
                                                    if($img['is_thumbnail'] == 0) {

                                                            $image = base_url() . 'uploads/images/products/' . $img['eng_image'];

                                                            ?>
                                                            <li><img src="<?php echo $image; ?>" alt=""><p class="topNo"><?php echo $count; ?>/<?php  echo count($total)-1; ?></p></li>

                                                            <?php
                                                        $count++;
                                                    }

                                                }

                                                $prod_image =  getProductImages($product->id);
                                                foreach($prod_image as $key => $prod_img){
                                                    if($prod_img['is_thumbnail'] == 1){
                                                        $thumbnail = $prod_img['thumbnail'];
                                                    }
                                                }

                                                ?>

                                            </ul>
                                            <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
                                            <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideshow-item="next"></a>

                                            <div class="uk-form-file uploadImages">
                                              <button class="btn ioudBtn green" type="button">
                                                    <i class="material-icons">crop_original</i>images
                                                </button>

                                            </div>
                                        </div>


                                        <li><img src="<?php echo base_url() . 'uploads/images/thumbs/products/'.$thumbnail; ?>" alt=""><p class="topNo"></p></li>

                                        <button class="uk-form-file btn ioudBtn blue">
                                            <i class="material-icons">crop_original</i>Thumbnail
                                        </button>

                                    </div>
                                </div>
                                <div class="uk-width-xLarge-8-10  uk-width-large-7-10">
                                    <div class="uk-grid uk-grid-medium">
                                        <div class="uk-width-10-10">
                                            <h1 class="inlineBlock"><?php echo $product->eng_name; ?></h1>
                                            <h1 class="inlineBlock sepr">|</h1>
                                            <h1 class="inlineBlock"><?php echo $product->arb_name; ?></h1>
                                            <div class="clearfix"></div>
                                            <h3><?php $cat = getCategoriesById($product->category_id);

                                            echo $cat['eng_name']; ?></h3>
                                        </div>
                                    </div>
                                    <div class="uk-grid">
                                        <!--<div class="uk-width-medium-1-6" >
                                            <h2>Serial Number</h2>
                                            <h3>0491601003</h3>
                                        </div>-->
                                        <div class="uk-width-medium-1-6">
                                            <h2>Price</h2>
                                            <h3><?php echo $product->eng_price; ?> SAR</h3>
                                        </div>
                                        <div class="uk-width-medium-1-6">
                                            <h2>Availability</h2>
                                            <h3><?php echo $product->total_quantity; ?> Pcs</h3>
                                        </div>
                                        <div class="uk-width-medium-1-6">
                                            <h2>Discount Type</h2>
                                            <h3><?php echo $product->discount_type; ?></h3>
                                        </div>
                                        <div class="uk-width-medium-1-6">
                                            <h2>Discount Figure</h2>
                                            <h3><?php echo ($product->discount_amount == "" ? 'N/A' : $product->discount_amount); echo ($product->discount_type == "percentage" ? '%' : ''); ?></h3>
                                        </div>
                                        <!--<div class="uk-width-medium-1-6">
                                            <h2>Sub products</h2>
                                            <h3>11</h3>
                                        </div>-->
                                    </div>

                                    <div class="uk-grid  uk-grid-medium">
                                        <div class="uk-width-5-10">
                                            <h1>Description</h1>
                                            <p><?php echo $product->eng_description; ?></p>
                                        </div>
                                        <div class="uk-width-5-10 rtl">
                                            <h1>وصف</h1>
                                            <p><?php echo $product->arb_description; ?></p>
                                        </div>

                                        <div class="uk-width-5-10">
                                            <h1>English More Detail</h1>
                                            <p><?php echo strip_tags($product->eng_more_detail); ?></p>
                                        </div>
                                        <div class="uk-width-5-10 rtl">
                                            <h1>Arabic More Detail</h1>
                                            <p><?php echo strip_tags($product->arb_more_detail); ?></p>
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <div class=" uk-grid uk-grid-medium" data-uk-grid-margin>
                                <div class="uk-width-xLarge-10-10">
                                    <div class="md-card">
                                        <div class="md-card-content">
                                            <h1>Tags</h1>
                                            <br />
                                            <h2>Meta Tag title</h2>
                                            <p class="inlineBlock"><?php echo $product->eng_meta_title; ?> Title Goes here</p> <h1 class="inlineBlock sepr">|</h1> <p class="inlineBlock"><?php echo $product->arb_meta_title; ?> Title Goes here</p>
                                            <br />
                                            <br />
                                            <h2>Meta Tag Description</h2>
                                            <p><?php echo $product->eng_meta_description; ?>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean tincidunt mollis sapien, sit amet consequat erat viverra non. Phasellus dignissim ultrices orci congue vestibulum.</p>
                                            <br />
                                            <div class="uk-grid tagBox" data-uk-grid-margin>
                                                <div class="uk-width-large-1-1">
                                                    <h2>Keywords</h2>
                                                    <div class="uk-form-row">

                                                        <?php
                                                        $options = '';
                                                        $eng_meta_keyword = explode(',',$product->eng_meta_keyword);
                                                        foreach ($eng_meta_keyword as $keyword) {
                                                            $options .= '<option value="'.$keyword.'" selected>'.$keyword.'</option>';
                                                        }
                                                        ?>

                                                        <select id="kerWordsTag" name="kerWordsTag" multiple disabled>
                                                            <?php echo $options; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="uk-width-large-1-1">
                                                    <div class="uk-form-row">

                                                        <?php
                                                        $options = '';
                                                        $arb_meta_keyword = explode(',',$product->arb_meta_keyword);
                                                        foreach ($arb_meta_keyword as $keyword) {
                                                            $options .= '<option value="'.$keyword.'" selected>'.$keyword.'</option>';
                                                        }
                                                        ?>

                                                        <select id="kerWordsTag_ar" name="kerWordsTag_ar" multiple disabled>
                                                            <?php echo $options; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="uk-grid tagBox" data-uk-grid-margin>
                                                <div class="uk-width-large-1-1">
                                                    <h2>Product Tags</h2>
                                                    <div class="uk-form-row">

                                                        <?php
                                                        $options = '';
                                                        $eng_product_keyword = explode(',',$product->eng_product_keyword);
                                                        foreach ($eng_product_keyword as $keyword) {
                                                            $options .= '<option value="'.$keyword.'" selected>'.$keyword.'</option>';
                                                        }
                                                        ?>
                                                        <select id="productTag" name="productTag" multiple disabled>
                                                            <?php echo $options; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="uk-width-large-1-1">
                                                    <div class="uk-form-row">

                                                        <?php
                                                        $options = '';
                                                        $arb_product_keyword = explode(',',$product->arb_product_keyword);
                                                        foreach ($arb_product_keyword as $keyword) {
                                                            $options .= '<option value="'.$keyword.'" selected>'.$keyword.'</option>';
                                                        }
                                                        ?>
                                                        <select id="productTag_ar" name="productTag_ar" multiple disabled>
                                                            <?php echo $options; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <br />
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class=" uk-grid uk-grid-small smiVarActSec" data-uk-grid-margin="">

                                <div class="uk-width-large-5-10">
                                    <div class="md-card">
                                        <div class="md-card-content">
                                            <h1>Actions</h1>
                                            <ul class="actionSwitcher">
                                                <li>
                                                    <input disabled <?php echo ($product->active == 1 ? 'checked' : ''); ?> id="switch_Active" type="checkbox" data-switchery data-switchery-size="large" data-switchery-color="#85b102" />
                                                    <label for="switch_Active" class="inline-label">Active</label>
                                                </li>
                                                <li>
                                                    <input type="checkbox" data-switchery data-switchery-size="large" data-switchery-color="#85b102" id="display_to_home_product" name="display_to_home_product"  value="1" <?php echo ($product->display_to_home_product == '1' ? 'checked' : ''); ?>/>

                                                    <label for="display_to_home_product" class="inline-label">Display to home under product section</label>
                                                </li>


                                            </ul>
                                        </div>


                                        <br>

                                        <div class="uk-form-row">

                                            <select name="weight_unit" data-md-selectize data-md-selectize-bottom>
                                                <option value="">Weight Unit</option>
                                                <option value="kg" <?php if($product->weight_unit == "kg") { echo 'selected'; } ?>>KG</option>
                                                <option value="g" <?php if($product->weight_unit == "g") { echo 'selected'; } ?>>G</option>
                                                <option value="lb" <?php if($product->weight_unit == "lb") { echo 'selected'; } ?>>LB</option>
                                                <option value="oz" <?php if($product->weight_unit == "oz") { echo 'selected'; } ?>>OZ</option>
                                            </select>

                                        </div>

                                        <div class="uk-form-row">
                                            <label for="eng_quantity">Weight Value</label>
                                            <input type="text" class="md-input" id="weight_value" name="weight_value" value="<?php echo $product->weight_value; ?>"/>


                                        </div>

                                    </div>
                                    <br>

                                </div>


                                
                                <div class="uk-width-large-5-10">
                                    <div class="md-card">
                                        <div class="md-card-content">
                                            <h1>Actions</h1>
                                            <ul class="actionSwitcher">

                                               <!-- <li>
                                                    <input checked id="switch_Featured" type="checkbox" data-switchery data-switchery-size="large" data-switchery-color="#85b102" />
                                                    <label for="switch_Featured" class="inline-label">Featured</label>
                                                </li>
                                                <li>
                                                    <input checked id="switch_New_Arrival" type="checkbox" data-switchery data-switchery-size="large" data-switchery-color="#85b102" />
                                                    <label for="switch_New_Arrival" class="inline-label">New Arrival</label>
                                                </li>
                                                <li>
                                                    <input checked id="switch_Best_Seller" type="checkbox" data-switchery data-switchery-size="large" data-switchery-color="#85b102" />
                                                    <label for="switch_Best_Seller" class="inline-label">Best Seller</label>
                                                </li>-->

                                                <li>
                                                    <input type="checkbox" data-switchery data-switchery-size="large" data-switchery-color="#85b102" id="display_to_home_offer" name="display_to_home_offer"  value="1" <?php echo ($product->display_to_home_offer == '1' ? 'checked' : ''); ?>/>

                                                    <label for="display_to_home_offer" class="inline-label">Display to home under offer section</label>

                                                </li>
                                                <li class="">
                                                    <input disabled <?php echo ($product->daily_offer == 1 ? 'checked' : ''); ?> id="daily_offer" type="checkbox" data-switchery data-switchery-size="large" data-switchery-color="#85b102" />
                                                    <label for="daily_offer" class="inline-label">Daily Offer</label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <br>

                                    <div class="uk-form-row daily_offer_date <?php if($product->daily_offer == '1') { echo 'display-block'; } else { echo 'display-none'; } ?>">

                                        <label for="start_date">Offer Start Date </label>

                                        <input type="text" class="md-input" id="start_date" name="start_date" data-uk-datepicker="{format:'DD/MM/YYYY'}" value="<?php if($product->daily_offer == '1') { echo date('d/m/Y',strtotime($product->start_date)); } else { echo ''; }; ?>"/>

                                    </div>

                                    <div class="uk-form-row daily_offer_date <?php if($product->daily_offer == '1') { echo 'display-block'; } else { echo 'display-none'; } ?>">

                                        <label for="end_date">Offer End Date </label>

                                        <input type="text" class="md-input" id="end_date" name="end_date" value="<?php if($product->daily_offer == '1') { echo date('d/m/Y',strtotime($product->end_date)); } else { echo ''; }; ?>" data-uk-datepicker="{format:'DD/MM/YYYY'}"/>

                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div><!--      Single Order Row END Here       -->
            </div>
        </div>


    </div>
</div>

<div class="md-fab-wrapper">
    <a class="md-fab md-fab-primary" href="<?php echo base_url().'admin/product/edit/'.$product->id; ?>">
        <i class="material-icons">&#xE150;</i>
    </a>
</div>

<div class="uk-modal addA_variant" id="addA_variant">
    <div class="uk-modal-dialog uk-modal-dialog-large">
        <div class="uk-modal-header">
            <h3 class="uk-modal-title">Add a Variantc</h3>
            <button class="uk-modal-close uk-close closeBtnEd" type="button">&nbsp;</button>
        </div>

        <div class="uk-grid " data-uk-grid-margin>
            <div class="uk-width-medium-3-10">
                <div class="uk-margin-bottom uk-text-center">
                    <div class="uk-slidenav-position proSliderBox" data-uk-slideshow="{animation:'scale'}">
                        <ul class="uk-slideshow">
                            <li class="uk-active" aria-hidden="false"><img src="assets/img/slider.png" alt=""><p class="topNo">1/4</p></li>
                            <li><img src="assets/img/gallery/Image02.jpg" alt=""><p class="topNo">2/4</p></li>
                            <li><img src="assets/img/gallery/Image03.jpg" alt=""><p class="topNo">3/4</p></li>
                            <li><img src="assets/img/gallery/Image04.jpg" alt=""><p class="topNo">4/4</p></li>
                        </ul>
                        <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
                        <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideshow-item="next"></a>
                        <div class="uk-form-file uploadImages">
                            <button class="btn ioudBtn green" type="button">
                                <i class="material-icons">crop_original</i> Upload More images
                            </button>
                            <input id="form-file" type="file" multiple>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-width-medium-7-10">
                <div class="uk-grid " data-uk-grid-margin>
                    <div class="uk-width-medium-3-10">
                        <label>Type</label>
                    </div>
                    <div class="uk-width-medium-7-10">
                        <select class="uk-form-width-full" name="task_assignee" id="task_assignee" data-md-selectize-inline>
                            <option value="">Sizes</option>
                            <option value="Small">Small</option>
                            <option value="medium">MediumPayment</option>
                            <option value="large">Large</option>
                            <option value="extra_large">Extra Large</option>
                        </select>
                    </div>
                </div>
                <div class="uk-grid " data-uk-grid-margin>
                    <div class="uk-width-medium-3-10">
                        <label>Size</label>
                    </div>
                    <div class="uk-width-medium-7-10">
                        <label>Write</label>
                        <input type="text" class="md-input uk-form-width-large" />
                    </div>
                </div>
                <div class="uk-grid " data-uk-grid-margin>
                    <!--<div class="uk-width-medium-3-10">
                        <label>Serial Number</label>
                    </div>-->
                    <div class="uk-width-medium-7-10">
                        <label>Write</label>
                        <input type="text" class="md-input uk-form-width-large" />
                    </div>
                </div>
                <div class="uk-grid " data-uk-grid-margin>
                    <div class="uk-width-medium-3-10">
                        <label>Quanity</label>
                    </div>
                    <div class="uk-width-medium-7-10">
                        <label>Write</label>
                        <input type="text" class="md-input uk-form-width-large" />
                    </div>
                </div>
                <div class="uk-grid " data-uk-grid-margin>
                    <div class="uk-width-medium-3-10">
                        <label>Price</label>
                    </div>
                    <div class="uk-width-medium-7-10">
                        <label>Write</label>
                        <input type="text" class="md-input uk-form-width-large" />
                    </div>
                </div>
                <div class="uk-grid " data-uk-grid-margin>
                    <div class="uk-width-medium-3-10">
                        <label>Discounted Price</label>
                    </div>
                    <div class="uk-width-medium-7-10">
                        <label>Write</label>
                        <input type="text" class="md-input uk-form-width-large" />
                    </div>
                </div>
                <div class="uk-modal-footer uk-text-left">
                    <button type="button" class="btn ioudBtn red uk-modal-close">Discard</button>
                    <button type="button" class="btn ioudBtn green">Add</button>
                </div>
            </div>
        </div>



    </div>
</div>