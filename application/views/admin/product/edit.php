<div id="page_content">
    <form action="<?php echo base_url(); ?>admin/product/action" method="post" onsubmit="return false" class="uk-form-stacked ajax_form" id="product_edit_form" enctype="multipart/form-data">
        <input type="hidden" name="form_type" value="update">

        <input type="hidden" name="id" value="<?php echo $product->id;?>">
        <div id="page_content_inner">

            <div class="md-card-content orderPage ordDtlPgEd">
                <div class="orderListBox">
                    <div class="singleOrder noBorder">
                        <div class="orderExpand uk-child"  style="display: block;" >
                            <div class="uk-grid ordSingDtlEdHead" data-uk-grid-margin data-uk-grid-match="{target:'.md-card'}">
                                <div class="uk-width-medium-5-10">
                                    <a href="<?php echo base_url().'admin/product'; ?>">
                                        <i class="material-icons">keyboard_arrow_left</i>
                                        Go back
                                    </a>
                                </div>
                                <div class="uk-width-medium-5-10">
                                    <div class="gallery_grid_image_menu" data-uk-dropdown="{pos:'bottom-right'}">
                                        <i class="md-icon material-icons">&#xE5D4;</i>
                                        <div class="uk-dropdown uk-dropdown-small">
                                            <ul class="uk-nav">
                                                <li><a href="<?php echo base_url().'admin/product/edit/'.$product->id; ?>"><i class="material-icons uk-margin-small-right">&#xE150;</i> Edit</a></li>
                                                <li><a href="#" onclick="deleteRecord(<?php echo $product->id;?>,'admin/product/action','<?php echo base_url()."admin/product"; ?>');"><i class="material-icons uk-margin-small-right">&#xE872;</i> Remove</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="productDtlSec">
                                <div class=" uk-grid uk-grid-medium" data-uk-grid-margin>
                                    <div class="uk-width-xLarge-2-10 uk-width-large-3-10">
                                        <div class="uk-margin-bottom uk-text-center">
                                            <div class="uk-slidenav-position proSliderBox" data-uk-slideshow="{animation:'scale'}">
                                                <ul class="uk-slideshow">

                                                    <?php
                                                    $count = 1;
                                                    $images = getProductImages($product->id);
                                                    $total = array_count_values(
                                                        array_column($images, 'eng_image')
                                                    );

                                                    foreach($images as $img){
                                                        if($img['is_thumbnail'] == 0 && $img['eng_image'] != '') {

                                                            $image = base_url() . 'uploads/images/products/' . $img['eng_image'];

                                                            ?>
                                                            <li>
                                                                <img src="<?php echo $image; ?>" alt="">
                                                                <p class="topNo"><?php echo $count; ?>/<?php  echo count($total)-1; ?></p>
                                                                <button type="button" class="uk-modal-close uk-close uk-close-alt uk-position-absolute" onclick="deleteProdImage('<?php echo $img["id"]; ?>');"></button>
                                                            </li>

                                                            <?php
                                                            $count++;
                                                        }

                                                    }

                                                    $thumb_id = '';
                                                    $gif_id = '';
                                                    $eng_pdf = '';
                                                    $arb_pdf = '';
                                                    $eng_pdf_id = '';
                                                    $arb_pdf_id = '';

                                                    $prod_image =  getProductImages($product->id);
                                                    foreach($prod_image as $key => $prod_img){
                                                        if($prod_img['is_thumbnail'] == 1){
                                                            $thumbnail = $prod_img['thumbnail'];
                                                            $thumb_id = $prod_img['id'];
                                                        }
                                                        if($prod_img['is_thumbnail'] == 0 && $prod_img['eng_pdf'] != ''){
                                                            $eng_pdf = base_url() . 'uploads/images/products/' . $prod_img['eng_pdf'];
                                                            $eng_pdf_id = $prod_img['id'];
                                                        }
                                                        if($prod_img['is_thumbnail'] == 0 && $prod_img['arb_pdf'] != ''){
                                                            $arb_pdf = base_url() . 'uploads/videos/' . $prod_img['arb_pdf'];
                                                            $arb_pdf_id = $prod_img['id'];
                                                        }
                                                    }

                                                    ?>

                                                </ul>
                                                <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
                                                <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideshow-item="next"></a>

                                                <div class="uk-form-file uploadImages">
                                                    <button class="btn ioudBtn green" type="button">
                                                        <i class="material-icons">crop_original</i> Upload More images
                                                    </button>
                                                    <input id="form-file" type="file" name="image[]" multiple>
                                                </div>


                                                <br>
                                                <br>

                                                <?php  if($thumbnail != ''){?>
                                                    <li style="position:relative;">
                                                        <img src="<?php echo base_url() . 'uploads/images/thumbs/products/'.$thumbnail; ?>" alt="">
                                                        <p class="topNo"></p>
                                                        <button type="button" class="uk-modal-close uk-close uk-close-alt uk-position-absolute" onclick="deleteProdImage('<?php echo $thumb_id; ?>');"></button>

                                                    </li>
                                                <?php } ?>

                                                <button class="uk-form-file btn ioudBtn blue">
                                                    <i class="material-icons">crop_original</i> Upload Thumbnail
                                                    <input id="form-file" name="thumbnail[]" type="file">
                                                </button>


                                                <br>
                                                <br>
                                                <?php  if($eng_pdf != ''){?>
                                                    <li style="position:relative;">
                                                        <img src="<?php echo base_url().'assets/frontend/images/products/pdf/pdf_preview.png'; ?>" alt="">
                                                        <p class="topNo"></p>
                                                        <button type="button" class="uk-modal-close uk-close uk-close-alt uk-position-absolute" onclick="deleteProdImage('<?php echo $eng_pdf_id; ?>');"></button>

                                                    </li>
                                                <?php } ?>

                                                <button class="uk-form-file btn ioudBtn blue">
                                                    <i class="material-icons">crop_original</i> Upload English PDF
                                                    <input id="form-file" name="eng_pdf" type="file" accept="application/pdf" >
                                                </button>


                                                <br>

                                                <?php  if($arb_pdf != ''){?>
                                                    <li style="position:relative;">
                                                        <img src="<?php echo base_url().'assets/frontend/images/products/pdf/pdf_preview.png'; ?>" alt="">
                                                        <p class="topNo"></p>
                                                        <button type="button" class="uk-modal-close uk-close uk-close-alt uk-position-absolute" onclick="deleteProdImage('<?php echo $arb_pdf_id; ?>');"></button>

                                                    </li>

                                                <?php } ?>

                                                <button class="uk-form-file btn ioudBtn blue">
                                                    <i class="material-icons">crop_original</i> Upload Arabic PDF
                                                    <input id="form-file" name="arb_pdf" type="file" accept="application/pdf">
                                                </button>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="uk-width-xLarge-8-10  uk-width-large-7-10">
                                        <div class="uk-grid uk-grid-medium">
                                            <div class="uk-width-10-10">
                                                <div class="inlineBlock">
                                                    <label for="title_eng">Title goes here</label>
                                                    <input type="text" class="md-input" id="title_eng" name="eng_name" value="<?php echo $product->eng_name; ?>"/>
                                                </div>
                                                <h1 class="inlineBlock sepr">|</h1>
                                                <div class="inlineBlock">
                                                    <label for="title_arb">العنوان يذهب هنا</label>
                                                    <input type="text" class="md-input" id="title_arb" name="arb_name" value="<?php echo $product->arb_name; ?>"/>
                                                </div>
                                                <div class="inlineBlock">
                                                    <label for="priceTxt">Price</label>
                                                    <input type="text" class="md-input" id="priceTxt" name="eng_price" value="<?php echo $product->eng_price; ?>"/>
                                                </div>
                                                <br>
                                                <br>

                                                <div class="inlineBlock">
                                                    <label for="eng_quantity">Quantity</label>
                                                    <input type="text" class="md-input" id="eng_quantity" name="eng_quantity" value="<?php echo $product->eng_quantity; ?>"/>
                                                </div>

                                                <div class="clearfix"></div>

                                                <div>
                                                    <br />


                                                    <select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}" title="Select with tooltip" name="category_id">

                                                        <option value="">Select Category</option>

                                                        <?php
                                                        foreach ($categories as $category){ ?>

                                                            <option value="<?php echo $category->id; ?>" <?php if($product->category_id == $category->id) echo 'selected'; ?>><?php echo $category->eng_name; ?></option>
                                                        <?php }

                                                        ?>

                                                    </select>
                                                </div>

                                                <div>
                                                    <br />

                                                    <select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}" title="Select with tooltip" name="brand_id">

                                                        <option value="">Select Brand</option>

                                                        <?php
                                                        foreach ($brands as $brand){ ?>

                                                            <option value="<?php echo $brand->id; ?>" <?php if($product->brand_id == $brand->id) echo 'selected'; ?>><?php echo $brand->eng_name; ?></option>
                                                        <?php }

                                                        ?>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <br>

                                        <div class=" uk-grid uk-grid-medium" data-uk-grid-margin>
                                            <div class="uk-width-xLarge-10-10">
                                                <div class="md-card">
                                                    <div class="uk-grid  uk-grid-medium">

                                                        <div class="uk-width-5-10">
                                                            <label>English Short description</label>
                                                            <br>
                                                            <textarea name="eng_short_des" cols="30" rows="4" class="md-input eng_text" id="eng_short_des"><?php echo $product->eng_short_des; ?></textarea>
                                                        </div>
                                                        <div class="uk-width-5-10 rtl">
                                                            <label>Arbic Short description</label>
                                                            <br>
                                                            <textarea name="arb_short_des" cols="30" rows="4" class="md-input arb_text" id="arb_short_des"><?php echo $product->arb_short_des; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class=" uk-grid uk-grid-medium" data-uk-grid-margin>
                                            <div class="uk-width-xLarge-10-10">
                                                <div class="md-card">
                                                    <div class="uk-grid  uk-grid-medium">

                                                        <div class="uk-width-5-10">
                                                            <label>English Overview & Usage</label>
                                                            <br>
                                                            <textarea name="eng_more_detail" cols="30" rows="4" class="md-input eng_text" id="eng_more_detail"><?php echo $product->eng_more_detail; ?></textarea>
                                                        </div>
                                                        <div class="uk-width-5-10 rtl">
                                                            <label>Arbic Overview & Usage</label>
                                                            <br>
                                                            <textarea name="arb_more_detail" cols="30" rows="4" class="md-input arb_text" id="arb_more_detail"><?php echo $product->arb_more_detail; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>

                                <div class=" uk-grid uk-grid-medium" data-uk-grid-margin>
                                    <div class="uk-width-xLarge-10-10">
                                        <div class="md-card">
                                            <div class="uk-width-10-10 inlineBlock multi_selector">


                                                <?php
                                                $options = '';
                                                $p_color = array();
                                                $prod_colors = GetProductColor($product->id);
                                                foreach($prod_colors as $prod_color){

                                                    $p_color[] = $prod_color['color_id'];
                                                }
                                                foreach ($colors as $color) {

                                                    if(in_array($color->id,$p_color)){
                                                        $selected = 'selected';
                                                    }else{
                                                        $selected = '';
                                                    }

                                                    $options .= '<option value="'.$color->id.'" '.$selected.'>'.$color->eng_name.'</option>';
                                                }
                                                ?>

                                                <select id="kerWordsTag" class="color_id" multiple>
                                                    <option value="">Select Color</option>

                                                    <?php echo $options; ?>

                                                </select>

                                                <input id="color_id" type="hidden" name="color_id" value="<?php echo implode(',',$p_color); ?>">


                                            </div>
                                            <div class="md-card-content uk-width-10-10">

                                                <div class="inlineBlock">
                                                    <label for="eng_quantity">Weight Value</label>
                                                    <input type="text" class="md-input" id="weight_value" name="weight_value" value="<?php echo $product->weight_value; ?>"/>

                                                </div>
                                                <div class="inlineBlock">
                                                    <label for="eng_quantity">Length Value</label>
                                                    <input type="text" class="md-input" id="length_value" name="length_value" value="<?php echo $product->length_value; ?>"/>
                                                </div>
                                                <div class="inlineBlock">
                                                    <label for="eng_quantity">Width Value</label>
                                                    <input type="text" class="md-input" id="width_value" name="width_value" value="<?php echo $product->width_value; ?>"/>

                                                </div>
                                                <div class="inlineBlock">
                                                    <label for="eng_quantity">Height Value</label>
                                                    <input type="text" class="md-input" id="height_value" name="height_value" value="<?php echo $product->height_value; ?>"/>


                                                </div>



                                            </div>

                                        </div>

                                    </div>

                                </div>

                                <div class=" uk-grid uk-grid-small smiVarActSec" data-uk-grid-margin="">

                                    <div class="uk-width-large-5-10">
                                        <div class="md-card">
                                            <div class="md-card-content">
                                                <h1>Actions</h1>
                                                <ul class="actionSwitcher">
                                                    <li>
                                                        <input name="active" <?php echo ($product->active == 1 ? 'checked' : ''); ?> id="switch_Active" value="1" type="checkbox" data-switchery data-switchery-size="large" data-switchery-color="#85b102" />
                                                        <label for="switch_Active" class="inline-label">Active</label>
                                                    </li>

                                                </ul>
                                            </div>


                                        </div>


                                    </div>

                                    <div class="uk-width-large-5-10">
                                        <div class="md-card">
                                            <div class="md-card-content">
                                                <h1>Actions</h1>
                                                <ul class="actionSwitcher">

                                                    <li class="daily_offer">
                                                        <input name="daily_offer" id="daily_offer" <?php echo ($product->daily_offer == 1 ? 'checked' : ''); ?> type="checkbox" data-switchery data-switchery-size="large" data-switchery-color="#85b102" />
                                                        <label for="switch_Daily_Offer" class="inline-label">Offer</label>
                                                    </li>
                                                </ul>
                                            </div>

                                            <br>

                                            <div class="uk-form-row daily_offer_date display-none">

                                                <label for="start_date">Offer Start Date </label>

                                                <input type="text" class="md-input" id="start_date" name="start_date" data-uk-datepicker="{format:'DD/MM/YYYY'}" value=""/>

                                            </div>

                                            <div class="uk-form-row daily_offer_date display-none">

                                                <label for="end_date">Offer End Date </label>

                                                <input type="text" class="md-input" id="end_date" name="end_date" value="" data-uk-datepicker="{format:'DD/MM/YYYY'}"/>

                                            </div>
                                        </div>


                                    </div>





                                </div>

                            </div>
                        </div><!--      Single Order Row END Here       -->
                    </div>
                </div>

                <div class="md-card">
                    <div class="md-card-toolbar">
                        <h3 class="md-card-toolbar-heading-text"> Keyword </h3>
                    </div>

                    <div class="md-card-content large-padding">
                        <div class="uk-grid uk-grid-divider uk-grid-medium" data-uk-grid-margin>
                            <div class="uk-width-medium-1-2">
                                <div class="uk-form-row"><label>English Meta Title</label> <input type="text"
                                                                                                  class="md-input"
                                                                                                  value="<?php echo $meta_tags['eng_meta_title']; ?>"
                                                                                                  name="eng_meta_title"/>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <div class="uk-form-row"><label>Arabic Meta Title</label> <input type="text"
                                                                                                 class="md-input"
                                                                                                 value="<?php echo $meta_tags['arb_meta_title']; ?>"
                                                                                                 name="arb_meta_title"/>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <div class="uk-form-row"><label>Englishn Meta Description</label> <textarea cols="30"
                                                                                                            rows="4"
                                                                                                            class="md-input"
                                                                                                            name="eng_meta_description"><?php echo $meta_tags['eng_meta_description']; ?></textarea>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <div class="uk-form-row"><label>Arabic Meta Description</label> <textarea cols="30" rows="4"
                                                                                                          class="md-input"
                                                                                                          name="arb_meta_description"><?php echo $meta_tags['arb_meta_description']; ?></textarea>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <div class="uk-form-row"><label>Englishn Meta Keyword</label> <textarea cols="30" rows="4"
                                                                                                        class="md-input"
                                                                                                        name="eng_meta_keyword"><?php echo $meta_tags['eng_meta_keyword']; ?></textarea>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <div class="uk-form-row"><label>Arabic Meta Keyword</label> <textarea cols="30" rows="4"
                                                                                                      class="md-input"
                                                                                                      name="arb_meta_keyword"><?php echo $meta_tags['arb_meta_keyword']; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
</div>

<div class="md-fab-wrapper">
    <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);">
        <i class="material-icons">&#xE161;</i>
    </a>
</div>
</form>
</div>
</div>