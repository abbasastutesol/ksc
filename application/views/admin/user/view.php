<div id="page_content">
  <div id="page_content_inner">
    <div class="md-card">
      <div class="md-card-content">
        <h3 class="heading_a">User Detail</h3><br>
        
		<div class="uk-grid" data-uk-grid-margin>
		
          <div class="uk-width-medium-1-1">
            
            <div class="uk-form-row">
              <label><strong>First Name: </strong></label>
              <?php echo $user->first_name?>
            </div>
          </div>
          
          <div class="uk-width-medium-1-1">
            
            <div class="uk-form-row">
              <label><strong>Last Name: </strong></label>
              <?php echo $user->last_name?>
            </div>
          </div>
          
          <div class="uk-width-medium-1-1">
            
            <div class="uk-form-row">
              <label><strong>Email: </strong></label>
              <?php echo $user->email?>
            </div>
          </div>
          
         
          <div class="uk-width-medium-1-1">
            
            <div class="uk-form-row">
              <label><strong>Mobile No: </strong></label>
              <?php echo $user->mobile_no?>
            </div>
          </div>
		<div class="uk-width-medium-1-1">
            <div class="uk-form-row">
                <label><strong>Country: </strong></label>
                <?php echo getCoutryByCode($user->country); ?>
            </div>          </div>
          <div class="uk-width-medium-1-1">
            <div class="uk-form-row">
              <label><strong>City: </strong></label>
              <?php echo getCityById($user->city); ?>
            </div>
          </div>

          <div class="uk-width-medium-1-1">
              <div class="uk-form-row">
                  <label><strong>Date of Birth: </strong></label>
                  <?php echo $user->dob?>
              </div>
          </div>

            <?php if($loyalty){ ?>
            <div class="uk-width-medium-1-1">
              <div class="uk-form-row">
                  <label><strong>Loyalty Type: </strong></label>
                  <?php
                  if($loyalty->loyalty_level == 1){
                      echo "Silver";
                  }
                  if($loyalty->loyalty_level == 2){
                      echo "Golden";
                  }
                  if($loyalty->loyalty_level == 3){
                      echo "Platinum";
                  }

                  ?>
              </div>
          </div>

            <div class="uk-width-medium-1-1">
                <div class="uk-form-row">
                    <label><strong>Loyalty Points: </strong></label>
                    <?php echo $loyalty->loyalty_points; ?>
                </div>
            </div>
            <?php } ?>
                  
          <!--<div class="uk-width-medium-1-1">
            
            <div class="uk-form-row">
              <label><strong>User Addresses: </strong></label>
              	<?php foreach($user_address as $user_add){?>
                	 <div class="uk-width-medium-1-1">
                        <div class="uk-form-row">
                          <label><strong>Address 1: </strong></label>
                          <?php echo $user_add->address_1;?>
                        </div>
                      </div>
                      <div class="uk-width-medium-1-1">
                        <div class="uk-form-row">
                          <label><strong>Address 2: </strong></label>
                          <?php echo $user_add->address_2 ?>
                        </div>
                      </div>
                      <div class="uk-width-medium-1-1">
                        <div class="uk-form-row">
                          <label><strong>City: </strong></label>
                          <?php echo $user_add->city?>
                        </div>
                      </div>
                      <div class="uk-width-medium-1-1">
                        <div class="uk-form-row">
                          <label><strong>Country: </strong></label>
                          <?php echo $user_add->country?>
                        </div>
                      </div>
                      <div class="uk-width-medium-1-1">
                        <div class="uk-form-row">
                          <label><strong>Region: </strong></label>
                          <?php echo $user_add->region?>
                        </div>
                      </div>
                      <div class="uk-width-medium-1-1">
                        <div class="uk-form-row">
                          <label><strong>Zip Code: </strong></label>
                          <?php echo $user_add->zip_code?>
                        </div>
                      </div>
                      <div class="uk-width-medium-1-1">
                        <div class="uk-form-row">
                          <label><strong>Contact Number: </strong></label>
                          <?php echo $user_add-> phone_no?>
                        </div>
                      </div>
                <?php }?>
            </div>
          </div>-->
		  
			
                
         
        </div>
       
      </div>
    </div>
    
    
    <div class="md-card">
        <div class="md-card-content">
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-1-1">
                    <div class="uk-overflow-container"> 
                    <h2>User Addresses</h2>
                        <table class="uk-table uk-table-align-vertical listing dt_default">
                            <thead>
                                <tr>
                                     <th>Sr#</th>
                                     <th class="nosort">Address Title</th>.									                                      <th class="nosort">Address 1</th>
                                     <th class="nosort">Address 2</th>
                                     <th class="nosort">Country</th>
                                     <th class="nosort">City</th>
                                     <th class="nosort">Phone Number</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
							$i=1;
							foreach($user_address as $user_add){?>
                                <tr id="<?php echo $user->id;?>">
                                    <td class="uk-text-center"><?php echo $i;?></td>
                                    <td><?php echo $user_add->address_title;?></td>									                                    <td><?php echo $user_add->address_1;?></td>
                                    <td><?php echo $user_add->address_2;?></td>
                                    <td><?php echo getCoutryByCode($user_add->country);?></td>									
                                    <td><?php echo getCityById($user_add->city);?></td>
                                    <td><?php echo $user_add->phone_no;?></td>
                                </tr>
                            <?php 
                                $i++;
                            }?>
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

  </div>
</div>
<?php
    if(isset($_GET['user']) && $_GET['user'] !=''){
        $backUrl = base_url().'admin/report/customer_orders';
    }else{
        $backUrl = base_url().'admin/users';
    }
?>
<div class="md-fab-wrapper" style="right:95px;">
        <a class="md-fab md-fab-primary" href="<?php echo $backUrl; ?>" id="">
            <i class="material-icons">keyboard_backspace</i>
        </a>
    </div>