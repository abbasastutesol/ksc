 <!-- Content Wrapper. Contains page content -->

      <div id="page_content">

        <div id="page_content_inner">
<?php if($users) { ?>
        <a href="<?php echo base_url();?>admin/users/exportExcelUsers" target="_blank" class="md-btn"><img src="<?php echo base_url();?>assets/admin/assets/images/excelIcon.png" alt="excel" /></a>
        <?php } ?>

			<div class="md-card">

                <div class="md-card-content">

                    <div class="uk-grid" data-uk-grid-margin>

                        <div class="uk-width-1-1">

                            <div class="uk-overflow-container"> 

                                <table class="uk-table uk-table-align-vertical listing dt_default">

                                    <thead>

                                        <tr>

                                             <th>Sr#</th>

                                             <th class="nosort">Name</th>

                                             <th class="nosort">Email</th>

                                             <!--<th class="nosort">Mobile Number</th>

                                             <th class="nosort">Phone Number</th>

                                             <th class="nosort">City</th>-->

                                             <th class="nosort">Created Date</th>
                                            <?php if(viewEditDeleteRights('Registered Users','view') || viewEditDeleteRights('Registered Users','delete')) { ?>
                                             <th class="nosort">Action</th>
                                            <?php } ?>

                                        </tr>

                                    </thead>

                                    <tbody>

                                    <?php 

									$i=1;

									foreach($users as $user)

									{

										$mobile_no = $user->mobile_no;

										if($user->mobile_ios2_code != '')

										{

											$mobile_code = explode('|', $user->mobile_ios2_code);

											$mobile_no = '+'.$mobile_code[1].' '.$user->mobile_no;

										}

										$phone_no = $user->phone_no;

										if($user->phone_ios2_code != '')

										{

											$phone_code = explode('|', $user->phone_ios2_code);

											$phone_no = '+'.$phone_code[1].' '.$user->phone_no;

										}

										?>

                                    	<tr id="<?php echo $user->id;?>">

                                       		<td class="uk-text-center"><?php echo $i;?></td>

                                        	<td><?php echo $user->first_name.' '.$user->last_name;?></td>

                                            <td><?php echo $user->email;?></td>

                                            <!--<td><?php echo $mobile_no;?></td>

                                            <td><?php echo $phone_no;?></td>

                                            <td><?php echo $user->city;?></td>-->

                                            <td><?php echo date('d-m-Y',strtotime($user->created_at));?></td>
                                            <?php if(viewEditDeleteRights('Registered Users','view') || viewEditDeleteRights('Registered Users','delete')) { ?>
                                            <td>
                                                <?php if(viewEditDeleteRights('Registered Users','view')) { ?>
                                                <a href="<?php echo base_url().'admin/users/view/'.$user->id;?>" title="View Users">
                                                    <i class="md-icon material-icons">visibility</i>
                                                </a>
                                                <?php } ?>
                                                <?php if(viewEditDeleteRights('Registered Users','delete')) { ?>
                                                <a href="javascript:void(0);" class="uk-margin-left delete" onclick="deleteRecord(<?php echo $user->id;?>,'admin/users/action','');" title="Delete Product">
                                                    <i class="material-icons md-24 delete">&#xE872;</i>
                                                </a>
                                                <?php } ?>
                                                

                                            </td>
                                            <?php } ?>

                                    	</tr>

                                    <?php 

										$i++;

									}?>

                                    </tbody>

                                </table>

                            </div>

                            

                        </div>

                    </div>

                </div>

            </div>



        </div>

    </div>

       <!-- /.content-wrapper --> 