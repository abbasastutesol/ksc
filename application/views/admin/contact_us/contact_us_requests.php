 <!-- Content Wrapper. Contains page content -->

      <div id="page_content">

        <div id="page_content_inner">
        

			<div class="md-card">

                <div class="md-card-content">

                    <div class="uk-grid" data-uk-grid-margin>

                        <div class="uk-width-1-1">

                        	<div class="uk-overflow-container"> 

                            	

                                <table class="uk-table uk-table-align-vertical listing dt_default">

                                    <thead>

                                        <tr>

                                             <th>Sr#</th>

                                              <th>Name</th>

											  <th>Email</th>

                                             <th>Postion</th>
                                             <th>Company</th>
                                             <th>Mobile</th>
                                            <th>Date</th>
                                             <th>Message</th>
                                            <?php if(viewEditDeleteRights('Contact Us Requests','delete')) { ?>
                                              <th>Action</th>
                                            <?php } ?>

                                        </tr>

                                    </thead>

                                    <tbody>

									<?php

									if($feedbacks)

									{

										$i=0;

										foreach($feedbacks as $feedback)

										{

										?>

											<tr class="<?php echo $feedback->id;?>">

													<td><?php echo ++$i;?></td>

													<td><?php echo ucfirst($feedback->full_name);?></td>

													<td><?php echo $feedback->email;?></td>

                                                   <td><?php echo $feedback->position;?></td>

													<td><?php echo $feedback->company;?></td>

                                                    <td><?php echo $feedback->mobile;?></td>
                                                    <td><?php echo date('d-m-Y',strtotime($feedback->created_at));?></td>

                                                <td>
													
													<?php echo substr($feedback->message,'0','15');?>
                                                    <a href="javascript:void(0);" title="View Message" onClick="viewContctMessage(<?php echo $feedback->id; ?>);">

													<i class="md-icon material-icons">visibility</i></a>
                                                   
                                                    </td>

                                            <?php if(viewEditDeleteRights('Contact Us Requests','delete')) { ?>
                                                    <td>
													<a href="javascript:void(0);" class="uk-margin-left delete" onclick="deleteRecord(<?php echo $feedback->id;?>,'admin/contact_us/contact_delete','');" title="Delete feedback">
                                                        <i class="material-icons md-24 delete">&#xE872;</i>
                                                    </a>
                                                    </td>
                                            <?php } ?>

											</tr>

<button class="md-btn" id="contact_us-message-button-<?php echo $feedback->id; ?>" data-uk-modal="{target:'#contactUsMsg_<?php echo $feedback->id; ?>'}" style="display:none;">Open</button>
	<div class="uk-modal" id="contactUsMsg_<?php echo $feedback->id; ?>">   
		 <div class="uk-modal-dialog">         
		 <button type="button" class="uk-modal-close uk-close"></button>      
		 <h2 id="contact_us-message-heading"></h2> 
		 <div id="contact_us-message"><?php echo $feedback->message; ?></div>   
		 </div>   
	</div>

										<?php

										}
	

									}

									

									?>

                                           

                                    </tbody>

                                </table>

                            </div>

                            

                        </div>

                    </div>

                </div>

            </div>



        </div>

    </div>

       <!-- /.content-wrapper --> 