<div id="page_content">
    <div id="page_content_inner">
        <form action="<?php echo base_url(); ?>admin/contact_us/action" method="post" onsubmit="return false"
              class="ajax_form" enctype="multipart/form-data">
            <div class="md-card">
                <div class="md-card-content"><h3 class="heading_a">Edit Contact us</h3><br>
                    <div class="uk-grid" data-uk-grid-margin><input type="hidden" name="form_type" value="update">
                        <input type="hidden" name="id" value="<?php echo $contact_us->id; ?>">
                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row"><label>English Title</label> <input type="text" class="md-input"
                                                                                         value="<?php echo $contact_us->eng_title; ?>"
                                                                                         name="eng_title"/></div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row"><label>Arabic Title</label> <input type="text" class="md-input"
                                                                                        value="<?php echo $contact_us->arb_title; ?>"
                                                                                        name="arb_title"/></div>
                        </div>
                        <div class="uk-width-medium-1-2"><label>English Address</label>
                            <div class="uk-form-row"><textarea cols="30" rows="4" class="md-input eng_text"
                                                               id="eng_address"
                                                               name="eng_address"><?php echo $contact_us->eng_address; ?></textarea>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2"><label>Arabic Address</label>
                            <div class="uk-form-row"><textarea cols="30" rows="4" class="md-input arb_text"
                                                               id="arb_address"
                                                               name="arb_address"><?php echo $contact_us->arb_address; ?></textarea>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2"><label>English Street Address</label>
                            <div class="uk-form-row"><textarea cols="30" rows="4" class="md-input eng_text"
                                                               id="eng_street"
                                                               name="eng_street"><?php echo $contact_us->eng_street; ?></textarea>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2"><label>Arabic Street Address</label>
                            <div class="uk-form-row"><textarea cols="30" rows="4" class="md-input arb_text"
                                                               id="arb_street"
                                                               name="arb_street"><?php echo $contact_us->arb_street; ?></textarea>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row"><label>Tele Phone</label> <input type="text" class="md-input"
                                                                                      value="<?php echo $contact_us->telephone; ?>"
                                                                                      name="telephone"/></div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row"><label>Fax</label> <input type="text" class="md-input"
                                                                               value="<?php echo $contact_us->fax; ?>"
                                                                               name="fax"/></div>
                        </div> <?php //echo "<pre>"; print_r($locations); exit;?>
                        <div class="uk-width-large-1-1"><h3 class="heading_a"> Choose Map Location </h3>
                            <div class="append_location">         <?php if (count($locations) > 0) {
                                    foreach ($locations as $location) { ?>
                                        <div class="uk-grid form_section" id="im_form_row"
                                             style="display: block !important;">
                                            <div class="uk-width-1-2"><input id="file_upload-select" type="text"
                                                                             value="<?php echo $location->lat_long; ?>"
                                                                             class="md-input location" data-exist=""
                                                                             name="lat_long[]"></div>
                                            <span class="uk-input-group-addon">                 <a href="#"
                                                                                                   class="btnSectionRemove">                     <i
                                                            class="material-icons md-24"></i>                 </a>             </span>
                                        </div>             <?php }
                                } ?>         </div>
                            <div class="uk-grid form_section" id="im_form_row">
                                <div class="uk-width-1-2"><input id="file_upload-select" type="text"
                                                                 class="md-input location" data-exist=""
                                                                 name="lat_long[]"></div>
                                <span class="uk-input-group-addon">                    <a href="#"
                                                                                          class="btnSectionClone"
                                                                                          data-section-clone="#im_form_row">                        <i
                                                class="material-icons md-24">&#xE146;</i>                    </a>                </span>
                            </div>
                            <br> <span class="uk-input-group-addon">Click + to add locations             <a
                                        href="javascript:void(0);" class="btnSectionClone"
                                        data-section-clone="#im_form_row"><i class="material-icons md-24"> &#xE146;</i></a>            </span>
                        </div>
                    </div>
                </div>
            </div>
        </form> <?php if (viewEditDeleteRights('Contact Us', 'edit')) { ?>
            <div class="md-fab-wrapper"><a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);"
                                           id=""> <i class="material-icons">&#xE161;</i> </a></div>    <?php } ?>
        <!-- light box for image -->
        <div class="uk-modal" id="modal_lightbox">
            <div class="uk-modal-dialog uk-modal-dialog-lightbox">
                <button type="button" class="uk-modal-close uk-close uk-close-alt"></button>
                <img src="<?php echo base_url() . 'uploads/images/aboutus/' . ($contact_us->banner_image != '' ? $contact_us->banner_image : 'no_imge.png'); ?>"
                     alt=""/></div>
        </div>     <!-- end light box for image -->          </div>
</div>