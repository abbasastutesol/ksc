<!-- Content Wrapper. Contains page content -->
      <div id="page_content">
        <div id="page_content_inner">
            <?php if(viewEditDeleteRights('Menu Setting','add')) { ?>
		    <a href="<?php echo base_url();?>admin/settings/add_menu_bar" class="md-btn"> Add</a>
            <?php } ?>
			<div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                        	<div class="uk-overflow-container">

                                <table class="uk-table uk-table-align-vertical listing dt_default">
                                    <thead>
                                        <tr>
                                             <th>Sr#</th>

                                              <th>English Title</th>
											  <th>Arabic Title</th>
                                                <th>Type</th>
                                              <th>English Link</th>
                                              <th>Status</th>

                                            <?php if(viewEditDeleteRights('Menu Setting','edit') || viewEditDeleteRights('Menu Setting','delete')) { ?>

                                              <th class="nosort">Action</th>
                                            <?php } ?>
                                        </tr>
                                    </thead>
                                    <tbody>
									<?php
									if($menu_bars)
									{
										$i=0;

										foreach($menu_bars as $menu)
										{
										?>
											<tr class="<?php echo $menu->id;?>">
													<td><?php echo ++$i;?></td>
													<td><?php echo $menu->eng_menu_title; ?></td>
													<td><?php echo $menu->arb_menu_title; ?></td>
                                                    <td><strong><?php echo $menu->menu_type; ?></strong></td>
                                                    <td><?php echo $menu->eng_link; ?></td>
                                                <td>
                                                    <?php if($menu->is_active == 1) { ?>
                                                        <i class="md-icon material-icons green">check_circle</i>
                                                    <?php } else{ ?>
                                                        <i class="md-icon material-icons black">highlight_off</i>

                                                    <?php } ?>
                                                </td>

                                            <?php if(viewEditDeleteRights('Menu Setting','edit') || viewEditDeleteRights('Menu Setting','delete')) { ?>
													<td>
                                            <?php if(viewEditDeleteRights('Menu Setting','edit') ) { ?>
                                                        <a href="<?php echo base_url().'admin/settings/edit_menu/'.$menu->id;?>" title="Edit Menu Bar">
													<i class="md-icon material-icons">&#xE254;</i>
													</a>
                                                <?php } ?>
                                            <?php if(viewEditDeleteRights('Menu Setting','delete')) { ?>
													<a href="javascript:void(0);" class="uk-margin-left delete" onclick="deleteRecord(<?php echo $menu->id;?>,'admin/settings/action','');" title="Delete Menu">
                                                        <i class="material-icons md-24 delete">&#xE872;</i>
                                                    </a>
                                                <?php } ?>

                                                    </td>
                                            <?php } ?>


											</tr>
										<?php
										}
									}

									?>

                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
       <!-- /.content-wrapper -->