<div id="page_content">
        <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
            <h1 id="product_edit_name">Add Menu Bar</h1>
            <span class="uk-text-muted uk-text-upper uk-text-small" id="product_edit_sn"></span>
        </div>
        <div id="page_content_inner">
            <form action="<?php echo base_url(); ?>admin/settings/action" method="post" onsubmit="return false" class="uk-form-stacked ajax_form" id="product_edit_form">
				<input type="hidden" name="form_type" value="menu_bar">
                <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
                    
                    <div class="uk-width-xLarge-10-10  uk-width-large-10-10">
                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text">
                                    Add Menu Bar
                                </h3>
                            </div>
                            <div class="md-card-content large-padding">
                                <div class="uk-grid uk-grid-divider uk-grid-medium" data-uk-grid-margin>
                                	<div class="uk-width-large-1-2">
                                     </div>

                                    <div class="uk-width-large-1-2">
                                        <div class="uk-form-row">

                                        </div>
                                     </div>
                                    <div class="uk-width-large-1-2">

                                        <label for="eng_title">English Title</label>
										<div class="uk-form-row">
                                            <input type="text" class="md-input" name="eng_menu_title" id="eng_menu_title" value="">
                                        </div>

                                    </div>


                                    <div class="uk-width-large-1-2">

                                        <label for="arb_menu_title">Arabic Title</label>
                                        <div class="uk-form-row">

                                            <input type="text" class="md-input" name="arb_menu_title" id="arb_menu_title" value="">
                                        </div>

                                    </div>

                                    <div class="uk-width-large-1-2">

                                        <label for="eng_link">English Link</label>
                                        <div class="uk-form-row">

                                            <input type="text" class="md-input" id="eng_link" name="eng_link" value="">
                                        </div>



                                    </div>

                                    <div class="uk-width-large-1-2">

                                        <label for="arb_link">Arabic Link</label>
                                        <div class="uk-form-row">

                                            <input type="text" class="md-input" id="arb_link" name="arb_link" value="">
                                        </div>



                                    </div>


                                    <div class="uk-width-large-1-2">

                                        <label for="menu_type">Menu Type</label>
                                        <div class="uk-form-row">

                                            <select id="select_demo_4" name="menu_type" data-md-selectize>
                                                <option value="header" >Header</option>
                                                <option value="footer" >Footer</option>

                                            </select>
                                        </div>

                                    </div>

                                    <div class="uk-width-large-1-2">
                                        <input type="checkbox" data-switchery data-switchery-size="large" checked id="is_active" value="1" name="is_active" />

                                        <label for="is_active" class="inline-label">Active</label>
                                    </div>

                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </form>

        </div>
    </div>

    <div class="md-fab-wrapper">
        <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">
            <i class="material-icons">&#xE161;</i>
        </a>
    </div>
    
    <div class="md-fab-wrapper" style="right:95px;">
        <a class="md-fab md-fab-primary" href="<?php echo base_url();?>admin/settings/menu_bar" id="">
            <i class="material-icons">keyboard_backspace</i>
        </a>
    </div>
    