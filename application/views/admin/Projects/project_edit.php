<div id="page_content"> 
 <div id="page_content_inner">   
 <form action="<?php echo base_url(); ?>admin/Projects/action" method="post" onsubmit="return false" class="ajax_form" enctype="multipart/form-data">  
 <div class="md-card">   
 <div class="md-card-content"> 
 <h3 class="heading_a">Edit Project</h3><br>	
 <div class="uk-grid" data-uk-grid-margin>			
 <input type="hidden" name="form_type" value="update">		
 <input type="hidden" name="tpl_name" value="projects">	
 <input type="hidden" name="id" value="<?php echo $record[0]->id;?>">
<input type="hidden" name="id" value="<?php echo $record[0]->id;?>">
<input type="hidden" name="banner_image_old" value="<?php echo $record[0]->banner_image;?>"> 
 <input type="hidden" name="gallery_image_old" value="<?php echo $record[0]->gallery_image;?>"> 
   
   
 <div class="uk-width-medium-1-2">     
 <div class="uk-form-row">   
 <label>English Page Title</label>    
 <input type="text" class="md-input" value="<?php echo $record[0]->eng_title;?>" name="eng_title" />    
 </div>  
<label>English Description</label>            
                     <div class="uk-form-row">                            <textarea cols="30" rows="4" id="eng_description" class="md-input eng_text" name="eng_description"><?php echo $record[0]->eng_description;?></textarea>            </div> 
 </div>  
 
 <div class="uk-width-medium-1-2">           
 <div class="uk-form-row">    
 <label>Arabic Page Title</label>      
 <input type="text" class="md-input" value="<?php echo $record[0]->arb_title;?>" name="arb_title" />            
 </div> 
 <label>Arabic Description</label>            
                     <div class="uk-form-row">                            <textarea cols="30" rows="4" id="arb_description" class="md-input arb_text" name="arb_description"><?php echo $record[0]->arb_description;?></textarea>            </div> 
 </div>         
       
 
					 	<div class="uk-width-xLarge-2-10 uk-width-large-3-10">
                                    <div class="uk-margin-bottom uk-text-center">
                                        <div class="uk-slidenav-position proSliderBox" data-uk-slideshow="{animation:'scale'}">
                                            <ul class="uk-slideshow">

                                                <?php $count = 1;
                                                
                                                foreach($imgs as $img){
                                                  
                                                        $image = base_url() . 'assets/frontend/images/' . $img;
                                                         
                                                        ?>
                                                        <li>
                                                            <img src="<?php echo $image; ?>" alt="">
                                                            <p class="topNo"><?php echo $count; ?>/<?php  echo count($total)-1; ?></p>
                                                            <button type="button" class=" del_img uk-modal-close uk-close uk-close-alt uk-position-absolute" id = "<?php echo $record[0]->id; ?>" imges = "<?php echo $img;?>"></button>
                                                        </li>

                                                        <?php
                                                        $count++;
                                                  
                                                }

                                                ?>

                                            </ul>
                                            <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
                                            <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideshow-item="next"></a>

                                            <div class="uk-form-file uploadImages">
                                                <button class="btn ioudBtn green" type="button">
                                                    <i class="material-icons">crop_original</i> Upload More images
                                                </button>
                                                <input id="form-file" type="file" name="image[]" multiple>
                                            </div>


                                            <br>
                                            <br>


                                        </div>
                                    </div>
                                </div>
 

						
						
 </div>        
 </div>  
 </div>  
    <div class="md-card">
            <div class="md-card-toolbar">
               <h3 class="md-card-toolbar-heading-text">              Keyword          </h3>
            </div>
             
            <div class="md-card-content large-padding">
               <div class="uk-grid uk-grid-divider uk-grid-medium" data-uk-grid-margin>
                  <div class="uk-width-medium-1-2">
                     <div class="uk-form-row">                    <label>English Meta Title</label>                    <input type="text" class="md-input" value="<?php echo $record[0]->eng_meta_title;?>" name="eng_meta_title" />                  </div>
                  </div>
                  <div class="uk-width-medium-1-2">
                     <div class="uk-form-row">                    <label>Arabic Meta Title</label>                    <input type="text" class="md-input" value="<?php echo $record[0]->arb_meta_title;?>" name="arb_meta_title" />                  </div>
                  </div>
                  <div class="uk-width-medium-1-2">
                     <div class="uk-form-row">                    <label>Englishn Meta Description</label>                    <textarea cols="30" rows="4" class="md-input" name="eng_meta_description"><?php echo $record[0]->eng_meta_description;?></textarea>                  </div>
                  </div>
                  <div class="uk-width-medium-1-2">
                     <div class="uk-form-row">                    <label>Arabic Meta Description</label>                    <textarea cols="30" rows="4" class="md-input" name="arb_meta_description"><?php echo $record[0]->arb_meta_description;?></textarea>                  </div>
                  </div>
                  <div class="uk-width-medium-1-2">
                     <div class="uk-form-row">                    <label>Englishn Meta Keyword</label>                    <textarea cols="30" rows="4" class="md-input" name="eng_meta_keyword"><?php echo $record[0]->eng_meta_keyword;?></textarea>                  </div>
                  </div>
                  <div class="uk-width-medium-1-2">
                     <div class="uk-form-row">                    <label>Arabic Meta Keyword</label>                    <textarea cols="30" rows="4" class="md-input" name="arb_meta_keyword"><?php echo $record[0]->arb_meta_keyword;?></textarea>                  </div>
                  </div>
               </div>
            </div>
         </div>
 </form>      
 <?php //if(viewEditDeleteRights('Contact Us','edit')) { ?> 
 <div class="md-fab-wrapper">    
 <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">   
 <i class="material-icons">&#xE161;</i>      
 </a>  
 </div>   
 <?php //} ?>
 <!-- light box for image -->
 <div class="uk-modal" id="modal_lightbox"> 
 <div class="uk-modal-dialog uk-modal-dialog-lightbox"> 
 <button type="button" class="uk-modal-close uk-close uk-close-alt"></button> 
<?php foreach($imgs as $img){  ?> 
<img src="<?php echo base_url().'assets/frontend/images/'.($img != '' ? $img : 'no_imge.png'); ?>" alt=""/>  <?php }?>
 </div>     
 </div>	
 <!---<div class="uk-modal" id="modal_lightbox2"> 
 <div class="uk-modal-dialog uk-modal-dialog-lightbox"> 
 <button type="button" class="uk-modal-close uk-close uk-close-alt"></button>   
 <img src="<?php //echo base_url().'assets/frontend/images/'.($company_profile->gallery_image != '' ? $company_profile->gallery_image : 'no_imge.png'); ?>" alt=""/>  
 </div>     
 </div>	--->
 <!-- end light box for image -->         
 </div>
</div>

