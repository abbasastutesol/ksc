<div id="page_content"> 
 <div id="page_content_inner">   
 <form action="<?php echo base_url(); ?>admin/Projects/action" method="post" onsubmit="return false" class="ajax_form" enctype="multipart/form-data">  
 <div class="md-card">   
 <div class="md-card-content"> 
 <h3 class="heading_a">Add Project Gallery Images</h3><br>	
 <div class="uk-grid" data-uk-grid-margin>		
 
 <input type="hidden" name="form_type" value="save">		
 <input type="hidden" name="tpl_name" value="projects_gallery">
 
   
   
   
 <div class="uk-width-medium-1-2">     
 <div class="uk-form-row">   
 <label>English Page Title</label>    
 <input type="text" class="md-input" value="<?php //echo $company_profile->eng_page_title;?>" name="eng_title" />    
 </div>  
<label>English Description</label>            
                     <div class="uk-form-row">                            <textarea cols="30" rows="4" id="eng_description" class="md-input eng_text" name="eng_description"><?php //echo $record[0]->eng_description;?></textarea>            </div> 
 </div>  
 
 <div class="uk-width-medium-1-2">           
 <div class="uk-form-row">    
 <label>Arabic Page Title</label>      
 <input type="text" class="md-input" value="<?php //echo $company_profile->arb_page_title;?>" name="arb_title" />            
 </div> 
 <label>Arabic Description</label>            
                     <div class="uk-form-row">                            <textarea cols="30" rows="4" id="arb_description" class="md-input arb_text" name="arb_description"><?php //echo $record[0]->arb_description;?></textarea>            </div> 
 </div>         
       
 <!---<div class="uk-width-medium-1-1">                    
 <div class="uk-form-row">    
 <label>Banner Image</label>       
 <img src="<?php //echo base_url().'assets/frontend/images/'.($company_profile->banner_image != '' ? $company_profile->banner_image : 'no_imge.png'); ?>" alt="" data-uk-modal="{target:'#modal_lightbox'}" width="100">      
 </div>      
 </div>--->  	
 <div class="uk-width-medium-1-2">
            <label>Projects</label>
                <div class="uk-form-row">

                    <select class="md-input " id=""  name="project_id">
                        <option value="">Select Projects</option>
						<?php foreach($projects as $project){?>
                                <option value="<?php echo $project->id;?>"><?php echo $project->eng_title; ?></option>
						<?php } ?>
                    </select>
                </div>
           
			</div>
			
 <div class="uk-width-large-1-2">         
 <h3 class="heading_a">         
 Upload gallery Image           
 </h3>                 
 <span style="color:red">(600 * 450 pixels)</span>     
 <div class="uk-grid">   
 <div class="uk-width-1-1">        
  <div class="uk-slidenav-position proSliderBox" data-uk-slideshow="{animation:'scale'}">

                                                <div class="uk-form-file uploadImages">
                                                    <button class="btn ioudBtn green" type="button">
                                                        <i class="material-icons">crop_original</i> Upload images
                                                    </button>
                                                    <input id="form-file" type="file" name="gallery_image">
                                                </div>

                                            </div>                  
 <div id="file_upload-progressbar" class="uk-progress uk-hidden">      
 <div class="uk-progress-bar" style="width:0">0%</div>                    
 </div>                
 </div>               
 </div>         
 </div>  

<!---<div class="uk-width-large-1-1">         
 <h3 class="heading_a">         
Upload gallery Image                                                   
 </h3>                 
 <span style="color:red">(600 * 450 pixels)</span>     
 <div class="uk-grid">   
 <div class="uk-width-1-1">        
  <div class="uk-slidenav-position proSliderBox" data-uk-slideshow="{animation:'scale'}">

                                                <div class="uk-form-file uploadImages">
                                                    <button class="btn ioudBtn green" type="button">
                                                        <i class="material-icons">crop_original</i> Upload gallery images
                                                    </button>
                                                    <input id="form-file" type="file" name="gallery_image[]" multiple>
                                                </div>

                                            </div>                  
 <div id="file_upload-progressbar" class="uk-progress uk-hidden">      
 <div class="uk-progress-bar" style="width:0">0%</div>                    
 </div>                
 </div>               
 </div>         
 </div> --->
 </div>        
 </div>  
 </div>  
   
 </form>      
 <?php //if(viewEditDeleteRights('Contact Us','edit')) { ?> 
 <div class="md-fab-wrapper">    
 <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">   
 <i class="material-icons">&#xE161;</i>      
 </a>  
 </div>   
 <?php //} ?>
 <!-- light box for image -->
 <div class="uk-modal" id="modal_lightbox"> 
 <div class="uk-modal-dialog uk-modal-dialog-lightbox"> 
 <button type="button" class="uk-modal-close uk-close uk-close-alt"></button>   
 <img src="<?php //echo base_url().'assets/frontend/images/'.($company_profile->banner_image != '' ? $company_profile->banner_image : 'no_imge.png'); ?>" alt=""/>  
 </div>     
 </div>	
 <!-- end light box for image -->         
 </div>
</div>

