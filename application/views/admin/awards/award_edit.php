<div id="page_content">
    <div id="page_content_inner">
        <form action="<?php echo base_url(); ?>admin/Awards/action" method="post" onsubmit="return false" class="ajax_form">
            <div class="md-card">
                <div class="md-card-content">
                    <h3 class="heading_a">Edit Home Slider</h3><br>
                    <div class="uk-grid" data-uk-grid-margin>

                        <input type="hidden" name="form_type" value="update">
						<input type="hidden" name="tpl_name" value="awards">
                        <input type="hidden" name="id" value="<?php echo $records[0]->id; ?>">

                        <div class="uk-width-medium-1-2">

                            <label>English Title</label>
                            <div class="uk-form-row">
                                <input type="text" class="md-input" name="eng_title" value="<?php echo $records[0]->eng_title; ?>">
                            </div>

						
							<!--<br>
            
						   <label>Slider for Home Page</label>

							   <div class="uk-width-medium-1-2">

								   <input type="checkbox" id="home_page_slider" data-switchery="true" data-switchery-size="large" id="active" name="is_home" value="<?php //echo $slider->is_home; ?>" <?php //echo ($slider->is_home == '1'?"checked":"");?>>

							   </div> -->
                        </div>
						
                        <div class="uk-width-medium-1-2">
						  
						  <label>Arabic Title</label>
                            <div class="uk-form-row">
                                <input type="text" class="md-input" name="arb_title" value="<?php echo $records[0]->arb_title	; ?>">
                            </div>
						  
						</div>
						<div class="uk-width-medium-1-1">           
						 <div class="uk-form-row">    
						 <label>Award year</label>      
						<select name="award_year" id="yearpicker">
						<option value="<?php echo $records[0]->award_year; ?>" selected = "selected"><?php echo $records[0]->award_year; ?></option></select>           
						 </div>        
						 </div>
						
                        <div class="uk-width-medium-1-1">
                            <label>Award Image</label>
                            <img src="<?php echo base_url().'assets/frontend/images/'.($records[0]->award_image != '' ? $records[0]->award_image : 'no_imge.png'); ?>" alt="Client logo" data-uk-modal="{target:'#modal_lightbox'}" width="100">
                        </div>

                        <div class="uk-width-large-1-1">
                            <h3 class="heading_a">
                                Upload Award Image

                            </h3>
                            <span style="color:red">(360 * 250 pixels)</span>
                            <div class="uk-grid">
                                <div class="uk-width-1-1">
                                    <div id="file_upload-drop" class="uk-file-upload">
                                        <a class="uk-form-file md-btn">choose file<input id="file_upload-select" class="images_upload" type="file" name="image"></a>
                                    </div>
                                    <div id="file_upload-progressbar" class="uk-progress uk-hidden">
                                        <div class="uk-progress-bar" style="width:0">0%</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                       

                        


                    </div>

                </div>
            </div>

<div class="md-card">
            <div class="md-card-toolbar">
               <h3 class="md-card-toolbar-heading-text">              Keyword          </h3>
            </div>
             
            <div class="md-card-content large-padding">
               <div class="uk-grid uk-grid-divider uk-grid-medium" data-uk-grid-margin>
                  <div class="uk-width-medium-1-2">
                     <div class="uk-form-row">                    <label>English Meta Title</label>                    <input type="text" class="md-input" value="<?php echo $records[0]->eng_meta_title;?>" name="eng_meta_title" />                  </div>
                  </div>
                  <div class="uk-width-medium-1-2">
                     <div class="uk-form-row">                    <label>Arabic Meta Title</label>                    <input type="text" class="md-input" value="<?php echo $records[0]->arb_meta_title;?>" name="arb_meta_title" />                  </div>
                  </div>
                  <div class="uk-width-medium-1-2">
                     <div class="uk-form-row">                    <label>Englishn Meta Description</label>                    <textarea cols="30" rows="4" class="md-input" name="eng_meta_description"><?php echo $records[0]->eng_meta_description;?></textarea>                  </div>
                  </div>
                  <div class="uk-width-medium-1-2">
                     <div class="uk-form-row">                    <label>Arabic Meta Description</label>                    <textarea cols="30" rows="4" class="md-input" name="arb_meta_description"><?php echo $records[0]->arb_meta_description;?></textarea>                  </div>
                  </div>
                  <div class="uk-width-medium-1-2">
                     <div class="uk-form-row">                    <label>Englishn Meta Keyword</label>                    <textarea cols="30" rows="4" class="md-input" name="eng_meta_keyword"><?php echo $records[0]->eng_meta_keyword;?></textarea>                  </div>
                  </div>
                  <div class="uk-width-medium-1-2">
                     <div class="uk-form-row">                    <label>Arabic Meta Keyword</label>                    <textarea cols="30" rows="4" class="md-input" name="arb_meta_keyword"><?php echo $records[0]->arb_meta_keyword;?></textarea>                  </div>
                  </div>
               </div>
            </div>
         </div>

        </form>


        <div class="md-fab-wrapper">
            <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">
                <i class="material-icons">&#xE161;</i>
            </a>
        </div>

        <div class="md-fab-wrapper" style="right:95px;">
            <a class="md-fab md-fab-primary" href="<?php echo base_url();?>admin/home/slider" id="">
                <i class="material-icons">keyboard_backspace</i>
            </a>
        </div>


    </div>
</div>


<!-- light box for image -->
<div class="uk-modal" id="modal_lightbox">
    <div class="uk-modal-dialog uk-modal-dialog-lightbox">
        <button type="button" class="uk-modal-close uk-close uk-close-alt"></button>
    <img src="<?php echo base_url().'assets/frontend/images/'.($records[0]->award_image != '' ? $records[0]->award_image : 'no_imge.png'); ?>" alt=""/>

    </div>
</div>
<!-- end light box for image -->
