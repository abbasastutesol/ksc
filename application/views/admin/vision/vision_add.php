<div id="page_content"> 
 <div id="page_content_inner">   
 <form action="<?php echo base_url(); ?>admin/Awards/action" method="post" onsubmit="return false" class="ajax_form" enctype="multipart/form-data">  
 <div class="md-card">   
 <div class="md-card-content"> 
 <h3 class="heading_a">Add Awards</h3><br>	
 <div class="uk-grid" data-uk-grid-margin>			
 <input type="hidden" name="form_type" value="save">		
 <!--<input type="hidden" name="id" value="<?php //echo $company_profile->id;?>">-->	
   
   
   
 <div class="uk-width-medium-1-2">     
 <div class="uk-form-row">   
 <label>English Page Title</label>    
 <input type="text" class="md-input" value="<?php //echo $company_profile->eng_page_title;?>" name="eng_title" />    
 </div>          
 </div>  
 
 <div class="uk-width-medium-1-2">           
 <div class="uk-form-row">    
 <label>Arabic Page Title</label>      
 <input type="text" class="md-input" value="<?php //echo $company_profile->arb_page_title;?>" name="arb_title" />            
 </div>        
 </div>         
        
 <!---<div class="uk-width-medium-1-1">                    
 <div class="uk-form-row">    
 <label>Banner Image</label>       
 <img src="<?php //echo base_url().'assets/frontend/images/'.($company_profile->banner_image != '' ? $company_profile->banner_image : 'no_imge.png'); ?>" alt="" data-uk-modal="{target:'#modal_lightbox'}" width="100">      
 </div>      
 </div>--->  	
 <div class="uk-width-large-1-1">         
 <h3 class="heading_a">         
 Upload Award Image           
 </h3>                 
 <span style="color:red">(1400 * 365 pixels)</span>     
 <div class="uk-grid">   
 <div class="uk-width-1-1">        
 <div id="file_upload-drop" class="uk-file-upload">       
 <a class="uk-form-file md-btn">choose file<input id="file_upload-select" class="images_upload" type="file" name="image"></a>   
 </div>                  
 <div id="file_upload-progressbar" class="uk-progress uk-hidden">      
 <div class="uk-progress-bar" style="width:0">0%</div>                    
 </div>                
 </div>               
 </div>         
 </div>             
 </div>        
 </div>  
 </div>  
    
 </form>      
 <?php //if(viewEditDeleteRights('Contact Us','edit')) { ?> 
 <div class="md-fab-wrapper">    
 <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">   
 <i class="material-icons">&#xE161;</i>      
 </a>  
 </div>   
 <?php //} ?>
 <!-- light box for image -->
 <div class="uk-modal" id="modal_lightbox"> 
 <div class="uk-modal-dialog uk-modal-dialog-lightbox"> 
 <button type="button" class="uk-modal-close uk-close uk-close-alt"></button>   
 <img src="<?php //echo base_url().'assets/frontend/images/'.($company_profile->banner_image != '' ? $company_profile->banner_image : 'no_imge.png'); ?>" alt=""/>  
 </div>     
 </div>	
 <!-- end light box for image -->         
 </div>
</div>

