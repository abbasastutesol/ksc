<div id="page_content">
  <div id="page_content_inner">
    <div class="md-card">
          <div class="md-card-content">
              <h3 class="heading_a">Products Viewed Report</h3>

              <div class="uk-grid" data-uk-grid-margin>
                  <div class="uk-width-1-1">
                      <div class="uk-overflow-container">
                          <table class="uk-table uk-table-align-vertical listing dt_default">
                              <thead>
                              <tr>
                                  <th class="nosort">Product Name</th>
                                  <th class="nosort">Category Name</th>
                                  <th class="nosort"> Viewed</th>
                                  <th class="nosort"> Percent</th>

                              </tr>
                              </thead>
                              <tbody>

                              <?php foreach ($viewed as $view){ ?>
                              <tr>

                                  <td><?php echo $view['product_name']; ?></td>
                                  <td><?php echo $view['cat_name']; ?></td>
                                  <td><?php echo $view['viewed']; ?></td>
                                  <td><?php echo $view['percent']; ?></td>
                              </tr>
                              <?php } ?>

                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>

          </div>
      </div>

  </div>
</div>

<div class="md-fab-wrapper" style="right:95px;">
        <a class="md-fab md-fab-primary" href="<?php echo base_url();?>admin/category" id="">
            <i class="material-icons">keyboard_backspace</i>
        </a>
    </div>