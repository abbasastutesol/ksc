<div id="page_content">
  <div id="page_content_inner">
    <div class="md-card">
      <div class="md-card-content">
        <h3 class="heading_a">Coupon Report</h3>
          <br>
        <form action="<?php echo base_url(); ?>admin/report/coupon" method="post" >
		<div class="uk-grid" data-uk-grid-margin>
          <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
              <label>Start Date</label>
              <input type="text" class="md-input" id="start_date" value="<?php if($start_date != '') echo date('d/m/Y',strtotime($start_date)); ?>" name="start_date" data-uk-datepicker="{format:'DD/MM/YYYY'}" />
            </div>

          </div>

            <div class="uk-width-medium-1-2">
                <div class="uk-form-row">
                    <label>End Date</label>
                    <input type="text" class="md-input" id="end_date" value="<?php if($end_date != '') echo date('d/m/Y',strtotime($end_date)); ?>" name="end_date"  data-uk-datepicker="{format:'DD/MM/YYYY'}"/>

                </div>

                <br>
                <div class="uk-form-row" align="right">
                    <input type="submit" class="md-btn md-fab-primary" value="Filter" name="submit">
                </div>
            </div>
        </div>
       </form> 
      </div>

          <div class="md-card-content">

              <div class="uk-grid" data-uk-grid-margin>
                  <div class="uk-width-1-1">
                      <div class="uk-overflow-container">
                          <table class="uk-table uk-table-align-vertical listing dt_default">
                              <thead>
                              <tr>

                                  <th class="nosort"> Coupon Name</th>
                                  <th class="nosort">Coupon Code</th>
                                  <th class="nosort">No.Orders</th>
                                  <th class="nosort">Total</th>
                              </tr>
                              </thead>
                              <tbody>

                              <?php foreach ($coupon as $coup){ ?>
                              <tr>
                                  <td><?php echo $coup['coupon_name']; ?></td>
                                  <td><?php echo $coup['code']; ?></td>
                                  <td><?php echo $coup['orders']; ?></td>
                                  <td><?php echo number_format($coup['total'],2,".",","); ?></td>
                              </tr>
                              <?php } ?>

                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>

          </div>
      </div>

  </div>
</div>

<div class="md-fab-wrapper" style="right:95px;">
        <a class="md-fab md-fab-primary" href="<?php echo base_url();?>admin/category" id="">
            <i class="material-icons">keyboard_backspace</i>
        </a>
    </div>