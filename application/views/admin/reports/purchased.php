<div id="page_content">
  <div id="page_content_inner">
    <div class="md-card">
      <div class="md-card-content">
        <h3 class="heading_a">Product Purchased List</h3>
          <br>
        <form action="<?php echo base_url(); ?>admin/report/purchased" method="post" >
		<div class="uk-grid" data-uk-grid-margin>
          <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
              <label>Start Date</label>
              <input type="text" class="md-input" id="start_date" value="<?php if($start_date != '') echo date('d/m/Y',strtotime($start_date)); ?>" name="start_date" data-uk-datepicker="{format:'DD/MM/YYYY'}" />
            </div>

          </div>

         <!-- <div class="uk-width-medium-1-2">
            <div class="uk-form-row">

                <select id="select_demo_4" name="group" data-md-selectize>
                    <option value="year" <?php /*echo ($group == 'year' ? 'selected' : ''); */?>>Years</option>
                    <option value="month" <?php /*echo ($group == 'month' ? 'selected' : ''); */?>>Months</option>
                    <option value="week" <?php /*echo ($group == 'week' ? 'selected' : ''); */?>>Weeks</option>
                    <option value="day" <?php /*echo ($group == 'day' ? 'selected' : ''); */?>>Days</option>

                </select>
            </div>

          </div>
            <br>
            <br>
            <br>-->
            <div class="uk-width-medium-1-2">
                <div class="uk-form-row">
                    <label>End Date</label>
                    <input type="text" class="md-input" id="end_date" value="<?php if($end_date != '') echo date('d/m/Y',strtotime($end_date)); ?>" name="end_date"  data-uk-datepicker="{format:'DD/MM/YYYY'}"/>

                </div>

                <br>
                <div class="uk-form-row" align="right">
                    <input type="submit" class="md-btn md-fab-primary" value="Filter" name="submit">
                </div>
            </div>


        </div>
       </form> 
      </div>



          <div class="md-card-content">


              <div class="uk-grid" data-uk-grid-margin>
                  <div class="uk-width-1-1">
                      <div class="uk-overflow-container">
                          <table class="uk-table uk-table-align-vertical listing dt_default">
                              <thead>
                              <tr>

                                  <th class="nosort"> Product Name</th>
                                  <th class="nosort">Category Name</th>
                                  <th class="nosort">Quantity</th>
                                  <th class="nosort">Total</th>
                              </tr>
                              </thead>
                              <tbody>

                              <?php foreach ($purchased as $purchase){ ?>
                              <tr>
                                  <td><?php echo $purchase['product']; ?></td>
                                  <td><?php $cats =  getCategoriesById($purchase['category_id']);
                                   echo $cats['eng_name'].$purchase['category_id'];
                                  ?></td>
                                  <td><?php echo $purchase['quantity']; ?></td>
                                  <td><?php echo $purchase['total']; ?></td>
                              </tr>
                              <?php } ?>

                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>

          </div>
      </div>

  </div>
</div>

<div class="md-fab-wrapper" style="right:95px;">
        <a class="md-fab md-fab-primary" href="<?php echo base_url();?>admin/category" id="">
            <i class="material-icons">keyboard_backspace</i>
        </a>
    </div>