 <!-- Content Wrapper. Contains page content -->
      <div id="page_content">
        <div id="page_content_inner">
			<div class="md-card">

                <div class="md-card-toolbar">
                    <h3 class="md-card-toolbar-heading-text">
                        Logs
                    </h3>
                </div>

                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                        	<div class="uk-overflow-container"> 

                                <table class="uk-table uk-table-align-vertical listing dt_default">
                                    <thead>
                                        <tr>
                                             <th class="nosort">Sr#</th>
                                              <th class="nosort">User Name</th>
                                              <th class="nosort">Activity</th>
											  <th class="nosort">Section</th>

                                              <th class="nosort">Action Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
									<?php
									if($logs)
									{
										$i=0;
										foreach($logs as $log)
										{
										    if($log->type == 'add'){
										        $class = "uk-badge-success";
                                            }if($log->type == 'update'){
										        $class = "uk-badge-primary";
                                            }if($log->type == 'delete'){
										        $class = "uk-badge-danger";
                                            }if($log->type == 'export'){
										        $class = "uk-badge-warning";
                                            }if($log->type == 'import'){
										        $class = "";
                                            }

										?>
											<tr class="<?php echo $log->id;?>">
													<td><?php echo ++$i;?></td>
													<td><?php echo $log->user_name;?></td>
													<td>
                                                        <span class="uk-badge <?php echo $class; ?>">
                                                            <?php echo $log->message;?>
                                                        </span>

                                                    </td>
													<td><?php echo $log->section;?></td>

													<td>
                                                        <?php

                                                        echo date('l, jS F Y g:i a',strtotime($log->action_date));

                                                        ?>
                                                    </td>

													<!--<td>

                                                        <a href="javascript:void(0);" class="uk-margin-left delete" onclick="deleteRecord(<?php /*echo $log->id;*/?>,'admin/report/delete','');" title="Delete Log">
                                                            <i class="material-icons md-24 delete">&#xE872;</i>
                                                        </a>
                                                    </td>-->

													  
											</tr>
										<?php
										}
										
									}
									
									?>
                                           
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
       <!-- /.content-wrapper --> 