<section class="main">
        <div class="container">
            <div class="sliderHome">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <div class="indicatorDiv">
					
					<?php if(sizeof($images) > 1) { ?>
                        <div class="moreThen1">                        	
                        	 <a href="#carousel-example-generic" role="button" data-slide="next"><i class="sprite_ioud ioudSparrowright"></i></a>
                        </div>
					<?php } ?>
                        <ol class="carousel-indicators">
                            <?php
							if(sizeof($images) > 1) {
							
							 foreach ($images as $key => $image) {
								if($image[$lang.'_image'] == '')
								{
									continue;
								}
                                if($key == 0) {
                                    $class = 'active';
                                }else{
                                    $class = '';
                                }
                                ?>
                            <li data-target="#carousel-example-generic" data-slide-to="<?php echo $key; ?>" class="<?php echo $class; ?>"></li>
                        <?php } 
							}
						?>
                            </ol>
                    </div>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <?php $sl = 1;
                        $active = '';
                        foreach($images as $img)
                        {
							if($img[$lang.'_image'] == '')
							{
								continue;
							}
                        if($sl == 1)
                        {
                            $active = 'active';
                        }
                        else
                        {
                            $active = '';
                        } ?>
                        <div class="item <?php echo $active; ?>" id="<?php echo 'item-'.$img['id'];?>">
						<?php if($img[$lang.'_slider_link'] != "") { ?>
                            <a href="<?php echo $img[$lang.'_slider_link']; ?>">
                                <img src="<?php echo base_url();?>uploads/images/home/<?php echo $img[$lang.'_image']?>" alt="Slider" height="688" width="1402" />
                            </a>
                            <?php }else{ ?>
                            <img src="<?php echo base_url();?>uploads/images/home/<?php echo $img[$lang.'_image']?>" alt="Slider" height="688" width="1402" />
							
							<?php } ?>
                            
                            <div class="carousel-caption">
                                <div class="caption_box">
                                    <h1 class="makeShadow"><?php echo $img[$lang.'_title']?></h1>
                                    <?php 
									if($lang == "eng"){
										echo substr($img[$lang.'_desc'],'0','200');
									}else{
										echo substr($img[$lang.'_desc'],'0','350');	
									}?>
                                </div>
                            </div>
                        </div>
                            <?php
                            $sl++;
                        }?>
                    </div>
                </div>
            </div>
            <div class="homeInnovation">
                <div class="lftImgSec">
                    <img src="<?php echo base_url(); ?>uploads/images/home/<?php echo $home_content['discover_more_image']; ?>" alt="Innovation" height="424" width="529" />
                </div>
                <div class="homeVdText">
					<?php if($home_content['more_video'] !=''){?>
                    <a href="#homeVideo" class="videoBtnEdH"  data-toggle="modal">
                        <span class="makeShadow up"><?php echo ($lang == 'arb' ? 'اكتشف' : 'Discover');?></span>
                        <span class="makeShadow down"><?php echo ($lang == 'arb' ? 'أكثر من' : 'More');?></span>
                    </a>
					<?php } ?>
                    <div class="clearfix"></div>
                    <h1 class="makeShadow"><?php echo $home_content[$lang.'_more_title']; ?></h1>
                    <?php echo $home_content[$lang.'_more_discover']; ?>
                    <a href="<?php echo lang_base_url().'aboutUs/why_ioud'; ?>" class="btn btn btn-default"><?php echo $read_more; ?></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="homeProducts">
                <div class="headingBar">
                    <h2><?php echo $products_label; ?></h2>
                    <a href="<?php echo lang_base_url().'product'; ?>" class="edBtn btn-default"><?php echo $view_all; ?></a>
                    <div class="clearfix"></div>
                </div>
                <div class="productsListing">
                    <ul>
                    
                       <?php 
					   if($products) {
						   
					   foreach ($products as $product) {
                            $prod_image =  getProductImages($product['id']);
							
							foreach($prod_image as $key => $prod_img){
								if($prod_img['is_thumbnail'] == 1){
									$thumbnail = $prod_img['thumbnail'];
								}
							}
                           ?>
                        <li>
                            <div class="whiteBox">
                                <a href="<?php echo lang_base_url().'product/product_page/'.$product['id']; ?>">
                                <div class="imgBox"><img src="<?php echo base_url(); ?>uploads/images/thumbs/products/<?php echo $thumbnail; ?>" alt="Product" height="144" width="145" /></div>
                                </a>
                                <a href="<?php echo lang_base_url().'product/product_page/'.$product['id']; ?>">
                                    <h3><?php echo $product[$lang.'_name']; ?></h3>
                                </a>
                                <p><?php echo $product[$lang . '_cat_name']; ?></p>
                                <h4>
                                    <?php
                                    $price = getPriceByOffer($product['id']);
                                    $calculated_price = currencyRatesCalculate($price);
                                    echo $calculated_price['rate']; ?> <?php echo $calculated_price['unit']; ?>
                                </h4>
                                <input type="hidden" class="selQty" name="selQty" value="1">
                                <?php
                                $mMode = maintenanceMode('');
                                if($mMode != ''){
                                    $onClick = 'maintenanceMode();';
                                }else{
                                    $onClick = 'addToCart('.$product["id"].','.$product["total_quantity"].');';
                                }
                                ?>
                                <?php if(true  == false) { ?>
                                <div class="stars stars-example-bootstrap">
                                    <?php $rate = getAverageRating($product['id']);
                                    ?>
                                    <select style="display: none;" class="example-bootstrap" name="rating" autocomplete="off" onchange="rateProduct(this.value, '<?php echo $product['id']; ?>');">
                                        <option <?php echo ($rate == 1 ? 'selected' : '');?> value="1">1</option>
                                        <option <?php echo ($rate == 2 ? 'selected' : '');?> value="2">2</option>
                                        <option <?php echo ($rate == 3 ? 'selected' : '');?> value="3">3</option>
                                        <option <?php echo ($rate == 4 ? 'selected' : '');?> value="4">4</option>
                                        <option <?php echo ($rate == 5 ? 'selected' : '');?> value="5">5</option>
                                    </select>
                                </div>
                               <?php } ?>
                                <a href="javascript:void(0);" onclick="<?php echo $onClick; ?>">
                                    <button class="btn btn-success" type="button">
                                        <i class="sprite_ioud ioudSppluscart"></i>
                                    </button>
                                </a>
                            </div>
                        </li>
                           
                        <?php } 
					   }else{?>
                       
                       <div><li style="color: red; font-size: large; text-align: center;">
                               <?php echo $no_record_founds_label; ?>
                           </li>
                       </div>
                       <?php } ?>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
			
			<?php if($offers) { ?>
            <div class="offersSec">
                <div class="headingBar">
                    <h2><?php echo $offers_label; ?></h2>
                    <a href="<?php echo lang_base_url().'promotion'; ?>" class="edBtn btn-default"><?php echo $view_all; ?></a>
                    <div class="clearfix"></div>
                </div>
                <div class="offProList">
                    <ul>
                        <?php
						
						foreach ($offers as $offer) {
							
                            $offer_image =  getProductImages($offer['id']);
							
							foreach($offer_image as $key => $offer_img){
								if($offer_img['is_thumbnail'] == 1){
									$thumbnail = $offer_img['thumbnail'];
								}
							}
                        ?>
                        <li>
                            <div class="whiteBox">
                                <a href="<?php echo lang_base_url().'product/product_page/'.$offer['id']; ?>">
                                <div class="imgBox"><img src="<?php echo base_url(); ?>uploads/images/thumbs/products/<?php echo $thumbnail; ?>" alt="Offer" height="192" width="212" /></div>
                                </a>
                                <a href="<?php echo lang_base_url().'product/product_page/'.$offer['id']; ?>">
                                    <h3><?php echo $offer[$lang.'_name']; ?></h3>
                                </a>
                                <?php echo substr($offer[$lang . '_description'],0,100).'...'; ?>
                                <h4><?php
                                    $price = getPriceByOffer($offer['id']);
                                    $calculated_price = currencyRatesCalculate($price);
                                    echo $calculated_price['rate']; ?> <?php echo $calculated_price['unit']; ?></h4>
                                <input type="hidden" class="selQty" name="selQty" value="1">
                                <a href="javascript:void(0);" onclick="addToCart(<?php echo $offer['id']; ?>)">
                                    <button class="btn btn-success" type="button">
                                        <i class="sprite_ioud ioudSppluscart"></i>
                                    </button>
                                </a>
                            </div>
                        </li>
                        <?php }
						 ?>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
            <?php } ?>
        </div>
    </section>
    <!-- Home page PopUp VideoL -->
    <div id="homeVideo" class="modal fade">
        <div class="modal-dialog ">
            <div class="modal-content">
                <iframe id="hVideoIframe" width="598" height="336" src="<?php echo $home_content['more_video']; ?>" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
<!-- Online Chat Button -->
<div class="onlineChatBtn">
    <a href="javascript:void(Tawk_API.toggle())">Online Chat</a>
</div>