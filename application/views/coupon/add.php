<div id="page_content">
    <div id="page_content_inner">
    
        <div class="md-card-content orderPage ordDtlPgEd">
            <div class="orderListBox">
                <div class="singleOrder noBorder">
                    <div class="orderExpand uk-child"  style="display: block;" >
                        <div class="uk-grid ordSingDtlEdHead" data-uk-grid-margin data-uk-grid-match="{target:'.md-card'}">
                            <div class="uk-width-medium-5-10">
                                <a href="<?php echo base_url();?>admin/coupon">
                                    <i class="material-icons">keyboard_arrow_left</i>
                                    Go back
                                </a>
                            </div>
                        </div>
                        <div class="productDtlSec">
                            <div class=" uk-grid uk-grid-medium" data-uk-grid-margin>
                                <div class="uk-width-xLarge-10-10">
                                	<form action="<?php echo base_url(); ?>admin/coupon/action" method="post" onsubmit="return false" class="uk-form-stacked ajax_form">
                                    	<input type="hidden" name="form_type" value="save">
                                        <div class="md-card">
                                            <div class="md-card-content formCoupons">
                                                <h1>Create Coupons</h1>
        
                                                <br />
                                                <br />
        
                                                <div class="uk-grid tagBox" data-uk-grid-margin>
                                                    <div class="uk-width-medium-2-6">
                                                        <div class="labelTxt">Coupon Name</div>
                                                        <input type="text" class="md-input" placeholder="Write" id="coupon_name" name="coupon_name" />
                                                    </div>
                                                    <div class="uk-width-medium-2-6">
                                                        <div class="labelTxt">Code</div>
                                                        <input type="text" class="md-input" placeholder="Write" id="code" name="code" />
                                                    </div>
                                                    <div class="uk-width-medium-2-6">
                                                        <div class="labelTxt">Type</div>
                                                        <select id="select_demo_5" name="type" data-md-selectize data-md-selectize-bottom>
                                                            <option value="1">Percentage</option>
                                                            <option value="2">Fixed Amount</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="uk-grid tagBox" data-uk-grid-margin>
                                                    <div class="uk-width-medium-2-6">
                                                        <div class="labelTxt">Discount</div>
                                                        <input type="text" class="md-input" placeholder="Write" id="discount" name="discount" />
                                                    </div>
                                                    <div class="uk-width-medium-2-6">
                                                        <div class="labelTxt">Total Amount</div>
                                                        <input type="text" class="md-input" placeholder="Write" id="total_amount" name="total_amount" />
                                                    </div>
                                                    <div class="uk-width-medium-2-6">
                                                        <div class="labelTxt">Customer Login</div>
                                                        <label class="radioLbl"><input type="radio" name="customer_login" value="1" /> Yes</label>
                                                        <label class="radioLbl"><input type="radio" name="customer_login" value="0" checked /> No</label>
                                                    </div>
                                                </div>
                                                <div class="uk-grid tagBox" data-uk-grid-margin>
                                                    <div class="uk-width-medium-6-6">
                                                        <div class="labelTxt">Free Shipping</div>
                                                        <label class="radioLbl"><input type="radio" name="free_shipping" value="1" /> Yes</label>
                                                        <label class="radioLbl"><input type="radio" name="free_shipping" value="0" checked /> No</label>
                                                    </div>
                                                </div>
                                                <div class="uk-grid tagBox" data-uk-grid-margin>
                                                    <div class="uk-width-large-1-1 autoEnterBox">
                                                        <div class="labelTxt">Products</div>
                                                        <div class="uk-form-row">
                                                            <select name="product_id[]" id="kerWordsTag" multiple>
                                                                <?php foreach($products as $product){?>
                                                                    <option value="<?php echo $product->id?>"><?php echo $product->eng_name?></option>
                                                                <?php }?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="uk-grid tagBox" data-uk-grid-margin>
                                                    <div class="uk-width-large-1-1 autoEnterBox">
                                                        <div class="labelTxt">Category</div>
                                                        <div class="uk-form-row">
                                                            <select id="productTag" name="category_id[]" multiple>
                                                                <?php foreach($categories as $category){?>
                                                                    <option value="<?php echo $category->id?>"><?php echo $category->eng_name?></option>
                                                                <?php }?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="uk-grid tagBox" data-uk-grid-margin>
                                                    <div class="uk-width-medium-3-6">
                                                        <div class="labelTxt">Start Date</div>
                                                        <input type="text" class="md-input" id="start_date" name="start_date" data-uk-datepicker="{format:'DD/MM/YYYY'}" placeholder="Select Date" />
                                                    </div>
                                                    <div class="uk-width-medium-3-6">
                                                        <div class="labelTxt">End Date</div>
                                                        <input type="text" class="md-input" id="end_date" name="end_date" data-uk-datepicker="{format:'DD/MM/YYYY'}" placeholder="Select Date" />
                                                    </div>
                                                </div>
                                                <div class="uk-grid tagBox" data-uk-grid-margin>
                                                    <div class="uk-width-medium-2-6">
                                                        <div class="labelTxt">Uses Per Coupon</div>
                                                        <input type="text" class="md-input" placeholder="Write" id="uses_per_coupon" name="uses_per_coupon" />
                                                    </div>
                                                    <div class="uk-width-medium-2-6">
                                                        <div class="labelTxt">Uses Per Customer</div>
                                                        <input type="text" class="md-input" placeholder="Write" id="uses_per_customer" name="uses_per_customer" />
                                                    </div>
                                                    <div class="uk-width-medium-2-6">
                                                        <div class="labelTxt">status</div>
                                                        <select id="select_demo_5" name="status" data-md-selectize data-md-selectize-bottom>
                                                            <option value="1">Enabled</option>
                                                            <option value="0">Disabled</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <br /><br />
                                                <button type="submit" class="btn ioudBtn green">Add</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
    
                            </div>
    
    
                        </div>
                    </div><!--      Single Order Row END Here       -->
                </div>
            </div>
    
    
        </div>
    </div>
</div>