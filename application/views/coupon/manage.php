<div id="page_content">

    <div id="page_content_inner">
        <div class="md-card">
            <div class="md-card-content orderPage">
                <div class="orderListBox">
                    <div class="couponBox">
                        <div class="md-card upperSandBox">
                            <div class="uk-grid uk-grid-small">
                                <div class="uk-width-small-5-10">
                                    <h1>Coupons Report</h1>
                                </div>
                                <?php if(viewEditDeleteRights('Coupon','add')) { ?>
                                <div class="uk-width-small-5-10 uk-text-right">
                                    <a href="<?php echo base_url();?>admin/coupon/add"><button class="btn ioudBtn green" type="button">Create a Coupon</button></a>
                                </div>
                                <?php } ?>

                            </div>
							<form action="<?php echo base_url(); ?>admin/coupon" method="post" class="uk-form-stacked">
                                <div class="uk-grid uk-grid-small">
                                    <div class="uk-width-small-10-10 uk-width-medium-10-10 uk-width-large-10-10">
                                        <h2>Dates</h2>
                                    </div>
                                    <div class="uk-width-small-5-10 uk-width-medium-5-10 uk-width-large-4-10">
                                        <label>From</label>
                                        <input id="kUI_datepicker_frm" name="start_date" value="<?php echo ($start_date ? date('d/m/Y', strtotime($start_date)) : '');?>" data-uk-datepicker="{format:'DD/MM/YYYY'}" placeholder="Select Date" />
                                    </div>
                                    <div class="uk-width-small-5-10 uk-width-medium-5-10 uk-width-large-4-10">
                                        <label>To</label>
                                        <input id="kUI_datepicker_to" name="end_date" value="<?php echo ($end_date ? date('d/m/Y', strtotime($end_date)) : '');?>" data-uk-datepicker="{format:'DD/MM/YYYY'}" placeholder="Select Date" />
                                    </div>
                                    <div class="uk-width-small-10-10 uk-width-medium-10-10 uk-width-large-2-10 uk-text-right">
                                        <button type="submit" class="btn ioudBtn green filter" type="button">Filter</button>
                                    </div>
                                    
                                </div>
                        	</form>
                        </div>
                        <div class="md-card">
                            <div class="md-card-content">
                                <h1><i class="material-icons">card_giftcard</i> Coupons</h1>
                                <div class="couponTable">
                                    <div class="uk-overflow-container">
                                        <table class="uk-table uk-text-nowrap dt_default">
                                            <thead>
                                            <tr>
                                                <th>Coupon Name</th>
                                                <th>Code</th>
                                                <th>Orders</th>
                                                <th>Total</th>
                                                <?php if(viewEditDeleteRights('Coupon','edit') || viewEditDeleteRights('Coupon','delete')) { ?>
                                                <th>&nbsp;Actions</th>
                                                <?php } ?>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            	<?php foreach($coupons as $coupon){?>
                                                    <tr>
                                                        <td><?php echo $coupon->coupon_name?></td>
                                                        <td><?php echo $coupon->code?></td>
                                                        <td><?php echo getOrderCount(array('coupon_id' => $coupon->id))?></td>
                                                        <td><?php echo $coupon->total_amount.' '.getCurrency();?></td>

                                                        <?php if(viewEditDeleteRights('Coupon','edit') || viewEditDeleteRights('Coupon','delete')) { ?>
                                                        <td class="uk-text-right">
                                                            <?php if(viewEditDeleteRights('Coupon','edit')) { ?>
                                                            <a href="<?php echo base_url().'admin/coupon/edit/'.$coupon->id;?>">
                                                            <i class="md-icon material-icons green">mode_edit</i>
                                                            </a>

                                                            <?php } ?>
                                                            <?php if(viewEditDeleteRights('Coupon','delete')) { ?>
                                                            <i class="md-icon material-icons black" onclick="deleteRecord(<?php echo $coupon->id;?>,'admin/coupon/action','');">delete</i>

                                                            <?php } ?>

                                                        </td>
                                                        <?php } ?>

                                                    </tr>
                                                <?php }?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="md-fab-wrapper">
    <a class="md-fab md-fab-accent" href="#new_issue" data-uk-modal="{ center:true }">
        <i class="material-icons">&#xE145;</i>
    </a>
</div>