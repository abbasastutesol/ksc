 <!-- Content Wrapper. Contains page content -->
      <div id="page_content">
        <div id="page_content_inner">
		<a href="<?php echo base_url();?>admin/variant/add" class="md-btn"> Add</a>
			<div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <div class="uk-overflow-container"> 
                                <table class="uk-table uk-table-align-vertical listing dt_default">
                                    <thead>
                                        <tr>
                                             <th>Sr#</th>

                                              <th>Variant Name</th>
											  <th class="nosort">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
										if($variants)
										{
											$i= 1;										
										
											foreach($variants as $variant)
											{
											?>
											<tr class="<?php echo $variant->id ;?>">
												<td><?php echo $i;?></td>
												<td><?php echo $variant->eng_name;?></td>
												<td><a href="<?php echo base_url().'admin/variant/edit/'.$variant->id;?>" title="Edit Variant">
												<i class="md-icon material-icons">&#xE254;</i>
												</a>
												<a href="javascript:void(0);" class="uk-margin-left delete" onclick="deleteRecord(<?php echo $variant->id;?>,'admin/variant/action','');" title="Delete Variant"> <i class="material-icons md-24 delete">&#xE872;</i></a></td>
											</tr>	
											<?php		
											$i++;
											}
										}
										
										
										?>    
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
       <!-- /.content-wrapper --> 