<div id="page_content">
  <div id="page_content_inner">
    <div class="md-card">
      <div class="md-card-content">
        <h3 class="heading_a">Edit a Variant</h3><br>
        <form action="<?php echo base_url(); ?>admin/variant/action" method="post" onsubmit="return false" class="ajax_form">
		<div class="uk-grid" data-uk-grid-margin>
		
		<input type="hidden" name="form_type" value="update">
		<input type="hidden" name="id" value="<?php echo $variant->id;?>">
		
          <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
              <label>English Name</label>
              <input type="text" class="md-input" value="<?php echo $variant->eng_name;?>" name="eng_name" />
            </div>
			
          </div>
          <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
              <label>Arabic Name</label>
              <input type="text" class="md-input" value="<?php echo $variant->arb_name;?>" name="arb_name" />
            </div>
            
          </div>
		  <?php //$variant_values = getVariantValue($variant->id); 
				
		  ?>
		<!--<div class="uk-width-medium-1-1">
            <div class="uk-form-row">
              <label>English Variant Values (Comma seprated)</label>
              <input type="text" class="md-input" value="<?php echo $variant_values['english'];?>" name="eng_value" />
            </div>
            
          </div>
		  
		  <div class="uk-width-medium-1-1">
            <div class="uk-form-row">
              <label>Arabic Variant Values (Comma seprated)</label>
              <input type="text" class="md-input" value="<?php echo $variant_values['arabic'];?>" name="arb_value" />
            </div>
            
          </div> -->
		  
			<div class="uk-width-medium-1-1">
			<?php 
				$k=1;
				if($variant_values){
				foreach($variant_values as $variant){ ?>
		   <div class="uk-grid form_section <?php echo $variant->id;?>" data-section-added="<?php echo $k;?>">
		                    <input type="hidden" name="variant_value_ids[]" value="<?php echo $variant->id;?>">
                                <div class="uk-width-1-1">
                                    <div class="uk-input-group">
                                        <label>English Variant Value</label>
                                        <input type="text" class="md-input" name="eng_value[]" value="<?php echo $variant->eng_value;?>">
										<label>Arabic Variant Value</label>
										<input type="text" class="md-input" name="arb_value[]" value="<?php echo $variant->arb_value;?>">
                                        <span class="uk-input-group-addon">
										<a href="javascript:void(0);" onClick="deleteRecord(<?php echo $variant->id;?>,'admin/variant_value/action','');"><i class="material-icons md-24"></i></a>
                                            
                                        </span>
                                    </div>
                                </div>
			</div>					
				<?php $k++; } } ?>
							
			 <div class="uk-grid form_section" id="d_form_row">
                                <div class="uk-width-1-1">
                                    <div class="uk-input-group">
                                        <label>English Variant Value</label>
                                        <input type="text" class="md-input" name="eng_value[]">
										<label>Arabic Variant value</label>
										<input type="text" class="md-input" name="arb_value[]">
                                        <span class="uk-input-group-addon">
                                            <a href="#" class="btnSectionClone" data-section-clone="#d_form_row"><i class="material-icons md-24">&#xE146;</i></a>
                                        </span>
                                    </div>
                                </div>
            </div>
			
				<br><span class="uk-input-group-addon">Click + to add variant values
                                            <a href="#" class="btnSectionClone" data-section-clone="#d_form_row"><i class="material-icons md-24"> &#xE146;</i></a>
            </span>			
            
		  </div>
			  
			
			
	
        </div>
       </form> 
      </div>
    </div>
   
   <!-- light box for image -->
		   <div class="uk-modal" id="modal_lightbox">
                                <div class="uk-modal-dialog uk-modal-dialog-lightbox">
                                    <button type="button" class="uk-modal-close uk-close uk-close-alt"></button>
                                    <img src="<?php echo base_url().'uploads/images/categories/'.$variant->eng_image; ?>" alt=""/>
                                    
                                </div>
           </div>
			<!-- end light box for image -->   
    
    
    
  </div>
</div>

<div class="md-fab-wrapper">
        <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">
            <i class="material-icons">&#xE161;</i>
        </a>
    </div>
<div class="md-fab-wrapper" style="right:95px;">
        <a class="md-fab md-fab-primary" href="<?php echo base_url();?>admin/variant" id="">
            <i class="material-icons">keyboard_backspace</i>
        </a>
    </div>