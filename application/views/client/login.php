<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>eDesign</title>
	<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
	<link href="<?php echo base_url();?>assets/css/all.css" rel="stylesheet" type="text/css" media="all">
	<link href="https://fonts.googleapis.com/css?family=Text+Me+One" rel="stylesheet">
</head>
<body>
	<div class="pp"></div>
	<div class="login-box">
    	<div class="login-holder">
            <section class="login wow fadeInDown" data-wow-duration="1s">
                <header>
                	<div class="logo"><a href="#"><img src="<?php echo base_url();?>assets/images/logo.png" alt="edesign"></a></div>
                    <ul class="panel">
                    	<li><a href="#">Register</a></li>
                        <li class="active"><a href="#">Login</a></li>
                    </ul>
                </header>
                <div class="holder">
                    <h1>Welcome <span>back!</span></h1>
                    <form action="#">
                        <div class="text-field">
                        	<i class="icon-profile"></i>
                            <input type="text" placeholder="Username">
                        </div>
                        <div class="text-field">
                        	<i class="icon-lock"></i>
                            <input type="password" placeholder="Password">
                        </div>
                        <button type="submit" class="btn-default">Submit <i class="icon-angle-right"></i></button>
                    </form>
                </div>
            </section>
        </div>
    </div>
	<script src="<?php echo base_url();?>assets/js/jquery-1.11.2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.main.js" defer></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/js/wow.js" defer></script>
</body>
</html>