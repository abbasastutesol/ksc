<main id="main" class="add">
               <div class="posts-block">
                    <section class="posts wow fadeInUp" data-wow-delay="1">
                        <header class="head">
                            <h2>WED 4th AUGUST</h2>
                            <ul class="options">
                            	<li><a href="#popup1" class="icon-group lightbox"></a></li>
                                <li><a href="#" class="icon-ribbion"></a></li>
                            </ul>
                            <a href="#" class="btn-default add_post">Add post</a>
                        </header>
                        <div class="post-holder">
                            <article class="post wow fadeInLeft" data-wow-delay="1.3s">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standar.Lorem Ipsum text of the printing and typesetting industinry. Lorem Ipsum has been the industry's standar.Lorem Ipsum the industrthe industry's standar</p>
                                <div class="img-holder"><img src="<?php echo base_url();?>assets/images/img1.jpg" alt=""></div>
                                <div class="toolbar">
                                    <a href="#" class="icon-edit2"></a>
                                    <ul>
                                        <li class="hidden"><a href="#" class="icon-ribbion1"><span>1</span></a></li>
                                        <li><a href="#" class="icon-ribbion"></a></li>
                                        <li class="hidden"><a href="#" class="icon-ribbion12"></a></li>
                                        <li><a href="#popup2" class="icon-edit lightbox"></a></li>
                                        <li><a href="#" class="icon-eye"></a></li>
                                    </ul>
                                </div>
                                <div class="edit-box">
                                	<textarea rows="5" cols="5" placeholder="Post Text:" class="form-control"></textarea>
                                    <div class="radio-box">
                                    	<label>
                                        	<input type="radio" name="radio" value="Photo" checked>
                                            Photo
                                        </label>
                                        <label>
                                        	<input type="radio" name="radio" value="Video">
                                            Video
                                        </label>
                                    </div>
                                    <input type="text" placeholder="Post Video:" class="form-control video-url">
                                    <input type="file" data-jcf='{"buttonText": "Add Photo", "placeholderText": ""}' class="file">
                                    <div class="toolbar">
                                        <a href="#" class="icon-trash-o"></a>
                                        <div class="btn-holder">
                                            <a href="#" class="btn-default">Save</a>
                                            <a href="#" class="btn-default black cancel">Cancel </a>
                                        </div>
                                    </div>
                                </div>
                            </article>
                            <article class="post wow fadeInLeft" data-wow-delay="1.3s">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standar.Lorem Ipsum text of the printing and typesetting industinry. Lorem Ipsum has been the industry's standar.Lorem Ipsum the industrthe industry's standar</p>
                                <div class="img-holder"><img src="<?php echo base_url();?>assets/images/img2.jpg" alt=""></div>
                                <div class="toolbar">
                                    <a href="#" class="icon-edit2"></a>
                                    <ul>
                                        <li class="hidden"><a href="#" class="icon-ribbion1"><span>1</span></a></li>
                                        <li><a href="#" class="icon-ribbion"></a></li>
                                        <li class="hidden"><a href="#" class="icon-ribbion12"></a></li>
                                        <li><a href="#popup2" class="icon-edit lightbox"></a></li>
                                        <li><a href="#" class="icon-eye"></a></li>
                                    </ul>
                                </div>
                                <div class="edit-box">
                                	<textarea rows="5" cols="5" placeholder="Post Text:" class="form-control"></textarea>
                                    <div class="radio-box">
                                    	<label>
                                        	<input type="radio" name="radio" value="Photo" checked>
                                            Photo
                                        </label>
                                        <label>
                                        	<input type="radio" name="radio" value="Video">
                                            Video
                                        </label>
                                    </div>
                                    <input type="text" placeholder="Post Video:" class="form-control video-url">
                                    <input type="file" data-jcf='{"buttonText": "Add Photo", "placeholderText": ""}' class="file">
                                    <div class="toolbar">
                                        <a href="#" class="icon-trash-o"></a>
                                        <div class="btn-holder">
                                            <a href="#" class="btn-default">Save</a>
                                            <a href="#" class="btn-default black cancel">Cancel </a>
                                        </div>
                                    </div>
                                </div>
                            </article>
                            <article class="post wow fadeInLeft" data-wow-delay="1.3s">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standar.Lorem Ipsum text of the printing and typesetting industinry. Lorem Ipsum has been the industry's standar.Lorem Ipsum the industrthe industry's standar</p>
                                <div class="img-holder"><img src="<?php echo base_url();?>assets/images/img1.jpg" alt=""></div>
                                <div class="toolbar">
                                    <a href="#" class="icon-edit2"></a>
                                    <ul>
                                        <li class="hidden"><a href="#" class="icon-ribbion1"><span>1</span></a></li>
                                        <li><a href="#" class="icon-ribbion"></a></li>
                                        <li class="hidden"><a href="#" class="icon-ribbion12"></a></li>
                                        <li><a href="#popup2" class="icon-edit lightbox"></a></li>
                                        <li><a href="#" class="icon-eye"></a></li>
                                    </ul>
                                </div>
                                <div class="edit-box">
                                	<textarea rows="5" cols="5" placeholder="Post Text:" class="form-control"></textarea>
                                    <div class="radio-box">
                                    	<label>
                                        	<input type="radio" name="radio" value="Photo" checked>
                                            Photo
                                        </label>
                                        <label>
                                        	<input type="radio" name="radio" value="Video">
                                            Video
                                        </label>
                                    </div>
                                    <input type="text" placeholder="Post Video:" class="form-control video-url">
                                    <input type="file" data-jcf='{"buttonText": "Add Photo", "placeholderText": ""}' class="file">
                                    <div class="toolbar">
                                        <a href="#" class="icon-trash-o"></a>
                                        <div class="btn-holder">
                                            <a href="#" class="btn-default">Save</a>
                                            <a href="#" class="btn-default black cancel">Cancel </a>
                                        </div>
                                    </div>
                                </div>
                            </article>
                            <article class="post wow fadeInLeft" data-wow-delay="1.3s">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standar.Lorem Ipsum text of the printing and typesetting industinry. Lorem Ipsum has been the industry's standar.Lorem Ipsum the industrthe industry's standar</p>
                                <div class="img-holder"><img src="<?php echo base_url();?>assets/images/img2.jpg" alt=""></div>
                                <div class="toolbar">
                                    <a href="#" class="icon-edit2"></a>
                                    <ul>
                                        <li class="hidden"><a href="#" class="icon-ribbion1"><span>1</span></a></li>
                                        <li><a href="#" class="icon-ribbion"></a></li>
                                        <li class="hidden"><a href="#" class="icon-ribbion12"></a></li>
                                        <li><a href="#popup2" class="icon-edit lightbox"></a></li>
                                        <li><a href="#" class="icon-eye"></a></li>
                                    </ul>
                                </div>
                                <div class="edit-box">
                                	<textarea rows="5" cols="5" placeholder="Post Text:" class="form-control"></textarea>
                                    <div class="radio-box">
                                    	<label>
                                        	<input type="radio" name="radio" value="Photo" checked>
                                            Photo
                                        </label>
                                        <label>
                                        	<input type="radio" name="radio" value="Video">
                                            Video
                                        </label>
                                    </div>
                                    <input type="text" placeholder="Post Video:" class="form-control video-url">
                                    <input type="file" data-jcf='{"buttonText": "Add Photo", "placeholderText": ""}' class="file">
                                    <div class="toolbar">
                                        <a href="#" class="icon-trash-o"></a>
                                        <div class="btn-holder">
                                            <a href="#" class="btn-default">Save</a>
                                            <a href="#" class="btn-default black cancel">Cancel </a>
                                        </div>
                                    </div>
                                </div>
                            </article>
                            <article class="post wow fadeInLeft" data-wow-delay="1.3s">
                                <textarea rows="5" cols="5" placeholder="Post Text:" class="form-control"></textarea>
                                <div class="radio-box">
                                    <label>
                                        <input type="radio" name="radio1" value="Photo" checked>
                                        Photo
                                    </label>
                                    <label>
                                        <input type="radio" name="radio1" value="Video">
                                        Video
                                    </label>
                                </div>
                                <input type="text" placeholder="Post Video:" class="form-control video-url">
                                <input type="file" data-jcf='{"buttonText": "Add Photo", "placeholderText": ""}' class="file">
                                <div class="toolbar">
                                    <a href="#" class="icon-trash-o"></a>
                                    <div class="btn-holder">
                                        <a href="#" class="btn-default">Save</a>
                                        <a href="#" class="btn-default black">Cancel </a>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </section>
                    <section class="posts wow fadeInUp" data-wow-delay="1">
                        <header class="head">
                            <h2>WED 4th AUGUST</h2>
                            <ul class="options">
                            	<li><a href="#popup1" class="icon-group lightbox"></a></li>
                                <li><a href="#" class="icon-ribbion"></a></li>
                            </ul>
                            <a href="#" class="btn-default add_post">Add post</a>
                        </header>
                        <div class="post-holder">
                            <article class="post wow fadeInLeft" data-wow-delay="1.3s">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standar.Lorem Ipsum text of the printing and typesetting industinry. Lorem Ipsum has been the industry's standar.Lorem Ipsum the industrthe industry's standar</p>
                                <div class="img-holder"><img src="<?php echo base_url();?>assets/images/img1.jpg" alt=""></div>
                                <div class="toolbar">
                                    <a href="#" class="icon-edit2"></a>
                                    <ul>
                                        <li class="hidden"><a href="#" class="icon-ribbion1"><span>1</span></a></li>
                                        <li><a href="#" class="icon-ribbion"></a></li>
                                        <li class="hidden"><a href="#" class="icon-ribbion12"></a></li>
                                        <li><a href="#popup2" class="icon-edit lightbox"></a></li>
                                        <li><a href="#" class="icon-eye"></a></li>
                                    </ul>
                                </div>
                                <div class="edit-box">
                                	<textarea rows="5" cols="5" placeholder="Post Text:" class="form-control"></textarea>
                                    <div class="radio-box">
                                    	<label>
                                        	<input type="radio" name="radio" value="Photo" checked>
                                            Photo
                                        </label>
                                        <label>
                                        	<input type="radio" name="radio" value="Video">
                                            Video
                                        </label>
                                    </div>
                                    <input type="text" placeholder="Post Video:" class="form-control video-url">
                                    <input type="file" data-jcf='{"buttonText": "Add Photo", "placeholderText": ""}' class="file">
                                    <div class="toolbar">
                                        <a href="#" class="icon-trash-o"></a>
                                        <div class="btn-holder">
                                            <a href="#" class="btn-default">Save</a>
                                            <a href="#" class="btn-default black cancel">Cancel </a>
                                        </div>
                                    </div>
                                </div>
                            </article>
                            <article class="post wow fadeInLeft" data-wow-delay="1.3s">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standar.Lorem Ipsum text of the printing and typesetting industinry. Lorem Ipsum has been the industry's standar.Lorem Ipsum the industrthe industry's standar</p>
                                <div class="img-holder"><img src="<?php echo base_url();?>assets/images/img2.jpg" alt=""></div>
                                <div class="toolbar">
                                    <a href="#" class="icon-edit2"></a>
                                    <ul>
                                        <li class="hidden"><a href="#" class="icon-ribbion1"><span>1</span></a></li>
                                        <li><a href="#" class="icon-ribbion"></a></li>
                                        <li class="hidden"><a href="#" class="icon-ribbion12"></a></li>
                                        <li><a href="#popup2" class="icon-edit lightbox"></a></li>
                                        <li><a href="#" class="icon-eye"></a></li>
                                    </ul>
                                </div>
                                <div class="edit-box">
                                	<textarea rows="5" cols="5" placeholder="Post Text:" class="form-control"></textarea>
                                    <div class="radio-box">
                                    	<label>
                                        	<input type="radio" name="radio" value="Photo" checked>
                                            Photo
                                        </label>
                                        <label>
                                        	<input type="radio" name="radio" value="Video">
                                            Video
                                        </label>
                                    </div>
                                    <input type="text" placeholder="Post Video:" class="form-control video-url">
                                    <input type="file" data-jcf='{"buttonText": "Add Photo", "placeholderText": ""}' class="file">
                                    <div class="toolbar">
                                        <a href="#" class="icon-trash-o"></a>
                                        <div class="btn-holder">
                                            <a href="#" class="btn-default">Save</a>
                                            <a href="#" class="btn-default black cancel">Cancel </a>
                                        </div>
                                    </div>
                                </div>
                            </article>
                            <article class="post wow fadeInLeft" data-wow-delay="1.3s">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standar.Lorem Ipsum text of the printing and typesetting industinry. Lorem Ipsum has been the industry's standar.Lorem Ipsum the industrthe industry's standar</p>
                                <div class="img-holder"><img src="<?php echo base_url();?>assets/images/img1.jpg" alt=""></div>
                                <div class="toolbar">
                                    <a href="#" class="icon-edit2"></a>
                                    <ul>
                                        <li class="hidden"><a href="#" class="icon-ribbion1"><span>1</span></a></li>
                                        <li><a href="#" class="icon-ribbion"></a></li>
                                        <li class="hidden"><a href="#" class="icon-ribbion12"></a></li>
                                        <li><a href="#popup2" class="icon-edit lightbox"></a></li>
                                        <li><a href="#" class="icon-eye"></a></li>
                                    </ul>
                                </div>
                                <div class="edit-box">
                                	<textarea rows="5" cols="5" placeholder="Post Text:" class="form-control"></textarea>
                                    <div class="radio-box">
                                    	<label>
                                        	<input type="radio" name="radio" value="Photo" checked>
                                            Photo
                                        </label>
                                        <label>
                                        	<input type="radio" name="radio" value="Video">
                                            Video
                                        </label>
                                    </div>
                                    <input type="text" placeholder="Post Video:" class="form-control video-url">
                                    <input type="file" data-jcf='{"buttonText": "Add Photo", "placeholderText": ""}' class="file">
                                    <div class="toolbar">
                                        <a href="#" class="icon-trash-o"></a>
                                        <div class="btn-holder">
                                            <a href="#" class="btn-default">Save</a>
                                            <a href="#" class="btn-default black cancel">Cancel </a>
                                        </div>
                                    </div>
                                </div>
                            </article>
                            <article class="post wow fadeInLeft" data-wow-delay="1.3s">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standar.Lorem Ipsum text of the printing and typesetting industinry. Lorem Ipsum has been the industry's standar.Lorem Ipsum the industrthe industry's standar</p>
                                <div class="img-holder"><img src="<?php echo base_url();?>assets/images/img2.jpg" alt=""></div>
                                <div class="toolbar">
                                    <a href="#" class="icon-edit2"></a>
                                    <ul>
                                        <li class="hidden"><a href="#" class="icon-ribbion1"><span>1</span></a></li>
                                        <li><a href="#" class="icon-ribbion"></a></li>
                                        <li class="hidden"><a href="#" class="icon-ribbion12"></a></li>
                                        <li><a href="#popup2" class="icon-edit lightbox"></a></li>
                                        <li><a href="#" class="icon-eye"></a></li>
                                    </ul>
                                </div>
                                <div class="edit-box">
                                	<textarea rows="5" cols="5" placeholder="Post Text:" class="form-control"></textarea>
                                    <div class="radio-box">
                                    	<label>
                                        	<input type="radio" name="radio" value="Photo" checked>
                                            Photo
                                        </label>
                                        <label>
                                        	<input type="radio" name="radio" value="Video">
                                            Video
                                        </label>
                                    </div>
                                    <input type="text" placeholder="Post Video:" class="form-control video-url">
                                    <input type="file" data-jcf='{"buttonText": "Add Photo", "placeholderText": ""}' class="file">
                                    <div class="toolbar">
                                        <a href="#" class="icon-trash-o"></a>
                                        <div class="btn-holder">
                                            <a href="#" class="btn-default">Save</a>
                                            <a href="#" class="btn-default black cancel">Cancel </a>
                                        </div>
                                    </div>
                                </div>
                            </article>
                            <article class="post wow fadeInLeft" data-wow-delay="1.3s">
                                <textarea rows="5" cols="5" placeholder="Post Text:" class="form-control"></textarea>
                                <div class="radio-box">
                                    <label>
                                        <input type="radio" name="radio1" value="Photo" checked>
                                        Photo
                                    </label>
                                    <label>
                                        <input type="radio" name="radio1" value="Video">
                                        Video
                                    </label>
                                </div>
                                <input type="text" placeholder="Post Video:" class="form-control video-url">
                                <input type="file" data-jcf='{"buttonText": "Add Photo", "placeholderText": ""}' class="file">
                                <div class="toolbar">
                                    <a href="#" class="icon-trash-o"></a>
                                    <div class="btn-holder">
                                        <a href="#" class="btn-default">Save</a>
                                        <a href="#" class="btn-default black">Cancel </a>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </section>
                    <div class="btn-box">
                        <a href="#" class="btn-default black">download</a>
                        <a href="#" class="btn-default">send email</a>
                    </div>
                </div>
            </main>
        