
<main class="main">
	<div class="container">
		<section class="content-wrap">
			<div class="left-sidebar">
				
                <ul>
                    
                    <?php
					
                      $class = '';
                    $getMenu = getMenu('footer','',1);
                    foreach($getMenu as $getMe){
                        $active = activeMenuClass($_SERVER['REQUEST_URI'],$getMe[$lang.'_link'],$lang);

                        ?>

                    <li class="<?php echo $active; ?>"><a href="<?php echo $getMe[$lang.'_link']?>"><?php echo $getMe[$lang.'_menu_title']; ?></a></li>

                    <?php } ?>
                </ul>
			</div>
			<div class="right-content">
				<h1><?php echo $ceoMessage[0][$lang.'_title'];?></h1>
				<div class="ceo-thumbnail"><img src="<?php echo base_url();?>assets/frontend/images/<?php echo $ceoMessage[0]['banner_image']; ?>" alt=""></div>
				<div class="ceo-desc">
				<?php echo $ceoMessage[0][$lang.'_description'];?>
					
				</div>
				<div class="ceo-info">
					<h2><?php echo $ceoMessage[0][$lang.'_name'];?><span class="designation"><?php echo $ceoMessage[0][$lang.'_position'];?></span><span class="company-name">KSC Group</span></h2>
				</div>
			</div>
		</section>
	</div>
</main>
