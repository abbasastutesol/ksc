 <!-- Content Wrapper. Contains page content -->
      <div id="page_content">
        <div id="page_content_inner">
        <a href="<?php echo base_url();?>admin/shippingDetails/add" class="md-btn" > Add</a>
			<div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <div class="uk-overflow-container"> 
                                <table class="uk-table uk-table-align-vertical listing dt_default">
                                    <thead>
                                        <tr>
                                             <th>Sr#</th>

                                              <th class="nosort">City Name</th>
                      
                                              <th class="nosort"> Shipping Amount</th>
                                              
                                              <th class="nosort"> Delivery Time</th>
                                              
                                              <th class="nosort">Action</th>
                                              
                                        </tr>
                                    </thead>
                                    <tbody>
                                       	<?php 
											$i=1;
											foreach($shippings as $shipping)
											{
												
												?>
												<tr id="<?php echo $shipping->id;?>">
													<td class="uk-text-center"><?php echo $i;?></td>
													<td><?php echo getCityById($shipping->city_id);?></td>
                                                    <td><?php echo $shipping->shipping_price;?></td>
                                                    <td><?php echo $shipping->delivery_time.' Days';?></td>
                                                    <td><a href="<?php echo base_url().'admin/shippingDetails/edit/'.$shipping->id;?>" title="Edit Shipping"><i class="md-icon material-icons"></i></a><a href="javascript:void(0);" class="uk-margin-left delete" onclick="deleteRecordAll('<?php echo $shipping->id;?>','admin/shippingDetails/action','','delete');" title="Delete shipping"> <i class="material-icons md-24 delete">&#xE872;</i></a></td>
												</tr>
											<?php 
												$i++;
											}?>   
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
       <!-- /.content-wrapper --> 