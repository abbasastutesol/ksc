<div id="page_content">
  <div id="page_content_inner">
    <div class="md-card">
      <div class="md-card-content">
        <h3 class="heading_a">Edit a Shipping Detail</h3>
        <form action="<?php echo base_url(); ?>admin/shippingDetails/action" method="post" onsubmit="return false" class="ajax_form">
		<div class="uk-grid" data-uk-grid-margin>
		
		<input type="hidden" name="form_type" value="update">
		<input type="hidden" name="id" value="<?php echo $shipping->id;?>">
		<div class="uk-width-medium-1-1">
            <select id="select_demo_5" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}" title="Select with tooltip" name="city_id">
              <option value="">Select Category</option>
			   <?php
			  echo getCityLang('', $shipping->city_id);
			  ?>
              
            </select>
        </div>
        
        <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
              <label>Shipping Amount</label>
              <input type="text" class="md-input" name="shipping_price" value="<?php echo $shipping->shipping_price;?>" />
            </div>
			
          </div>
          <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
              <label>Delivery Time (In Days eg: 4,5)</label>
              <input type="text" class="md-input" name="delivery_time" value="<?php echo $shipping->delivery_time;?>" />
            </div>
            
          </div>
		  
		  <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
              <label>Tax (in %)</label>
              <input type="text" class="md-input" name="tax" value="<?php echo $shipping->tax;?>" />
            </div>
            
          </div>
		
        </div>
       </form> 
      </div>
    </div>
   
  
  </div>
</div>

<div class="md-fab-wrapper">
        <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">
            <i class="material-icons">&#xE161;</i>
        </a>
    </div>
