 <!-- Content Wrapper. Contains page content -->
      <div id="page_content">
        <div id="page_content_inner">
		<a href="<?php echo base_url();?>admin/question/add" class="md-btn"> Add</a>
			<div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <div class="uk-overflow-container"> 
                                <table class="uk-table uk-table-align-vertical listing dt_default">
                                    <thead>
                                        <tr>
                                             <th>Sr#</th>

                                              <th>Question</th>
                                              <th>Question For</th>
											  <th class="nosort">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
										if($question)
										{
											$i= 1;										
										
											foreach($question as $ques)
											{
											?>
											<tr class="<?php echo $ques->id ;?>">
												<td><?php echo $i;?></td>
												<td><?php echo $ques->eng_question;?></td>
                                                <td><?php echo $ques->type;?></td>
												<td><a href="<?php echo base_url().'admin/question/edit/'.$ques->id;?>" title="Edit Variant">
												<i class="md-icon material-icons">&#xE254;</i>
												</a>
												<a href="javascript:void(0);" class="uk-margin-left delete" onclick="deleteRecord(<?php echo $ques->id;?>,'admin/question/action','');" title="Delete Variant"> <i class="material-icons md-24 delete">&#xE872;</i></a></td>
											</tr>	
											<?php		
											$i++;
											}
										}
										
										
										?>    
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
       <!-- /.content-wrapper --> 