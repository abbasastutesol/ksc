<div id="page_content">
  <div id="page_content_inner">
    <div class="md-card">
      <div class="md-card-content">
        <h3 class="heading_a">Edit a Question</h3><br>
        <form action="<?php echo base_url(); ?>admin/question/action" method="post" onsubmit="return false" class="ajax_form">
		<div class="uk-grid" data-uk-grid-margin>
		
		<input type="hidden" name="form_type" value="update">
		<input type="hidden" name="id" value="<?php echo $question->id;?>">
			<br />
          <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
              <label>English question</label>
              <input type="text" class="md-input" value="<?php echo $question->eng_question;?>" name="eng_question" />
            </div>
			
          </div>
          <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
              <label>Arabic question</label>
              <input type="text" class="md-input" value="<?php echo $question->arb_question;?>" name="arb_question" />
            </div>
            
          </div>
          <br />
          <div class="uk-width-medium-1-2">
              <select class="md-input" id="type" name="type" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}" title="Select with tooltip" required="required">
                  <option value="">Question For</option>
                  <option <?php echo ($question->type == 'Feedback' ? 'selected' : '');?> value="Feedback">Feedback</option>
                  <option <?php echo ($question->type == 'Lost Item' ? 'selected' : '');?> value="Lost Item">Lost item</option>
               </select>
          </div>
		  
			<div class="uk-width-medium-1-1">
			<?php 
				$k=1;
				if($answer){
				foreach($answer as $ans){ ?>
		   <div class="uk-grid form_section <?php echo $ans->id;?>" data-section-added="<?php echo $k;?>">
		                    <input type="hidden" name="ans_ids[]" value="<?php echo $ans->id;?>">
                                <div class="uk-width-1-1">
                                    <div class="uk-input-group">
                                        <label>English Answer</label>
                                        <input type="text" class="md-input" name="eng_answer[]" value="<?php echo $ans->eng_answer;?>">
										<label>Arabic Answer</label>
										<input type="text" class="md-input" name="arb_answer[]" value="<?php echo $ans->arb_answer;?>">
                                        <span class="uk-input-group-addon">
										<a href="javascript:void(0);" onClick="deleteRecord(<?php echo $ans->id;?>,'admin/question/action','');"><i class="material-icons md-24"></i></a>
                                            
                                        </span>
                                    </div>
                                </div>
			</div>					
				<?php $k++; } } ?>
							
			 <div class="uk-grid form_section" id="d_form_row">
                                <div class="uk-width-1-1">
                                    <div class="uk-input-group">
                                        <label>English Answer</label>
                                        <input type="text" class="md-input" name="eng_answer[]">
										<label>Arabic Answer</label>
										<input type="text" class="md-input" name="arb_answer[]">
                                        <span class="uk-input-group-addon">
                                            <a href="#" class="btnSectionClone" data-section-clone="#d_form_row"><i class="material-icons md-24">&#xE146;</i></a>
                                        </span>
                                    </div>
                                </div>
            </div>
			
				<br><span class="uk-input-group-addon">Click + to add answer values
                                            <a href="#" class="btnSectionClone" data-section-clone="#d_form_row"><i class="material-icons md-24"> &#xE146;</i></a>
            </span>			
            
		  </div>
			  
			
			
	
        </div>
       </form> 
      </div>
    </div>
   
   <!-- light box for image -->
		   <div class="uk-modal" id="modal_lightbox">
                                <div class="uk-modal-dialog uk-modal-dialog-lightbox">
                                    <button type="button" class="uk-modal-close uk-close uk-close-alt"></button>
                                    <img src="<?php echo base_url().'uploads/images/categories/'.$variant->eng_image; ?>" alt=""/>
                                    
                                </div>
           </div>
			<!-- end light box for image -->   
    
    
    
  </div>
</div>

<div class="md-fab-wrapper">
        <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">
            <i class="material-icons">&#xE161;</i>
        </a>
    </div>
<div class="md-fab-wrapper" style="right:95px;">
        <a class="md-fab md-fab-primary" href="<?php echo base_url();?>admin/question" id="">
            <i class="material-icons">keyboard_backspace</i>
        </a>
    </div>