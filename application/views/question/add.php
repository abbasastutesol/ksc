<div id="page_content">
  <div id="page_content_inner">
    <div class="md-card">
      <div class="md-card-content">
        <h3 class="heading_a">Add a Question</h3>
        <form action="<?php echo base_url(); ?>admin/question/action" method="post" onsubmit="return false" class="ajax_form">
		<div class="uk-grid" data-uk-grid-margin>
		
		<input type="hidden" name="form_type" value="save">
		
			 <br />
          <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
              <label>English question</label>
              <input type="text" class="md-input" value="" name="eng_question" required="required"/>
            </div>
			
          </div>
          <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
              <label>Arabic question</label>
              <input type="text" class="md-input" value="" name="arb_question" required="required" />
            </div>
            
          </div>
          <br />
          <div class="uk-width-medium-1-2">
              <select class="md-input" id="type" name="type" data-md-selectize data-md-selectize-bottom data-uk-tooltip="{pos:'top'}" title="Select with tooltip" required="required">
                  <option value="">Question For</option>
                  <option value="Feedback">Feedback</option>
                  <option value="Lost Item">Lost item</option>
               </select>
          </div>
		
		  <div class="uk-width-medium-1-1">
		   <div class="uk-grid form_section" id="d_form_row">
                                <div class="uk-width-1-1">
                                    <div class="uk-input-group">
                                        <label>English Answer</label>
                                        <input type="text" class="md-input" name="eng_answer[]">
										<label>Arabic Answer</label>
										<input type="text" class="md-input" name="arb_answer[]">
                                        <span class="uk-input-group-addon">
                                            <a href="#" class="btnSectionClone" data-section-clone="#d_form_row"><i class="material-icons md-24">&#xE146;</i></a>
                                        </span>
                                    </div>
                                </div>
            </div>
			<br><span class="uk-input-group-addon">Click + to add answer values
                                            <a href="#" class="btnSectionClone" data-section-clone="#d_form_row"><i class="material-icons md-24"> &#xE146;</i></a>
            </span>
		  </div>
		   
			
			
	
        </div>
       </form> 
      </div>
    </div>
   
    
    
    
    
  </div>
</div>
<div class="md-fab-wrapper">
        <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">
            <i class="material-icons">&#xE161;</i>
        </a>
    </div>
<div class="md-fab-wrapper" style="right:95px;">
        <a class="md-fab md-fab-primary" href="<?php echo base_url();?>admin/question" id="">
            <i class="material-icons">keyboard_backspace</i>
        </a>
    </div>