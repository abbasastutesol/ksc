
<main class="main">
	<div class="container">
		<section class="content-wrap">
			<div class="left-sidebar">
			
                <ul>
                    
                    <?php
					$class = '';
                    $getMenu = getMenu('footer','',1);
                    foreach($getMenu as $getMe){
                        $active = activeMenuClass($_SERVER['REQUEST_URI'],$getMe[$lang.'_link'],$lang);

                        ?>

                    <li class="<?php echo $active; ?>"><a href="<?php echo $getMe[$lang.'_link']?>"><?php echo $getMe[$lang.'_menu_title']; ?></a></li>

                    <?php } ?>
                </ul>
				
			</div>
			<div class="right-content">
				<div class="large-thumbnail"><img src="<?php echo base_url();?>assets/frontend/images/<?php echo $values[0]['banner_image']; ?>" alt=""></div>
				<h1><?php echo $values[0][$lang.'_title']; ?></h1>
				<?php echo $values[0][$lang.'_description']; ?>
			</div>
		</section>
	</div>
</main>
