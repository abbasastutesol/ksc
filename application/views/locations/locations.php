 <section class="">

        <div class="container">

            <div class="cartHeading fontWhtReg">

                <h1 class="">Locations</h1>

                <div class="clearfix"></div>

            </div>

            <div class="locationSec">

                <div class="mapSec">

                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d29000.10251132215!2d46.66603254834186!3d24.692086236241522!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e2f033bb824b417%3A0xdc9745cc4a78b2d2!2sAl+Olaya%2C+Riyadh+Saudi+Arabia!5e0!3m2!1sen!2s!4v1496313775855"  frameborder="0" style="border:0" allowfullscreen></iframe>

                </div>

                <div class="LocAddSec">

                    <ul>

                        <li>

                            <div class="whiteBox haveEqHeight">

                                <h1 class="makeShadow">01</h1>

                                <h2>Riyadh Al Olaya</h2>

                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas suscipit.</p>

                                <br/>

                                <p><strong>T: </strong> <bdi>+966 1 7894561</bdi></p>

                                <p><strong>F: </strong> <bdi>+966 1 7894561</bdi></p>

                                <p><strong>E: </strong> riyadh@ioudstore.com</p>

                            </div>

                        </li>

                        <li>

                            <div class="whiteBox haveEqHeight">

                                <h1 class="makeShadow">02</h1>

                                <h2>Riyadh Al Olaya</h2>

                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas suscipit.

                                <br/> <br/>

                                <strong>T: </strong> <bdi>+966 1 7894561</bdi>

                                <p><strong>F: </strong> <bdi>+966 1 7894561</bdi></p>

                                <p><strong>E: </strong> riyadh@ioudstore.com</p>

                            </div>

                        </li>

                        <li>

                            <div class="whiteBox haveEqHeight">

                                <h1 class="makeShadow">03</h1>

                                <h2>Riyadh Al Olaya</h2>

                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas suscipit.

                                <br/> <br/>

                                <strong>T: </strong> <bdi>+966 1 7894561</bdi>

                                <p><strong>F: </strong> <bdi>+966 1 7894561</bdi></p>

                                <p><strong>E: </strong> riyadh@ioudstore.com</p>

                            </div>

                        </li>

                        <li>

                            <div class="whiteBox haveEqHeight">

                                <h1 class="makeShadow">04</h1>

                                <h2>Riyadh Al Olaya</h2>

                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas suscipit.

                                <br/> <br/>

                                <strong>T: </strong> <bdi>+966 1 7894561</bdi>

                                <p><strong>F: </strong> <bdi>+966 1 7894561</bdi></p>

                                <p><strong>E: </strong> riyadh@ioudstore.com</p>

                            </div>

                        </li>

                        <li>

                            <div class="whiteBox haveEqHeight">

                                <h1 class="makeShadow">05</h1>

                                <h2>Riyadh Al Olaya</h2>

                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas suscipit.

                                <br/> <br/>

                                <strong>T: </strong> <bdi>+966 1 7894561</bdi>

                                <p><strong>F: </strong> <bdi>+966 1 7894561</bdi></p>

                                <p><strong>E: </strong> riyadh@ioudstore.com</p>

                            </div>

                        </li>

                    </ul>

                    <div class="clearfix"></div>

                </div>

            </div>

        </div>

    </section>
    
    <script type="text/javascript">
	alert('test')
    // When the window has finished loading create our google map below
    google.maps.event.addDomListener(window, 'load', init);

    var locations = [
    <?php
        $locations = getAllBranchesLocations();
        foreach ($locations as $location) {
    ?>
        ['<?php echo $location[$lang."_name"]; ?>', <?php echo $location["lat_long"]; ?>],

    <?php } ?>
    ];


    function init() {

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 7,
            center: new google.maps.LatLng(24.774265,46.738586),
            styles: mapStyle,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var infowindow = new google.maps.InfoWindow();

        var marker, i;

        for (i = 0, index = 1; i < locations.length; i++,index++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map,
				label: {
					text: '0'+index,
					origin: {x: 100, y: 2}
				},
                //label: '0'+index,
                icon: pinSymbol("#000")
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }
    }

    // this function to make custom marker
    function pinSymbol(color) {
        return {
            path: 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z M -2,-30 a 2,2 0 1,1 4,0 2,2 0 1,1 -4,0',
            fillColor: color,
            fillOpacity: 1,
            strokeColor: '#FFF',
            strokeWeight: 2,
            scale: 1.5
        };
    }

</script>

