<?php
if($lang == 'eng') {
    $page_name = $this->uri->segment(2);
}else{
    $page_name = $this->uri->segment(1);
}
?>
<html lang="en" <?php if ($lang=='arb') { echo 'dir="rtl" '; } ?>>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
	<?php $metTags = getMetaTags($page_id); //print_r($metTags); exit;?>
	<meta name="meta_title" content="<?php echo $metTags[$lang.'_meta_title']; ?>"/>
	<meta name="meta_description" content="<?php echo $metTags[$lang.'_meta_description']; ?>"/>
	<meta name="meta_keyword" content="<?php echo $metTags[$lang.'_meta_keyword']; ?>"/>
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <title>Khereiji Showrooms Company</title>
    <!--[if gte IE 9]
        <style type="text/css">
            .gradient {
                filter: none;
            }
        </style>
    <![endif]-->
    <!--[if lt IE 9]>
    <!--<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>-->
    <![endif]-->
    <!--<link rel="stylesheet" href="css/jquery-ui_1.12.1_themes.css">-->
    <!--<link href="css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" media="all">-->
    <!--<link href="css/jquery.fancybox.css" rel="stylesheet" type="text/css" media="all">-->
    <!--<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">-->
    <link href="<?php echo base_url();?>assets/frontend/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
    <link href="<?php echo base_url();?>assets/frontend/css/<?php echo ($lang=='arb' ? 'rtl' : 'ltr'); ?>.css" rel="stylesheet" type="text/css" media="all">
    <!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=false&controls=1"></script>-->
    <script src="<?php echo base_url();?>assets/frontend/js/jquery-2.1.3.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/frontend/js/jquery-ui_1.12.1.js"></script>
    <script src="<?php echo base_url();?>assets/frontend/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/frontend/js/jquery.material.form.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/frontend/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/frontend/js/scripts.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/frontend/js/custom_script.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/frontend/js/validations.js" type="text/javascript"></script>

    <script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>

    <script>
        var base_url = "<?php echo base_url(); ?>";
        var lang_base_url = "<?php echo lang_base_url(); ?>";
        var lang = "<?php echo $lang; ?>";
    </script>

</head>

<?php


$link =  "//$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$pieces = explode("ksc/", $link);
$class_name=str_replace(".php","",$pieces[1]);

?>
<body class="<?php echo $class_name; ?> <?php echo ($page_name == '' ? 'homepage' : ''); ?>">
<header class="header scrlFX">
    <div class="top-header">
        <div class="container">
            <div class="search-box">
                <form action="<?php echo lang_base_url().'search'; ?>" method="get">
                    <input type="search" name="search" value="<?php echo (isset($search) ? $search : ''); ?>" placeholder="SEARCH">
                </form>
            </div>
            <div class="lang-box">
                <a href="javascript:void(0);" id="<?php echo ($lang == 'eng' ? 'arb' : 'eng'); ?>" onclick="switchLang($(this).attr('id'), '<?php echo uri_string(); ?>');"><?php echo ($lang=='eng' ? 'العربية' : 'English'); ?></a>
            </div>
        </div>
    </div>
    <div class="btm-header">
        <div class="container">
            <div class="logo"><a href="<?php echo lang_base_url(); ?>"><img src="<?php echo base_url();?>assets/frontend/images/logo.jpg" alt="" width="312" height="76"></a></div>
            <nav class="nav menu-main-menu-container">
                <?php
                /*params for this function
                getMenu('menu_type:header,footer','sub_menu:yes,no',Footer_type:0,1,2,3,4,5)*/
                $getMenu = getMenu('header','yes',0);
                $sub_menu = '';
                $class = '';
                foreach($getMenu as $Men){

                    $active = activeMenuClass($_SERVER['REQUEST_URI'],$Men[$lang.'_link'],$lang);
                    if($active == 'active'){
                        $class = 'active';
                    }
                    $sub_menu .='<li class="'.$active.'"><a href="'.$Men[$lang.'_link'].'">'.$Men[$lang.'_menu_title'].'</a></li>';
                } ?>
                <ul>
                    <li class="has-dropdown <?php if ($page_name == '' || $class != '') echo 'active'; ?>">
                        <a href="javascript:void(0);"><?php echo ($lang=='eng' ? 'KSC' : 'ش.م.ك'); ?></a>

                        <ul class="sub-menu">
                            <?php echo $sub_menu; ?>

                        </ul>

                    </li>

                    <?php
                    $getMenu = getMenu('header','no',0);
                    foreach($getMenu as $getMe){
                        $active = activeMenuClass($_SERVER['REQUEST_URI'],$getMe[$lang.'_link'],$lang);

                        ?>

                    <li class="<?php echo $active; ?>"><a href="<?php echo $getMe[$lang.'_link']?>"><?php echo $getMe[$lang.'_menu_title']; ?></a></li>

                    <?php } ?>
                </ul>
            </nav>
            <div class="mob-menu-icon"><a href="javascript:void(0);" class="menu-opener"><span></span></a></div>
        </div>
    </div>
</header>

<div class="loader" style="display: none;"><div class="lds-spinner" style="100%;height:100%"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>