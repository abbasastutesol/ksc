
<footer class="footer scrlFX">
    <div class="footer-top">
        <div class="container">
            <div class="footer-col">
                <ul>
                    <li class="has-dropdown active"><a
                                href="javascript:void(0);"><?php echo($lang == 'eng' ? 'KSC' : 'ش.م.ك'); ?></a>
                        <ul class="sub-menu">
                           <?php
                           $getMenu = getMenu('footer','',1);

                           foreach($getMenu as $getM){ ?>

                            <li>
                                <a href="<?php echo $getM[$lang.'_link']?>"><?php echo $getM[$lang.'_menu_title']; ?></a>
                            </li>
                            <?php } ?>
                            <!--<li>
                                <a href="vision-mission.php"><?php /*echo($lang == 'eng' ? 'MISSION &amp; VISION' : 'رؤيتنا و مهمتنا'); */?></a>
                            </li>
                            <li>
                                <a href="chairmans-message.php"><?php /*echo($lang == 'eng' ? 'CHAIRMAN`S MESSAGE' : 'رسالة رئيس مجلس الإدارة'); */?></a>
                            </li>
                            <li>
                                <a href="ceo-message.php"><?php /*echo($lang == 'eng' ? 'CEO’S MESSAGE' : 'رسالة المدير التنفيذي'); */?></a>
                            </li>
                            <li><a href="value.php"><?php /*echo($lang == 'eng' ? 'VALUES' : ' قيمنا'); */?></a></li>
                            <li><a href="awards.php"><?php /*echo($lang == 'eng' ? 'AWARDS' : ' جوائز'); */?></a></li>-->
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="footer-col">
                <ul>
                    <?php
                    $getMenus = getMenu('footer','',2);
                    foreach($getMenus as $getMe){ ?>

                    <li><a href="<?php echo $getMe[$lang.'_link']?>"><?php echo $getMe[$lang.'_menu_title']; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="footer-col">
                <ul>
                    <li class="has-dropdown"><a
                                href="#"><?php echo($lang == 'eng' ? 'SHOWROOMS' : 'صالات العرض'); ?></a>
                        <ul class="sub-menu">

                            <?php
                            $getMenus = getMenu('footer','',3);
                            foreach($getMenus as $getMe){ ?>

                            <li><a href="<?php echo $getMe[$lang.'_link']?>"><?php echo $getMe[$lang.'_menu_title']; ?></a></li>
                            <?php } ?>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="footer-col">
                <ul>
                    <li class="has-dropdown"><a
                                href="careers.php"><?php echo($lang == 'eng' ? 'CAREERS' : 'صالات العرض'); ?></a>
                        <ul class="sub-menu">
                            <?php
                            $getMenus = getMenu('footer','',4);

                            foreach($getMenus as $getMe) {

                                if ($getMe['is_link'] == 2 && $getMe['popup_type'] == 'apply') { ?>
                                    <li><a href="#." data-toggle="modal"
                                           data-target="#Modal_4"><?php echo $getMe[$lang . '_menu_title']; ?></a></li>
                                <?php } else { ?>

                                    <li>
                                        <a href="<?php echo $getMe[$lang . '_link'] ?>"><?php echo $getMe[$lang . '_menu_title']; ?></a>
                                    </li>
                                <?php }
                            }?>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="footer-col">
                <ul>
                    <?php
                    $getMenus = getMenu('footer','',5);
                    foreach($getMenus as $getMe) {

                        if ($getMe['is_link'] == 2 && $getMe['popup_type'] == 'newsletter') { ?>
                            <li><a href="#." data-toggle="modal"
                                   data-target="#Modal_6"><?php echo $getMe[$lang . '_menu_title']; ?></a></li>
                        <?php } else { ?>

                            <li>
                                <a href="<?php echo $getMe[$lang . '_link'] ?>"><?php echo $getMe[$lang . '_menu_title']; ?></a>
                            </li>
                        <?php }
                    }?>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-btm">
        <div class="container">
            <div class="footer-left">
                <div class="copyrights"><p>COPYRIGHTS | KSC, 2018</p></div>
				
                <div class="information"><a href="<?php echo lang_base_url();?>Legal_info/info">Legal Information</a></div>
            </div><?php $settings = settings(); ?>
            <div class="footer-right">
                <div class="footer-mail"><a href="mailto:customer.support@ksc.com.sa"><img
                                src="<?php echo base_url(); ?>assets/frontend/images/envelope-icon.png" alt=""><span><?php echo $settings->support_email;?></span></a></div>
                <div class="footer-number"><a href="tel:+966 12 6652757"><img src="<?php echo base_url(); ?>assets/frontend/images/telephone-icon.png" alt=""><span><?php echo $settings->phone_no;?></span></a></div>
                <div class="footer-sm-links">
                    <ul>
                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer><!-- Awards Image Modal Start-->
<?php
    $page_name = $this->uri->segment(2);
 if($page_name == 'contact_us' || $page_name == 'showrooms'){ ?>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCmPD0_pXZ9iiyvwqn_CMRx0cVBPX2AhIM&callback=initialize"
            type="text/javascript"></script>

<?php } ?>

<?php $this->load->view('layouts/modals')?>

<style>


    .loader {
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        z-index: 99999;
        background-color: rgba(0,0,0,.6);
    }
    /*page-loader*/
    @keyframes lds-spinner {
        0% {
            opacity: 1;
    }
        100% {
            opacity: 0;
    }
    }
    @-webkit-keyframes lds-spinner {
        0% {
            opacity: 1;
    }
        100% {
            opacity: 0;
    }
    }
    .lds-spinner {
        position: relative;
    }
    .lds-spinner div {
        left: 94px;
        top: 48px;
        position: absolute;
        -webkit-animation: lds-spinner linear 1s infinite;
        animation: lds-spinner linear 1s infinite;
        background: #fff;
        width: 12px;
        height: 24px;
        border-radius: 40%;
        -webkit-transform-origin: 6px 52px;
        transform-origin: 6px 52px;
    }
    .lds-spinner div:nth-child(1) {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
        -webkit-animation-delay: -0.916666666666667s;
        animation-delay: -0.916666666666667s;
    }
    .lds-spinner div:nth-child(2) {
        -webkit-transform: rotate(30deg);
        transform: rotate(30deg);
        -webkit-animation-delay: -0.833333333333333s;
        animation-delay: -0.833333333333333s;
    }
    .lds-spinner div:nth-child(3) {
        -webkit-transform: rotate(60deg);
        transform: rotate(60deg);
        -webkit-animation-delay: -0.75s;
        animation-delay: -0.75s;
    }
    .lds-spinner div:nth-child(4) {
        -webkit-transform: rotate(90deg);
        transform: rotate(90deg);
        -webkit-animation-delay: -0.666666666666667s;
        animation-delay: -0.666666666666667s;
    }
    .lds-spinner div:nth-child(5) {
        -webkit-transform: rotate(120deg);
        transform: rotate(120deg);
        -webkit-animation-delay: -0.583333333333333s;
        animation-delay: -0.583333333333333s;
    }
    .lds-spinner div:nth-child(6) {
        -webkit-transform: rotate(150deg);
        transform: rotate(150deg);
        -webkit-animation-delay: -0.5s;
        animation-delay: -0.5s;
    }
    .lds-spinner div:nth-child(7) {
        -webkit-transform: rotate(180deg);
        transform: rotate(180deg);
        -webkit-animation-delay: -0.416666666666667s;
        animation-delay: -0.416666666666667s;
    }
    .lds-spinner div:nth-child(8) {
        -webkit-transform: rotate(210deg);
        transform: rotate(210deg);
        -webkit-animation-delay: -0.333333333333333s;
        animation-delay: -0.333333333333333s;
    }
    .lds-spinner div:nth-child(9) {
        -webkit-transform: rotate(240deg);
        transform: rotate(240deg);
        -webkit-animation-delay: -0.25s;
        animation-delay: -0.25s;
    }
    .lds-spinner div:nth-child(10) {
        -webkit-transform: rotate(270deg);
        transform: rotate(270deg);
        -webkit-animation-delay: -0.166666666666667s;
        animation-delay: -0.166666666666667s;
    }
    .lds-spinner div:nth-child(11) {
        -webkit-transform: rotate(300deg);
        transform: rotate(300deg);
        -webkit-animation-delay: -0.083333333333333s;
        animation-delay: -0.083333333333333s;
    }
    .lds-spinner div:nth-child(12) {
        -webkit-transform: rotate(330deg);
        transform: rotate(330deg);
        -webkit-animation-delay: 0s;
        animation-delay: 0s;
    }
    .lds-spinner {
        position: absolute;
        top:0;
        bottom: 0;
        left: 0;
        right: 0;
        width: 83px !important;
        height: 83px !important;
        -webkit-transform: translate(-41.5px, -41.5px) scale(0.415) translate(41.5px, 41.5px);
        transform: translate(-41.5px, -41.5px) scale(0.415) translate(41.5px, 41.5px);
        margin: auto;
    }

    position:fixed;top:0;left:0;right:0;bottom:0;z-index:99999;background-color:rgba(0,0,0,.6)}.line,.pos_relative{position:relative


</style>
</body>
</html>