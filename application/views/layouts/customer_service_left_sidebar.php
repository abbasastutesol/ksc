<!--<script>
    $('header .rightSec .menu li:nth-child(3)').addClass('active');
</script>-->

<div class="leftMenu">
    <ul>
        <li class="<?php if($class == 'customer_service'){ echo 'active';} ?>"><a href="<?php echo lang_base_url().'customer_service'; ?>"><?php echo getLeftMenu('customer_service'); ?></a></li>
        <li class="<?php if($class == 'latest_news'){ echo 'active';} ?>"><a href="<?php echo lang_base_url().'customer_service/latest_news'; ?>"><?php echo $latest_news_label; ?></a></li>
        <li class="<?php if($class == 'payment_confirm'){ echo 'active';} ?>"><a href="<?php echo lang_base_url().'customer_service/payment_confirm'; ?>"><?php echo $payment_confirmation_label; ?></a></li>
        <li class="<?php if($class == 'returns'){ echo 'active';} ?>"><a href="<?php echo lang_base_url().'customer_service/returns'; ?>"><?php echo getLeftMenu('return_policy'); ?></a></li>
        <li class="<?php if($class == 'distributor'){ echo 'active';} ?>"><a href="<?php echo lang_base_url().'customer_service/distributor'; ?>"><?php echo getLeftMenu('distributor'); ?></a></li>
        <li class="<?php if($class == 'privacy_policy'){ echo 'active';} ?>"><a href="<?php echo lang_base_url().'customer_service/privacy_policy'; ?>"><?php echo getLeftMenu('privacy_policy'); ?></a></li>
        <li class="<?php if($class == 'medauf_program'){ echo 'active';} ?>"><a href="<?php echo lang_base_url().'customer_service/medauf_program'; ?>"><?php echo getLeftMenu('meduaf_program'); ?></a></li>
    </ul>
</div>

