<div class="uk-width-medium-1-3">                            
    <button class="md-btn alert-message-button" data-uk-modal="{target:'#modal_default', bgclose:false}" style="display:none;">Open</button>
	
	<button class="md-btn validation-message-button" data-uk-modal="{target:'#modal_validation'}" style="display:none;">Open</button>
	
	<div class="uk-modal" id="modal_default">   
		 <div class="uk-modal-dialog">         
		 <button type="button" class="uk-modal-close uk-close admin-modal"></button>      
		 <h2 id="alert-message-heading"></h2> 
		 <div id="alert-message"></div>   
		 </div>   
	</div>
	
	<div class="uk-modal" id="modal_validation">   
		 <div class="uk-modal-dialog">         
		 <button type="button" class="uk-modal-close uk-close"></button>      
		 <h2 id="validation-message-heading"></h2> 
		 <div id="validation-message"></div>   
		 </div>   
	</div>

    <!--for pick location-->
    <button class="md-btn location-pick-button" data-uk-modal="{target:'#modal_location'}" style="display:none;">Open</button>

    <div class="uk-modal" id="modal_location">
        <div class="uk-modal-dialog">
            <button type="button" class="uk-modal-close uk-close"></button>
            <h2 id="location-message-heading"></h2>
            <div id="location-message"><div id="location_map" style="height: 320px; position: relative; overflow: hidden;"></div></div>

            <button style="margin-left: 38%; margin-top: 20px;" id="close_popup" type="button" class=" md-btn md-btn-primary">Choose</button>

        </div>

    </div>

</div>

<!-- google web fonts -->
    <script>
        WebFontConfig = {
            google: {
                families: [
                    'Source+Code+Pro:400,700:latin',
                    'Roboto:400,300,500,700,400italic:latin'
                ]
            }
        };
        (function() {
            var wf = document.createElement('script');
            wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
            '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })();
    </script>

    <!-- common functions -->
    <script src="<?php echo base_url();?>assets/admin/assets/js/common.min.js"></script>
    
    <!-- uikit functions -->
    <script src="<?php echo base_url();?>assets/admin/assets/js/uikit_custom.min.js"></script>
    <!-- altair common functions/helpers -->
    <script src="<?php echo base_url();?>assets/admin/assets/js/altair_admin_common.min.js"></script>
    
    <!-- ckeditor -->
    <script src="<?php echo base_url();?>assets/admin/bower_components/ckeditor/ckeditor.js"></script>
    
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
		
    <script type="text/javascript">CKEDITOR_BASEPATH = '<?php echo base_url();?>assets/admin/bower_components/ckeditor/';</script>	
	<script type="text/javascript">

        $('.eng_text').each(function (e) {
            CKEDITOR.replace(this.id, {toolbar: 'Full', width: '95%', height: '200px'});
        });
        $('.arb_text').each(function (e) {
            CKEDITOR.replace(this.id, {language: 'ar', toolbar: 'Full', width: '95%', height: '200px'});
        });


     	/*CKEDITOR.replace('eng_description', {toolbar : 'Full',width : '95%',height : '200px'});
		CKEDITOR.replace('arb_description', {toolbar : 'Full',width : '95%',height : '200px'});
		CKEDITOR.replace('eng_washing_instruction', {toolbar : 'Full',width : '95%',height : '200px'});
		CKEDITOR.replace('arb_washing_instruction', {toolbar : 'Full',width : '95%',height : '200px'});
		CKEDITOR.replace('eng_brief_description', {toolbar : 'Full',width : '95%',height : '200px'});
		CKEDITOR.replace('arb_brief_description', {toolbar : 'Full',width : '95%',height : '200px'});
		CKEDITOR.replace('eng_more_detail', {toolbar : 'Full',width : '95%',height : '200px'});
		CKEDITOR.replace('arb_more_detail', {toolbar : 'Full',width : '95%',height : '200px'});*/

    </script>


    <!-- page specific plugins -->
        <!-- d3 -->
        <script src="<?php echo base_url();?>assets/admin/bower_components/d3/d3.min.js"></script>
        <!-- metrics graphics (charts) -->
       <!-- <script src="<?php echo base_url();?>assets/admin/bower_components/metrics-graphics/dist/metricsgraphics.min.js"></script>-->
        <!-- chartist (charts) -->
        <script src="<?php echo base_url();?>assets/admin/bower_components/chartist/dist/chartist.min.js"></script>
        <!-- maplace (google maps) -->
       <!-- <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script src="<?php echo base_url();?>assets/admin/bower_components/maplace-js/dist/maplace.min.js"></script>-->
        <!-- peity (small charts) -->
        <script src="<?php echo base_url();?>assets/admin/bower_components/peity/jquery.peity.min.js"></script>
        <!-- easy-pie-chart (circular statistics) -->
        <script src="<?php echo base_url();?>assets/admin/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
        <!-- countUp -->
        <script src="<?php echo base_url();?>assets/admin/bower_components/countUp.js/dist/countUp.min.js"></script>
        <!-- handlebars.js -->
        <script src="<?php echo base_url();?>assets/admin/bower_components/handlebars/handlebars.min.js"></script>
        <script src="<?php echo base_url();?>assets/admin/assets/js/custom/handlebars_helpers.min.js"></script>
        <!-- CLNDR -->
        <script src="<?php echo base_url();?>assets/admin/bower_components/clndr/clndr.min.js"></script>
        <!-- fitvids -->
        <script src="<?php echo base_url();?>assets/admin/bower_components/fitvids/jquery.fitvids.js"></script>

        <!--  dashbord functions -->
        <script src="<?php echo base_url();?>assets/admin/assets/js/pages/dashboard.min.js"></script>
        
        <!--kendoui Js-->
        <!-- kendo UI -->
        <script src="<?php echo base_url();?>assets/admin/assets/js/kendoui_custom.min.js"></script>
    
        <!--  kendoui functions -->
        <script src="<?php echo base_url();?>assets/admin/assets/js/pages/kendoui.min.js"></script>
        
    	<script src="<?php echo base_url();?>assets/admin/assets/js/pages/kendoui.min.js"></script>


<link href="https://cdn.datatables.net/rowreorder/1.0.0/css/rowReorder.dataTables.min.css" type="text/css" rel="stylesheet">
		<!-- page specific plugins -->
    <!-- datatables -->
    <script src="<?php echo base_url();?>assets/admin/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <!-- datatables colVis-->
    
    <!-- datatables custom integration -->
    <script src="<?php echo base_url();?>assets/admin/assets/js/custom/datatables_uikit.min.js"></script>

    <!--  datatables functions -->
    <script src="<?php echo base_url();?>assets/admin/assets/js/pages/plugins_datatables.min.js"></script> 
	<script src="<?php echo base_url();?>assets/admin/assets/js/pages/product_edit.js"></script>
    <script src="<?php echo base_url();?>assets/admin/assets/js/script.js"></script>
    <script src="<?php echo base_url();?>assets/admin/assets/js/jquery.cookie.js"></script>
    <script src="https://cdn.datatables.net/rowreorder/1.0.0/js/dataTables.rowReorder.min.js"></script>

    <script>
	
        $(function() {
            if(isHighDensity) {
                // enable hires images
                altair_helpers.retina_images();
            }
            if(Modernizr.touch) {
                // fastClick (touch devices)
                FastClick.attach(document.body);
            }
        });
        $window.load(function() {
            // ie fixes
            altair_helpers.ie_fix();
        });
    </script>
	
    <div id="style_switcher">
        <div id="style_switcher_toggle"><i class="material-icons">&#xE8B8;</i></div>
        <div class="uk-margin-medium-bottom">
            <h4 class="heading_c uk-margin-bottom">Colors</h4>
            <ul class="switcher_app_themes" id="theme_switcher">
                <li class="app_style_default active_theme" data-app-theme="">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_a" data-app-theme="app_theme_a">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_b" data-app-theme="app_theme_b">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_c" data-app-theme="app_theme_c">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_d" data-app-theme="app_theme_d">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_e" data-app-theme="app_theme_e">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_f" data-app-theme="app_theme_f">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_g" data-app-theme="app_theme_g">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_h" data-app-theme="app_theme_h">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_i" data-app-theme="app_theme_i">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
                <li class="switcher_theme_dark" data-app-theme="app_theme_dark">
                    <span class="app_color_main"></span>
                    <span class="app_color_accent"></span>
                </li>
            </ul>
        </div>
        <div class="uk-visible-large uk-margin-medium-bottom">
            <h4 class="heading_c">Sidebar</h4>
            <p>
                <input type="checkbox" name="style_sidebar_mini" id="style_sidebar_mini" data-md-icheck />
                <label for="style_sidebar_mini" class="inline-label">Mini Sidebar</label>
            </p>
            <p>
                <input type="checkbox" name="style_sidebar_slim" id="style_sidebar_slim" data-md-icheck />
                <label for="style_sidebar_slim" class="inline-label">Slim Sidebar</label>
            </p>
        </div>
        <div class="uk-visible-large uk-margin-medium-bottom">
            <h4 class="heading_c">Layout</h4>
            <p>
                <input type="checkbox" name="style_layout_boxed" id="style_layout_boxed" data-md-icheck />
                <label for="style_layout_boxed" class="inline-label">Boxed layout</label>
            </p>
        </div>
        <div class="uk-visible-large">
            <h4 class="heading_c">Main menu accordion</h4>
            <p>
                <input type="checkbox" name="accordion_mode_main_menu" id="accordion_mode_main_menu" data-md-icheck />
                <label for="accordion_mode_main_menu" class="inline-label">Accordion mode</label>
            </p>
        </div>
    </div>

<!--these variables for page sepecify script-->
<?php $path = $this->uri->segment(2);
    $full_path = $this->uri->segment(2).'/'.$this->uri->segment(3);
?>


    <script>
        $(function() {
			for (i = new Date().getFullYear(); i > 1900; i--)
{
    $('#yearpicker').append($('<option />').val(i).html(i));
}
            var $switcher = $('#style_switcher'),
                $switcher_toggle = $('#style_switcher_toggle'),
                $theme_switcher = $('#theme_switcher'),
                $mini_sidebar_toggle = $('#style_sidebar_mini'),
                $slim_sidebar_toggle = $('#style_sidebar_slim'),
                $boxed_layout_toggle = $('#style_layout_boxed'),
                $accordion_mode_toggle = $('#accordion_mode_main_menu'),
                $html = $('html'),
                $body = $('body');

            
            $switcher_toggle.click(function(e) {
                e.preventDefault();
                $switcher.toggleClass('switcher_active');
            });

            $theme_switcher.children('li').click(function(e) {
                e.preventDefault();
                var $this = $(this),
                    this_theme = $this.attr('data-app-theme');

                $theme_switcher.children('li').removeClass('active_theme');
                $(this).addClass('active_theme');
                $html
                    .removeClass('app_theme_a app_theme_b app_theme_c app_theme_d app_theme_e app_theme_f app_theme_g app_theme_h app_theme_i app_theme_dark')
                    .addClass(this_theme);

                if(this_theme == '') {
                    localStorage.removeItem('altair_theme');
                } else {
                    localStorage.setItem("altair_theme", this_theme);
                    if(this_theme == 'app_theme_dark') {
                        $('#kendoCSS').attr('href','bower_components/kendo-ui/styles/kendo.materialblack.min.css')
                    }
                }

            });

            // hide style switcher
            $document.on('click keyup', function(e) {
                if( $switcher.hasClass('switcher_active') ) {
                    if (
                        ( !$(e.target).closest($switcher).length )
                        || ( e.keyCode == 27 )
                    ) {
                        $switcher.removeClass('switcher_active');
                    }
                }
            });

            // get theme from local storage
            if(localStorage.getItem("altair_theme") !== null) {
                $theme_switcher.children('li[data-app-theme='+localStorage.getItem("altair_theme")+']').click();
            }


        // toggle mini sidebar

            // change input's state to checked if mini sidebar is active
            if((localStorage.getItem("altair_sidebar_mini") !== null && localStorage.getItem("altair_sidebar_mini") == '1') || $body.hasClass('sidebar_mini')) {
                $mini_sidebar_toggle.iCheck('check');
            }

            $mini_sidebar_toggle
                .on('ifChecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.setItem("altair_sidebar_mini", '1');
                    localStorage.removeItem('altair_sidebar_slim');
                    location.reload(true);
                })
                .on('ifUnchecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.removeItem('altair_sidebar_mini');
                    location.reload(true);
                });

        // toggle slim sidebar

            // change input's state to checked if mini sidebar is active
            if((localStorage.getItem("altair_sidebar_slim") !== null && localStorage.getItem("altair_sidebar_slim") == '1') || $body.hasClass('sidebar_slim')) {
                $slim_sidebar_toggle.iCheck('check');
            }

            $slim_sidebar_toggle
                .on('ifChecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.setItem("altair_sidebar_slim", '1');
                    localStorage.removeItem('altair_sidebar_mini');
                    location.reload(true);
                })
                .on('ifUnchecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.removeItem('altair_sidebar_slim');
                    location.reload(true);
                });

        // toggle boxed layout

            if((localStorage.getItem("altair_layout") !== null && localStorage.getItem("altair_layout") == 'boxed') || $body.hasClass('boxed_layout')) {
                $boxed_layout_toggle.iCheck('check');
                $body.addClass('boxed_layout');
                $(window).resize();
            }

            $boxed_layout_toggle
                .on('ifChecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.setItem("altair_layout", 'boxed');
                    location.reload(true);
                })
                .on('ifUnchecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.removeItem('altair_layout');
                    location.reload(true);
                });

        // main menu accordion mode
            if($sidebar_main.hasClass('accordion_mode')) {
                $accordion_mode_toggle.iCheck('check');
            }

            $accordion_mode_toggle
                .on('ifChecked', function(){
                    $sidebar_main.addClass('accordion_mode');
                })
                .on('ifUnchecked', function(){
                    $sidebar_main.removeClass('accordion_mode');
                });


        });
var page = '<?php echo $this->uri->segment(2).'/'.$this->uri->segment(3); ?>';

if(page == 'career/edit') {
// select country and city script for career page
    $(document).ready(function () {

        $('.countryClassCart > #' + replacedCountry).attr('selected', 'selected');


        $.ajax({
            url: base_url + "ajax/getCities",
            type: 'POST',
            data: {'country': country},
            dataType: 'json',
            success: function (data) {

                $(".cityClassCart").html(data.html);
                $('.cityClassCart > #' + replacedCity).attr('selected', 'selected');

            }
        });

    });
}
/* $(function($) {//console.log('d');
    // Asynchronously Load the map API 
    var script = document.createElement('script');
    script.src = "//maps.googleapis.com/maps/api/js?sensor=false&callback=initialize";
    document.body.appendChild(script);
	
}); */ 
var map; //Will contain map object.
var marker = false; ////Has the user plotted their location marker? 
        
//Function called to initialize / create the map.
//This is called when the page has loaded.
function initMap() {
 
    //The center location of our map.
    var centerOfMap = new google.maps.LatLng(52.357971, -6.516758);
 
    //Map options.
    var options = {
      center: centerOfMap, //Set center.
      zoom: 7 //The zoom value.
    };
 
    //Create the map object.
    map = new google.maps.Map(document.getElementById('map'), options);
 
    //Listen for any clicks on the map.
    google.maps.event.addListener(map, 'click', function(event) {                
        //Get the location that the user clicked.
        var clickedLocation = event.latLng;
        //If the marker hasn't been added.
        if(marker === false){
            //Create the marker.
            marker = new google.maps.Marker({
                position: clickedLocation,
                map: map,
                draggable: true //make it draggable
            });
            //Listen for drag events!
            google.maps.event.addListener(marker, 'dragend', function(event){
                markerLocation();
            });
        } else{
            //Marker has already been added, so just change its location.
            marker.setPosition(clickedLocation);
        }
        //Get the marker's location.
        markerLocation();
    });
}
        
//This function will get the marker's current location and then add the lat/long
//values to our textfields so that we can save the location.
function markerLocation(){
    //Get location.
    var currentLocation = marker.getPosition();
    //Add lat and lng values to a field that we can save.
    document.getElementById('lat').value = currentLocation.lat(); //latitude
    document.getElementById('lng').value = currentLocation.lng(); //longitude
}
        
        
//Load the map when the page has finished loading.
google.maps.event.addDomListener(window, 'load', initMap);

    </script>
<script>
    $('.order_limit').on('change',function () {
        var limits = $(this).val();

        //document.cookie = "order_limit = " + limits;
        $.cookie('order_limits', limits);

        window.location.href = base_url+'admin/orders'

    });

    $('.product_limit').on('change',function () {
        var limit = $(this).val();
        $.cookie('product_limits', limit);
        window.location.href = base_url+'admin/product'

    });

</script>


<?php if($path == "orders") { ?>

    <!-- google web fonts -->
    <script>


        //order status filter
        $('.order_status_filter').click(function(){
            var status = $(this).data('cat');

            document.cookie = "order_status = " + status;
            window.location.reload();
        });

        WebFontConfig = {
            google: {
                families: [
                    'Source+Code+Pro:400,700:latin',
                    'Roboto:400,300,500,700,400italic:latin'
                ]
            }
        };
        (function() {
            var wf = document.createElement('script');
            wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
                '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })();
    </script>
    

    <script>
        // Script Added by Adnan Khalid
        $('.uk-dropdown .parentCatEd li.hasDropDown > a').click(function (e) {
            $('.uk-dropdown .parentCatEd li.hasDropDown').removeClass('open');
            $(this).parent().addClass('open');
        });

        $(document).ready(function($){
            $(".singleOrder .orderRow .colEd:nth-child(7) button.btn, .topIcons li").click(function(e) {
                //do something
                e.stopPropagation();
                //alert();
            })
            $('.singleOrder .orderRow.uk-parent').click(function (e) {
                $('.singleOrder .orderExpand.uk-child').slideUp();
                $('.singleOrder .orderRow.uk-parent').slideDown();
                $(this).slideUp();
                $(this).next('.orderExpand.uk-child').slideDown();
                $('.singleOrder').removeClass('open');
                $(this).parent().addClass('open');
            })
            $('.singleOrder .orderExpand.uk-child .orderRow').click(function (e) {
                $('.singleOrder .orderExpand.uk-child').slideUp();
                $('.singleOrder .orderRow.uk-parent').slideDown();
                $('.singleOrder').removeClass('open');
            })
        });

    </script>

<?php
} if($full_path == "orders/order_detail"){ ?>
    
    <!-- google web fonts -->
    <script>
        WebFontConfig = {
            google: {
                families: [
                    'Source+Code+Pro:400,700:latin',
                    'Roboto:400,300,500,700,400italic:latin'
                ]
            }
        };
        (function() {
            var wf = document.createElement('script');
            wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
                '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })();
    </script>

<?php } ?>

<?php
    if($path == "product"){ ?>

        <script>

            //order status filter

            function getProductByCat(cat_id) {

                $.cookie('prod_cat_id', cat_id);
                window.location.href = base_url+'admin/product'
            }

            function getProductByAvail(available) {

                $.cookie('available_product', available);
                window.location.href = base_url+'admin/product'
            }
            
            //      Adnan Khalid
            $(document).ready(function(e){

                $('#cancel').click(function () {
                    $(this).hide();
                    $('#applyBtn').hide();
                    $('.forIndex ').removeClass('active');
                    $('.proListView tbody tr').removeClass('active');
                    $('#massDisBtn').fadeIn();
                    $('.proSelection').hide();
                });


                $('#massDisBtn').click(function (e) {
                    $(this).hide();
                    $('#cancel').fadeIn();
                    $('#applyBtn').fadeIn();
                    $('.proSelection').fadeIn();
                });
                $('.proGridView .forIndex .proSelection a').click(function () {
                    var gridBoxIndex = $(this).parent().parent().parent().parent().index() ;
                    var gridBoxIndex = gridBoxIndex+1;
                    //     alert(gridBoxIndex );
                    $('.proGridView .forIndex:nth-child('+gridBoxIndex+')').toggleClass('active')
                    $('.proListView tr:nth-child('+gridBoxIndex+')').toggleClass('active');
                });
                $('.proListView tr td .proSelection a').click(function () {
                    var listViewIndex = $(this).parent().parent().parent().index() ;
                    var listViewIndex = listViewIndex+1;
                    //     alert(listViewIndex);
                    $('.proGridView .forIndex:nth-child('+listViewIndex+')').toggleClass('active');
                    $(this).parent().parent().parent().toggleClass('active');
                });

                /*             $('.proSelection a').click(function () {
                 $(this).toggleClass('active');
                 });*/


                //     Switching between grid and list
                $('#listBtn').click(function () {
                    $('.proGridView').hide();
                    $(this).hide();
                    $('.proListView').show();
                    $('#gridBtn').css('display','inline-block');
                });
                $('#gridBtn').click(function () {
                    $('.proListView').hide();
                    $(this).hide();
                    $('.proGridView').show();
                    $('#listBtn').css('display','inline-block');
                });

            });



        </script>
<?php } ?>

<?php if($full_path == "product/detail") { ?>


    <script>

        // Script Added by Adnan Khalid
        $('.uk-dropdown .parentCatEd li.hasDropDown > a').click(function (e) {
            $('.uk-dropdown .parentCatEd li.hasDropDown').removeClass('open');
            $(this).parent().addClass('open');
        });
        //  File chick by Adnan Khalid
        $('.uk-form-file ').click(function (){
            //    alert();
            $('input[type="file"]').trigger();
        });
        //  Click Function on Popuping up
        

    </script>

<?php } ?>


<div class="page_loader"></div>

</body>
</html>