 <!-- main sidebar -->

    <aside id="sidebar_main">

        <div class="sidebar_main_header">

            <div class="sidebar_logo">

                <a href="<?php echo base_url();?>" class="sSidebar_hide sidebar_logo_large">

                    <img class="logo_regular" src="<?php echo base_url();?>assets/frontend/images/logo.png" alt="" height="15" width="71"/>

                    <img class="logo_light" src="<?php echo base_url();?>assets/frontend/images/logo.png" alt="" height="15" width="71"/>

                </a>

                <a href="<?php echo base_url();?>" class="sSidebar_show sidebar_logo_small">

                    <img class="logo_regular" src="<?php echo base_url();?>assets/admin/assets/img/logo_main_small.png" alt="" height="32" width="32"/>

                    <img class="logo_light" src="<?php echo base_url();?>assets/admin/assets/img/logo_main_small_light.png" alt="" height="32" width="32"/>

                </a>

            </div>

           <!-- <div class="sidebar_actions">

                <select id="lang_switcher" name="lang_switcher">

                    <option value="gb" selected>English</option>

                </select>

            </div>-->

        </div>

        

        <div class="menu_section">

            <ul>
					<?php if($user_rights_array['Dashboard']['show_p'] == 1) { ?>
                        <li class="<?php if($class == 'dashboard'){ echo 'current_section';} ?>" title="Dashboard">

                        <a href="<?php echo base_url();?>admin/dashboard">

                            <span class="menu_icon"><i class="material-icons">&#xE871;</i></span>

                            <span class="menu_title">Dashboard</span>

                        </a>
						</li>
                    <?php }?>
                                         
					<?php if($user_rights_array['Category']['show_p'] == 1 || $user_rights_array['Products']['show_p'] == 1) { ?>
                       <li title="E-commerce" class="<?php if($class == 'category' || $class =='product'|| $class == 'variant'){ echo 'current_section';} ?>">

                        <a href="#">

                            <span class="menu_icon"><i class="material-icons">&#xE8CB;</i></span>

                            <span class="menu_title">E-commerce</span>

                        </a>

                                                    <ul>
								<?php if($user_rights_array['Category']['show_p'] == 1) { ?>
									 <li class="<?php if($class == 'category'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/category">Categories</a></li>
                                <?php }if($user_rights_array['Category']['show_p'] == 1) { ?>
                                    <li class="<?php if($class == 'brand'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/brand">Brands</a></li>
                                <?php } if($user_rights_array['Products']['show_p'] == 1) { ?>
									<li class="<?php if($class == 'product'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/product">Products<?php if(getAllProductUnreadCount() > 0){?>

                                            <span style="color: red;font-size: 14px;font-weight: bolder;line-height: 0.5px;display: inline-block;vertical-align: top;"><?php echo getAllProductUnreadCount();?></span>

                                                    <?php }?></a></li>
								<?php }?>

<!--									<li class="--><?php //if($class == 'variant'){ echo 'act_item';} ?><!--"><a href="--><?php //echo base_url();?><!--admin/variant">Variants</a></li>-->

<!--									<li class="--><?php //if($class == 'offers'){ echo 'act_item';} ?><!--"><a href="--><?php //echo base_url();?><!--admin/offers">Offers</a></li>-->

                                                                                                </ul>

                        </li>
                     <?php }?>

                     <?php if($user_rights_array['Registered Users']['show_p'] == 1) { ?>   

                        <li title="Users" class="<?php if($class == 'registered_user'){ echo 'current_section';} ?>">

                        <a href="#">

                            <span class="menu_icon"><i class="material-icons">people</i></span>

                            <span class="menu_title">Users</span>

                        </a>

                               <ul>

                               		<li class="<?php if($class == 'registered_user'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/users">Registered Users</a></li>

                               </ul>

						</li>
                     <?php }?>
                        
                        
                        
                        
                     <?php if($user_rights_array['Contact Us Requests']['show_p'] == 1 || $user_rights_array['Distributors Requests']['show_p'] == 1 || $user_rights_array['Payment Requests']['show_p'] == 1) { ?>   
                        <li title="Users" class="<?php if($class == 'contact_us'){ echo 'current_section';} ?>">

                        <a href="#">

                            <span class="menu_icon"><i class="material-icons">&#xE7FD;</i></span>

                            <span class="menu_title">Users Requests</span>

                        </a>

                               <ul>
								 <?php if($user_rights_array['Contact Us Requests']['show_p'] == 1) { ?>
                               		<li class="<?php if($class == 'contact_us_request'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/contact_us/contact_requests">Contact Us Requests</a></li>
                                  <?php } if($user_rights_array['Distributors Requests']['show_p'] == 1) { ?>  
                                   <li class="<?php if($class == 'email_us'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/email_us">Email Requests</a></li>
								 <?php } if($user_rights_array['Payment Requests']['show_p'] == 1) { ?>  
								   <li class="<?php if($class == 'quota_request'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/quota_request">User Quota Requests</a></li>
								<?php }?>
                               </ul>

						</li>
                    <?php }?>
                        
                        
                        
					<?php if($user_rights_array['Career Content']['show_p'] == 1 || $user_rights_array['Jobs']['show_p'] == 1 || $user_rights_array['Job Applicants']['show_p'] == 1) { ?>
                        

                        <li title="Career" class="<?php if($class == 'career'){ echo 'current_section';} ?>">

                        <a href="#">

                            <span class="menu_icon"><i class="material-icons">work</i></span>

                            <span class="menu_title">Career</span>

                        </a>

                               <ul>

                               	
                                  <li class="<?php if($class == 'career_dep'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/Career/job_dep">Departments</a></li>
								  <?php if($user_rights_array['Career Content']['show_p'] == 1) { ?>
                                     <li class="<?php if($class == 'career'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/career">Career Content</a></li>
                                <?php } if($user_rights_array['Jobs']['show_p'] == 1) { ?>

                                     <li class="<?php if($class == 'career_jobs'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/career/jobs">Jobs</a></li> 
								<?php } if($user_rights_array['Job Applicants']['show_p'] == 1) { ?>
                                     <li class="<?php if($class == 'applied_jobs'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/career/appliedJobs">Job Applicants</a></li>
								<?php }?>
                               </ul>

						</li>
				<?php }?>


				<?php if($user_rights_array['Payment & Delivery']['show_p'] == 1 || $user_rights_array['Latest News']['show_p'] == 1 ||
                $user_rights_array['Returns']['show_p'] == 1 ||
                $user_rights_array['Be A Distributor']['show_p'] == 1 ||
                $user_rights_array['Privacy Policy']['show_p'] == 1 ||
                $user_rights_array['Payment Confirm']['show_p'] == 1 ||
                $user_rights_array['Meduaf program']['show_p'] == 1) { ?>

                <li title="Customer_service" class="<?php if($class == 'customer_service'){ echo 'current_section';} ?>">

                    <a href="#">

                        <span class="menu_icon"><i class="material-icons">&#xE87C;</i></span>

                        <span class="menu_title">Customer Service</span>

                    </a>

                    <ul>
					<?php if($user_rights_array['Payment & Delivery']['show_p'] == 1) { ?>
                        <li class="<?php if($class == 'customer_service'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/customer_service">Payment & Delivery</a></li>
					<?php } if($user_rights_array['Latest News']['show_p'] == 1) { ?>
                        <li class="<?php if($class == 'latest_news'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/latest_news">Latest News</a></li>
					<?php } if($user_rights_array['Returns']['show_p'] == 1) { ?>
                        <li class="<?php if($class == 'return_policy'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/returnPolicy">Returns</a></li>
					<?php } if($user_rights_array['Be A Distributor']['show_p'] == 1) { ?>
                        <li class="<?php if($class == 'distributor'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/distributor">Be A Distributor</a></li>
					<?php } if($user_rights_array['Privacy Policy']['show_p'] == 1) { ?>
                        <li class="<?php if($class == 'privacy_policy'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/privacy_policy">Privacy Policy</a></li>

                    <?php } if($user_rights_array['Payment Confirm']['show_p'] == 1) { ?>
                        <li class="<?php if($class == 'payment_confirm'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/payment_confirmation">Payment Confirm</a></li>
                    <?php } if($user_rights_array['Meduaf program']['show_p'] == 1) { ?>
                            <li class="<?php if($class == 'meduaf_program'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/customer_service/meduaf_program">Meduaf Program</a></li>
                        <?php }?>
                    </ul>

                </li>
               <?php }?>



			<?php if($user_rights_array['About Us']['show_p'] == 1 || $user_rights_array['Mission & Vision']['show_p'] == 1 || $user_rights_array['Why Ioud']['show_p'] == 1 || $user_rights_array['Our Distinctiveness']['show_p'] == 1) { ?>

                <li title="aboutus" class="<?php if($class == 'aboutus'){ echo 'current_section';} ?>">

                    <a href="#">

                        <span class="menu_icon"><i class="material-icons">&#xE7FC;</i></span>

                        <span class="menu_title">About Us</span>

                    </a>

                    <ul>
					<?php //if($user_rights_array['About Us']['show_p'] == 1) { ?>
                        <!---<li class="<?php //if($class == 'aboutus'){ echo 'act_item';} ?>"><a href="<?php //echo base_url();?>admin/aboutUs">About Us</a></li>--->
					<?php //} 
					if($user_rights_array['Mission & Vision']['show_p'] == 1) { ?>
                        <li class="<?php if($class == 'mission_vision'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/mission_vision">Mission & Vision</a></li>
					<?php } //if($user_rights_array['Why Ioud']['show_p'] == 1) { ?>
                        <!---<li class="<?php //if($class == 'why_ioud'){ echo 'act_item';} ?>"><a href="<?php //echo base_url();?>admin/why_ioud">Why Ioud</a></li>
					<?php //} if($user_rights_array['Our Distinctiveness']['show_p'] == 1) { ?>
                        <li class="<?php //if($class == 'distinctiveness'){ echo 'act_item';} ?>"><a href="<?php //echo base_url();?>admin/distinctiveness">Our Distinctiveness</a></li>--->
					<?php // }
					//if($user_rights_array['Our Distinctiveness']['show_p'] == 1) { ?>
                        <li class="<?php if($class == 'ChairManMessage'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/ChairManmessage">Chairman Message</a></li>
						<li class="<?php if($class == 'CeoMessage'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/CeoMessage">Ceo's Message</a></li>
						<li class="<?php if($class == 'Value'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/Value">Value</a></li>
						<li class="<?php if($class == 'Projects'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/Projects">Project's</a></li>
						<li class="<?php if($class == 'Project_gallery'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/Projects/gallery">Project's gallery</a></li>
						<li class="<?php if($class == 'Legal_info'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/Legal_info">legal Information</a></li>
						 <li class="<?php if($class == 'company_profile'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/Company_profile">Company Profile</a></li>
						 <li class="<?php if($class == 'Awards'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/Awards">Awards</a></li>
					<?php //}?>
                    </ul>

                </li>
			<?php }?>


                        <!--<li title="Designers" class="<?php /*if($class == 'designers'){ echo 'current_section';} */?>">

                        <a href="#">

                            <span class="menu_icon"><i class="material-icons">art_track</i></span>

                            <span class="menu_title">Designers</span>

                        </a>

                               <ul>

                               		<li class="<?php /*if($class == 'designers'){ echo 'act_item';} */?>"><a href="<?php /*echo base_url();*/?>admin/designer/">Designers</a></li>

                               		 <li class="<?php /*if($class == 'designers_app'){ echo 'act_item';} */?>"><a href="<?php /*echo base_url();*/?>admin/designer/applications">Designers Applications</a></li>

                                     

                               </ul>

						</li>-->

                        

                       <!-- <li title="Reviews" class="<?php /*if($class == 'review'){ echo 'current_section';} */?>">

                        <a href="#">

                            <span class="menu_icon"><i class="material-icons">rate_review</i></span>

                            <span class="menu_title">Reviews</span>

                        </a>

                               <ul>

                               		<li class="<?php /*if($class == 'review'){ echo 'act_item';} */?>"><a href="<?php /*echo base_url();*/?>admin/review/">Reviews</a></li>

                  

                                     

                               </ul>

						</li>-->



                        <!--<li title="Newsletter" class="<?php /*if($class == 'newsletter'){ echo 'current_section';} */?>">

                        <a href="#">

                            <span class="menu_icon"><i class="material-icons">drafts</i></span>

                            <span class="menu_title">Newsletter</span>

                        </a>

                               <ul>

                               		 

                                     <li class="<?php /*if($class == 'newsletter'){ echo 'act_item';} */?>"><a href="<?php /*echo base_url();*/?>admin/newsletter">Subscribers</a></li>

                               </ul>

						</li>-->
					<?php if($user_rights_array['Orders']['show_p'] == 1 || $user_rights_array['Order Feedback']['show_p'] == 1) { ?>
                        <li title="Orders" class="<?php if($class == 'orders' || $class == 'returnItems' || $class == 'feedBack' || $class == 'lostItems' || $class == 'currentCarts'){ echo 'current_section';} ?>">

                        <a href="#">

                            <span class="menu_icon"><i class="material-icons">content_paste</i></span>

                            <span class="menu_title">Sales</span>

                        </a>

                               <ul>
                                   <?php if($user_rights_array['Orders']['show_p'] == 1) { ?>
                                    <li class="<?php if($class == 'orders'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/orders/">Orders</a></li>
                                   <?php } ?>
                                   <?php if($user_rights_array['Order Feedback']['show_p'] == 1) { ?>
                                   <li class="<?php if($class == 'feedBack'){ echo 'act_item';} ?>"><a href="<?php echo base_url(); ?>admin/orders/feedBack">FeedBack</a></li>

                                   <?php } ?>
                                     <!--<li class="<?php /*if($class == 'feedBack'){ echo 'act_item';} */?>"><a href="<?php /*echo base_url();*/?>admin/orders/feedBack">Feed Back</a></li>

                                     <li class="<?php /*if($class == 'lostItems'){ echo 'act_item';} */?>"><a href="<?php /*echo base_url();*/?>admin/orders/lostItems">Lost Items</a></li>

                                     <li class="<?php /*if($class == 'currentCarts'){ echo 'act_item';} */?>"><a href="<?php /*echo base_url();*/?>admin/orders/currentCarts">Current Carts</a></li>

                                     <li class="<?php /*if($class == 'products_not_purchased'){ echo 'act_item';} */?>"><a href="<?php /*echo base_url();*/?>admin/product/productNotPurchased">Products Never Purchased</a></li>
-->


                               </ul>

						</li>
                   <?php }?>
                   <?php if($user_rights_array['Coupon']['show_p'] == 1 || $user_rights_array['Loyalty']['show_p'] == 1) { ?>
                        <li title="Orders" class="<?php if($class == 'coupon'){ echo 'current_section';} ?>">

                        <a href="#">

                            <span class="menu_icon"><i class="material-icons">share</i></span>

                            <span class="menu_title">Marketing</span>

                        </a>

                               <ul>
                                   <?php if($user_rights_array['Coupon']['show_p'] == 1) { ?>
                                       <li class="<?php if($class == 'coupon'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/coupon/">Coupon</a></li>
                                   <?php } ?>
                                   <?php if($user_rights_array['Loyalty']['show_p'] == 1) { ?>
                                   <li class="<?php if($class == 'loyalty'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/loyalty/">Loyalty</a></li>
                                   <?php } ?>

								</ul>

						</li>
                   <?php }?>
				   <?php if($user_rights_array['Home']['show_p'] == 1 || $user_rights_array['Sliders']['show_p'] == 1 || $user_rights_array['Contact Us']['show_p'] == 1 || $user_rights_array['Faqs']['show_p'] == 1 || $user_rights_array['Branches']['show_p'] == 1 || $user_rights_array['Promotions']['show_p'] == 1 || $user_rights_array['Terms & Conditions']['show_p'] == 1) { ?>
                        <li title="Settings" class="<?php if($class == 'home' || $class == 'question'){ echo 'current_section';} ?>">

                        <a href="#">

                            <span class="menu_icon"><i class="material-icons">&#xE7F9;</i></span>

                            <span class="menu_title">Content Pages</span>

                        </a>

                               <ul>
								<?php if($user_rights_array['Home']['show_p'] == 1) { ?>
                               		 <li class="<?php if($class == 'home'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/home/index/1">Home</a></li>
								<?php } if($user_rights_array['Sliders']['show_p'] == 1) { ?>
                               		 <li class="<?php if($class == 'slider'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/home/slider">Sliders</a></li>
								<?php } if($user_rights_array['Contact Us']['show_p'] == 1) { ?>
                                     <li class="<?php if($class == 'contact_us'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/contact_us">Contact Us</a></li>
								<?php } if($user_rights_array['Faqs']['show_p'] == 1) { ?>
                                     <li class="<?php if($class == 'faqs'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/faqs">Faqs</a></li>
								<?php } if($user_rights_array['Branches']['show_p'] == 1) { ?>
                                     <li class="<?php if($class == 'branches'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/locations">Locations</a></li>
								<?php } //if($user_rights_array['Promotions']['show_p'] == 1) { ?>							 
									 <!---<li class="<?php //if($class == 'promotions'){ echo 'act_item';} ?>"><a href="<?php //echo base_url();?>admin/promotion">Promotions</a></li>--->
								<?php //}
								if($user_rights_array['Terms & Conditions']['show_p'] == 1) { ?>									 
									 <li class="<?php if($class == 'terms_conditions'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/termsConditions">Terms & Conditions</a></li>
								<?php }//if($user_rights_array['company_profile']['show_p'] == 1) { ?>									 
									
									 <?php //}if($user_rights_array['company_profile']['show_p'] == 1) { ?>									 
									 <li class="<?php if($class == 'clients'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/Clients">Clients</a></li>
									 
									 <li class="<?php if($class == 'show_room'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/Show_room">Show Rooms</a></li>
								<?php //}?>
                               </ul>

						</li>
                  <?php }?>

				<?php if($user_rights_array['Menu Setting']['show_p'] == 1 || $user_rights_array['Currency']['show_p'] == 1 || $user_rights_array['Settings']['show_p'] == 1 || $user_rights_array['Shipment Groups']['show_p'] == 1) { ?>
						<li title="Settings" class="<?php if($class == 'currency' || $class == 'home' || $class == 'question' || $class == 'menu_bar'){ echo 'current_section';} ?>">

                        <a href="#">

                            <span class="menu_icon"><i class="material-icons">settings</i></span>

                            <span class="menu_title">General Settings</span>

                        </a>

                               <ul>
                                   <?php if($user_rights_array['Menu Setting']['show_p'] == 1) { ?>
                                   <li class="<?php if($class == 'menu_bar'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/settings/menu_bar">Menu Settings</a></li>
                                   <?php } ?>
                                   <?php if($user_rights_array['Currency']['show_p'] == 1) { ?>
									 <li class="<?php if($class == 'currency'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/currency">Currency</a></li>
								<?php } if($user_rights_array['Settings']['show_p'] == 1) { ?>
                                     <li class="<?php if($class == 'settings'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/settings">Settings</a></li>
                                <?php } if($user_rights_array['Shipment Groups']['show_p'] == 1) { ?>
                                     <li class="<?php if($class == 'shipment_groups'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/shipment_groups">Shipment Groups</a></li>
                                
                                <?php } if($user_rights_array['Maintenance Mode']['show_p'] == 1) { ?>
                                     <li class="<?php if($class == 'maintenance'){ echo 'act_item';} ?>"><a href="<?php echo base_url();?>admin/maintenanceMode">Maintenance Mode</a></li>
								<?php }?>
                               </ul>
						</li>
				<?php }?>

                <?php if($user_rights_array['User Rights']['show_p'] == 1 || $user_rights_array['Add User']['show_p'] == 1) { ?>
					<li title="User Rights" class="<?php if ($class == 'user_rights') {
					echo 'current_section';
					} ?>">
						<a href="#"> <span class="menu_icon"><i class="material-icons">&#xE8D3;</i></span> <span
						class="menu_title">Admin</span> </a>
						<ul>
							<?php if($user_rights_array['User Rights']['show_p'] == 1) { ?>
								<li class="<?php if ($class == 'user_rights') {
								echo 'act_item';
							} ?>"><a href="<?php echo base_url(); ?>admin/user_rights">User Rights</a></li>
							<?php } if($user_rights_array['Add User']['show_p'] == 1) { ?>
								<li class="<?php if ($class == 'add_user') {
								echo 'act_item';
							} ?>"><a href="<?php echo base_url(); ?>admin/add_user">Add User</a></li>
							<?php } ?>
						</ul>
					</li>
				<?php } ?>
                <?php if($user_rights_array['Newsletter Subscribers']['show_p'] == 1) { ?> 
					   <li class="<?php if($class == 'newsletter'){ echo 'current_section';} ?>" title="Dashboard">

                        <a href="<?php echo base_url();?>admin/newsletter">

                            <span class="menu_icon"><i class="material-icons">&#xE7F9;</i></span>

                            <span class="menu_title">Newsletter Subscribers</span>

                        </a>
						</li>  
               <?php }?>

                <?php
                        $liClass =  '';
                        $display =  'none;';
                        $liSubClass1 =  '';
                        $liSubClass2 =  '';
                        $liSubClass3 =  '';
                        $liSubClass4 =  '';
                        $liSubClass5 =  '';
                        $liSubClass6 =  '';
                        $liSubClass7 =  '';

                        if($class == 'report_order'){
                            $liClass =  'current_section submenu_trigger act_section';
                            $display =  'block;';
                            $liSubClass1 =  'act_item';
                        }
                        if($class == 'report_shipping'){
                            $liClass =  'current_section submenu_trigger act_section';
                            $display =  'block;';
                            $liSubClass2 =  'act_item';
                        }
                        if($class == 'report_returns'){
                            $liClass =  'current_section submenu_trigger act_section';
                            $display =  'block;';
                            $liSubClass3 =  'act_item';
                        }
                        if($class == 'report_coupon'){
                            $liClass =  'current_section submenu_trigger act_section';
                            $display =  'block;';
                            $liSubClass4 =  'act_item';
                        }
                        if($class == 'report_viewed'){
                            $liClass =  'current_section';
                            $display =  'block;';
                            $liSubClass5 =  'act_item';
                        }
                        if($class == 'report_purchased'){
                            $liClass =  'current_section';
                            $display =  'block;';
                            $liSubClass6 =  'act_item';
                        }
                        if($class == 'cust_order'){
                            $liClass =  'current_section';
                            $display =  'block;';
                            $liSubClass7 =  'act_item';
                        }
                ?>
                <?php if($user_rights_array['Reports']['show_p'] == 1) { ?>
                    <li title="User Rights" class="<?php echo $liClass; ?>">
                        <a href="avascript:void(0);"> <span class="menu_icon"><i class="material-icons">event</i></span>
                            <span class="menu_title">Reports</span>
                        </a>
                        <ul id="testtest" style="display: <?php echo $display; ?>">
                            <li class="<?php echo $liClass; ?>">
                            <a href="javascript:void(0);"> <span class="menu_icon"></span>
                                <span class="menu_title">Sales</span>
                            </a>
                                <ul>
                                    <li class="<?php echo $liSubClass1; ?>">
                                        <a href="<?php echo base_url();?>admin/report/orders"> <span class="menu_icon"></span>
                                            <span class="menu_title">Orders</span>
                                        </a>
                                    </li>
                                    <li class="<?php echo $liSubClass2; ?>">
                                        <a href="<?php echo base_url();?>admin/report/shipping"> <span class="menu_icon"></span>
                                            <span class="menu_title">Shipping</span>
                                        </a>
                                    </li>
                                    <li class="<?php echo $liSubClass3; ?>">
                                        <a href="<?php echo base_url();?>admin/report/returns"> <span class="menu_icon"></span>
                                            <span class="menu_title">Returns</span>
                                        </a>
                                    </li>

                                    <li class="<?php echo $liSubClass4; ?>">
                                        <a href="<?php echo base_url();?>admin/report/coupon"> <span class="menu_icon"></span>
                                            <span class="menu_title">Coupon</span>
                                        </a>
                                    </li>

                                </ul>

                            </li>

                            <li class="<?php echo $liClass; ?>">
                                <a href="javascript:void(0);"> <span class="menu_icon"></span>
                                    <span class="menu_title">Products</span>
                                </a>
                            <ul>
                                <li class="<?php echo $liSubClass5; ?>">
                                    <a href="<?php echo base_url();?>admin/report/product_viewed"> <span class="menu_icon"></span>
                                        <span class="menu_title">Viewed</span>
                                    </a>
                                </li>

                                <li class="<?php echo $liSubClass6; ?>">
                                    <a href="<?php echo base_url();?>admin/report/purchased"> <span class="menu_icon"></span>
                                        <span class="menu_title">Purchased</span>
                                    </a>
                                </li>


                            </ul>
                            </li>



                            <li class="<?php echo $liClass; ?>">
                                <a href="javascript:void(0);"> <span class="menu_icon"></span>
                                    <span class="menu_title">Customers</span>
                                </a>
                                <ul>
                                    <li class="<?php echo $liSubClass7; ?>">
                                        <a href="<?php echo base_url();?>admin/report/customer_orders"> <span class="menu_icon"></span>
                                            <span class="menu_title">Orders</span>
                                        </a>
                                    </li>

                                </ul>
                            </li>


                        </ul>

                    </li>
                <?php } ?>

                <?php if($user_rights_array['Site Logs']['show_p'] == 1 ) { ?>
                    <li class="<?php if($class == 'logs'){ echo 'current_section';} ?>" title="Dashboard">

                        <a href="<?php echo base_url();?>admin/report/logs">

                            <span class="menu_icon"><i class="material-icons">assessment</i></span>

                            <span class="menu_title">Site logs</span>

                        </a>
                    </li>
                <?php } ?>

            </ul>

        </div>

    </aside><!-- main sidebar end -->