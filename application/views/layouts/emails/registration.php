<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<!-- If you delete this tag, the sky will fall on your head -->
	<meta name="viewport" content="width=device-width" />

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>IOUD</title>
<!--<base href="http://ioud.ed.sa/html/email_temp/" target="_blank">-->
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700" rel="stylesheet"> 
	<style type="text/css">
		.divWBox {
			background-color: #fff;
			background-image: url("images/bgShadow.png");
			background-position: left top;
			background-repeat: repeat-y;
			background-size: 100% 1px;
			padding: 30px 20px 13px;
			width: 100%;
		}
		* {
			margin:0;
			padding:0;
		}
		* { font-family: "Roboto Condensed", sans-serif;} 

		img {
			max-width: 100%;
		}
		body {
			-webkit-font-smoothing:antialiased;
			-webkit-text-size-adjust:none;
			width: 100%!important;
			height: 100%;
		}


		/* -------------------------------------------
                PHONE
                For clients that support media queries.
                Nothing fancy.
        -------------------------------------------- */
		@media only screen and (max-width: 600px) {

			a[class="btn"] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}

			div[class="column"] { width: auto!important; float:none!important;}

			table.social div[class="column"] {
				width:auto!important;
			}

		}
	</style>
</head>

<body bgcolor="#FFFFFF" style="background-color: #fff; font-family: Roboto Condensed, sans-serif;">

<table class="head-wrap" style="background-color: #1c2931;
								border: 0 none;
								margin: 0 auto;
								max-width: 660px;
								width: 660px;" bgcolor="#1c2931" border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="width: 20px;"></td>
			<td class="header container" style="clear: both;display: block;margin: 0 auto;max-width: 620px">
				<table style=" width: 100%; " border="0" cellpadding="0" cellspacing="0">
					<tbody>
						<tr>
							<td colspan="2" style="padding: 8px 0;">
								<a href="javascript:void(0);">
									<img src="images/logo.png" alt="logo" height="54" width="85" />
								</a>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td style="width: 20px;"></td>
		</tr>
	</tbody>
</table><!-- Header END -->

<table class="Textarea" style="background-color: #fff;
								border: 0 none;
								margin: 0 auto;
								max-width: 660px;
								width: 660px" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="width: 20px;"></td>
			<td class="header container" style="clear: both;display: block;margin: 0 auto;max-width: 620px">
				<table style=" width: 100%; "  border="0" cellpadding="0" cellspacing="0">
					<tbody>
						<tr>
							<td align="left" style="
													color: #666666;
													font-size: 15px;
													font-weight: 300;
													line-height: 1.2;
													min-height: 82px;
													padding: 24px 0;
													width: 50%;
													">
								Hello, Basit Chughtai, <br />
								Your Order has been received...
							</td>
							<td align="right" style="
													color: #2c8c13;
													font-size: 25px;
													font-weight: 300;
													min-height: 82px;
													padding: 24px 0;
													width: 50%;
													">
								Registered successfully!
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td style="width: 20px;"></td>
		</tr>
	</tbody>
</table><!-- Order Status END -->

<table class="whiteBox" style="background-color: #fff;
								border: 0 none;
								margin: 0 auto;
								max-width: 660px;
								padding-bottom:34px;
								width: 660px" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="width: 11px;"></td>
			<td style="clear: both;display: block;margin: 0 auto;max-width: 637px">
				<img src="images/topShadhow.png" alt="Shadhow" height="6" width="637" />
			</td>
			<td style="width: 11px;"></td>
		</tr>
		<tr>
			<td style="width: 11px;"></td>
			<td class="header container" style="clear: both;display: block;margin: 0 auto;max-width: 637px">
				<table style="
						background-color: #fff;
						background-image: url(images/bgShadow.png);
						background-position: left top;
						background-repeat: repeat-y;
						background-size: 100% 1px;
						padding: 30px 20px 13px;
						width: 100%;
							  " class="divWBox" border="0" cellpadding="0" cellspacing="0" background="images/bgShadow.png">
					<tbody>
						<tr>
							<td colspan="2" align="left" style="
													color: #666666;
													font-size: 15px;
													font-weight: 300;
													line-height: 1.2;
													min-height: 82px;
													width: 50%;
													">
								<span style="
									color: #2c8c13;
									display: block;
									font-size: 16px;
									letter-spacing: 4px;
									padding-bottom: 20px;
									width: 100%;
								">Order Number <strong>1123214</strong></span>
								
								Will be delivered in <strong>2 Business days</strong>
								<br />
								Approximately to:
								<br /><br />
								<strong>Basit Chughtai</strong>
								<br />
								82-21 150th Avenue
								<br />
								JED 778400
								<br />
								Springfield Gardens, NY 11413
								<br />
								United States
								<br />
								Phone: 718-553-8740
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
									<thead>
										<tr>
											<th colspan="4" align="right" style="
												color: #9f9277;
												font-size: 16px;
												font-weight: 300;
												padding: 0 10px 15px;
											">&nbsp;</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td style="border-top: 1px solid #d0d0d0; padding: 10px; width: 40px;">
												&nbsp;
											</td>
											<td style="
												border-top: 1px solid #d0d0d0;
												color: #333333;
												font-size: 13px;
												font-weight: 700;
												letter-spacing: 1px;
												padding: 10px;
												width:190px;
											">User Name</td>
											<td style="
												border-top: 1px solid #d0d0d0;
												color: #333333;
												font-size: 13px;
												font-weight: 700;
												letter-spacing: 1px;
												padding:10px;
											"  >Adnan Khalid</td>
										</tr>
										<tr>
											<td style="border-top: 1px solid #d0d0d0; padding: 10px; width: 40px;">
												&nbsp;
											</td>
											<td style="
												border-top: 1px solid #d0d0d0;
												color: #333333;
												font-size: 13px;
												font-weight: 700;
												letter-spacing: 1px;
												padding: 10px;
												width:190px;
											">Email Address</td>
											<td style="
												border-top: 1px solid #d0d0d0;
												color: #333333;
												font-size: 13px;
												font-weight: 700;
												letter-spacing: 1px;
												padding:10px;
											">adnan.khalid@astutesol.com</td>
										</tr>
										<tr>
											<td style="border-top: 1px solid #d0d0d0; padding: 10px; width: 40px;">
												&nbsp;
											</td>
											<td style="
												border-top: 1px solid #d0d0d0;
												color: #333333;
												font-size: 13px;
												font-weight: 700;
												letter-spacing: 1px;
												padding: 10px;
												width:190px;
											">Ph Number:</td>
											<td style="
												border-top: 1px solid #d0d0d0;
												color: #333333;
												font-size: 13px;
												font-weight: 700;
												letter-spacing: 1px;
												padding:10px;
											" >+92-321-0070080</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td style="width: 11px;"></td>
		</tr>		
		<tr>
			<td style="width: 11px;"></td>
			<td style="clear: both;display: block;margin: 0 auto;max-width: 637px">
				<img src="images/bottomShadhow.png" alt="Shadhow" height="12" width="637" />
			</td>
			<td style="width: 11px;"></td>
		</tr>
	</tbody>
</table><!-- Main White Box END -->

<table class="footer-wrap" style="background-color: #343434;
								border: 0 none;
								margin: 0 auto;
								max-width: 660px;
								width: 660px" bgcolor="#343434" border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="width: 20px;"></td>
			<td class="header container" style="clear: both;display: block;margin: 0 auto;max-width: 620px">
				<table style=" width: 100%; " border="0" cellpadding="0" cellspacing="0">
					<tbody>
						<tr>
							<td style="padding: 10px 0; width: 50%;">
								<a href="javascript:void(0);" style="margin-right: 25px;">
									<img src="images/insta.png" alt="Insta" height="19" width="19" />
								</a>
								<a href="javascript:void(0);" style="margin-right: 25px;">
									<img src="images/twitter.png" alt="Twitter" height="19" width="19" />
								</a>
								<a href="javascript:void(0);">
									<img src="images/facebook.png" alt="Facebook" height="19" width="19" />
								</a>
							</td>
							<td align="right" style="
													 color: #c1b9a6;
													 font-size: 13px;
													 height: 62px;
													 padding:10px 0;
													 width: 50%;">
								Copyrights Protected, All rights reserved
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td style="width: 20px;"></td>
		</tr>
	</tbody>
</table><!-- Footer END -->


</body>
</html>