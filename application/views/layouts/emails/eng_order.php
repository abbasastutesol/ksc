<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<!-- If you delete this tag, the sky will fall on your head -->
	<meta name="viewport" content="width=device-width" />

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url();?>assets/frontend/images/favicon.png">
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url();?>assets/frontend/images/favicon.ico">
	<title><?php echo $site_title?></title>
<base href="<?php echo base_url();?>" target="_blank">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700" rel="stylesheet">
	<style type="text/css">
		.divWBox {
			background-color: #fff;
			background-image: url("images/bgShadow.png");
			background-position: left top;
			background-repeat: repeat-y;
			background-size: 100% 1px;
			padding: 30px 20px 13px;
			width: 100%;
		}
		* {
			margin:0;
			padding:0;
		}
		* { font-family: "Roboto Condensed", sans-serif;} 

		img {
			max-width: 100%;
		}
		body {
			-webkit-font-smoothing:antialiased;
			-webkit-text-size-adjust:none;
			width: 100%!important;
			height: 100%;
		}


		/* -------------------------------------------
                PHONE
                For clients that support media queries.
                Nothing fancy.
        -------------------------------------------- */
		@media only screen and (max-width: 600px) {

			a[class="btn"] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}

			div[class="column"] { width: auto!important; float:none!important;}

			table.social div[class="column"] {
				width:auto!important;
			}

		}
	</style>
</head>

<body bgcolor="#FFFFFF" style="background-color: #fff; font-family: Roboto Condensed, sans-serif;">

<table class="head-wrap" style="background-color: #1c2931;
								border: 0 none;
								margin: 0 auto;
								max-width: 100%;
								width: 100%;" bgcolor="#1c2931" border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="width: 20px;"></td>
			<td class="header container" style="clear: both;display: block;margin: 0 auto;max-width: 620px">
				<table style=" width: 100%; " border="0" cellpadding="0" cellspacing="0">
					<tbody>
						<tr>
							<td colspan="2" style="padding: 8px 0;">
								<a href="javascript:void(0);">
									<img src="assets/frontend/images/print_page/logo.png" alt="logo" height="54" width="85" />
								</a>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td style="width: 20px;"></td>
		</tr>
	</tbody>
</table><!-- Header END -->

<table class="Textarea" style="background-color: #fff;
								border: 0 none;
								margin: 0 auto;
								max-width: 100%;
								width: 100%" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="width: 20px;"></td>
			<td class="header container" style="clear: both;display: block;margin: 0 auto;max-width: 620px">
				<table style=" width: 100%; "  border="0" cellpadding="0" cellspacing="0">
					<tbody>
						<tr>
							<td align="left" style="
													color: #666666;
													font-size: 15px;
													font-weight: 300;
													line-height: 1.2;
													min-height: 82px;
													padding: 24px 0;
													width: 50%;
													">
								<?php echo $email_template_hello;?> <?php
                                if($type == 'admin'){
								    echo $com_admin;
								}
								elseif($type == 'customer')
								{
								    if(!$this->session->userdata('user_id') && $this->session->userdata('login') == false)
								    {
								            echo $user->full_name;
								    } else
								        {
								            echo $reg_user->first_name.' '.$reg_user->last_name;
								    }
								}else{
                                    if(!$this->session->userdata('user_id') && $this->session->userdata('login') == false)
                                    {
                                        echo $user->full_name;
                                    } else
                                    {
                                        echo $reg_user->first_name.' '.$reg_user->last_name;
                                    }

                                } ?>, <br />
                                
								<?php echo $my_order_your_order_recieved;?>...
							</td>
							<td align="right" style="
													color: #2c8c13;
													font-size: 25px;
													font-weight: 300;
													min-height: 82px;
													padding: 24px 0;
													width: 50%;
													">
								<?php echo $my_order_received_label;?>!
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td style="width: 20px;"></td>
		</tr>
	</tbody>
</table><!-- Order Status END -->

<table class="whiteBox" style="background-color: #fff;
								border: 0 none;
								margin: 0 auto;
								max-width: 100%;
								padding-bottom:120px;
								width: 100%" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="width: 11px;"></td>
			<td style="clear: both;display: block;margin: 0 auto;max-width: 637px">
				<img src="assets/frontend/images/print_page/topShadhow.png" alt="Shadhow" height="6" width="637" />
			</td>
			<td style="width: 11px;"></td>
		</tr>
		<tr>
			<td style="width: 11px;"></td>
			<td class="header container" style="clear: both;display: block;margin: 0 auto;max-width: 637px">
				<table style="
						background-color: #fff;
						background-image: url(images/bgShadow.png);
						background-position: left top;
						background-repeat: repeat-y;
						background-size: 100% 1px;
						padding: 30px 20px 13px;
						width: 100%;
							  " class="divWBox" border="0" cellpadding="0" cellspacing="0" background="assets/frontend/images/print_page/bgShadow.png">
					<tbody>
						<tr>
							<td align="left" style="
													color: #666666;
													font-size: 15px;
													font-weight: 300;
													line-height: 1.2;
													min-height: 82px;
													width: 50%;
													">
								<span style="
									color: #2c8c13;
									display: block;
									font-size: 16px;
									letter-spacing: 4px;
									padding-bottom: 20px;
									width: 100%;
								"><?php echo $my_order_order_no_label;?> <strong><?php echo $order_id;?></strong></span>
								
								<?php /*?><?php echo ($order_detail->shipemnt_type == 'fast' ? $shipmentGroup[$lang.'_fast_shipment_days'] : $shipmentGroup[$lang.'_normal_shipment_days']);?><?php */?>
								<br />
								<?php /*?><?php echo $my_order_approximately_to;?>:
								<br /><br /><?php */?>
								<strong><?php echo $user->address_title; ?></strong>
								<br />
								<?php echo $user->address_1; ?>
								<br />
								<?php echo $user->address_2; ?>
								<br />
								<?php echo getCoutryByCode($user->country).", ".getCityById($user->city); ?>
								<br />
                                <?php echo $phone_label;?>: <?php echo $user->phone_no; ?>
							</td>
							<td align="right" style="
													color: #333333;
													font-size: 14px;
													font-weight: 400;
													vertical-align: top;
													width: 50%;
													">
								<table style="width: 100%; text-align: center; line-height: 1.2" border="0" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td colspan="4" style="padding-bottom: 11px; padding-left: 6px;">
                                            <?php
												$statusimg = ''; 
												if($order_detail->order_status == 2 || $order_detail->order_status == 3)
												{
													$statusimg = 'step_2';
												}elseif($order_detail->order_status == 3)
												{
													$statusimg = 'step_3';
												}elseif($order_detail->order_status == 5 || $order_detail->order_status == 6)
												{
													$statusimg = 'step_4';
												}
												else
												{
													$statusimg = 'step_1';
												}
											?>
												<img src="assets/frontend/images/print_page/<?php echo $statusimg;?>.png" alt="Step" height="" width="" />
											</td>
										</tr>
										<tr>
											<td>Order <br /> Received</td>
											<td>Preparing <br /> Shipment</td>
											<td>Shipped</td>
											<td>Delivered</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
									<thead>
										<tr>
											<th colspan="4" align="right" style="
												color: #9f9277;
												font-size: 16px;
												font-weight: 300;
												padding: 0 10px 15px;
											">(<?php echo count($products)?> Products)</th>
										</tr>
									</thead>
									<tbody>
                                    <?php

									$total = 0;
									$totalQuan = 0;
				
									foreach ($products as $key => $product) {
				
									$images =  getProductImages($product['id']);
									foreach($images as $key2 => $prod_img){
										if($prod_img['is_thumbnail'] == 1){
											$thumbnail = $prod_img['thumbnail'];
										}
									}
				
									$total += getPriceByOffer($product['id'])*$carts[$key]['quantity'];
				
									$totalQuan += $carts[$key]['quantity'];
									?>
										<tr>
											<td style="border-top: 1px solid #d0d0d0; padding: 10px; width: 73px;">
												<img src="<?php echo base_url(); ?>uploads/images/thumbs/products/<?php echo $thumbnail; ?>" alt="prod" width="52" height="46" >
											</td>
											<td style="
												border-top: 1px solid #d0d0d0;
												color: #333333;
												font-size: 13px;
												font-weight: 700;
												letter-spacing: 1px;
												padding: 10px;
												width:200px;
											"><?php echo $product[$lang.'_name']; ?></td>
											<td style="
												border-top: 1px solid #d0d0d0;
												color: #333333;
												font-size: 13px;
												font-weight: 700;
												letter-spacing: 1px;
												padding: 10px;
												width:150px;
											">Qty: <span style="display: inline-block"><?php echo $carts[$key]['quantity']; ?></td>
											<td style="
												border-top: 1px solid #d0d0d0;
												color: #333333;
												font-size: 13px;
												font-weight: 700;
												letter-spacing: 1px;
												padding: 10px;
											"  align="right"><?php echo number_format(getPriceByOffer($product['id']),'2','.',''); ?> <span style="display: inline-block"> <?php echo getCurrency($lang);?></span></td>
										</tr>
                                        
                  					<?php } ?>
                                    
									</tbody>
									<tfoot>
										<tr>
											<th align="right" colspan="3" style="
												border-top: 1px solid #d0d0d0;
												color: #333333;
												font-size: 13px;
												font-weight: 700;
												letter-spacing: 1px;
												padding: 10px 100px 10px 10px;
												width: 150px;
											"><?php if($order_detail->shipment_type == 'normal'){ echo $checkout_shipping_address_normal_shipping; }elseif($order_detail->shipment_type == 'fast'){ echo $checkout_shipping_address_fast_shipping; }else{ echo $checkout_shipping_address_free_shipping; }?>:</th>
											<th align="right" style="
												border-top: 1px solid #d0d0d0;
												color: #333333;
												font-size: 13px;
												font-weight: 700;
												letter-spacing: 1px;
												padding: 10px;
												width: 150px;
											"><?php echo number_format(intval($order_detail->shipment_price),'2','.','');?> <span style="display: inline-block;"> <?php echo getCurrency($lang);?></span></th>
										</tr>
                                        <?php if($order_detail->coupon_id > 0){
											$coupon = getCouponDiscountDb($order_detail->coupon_id);
											?>
                                            <tr>
                                                <th align="right" colspan="3" style="
                                                        color: #333333;
                                                        font-size: 13px;
                                                        font-weight: 700;
                                                        letter-spacing: 1px;
                                                        padding: 10px 100px 10px 10px;
                                                        width: 150px;
                                                    "><?php echo $shopping_cart_voucher_code;?>:</th>
                                                <th align="right" style="
                                                    color: #333333;
                                                    font-size: 13px;
                                                    font-weight: 700;
                                                    letter-spacing: 1px;
                                                    padding: 10px;
                                                    width: 150px;
                                                "><?php if($coupon->type == 1)
												{
													echo $coupon->discount.'%'; 
												}
												else
												{
													echo $coupon->discount.' <span style="display: inline-block;"> '.getCurrency($lang).'</span>'; 
												}?></th>
										</tr>
                                        <?php }?>

                                        <!--loyalty-->
                                        <?php if($order_detail->loyalty_discount != '' && $order_detail->loyalty_discount > 0){
                                        ?>
                                            <tr>
                                                <th align="right" colspan="3" style="
                                                        color: #333333;
                                                        font-size: 13px;
                                                        font-weight: 700;
                                                        letter-spacing: 1px;
                                                        padding: 10px 100px 10px 10px;
                                                        width: 150px;
                                                    "><?php echo $loyalty_discount_label;?>:</th>
                                                <th align="right" style="
                                                        color: #333333;
                                                        font-size: 13px;
                                                        font-weight: 700;
                                                        letter-spacing: 1px;
                                                        padding: 10px;
                                                        width: 150px;
                                                    ">
                                                    <?php

                                                    $loyaltyDiscount = currencyRatesCalculate($order_detail->loyalty_discount,$order_detail->currency_rate,$order_detail->id);

                                                    echo $loyaltyDiscount['rate'].' <span style="display: inline-block;"> '.$loyaltyDiscount['unit'].'</span>';
                                                    ?>
                                                </th>
                                            </tr>
                                        <?php }?>


                                        <?php $gifts = getGiftCarDetailDb($order_detail->id);
                                        
                                            if($gifts){
                                                
                                                foreach($gifts as $gift)
                                                {
                                            ?>
                                            <tr>
                                                <th align="right" colspan="3" style="
                                                    color: #333333;
                                                    font-size: 13px;
                                                    font-weight: 700;
                                                    letter-spacing: 1px;
                                                    padding: 10px 100px 10px 10px;
                                                    width: 150px;
                                                "><?php echo $gift[$lang.'_name'];?>:</th>
                                                <th align="right" style="
                                                    color: #333333;
                                                    font-size: 13px;
                                                    font-weight: 700;
                                                    letter-spacing: 1px;
                                                    padding: 10px;
                                                    width: 150px;
                                                "><?php echo number_format($gift['amount'],'2','.','');?> <span style="display: inline-block;"> <?php echo getCurrency($lang);?></span></th>
                                            </tr>
                                            
                                        <?php 	}
                                              }
                                                ?>
                                        <?php if(strtolower($order_detail->payment_method) == "cash on delivery"){
											?>
                                            <tr>
                                                <th align="right" colspan="3" style="
                                                        color: #333333;
                                                        font-size: 13px;
                                                        font-weight: 700;
                                                        letter-spacing: 1px;
                                                        padding: 10px 100px 10px 10px;
                                                        width: 150px;
                                                    "><?php echo $my_order_cod_label;?>:</th>
                                                <th align="right" style="
                                                    color: #333333;
                                                    font-size: 13px;
                                                    font-weight: 700;
                                                    letter-spacing: 1px;
                                                    padding: 10px;
                                                    width: 150px;
                                                "><?php echo number_format($order_detail->extra_charges,'2','.','');?> <span style="display: inline-block;"> <?php echo getCurrency($lang);?></span></th>
										</tr>
                                        <?php }?>


                                        <?php if($order_detail->tracking_id != ""){
                                            ?>
                                            <tr>
                                                <th align="right" colspan="3" style="
                                                        color: #333333;
                                                        font-size: 13px;
                                                        font-weight: 700;
                                                        letter-spacing: 1px;
                                                        padding: 10px 100px 10px 10px;
                                                        width: 150px;
                                                    "><?php echo $aramex_track_no;?>:</th>
                                                <th align="right" style="
                                                    color: #333333;
                                                    font-size: 13px;
                                                    font-weight: 700;
                                                    letter-spacing: 1px;
                                                    padding: 10px;
                                                    width: 150px;
                                                "><?php echo $order_detail->tracking_id;?> </th>
                                            </tr>
                                        <?php }?>
                                               
										<!--<tr>
											<th align="right" colspan="3" style="
												color: #333333;
												font-size: 13px;
												font-weight: 700;
												letter-spacing: 1px;
												padding: 10px 100px 10px 10px;
												width: 150px;
											">Taxes:</th>
											<th align="right" style="
												color: #333333;
												font-size: 13px;
												font-weight: 700;
												letter-spacing: 1px;
												padding: 10px;
												width: 150px;
											">0.00 <span style="display: inline-block;"> SR</span></th>
										</tr>-->
										<tr>
											<th colspan="4" style="padding-top: 20px;">
												<table style="background-color: #eeeeee;border-radius: 6px;padding: 10px 20px;width: 100%;" border="0" cellpadding="0" cellspacing="0">
													<tfoot>
														<tr>
															<th colspan="2" style="color: #1D2630; font-size: 16px; font-weight: 300;" align="left">Grand Total</th>
															<th colspan="2" align="right">
																
                                                                <?php if(strtolower($order_detail->payment_method) == "visa"){
                                                                    $payment_img = base_url().'assets/frontend/images/visa_payment.png';
                                                                }
                                                                elseif(strtolower($order_detail->payment_method) == "cash on delivery"){
                                                                    $payment_img = base_url().'assets/frontend/images/cash_payment.png';
                                                                }elseif(strtolower($order_detail->payment_method) == "bank_transfer"){
                                                                    $payment_img = base_url().'assets/frontend/images/wire_payment.png';
                                                                }elseif(strtolower($order_detail->payment_method) == "sadad"){
                                                                    $payment_img = base_url().'assets/frontend/images/sadad_payment.png';
                                                                }else{
                                                                    $payment_img = base_url().'assets/admin/assets/img/master.png';
                                                                }

                                                                ?>

																<img width="42" height="17" src="<?php echo $payment_img; ?>" alt="<?php echo $order_detail->payment_method; ?>" />

																<span style="
																	color: #2c8c13;
																	display: inline-block;
																	font-size: 35px;
																	font-weight: 700;
																	min-width: 140px;
																	padding-left: 10px; "><?php echo number_format($order_detail->total_amount,'2','.',''); //number_format(intval(getCouponTotal($total))+intval($order_detail->shipment_price)+intval(getGiftCarTotalDb($ord_id)),'2','.',''); ?></span>
																<span style="
																	color: #0c0c0c;
																	display: inline-block;
																	font-size: 35px;
																	font-weight: 300;
																	min-width: 70px;
																	padding-left: 5px;
																">SAR</span>
															</th>
														</tr>
													</tfoot>
												</table>
											</th>
										</tr>
									</tfoot>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td style="width: 11px;"></td>
		</tr>		
		<tr>
			<td style="width: 11px;"></td>
			<td style="clear: both;display: block;margin: 0 auto;max-width: 637px">
				<img src="assets/frontend/images/print_page/bottomShadhow.png" alt="Shadhow" height="12" width="637" />
			</td>
			<td style="width: 11px;"></td>
		</tr>
	</tbody>
</table><!-- Main White Box END -->

<table class="footer-wrap" style="background-color: #343434;
								border: 0 none;
								margin: 0 auto;
								max-width: 100%;
								width: 100%" bgcolor="#343434" border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="width: 20px;"></td>
			<td class="header container" style="clear: both;display: block;margin: 0 auto;max-width: 620px">
				<table style=" width: 100%; " border="0" cellpadding="0" cellspacing="0">
					<tbody>
						<tr>
							<td style="padding: 10px 0; width: 50%;">
                            	<?php $social_media = social_links();
										$social = $social_media[0];
								?>
                                <?php if($social->instagram_link !== ""){ ?>
                                    <a href="<?php echo $social->instagram_link; ?>" style="margin-right: 25px; text-decoration: none;">
                                        <img src="assets/frontend/images/print_page/insta.png" alt="Insta" height="19" width="19" />
                                    </a>
                                <?php }
									if($social->twitter_link !== ""){
								?>
                                    <a href="<?php echo $social->twitter_link; ?>" style="margin-right: 25px; text-decoration: none;">
                                        <img src="assets/frontend/images/print_page/twitter.png" alt="Twitter" height="19" width="19" />
                                    </a>
                                <?php }
								   if($social->facebook_link !== ""){
								?>
                                    <a href="<?php echo $social->facebook_link; ?>">
                                        <img src="assets/frontend/images/print_page/facebook.png" alt="Facebook" height="19" width="19" />
                                    </a>
                                <?php }?>
							</td>
							<td align="right" style="
													 color: #c1b9a6;
													 font-size: 13px;
													 height: 62px;
													 padding:10px 0;
													 width: 50%;">
                                All rights reserved to IOUD company
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td style="width: 20px;"></td>
		</tr>
	</tbody>
</table><!-- Footer END -->


</body>
</html>