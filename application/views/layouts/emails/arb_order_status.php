<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<!-- If you delete this tag, the sky will fall on your head -->
	<meta name="viewport" content="width=device-width" />

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url();?>assets/frontend/images/favicon.png">
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url();?>assets/frontend/images/favicon.ico">
	<title><?php echo $site_title;?></title>
<base href="<?php echo base_url();?>" target="_blank">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700" rel="stylesheet">
	<style type="text/css">
		.divWBox {
			background-color: #fff;
			background-image: url("images/bgShadow.png");
			background-position: left top;
			background-repeat: repeat-y;
			background-size: 100% 1px;
			padding: 30px 20px 13px;
			width: 100%;
		}
		* {
			margin:0;
			padding:0;
		}
		* { font-family: "Roboto Condensed", sans-serif;} 

		img {
			max-width: 100%;
		}
		body {
			-webkit-font-smoothing:antialiased;
			-webkit-text-size-adjust:none;
			width: 100%!important;
			height: 100%;
		}
       /* .changeImgArb img {
            -webkit-transform: rotateY(180deg);
            -moz-transform: rotateY(180deg);
            -ms-transform: rotateY(180deg);
            -o-transform: rotateY(180deg);
            transform: rotateY(180deg);
        }*/


		/* -------------------------------------------
                PHONE
                For clients that support media queries.
                Nothing fancy.
        -------------------------------------------- */
		@media only screen and (max-width: 600px) {

			a[class="btn"] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}

			div[class="column"] { width: auto!important; float:none!important;}

			table.social div[class="column"] {
				width:auto!important;
			}

		}
	</style>
</head>

<body bgcolor="#FFFFFF" style="background-color: #fff; direction: rtl; font-family: Roboto Condensed, sans-serif;" dir="rtl">

<table class="head-wrap" style="background-color: #1c2931;
								border: 0 none;
								margin: 0 auto;
								max-width: 100%;
								width: 100%;" bgcolor="#1c2931" border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="width: 20px;"></td>
			<td class="header container" style="clear: both;display: block;margin: 0 auto;max-width: 620px">
				<table style=" width: 100%; " border="0" cellpadding="0" cellspacing="0">
					<tbody>
						<tr>
							<td colspan="2" style="padding: 8px 0;">
								<a href="javascript:void(0);">
									<img src="assets/frontend/images/print_page/logo.png" alt="logo" height="54" width="85" />
								</a>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td style="width: 20px;"></td>
		</tr>
	</tbody>
</table><!-- Header END -->

<table class="Textarea" style="background-color: #fff;
								border: 0 none;
								margin: 0 auto;
								max-width: 100%;
								width: 100%" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="width: 20px;"></td>
			<td class="header container" style="clear: both;display: block;margin: 0 auto;max-width: 620px">
				<table style=" width: 100%; "  border="0" cellpadding="0" cellspacing="0">
					<tbody>
						<tr>
							<td align="right" style="
													color: #666666;
													font-size: 15px;
													font-weight: 300;
													line-height: 1.2;
													min-height: 82px;
													padding: 24px 0;
													width: 50%;
													">
								مرحبا
								 <?php echo $user->full_name; echo $reg_user->first_name.' '.$reg_user->last_name;  ?>, <br />
								<?php echo $order_message;?>...
							</td>
							<td align="left" style="
													color: #2c8c13;
													font-size: 25px;
													font-weight: 300;
													min-height: 82px;
													padding: 24px 0;
													width: 50%;
													">
								<?php echo $order_status_label;?>!
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td style="width: 20px;"></td>
		</tr>
	</tbody>
</table><!-- Order Status END -->

<table class="whiteBox" style="background-color: #fff;
								border: 0 none;
								margin: 0 auto;
								max-width: 100%;
								padding-bottom:120px;
								width: 100%" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="width: 11px;"></td>
			<td style="clear: both;display: block;margin: 0 auto;max-width: 637px">
				<img src="assets/frontend/images/print_page/topShadhow.png" alt="Shadhow" height="6" width="637" />
			</td>
			<td style="width: 11px;"></td>
		</tr>
		<tr>
			<td style="width: 11px;"></td>
			<td class="header container" style="clear: both;display: block;margin: 0 auto;max-width: 637px">
				<table style="
						background-color: #fff;
						background-image: url(images/bgShadow.png);
						background-position: left top;
						background-repeat: repeat-y;
						background-size: 100% 1px;
						padding: 30px 20px 13px;
						width: 100%;
							  " class="divWBox" border="0" cellpadding="0" cellspacing="0" background="assets/frontend/images/print_page/bgShadow.png">
					<tbody>
						<tr>
							<td align="right" style="
													color: #666666;
													font-size: 15px;
													font-weight: 300;
													line-height: 1.2;
													min-height: 82px;
													width: 50%;
													">
								<span style="
									color: #2c8c13;
									display: block;
									font-size: 16px;
									letter-spacing: 0;
									padding-bottom: 20px;
									width: 100%;
								"><?php echo $my_order_order_no_label;?> <strong><?php echo $order_id;?></strong></span>
								
								<?php if($status == "shipped") { ?>
								<?php echo $my_order_we_will_deliver;?> <strong>2 <?php echo $my_order_bussiness_days;?></strong>
                                <?php }else{ ?>
                                <?php echo $order_status_label;
								} ?>
								<br /> 
								<?php echo $my_order_approximately_to;?>:
								<br />
                               <strong><?php echo $reg_user->first_name.' '.$reg_user->last_name; ?></strong>
                                <br /><br />
								<?php echo $user->address_title; ?>
								<br />
								<?php echo $user->address_1; ?>
								<br />
								<?php echo $user->address_2; ?>
								<br />
								<?php echo getCoutryByCode($user->country); ?>
								<br />
                                <?php echo $phone_label;?>: <span style=" direction: ltr; text-align: right; unicode-bidi:plaintext; "><?php echo $user->phone_no; ?></span>
                                <br><br>يرجى إعطاء إستطلاع الرأي بالضغط<a href="<?php echo lang_base_url().'profile/myOrder?open=completed'; ?>"> هنا</a>
                                <br />
							</td>
							<td align="left" style="
													color: #333333;
													font-size: 14px;
													font-weight: 400;
													vertical-align: top;
													width: 50%;
													">
								<table style="width: 100%; text-align: center; line-height: 1.2" border="0" cellpadding="0" cellspacing="0">
									<tbody>
										<tr>
											<td class="" colspan="4" style="padding-bottom: 11px; padding-left: 6px;">
                                            <?php
												$statusimg = ''; 
												if($order_status == 3)
												{
													$statusimg = '_arb_step_3';
												}
												
												if($order_status == 4) {
													$statusimg = '_arb_step_4';
												}
											?>
												<img src="assets/frontend/images/print_page/<?php echo $statusimg;?>.png" alt="Step" height="" width="" />
											</td>
										</tr>
										<tr>
                                            <td> الطلب <br /> تم استلام</td>
                                            <td>جارى <br />  إعداد الشحنة</td>
											<td><?php echo $my_order_shipped_label;?></td>
											<td><?php echo $my_order_delivered_label;?></td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<table style="width: 100%;" border="0" cellpadding="0" cellspacing="0">
									<thead>
										<tr>
											<th colspan="4" align="left" style="
												color: #9f9277;
												font-size: 16px;
												font-weight: 300;
												padding: 0 10px 15px;
											">(<?php echo count($products)?> <?php echo $products_page_similar_products_count;?>)</th>
										</tr>
									</thead>
									<tbody>
                                    <?php

										$total = 0;
					
										$totalQuan = 0;
					
										foreach ($products as $key => $product) {
					
											$images =  getProductImages($product['id']);
						
											$total += $product[$lang.'_price']*$carts[$key]['quantity'];
						
											$totalQuan += $carts[$key]['quantity'];
					
										?>
										<tr>
											<td style="border-top: 1px solid #d0d0d0; padding: 10px; width: 73px;">
												<img src="assets/frontend/images/print_page/prod_1.png" alt="prod" width="52" height="46" >
											</td>
											<td style="
												border-top: 1px solid #d0d0d0;
												color: #333333;
												font-size: 13px;
												font-weight: 700;
												letter-spacing: 0;
												padding: 10px;
												width:200px;
											"><?php echo $product[$lang.'_name']; ?></td>
											<td style="
												border-top: 1px solid #d0d0d0;
												color: #333333;
												font-size: 13px;
												font-weight: 700;
												letter-spacing: 0;
												padding: 10px;
												width:150px;
											"><?php echo $shopping_cart_qty;?>: <span style="display: inline-block"><?php echo $carts[$key]['quantity']; ?></span></td>
											<td style="
												border-top: 1px solid #d0d0d0;
												color: #333333;
												font-size: 13px;
												font-weight: 700;
												letter-spacing: 0;
												padding: 10px;
											"  align="right"><?php echo number_format($product[$lang.'_price'],'2','.',''); ?> <span style="display: inline-block"> <?php echo getCurrency($lang);?> </span></td>
										</tr>
                                       <?php } ?>
									</tbody>
									<tfoot>
										<!--<tr>
											<th align="left" colspan="3" style="
												border-top: 1px solid #d0d0d0;
												color: #333333;
												font-size: 13px;
												font-weight: 700;
												letter-spacing: 0;
												padding: 10px 10px 10px 100px;
												width: 150px;
											">Free Shipping:</th>
											<th align="right" style="
												border-top: 1px solid #d0d0d0;
												color: #333333;
												font-size: 13px;
												font-weight: 700;
												letter-spacing: 0;
												padding: 10px;
												width: 150px;
											">20.00 <span style="display: inline-block;"> SR</span></th>
										</tr>
										<tr>
											<th align="left" colspan="3" style="
												color: #333333;
												font-size: 13px;
												font-weight: 700;
												letter-spacing: 0;
												padding: 10px 10px 10px 100px;
												width: 150px;
											">Taxes:</th>
											<th align="right" style="
												color: #333333;
												font-size: 13px;
												font-weight: 700;
												letter-spacing: 0;
												padding: 10px;
												width: 150px;
											">0.00 <span style="display: inline-block;"> SR</span></th>
										</tr>-->
										<tr>
											<th colspan="4" style="padding-top: 20px;">
												<table style="background-color: #eeeeee;border-radius: 6px;padding: 10px 20px;width: 100%;" border="0" cellpadding="0" cellspacing="0">
													<tfoot>
														<tr>
															<th colspan="2" style="color: #1D2630; font-size: 16px; font-weight: 300;" align="left">المبلغ الإجمالي</th>
															<th colspan="2" align="left">

                                                                <?php if(strtolower($order_detail->payment_method) == "visa"){
                                                                    $payment_img = base_url().'assets/frontend/images/visa_payment.png';
                                                                }
                                                                elseif(strtolower($order_detail->payment_method) == "cash on delivery"){
                                                                    $payment_img = base_url().'assets/frontend/images/cash_payment.png';
                                                                }elseif(strtolower($order_detail->payment_method) == "bank_transfer"){
                                                                    $payment_img = base_url().'assets/frontend/images/wire_payment.png';
                                                                }elseif(strtolower($order_detail->payment_method) == "sadad"){
                                                                    $payment_img = base_url().'assets/frontend/images/sadad_payment.png';
                                                                }else{
                                                                    $payment_img = base_url().'assets/admin/assets/img/master.png';
                                                                }

                                                                ?>
                                                                
                                                                <img height="17" width="42" src="<?php echo $payment_img; ?>" alt="<?php echo $order_detail->payment_method; ?>" />

																<span style="
																	color: #2c8c13;
																	display: inline-block;
																	font-size: 35px;
																	font-weight: 700;
																	min-width: 140px;
																	padding-left: 10px; "><?php echo number_format(intval($total)+intval($order_detail->shipment_price),'2','.',''); ?></span>
																<span style="
																	color: #0c0c0c;
																	display: inline-block;
																	font-size: 35px;
																	font-weight: 300;
																	min-width: 70px;
																	padding-left: 5px;
																"><?php echo getCurrency($lang);?></span>
															</th>
														</tr>
													</tfoot>
												</table>
											</th>
										</tr>
									</tfoot>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td style="width: 11px;"></td>
		</tr>		
		<tr>
			<td style="width: 11px;"></td>
			<td style="clear: both;display: block;margin: 0 auto;max-width: 637px">
				<img src="assets/frontend/images/print_page/bottomShadhow.png" alt="Shadhow" height="12" width="637" />
			</td>
			<td style="width: 11px;"></td>
		</tr>
	</tbody>
</table><!-- Main White Box END -->

<table class="footer-wrap" style="background-color: #343434;
								border: 0 none;
								margin: 0 auto;
								max-width: 100%;
								width: 100%" bgcolor="#343434" border="0" cellpadding="0" cellspacing="0">
	<tbody>
		<tr>
			<td style="width: 20px;"></td>
			<td class="header container" style="clear: both;display: block;margin: 0 auto;max-width: 620px">
				<table style=" width: 100%; " border="0" cellpadding="0" cellspacing="0">
					<tbody>
                    
						<tr>
							<td style="padding: 10px 0; width: 50%;">
                            	<?php $social_media = social_links();
										$social = $social_media[0];
								?>
                                <?php if($social->instagram_link !== ""){ ?>
                                    <a href="<?php echo $social->instagram_link; ?>" style="margin-left: 25px; text-decoration: none;">
                                        <img src="assets/frontend/images/print_page/insta.png" alt="Insta" height="19" width="19" />
                                    </a>
                                <?php }
									if($social->twitter_link !== ""){
								?>
                                    <a href="<?php echo $social->twitter_link; ?>" style="margin-left: 25px; text-decoration: none;">
                                        <img src="assets/frontend/images/print_page/twitter.png" alt="Twitter" height="19" width="19" />
                                    </a>
                                <?php }
								   if($social->facebook_link !== ""){
								?>
                                    <a href="<?php echo $social->facebook_link; ?>" style="text-decoration: none;">
                                        <img src="assets/frontend/images/print_page/facebook.png" alt="Facebook" height="19" width="19" />
                                    </a>
                                <?php }?>
							</td>
							<td align="left" style="
													 color: #c1b9a6;
													 font-size: 13px;
													 height: 62px;
													 padding:10px 0;
													 width: 50%;">
                                جميع الحقوق محفوظة لشركة البخور الذكي
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td style="width: 20px;"></td>
		</tr>
	</tbody>
</table><!-- Footer END -->


</body>
</html>