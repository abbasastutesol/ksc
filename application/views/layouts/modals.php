<!-- Awards Image Modal end--><!-- Email Us Modal Start-->
<div id="Modal_1" class="modal fade raq-lightbox" role="dialog">
    <div class="modal-dialog">        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="modal-body"><h1>Email Us</h1>
                <div class="popup-form">
                    <form id="email_us_form" action="<?php echo lang_base_url().'ajax/email_to_admin'; ?>" class="material" method="post" onsubmit="return false;">
                        <div class="pf-row"><input type="text" id="email_to_name" name="name" placeholder="Name" required></div>
                        <div class="pf-row"><input type="email" id="email_to_email" name="email" placeholder="Email" required></div>
                        <div class="pf-row"><input type="tel" id="email_to_mobile" name="mobile"
                                                   placeholder="MOBILE <span>(For example +966 5X XXX XXXX)</span>">
                        </div>
                        <div class="pf-row"><input type="text" id="email_to_company" name="company" placeholder="Company"></div>
                        <div class="pf-row"><textarea id="email_to_message" name="message" placeholder="MESSAGE"></textarea></div>
                        <div class="pf-row">
                            <div class="pf-col-left g-recaptcha">

                            </div>
                            <div class="pf-col-right"><input type="submit" value="REQUEST"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div><!-- Email Us Modal Modal end--><!-- Products Request Modal Start-->
<div id="Modal_2" class="modal fade raq-lightbox" role="dialog">
    <div class="modal-dialog">        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="modal-body"><h1>REQUEST A QUOTE</h1>
                <div class="popup-form">
                    <form action="<?php echo lang_base_url().'product/request_qouta'; ?>" id="request_quota" class="material" method="post" onsubmit="return false;">
                        <div class="pf-row"><input type="text" id="quota_name" name="name" placeholder="Name" required></div>
                        <div class="pf-row"><input type="email" id="quota_email" name="email" placeholder="Email" required></div>
                        <div class="pf-row"><input type="tel" id="quota_mobile" name="mobile"
                                                   placeholder="MOBILE <span>(For example +966 5X XXX XXXX)</span>">
                        </div>
                        <div class="pf-row">
                            <input type="text" id="quota_company" name="company" placeholder="Company">
                        </div>
                        <div class="pf-row">
                            <select name="country" class="reg_country" placeholder="Country" id="reg_country">
                                <option value="">Country</option>
                                <?php echo getCountries_html(); ?>
                            </select>
                            <!--<input type="text" name="country" placeholder="Country">-->
                        </div>
                        <div class="pf-row">
                            <select class="reg_city" name="city" placeholder="City" id="reg_city">
                                <option value="">City</option>
                            </select>
                            <!-- <input type="text" name="city" placeholder="City">-->
                        </div>
                        <div class="pf-row">

                            <div class="pf-col-left g-recaptcha">

                            </div>
                            <div class="pf-col-right"><input type="submit" value="REQUEST"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div><!-- Products Request  Modal end--><!-- Awards Image Modal Start--><!-- Awards Image Modal end--><!-- Careers Modal Start-->

<div id="Modal_4" class="modal fade raq-lightbox careers-form" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="modal-body"><h1>APPLY</h1>
                <div class="popup-form">
                    <form id="apply-jobs" action="<?php echo lang_base_url().'career/action';?>" class="material" method="post" onsubmit="return false;">
                        <div class="pf-row"><input type="text" name="name" placeholder="Name" required></div>
                        <div class="pf-row"><input type="email" name="email" placeholder="Email" required></div>
                        <div class="pf-row"><input type="tel" name="mobile"
                                                   placeholder="MOBILE <span>(For example +966 5X XXX XXXX)</span>">
                        </div>
                        <!--Note for programmer: Designer didn't design checkbox and radio button so checkbox and radio buttons are just for design.                        <div class="pf-row">                            <div class="checkbox-wrap">                                <label>Please select option</label>                                <div class="checkbox">                                    <label><input type="checkbox" value="">Option 1</label>                                </div>                                <div class="checkbox">                                    <label><input type="checkbox" value="">Option 2</label>                                </div>                                <div class="checkbox">                                    <label><input type="checkbox" value="" disabled>Option 3</label>                                </div>                            </div>                        </div>                        <div class="pf-row">                            <label>Please select option</label>                            <div class="checkbox">                                <label><input type="radio" value="">Option 1</label>                            </div>                            <div class="checkbox">                                <label><input type="radio" value="">Option 2</label>                            </div>                            <div class="checkbox">                                <label><input type="radio" value="" disabled>Option 3</label>                            </div>                        </div>                        <!--Note End-->
                        <input type="hidden" name="form_type" value="save">
                        <input type="hidden" id="job_id" name="job_id" value="">
                        <div class="pf-row">
                            <select name="nationality" placeholder="Nationality">
                                <?php echo getCountries_html(); ?>
                            </select>
                        </div>
                        <div class="pf-row">
                            <div class="fileUploader">
                                <input id="txt" type="text" value="CV / RESUME"
                                       onclick="javascript:document.getElementById('file').click();">
                                <input id="txt-btn" type="button" value="BROWSE"
                                       onclick="javascript:document.getElementById('file').click();"> <input id="file"
                                                                                                             type="file"
                                                                                                             style='visibility: hidden;'
                                                                                                             name="cv"
                                                                                                             onchange="ChangeText(this, 'txt');"/>
                            </div>
                        </div>
                        <div class="pf-row">
                            <div class="pf-col-left"><img src="<?php echo base_url(); ?>assets/frontend/images/recaptcha-img.png" alt=""></div>
                            <div class="pf-col-right"><input type="submit" value="Apply"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div><!-- Careers Us Modal Modal end--><!-- Success Message for all forms Modal Start-->

<div id="Modal_5" class="modal fade raq-lightbox success-modal" role="dialog">
    <div class="modal-dialog">        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="modal-body"><h1>SUCCESS</h1>
                <div class="success-msg"><p id="success-msg">YOUR FORM HAS BEEN SENT SUCCESSFULLY</p></div>
            </div>
        </div>
    </div>
</div><!-- Success Message Us Modal Modal end--><!-- Newsletter Modal Start-->

<div id="modal_error" class="modal fade raq-lightbox error-modal" role="dialog">
    <div class="modal-dialog">        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="modal-body"><h1>ERROR</h1>
                <div class="error-msg"><p id="error-msg">Some errors</p></div>
            </div>
        </div>
    </div>
</div><!-- Success Message Us Modal Modal end--><!-- Newsletter Modal Start-->


<div id="Modal_6" class="modal fade raq-lightbox newsletter-modal" role="dialog">
    <div class="modal-dialog">        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="modal-body"><h1>NEWSLETTER</h1>
                <div class="success-msg-newsletter"></div>
                <div class="popup-form">
                    <form action="javascript:void(0);" class="material" id="newsletterfrm" onsubmit="return false;">
                        <div class="pf-row"><input id="newsletter_id" type="email" name="newsletter" placeholder="Email" required></div>
                        <div class="pf-row"><input id="submit_id" type="submit" value="SIGN UP"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div><!-- Newsletter  Modal end-->
