<!-- this is error messages-->
<?php if($this->session->flashdata('messages') != NULL) {?>
    <p class="alert-danger"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
        <?php $messages = $this->session->flashdata('messages');
        foreach ($messages as $message){  ?>
            <li><?php echo $message; ?></li>

<?php } ?>
    </p>
<?php }?>


<!--this is success message-->
<?php if($this->session->flashdata('error') != NULL) {?>
    <p class="alert-danger"><i class="fa fa-check-square-o" aria-hidden="true"></i>
        <?php echo $this->session->flashdata('error'); ?>
    </p>
<?php } ?>

<!--this is success message-->
<?php if($this->session->flashdata('success') != NULL) {?>
    <p class="alert-success"><i class="fa fa-check-square-o" aria-hidden="true"></i>
        <?php echo $this->session->flashdata('success'); ?>
    </p>
<?php } ?>
