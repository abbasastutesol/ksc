<div id="page_content">
	<div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
		<h1 id="product_edit_name">Edit User Rights</h1>
		<span class="uk-text-muted uk-text-upper uk-text-small" id="product_edit_sn"></span>
	</div>
	<div id="page_content_inner">
		<div class="md-card">
			<div class="md-card-content">
				<form action="<?php echo base_url(); ?>admin/user_rights/updateUsersRights" method="post"
					onsubmit="return false" class="ajax_form">
					<div class="uk-width-medium-1-2">
						<div class="uk-form-row"><label>Name</label> <input type="text" name="name" class="md-input" value="<?php echo $user_role_name->role; ?>"/></div>
						<input type="hidden" name="role_id" class="md-input" value="<?php echo $user_role_name->id; ?>"/>
					</div>
					<br/>
                    <br/>
					<div class="uk-grid">
						<div class="uk-width-1-1">
							<div class="uk-overflow-container">
								<table class="uk-table uk-table-align-vertical">
									<thead>
										<tr>
											<th>#</th>
											<th>Page Name</th>
											<th>Show</th>
											<th>View</th>
											<th>Add</th>
											<th>Edit</th>
											<th>Delete</th>
											<!--<th>Export Data</th>
											<th>Import Data</th>-->
										</tr>
									</thead>
									<tbody>
										<?php 
											$i = '1';
											foreach($all_pages as $page){ 
											$user_role_id = $this->uri->segment(4);
											$page_id = $page->id;
											$userRights_data = userRolesRights($page_id,$user_role_id);
											?>
										<tr>
											<th scope="row"><?php echo $i; ?><input type="hidden" id="" name="rights_ids[]" value="<?php echo $userRights_data->id; ?>"/></th>
											<th><?php echo $page->page_name; ?></th>
											<td><input type="checkbox" id="" name="show_p_<?php echo $userRights_data->id; ?>" value="1" <?php if($userRights_data->show_p == '1'){ echo "checked"; }?> /></td>
                                            <td> <input type="checkbox" id="" name="view_p_<?php echo $userRights_data->id; ?>" value="1" <?php if($userRights_data->view_p == '1'){ echo "checked"; }?> /></td>
											<td> <input type="checkbox" id="" name="add_p_<?php echo $userRights_data->id; ?>" value="1" <?php if($userRights_data->add_p == '1'){ echo "checked"; }?> /></td>
											<td> <input type="checkbox" id="" name="edit_p_<?php echo $userRights_data->id; ?>" value="1" <?php if($userRights_data->edit_p == '1'){ echo "checked"; }?>/></td>
											<td> <input type="checkbox" id="" name="delete_p_<?php echo $userRights_data->id; ?>" value="1" <?php if($userRights_data->delete_p == '1'){ echo "checked"; }?>/></td>
											<!--<td> <input type="checkbox" id="" name="export_p_<?php /*echo $userRights_data->id; */?>" value="1" <?php /*if($userRights_data->export_p == '1'){ echo "checked"; }*/?>/></td>
											<td> <input type="checkbox" id="" name="import_p_<?php /*echo $userRights_data->id; */?>" value="1" <?php /*if($userRights_data->import_p == '1'){ echo "checked"; }*/?>/></td>-->
										</tr>
										<?php $i++; }?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<br/>               
					<div class="uk-width-large-1-1"><span class="uk-input-group-addon"><a
						class="md-btn submit_ajax_form" href="javascript:void(0);">Update</a></span></div>
			</div>
			</form>
		</div>
	</div>
</div>
</div>