<?php

$user_session_data = $this->session->userdata('user');
$user_id = $user_session_data['id'];
$user_id;


?>
<!-- Content Wrapper. Contains page content -->
<div id="page_content">
    <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
        <h1 id="product_edit_name">User Rights</h1>
        <span class="uk-text-muted uk-text-upper uk-text-small" id="product_edit_sn"></span>
    </div>

    <div id="page_content_inner">

        <?php if(viewEditDeleteRights('User Rights','add')) { ?>
        <a href="<?php echo base_url();?>admin/user_rights/add" >
            <input type="button" name="Add" class="md-btn submit_ajax_form" value="Add">
        </a>
        <?php } ?>

        <div class="md-card">
            <div class="md-card-content">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-1-1">
                        <div class="uk-overflow-container">
                            <table class="uk-table uk-table-align-vertical listing dt_default">
                                <thead>
                                <tr>
                                    <th>Sr#</th>
                                    <th class="nosort">Role</th>
                                    <th class="nosort">Created at</th>
                                    <?php if(viewEditDeleteRights('User Rights','edit') || viewEditDeleteRights('User Rights','delete')) { ?>
									<th class="nosort">Action</th>
                                    <?php } ?>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $i = '1';
                        foreach($UserRoles as $row){



                        echo '<tr class="'.$row->id.'"><td class="uk-text-center">'.$i++.'</td>		
                        <td>'.$row->role.'</td>				
                        <td>'.$row->created_at.'</td>';

                        if(viewEditDeleteRights('User Rights','edit') || viewEditDeleteRights('User Rights','delete')) {

                            echo '<td>';
                       if(viewEditDeleteRights('User Rights','edit')) {
                           echo '<a href="' . base_url() . 'admin/user_rights/edit/' . $row->id . '" title="Edit Right">				
                        <i class="md-icon material-icons">&#xE254;</i>								
                        </a>';
                       }
                            if(viewEditDeleteRights('User Rights','delete')) {
                                echo '<a href="javascript:void(0);" class="uk-margin-left delete" onclick="deleteRecord(' . $row->id . ',\'admin/user_rights/action\',\'\');" title="Delete Right"> <i class="material-icons md-24 delete">&#xE872;</i>
                        </a>';
                            }
                        
                        
                       echo '</td>';
                        }

                       echo '</tr>';
                        }
                        ?>
 </tbody>
 </table>  
 
 </div>                                                   
 </div>                   
 </div>
 
 </div>          
 </div>  
 </div>  
 </div>       <!-- /.content-wrapper -->