<div id="page_content">
  <div id="page_content_inner">
  <form action="<?php echo base_url(); ?>admin/configuration/action" method="post" onsubmit="return false" class="ajax_form" enctype="multipart/form-data">
        <div class="md-card">
          <div class="md-card-content">
            <h3 class="heading_a">Edit Configuration</h3><br>
            
            <div class="uk-grid" data-uk-grid-margin>
            
            <input type="hidden" name="form_type" value="update">
            <input type="hidden" name="id" value="<?php echo $configuration->id;?>">
            
              <div class="uk-width-medium-1-2">
                <div class="uk-form-row">
                  <label>Phone</label>
                  <input type="text" class="md-input" value="<?php echo $configuration->phone;?>" name="phone" />
                </div>
                
              </div>
              <div class="uk-width-medium-1-2">
                <div class="uk-form-row">
                  <label>Services Email</label>
                  <input type="text" class="md-input" value="<?php echo $configuration->email;?>" name="email" />
                </div>
                
              </div>
              <div class="uk-width-medium-1-2">
                <div class="uk-form-row">
                  <label>Email Alerts Email</label>
                  <input type="text" class="md-input" value="<?php echo $configuration->admin_email;?>" name="admin_email" />
                </div>
                
              </div>
              
              <div class="uk-width-medium-1-2">
                <div class="uk-form-row">
                  <label>HR Email</label>
                  <input type="text" class="md-input" value="<?php echo $configuration->hr_email;?>" name="hr_email" />
                </div>
                
              </div>
              <div class="uk-width-medium-1-1">
                <div class="uk-form-row">
                  <label>Facebook Link</label>
                  <input type="text" class="md-input" value="<?php echo $configuration->facebook_link;?>" name="facebook_link" />
                </div>
                
              </div>
              <div class="uk-width-medium-1-1">
                <div class="uk-form-row">
                  <label>Twitter Link</label>
                  <input type="text" class="md-input" value="<?php echo $configuration->twitter_link;?>" name="twitter_link" />
                </div>
                
              </div>
              <div class="uk-width-medium-1-1">
                <div class="uk-form-row">
                  <label>Instagram Link</label>
                  <input type="text" class="md-input" value="<?php echo $configuration->instagram_link;?>" name="instagram_link" />
                </div>
                
              </div>
              <div class="uk-width-medium-1-1">
                <div class="uk-form-row">
                  <label>Snapchat Link</label>
                  <input type="text" class="md-input" value="<?php echo $configuration->snapchat_link;?>" name="snapchat_link" />
                </div>
                
              </div>
              <div class="uk-width-medium-1-1">
                <div class="uk-form-row">
                  <label>Youtube Link</label>
                  <input type="text" class="md-input" value="<?php echo $configuration->youtube_link;?>" name="youtube_link" />
                </div>
                
              </div>	
              <div class="uk-width-medium-1-2">
                <label>Search English Description</label>
                <div class="uk-form-row">
                  
                  <textarea class="md-input" name="eng_search_description" id="eng_description" cols="30" rows="4"><?php echo $configuration->eng_search_description;?></textarea>
                </div>
                
              </div>
              <div class="uk-width-medium-1-2">
                <label>Search Arabic Description</label>
                <div class="uk-form-row">
                  
                  <textarea class="md-input" name="arb_search_description" id="arb_description" cols="30" rows="4"><?php echo $configuration->arb_search_description;?></textarea>
                </div>
                
              </div>
                
        
            </div>
           
          </div>
        </div>
   
   
       <div class="md-card">
          <div class="md-card-content">
            <h3 class="heading_a">Edit Email Message</h3><br>
            <div class="uk-grid" data-uk-grid-margin>
              <div class="uk-width-medium-1-2">
                <div class="uk-form-row">
                  <label>English Email Title</label>
                  <input type="text" class="md-input" value="<?php echo $configuration->eng_email_title;?>" name="eng_email_title" />
                </div>
                
              </div>
              <div class="uk-width-medium-1-2">
                <div class="uk-form-row">
                  <label>Arabic Email Title</label>
                  <input type="text" class="md-input" value="<?php echo $configuration->arb_email_title;?>" name="arb_email_title" />
                </div>
                
              </div>
              
              <div class="uk-width-medium-1-2">
                <label>English Email Description</label>
                <div class="uk-form-row">
                  
                  <textarea class="md-input" name="eng_email_description" id="eng_brief_description" cols="30" rows="4"><?php echo $configuration->eng_email_description;?></textarea>
                </div>
                
              </div>
              <div class="uk-width-medium-1-2">
                <label>Arabic Email Description</label>
                <div class="uk-form-row">
                  
                  <textarea class="md-input" name="arb_email_description" id="arb_brief_description" cols="30" rows="4"><?php echo $configuration->arb_email_description;?></textarea>
                </div>
                
              </div>
              <div class="uk-width-medium-1-2">
                <div class="uk-form-row">
                  <label>English Email Success Message</label>
                  <input type="text" class="md-input" value="<?php echo $configuration->eng_email_success_message;?>" name="eng_email_success_message" />
                </div>
                
              </div>
              
              <div class="uk-width-medium-1-2">
                <div class="uk-form-row">
                  <label>Arabic Email Success Message</label>
                  <input type="text" class="md-input" value="<?php echo $configuration->arb_email_success_message;?>" name="arb_email_success_message" />
                </div>
                
              </div>	
                
        
            </div>
          </div>
        </div>
        <div class="md-card">
          <div class="md-card-content">
            <h3 class="heading_a">Edit Size Chart</h3><br>
            <div class="uk-grid" data-uk-grid-margin>
              <div class="uk-width-1-2">
                <img src="<?php echo base_url().'uploads/images/home/'.$configuration->eng_size_chart;?>" alt="" class="img_small" data-uk-modal="{target:'#modal_lightbox_<?php echo $image->id;?>'}"/>
                </div>
                <div class="uk-width-1-2">
                <img src="<?php echo base_url().'uploads/images/home/'.$configuration->arb_size_chart;?>" alt="" class="img_small" data-uk-modal="{target:'#modal_lightbox_<?php echo $image->id;?>'}"/>
                </div>
                <div class="uk-width-1-2">
                  <div id="file_upload-drop" class="uk-file-upload">
                      <a class="uk-form-file md-btn" id="showCheck">choose file<input id="file_upload-select" class="images_upload" type="file" name="eng_size_chart" multiple></a><br /><br />
                      <span style="color:red">(937 * 441 pixels)</span>
                  </div>
                  <div id="file_upload-progressbar" class="uk-progress uk-hidden">
                      <div class="uk-progress-bar" style="width:0">0%</div>
                  </div>
              </div>
              
              <div class="uk-width-1-2">
                  <div id="file_upload-drop" class="uk-file-upload">
                      <a class="uk-form-file md-btn" id="showCheck">choose file<input id="file_upload-select" class="images_upload" type="file" name="arb_size_chart" multiple></a><br /><br />
                      <span style="color:red">(937 * 441 pixels)</span>
                  </div>
                  <div id="file_upload-progressbar" class="uk-progress uk-hidden">
                      <div class="uk-progress-bar" style="width:0">0%</div>
                  </div>
              </div>
        
            </div>
          </div>
        </div>
    </form> 
   <!-- light box for image -->
		   <div class="uk-modal" id="modal_lightbox">
                                <div class="uk-modal-dialog uk-modal-dialog-lightbox">
                                    <button type="button" class="uk-modal-close uk-close uk-close-alt"></button>
                                    <img src="<?php echo base_url().'uploads/images/categories/'.$variant->eng_image; ?>" alt=""/>
                                    
                                </div>
           </div>
			<!-- end light box for image -->   
    
    
    
  </div>
</div>
 <div class="md-fab-wrapper">
        <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">
            <i class="material-icons">&#xE161;</i>
        </a>
    </div>