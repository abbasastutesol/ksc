<div id="page_content">
  <div id="page_content_inner">
    <div class="md-card">
      <div class="md-card-content">
        <h3 class="heading_a">Add a Discount</h3>
        <form action="<?php echo base_url(); ?>admin/discount/action" method="post" onsubmit="return false" class="ajax_form">
		<div class="uk-grid" data-uk-grid-margin>
		
		<input type="hidden" name="form_type" value="update">
        <input type="hidden" name="id" value="<?php echo $discount->id;?>">
        <input type="hidden" class="md-input" value="<?php echo $discount->coupon_code;?>" name="old_coupon_code" />
			 
          <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
              <label>Coupon Code</label>
              <input type="text" class="md-input" value="<?php echo $discount->coupon_code;?>" name="coupon_code" />
            </div>
			
          </div>
          <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
              <label>Expiry Date</label>
              <input type="text" class="md-input" value="<?php echo $discount->expiry_date;?>" name="expiry_date" data-uk-datepicker="{format:'DD/MM/YYYY'}" />
            </div>
            
          </div>     
            <div class="uk-width-medium-1-2">
                <div class="uk-form-row">
                  <label>Amount / Percentage</label>
                  <input type="text" class="md-input" value="<?php echo $discount->amount;?>" name="amount" />
                </div>
                
              </div>
              
              <div class="uk-width-medium-1-4">
                <input type="checkbox" data-switchery data-switchery-size="large" <?php echo ($discount->active == 1 ? 'checked' : '');?> id="switch_demo_large" name="active" />
                <label for="switch_demo_large" class="inline-label">Active</label>
               
                </div>
                <div class="uk-width-medium-3-5">
                      <span class="icheck-inline">
                          <input type="radio" name="type" id="radio_demo_inline_1" data-md-icheck value="1" <?php echo ($discount->type == 1 ? 'checked' : '');?>/>
                          <label for="radio_demo_inline_1" class="inline-label">$</label>
                      </span>
                      <span class="icheck-inline">
                          <input type="radio" name="type" id="radio_demo_inline_2" data-md-icheck value="2" <?php echo ($discount->type == 2 ? 'checked' : '');?> />
                          <label for="radio_demo_inline_2" class="inline-label">%</label>
                      </span>
                  </div>
			
			
	
        </div>
       </form> 
      </div>
    </div>
   
   <!-- light box for image -->
		   <div class="uk-modal" id="modal_lightbox">
                                <div class="uk-modal-dialog uk-modal-dialog-lightbox">
                                    <button type="button" class="uk-modal-close uk-close uk-close-alt"></button>
                                    <img src="<?php echo base_url().'uploads/images/categories/'.$category->eng_image; ?>" alt=""/>
                                    
                                </div>
           </div>
			<!-- end light box for image -->   
    
    
    
  </div>
</div>
<div class="md-fab-wrapper">
        <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">
            <i class="material-icons">&#xE161;</i>
        </a>
    </div>
<div class="md-fab-wrapper" style="right:95px;">
        <a class="md-fab md-fab-primary" href="<?php echo base_url();?>admin/category" id="">
            <i class="material-icons">keyboard_backspace</i>
        </a>
    </div>