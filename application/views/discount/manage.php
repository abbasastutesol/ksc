 <!-- Content Wrapper. Contains page content -->
      <div id="page_content">
        <div id="page_content_inner">
		<a href="<?php echo base_url();?>admin/discount/add" class="md-btn" > Add</a>
			<div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <div class="uk-overflow-container"> 
                                <table class="uk-table uk-table-align-vertical listing dt_default">
                                    <thead>
                                        <tr>
                                             <th>Sr#</th>

                                              <th class="nosort">Coupon Code</th>
                                              
                                              <th class="nosort">Expiry Date</th>
                      
                                              <th class="nosort"> Amount</th>
                      
                                             
                      
                                              <th>Active</th>
                                              
                                              <th class="nosort">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php
											 $index = 1;
											 foreach($discounts as $discount){?>
												<tr id="<?php echo $discount->id;?>">
													 <td><?php echo $index;?></td>
							  
													  <td><?php echo $discount->coupon_code;?></td>
													  
													  <td><?php echo $discount->expiry_date;?></td>
                                                      
                                                      <td><?php echo $discount->amount;?></td>
													 
													  <td><?php echo ($discount->active == '1' ? '<i class="material-icons md-color-light-blue-600 md-24">&#xE86C;</i>': '');?></td>
                                                      
                                                      <td><a href="<?php echo base_url().'admin/discount/edit/'.$discount->id;?>" title="Edit Discount">
													<i class="md-icon material-icons">&#xE254;</i>
													</a>
													<a href="javascript:void(0);" class="uk-margin-left delete" onclick="deleteRecord('<?php echo $discount->id;?>','admin/discount/action','');" title="Delete Discount"> <i class="material-icons md-24 delete">&#xE872;</i></a></td>
												</tr>
											 <?php 
											 $index++;
											 }?> 
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
       <!-- /.content-wrapper --> 