<!-- Content Wrapper. Contains page content -->
<div id="page_content">
	<div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
		<h1 id="product_edit_name">All Users</h1>
		<span class="uk-text-muted uk-text-upper uk-text-small" id="product_edit_sn"></span>
	</div>
	<div id="page_content_inner">
		<?php if(viewEditDeleteRights('Add User','add')) { ?>
        <a href="<?php echo base_url();?>admin/add_user/add" >
		<input type="button" name="Add" class="md-btn submit_ajax_form" value="Add"></a>
        <?php } ?>

		<div class="md-card">
			<div class="md-card-content">
				<div class="uk-grid" data-uk-grid-margin>
					<div class="uk-width-1-1">
						<div class="uk-overflow-container">
							<table class="uk-table uk-table-align-vertical listing dt_default">
								<thead>
									<tr>
										<th>Sr#</th>
										<th class="nosort">Name</th>
										<th class="nosort">Email</th>
										<th class="nosort">Role</th>
                                        <?php if(viewEditDeleteRights('Add User','edit') || viewEditDeleteRights('Add User','delete')) { ?>
										<th class="nosort">Action</th>
                                        <?php } ?>
									</tr>
								</thead>
								<tbody>
									<?php
										$i = '1';
										foreach($UserRoles as $row){
										
										    echo '<tr class="'.$row->id.'"><td class="uk-text-center">'.$i++.'</td>		
										    	<td>'.$row->name.'</td>				
										    	<td>'.$row->email.'</td>				
										    	<td>'.getUserRoleName($row->user_type).'</td>';
                                    if(viewEditDeleteRights('Add User','edit') || viewEditDeleteRights('Add User','delete')) {
                                        echo '<td>' . ($row->is_admin == 0 ? '<a href="' . base_url() . 'admin/add_user/edit/' . $row->id . '" title="Edit User"><i class="md-icon material-icons">&#xE254;</i></a><a href="javascript:void(0);" class="uk-margin-left delete" onclick="deleteRecord(' . $row->id . ',\'admin/add_user/action\',\'\');" title="Delete User"> <i class="material-icons md-24 delete">&#xE872;</i></a>' : '') . '
												</td>';
                                    }

											echo '</tr>';
										}									
										?>									                                      
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.content-wrapper -->