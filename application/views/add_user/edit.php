<div id="page_content">
	<div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
		<h1 id="product_edit_name">Edit User</h1>
		<span class="uk-text-muted uk-text-upper uk-text-small" id="product_edit_sn"></span>
	</div>
	<div id="page_content_inner">
		<form action="<?php echo base_url(); ?>admin/add_user/action" method="post" onsubmit="return false"
			class="uk-form-stacked ajax_form" id="product_edit_form" enctype="multipart/form-data">
			<input
				type="hidden" name="form_type" value="update">
			<input type="hidden" name="id" value="<?php echo $user->id;?>">
			<div class="uk-grid uk-grid-medium" data-uk-grid-margin>
				<div class="uk-width-xLarge-10-10  uk-width-large-10-10">
					<div class="md-card">
						<div class="md-card-toolbar">
							<h3 class="md-card-toolbar-heading-text"> Details </h3>
						</div>
						<div class="md-card-content large-padding">
							<div class="uk-grid uk-grid-divider uk-grid-medium" data-uk-grid-margin>
								<div class="uk-width-large-1-2">
									<div class="uk-form-row"><label for="name">Name</label> <input
										type="text" class="md-input" id="name" name="name" value="<?php echo $user->name; ?>"/></div>
									<div class="uk-form-row"><label for="eng_title">Password</label> <input
										type="password" class="md-input" id="password" name="password" value=""/></div>
								</div>
								<div class="uk-width-large-1-2">
									<div class="uk-form-row"><label for="eng_title">Email</label> <input
										type="email" class="md-input" id="email" name="email" value="<?php echo $user->email; ?>"/></div>
									<div class="uk-form-row">
										<select id="select_demo_5" data-md-selectize
											data-md-selectize-bottom
											data-uk-tooltip="{pos:'top'}"
											title="Select with tooltip" name="user_type">
											<option value="">Select User Role</option>
											<?php											  								if($all_roles)	
												{
													foreach($all_roles as $all_role)
													{	
														if($all_role->id == $user->user_type){
															$selected = 'selected';
														}
														else{
															$selected = '';
														}
														echo '<option '.$selected.' value="'.$all_role->id.'">'.$all_role->role.'</option>'; 
													}
												} 
												
												?> 
										</select>
									</div>


								</div>
								<?php if($user->is_admin == 0) { ?>
									<div class="uk-width-large-1-1">
										<span class="uk-input-group-addon">
											<a class="md-btn submit_ajax_form" href="javascript:void(0);">Save</a>
										</span>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<div class="md-fab-wrapper"><a class="md-fab md-fab-primary submit_ajax_form" href="#" id=""> <i class="material-icons">
	&#xE161;</i> </a>
</div>