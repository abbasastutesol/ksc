<section>

	<div class="checkOutSetps">
		<div class="container max960_cont"> 
			<!-- Nav tabs -->
			<ul role="tablist">
				<li role="presentation" class="steps"><a href="javascript:void(0);" onclick="redirectPage('<?php echo lang_base_url();?>shopping_cart/');" aria-controls="step_1" role="tab" data-toggle="tab" style="text-decoration:none;">01</a></li>
                <?php if(!empty($products))
				{	?>
				<li role="presentation" class="steps"><a href="javascript:void(0);" onclick="redirectPage('<?php echo lang_base_url();?>shopping_cart/?step_2');" aria-controls="step_2" role="tab" data-toggle="tab" style="text-decoration:none;">02</a></li> 
                <li role="presentation" class="steps active"><a href="#step_3" data-toggle="tab" role="tab" aria-controls="messages" style="text-decoration:none; cursor:default;">03</a></li>
				<!--<li role="presentation"><a href="#step_3" data-toggle="modal" class="term_check" data-target=".bs-example-modal-sm">03</a></li>-->
                <?php } ?>
			</ul>
		</div>
	</div>
	<!-- Tab panes -->
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane tableArea" id="step_1">
			<div class="container max960_cont">
				<div class="step_one_table">
					<table class="table">
						<thead>
							<tr>
								<th colspan="2"><a href="javascript:void(0);" style="text-decoration:none; cursor:default;"><h1> <?php echo $shopping_cart_shopping_cart;?></h1></a></th>	
								<th><a href="javascript:void(0);" style="text-decoration:none; cursor:default;"><?php echo $shopping_cart_price;?></a></th>
								<th><a href="javascript:void(0);" style="text-decoration:none; cursor:default;"><?php echo $shopping_cart_quantity;?></a></th>
							</tr>
							<tr>
								<th colspan="4">
                                <?php if(!empty($products))
							 			{	?>
									<div class="cartStep1Btn">
										<a href="#step_2" aria-controls="step_2" onclick="calculateTotal();" role="tab" data-toggle="tab" class="edBtn"><?php echo $checkout_continue_shopping;?></a>
									</div>
                                 <?php } ?>
								</th>
							</tr>
						</thead>
						<tbody>
                        	<?php
							 if(!empty($products))
							 {
								 $total_amount = '0';
								 $i=1;
								 $product_price='';
								 foreach($products as $product){
									 $product_price = $product->prod_price;
									 /*if($product->has_discount_price == 1)
									 {
										 $product_price = $product->product_discount_price;
									 }
									 else
									 {
										 $product_price = $product->product_price;
									 }*/
									 ?>
									<tr id="prod_tr_<?php echo $i; ?>">
                                     	<?php $prod_image = getProductImages($product->p_id);
											if(!empty($prod_image))
											{
												$image_name = $prod_image[0][$lang.'_image'];
											}
											else
											{
												$image_name = 'no_imge.png';
											}
										?>
										<td class="imgBox"><img src="<?php echo base_url();?>uploads/images/products/<?php echo $image_name;?>" alt="img" /></td>
										<td>
											<h3><?php echo $product->category_name;?></h3>
											<?php echo $product->product_name;?>
											<?php if($product->prod_quantity == 0){?>
											<h4><?php echo $shopping_cart_last_piece;?> </h4>
											<?php }?>
										</td>
										<td><strong id="prod_price_<?php echo $i; ?>"><?php echo $product_price;?></strong> <?php echo getPriceCur($product_price, true);?></td>
										<td>
											<select id="prod_drp_<?php echo $i; ?>" onchange="calculateOrder(this.value, '<?php echo $i;?>', '<?php echo $product_price;?>', '<?php echo $product->temp_order_id;?>');">
                                            <option value="" selected="selected" disabled="disabled"><?php echo $product->quantity;?></option>
                                            <optgroup label="<?php echo $home_display_select;?>">
											<?php for($q=1; $q<=$product->prod_quantity; $q++){?>
												<option value="<?php echo $q;?>"><?php echo $q;?></option>
											<?php } ?>
                                            </optgroup>
											</select>
											<a href="javascript:void(0);" onclick="delFromCart('<?php echo $product->temp_order_id; ?>', '<?php echo $i;?>');" ><img src="<?php echo base_url();?>assets/frontend/images/trashCan.png" alt="Del" height="15" width="12" /></a>
										</td>
									</tr>
								<?php 
									$total_amount += $product_price;
									$i++;
								} 
							 } else
							 {
								 echo '<tr><td colspan="4">'.$no_records.'</td></tr>';
							 }?>
						</tbody>
						<tfoot>
							<tr>
								<th colspan="4">
									<h2><?php echo $shopping_cart_total;?></h2>
									<strong id="prod_price_total"><?php echo $total_amount; ?> </strong>
									 <?php echo getPriceCur($total_amount, true);?>
								</th>
							</tr>
						</tfoot>
					</table> 
				</div>
			</div>
			<div class="line"></div>
			<div class="container">
		<div class="productSecFive playSlide green">
			<h2><?php echo $products_recently_viewed_products;?></h2>
			<div class="row">
				<ul>
					<?php 
					$recently_viewed = getRecentViewed();
					foreach($recently_viewed as $product){
						  $image_arr = getProductImages($product['id']);
								?>
					  <li>
						  <div class="imgBox">
							  <?php if(sizeof($image_arr) > 1){?>						
								  <div id="Product_id_<?php echo $main;?>" class="carousel slide" data-ride="carousel"> 															
									  <!-- Wrapper for slides -->
									  <div class="carousel-inner" role="listbox">
										  <?php 
										  $im=0;
										  foreach($image_arr as $image){?>
											  <div class="item<?php if($im==0){ echo ' active';}?>"> <a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"><img src="<?php echo base_url();?>uploads/images/products/<?php echo $image[$lang.'_image']?>" alt="Product" height="199" width="129" ></a>
											  </div>
										  <?php
										  $im++; 
										  }?>                                                   
									  </div>
								  </div>
							  <?php }else{ ?>
								  <?php if($image_arr){
									  foreach($image_arr as $image){?>
									  
									 <a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"> <img src="<?php echo base_url();?>uploads/images/products/<?php echo $image[$lang.'_image']?>" alt="Product" height="199" width="129" /></a>
								  <?php 
									  }
								  } else
								  { ?>
									 <a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"> <img src="<?php echo base_url();?>uploads/images/products/no_imge.png" alt="Product" height="199" width="129" /></a>
								  <?php }?>
							  <?php } ?>
						  </div>
						  <!-- Indicators -->
						  <?php if(sizeof($image_arr) > 1){?>
							  <ol class="carousel-indicators">
							  <?php 
							  $imo=0;
							  foreach($image_arr as $image){?>
								  <li data-target="#Product_id_<?php echo $main;?>" data-slide-to="<?php echo $imo;?>"<?php if($imo==0){ echo ' class="active"';}?>></li>
							  <?php 
							  $imo++; 
							  }
							  ?>
						  </ol>	
						  <?php }?>	
						  <h3><a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"><?php echo $product[$lang.'_name']?></a></h3>
						  <h4><?php echo getPriceCur($product[$lang.'_price'])?></h4>
					  </li>
				  <?php 
				  $main++;
				  }?>
				</ul>
			</div>
		</div>
	</div>
		</div><!-- step one END -->
		<div role="tabpanel" class="tab-pane" id="step_2">
			<div class="shopCartHeading">
				<div class="container max780_cont">
					<ul>
						<li class="active"><a href="javascript:void(0);" style="text-decoration:none; cursor:default;"><?php echo $checkout_personal_info;?></a></li>
						<li><i class="fa fa-angle-left" aria-hidden="true"></i></li>
						<li><a href="javascript:void(0);" style="text-decoration:none; cursor:default;"><?php echo $checkout_choose_mailing_address;?></a></li>
					</ul>
				</div>
				<div class="line"></div>
			</div>
			<div class="carStep1List">
				<div class="container max780_cont">
					<div class="overAutoXS">
						<table class="table table-hover">
							<tbody>
                            	<?php 
								$j=1;
								$product_price='';
								$total_prod_amount = '';
								 foreach($products as $product){
									$product_price = $product->prod_price;
									/* if($product->has_discount_price == 1)
									 {
										 $product_price = $product->product_discount_price;
									 }
									 else
									 {
										 $product_price = $product->product_price;
									 }*/
								 ?>
									<tr id="prod_2_tr_<?php echo $j; ?>">
                                        <td>
                                            <a href="javascript:void(0);" onclick="delFromCart('<?php echo $product->temp_order_id; ?>', '<?php echo $j;?>');"><img src="<?php echo base_url();?>assets/frontend/images/trashCan.png" alt="Delete" height="15" width="12" /></a>
                                            <label><?php echo $product->category_name; ?></label>
                                            <?php echo $product->product_name; ?>
                                            <span class="border">&nbsp;</span>
                                        </td>
                                        <td class="amount"><span class="red prod-prices" id="prod_pr_<?php echo $j; ?>"><?php echo ($product_price * $product->quantity); ?></span> <span style="font-size: 16px;display: inline-block;margin: 1px 0 0 0;"><?php echo getPriceCur($product_price, true);?></span></td>
                                    </tr>
                                <?php 
									$total_prod_amount += ($product_price * $product->quantity);
									$j++;
								} ?>
							</tbody>
							<tfoot>
								<tr>
									<th colspan="2">
										<span><?php echo $shopping_cart_total;?></span> <span class="border"></span> <span class="red" id="total-price"><?php echo $total_prod_amount; ?></span> <span style="font-size: 16px;display: inline-block;margin: 1px 0 0 0;"><?php echo getPriceCur($total_prod_amount, true);?></span>
									</th>
								</tr>
							</tfoot>
						</table>
					</div>
                    
                    <!-- Uncomment when apply payment method-->
					<!--<div class="row cardPaySec">
						<div class="col-md-12">
							<label>اختارى طريقة الدفع</label>
							<img src="<?php echo base_url();?>assets/frontend/images/visaMasterImg.png" alt="Master Card" height="57" width="125" />
							<img src="<?php echo base_url();?>assets/frontend/images/sadasImage.png" alt="Aad" height="57" width="97" />
							<img src="<?php echo base_url();?>assets/frontend/images/cashDlivImg.png" alt="cash Dilver" height="57" width="76" />
							<p>&nbsp;</p>
						</div>
					</div>
					
					<div class="row cardPaySec">
						<div class="col-md-12">
							<label>رقم البطاقة</label>
							<input type="text" />
						</div>
						<div class="col-md-12">
							<label>تاريخ الإنتهاء</label>
							<select>
								<option>السنة</option>
							</select>
							<select>
								<option>الشهر</option>
							</select>
						</div>
						<div class="col-md-12">
							<label><span>CVC</span></label>
							<input type="text" />
						</div>
					</div>-->
				</div>
			</div>
			<div class="line"></div>
			<div class="defaultPageSty">
				<div class="container max780_cont">
					<!--<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12 bigCheckBox">
							<input type="checkbox" name="checkbox" id="checkbox_1"><label for="checkbox_1"><span></span>لقد قرأت واطلعت وأوافق على الشروط والأحكام </label>
						</div>
					</div>-->
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12 text-center">
                        	<a href="#step_3" data-toggle="tab" role="tab" aria-controls="messages" id="Shipping_address_1"><input type="button" value="<?php echo $checkout_continue_shopping;?>" class="edBtn red"></a>
							<!--<a href="#step_3" data-toggle="modal" id="Shipping_address_1" class="term_check" data-target=".bs-example-modal-sm"><input type="button" value="اكمال عملية الشراء" class="edBtn red"></a>-->
						</div>
					</div>
				</div>
			</div>
		</div><!-- Step two end -->
        
		<div role="tabpanel" class="tab-pane active" id="step_3">
			<div class="shopCartHeading">
				<div class="container max960_cont">
					<ul>
                    	<li><a href="javascript:void(0);" style="text-decoration:none; cursor:default;"><?php echo $checkout_shopping_basket;?></a></li>
                        <li><i class="fa fa-angle-left" aria-hidden="true"></i></li>
						<li><a href="javascript:void(0);" style="text-decoration:none; cursor:default;"><?php echo $checkout_personal_info;?></a></li>
						<li><i class="fa fa-angle-left" aria-hidden="true"></i></li>
						<li class="active"><a href="javascript:void(0);" style="text-decoration:none; cursor:default;"><?php echo $checkout_personal_info_address;?></a></li>
					</ul>
				</div>
				<div class="line"></div>
			</div>
			<div class="defaultPageSty">
				<div class="container max960_cont">
					
						<!--<ul class="row">
							<li class="col-md-4 col-sm-6 col-xs-6">
								<div>
									<div class="addCheckBx">
										<input  id="checkbox_li_1" type="checkbox" value="1" name="checkbox">
										<label for="checkbox_li_1"><span></span></label>
									</div>
									عنوان 1 : جدة - صندوق بريد <span class="numeric">123445</span> - عمارة 1ب 
									<div class="clearfix"></div>
									<a href="javascript:void(0);" class="changeAdd">تعديل العنوان</a>
								</div>
							</li>
							<li class="col-md-4 col-sm-6 col-xs-6">
								<div>
									<div class="addCheckBx">
										<input  id="checkbox_li_2" type="checkbox" value="1" name="checkbox">
										<label for="checkbox_li_2"><span></span></label>
									</div>
									عنوان 1 : جدة - صندوق بريد <span class="numeric">123445</span> - عمارة 1ب 
									<div class="clearfix"></div>
									<a href="javascript:void(0);" class="changeAdd">تعديل العنوان</a>
								</div>
							</li>
							<li class="col-md-4 col-sm-6 col-xs-6">
								<div>
									<div class="addCheckBx">
										<input  id="checkbox_li_3" type="checkbox" value="1" name="checkbox">
										<label for="checkbox_li_3"><span></span></label>
									</div>
									عنوان 1 : جدة - صندوق بريد <span class="numeric">123445</span> - عمارة 1ب 
									<div class="clearfix"></div>
									<a href="javascript:void(0);" class="changeAdd">تعديل العنوان</a>
								</div>
							</li>
						</ul>-->
					<form class="ajax_form_order" action="<?php echo base_url(); ?>ajax/action/" onsubmit="return false;" method="post" id="surveyForm">
                    	<input type="hidden" name="form_type" value="save_order" />
                        <?php foreach($cart_data as $c_data){?>
                        <input type="hidden" name="temp_order_id[]" value="<?php echo $c_data['id']?>" />
                        <input type="hidden" name="temp_order_quantity[]" value="<?php echo $c_data['quantity']?>" />
                        <input type="hidden" name="temp_price[]" value="<?php echo $c_data['prod_price']?>" />
                        
                        <?php }?>
                        <div class="addNewAddStep3">
						
							<?php if($this->session->userdata('user_id') && $this->session->userdata('login') == true && $addresses){?>
                            <h2><?php echo $shipping_address1_choose_already_registered;?></h2>
                            	<input type="hidden" id="user_guest" value="0" />
                                <ul class="row">
                                	<?php 
									$ch = 1;
									foreach($addresses as $addr){?>
                                        <li class="col-md-4 col-sm-6 col-xs-6">
                                            <div>
                                                <div class="addCheckBx">
                                                    <input id="checkbox_li_<?php echo $ch;?>" class="address_checkbox" type="radio" name="address_checkbox" value="<?php echo $addr['id']?>" <?php echo ($ch == '1' ? 'checked' : '');?>>
                                                    <label for="checkbox_li_<?php echo $ch;?>"><span></span></label>
                                                </div>
                                                <?php echo $ch;?> : <?php echo $addr['address_city']?> - <?php echo $addr['street_name']?> <span class="numeric"><?php echo $addr['property_no']?></span> 
                                                <div class="clearfix"></div>
                                                <a href="javascript:void(0);" onclick="editAddressCheckout('<?php echo $addr['id'];?>', true);" class="changeAdd"><?php echo $shipping_address1_edit_address;?></a>
                                            </div>
                                        </li>
                                    <?php 
									$ch++;
									}?>
                                </ul>
                                <?php 
									$ch = 1;
									foreach($addresses as $address){?>
                                		<div class="row hideAllAddresses" style="display:none;" id="showAddress_<?php echo $address['id'];?>">
                                        	<div class="col-md-4 col-sm-4 col-xs-12">
                                                <label><?php echo $register_country;?></label>
                                                <select name="address_country[]" id="address_country_<?php echo $address['id'];?>" class="countryClass">
                                                    <option value=""><?php echo $register_country;?></option>
                                                    <?php echo getCoutries($address['address_country']);?>
                                                </select>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label><?php echo $shipping_address_city;?></label>
                                                <select name="address_city[]" id="address_city_<?php echo $address['id'];?>" class="cityClass">
													<?php echo getCity($address['address_country'], $address['address_city']);?>
                                                </select>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12 mapPopUp">
                                                <label><?php echo $shipping_address_district;?></label>
                                                <input type="text" name="address_district" id="address_district_<?php echo $address['id'];?>" value="<?php echo $address['address_district'];?>"  class="address_district" />
                                                <a href="javascript:void(0);" class="openMap"><?php echo $register_choose_from_map;?></a>
                                                <div class="us3" style="display:none"></div>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label><?php echo $shipping_address_street_name;?></label>
                                                <input type="text" name="street_name" id="street_name_<?php echo $address['id'];?>" value="<?php echo $address['street_name'];?>"/>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label><?php echo $shipping_address_property_number;?></label>
                                                <input type="text" name="property_no" id="property_no_<?php echo $address['id'];?>" value="<?php echo $address['property_no'];?>" />
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label><?php echo $shipping_address_po_box;?></label>
                                                <input type="text" name="address_po_box" id="address_po_box_<?php echo $address['id'];?>" value="<?php echo $address['address_po_box'];?>" />
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label><?php echo $shipping_address_zip_code;?></label>
                                                <input type="text" name="address_zip_code" id="address_zip_code_<?php echo $address['id'];?>" value="<?php echo $address['address_zip_code'];?>" />
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <label><?php echo $shipping_address_more_details;?></label>
                                                <input type="text" name="more_detail" id="more_details_<?php echo $address['id'];?>" value="<?php echo $address['more_details'];?>"/>
                                            </div>
                                           
                                                <span <?php echo ($lang == 'eng' ? 'style="margin: 33px 20px 0 0;float: right;color: #FF0000; cursor:pointer;"' : 'style="margin: 33px 0 0 20px;float: left;color: #FF0000;cursor:pointer;'); ?>><a href="javascript:void(0);" onclick="editAddressCheckout('<?php echo $address['id'];?>', false);"><?php echo $shipping_address1_cancel_editing;?></a></span>
                                        </div>
                    			<?php 
									$ch++;
									}?>
                            <?php }else
							{
								?>
                            <input type="hidden" id="user_guest" value="1" />
                                <p>&nbsp;</p>

                                <!-- Uncomment when apply payment method-->
                                <div class="container max960_cont">
                                    <h1><?php echo $register_addresses;?></h1>
                                </div>
                                <div class="line"></div>
                                <div class="container max960_cont">
                                    <div class="row">
                                    	<div class="col-md-4 col-sm-4 col-xs-12">
                                            <label><?php echo $register_country;?><span class="redStar">*</span></label>
                                            <select name="address_country" id="address_country" class="countryClass" data-toggle="tooltip" data-placement="top" title="">
                                                <option value=""><?php echo $register_country;?></option>
                                                <?php echo getCoutries($temp_user->country);?>
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <label><?php echo $shipping_address_city;?><span class="redStar">*</span></label>
                                            <select name="address_city" id="address_city" class="cityClass" data-toggle="tooltip" data-placement="top" title="">
                                                <?php echo getCity($temp_user->country, $temp_user->city);?>
                                            </select>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12 mapPopUp">
                                            <label><?php echo $shipping_address_district;?><span class="redStar">*</span></label>
                                            <input type="text" name="address_district" id="address_district" class="address_district" data-toggle="tooltip" data-placement="top" title="" />
                                            <a href="javascript:void(0);" class="openMap"><?php echo $register_choose_from_map;?></a>
            								<div class="us3" style="display:none"></div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <label><?php echo $shipping_address_street_name;?><span class="redStar">*</span></label>
                                            <input type="text" name="street_name" id="street_name" data-toggle="tooltip" data-placement="top" title="" />
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <label><?php echo $shipping_address_property_number;?><span class="redStar">*</span></label>
                                            <input type="text" name="property_no" id="property_no" data-toggle="tooltip" data-placement="top" title="" />
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <label><?php echo $shipping_address_po_box;?><span class="redStar">*</span></label>
                                            <input type="text" name="address_po_box" id="address_po_box" data-toggle="tooltip" data-placement="top" title="" />
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <label><?php echo $shipping_address_zip_code;?><span class="redStar">*</span></label>
                                            <input type="text" name="address_zip_code" id="address_zip_code" data-toggle="tooltip" data-placement="top" title="" />
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <label><?php echo $shipping_address_more_details;?></label>
                                            <input type="text" name="more_detail" />
                                        </div>
                                    </div>
                                </div>
                            
							<?php }?>
                         </div>


                        <!--<div class="hide" id="optionTemplateTwo">-->
                        <?php if($this->session->userdata('user_id') && $this->session->userdata('login') == true && $addresses){?>
                            <div class="row" id="addMoreButtonParent">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 addMoreButton">
                                    <button type="button" class="btn addButtonTwo">+</button><?php echo $shipping_address1_add_another_address;?>
                                </div>
                            </div>
                        <?php }?>
                        
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                <input type="submit" value="<?php echo $checkout_continue_shopping; ?>" class="edBtn red" id="sendCheckoutBtn">
                            </div>
                        </div>
                    </form>
					<p>&nbsp;</p>
					<?php /*if($paymentMethod == 'cc_merchantpage') ://merchant page iframe method */?>
					<section class="merchant-page-iframe">
						<?php
						$objFort = new PayfortCheckout();
						$merchantPageData = $objFort->getMerchantPageData();
						$postData = $merchantPageData['params'];
						$gatewayUrl = $merchantPageData['url'];
						?>
						<div class="cc-iframe-display">
							<div id="div-pf-iframe" style="display:none">
								<div class="pf-iframe-container">
									<div class="pf-iframe" id="pf_iframe_content">
									</div>
								</div>
							</div>
						</div>
					</section>
					<?php /*endif; */?>
                    <a href="#orderNumber" class="fancybox" id="success_pop"></a>
				</div>
			</div>
		</div><!-- step 3 end -->
	</div>	
</section>


<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <?php echo $checkout_terms_conditions;?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    </div>
  </div>
</div>
<div class="hide" id="optionTemplateTwo">
    <div class="row">
    	<input type="hidden" value="1" id="hiddenAddress" />
        <div class="col-md-4 col-sm-4 col-xs-12">
            <label><?php echo $register_country;?><span class="redStar">*</span></label>
            <select name="address_country" id="address_country" class="countryClass" data-toggle="tooltip" data-placement="top" title="">
                <option value=""><?php echo $register_country;?></option>
                <?php echo getCoutries($temp_user->country);?>
            </select>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <label><?php echo $shipping_address_city;?><span class="redStar">*</span></label>
            <select name="address_city" id="address_city" class="cityClass" data-toggle="tooltip" data-placement="top" title="">
                <?php echo getCity($temp_user->country, $temp_user->city);?>
            </select>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12 mapPopUp">
            <label><?php echo $shipping_address_district;?><span class="redStar">*</span></label>
            <input type="text" name="address_district" id="address_district" class="address_district" data-toggle="tooltip" data-placement="top" title="" />
            <a href="javascript:void(0);" class="openMap"><?php echo $register_choose_from_map;?></a>
            <div class="us3" style="display:none"></div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <label><?php echo $shipping_address_street_name;?><span class="redStar">*</span></label>
            <input type="text" name="street_name" id="street_name" data-toggle="tooltip" data-placement="top" title="" />
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <label><?php echo $shipping_address_property_number;?><span class="redStar">*</span></label>
            <input type="text" name="property_no" id="property_no" data-toggle="tooltip" data-placement="top" title="" />
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <label><?php echo $shipping_address_po_box;?><span class="redStar">*</span></label>
            <input type="text" name="address_po_box" id="address_po_box" data-toggle="tooltip" data-placement="top" title="" />
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <label><?php echo $shipping_address_zip_code;?><span class="redStar">*</span></label>
            <input type="text" name="address_zip_code" id="address_zip_code" data-toggle="tooltip" data-placement="top" title="" />
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <label><?php echo $shipping_address_more_details;?></label>
            <input type="text" name="more_detail"  />
        </div>
    </div>
</div>