 <section class="">

        <div class="container">

            <div class="cartHeading fontWhtReg">

                <h1 class=""><?php echo $profile_my_account_label; ?></h1>

                <div class="clearfix"></div>

            </div>

            <div class="profilePage">

                <!-- Don't add top Bottom Padding -->

                <div class="leftMenu ">

                    <ul>

                        <li><a href="<?php echo lang_base_url().'profile'; ?>"><?php echo $my_profile_label; ?></a></li>

                        <li class="active"><a href="<?php echo lang_base_url().'profile/myOrder'; ?>"><?php echo $my_orders_my_orders; ?></a></li>

                    </ul>

                    <a href="<?php echo lang_base_url().'login/logout'; ?>" class="logOut"><?php echo $logout_link; ?></a>

                </div>

                <!-- Don't add top Bottom Padding -->

                <div class="rhtProfSec ">

                    <div class="myOrderSec">

                        <!-- Nav tabs -->

                        <ul class="nav nav-tabs" role="tablist">

                            <li role="presentation" class="active"><a href="#currOrder" aria-controls="currOrder" role="tab" data-toggle="tab" onClick="$('.commonRating').val('');"><?php echo $my_order_current_order_label; ?></a></li>

                            <li role="presentation"><a href="#completed" aria-controls="completed" role="tab" data-toggle="tab" onClick="$('.commonRating').val('');"><?php echo $my_order_completed_label; ?></a></li>

                        </ul>
                        

                        <!-- Tab panes -->

                        <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="currOrder">
                            
                            <!-- start here-->
                         
                         <?php 
						 if($current_orders){
						 $count = 1;
						  foreach($current_orders as $order) {
							if($count == 1){
								$display1 = 'none;';
								$display2 = 'block;';
							}else{
								$display1 = 'block;';
								$display2 = 'none;';
							}
							
						
							$active1 = '';
							$active2 = '';
							$active3 = '';
							$active4 = '';
							$cancelOrder = false;
							
							
							if($order['order_status'] == 1) {
								$active1 = 'active';
								$order_status = $my_order_received_label;
								
								// only for pending
								$cancelOrder = true;
							}
							if($order['order_status'] == 2) {
								$active1 = 'active';
                                $cancelOrder = true;

								$order_status = $my_order_received_label;
							}if($order['order_status'] == 3) {
								$active1 = 'active';
								$active2 = 'active';
								$order_status = $my_order_preparing_ship_label;
							}
							if($order['order_status'] == 4) {
								$active1 = 'active';
								$active2 = 'active';
								$active3 = 'active';
								$order_status = $my_order_shipped_label;
							}
                              if($order['order_status'] == 5) {
                                  $active1 = 'active';
                                  $active2 = 'active';
                                  $active3 = 'active';
                                  $active4 = 'active';
                                  $order_status = $my_order_delivered_label;
                              }
							
										
							
							$porducts = getOrderProducts($order['id']);
							
							?>
						   
                            <div style="display: <?php echo $display1; ?>" id="order_<?php echo $count; ?>" class="orderPlaced green onlyOrder ">
                                <div class="menuBtn">
                                  
                                   <?php if($cancelOrder){ ?>
                                    <div class="dropdown">
                                        <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="sprite_ioud ioudSpdropdown"></i>
                                        </button>
                                       
                                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                                            <!--<li><a href="javascript:void(0);"><?php //echo $edit_label; ?></a> </li>-->         
                                            <li><a href="javascript:void(0);" onclick="cancelOrder('<?php echo $order["id"];?>', '<?php echo $cancel_confirm_general; ?>?');"><?php echo $cancel_label; ?></a> </li>
                                            
                                          
                                        </ul>
                                       
                                    </div>
                                     <?php } ?>
                                     
                                </div>
                                
                                <div class="row" onClick="expandOrder('<?php echo $count; ?>');">
                                    <div class="col-md-6">
                                        <h4>
                                            <span class="black">
                                                <?php echo $my_order_order_no_label; ?>
                                                <strong><?php echo str_pad($order['id'], 6, "0", STR_PAD_LEFT);?></strong>
                                            </span>
                                            <span class="status"><?php echo $order_status; ?></span>
                                        </h4>
                                    </div>
                                    <div class="col-md-6">
                                        <p class="text-right"><span>(<?php echo count($porducts); ?> <?php echo $products_count_label; ?>)</span></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-7 col-md-12">
                                        <ul class="orderStpStyle">
                                        
                                        
                                        
                                            <li class="<?php echo $active1; ?>"><a href="javascript:void(0);"></a></li>
                                            <li class="<?php echo $active2; ?>"><a href="javascript:void(0);"></a></li>
                                            <li class="<?php echo $active3; ?>"><a href="javascript:void(0);"></a></li>
                                            <li class="<?php echo $active4; ?>"><a href="javascript:void(0);"></a></li>
                                        </ul>
                                    </div>
                                    
                <?php if(strtolower($order['payment_method']) == "visa"){
					$payment_img = base_url().'assets/frontend/images/visa_payment.png';
				}
				elseif(strtolower($order['payment_method']) == "cash on delivery"){
					$payment_img = base_url().'assets/frontend/images/cash_payment.png';
				}elseif(strtolower($order['payment_method']) == "bank_transfer"){
					$payment_img = base_url().'assets/frontend/images/wire_payment.png';
				}else{
					$payment_img = base_url().'assets/frontend/images/sadad_payment.png';
				}
				
				 ?>
                                    <div class="col-lg-5 col-md-12">
                                        <h1>
                                            <img src="<?php echo $payment_img; ?>" alt="<?php echo $order['payment_method']; ?>" height="32" width="42" />
                                            <?php echo number_format($order['total_amount'],2, '.', ''); ?> <span><?php echo getCurrency($lang); ?></span>
                                        </h1>
                                    </div>
                                </div>
                            </div>
                            
                            
                            
                            
                            <div style="display: <?php echo $display2; ?>" id="order_<?php echo $count; ?>" class="orderPlaced green ordDtl">
                                <div class="menuBtn">
                                   
                                  <?php if($cancelOrder) { ?>  
                                    <div class="dropdown">
                                        <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="sprite_ioud ioudSpdropdown"></i>
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                                            
                                           
                                            <li><a href="javascript:void(0);" onclick="cancelOrder('<?php echo $order["id"];?>', '<?php echo $cancel_confirm_general; ?>?');"><?php echo $cancel_label; ?></a> </li>
                                           
                                        </ul>
                                    </div>
                                     <?php } ?>
                                    
                                </div>
                                
                                <div class="row" onClick="closeOrder('<?php echo $count; ?>');">
                                    <div class="col-md-5">
                                        <h4>
                                            <?php echo $my_order_order_no_label; ?>
                                            <strong><?php echo str_pad($order['id'], 6, "0", STR_PAD_LEFT);?></strong>
                                            <span class="status"><?php echo $order_status; ?></span>
                                        </h4>

                                        <?php /*?><p><?php echo getShipmentDays($order['shipment_group'],$order['shipemnt_type']);?>:</p><?php */?>

                                        <strong><?php echo $user['first_name']." ".$user['last_name']; ?></strong>

                                        <?php $address = getUserAddress($order['address_id'],$user['id']); ?>
                                        
                                        <br>
                                        <?php if($address['address_1'] != "") { ?>
                                        	<p><?php echo $address['address_1']; ?></p>
                                        <?php } ?>
                                        <?php if($address['address_2'] != "") { ?>
                                        <?php echo $address['address_2']; ?>
                                        <?php } ?>
                                        <p><?php echo getCoutryByCode($address['country']); ?></p>
                                        <?php if($address['phone_no'] != "") { ?>
                                        	<?php echo $phone_label; ?>: <span class="numberArb"><?php echo $address['phone_no']; ?></span>
                                        <?php } ?>
                                    </div>
                                    <div class="col-md-7 text-right">
                                        <ul class="orderStpStyle">
                                       
                                            <li class="<?php echo $active1; ?>"><a href="javascript:void(0);"><?php echo $my_order_received_label; ?></a></li>
                                            <li class="<?php echo $active2; ?>"><a href="javascript:void(0);"><?php echo $my_order_preparing_ship_label; ?></a></li>
                                            <li class="<?php echo $active3; ?>"><a href="javascript:void(0);"><?php echo $my_order_shipped_label; ?></a></li>
                                            <li class="<?php echo $active4; ?>"><a href="javascript:void(0);"><?php echo $my_order_delivered_label; ?></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <p class="text-right"><span>(<?php echo count($porducts); ?> <?php echo $products_count_label; ?>)</span></p>
                                <table class="table">
                                    <tbody>
                                    
                             <?php foreach($porducts as $porduct) { 
							 
							 $images = getProductImages($porduct['id']);
							
							foreach($images as $key => $img){
								if($img['is_thumbnail'] == 1){
									$thumbnail = $img['thumbnail'];
								}
							}
							
							 ?>      
                                  
                                    <tr>
                                        <td class="imgBox text-center" style="position: relative;"><img src="<?php echo base_url(); ?>uploads/images/thumbs/products/<?php echo $thumbnail; ?>" alt="Pro" <="" td="" width="111" height="144">
                                        </td>
                                        <td class="text-left"><h3><?php echo $porduct[$lang.'_name']; ?></h3></td>
                                        <td class="text-right"><?php echo $shopping_cart_qty; ?>: <span> <?php echo $porduct['quantity']; ?></span></td>
                                        <td class="text-right"><?php echo number_format($porduct['price'],'2','.',''); ?> <span> <?php echo getCurrency($lang); ?></span></td>
                                    </tr>
                                    
                                    
                                  <?php } ?>  
                                    
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th class="text-right" colspan="3"><?php echo ($order['shipment_type'] == 'normal' ? $checkout_shipping_address_normal_shipping : $checkout_shipping_address_fast_shipping )?></th>
                                        <th class="text-right"><?php echo number_format($order['shipment_price'], 2, '.', '');?> <span> <?php echo getCurrency($lang); ?></span></th>
                                    </tr>
                                    <?php if($order['payment_method'] == 'Cash On Delivery'){?>
                                        <tr>
                                            <th class="text-right" colspan="3"><?php echo $my_order_cod_label;?></th>
                                            <th class="text-right"><?php echo number_format($order['extra_charges'], 2, '.', '');?> <span> <?php echo getCurrency($lang); ?></span></th>
                                        </tr>
									<?php }?>                                    
                                    </tfoot>
                                </table>
                                
                                <h2 class="price text-right">
                                    <span class="pull-left"><?php echo $grand_total_label; ?></span>
                                    <img src="<?php echo $payment_img; ?>" alt="<?php echo $order['payment_method']; ?>" height="17" width="42" />
                                     <?php echo number_format($order['total_amount'],2, '.', ''); ?>
                                    <span class="curr"><?php echo getCurrency($lang); ?></span>
                                </h2>
                                
                              <!--  <div class="revFeedback">
                                    <h5><?php echo $my_order_reviews_label; ?></h5>
                                    <form class="saveStarRating" action="<?php echo base_url().'ajax/saveRating'; ?>" method="post">
                                    
								<?php $rating = getRatingByOrder($order['id'],$user['id']);

                                
								$product_active['1'] = '';
                                $product_active['2'] = '';
                                $product_active['3'] = '';
                                $product_active['4'] = '';
                                $product_active['5'] = '';
                                $product_active['6'] = '';
                                
                                for($i = 1; $i<=intval($rating['product_rating']); $i++){
								
								$product_active[$i] = 'active';
								
								}
                                
                                ?>
                                        <div class="row">
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <label><?php echo $my_order_product_quality_label; ?></label>
                                                <ul class="stars stars_1">
                                                    <li class="<?php echo $product_active['1']; ?> saveRating" data-product_rating="1"><a href="javascript:void(0);"><i class="fa fa-star"  aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $product_active['2']; ?> saveRating" data-product_rating="2"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $product_active['3']; ?> saveRating" data-product_rating="3"><a href="javascript:void(0);"><i class="fa fa-star"  aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $product_active['4']; ?> saveRating" data-product_rating="4"><a href="javascript:void(0);"><i class="fa fa-star"  aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $product_active['5']; ?> saveRating" data-product_rating="5"><a href="javascript:void(0);"><i class="fa fa-star"  aria-hidden="true"></i></a> </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <label><?php echo $my_order_packaging_label; ?></label>
                                                <ul class="stars stars_2">
                                                
								<?php 
                                
                                $packaging_active['1'] = '';
                                $packaging_active['2'] = '';
                                $packaging_active['3'] = '';
                                $packaging_active['4'] = '';
                                $packaging_active['5'] = '';
                                $packaging_active['6'] = '';
                                
                                for($i = 1; $i<=intval($rating['packaging_rating']); $i++){
                                
                                $packaging_active[$i] = 'active';
                                
                                }
                                
                                ?>
                                                
                                                    <li class="<?php echo $packaging_active['1']; ?> saveRating" data-packaging_rating="1"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $packaging_active['2']; ?> saveRating" data-packaging_rating="2"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $packaging_active['3']; ?> saveRating" data-packaging_rating="3"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $packaging_active['4']; ?> saveRating" data-packaging_rating="4"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $packaging_active['5']; ?> saveRating" data-packaging_rating="5"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <label><?php echo $my_order_packing_label; ?></label>
                                                
                             <?php 
                                
                                $packing_active['1'] = '';
                                $packing_active['2'] = '';
                                $packing_active['3'] = '';
                                $packing_active['4'] = '';
                                $packing_active['5'] = '';
                                $packing_active['6'] = '';
                                
                                for($i = 1; $i<=intval($rating['packing_rating']); $i++){
                                
                                $packing_active[$i] = 'active';
                                
                                }
                                
                              ?>
                                                
                                                <ul class="stars stars_3">
                                                    <li class="<?php echo $packing_active['1']; ?> saveRating" data-packing_rating="1"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $packing_active['2']; ?> saveRating" data-packing_rating="2"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $packing_active['3']; ?> saveRating" data-packing_rating="3"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $packing_active['4']; ?> saveRating" data-packing_rating="4"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $packing_active['5']; ?> saveRating" data-packing_rating="5"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <label><?php echo $my_order_ioud_customer_service_label; ?></label>
                                                
                                                
                             <?php 
                                
                                $service_active['1'] = '';
                                $service_active['2'] = '';
                                $service_active['3'] = '';
                                $service_active['4'] = '';
                                $service_active['5'] = '';
                                $service_active['6'] = '';
                                
                                for($i = 1; $i<=intval($rating['service_rating']); $i++){
                                
                                $service_active[$i] = 'active';
                                
                                }
                                
                              ?>
                                                
                                                
                                                <ul class="stars stars_4">
                                                    <li class="<?php echo $service_active['1']; ?> saveRating" data-service_rating="1"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $service_active['2']; ?> saveRating" data-service_rating="2"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $service_active['3']; ?> saveRating" data-service_rating="3"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $service_active['4']; ?> saveRating" data-service_rating="4"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $service_active['5']; ?> saveRating" data-service_rating="5"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <label><?php echo $my_order_shipment_duration_label; ?></label>
                                                
                                                
                              <?php 
                                
                                $shipment_active['1'] = '';
                                $shipment_active['2'] = '';
                                $shipment_active['3'] = '';
                                $shipment_active['4'] = '';
                                $shipment_active['5'] = '';
                                $shipment_active['6'] = '';
                                
                                for($i = 1; $i<=intval($rating['shipment_rating']); $i++){
                                
                                $shipment_active[$i] = 'active';
                                
                                }
                                
                              ?>
                                                
                                                
                                                <ul class="stars stars_5">
                                                    <li class="<?php echo $shipment_active['1']; ?> saveRating" data-shipment_rating="1"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $shipment_active['2']; ?> saveRating" data-shipment_rating="2"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $shipment_active['3']; ?> saveRating" data-shipment_rating="3"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $shipment_active['4']; ?> saveRating" data-shipment_rating="4"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $shipment_active['5']; ?> saveRating" data-shipment_rating="5"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <label><?php echo $my_order_satisfaction_label; ?></label>
                                                
                                                
                            <?php 
                                
                                $satisfaction_active['1'] = '';
                                $satisfaction_active['2'] = '';
                                $satisfaction_active['3'] = '';
                                $satisfaction_active['4'] = '';
                                $satisfaction_active['5'] = '';
                                $satisfaction_active['6'] = '';
                                
                                for($i = 1; $i<=intval($rating['satisfaction_rating']); $i++){
                                
                                $satisfaction_active[$i] = 'active';
                                
                                }
                                
                             ?>
                                                
                                                <ul class="stars stars_6">
                                                    <li class="<?php echo $satisfaction_active['1']; ?> saveRating" data-satisfaction_rating="1"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $satisfaction_active['2']; ?> saveRating" data-satisfaction_rating="2"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $satisfaction_active['3']; ?> saveRating" data-satisfaction_rating="3"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $satisfaction_active['4']; ?> saveRating" data-satisfaction_rating="4"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $satisfaction_active['5']; ?> saveRating" data-satisfaction_rating="5"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <textarea placeholder="<?php echo $my_order_feedback_placeholder; ?>" name="feedback" class="feedback commonRating"></textarea>
                                           <input type="hidden" name="order_id" value="<?php echo $order['id']; ?>">     
                                            <input type="hidden" class="commonRating product_rating" name="product_rating" value="">    
                                            <input type="hidden" class="commonRating packaging_rating" name="packaging_rating" value="">    
                                            <input type="hidden" class="commonRating packing_rating" name="packing_rating" value="">    
                                            <input type="hidden" class="commonRating service_rating" name="service_rating" value="">    
                                            <input type="hidden" class="commonRating shipment_rating" name="shipment_rating" value="">    
                                            <input type="hidden" class="commonRating satisfaction_rating" name="satisfaction_rating" value="">    
                                                
                                               
                                            </div>
                                            <div class="col-xs-12">
                                                <button type="submit" class="btn btn-black"><?php echo $submit_btn_label; ?></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>-->
                                <br>
                                <div class="clearfix"></div>

                            </div>
                            
                           <?php $count++;
						   }
						 }else{ ?>
                         
                         <span style="color:red;"><?php echo $no_record_founds_label; ?></span>
                         <?php } ?>   
                            <!--- end here-->
                            
                        </div>
                        
                        
                        <!-- completed orders-->
                        
                        <div role="tabpanel" class="tab-pane" id="completed">

						<!-- closed tab-->
                        
                        <?php 
						 if($complete_orders){
						 $c = 1;
						  foreach($complete_orders as $compOrder) {
							if($count == 1){
								$compDisplay1 = 'none;';
								$compDisplay2 = 'block;';
							}else{
								$compDisplay1 = 'block;';
								$compDisplay2 = 'none;';
							}
							
							
							$compActive1 = '';
							$compActive2 = '';
							$compActive3 = '';
							$compActive4 = '';
							$returnComOrder = false; 
							
							if($compOrder['order_status'] == 1) {
							$compActive1 = 'active';
							
							}
							if($compOrder['order_status'] == 2) {
								$compActive2 = 'active';
							}
							if($compOrder['order_status'] == 3) {
								$compActive3 = 'active';
							}
							if($compOrder['order_status'] == 4) {
								$compActive4 = 'active';

							}
                              if($compOrder['order_status'] == 6) {
                                  $returnComOrder = true;
                              }

                              $compPorducts = getOrderProducts($compOrder['id']);
							
							?>

                            <div class="orderPlaced onlyOrder black" style="display: <?php echo $compDisplay1; ?>" id="order_<?php echo $c; ?>">
                                <div class="menuBtn">
                                    <div class="dropdown">
                                        <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="sprite_ioud ioudSpdropdown"></i>
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                                            
                                        <?php if($returnComOrder) { ?>    
                                            <li><a href="javascript:void(0);" onClick="returnOrder('<?php echo $compOrder['id'];?>');"><?php echo $return_label; ?></a> </li>
                                            <?php } ?>
                                            <?php /*if($removeComOrder) { ?>
                                            <li><a href="javascript:void(0);" onclick="cancelOrder('<?php echo $compOrder["id"];?>', '<?php echo $cancel_confirm_general; ?>?');"><?php echo $cancel_label; ?></a> </li>
                                            <?php }*/ ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row" onClick="expandOrder('<?php echo $c; ?>');">
                                    <div class="col-md-6">
                                        <h4>
                                            <span class="black">
                                                 <?php echo $my_order_order_no_label; ?>
                                                <strong><?php echo str_pad($compOrder['id'], 6, "0", STR_PAD_LEFT);?></strong>
                                            </span>
                                            <span class="status"><?php echo $my_order_completed_label; ?></span>
                                        </h4>
                                    </div>
                                    <div class="col-md-6">
                                        <p class="text-right"><span>(<?php echo count($compPorducts); ?> <?php echo $products_count_label; ?>)</span></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-7 col-md-12">
                                        <ul class="orderStpStyle black">
                                      
                                        
                                            <li class="<?php echo $compActive1; ?>"><a href="javascript:void(0);"></a></li>
                                            <li class="<?php echo $compActive2; ?>"><a href="javascript:void(0);"></a></li>
                                            <li class="<?php echo $compActive3; ?>"><a href="javascript:void(0);"></a></li>
                                            <li class="<?php echo $compActive4; ?>"><a href="javascript:void(0);"></a></li>
                                        </ul>
                                    </div>
                                    
			<?php if(strtolower($compOrder['payment_method']) == "visa"){
					$payment_img = base_url().'assets/frontend/images/visa_payment.png';
				}
				elseif(strtolower($compOrder['payment_method']) == "cash on delivery"){
					$payment_img = base_url().'assets/frontend/images/cash_payment.png';
				}elseif(strtolower($compOrder['payment_method']) == "bank_transfer"){
					$payment_img = base_url().'assets/frontend/images/wire_payment.png';
				}else{
					$payment_img = base_url().'assets/frontend/images/sadad_payment.png';
				}
			 ?>
                                    
                                    
                                    <div class="col-lg-5 col-md-12">
                                        <h1>
                                            <img src="<?php echo $payment_img; ?>" alt="<?php echo $compOrder['payment_method']; ?>" height="32" width="42" />
                                            <?php echo number_format($compOrder['total_amount'],2, '.', ''); ?> <span><?php echo getCurrency($lang); ?></span>
                                        </h1>
                                    </div>
                                </div>
                            </div>
                            
                        <!-- end closed tab-->
                              
                        <div style="display: <?php echo $compDisplay2; ?>" id="order_<?php echo $c; ?>" class="orderPlaced black ordDtl">
                                <div class="menuBtn">
                                    <div class="dropdown">
                                        <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="sprite_ioud ioudSpdropdown"></i>
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                                          <?php if($returnComOrder) { ?>
                                            <li><a href="javascript:void(0);" onClick="returnOrder('<?php echo $compOrder['id'];?>');"><?php echo $return_label; ?></a> </li>
                                            <?php } ?>
                                            
                                            <!--<li><a href="javascript:void(0);" onclick="cancelOrder('<?php //echo $compOrder["id"];?>', '<?php //echo $cancel_confirm_general; ?>?');"><?php //echo $cancel_label; ?></a> </li>-->
                                        </ul>
                                    </div>
                                </div>
                                <div class="row" onClick="closeOrder('<?php echo $c; ?>');">
                                    <div class="col-md-5">
                                        <h4>
                                            <?php echo $my_order_order_no_label; ?>
                                            <strong><?php echo str_pad($compOrder['id'], 6, "0", STR_PAD_LEFT);?></strong>
                                            <span class="status"><?php echo $my_order_completed_label; ?></span>
                                        </h4>
                                        <p><?php echo getShipmentDays($compOrder['shipment_group'],$compOrder['shipemnt_type']);?>:</p>
                                        <strong><?php echo $user['first_name']." ".$user['last_name']; ?></strong>
                                        <?php $address = getUserAddress($compOrder['address_id'],$compOrder['user_id']); ?>
                                        
                                        <br>
                                        <?php if($address['address_1'] != "") { ?>
                                        	<p><?php echo $address['address_1']; ?></p>
                                        <?php } ?>
                                        <?php if($address['address_2'] != "") { ?>
                                        <?php echo $address['address_2']; ?>
                                        <?php } ?>
                                        <p><?php echo getCoutryByCode($address['country']); ?></p>
                                        <?php if($address['phone_no'] != "") { ?>
                                        	<?php echo $phone_label; ?>: <?php echo $address['phone_no']; ?>
                                        <?php } ?>
                                    </div>
                                    <div class="col-md-7 text-right">
                                        <ul class="orderStpStyle black">
                                            
                                            <li class="<?php echo $compActive1; ?>"><a href="javascript:void(0);"><?php echo $my_order_received_label; ?></a></li>
                                            <li class="<?php echo $compActive2; ?>"><a href="javascript:void(0);"><?php echo $my_order_preparing_ship_label; ?></a></li>
                                            <li class="<?php echo $compActive3; ?>"><a href="javascript:void(0);"><?php echo $my_order_shipped_label; ?></a></li>
                                            <li class="<?php echo $compActive4; ?>"><a href="javascript:void(0);"><?php echo $my_order_delivered_label; ?></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <p class="text-right"><span>(<?php echo count($compPorducts); ?> <?php echo $products_count_label; ?>)</span></p>
                                <table class="table">
                                    <tbody>
                                    
                             <?php

                             // feedback and reviews dynamically here
                             $feedbackList['product_quality'] = "0";
                             $feedbackList['shipment_packaging'] = "0";
                             $feedbackList['product_packing'] = "0";
                             $feedbackList['customer_service'] = "0";
                             $feedbackList['shipment_duration'] = "0";
                             $feedbackList['satisfaction'] = "0";

                             foreach($feedback as $name => $feed){

                                 $feedbackList[$name] = $feed;

                             }
                             // end feedback portion

                             foreach($compPorducts as $compPorduct) {
							 
							 $images = getProductImages($compPorduct['id']);
							
							foreach($images as $key => $img){
								if($img['is_thumbnail'] == 1){
									$thumbnail = $img['thumbnail'];
								}
							}
							
							 ?>      
                                  
                                    <tr>
                                        <td class="imgBox text-center" style="position: relative;"><img src="<?php echo base_url(); ?>uploads/images/thumbs/products/<?php echo $thumbnail; ?>" alt="Pro" <="" td="" width="111" height="144">
                                        </td>
                                        <td class="text-left"><h3><?php echo $compPorduct[$lang.'_name']; ?></h3></td>
                                        <td class="text-right">Qty: <span> <?php echo $compPorduct['quantity']; ?></span></td>
                                        <td class="text-right"><?php echo number_format($compPorduct['price'],'2','.',''); ?> <span> <?php echo getCurrency($lang); ?></span></td>
                                    </tr>
                                    
                                    
                                  <?php } ?>  
                                    
                                   
                                    
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th class="text-right" colspan="3"><?php echo ($compOrder['shipment_type'] == 'normal' ? $checkout_shipping_address_normal_shipping : $checkout_shipping_address_fast_shipping )?></th>
                                        <th class="text-right"><?php echo number_format($compOrder['shipment_price'], 2, '.', '');?> <span> <?php echo getCurrency($lang); ?></span></th>
                                    </tr>
                                    <?php if($compOrder['payment_method'] == 'Cash On Delivery'){?>
                                        <tr>
                                            <th class="text-right" colspan="3"><?php echo $my_order_cod_label;?></th>
                                            <th class="text-right"><?php echo number_format($compOrder['extra_charges'], 2, '.', '');?> <span> <?php echo getCurrency($lang); ?></span></th>
                                        </tr>
									<?php }?>
                                    </tfoot>
                                </table>
                                <h2 class="price text-right">
                                    <span class="pull-left"><?php echo $grand_total_label; ?></span>
                                    <img src="<?php echo $payment_img; ?>" alt="<?php echo $compOrder['payment_method']; ?>" height="17" width="42" />
                                     <?php echo number_format($compOrder['total_amount'],2, '.', ''); ?>
                                    <span class="curr"><?php echo getCurrency($lang); ?></span>
                                </h2>
                                
                                
                                
                                <?php $rating = getRatingByOrder($compOrder['id'],$user['id']);
                                if(!$rating){
                                    $existRating = '0';
                                }else{
                                    $existRating = '1';
                                }

								$product_active['1'] = '';
                                $product_active['2'] = '';
                                $product_active['3'] = '';
                                $product_active['4'] = '';
                                $product_active['5'] = '';
                                $product_active['6'] = '';
                                
                                for($i = 1; $i<=intval($rating['product_rating']); $i++){
								
									$product_active[$i] = 'active';
								
								}
                                ?>
                                
                                
                                <div class="revFeedback">
                                    <h5><?php echo $my_order_reviews_label; ?></h5>
                                    <form class="saveStarRating" action="<?php echo base_url().'ajax/saveRating'; ?>" method="post">
                                        <div class="row">
                                            <?php if($feedbackList['product_quality'] == "1") { ?>
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <label><?php echo $my_order_product_quality_label; ?></label>
                                                <ul class="stars stars_1">
                                                    <li class="<?php echo $product_active['1']; ?> saveRating" data-product_rating="1"><a href="javascript:void(0);"><i class="fa fa-star"  aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $product_active['2']; ?> saveRating" data-product_rating="2"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $product_active['3']; ?> saveRating" data-product_rating="3"><a href="javascript:void(0);"><i class="fa fa-star"  aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $product_active['4']; ?> saveRating" data-product_rating="4"><a href="javascript:void(0);"><i class="fa fa-star"  aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $product_active['5']; ?> saveRating" data-product_rating="5"><a href="javascript:void(0);"><i class="fa fa-star"  aria-hidden="true"></i></a> </li>
                                                </ul>
                                            </div>
                                  <?php } ?>
                              <?php if($feedbackList['shipment_packaging'] == "1") { ?>
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <label><?php echo $my_order_packaging_label; ?></label>
                              <?php 
										
								$packaging_active['1'] = '';
                                $packaging_active['2'] = '';
                                $packaging_active['3'] = '';
                                $packaging_active['4'] = '';
                                $packaging_active['5'] = '';
                                $packaging_active['6'] = '';
                                
                                for($i = 1; $i<=intval($rating['packaging_rating']); $i++){
								
									$packaging_active[$i] = 'active';
								
								}
                                
							?>
                                                
                                                
                                                <ul class="stars stars_2">
                                                    <li class="<?php echo $packaging_active['1']; ?> saveRating" data-packaging_rating="1"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $packaging_active['2']; ?> saveRating" data-packaging_rating="2"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $packaging_active['3']; ?> saveRating" data-packaging_rating="3"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $packaging_active['4']; ?> saveRating" data-packaging_rating="4"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $packaging_active['5']; ?> saveRating" data-packaging_rating="5"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                </ul>
                                            </div>

                                  <?php } ?>
                                    <?php if($feedbackList['product_packing'] == "1") { ?>
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <label><?php echo $my_order_packing_label; ?></label>
                                                
                                  
                             <?php 
										
								$packing_active['1'] = '';
                                $packing_active['2'] = '';
                                $packing_active['3'] = '';
                                $packing_active['4'] = '';
                                $packing_active['5'] = '';
                                $packing_active['6'] = '';
                                
                                for($i = 1; $i<=intval($rating['packing_rating']); $i++){
								
									$packing_active[$i] = 'active';
								
								}
                                
							?>
                                                <ul class="stars stars_3">
                                                    <li class="<?php echo $packing_active['1']; ?> saveRating" data-packing_rating="1"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $packing_active['2']; ?> saveRating" data-packing_rating="2"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $packing_active['3']; ?> saveRating" data-packing_rating="3"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $packing_active['4']; ?> saveRating" data-packing_rating="4"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $packing_active['5']; ?> saveRating" data-packing_rating="5"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                </ul>
                                            </div>
                                  <?php } ?>
                              <?php if($feedbackList['customer_service'] == "1") { ?>
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <label><?php echo $my_order_ioud_customer_service_label; ?></label>
                                                
                           <?php 
										
								$service_active['1'] = '';
                                $service_active['2'] = '';
                                $service_active['3'] = '';
                                $service_active['4'] = '';
                                $service_active['5'] = '';
                                $service_active['6'] = '';
                                
                                for($i = 1; $i<=intval($rating['service_rating']); $i++){
								
									$service_active[$i] = 'active';
								
								}
                                
							?>
                                                
                                                <ul class="stars stars_4">
                                                    <li class="<?php echo $service_active['1']; ?> saveRating" data-service_rating="1"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $service_active['2']; ?> saveRating" data-service_rating="2"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $service_active['3']; ?> saveRating" data-service_rating="3"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $service_active['4']; ?> saveRating" data-service_rating="4"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $service_active['5']; ?> saveRating" data-service_rating="5"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                </ul>
                                            </div>
                                  <?php } ?>
                              <?php if($feedbackList['shipment_duration'] == "1") { ?>
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <label><?php echo $my_order_shipment_duration_label; ?></label>
                                                
                                                
                                 
                         	<?php 
										
								$shipment_active['1'] = '';
                                $shipment_active['2'] = '';
                                $shipment_active['3'] = '';
                                $shipment_active['4'] = '';
                                $shipment_active['5'] = '';
                                $shipment_active['6'] = '';
                                
                                for($i = 1; $i<=intval($rating['shipment_rating']); $i++){
								
									$shipment_active[$i] = 'active';
								
								}
                                
							?>
                                                <ul class="stars stars_5">
                                                    <li class="<?php echo $shipment_active['1']; ?> saveRating" data-shipment_rating="1"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $shipment_active['2']; ?> saveRating" data-shipment_rating="2"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $shipment_active['3']; ?> saveRating" data-shipment_rating="3"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $shipment_active['4']; ?> saveRating" data-shipment_rating="4"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $shipment_active['5']; ?> saveRating" data-shipment_rating="5"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                </ul>
                                            </div>
                                  <?php } ?>
                              <?php if($feedbackList['satisfaction'] == "1") { ?>
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <label><?php echo $my_order_satisfaction_label; ?></label>
                                                
                                                
                                                
                            <?php 
										
								$satisfaction_active['1'] = '';
                                $satisfaction_active['2'] = '';
                                $satisfaction_active['3'] = '';
                                $satisfaction_active['4'] = '';
                                $satisfaction_active['5'] = '';
                                $satisfaction_active['6'] = '';
                                
                                for($i = 1; $i<=intval($rating['satisfaction_rating']); $i++){
								
									$satisfaction_active[$i] = 'active';
								
								}
                                
							?>
                                                
                                                <ul class="stars stars_6">
                                                    <li class="<?php echo $satisfaction_active['1']; ?> saveRating" data-satisfaction_rating="1"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $satisfaction_active['2']; ?> saveRating" data-satisfaction_rating="2"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $satisfaction_active['3']; ?> saveRating" data-satisfaction_rating="3"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $satisfaction_active['4']; ?> saveRating" data-satisfaction_rating="4"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                    <li class="<?php echo $satisfaction_active['5']; ?> saveRating" data-satisfaction_rating="5"><a href="javascript:void(0);"><i class="fa fa-star" aria-hidden="true"></i></a> </li>
                                                </ul>
                                            </div>
                              <?php } ?>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12">
                                                <textarea placeholder="<?php echo $my_order_feedback_placeholder; ?>" name="feedback" class="feedback commonRating"></textarea>

                                           <input type="hidden" name="order_id" value="<?php echo $compOrder['id']; ?>">
                                            <input type="hidden" class="commonRating product_rating" name="product_rating" value="">
                                            <input type="hidden" class="commonRating packaging_rating" name="packaging_rating" value="">    
                                            <input type="hidden" class="commonRating packing_rating" name="packing_rating" value="">    
                                            <input type="hidden" class="commonRating service_rating" name="service_rating" value="">    
                                            <input type="hidden" class="commonRating shipment_rating" name="shipment_rating" value="">    
                                            <input type="hidden" class="commonRating satisfaction_rating" name="satisfaction_rating" value="">    
                                                
                                               
                                            </div>
                                            <div class="col-xs-12">
                                                <button type="submit" class="btn btn-black" <?php if($existRating == '1') echo 'disabled'; ?>><?php echo $submit_btn_label; ?></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                
                                
                                
                                <div class="clearfix"></div>

                            </div>
                        
                        <?php $c++; }
						} else{ ?>
                         
                         <span style="color:red;"><?php echo $no_record_founds_label; ?></span>
                         <?php } ?>  
                        
                        
                            
                        </div>
                    </div>

                    </div>

                </div>

                <div class="clearfix"></div>

            </div>

        </div>

    </section>



    <!-- Add new Address -->

    <div class="modal fade iOudModel" id="addAddress" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">

        <div class="modal-dialog modal-sm" role="document">

            <div class="modal-content">

                <div class="modal-header">

                    <h4 class="modal-title" id="myModalLabel">Attention! Login</h4>

                </div>

                <div class="modal-body">

                    <div class="popupForm newAdd_form" >

                        <form action="javascript:void(0);" method="get">

                            <label>Address Title</label>

                            <input type="text" placeholder="Enter Here" class="" required>



                            <label>Street address 1</label>

                            <input type="text" placeholder="Apartment / building No. " class="" required>



                            <label>Street address 2</label>

                            <input type="text" placeholder="Area / District / Street" class="" required>



                            <label>Country </label>

                            <select>

                                <option>Select</option>

                                <option>Country name</option>

                            </select>



                            <label>City</label>

                            <select>

                                <option>Select</option>

                                <option>City name</option>

                            </select>



                            <label>Phone</label>

                            <input type="text" placeholder="Enter Here" class="" required>



                            <div class="clearfix"></div>

                            <input type="submit" value="Save" class="btn btn-success pull-right">

                            <input type="submit" value="Cancel" class="btn btn-danger pull-left"  data-dismiss="modal" aria-label="Close">

                            <div class="clearfix"></div>

                        </form>

                    </div>

                </div>

            </div>

        </div>

    </div>
    

<script>
	
	function expandOrder(curnt_id){
			
		$('.onlyOrder').slideDown('fast');
		$('.myOrderSec .orderPlaced.ordDtl').slideUp('fast');
		$('#order_'+ curnt_id +'.onlyOrder').slideUp('fast');
		$('#order_'+ curnt_id +'.ordDtl').slideDown('fast');
	}
	
	function closeOrder(id){
	
		$('.onlyOrder').slideDown('fast');
		$('.myOrderSec .orderPlaced.ordDtl').slideUp('fast');
	}

		
    $(document).ready(function($){
        <?php if(isset($_GET['open'])){ ?>
            var tab =  '<?php echo $_GET['open']; ?>';
            $('#currOrder').removeClass('active');
            $('#completed').addClass('active');

            $('.myOrderSec > ul > li:nth-child(1)').removeClass('active');
            $('.myOrderSec > ul > li:nth-child(2)').addClass('active');
       <?php } ?>
		
        /*$('.myOrderSec .orderPlaced.onlyOrder').click(function (e) {
            var curID = $(this).attr('id');
            $('.onlyOrder').slideDown('fast');
            $('.myOrderSec .orderPlaced.ordDtl').slideUp('fast');
            $('#'+ curID +'.onlyOrder').slideUp('fast');
            $('#'+ curID +'.ordDtl').slideDown('fast');
        })*/
		
        /*$('.myOrderSec .orderPlaced.ordDtl h4').click(function (e) {
            var curID = $(this).attr('id');
            $('.onlyOrder').slideDown('fast');
            $('.myOrderSec .orderPlaced.ordDtl').slideUp('fast');
        })*/
    });

</script>
