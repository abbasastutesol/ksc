<section class="overflow_hidden">
	<br />
	<div class="line"></div>
	<div class="container">
		<div class="addCartStat">
			<div class="cartIcon"><img src="<?php echo base_url();?>assets/frontend/images/cartYes.png" alt="Cart" height="31" width="35" /></div>
            <?php $prod_image = getProductImages($products->p_id);
				  if(!empty($prod_image))
				  {
					  $image_name = $prod_image[0][$lang.'_image'];
				  }
				  else
				  {
					  $image_name = 'no_imge.png';
				  }
			  ?>
			<div class="imgBox">	<img src="<?php echo base_url();?>uploads/images/products/<?php echo $image_name;?>" alt="Icon" height="50" width="39" /></div>
			<h2 class="title"><?php echo $products->category_name;?></h2>
			<h3><?php echo $products->product_name;?></h3>
            <?php $vars = getVariantValueById($products->varient_value_id);?>
			<h4><?php echo ($lang == 'eng' ? $vars->eng_value : $vars->arb_value);?></h4>
			<div class="pull-left">
				<p class="price"><span><?php echo $products->product_price;?></span>&nbsp;<?php echo getCurrency($lang);?></p>
				<a href="<?php echo lang_base_url();?>product" ><input type="button" class="edBtn edBtnMGreen" value="<?php echo $products_page_continue_shopping; ?>" /></a>
				<a href="<?php echo lang_base_url();?>shopping_cart" ><input type="button" class="edBtn edBtnGreen" value="<?php echo $products_page_complete_shopping; ?> <?php echo ($product_count > 0 ? '('.$product_count.' '.($product_count > 1 ? $products_page_similar_products_count : $products_page_singular_products_count).')' : '');?>" /></a>
			</div>
			<div class="clearfix"></div>
		</div>
        <?php if($customer_bought){?>
            <div class="slicEdSliderBs sevenItems black">
                <h2><?php echo $products_page_customer_bought_same; ?></h2>
                <div class="row">
                    <ul>
                        <?php 
                        foreach($customer_bought as $product){
                              $image_arr = getProductImages($product['id']);
                                    ?>
                          <li>
                              <div class="imgBox">
                                  <?php if(sizeof($image_arr) > 1){?>						
                                      <div id="Product_id_<?php echo $main;?>" class="carousel slide cntCenter" data-ride="carousel"> 															
                                          <!-- Wrapper for slides -->
                                          <div class="carousel-inner" role="listbox">
                                              <?php 
                                              $im=0;
                                              foreach($image_arr as $image){?>
                                                  <div class="item<?php if($im==0){ echo ' active';}?>"> <a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"><img src="<?php echo base_url();?>uploads/images/products/<?php echo $image[$lang.'_image']?>" alt="Product" height="199" width="129" ></a>
                                                  </div>
                                              <?php
                                              $im++; 
                                              }?>                                                   
                                          </div>
                                      </div>
                                  <?php }else{ ?>
                                      <?php if($image_arr){
                                          foreach($image_arr as $image){?>
                                          
                                         <a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"> <img src="<?php echo base_url();?>uploads/images/products/<?php echo $image[$lang.'_image']?>" alt="Product" height="199" width="129" /></a>
                                      <?php 
                                          }
                                      } else
                                      { ?>
                                         <a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"> <img src="<?php echo base_url();?>uploads/images/products/no_imge.png" alt="Product" height="199" width="129" /></a>
                                      <?php }?>
                                  <?php } ?>
                              </div>
                              <!-- Indicators -->
                              <?php if(sizeof($image_arr) > 1){?>
                                  <!--<ol class="carousel-indicators">
                                  <?php 
                                  $imo=0;
                                  foreach($image_arr as $image){?>
                                      <li data-target="#Product_id_<?php echo $main;?>" data-slide-to="<?php echo $imo;?>"<?php if($imo==0){ echo ' class="active"';}?>></li>
                                  <?php 
                                  $imo++; 
                                  }
                                  ?>
                              </ol>	-->
                              <?php }?>	
                              <h3><a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"><?php echo $product[$lang.'_name']?></a></h3>
                              <h4><?php echo getPriceCur($product[$lang.'_price'])?></h4>
                          </li>
                      <?php 
                      $main++;
                      }?>
                    </ul>
                </div>
            </div>
        <?php }?>
	</div>
	<?php $check_other = array();
		
		foreach($same_category_products as $prod){
			if($prod['id'] == $product_id)
			{
				continue;
			}
			else
			{
				$check_other[] = $prod['id'];
			}
		} 
		if($check_other)
		{
		?>
	<div class="line"></div>
    <?php }?>
	<div class="container">
    	<?php 
		
		
		if($check_other){
			?>
            <div class="productSecFive playSlide">
                <h2><?php echo $products_page_from_same_category;?></h2>
               <!-- <h2 class="noOfProd"><?php echo (sizeof($same_category_products)-1).' '.$products_page_similar_products_count; ?></h2>-->
                <div class="row">
                    <ul>
                        <?php 
						  $main=1;
						  $formId=0;
						  foreach($same_category_products as $product){
							  if($product['id'] == $product_id)
							  {
								  continue;
							  }
							  $image_arr = getProductImages($product['id']);
							  ?>
									  <li>
										  <div class="imgBox">
											  <?php if(sizeof($image_arr) > 1){?>						
												  <div id="Product_id_<?php echo $main;?>" class="carousel slide cntCenter" data-ride="carousel"> 															
													  <!-- Wrapper for slides -->
													  <div class="carousel-inner" role="listbox">
														  <?php 
														  $im=0;
														  foreach($image_arr as $image){?>
															  <div class="item<?php if($im==0){ echo ' active';}?>"> <a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"><img src="<?php echo base_url();?>uploads/images/products/<?php echo $image[$lang.'_image']?>" alt="Product" height="199" width="129" ></a>
															  </div>
														  <?php
														  $im++; 
														  }?>                                                   
													  </div>
												  </div>
											  <?php }else{ ?>
												  <?php if($image_arr){
													  foreach($image_arr as $image){?>
													  
													  <a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"><img src="<?php echo base_url();?>uploads/images/products/<?php echo $image[$lang.'_image']?>" alt="Product" height="199" width="129" /></a>
												  <?php 
													  }
												  } else
												  { ?>
													  <a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"><img src="<?php echo base_url();?>uploads/images/products/no_imge.png" alt="Product" height="199" width="129" /></a>
												  <?php }?>
											  <?php } ?>
										  </div>
										  <!-- Indicators -->
										  <?php if(sizeof($image_arr) > 1){?>
											  <ol class="carousel-indicators">
												  <?php 
												  $imo=0;
												  foreach($image_arr as $image){?>
													  <li data-target="#Product_id_<?php echo $main;?>" data-slide-to="<?php echo $imo;?>"<?php if($imo==0){ echo ' class="active"';}?>></li>
												  <?php 
												  $imo++; 
												  }
												  ?>
											  </ol>	
										  <?php }?>
										  <h3><?php echo $product[$lang.'_name']?></h3>
										 <?php  
										  if($product['has_discount_price'])
										  {
											    echo '<h4><span class="discount">'.$product[$lang.'_price'].'</span>'.getPriceCur($product[$lang.'_discount_price']).'</h4>';
										  }
										  else
										  {
											  echo '<h4>'.getPriceCur($product[$lang.'_price']).'</h4>';
										  }
										  
										  ?>
										  <div class="starAddToCart">
											  <form class="ajax_form" action="<?php echo base_url(); ?>ajax/action/" onsubmit="return false;" method="post" id="cartForm<?php echo $formId;?>">
                                                  <input type="hidden" name="form_type" value="save">
                                                  <input type="hidden" name="prod_price" id="prod_price" value="<?php echo $product[$lang.'_price'];?>">
                                                  <input type="hidden" name="product_id" id="product_id" value="<?php echo $product['id'];?>">
                                                  <input type="hidden" name="varient[]" id="varient" value="">
                                                  
                                                  <div class="addToCart"><a class="fancybox fancybox.ajax" href="<?php echo base_url();?>ajax/getProduct/<?php echo $product['id']?>"><img src="<?php echo base_url();?>assets/frontend/images/addToCartGreen.png" alt="Cart" height="15" width="17" /></a></div>
                                              </form>
											  <div class="openInNewWindow"><a href="<?php echo lang_base_url();?>product/product_page/<?php echo ($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"><img src="<?php echo base_url();?>assets/frontend/images/openLinkSep.png" alt="Open" height="15" width="17" /></a></div>
											  <div class="stars stars-example-bootstrap">
												<?php $rate = getAverageRating($product['id']);?>
                                                <select class="example-bootstrap" name="rating" autocomplete="off" onchange="rateProduct(this.value, '<?php echo $product['id']; ?>');">
                                                  <option <?php echo ($rate == 1 ? 'selected' : '');?> value="1">1</option>
                                                  <option <?php echo ($rate == 2 ? 'selected' : '');?> value="2">2</option>
                                                  <option <?php echo ($rate == 3 ? 'selected' : '');?> value="3">3</option>
                                                  <option <?php echo ($rate == 4 ? 'selected' : '');?> value="4">4</option>
                                                  <option <?php echo ($rate == 5 ? 'selected' : '');?> value="5">5</option>
                                                </select>
                                              </div>
											  <div class="clearfix"></div>
										  </div>
									  </li>
						  <?php
							  $main++;
							  $formId++; 		
								  }?>
                    </ul>
                </div>
            </div>
        <?php }?>
	</div>
</section>