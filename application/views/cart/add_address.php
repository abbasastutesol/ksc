<?php include('header.php'); ?>
<section class="">
	<div class="shopCartHeading nHeading">
		<div class="container">
			<ul>
				<li class="active"><a href="javascript:void(0);">اختيار عنوان الشحن</a></li>
			</ul>
		</div>
		<div class="line"></div>
	</div>
</section>
<section class="addAddress">
	<div class="container">
		<div class="onlyForm" style="display: none;">
			<h1>اختيار العنوان المسجل بالفعل او اضافة عنوان جديد</h1>
			<div class="formArea">
				<form action="javascript:void(0);" method="get">
					<label>اسم العنوان</label>
					<input type="text" placeholder="أكتب هنا" />

					<label>أسم الشركة العنوان</label>
					<input type="text" placeholder="أكتب هنا" />

					<label>الغنوان 1</label>
					<input type="text" placeholder="عنوان المبنى، عنوان الشارع، حي" />

					<label>الغنوان 2</label>
					<input type="text" placeholder="عدد المبنى، الطابق، شقة" />

					<label>المدينة</label>
					<input type="text" placeholder="أكتب هنا" />

					<label>المنطقة</label>
					<input type="text" placeholder="أكتب هنا" />

					<div class="row">
						<div class="col-xs-6">
							<label>الدولة</label>
							<select>
								<option>أختر</option>
							</select>
						</div>
						<div class="col-xs-6">
							<label>الرمز البريدي</label>
							<input type="text" placeholder="أكتب هنا" />
						</div>
					</div>

					<label>رقم التواصل</label>
					<input type="text" placeholder="أكتب هنا" />

					<input type="submit" class="edBtnGreen" value="اضف" />
				</form>
			</div>
		</div>
		<div class="oldAddress">
			<h1>اختيار العنوان المسجل بالفعل او اضافة عنوان جديد</h1>
			<ul>
				<li>
					<div class="whiteBox">
						<h2>Basit Chughtai</h2>
						<div class="eeeBox">
							<p>
								182-21 150th Avenue <br />
								JED 778400 <br />
								Springfield Gardens, NY 11413
							</p>
							<p>
								United States <br />
								Phone: 718-553-8740
							</p>
						</div>
						<a href="javascript:void(0);"><input type="button" class="edBtnGreen" value="أرسل الشحنة لهذا العنوان" /></a>
						<div class="clearfix"></div>
						<a href="javascript:void(0);" class="link">حذف</a>
						<a href="javascript:void(0);" class="link">تصحيح</a>
					</div>
				</li>
				<li>
					<div class="whiteBox">
						<h2>Basit Chughtai</h2>
						<div class="eeeBox">
							<p>
								182-21 150th Avenue <br />
								JED 778400 <br />
								Springfield Gardens, NY 11413
							</p>
							<p>
								United States <br />
								Phone: 718-553-8740
							</p>
						</div>
						<a href="javascript:void(0);"><input type="button" class="edBtnGreen" value="أرسل الشحنة لهذا العنوان" /></a>
						<div class="clearfix"></div>
						<a href="javascript:void(0);" class="link">حذف</a>
						<a href="javascript:void(0);" class="link">تصحيح</a>
					</div>
				</li>
				<li>
					<div class="whiteBox">
						<h2>Basit Chughtai</h2>
						<div class="eeeBox">
							<p>
								182-21 150th Avenue <br />
								JED 778400 <br />
								Springfield Gardens, NY 11413
							</p>
							<p>
								United States <br />
								Phone: 718-553-8740
							</p>
						</div>
						<a href="javascript:void(0);"><input type="button" class="edBtnGreen" value="أرسل الشحنة لهذا العنوان" /></a>
						<div class="clearfix"></div>
						<a href="javascript:void(0);" class="link">حذف</a>
						<a href="javascript:void(0);" class="link">تصحيح</a>
					</div>
				</li>
				<li>
					<div class="whiteBox">
						<h2>Basit Chughtai</h2>
						<div class="eeeBox">
							<p>
								182-21 150th Avenue <br />
								JED 778400 <br />
								Springfield Gardens, NY 11413
							</p>
							<p>
								United States <br />
								Phone: 718-553-8740
							</p>
						</div>
						<a href="javascript:void(0);"><input type="button" class="edBtnGreen" value="أرسل الشحنة لهذا العنوان" /></a>
						<div class="clearfix"></div>
						<a href="javascript:void(0);" class="link">حذف</a>
						<a href="javascript:void(0);" class="link">تصحيح</a>
					</div>
				</li>
			</ul>
			<div class="clearfix"></div>
			<div class="AddN_address">
				<a href="javascript:void(0);"><img src="images/plusIcon.png" alt="+" height="48" width="48" />أضف عنوان آخر</a>
			</div>
		</div>
		<div class="onlyForm" id="AddNewFrm">
			<h1>اختيار العنوان المسجل بالفعل او اضافة عنوان جديد</h1>
			<div class="formArea">
				<form action="javascript:void(0);" method="get">
					<label>اسم العنوان</label>
					<input type="text" placeholder="أكتب هنا" />

					<label>أسم الشركة العنوان</label>
					<input type="text" placeholder="أكتب هنا" />

					<label>الغنوان 1</label>
					<input type="text" placeholder="عنوان المبنى، عنوان الشارع، حي" />

					<label>الغنوان 2</label>
					<input type="text" placeholder="عدد المبنى، الطابق، شقة" />

					<label>المدينة</label>
					<input type="text" placeholder="أكتب هنا" />

					<label>المنطقة</label>
					<input type="text" placeholder="أكتب هنا" />

					<div class="row">
						<div class="col-xs-6">
							<label>الدولة</label>
							<select>
								<option>أختر</option>
							</select>
						</div>
						<div class="col-xs-6">
							<label>الرمز البريدي</label>
							<input type="text" placeholder="أكتب هنا" />
						</div>
					</div>

					<label>رقم التواصل</label>
					<input type="text" placeholder="أكتب هنا" />

					<input type="submit" class="edBtnGreen" value="اضف" />
				</form>
			</div>
		</div>
	</div>
</section>
<?php include('footer.php'); ?>
<script>
	$('header .headSearchSec a').removeClass('active');
	$('header .headSearchSec a:nth-of-type(2)').addClass('active');
</script>