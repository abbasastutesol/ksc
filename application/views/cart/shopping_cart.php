<section class="">
    <div class="container">
        <div class="cartHeading"><h1
                    class="pull-left"><?php echo $shopping_cart_shopping_cart; ?></h1> <?php $isLogin = $this->session->userdata('login'); ?>                <?php if ($isLogin != true) { ?>
                <a href="javascript:void(0);" data-toggle="modal" id="openLoGinPopup" data-target="#attentionLogin">
                    <button class="btn btn-black pull-right"><?php echo $box_log_in; ?></button>
                </a>                <?php } ?>
            <div class="clearfix"></div>
        </div>
        <div class="mainCart">
            <div class="whiteBox mCart_dtl haveEqHeight">
                <div class="mCartHead_top">
                    <div class="prod_img_name"><?php echo $products_page_singular_products_count; ?></div>
                    <div class="prodOth_dtl">
                        <div class="qty"><?php echo $shopping_cart_qty; ?></div>
                        <div class="price"><?php echo $products_price; ?></div>
                        <div class="total"><?php echo $shopping_cart_total; ?></div>
                        <div class="actions"><?php echo $shopping_cart_actions; ?></div>
                    </div>
                    <div class="clearfix"></div>
                </div> <?php $totalPrice = 0;
                $i = 1;
                if (count($carts) > 0) {
                    foreach ($products as $key => $product) {
                        $images = getProductImages($product['id']);
                        foreach ($images as $prod_img) {
                            if ($prod_img['is_thumbnail'] == 1) {
                                $thumbnail = $prod_img['thumbnail'];
                            }
                        }
                        $totalPrice += $total = getPriceByOffer($product['id']) * $carts[$key]['quantity']; ?>
                        <div class="mCart_row" id="product_row_<?php echo $i; ?>">
                            <div class="prod_img_name"
                                 data-text="<?php echo $products_page_singular_products_count; ?>">
                                <div class="imgBox"><img
                                            src="<?php echo base_url(); ?>uploads/images/thumbs/products/<?php echo $thumbnail; ?>"
                                            alt="img" height="147" width="147"/>
                                </div> <?php $category = getCategoriesById($product['category_id']); ?>
                                <h1><?php echo $product[$lang . '_name']; ?></h1>
                                <h3><?php echo $category[$lang . '_name']; ?></h3>
                                <p><?php echo $product[$lang . '_description']; ?></p></div>
                            <div class="prodOth_dtl">
                                <div class="qty"
                                     data-text="<?php echo $shopping_cart_qty; ?>">                                <?php $quantity = checkProdQuantity($product['id']);
                                    $priceByOffer = getPriceByOffer($product['id']);
                                    $priceByOffer = currencyRatesCalculate($priceByOffer);
                                    if ($quantity == 0) { ?>                                <select
                                            id="prod_drp_<?php echo $i; ?>" class="selQty"
                                            onchange="calculateOrder(this.value, '<?php echo $i; ?>', '<?php echo $priceByOffer['rate']; ?>', <?php echo $carts[$key]["id"]; ?>, '<?php echo $priceByOffer["rate"]; ?>');">                                  <?php for ($j = 1; $j <= intval($carts[$key]['quantity']); $j++) { ?>
                                            <option value="<?php echo $j; ?>" <?php echo($carts[$key]['quantity'] == $j ? 'selected' : ''); ?>><?php echo $j; ?></option>                                  <?php } ?>
                                    </select>                                <?php } else {
                                        $priceByOffer = getPriceByOffer($product['id']);
                                        $priceByOffer = currencyRatesCalculate($priceByOffer); ?><?php $total_quantity = checkProdQuantity($product['id']); ?>
                                        <select id="prod_drp_<?php echo $i; ?>" class="selQty"
                                                onchange="calculateOrder(this.value, '<?php echo $i; ?>', '<?php echo $priceByOffer['rate']; ?>', <?php echo $carts[$key]['id']; ?>, '<?php echo getCurrency($lang); ?>');">                                                                                                                <?php /*$total_quantity*/									for($k=1; $k<=100; $k++){																						echo "<option value=".$k." ".($carts[$key]['quantity'] == $k ? 'selected' : '').">".$k."</option>";																					}?>                                    </select>                                <?php } ?>
                                </div> <?php $gProdPrice = getPriceByOffer($product['id']);
                                $gProdPrice = currencyRatesCalculate($gProdPrice);
                                $total = currencyRatesCalculate($total); ?>
                                <div class="price" id="item_price_<?php echo $i; ?>"
                                     data-text="<?php echo $products_price; ?>"><?php echo $gProdPrice['rate']; ?>
                                    <span><?php echo $gProdPrice['unit']; ?></span></div>
                                <div class="total total_product_price" data-text="<?php echo $shopping_cart_total; ?>"
                                     data-price="<?php echo $total['rate']; ?>"><span
                                            id="item_totel_price_<?php echo $i; ?>"><?php echo $total['rate']; ?></span> <?php echo $total['unit']; ?>
                                </div>
                                <input type="hidden" value="<?php echo $total; ?>"
                                       id="item_totel_price_val_<?php echo $i; ?>"/>
                                <div class="actions" data-text="<?php echo $shopping_cart_actions; ?>"><a
                                            href="javascript:void(0);"
                                            onclick="delFromCart(<?php echo $carts[$key]['id']; ?>, <?php echo $i; ?>)"
                                            ; class="red"><?php echo $shopping_cart_delete_product; ?></a> <a
                                            href="javascript:void(0);" class="green"
                                            onclick="saveForLater('<?php echo $carts[$key]['id']; ?>', '1')"><?php echo $shopping_cart_save_for_later; ?></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>                    <?php $i++;
                    }
                } else {
                    $no_records;
                } ?>                </div>
            <div class="whiteBox mCart_price haveEqHeight"><p><?php echo $shopping_cart_discount_coupon; ?></p>
                <div class="mCart_voucher">
                    <form action="<?php echo lang_base_url() . 'ajax/voucherCode'; ?>" method="post"
                          class="voucher_code" onsubmit="return false;"><input type="hidden" name="cart_total"
                                                                               id="cart_total"
                                                                               value=""/> <?php $cartTotalPrice = currencyRatesCalculate($totalPrice); ?>
                        <input type="hidden" name="cart_total_val" id="cart_total_val"
                               value="<?php echo $cartTotalPrice['rate']; ?>"/> <?php foreach ($products as $key => $product) { ?>
                            <input type="hidden" name="product_ids[]"
                                   value="<?php echo $product['id']; ?>"/>                                                        <?php } ?>
                        <input type="text" id="voucher_code" name="voucher_code"
                               value="<?php echo(get_cookie('voucher_code') ? get_cookie('voucher_code') : ''); ?>"
                               placeholder="<?php echo $shopping_cart_coupon_enter_here; ?>"/> <input type="submit"
                                                                                                      class="btn btn-black couponAppliedBtn"
                                                                                                      value="<?php echo $shopping_cart_apply; ?>"/>
                        <!-- You can change button to submit -->
                        <div class="clearfix"></div>
                    </form>
                </div>
                <div class="mCart_form">
                    <ul class="extras">                         <?php $g = 1;
                        $giftIds = unserialize(get_cookie('giftIds'));
                        foreach ($gifts as $gift) { ?>
                            <li>                                <?php $gift_amount = currencyRatesCalculate($gift['amount']); ?>
                                <input id="giftBox<?php echo $g; ?>"
                                       class="giftCard" <?php echo(in_array($gift['id'], $giftIds) ? 'checked' : ''); ?>
                                       value="<?php echo $gift['id'] . '|' . $gift_amount['rate']; ?>" type="checkbox"
                                       name="extras" value="1"> <label
                                        for="giftBox"><span>&nbsp;</span></label> <?php echo $gift[$lang . '_name']; ?>
                                , <?php echo $gift_amount['rate']; ?> <?php echo $gift_amount['unit']; ?>
                                (<?php echo $checkout_shipping_address_extra; ?>)
                            </li>                         <?php $g++;
                        } ?>
                        <!--<li>                                <input  id="greetingCard" type="checkbox" name="extras" value="1" >                                <label for="greetingCard"><span>&nbsp;</span></label>                                Add a greeting card, 5 SR (Extra)                            </li>-->
                    </ul> <?php if (getCouponPrice($totalPrice) == 0) {
                        $disDisplay = 'none;';
                    } else {
                        $disDisplay = 'block;';
                    } ?>                        <?php $ifcDiscount = getCouponPrice($totalPrice);
                    $originalTotal = $totalPrice;                            /*calculate loyalty discount with currency rates changing*/
                    $loyalty_discount = loyaltyDiscount($totalPrice, $ifcDiscount);
                    if ($loyalty_discount['rate'] > 0) {
                        $totalPrice = $totalPrice - $loyalty_discount['rate'];                            /*======================================================*/ ?>
                        <p><?php echo $loyalty_discount_label; ?> </p>                        <h3
                                class="price loyalti_price_total"><span
                                    id="loyalti_price_total"><?php echo $loyalty_discount['rate']; ?></span> <span
                                    class="curr"><?php echo getCurrency($lang); ?></span>
                        </h3>                        <?php } ?>
                    <div class="discountPriceShow" style="display:<?php echo $disDisplay; ?>">
                        <p><?php echo $shopping_car_old_price; ?> </p> <?php $getGiftCartTotal = $originalTotal + getGiftCarTotal();
                        $getGiftCartTotal = currencyRatesCalculate($getGiftCartTotal); ?> <h3
                                class="price old_price_total"><span
                                    id="old_price_total"><?php echo $getGiftCartTotal['rate']; ?></span> <span
                                    class="curr"><?php echo $getGiftCartTotal['unit']; ?></span></h3>
                        <input type="hidden" id="old_price_total_val" value="<?php echo $getGiftCartTotal['rate']; ?>">
                        <p><?php echo $shopping_car_discount_price; ?> </p> <?php $getTotalPrice = getCouponPrice($originalTotal);
                        $getTotalPrice = currencyRatesCalculate($getTotalPrice); ?> <h3
                                class="price discount_price_total"><span
                                    id="discount_price_total"><?php echo $getTotalPrice['rate']; ?></span> <span
                                    class="curr"><?php echo $getTotalPrice['unit']; ?></span></h3></div>
                    <p><?php echo $shopping_cart_total_cost; ?>
                        <span>(<?php echo count($products); ?> <?php echo $products_page_similar_products_count; ?>
                            )</span></p> <?php $getGrandTotalPrice = getCouponTotal($totalPrice) + getGiftCarTotal();
                    $getGrandTotalPrice = currencyRatesCalculate($getGrandTotalPrice); ?> <h3
                            class="price total_prod_price_sum"><span
                                id="total_prod_price_sum"><?php echo $getGrandTotalPrice['rate']; ?></span> <span
                                class="curr"><?php echo $getGrandTotalPrice['unit']; ?></span></h3>
                    <input type="hidden" value="<?php echo $getGrandTotalPrice['rate']; ?>"
                           id="total_prod_price_sum_val"/> <a
                            href="<?php echo lang_base_url() . 'shopping_cart/shippingAddress'; ?>">
                        <button class="btn btn-success"><i
                                    class="sprite_ioud ioudSpcart"></i> <?php echo $shopping_cart_proceed_to_checkout_btn; ?>
                        </button>
                    </a></div>
                <div class="mCartCont_btn"><a href="<?php echo lang_base_url() . 'product'; ?>"><input type="button"
                                                                                                       class="btn btn-default"
                                                                                                       value="<?php echo $checkout_continue_shopping_btn; ?>"/></a>
                </div>
            </div> <?php if ($saved_carts) { ?>
                <div class="whiteBox mCart_dtl savLatter">
                    <div class="mCartHead_top">
                        <div class="prod_img_name"><?php echo $shopping_cart_saved_products; ?></div>
                        <div class="prodOth_dtl">
                            <div class="price"><?php echo $products_price; ?></div>
                            <div class="actions"><?php echo $shopping_cart_actions; ?></div>
                        </div>
                        <div class="clearfix"></div>
                    </div> <?php $totalPrice = 0;
                    $i = 1;
                    foreach ($saved_products as $key => $product) {
                        $images = getProductImages($product['id']);
                        foreach ($images as $prod_img) {
                            if ($prod_img['is_thumbnail'] == 1) {
                                $thumbnail = $prod_img['thumbnail'];
                            }
                        }
                        $totalPrice += $total = $product[$lang . '_price'] * $saved_carts[$key]['quantity']; ?>
                        <div class="mCart_row" id="product_row_<?php echo $i; ?>">
                            <div class="prod_img_name" data-text="Product">
                                <div class="imgBox"><img
                                            src="<?php echo base_url(); ?>uploads/images/thumbs/products/<?php echo $thumbnail; ?>"
                                            alt="img" height="147" width="147"/>
                                </div> <?php $category = getCategoriesById($product['category_id']); ?>
                                <h1><?php echo $product[$lang . '_name']; ?></h1>
                                <h3><?php echo $category[$lang . '_name']; ?></h3> <?php echo $product[$lang . '_description']; ?>
                            </div>
                            <div class="prodOth_dtl">                                    <?php $prodPrice = currencyRatesCalculate($product[$lang . '_price']); ?>
                                <div class="price" id="item_price_<?php echo $i; ?>"
                                     data-text="price"><?php echo $prodPrice['rate']; ?>
                                    <span><?php echo $prodPrice['unit']; ?></span></div>
                                <div class="actions" data-text="actions"><a href="javascript:void(0);"
                                                                            onclick="delFromCart(<?php echo $saved_carts[$key]['id']; ?>, <?php echo $i; ?>)"
                                                                            ;
                                                                            class="red"><?php echo $shopping_cart_delete_product; ?></a>
                                    <a href="javascript:void(0);" class="red"
                                       onclick="saveForLater('<?php echo $saved_carts[$key]['id']; ?>', '0')"><?php echo $shopping_cart_move_to_cart; ?></a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>                                    <?php $i++;
                    } ?>                        </div>
                <div class="clearfix"></div>                <?php } ?>            </div>
    </div>
</section>