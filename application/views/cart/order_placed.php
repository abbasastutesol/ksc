
<style>
    @media print
    {
        footer
        {
            display: none !important;
        }
        header
        {
            display: none !important;
        }
        .order_placed_print{
            margin-top: 200px;
        }
    }
</style>
    <section class="order_placed_print">
        <div class="container container1070">
            <div class="cartHeading fontWhtReg">
                <h1 class=""><?php echo $my_order_placed_successfully;?></h1>
                <div class="clearfix"></div>
            </div>
            <div class="orderPlaced ">
                <div class="printIcons">
                    <a href="javascript:void(0)" onclick="window.open('<?php echo lang_base_url();?>my_orders/printPage/<?php echo $ord_id;?>');" target="_blank">
                        <i class="fa fa-print" aria-hidden="true"></i>
                        <br />
                        <?php echo $my_order_print;?>
                    </a>
                    <a href="#." data-toggle="modal" data-target="#send_order_email">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                        <br />
                        <?php echo $chat_email;?>
                    </a>
                </div>
                <h4><?php echo $place_feedback2_order_number;?> <strong><?php echo $order_id;?></strong></h4>
                <?php /*?><p><?php echo getShipmentDays($order_detail->shipment_group,$order_detail->shipment_type); ?>:</p><?php */?>
<!--                <p>Will be delivered in <strong>2 Business days  </strong> Approximately to:</p>-->
                <strong><?php echo $user->address_title; ?></strong>

                <!--<br />
                82-21 150th
                <br />
                <p>JED 778400</p>
                Springfield Gardens, NY 11413
                <p>United States</p>
                Phone: 718-553-8740-->
                <br>
                <p><?php echo $user->address_1; ?></p>
                <p><?php echo getCoutryByCode($user->country).", ".getCityById($user->city); ?></p>
                <?php echo $phone_label;?>: <span class="numberArb"><?php echo $user->phone_no; ?></span>
                <p class="text-right"><span>(<?php echo count($products); ?> <?php echo $products_page_similar_products_count; ?>)</span></p>
                <table class="table">
                    <tbody>

                    <?php
                    $total = 0;
                    $totalQuan = 0;
                    foreach ($carts as $key => $product) {
                    $images =  getProductImages($product['product_id']);
                    foreach($images as $prod_img){
                        if($prod_img['is_thumbnail'] == 1){
                            $thumbnail = $prod_img['thumbnail'];
                        }
                    }
                    //echo "<pre>"; print_r($product['quantity']); exit;
                    $total += $product['price']*$product['quantity'];
                    $totalQuan += $product['quantity'];
                        $loyaltyPopupTotal = $total;
                    ?>
                    <tr>
                        <td class="imgBox text-center" style="position: relative;"><img src="<?php echo base_url(); ?>uploads/images/thumbs/products/<?php echo $thumbnail; ?>" alt="Pro"  width="111" height="144">
                        </td>
                        <?php
                            $prodDetail = getProductDetailById($product['product_id']);
                        ?>
                        <td class="text-left"><h3><?php echo $prodDetail[$lang.'_name']; ?></h3></td>
                        <td class="text-right"><?php echo $shopping_cart_qty;?>: <span> <?php echo $product['quantity']; ?></span></td>
                        <?php
                        $convertPrice = currencyRatesCalculate($product['price'],$product['currency_rate'],$order_detail->id);
                        ?>
                        <td class="text-right"><?php echo $convertPrice['rate']; ?> <span> <?php echo getCurrency($lang);?></span></td>
                    </tr>
                  <?php } ?>

                    </tbody>
                    <tfoot>
                    <tr>
                        <th class="text-right" colspan="3">
                            <?php /*?><?php echo getShipmentDays($order_detail->shipment_group,$order_detail->shipment_type); ?><?php */?>
							<?php if($order_detail->shipment_type == 'normal'){ echo $checkout_shipping_address_normal_shipping; }elseif($order_detail->shipment_type == 'fast'){ echo $checkout_shipping_address_fast_shipping; }elseif($order_detail->shipment_type == 'aramex') {echo $aramex_shipment; } else{ echo $checkout_shipping_address_free_shipping; }?>
                        </th>
                        <?php $shipPrice = currencyRatesCalculate($order_detail->shipment_price,$order_detail->currency_rate,$order_detail->id)?>
                        <th class="text-right"><?php echo $shipPrice['rate']; ?> <span> <?php echo getCurrency($lang);?></span></th>
                    </tr>
                    <?php if($order_detail->coupon_id > 0){
						$coupon = getCouponDiscountDb($order_detail->coupon_id);
						?>
                        <tr>
                            <th class="text-right" colspan="3"><?php echo $shopping_cart_voucher_code;?></th>
                            <th class="text-right"><?php if($coupon->type == 1)
								{
									echo $coupon->discount.'%';
								}
								else
								{
                                    $coupon_discount = currencyRatesCalculate($coupon->discount);
									echo $coupon_discount['rate'].' '.$coupon_discount['unit'];
								}?>
                            </th>
                        </tr>
                    <?php }?>


                    <?php if($order_detail->loyalty_discount != '' && $order_detail->loyalty_discount > 0){

                        ?>
                        <tr>
                            <th class="text-right" colspan="3"><?php echo $loyalty_discount_label;?></th>
                            <th class="text-right">
                                <?php

                                $loyaltyDiscount = currencyRatesCalculate($order_detail->loyalty_discount,$order_detail->currency_rate,$order_detail->id);
                                echo $loyaltyDiscount['rate'].' '.$loyaltyDiscount['unit'];
                                ?>
                            </th>
                        </tr>
                    <?php }?>

                    <?php $gifts = getGiftCarDetailDb($order_detail->id);


						if($gifts){

                            foreach($gifts as $gift)
                            {
                                $gift_amount = currencyRatesCalculate($gift['amount']);
                        ?>
                        <tr>
                            <th class="text-right" colspan="3"><?php echo $gift[$lang.'_name'];?></th>
                            <th class="text-right"><?php echo $gift_amount['rate'];?> <?php echo $gift_amount['unit'];?></th>
                        </tr>
                    <?php 	}
                          }
							?>
                    <?php if($order_detail->payment_method == "Cash On Delivery"){
						?>
                        <tr>
                            <th class="text-right" colspan="3"><?php echo $my_order_cod_label;?></th>
                            <?php
                            $extra_charges = currencyRatesCalculate($order_detail->extra_charges,$order_detail->currency_rate,$order_detail->id);
                            ?>
                            <th class="text-right"><?php echo $extra_charges['rate'];?> <?php echo $extra_charges['unit'];?>
                            </th>
                        </tr>
                    <?php }?>

                    <?php
                        /*aramex tracking id calculate*/
                        if($order_detail->tracking_id != ""){
                        ?>
                        <tr>
                            <th class="text-right" colspan="3">
                                <?php echo $aramex_track_no; ?>
                            </th>

                            <th class="text-right">
                                <?php echo $order_detail->tracking_id;?>
                            </th>
                        </tr>
                    <?php }
                    ?>
                    </tfoot>
                </table>
                <?php if(strtolower($order_detail->payment_method) == "visa"){
                    $payment_img = base_url().'assets/frontend/images/visa_payment.png';
                }
                elseif(strtolower($order_detail->payment_method) == "cash on delivery"){
                    $payment_img = base_url().'assets/frontend/images/cash_payment.png';
                }elseif(strtolower($order_detail->payment_method) == "bank_transfer"){
                    $payment_img = base_url().'assets/frontend/images/wire_payment.png';
                }elseif(strtolower($order_detail->payment_method) == "sadad"){
                    $payment_img = base_url().'assets/frontend/images/sadad_payment.png';
                }else{
                    $payment_img = base_url().'assets/admin/assets/img/master.png';
                }

                ?>

                <h2 class="price text-right">
                    <span class="pull-left"><?php echo $grand_total_label;?></span>
                    <img src="<?php echo $payment_img; ?>" alt="<?php echo $order_detail->payment_method; ?>"  />
                    <?php
                    $convertTotalPrice = currencyRatesCalculate($order_detail->total_amount,$order_detail->currency_rate,$order_detail->id);
                    ?>
                    <?php echo $convertTotalPrice['rate'];
					//number_format(getCouponTotalDb($total, $order_detail->coupon_id)+$order_detail->shipment_price+getGiftCarTotalDb($ord_id),'2','.',''); ?>
                    <span class="curr"><?php echo getCurrency($lang);?></span>
                </h2>
                <div class="clearfix"></div>
            </div>

            <input id="loyaltyPopupTotal" type="hidden" value="<?php echo $loyaltyPopupTotal; ?>">
        </div>
    </section>

<?php
//if($_SERVER['REMOTE_ADDR'] == "116.58.71.82") {

    //echo "<pre>"; print_r($trackId);
//}
?>