<section class="">
	<div class="shopCartHeading nHeading">
		<div class="container">
			<div class="max1010">
				<ul>
					<li class="active"><a href="javascript:void(0);"><?php echo $checkout_personal_info_address;?></a></li>
				</ul>
			</div>
		</div>
		<div class="line"></div>
	</div>
</section>
<section class="addAddress">
	<div class="container">
		<div class="paymentSteps">
			<div class="whiteBox">
				<div class="max1010">
					<!--<h1>يرجى تحديد طريقة الدفع</h1>-->
					<div class="eeeBox">
						<ul class="payOptions addNewCardAtCheckout">
							<li class="title"><?php echo $login_payment_methods;?></li>
							<li>
								<input id="checkbox_1" type="radio" name="order_idsED" value="cc_merchantpage" checked="checked" ><label for="checkbox_1"><span><span></span></span></label>
								<img src="<?php echo base_url()?>assets/frontend/images/payOptions_VM.png" alt="Visa Master Card" height="22" width="65" />
							</li>
							<li>
								<input id="checkbox_2" type="radio" name="order_idsED" value="sadad"><label for="checkbox_2"><span><span></span></span></label>
								<img src="<?php echo base_url()?>assets/frontend/images/payOptions_Sadat.png" alt="" height="22" width="53" />
							</li>
							<li>
								<input id="checkbox_3" type="radio" name="order_idsED" value="COD"><label for="checkbox_3"><span><span></span></span></label>
								<img src="<?php echo base_url()?>assets/frontend/images/payOptions_Cash.png" alt="Cash On Delivery"  height="22" width="36" />
								<!--الدفع عن الاستلام-->
							</li>
						</ul>
					</div>
					<div class="max862 hideAllMethod" id="visaCard">
						<div class="masterCardBox">
							<div class="oldAddress">
								<div class="imgBox">
									<img src="<?php echo base_url()?>assets/frontend/images/rc2_img_2.png" alt="Master Card" height="30" width="110" />
								</div>
								<ul>
                                <?php
									$ch = 1;
									foreach($cards as $card){?>
									<li>
										<div class="whiteBox">
											<h2><?php echo $card['customer_name']; ?></h2>
											<div class="eeeBox" >
												<div class="CardImg"><img src="<?php echo base_url()?>assets/frontend/images/masterCard.png" alt="Master" height="22" width="33" /> </div>
												<p>
                                                	<input type="hidden" value="<?php echo $card['id']; ?>" name="card_method_old" />
													<?php echo $card['card_number']; ?><br>
													<?php echo $checkout_expiry_date;?>: <?php echo $card['expiry_date']; ?><br>
                                                    <?php if($lang == 'eng'){?>
														<?php echo $checkout_cvc;?>: *****
													<?php }else
													{?>
                                                    	***** :<?php echo $checkout_cvc;?>
                                                    <?php }?>
                                                </p>
											</div>
											<a href="javascript:void(0);" onclick="oldCardsData('<?php echo $card['id']?>')"><input class="edBtnGreen" value="<?php echo $checkout_choose_account;?>" type="button"></a>
											<div class="clearfix"></div>
											<a href="javascript:void(0);" onclick="deleteCard('<?php echo $card['id'];?>', '<?php echo $delete_confirm_general;?>');" class="link"><?php echo $shopping_cart_delete_product;?></a>
										</div>
									</li>
                                    <?php
										$ch++;
									}?>
								</ul>
								<div class="clearfix"></div>
								<div class="AddN_address addPaymentPage">
									<a href="javascript:void(0);" class="payProcess"><img src="<?php echo base_url()?>assets/frontend/images/plusIcon.png" alt="+" width="48" height="48"><?php echo $shipping_address1_add_another_address;?></a>
								</div>
							</div>
						</div>
						<div class="vmCardForm ">
							<div class="vmElements">
								 <section class="merchant-page-iframe">
									<?php
                                    $objFort = new PayfortCheckout();
                                    $merchantPageData = $objFort->getMerchantPageData();
                                    $postData = $merchantPageData['params'];
                                    $gatewayUrl = $merchantPageData['url'];
                                    ?>
                                    <div class="cc-iframe-display">
                                        <div id="div-pf-iframe" style="display:none">
                                            <div class="pf-iframe-container">
                                                <div class="pf-iframe" id="pf_iframe_content">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
							</div>
						</div>
                        
					</div>
                    <div class="sadatForm hideAllMethod" style="display:none;" id="sadadMethod">
                        <p>
                            <img src="<?php echo base_url()?>assets/frontend/images/payOptions_Sadat.png" alt="Sadat" height="22" width="53" />                           
                        </p>
                        <a href="javascript:void(0);" class="payProcess"><input type="button" value="<?php echo $checkout_pay;?>" class="edBtnGreen" /></a>
                    </div>
                    
                    <div class="sadatForm hideAllMethod" style="display:none;" id="codMethod">
                        <p>
                            <img src="<?php echo base_url()?>assets/frontend/images/payOptions_Cash.png" alt="Sadat" height="22" width="36" />                           
                        </p>
                        <a href="javascript:void(0);" class="payProcess"><input type="button" value="<?php echo $checkout_submit;?>" class="edBtnGreen" /></a>
                    </div>
					<input type="hidden" id="card_method_new" name="card_method_new" value="cc_merchantpage">
                    <input type="hidden" id="method_not_selected" value="0">
                    <input type="hidden" id="user_name" value="<?php echo $user->first_name;?>">
                    <input type="hidden" id="user_email" value="<?php echo $user->email;?>">
					<input type="hidden" name="total_amount" id="total_amonut" value="<?php echo $total_prod_amount; ?>">

				</div>
			</div>
		</div>
	</div>
</section>