<section class="overflow_hidden">
	<br />
	<div class="line"></div>
	<div class="container">
		<div class="thankuSec">

			<h2>  <img src="<?php echo base_url();?>assets/frontend/images/greenTick.png" alt="" height="19" width="23" />   <?php echo $checkout_thankyou_message;?></h2>
			<div class="tnkBox">
				<h3><?php echo $my_orders_order_id;?>: 	<strong><?php echo $order_id?></strong></h3>
				<p> <?php echo $checkout_item_ship_to.' '.$product_count.' '.$checkout_to.' '.$full_name;?></p>
				<p>  <?php echo $checkout_delivery;?>: <?php echo dateFormat(date('Y-m-d'))?> - <?php echo dateFormat(date('Y-m-d', strtotime("+".$delivery_time." days")))?></p>
				<!--<p><strong>مراجعة أو تعديل طلبك</strong></p>-->
				<a href="javascript:void(0);" class="printIt"><img src="<?php echo base_url();?>assets/frontend/images/printIco.png" alt="Print" height="28" width="29" /> </a>
			</div>
		</div>
		<?php
		 if($customer_bought){?>
            <div class="slicEdSliderBs sevenItems black">
                <h2><?php echo $products_page_customer_bought_same; ?></h2>
                <div class="row">
                    <ul>
                        <?php 
                        foreach($customer_bought as $product){
                              $image_arr = getProductImages($product['id']);
                                    ?>
                          <li>
                              <div class="imgBox">
                                  <?php if(sizeof($image_arr) > 1){?>						
                                      <div id="Product_id_<?php echo $main;?>" class="carousel slide cntCenter" data-ride="carousel"> 															
                                          <!-- Wrapper for slides -->
                                          <div class="carousel-inner" role="listbox">
                                              <?php 
                                              $im=0;
                                              foreach($image_arr as $image){?>
                                                  <div class="item<?php if($im==0){ echo ' active';}?>"> <a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"><img src="<?php echo base_url();?>uploads/images/products/<?php echo $image[$lang.'_image']?>" alt="Product" height="199" width="129" ></a>
                                                  </div>
                                              <?php
                                              $im++; 
                                              }?>                                                   
                                          </div>
                                      </div>
                                  <?php }else{ ?>
                                      <?php if($image_arr){
                                          foreach($image_arr as $image){?>
                                          
                                         <a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"> <img src="<?php echo base_url();?>uploads/images/products/<?php echo $image[$lang.'_image']?>" alt="Product" height="199" width="129" /></a>
                                      <?php 
                                          }
                                      } else
                                      { ?>
                                         <a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"> <img src="<?php echo base_url();?>uploads/images/products/no_imge.png" alt="Product" height="199" width="129" /></a>
                                      <?php }?>
                                  <?php } ?>
                              </div>
                              <!-- Indicators -->
                              <?php if(sizeof($image_arr) > 1){?>
                                  <!--<ol class="carousel-indicators">
                                  <?php 
                                  $imo=0;
                                  foreach($image_arr as $image){?>
                                      <li data-target="#Product_id_<?php echo $main;?>" data-slide-to="<?php echo $imo;?>"<?php if($imo==0){ echo ' class="active"';}?>></li>
                                  <?php 
                                  $imo++; 
                                  }
                                  ?>
                              </ol>	-->
                              <?php }?>	
                              <h3><a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"><?php echo $product[$lang.'_name']?></a></h3>
                              <h4><?php echo getPriceCur($product[$lang.'_price'])?></h4>
                          </li>
                      <?php 
                      $main++;
                      }?>
                    </ul>
                </div>
            </div>
        <?php }?>
	</div>
</section>