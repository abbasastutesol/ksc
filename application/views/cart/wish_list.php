<section>
	<!-- Tab panes -->
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane tableArea active" id="step_1">
			<div class="container max960_cont">
				<div class="step_one_table">
					<table class="table">
						<thead>
							<tr>
								<th colspan="2"><h2><?php echo $wishlist_title;?></h2></th>	
								<th><a href="javascript:void(0);"><?php echo $products_price;?></a></th>
							</tr>
						</thead>
						<tbody>
                        	<?php
							 if(!empty($products))
							 {
								 foreach($products as $product){
									 $product_price = $product[$lang.'_price'];
									 ?>
									<tr id="prod_tr_<?php echo $i; ?>">
                                    <?php $prod_image = getProductImages($product['id']);
											if(!empty($prod_image))
											{
												$image_name = $prod_image[0][$lang.'_image'];
											}
											else
											{
												$image_name = 'no_imge.png';
											}								
									?>
										<td class="imgBox"><a href="<?php echo lang_base_url();?>product/product_page/<?php echo $product['id']; ?>" ><img src="<?php echo base_url();?>uploads/images/products/<?php echo $image_name;?>" alt="img" /></a></td>
										<td>
											<h3><?php echo $product[$lang.'_category_name'];?></h3>
											<a href="<?php echo lang_base_url();?>product/product_page/<?php echo $product['id']; ?>" ><?php echo $product[$lang.'_name'];?></a>
										</td>
										<td><strong><?php echo $product[$lang.'_price'];?></strong> <?php echo getPriceCur($product[$lang.'_price'], true);?></td>
									</tr>
								<?php 
								} 
							 } else
							 {
								 echo '<tr><td colspan="4">'.$no_records.'</td></tr>';
							 }?>
						</tbody>
					</table> 
				</div>
			</div>
			<div class="line"></div>
			<div class="container">
		<div class="productSecFive playSlide green">
			<h2><?php echo $products_recently_viewed_products;?></h2>
			<div class="row">
				<ul>
					<?php 
					$recently_viewed = getRecentViewed();
					foreach($recently_viewed as $product){
						  $image_arr = getProductImages($product['id']);
								?>
					  <li>
						  <div class="imgBox">
							  <?php if(sizeof($image_arr) > 1){?>						
								  <div id="Product_id_<?php echo $main;?>" class="carousel slide cntCenter" data-ride="carousel"> 															
									  <!-- Wrapper for slides -->
									  <div class="carousel-inner" role="listbox">
										  <?php 
										  $im=0;
										  foreach($image_arr as $image){?>
											  <div class="item<?php if($im==0){ echo ' active';}?>"> <a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"><img src="<?php echo base_url();?>uploads/images/products/<?php echo $image[$lang.'_image']?>" alt="Product" height="199" width="129" ></a>
											  </div>
										  <?php
										  $im++; 
										  }?>                                                   
									  </div>
								  </div>
							  <?php }else{ ?>
								  <?php if($image_arr){
									  foreach($image_arr as $image){?>
									  
									 <a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"> <img src="<?php echo base_url();?>uploads/images/products/<?php echo $image[$lang.'_image']?>" alt="Product" height="199" width="129" /></a>
								  <?php 
									  }
								  } else
								  { ?>
									 <a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"> <img src="<?php echo base_url();?>uploads/images/products/no_imge.png" alt="Product" height="199" width="129" /></a>
								  <?php }?>
							  <?php } ?>
						  </div>
						  <!-- Indicators -->
						  <?php if(sizeof($image_arr) > 1){?>
							  <ol class="carousel-indicators">
							  <?php 
							  $imo=0;
							  foreach($image_arr as $image){?>
								  <li data-target="#Product_id_<?php echo $main;?>" data-slide-to="<?php echo $imo;?>"<?php if($imo==0){ echo ' class="active"';}?>></li>
							  <?php 
							  $imo++; 
							  }
							  ?>
						  </ol>	
						  <?php }?>	
						  <h3><a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"><?php echo $product[$lang.'_name']?></a></h3>
						  <?php  
									  if($product['has_discount_price'])
									  {
										   echo '<h4><span class="discount">'.$product[$lang.'_price'].'</span>'.getPriceCur($product[$lang.'_discount_price']).'</h4>';
									  }
									  else
									  {
										  echo '<h4>'.getPriceCur($product[$lang.'_price']).'</h4>';
									  }
									  
									  ?>
					  </li>
				  <?php 
				  $main++;
				  }?>
				</ul>
			</div>
		</div>
	</div>
		</div><!-- step one END -->
	</div>	
</section>


<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <?php echo $checkout_terms_conditions;?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    </div>
  </div>
</div>