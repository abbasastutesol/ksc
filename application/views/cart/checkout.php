

    <section class="">

        <div class="container">

            <div class="cartHeading">

                <h1 class="pull-left"><?php if($lang == 'eng') { echo $products_page_complete_shopping.','; } ?> <span><?php echo $login_payment_methods;?></span></h1>

               <?php $isLogin = $this->session->userdata('login');

                ?>

                <?php

                if($isLogin != true) {

                ?>
               
               
                <a href="javascript:void(0);"  data-toggle="modal" data-target="#attentionLogin">

                    <button class="btn btn-black pull-right"><?php echo $box_log_in;?></button>

                </a>
                
                 <?php } ?>

                <div class="clearfix"></div>

            </div>

            <div class="ckMain_box">

                <div class="payMethodOpt whiteBox">

                    <!-- Nav tabs -->

                    <ul class="nav nav-tabs" role="tablist">



                        <?php $method = payment_methods();
						/*echo '<pre>';
						print_r($methods);
						die();*/

                        //foreach($methods as $method) {

                        ?>

                            <?php if($method->visa == '1' && in_array('1', $payments_allow)) { ?>

                                <li role="presentation" class="active">

                                    <a href="#payTab_1" aria-controls="payTab_1" class="payment_method" data-back-btn-text="vis" role="tab" data-toggle="tab"><i class="payOptBtn sprite_1"></i></a>

                                </li>

                            <?php }

                            if($method->sadad == '1' && in_array('2', $payments_allow)) {  ?>



                                <li role="presentation" <?php echo ($method->visa != 1 ? 'class="active"' : '')?>>

                                    <a href="#payTab_2" aria-controls="payTab_2" class="payment_method" data-back-btn-text="sadad" role="tab" data-toggle="tab"><i class="payOptBtn sprite_2"></i></a>

                                </li>

                             <?php }

                            if($method->transfer == '1' && in_array('4', $payments_allow)) { ?>



                                <li role="presentation" <?php echo ($method->visa != 1 && $method->sadad != 1 ? 'class="active"' : '')?>>

                                    <a href="#payTab_3" aria-controls="payTab_3" class="payment_method" data-back-btn-text="bank_transfer" role="tab" data-toggle="tab"><i class="payOptBtn sprite_3"></i></a>

                                </li>

                            <?php }

                            if($method->cash_on_delivery == '1' && in_array('3', $payments_allow)) {

                            ?>



                            <li role="presentation" <?php echo ($method->visa != 1 && $method->sadad != 1 && $method->transfer != 1 ? 'class="active"' : '')?>>

                                <a href="#payTab_4" aria-controls="payTab_4" class="payment_method" data-back-btn-text="COD" role="tab" data-toggle="tab">
                                <!--<i class="payOptBtn sprite_4"></i>-->
                                <img src="<?php echo base_url();?>assets/frontend/images/payOptBtnNew.jpg" alt="COD" height="42" width="150" />
                                </a>

                            </li>



                       <?php }



                       //}

                        ?>

                    </ul>



                    <!-- Tab panes -->

                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane <?php echo ($method->visa == 1  && in_array('1', $payments_allow) ? 'active' : '')?>" id="payTab_1">

                            <i class="fa fa-lock" aria-hidden="true"></i>

                            <br />

                            <p><?php echo ($lang == 'eng' ? $method->eng_description : $method->arb_description);?></p>
							<?php $setting = getSettings();?>
                            <div class="payOptForm">
								
                                <div class="PT_express_checkout" style="height:500px;"></div>
                            </div>

                        </div>

                        <div role="tabpanel" class="tab-pane <?php echo ($method->visa != 1 && $method->sadad == 1 && in_array('2', $payments_allow) ? 'active' : '')?>" id="payTab_2">

                            <i class="fa fa-lock" aria-hidden="true" ></i>

                            <?php echo ($lang == 'eng' ? $method->eng_sadad_description : $method->arb_sadad_description);?>

                            <div class="payOptForm">

                                <div class="col-md-12">
  
                                  <div class="row">
                                      <div class="col-sm-6 col-xs-9">
  
                                          <button type="submit" class="btn btn-success proceed_checkout"><?php echo $shopping_cart_proceed_checkout;?></button>
  
                                      </div>
  
                                  </div>
  
                              </div>

                            </div>

                        </div>


                        <div role="tabpanel" class="tab-pane <?php echo ($method->visa != 1 && $method->sadad != 1 && $method->transfer == 1 && in_array('4', $payments_allow) ? 'active' : '')?>" id="payTab_3">

                            <i class="fa fa-lock" aria-hidden="true" ></i>

                           <?php echo ($lang == 'eng' ? $method->eng_wire_description : $method->arb_wire_description);?>
                            <div class="bankTransfer">
                                <table class="transferTable">
                                    <thead>
                                        <tr>
                                            <th><?php echo $payment_bank_name_label;?></th>
                                            <th><?php echo $payment_bank_account_no_label;?></th>
                                            <th><?php echo $payment_bank_iban_no_label;?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($wire_transfers as $wire_transfer) { ?>
                                        <tr>
                                            <td><?php echo $wire_transfer[$lang.'_bank_name']; ?></td>
                                            <td><?php echo $wire_transfer['account_no']; ?></td>
                                            <td><?php echo $wire_transfer['iban_no']; ?></td>
                                        </tr>
                                    <?php } ?>

                                    </tbody>

                                </table>
							</div>
                            <div class="payOptForm">

                                <div class="col-md-12">
  
                                  <div class="row">
                                      <div class="col-sm-6 col-xs-9">
  
                                          <button type="submit" class="btn btn-success proceed_checkout"><?php echo $shopping_cart_proceed_checkout;?></button>
  
                                      </div>
  
                                  </div>
  
                              </div>

                            </div>

                        </div>

                        <div role="tabpanel" class="tab-pane <?php echo ($method->visa != 1 && $method->sadad != 1 && $method->transfer != 1 && $method->cash_on_delivery == 1 && in_array('3', $payments_allow) ? 'active' : '')?>" id="payTab_4">

                            <i class="fa fa-lock" aria-hidden="true" ></i>

                            <p style="float:left;"><?php echo ($lang == 'eng' ? $method->eng_cash_description : $method->arb_cash_description);?></p>

                            <div class="payOptForm">

                                <div class="col-md-12">
  
                                  <div class="row">
                                      <div class="col-sm-6 col-xs-9">
  					
                                          <button type="submit" class="btn btn-success proceed_checkout"><?php echo $shopping_cart_proceed_checkout;?></button>
  
                                      </div>
  
                                  </div>
  
                              </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="ck_order_sum">

                    <div class="whiteBox">

                        <p><?php echo $checkout_order_summary;?></p>

                        <table class="table">

                            <tbody>



                            <?php

                            $total = 0;

                            $totalQuan = 0;

                            foreach ($products as $key => $product) {

                                $images =  getProductImages($product['id']);
                                foreach($images as $prod_img){
                                    if($prod_img['is_thumbnail'] == 1){
                                        $thumbnail = $prod_img['thumbnail'];
                                    }
                                }
                                $total += getPriceByOffer($product['id'])*$carts[$key]['quantity'];

                                $totalQuan += $carts[$key]['quantity'];

                            ?>

                            <tr>

                                <td class="imgBox text-center"><img src="<?php echo base_url(); ?>uploads/images/thumbs/products/<?php echo $thumbnail; ?>" alt="Pro" height="144" width="111" </td>

                                <td class="text-left">

                                    <h3><?php echo $product[$lang.'_name']; ?></h3>

                                    <?php echo $shopping_cart_qty;?>: <span><?php echo $carts[$key]['quantity']; ?></span>

                                </td>

                                <?php
                                $getPriceByOffer = getPriceByOffer($product['id']);
                                $getPriceByOffer = currencyRatesCalculate($getPriceByOffer);
                                ?>

                                <td class="text-right"><?php echo $getPriceByOffer['rate']; ?> <span><?php echo $getPriceByOffer['unit'];?></span></td>

                            </tr>

                            <?php } ?>

                            </tbody>

                            <tfoot>

                            <tr>

                                <th class="text-right" colspan="2"><?php if($type == 'normal'){ echo $checkout_shipping_address_normal_shipping; }elseif($type == 'fast'){ echo $checkout_shipping_address_fast_shipping; } elseif($type == 'aramex') { echo $aramex_shipment; } else{ echo $checkout_shipping_address_free_shipping; }?></th>

                                <?php

                                $shippingAmount = currencyRatesCalculate($shipping_amount);

                                ?>
                                <th class="text-right"><?php echo $shippingAmount['rate']; ?> <?php echo $shippingAmount['unit'];?></th>

                            </tr>
                            
                            <?php if(get_cookie('voucher_code')){?>
                                <tr>
    
                                    <th class="text-right" colspan="2"><?php echo $shopping_cart_voucher_code;?></th>


                                    <?php
                                    $getCouponDiscount = getCouponDiscount();
                                    $couponArr = explode(' ',$getCouponDiscount);
                                    $getCouponDiscountVal = $couponArr[0];
                                    $getCouponDiscountUnit = '';

                                    if (strpos($getCouponDiscount, '%') == false) {
                                        // not have percentage value
                                        $getCouponDiscount = currencyRatesCalculate($getCouponDiscount);
                                        $getCouponDiscountVal = $getCouponDiscount['rate'];
                                        $getCouponDiscountUnit = $getCouponDiscount['unit'];

                                    }

                                    ?>

                                    <th class="text-right"><?php echo $getCouponDiscountVal.' '.$getCouponDiscountUnit;?></th>
    
                                </tr>
                            <?php }?>
                            <?php
                            // here calculate actual amount in sar for paytab with out convsersion
                            $forPayTabTotal = $total;

                            /*calculate loyalty discount with currency rates changing*/
                            $ifcDiscount = getCouponPrice($total);
                            $loyalty_discount = loyaltyDiscount($total,$ifcDiscount);

                            if($loyalty_discount['rate'] != 0) {
                                $forPayTabTotalamount = $forPayTabTotal - $loyalty_discount['paytabs'];
                                $totalAmountForPayTabs = getCouponTotal($forPayTabTotalamount) + getGiftCarTotal() + $shipping_amount;
                            }else{
                                $totalAmountForPayTabs = getCouponTotal($forPayTabTotal) + getGiftCarTotal() + $shipping_amount;
                            }

                            if($loyalty_discount['rate'] != 0) {
                            $total = $total - $loyalty_discount['paytabs'];
                            /*======================================================*/

                            ?>
                            <tr>
                                <th class="text-right" colspan="2"><?php echo $loyalty_discount_label;?></th>
                                <th class="text-right"><?php echo $loyalty_discount['rate'].' '.getCurrency($lang); ?></th>
                            </tr>
                            <?php } ?>

                            <?php if(get_cookie('giftIds')){
									$gifts = getGiftCardDetail();
									foreach($gifts as $gift)
									{
								?>
                                <tr>
    
                                    <th class="text-right" colspan="2"><?php echo $gift[$lang.'_name'];?></th>

                                    <?php
                                        $gift_amount = currencyRatesCalculate($gift['amount']);
                                    ?>
                                    <th class="text-right"><?php echo $gift_amount['rate'];?> <?php echo $gift_amount['unit'];?></th>
    
                                </tr>
                            <?php 	}
								  }
							?>
                            <?php $paymentCharges = payment_methods();
                            $paymentChargesRate = currencyRatesCalculate($paymentCharges->extra_cash_amount);
                            ?>
                                <tr id="cashChargeTr" style="display:none;">
    
                                    <th class="text-right" colspan="2"><?php echo $my_order_cod_label;?></th>
    
                                    <th class="text-right"><?php echo $paymentChargesRate['rate'];?> <?php echo getCurrency($lang);?></th>
    
                                </tr>


                            </tfoot>

                        </table>
                        <?php
                        $getGrandTotal = $AmountForPaytabs  = getCouponTotal($total)+getGiftCarTotal()+$shipping_amount;
                        $getGrandTotal = currencyRatesCalculate($getGrandTotal);

                        ?>

						<input type="hidden" value="<?php echo $getGrandTotal['rate'].' <span class=\'curr\'>'.$getGrandTotal['unit'].'</span>'; ?>" id="org_price" >

                        <?php
                        $getGrandTotalAllcharges = getCouponTotal($total)+getGiftCarTotal()+$shipping_amount+$paymentCharges->extra_cash_amount;
                        $getGrandTotalAllcharges = currencyRatesCalculate($getGrandTotalAllcharges);
                        ?>
                        <input type="hidden" value="<?php echo $getGrandTotalAllcharges['rate'].' <span class=\'curr\'>'.$getGrandTotalAllcharges['unit'].'</span>'; ?>" id="cod_price" >
                        
                        
                        <p><?php echo $shopping_cart_total_cost; ?> <span>(<?php echo count($products); ?> <?php echo $products_page_similar_products_count; ?>)</span></p>
						
                        <?php if($method->visa != 1 && $method->sadad != 1 && $method->transfer != 1 && $method->cash_on_delivery == 1 && in_array('3', $payments_allow)){?>


                            <h3 class="price" id="priceTotal"><?php echo $getGrandTotalAllcharges['rate']; ?> <span class="curr"><?php echo $getGrandTotalAllcharges['unit'];?></span> </h3>
                  		<?php }else{?>
                            <?php
                            $getGrandTotalCouponGifts = getCouponTotal($total)+getGiftCarTotal()+$shipping_amount;
                            $getGrandTotalCouponGifts = currencyRatesCalculate($getGrandTotalCouponGifts);
                            ?>
                        	<h3 class="price" id="priceTotal"><?php echo $getGrandTotalCouponGifts['rate']; ?> <span class="curr"><?php echo $getGrandTotalCouponGifts['unit'];?></span> </h3>
                        <?php }?>
                        <a href="<?php echo lang_base_url();?>shopping_cart/shippingAddress">
                            <button class="btn btn-success">
        
                                <?php echo $go_back_to_previous;?>
                            </button>
                        </a>

                    </div>

                </div>

                <div class="clearfix"></div>

            </div>

        </div>
        <?php if($method->visa != 1  && $method->sadad != 1 && $method->transfer != 1 && $method->cash_on_delivery){ 
					$payment_method = 'COD';
				}
				elseif($method->visa != 1  && $method->sadad != 1 && $method->transfer)
				{
					$payment_method = 'bank_transfer';
				}elseif($method->visa != 1  && $method->sadad)
				{
					$payment_method = 'sadad';
				}elseif($method->visa)
				{
					$payment_method = 'visa';
				}?>
        
        <form action="<?php echo lang_base_url();?>shopping_cart/saveOrder" method="post" class="submit_form">
	       	<input type="hidden" id="card_method_new" name="card_method_new" value="<?php echo $payment_method;?>">
            <?php
            $totalCalculateAmount = getCouponTotal($total)+$shipping_amount+getGiftCarTotal();
            $totalCalculateAmount = currencyRatesCalculate($totalCalculateAmount);

            $shippingAmount = currencyRatesCalculate($shipping_amount);
            ?>
            <input type="hidden" name="total_amount" id="total_amonut" value="<?php echo intval($totalCalculateAmount['rate']); ?>">
            <input type="hidden" name="shipment_price" id="shipment_price" value="<?php echo intval($shippingAmount['rate']); ?>">
            <input type="hidden" name="shipment_type" id="shipment_type" value="<?php echo $type; ?>">
            <input type="hidden" name="temp_order_id" id="orderId" value="<?php echo $orderID; ?>">

        </form>

    </section>
    
<?php $names = explode(' ', $address->address_title);?>
<link rel="stylesheet" href="https://www.paytabs.com/express/express.css">
<script src="https://www.paytabs.com/theme/express_checkout/js/jquery-1.11.1.min.js"></script>
<script src="https://www.paytabs.com/express/express_checkout_v3.js"></script>
<!-- Button Code for PayTabs Express Checkout -->


    <?php

    if($this->session->userdata('user_id') &&
        $this->session->userdata('login') == true) {

        $users = getUserById($this->session->userdata('user_id'));
        $email = $users['email'];
        $mobile_no = $users['mobile_no'];
    }

    $address = getCartAddress($cart_user_address);

   if(!$users){
        $email = $address->email;
        $mobile_no = $address->mobile_no;
    }
    //echo "<pre>"; print_r($address);

    ?>


<script type="text/javascript">

Paytabs("#express_checkout").expresscheckout({
	settings:{
		secret_key: "<?php echo $setting->paytab_api_key;?>",
		merchant_id: "<?php echo $setting->paytab_merchant_id;?>",
		amount: "<?php echo $totalAmountForPayTabs;?>",
		currency: "SAR",
        //currency: "<?php //echo getCurrency("eng");?>",
		title: "<?php echo $address->address_title;?>",
		product_names: "Product1,Product2,Product3",
		order_id: '<?php echo $orderID; ?>',
		url_redirect: "<?php echo lang_base_url().'shopping_cart/saveOrder'?>",
		display_billing_fields: 0,
		display_shipping_fields: 0,
		display_customer_info: 0,
		language: "<?php echo ($lang == 'eng' ? 'en' : 'ar'); ?>",
		redirect_on_reject: 1,
		is_iframe:{
			load: "onbodyload",
			show: 1,
		},

	},
	customer_info:{
		first_name: "",
		last_name: "",
		phone_number: "<?php echo $address->phone_no;?>",
		country_code: "<?php echo $address->country;?>",
		email_address: "<?php echo $email;?>"
	},
	billing_address:{
		full_address: "<?php echo $address->address_1;?>",
		city: "<?php echo getCityById($address->city);?>",
		state: "state 1",
		country: "<?php echo $address->country;?>",
		postal_code: "12345"
	},
	shipping_address:{
		shipping_first_name: "",
		shipping_last_name: "",
		full_address_shipping: "<?php echo $address->address_1;?>",
		city_shipping: "<?php echo getCityById($address->city);?>",
		state_shipping: "state 1",
		country_shipping: "<?php echo $address->country;?>",
		postal_code_shipping: "10"
	}
});
</script>