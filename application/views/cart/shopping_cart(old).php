<section>
	<div class="checkOutSetps">
		<div class="container max960_cont"> 
			<!-- Nav tabs -->
			<ul role="tablist">
				<li role="presentation" class="steps"><a <?php if(isset($_GET['step_2'])){?>href="javascript:void(0);" onclick="redirectPage('<?php echo lang_base_url();?>shopping_cart/');"<?php }else{?>href="#step_1" <?php }?> aria-controls="step_1" role="tab" data-toggle="tab" style="text-decoration:none; ">01</a></li>
                <?php if(!empty($products))
				{	?>
				<li role="presentation"><a href="#step_2" onclick="calculateTotal();" aria-controls="step_2" role="tab" data-toggle="tab" style="text-decoration:none;" id="step_2_anc">02</a></li> 
                <li role="presentation"><a href="#step_3" data-toggle="tab" role="tab" aria-controls="messages" style="text-decoration:none; cursor:default;">03</a></li>
				<!--<li role="presentation"><a href="#step_3" data-toggle="modal" class="term_check" data-target=".bs-example-modal-sm">03</a></li>-->
                <?php } ?>
			</ul>
		</div>
	</div>
    <input type="hidden" id="please_select_payment" value="<?php echo ($lang == 'eng' ? 'Please select payment method' : 'يرجى إختيار طريقة الدفع')?>" />
	<!-- Tab panes -->
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane tableArea active" id="step_1">
        	<div class="shopCartHeading">
				<div class="container max780_cont">
					<ul>
                    	<li class="active"><a href="javascript:void(0);" style="text-decoration:none; cursor:default;"><?php echo $checkout_shopping_basket;?></a></li>
                        <li><i class="fa fa-angle-left" aria-hidden="true"></i></li>
						<li><a href="javascript:void(0);" style="text-decoration:none; cursor:default;"><?php echo $checkout_personal_info;?></a></li>
						<li><i class="fa fa-angle-left" aria-hidden="true"></i></li>
						<li><a href="javascript:void(0);" style="text-decoration:none; cursor:default;"><?php echo $checkout_personal_info_address;?></a></li>
					</ul>
				</div>
				<div class="line"></div>
			</div>
			<div class="container max960_cont">
				<div class="step_one_table">
					<table class="table">
						<thead>
							<tr>
								<th colspan="2"><a href="javascript:void(0);" style="text-decoration:none; cursor:default;"><h1> <?php echo $shopping_cart_shopping_cart;?></h1></a></th>	
								<th><a href="javascript:void(0);" style="text-decoration:none; cursor:default;"><?php echo $shopping_cart_price;?></a></th>
								<th><a href="javascript:void(0);" style="text-decoration:none; cursor:default;"><?php echo $shopping_cart_quantity;?></a></th>
							</tr>
							<tr>
								<th colspan="4">
                                
								</th>
							</tr>
						</thead>
						<tbody>
                        	<?php
							 if(!empty($products))
							 {
								 $total_amount = '0';
								 $i=1;
								 $product_price='';
								 foreach($products as $product){
									 $product_price = $product->prod_price;
									 ?>
									<tr id="prod_tr_<?php echo $i; ?>">
                                     	<?php $prod_image = getProductImages($product->p_id);
											if(!empty($prod_image))
											{
												$image_name = $prod_image[0][$lang.'_image'];
											}
											else
											{
												$image_name = 'no_imge.png';
											}
										?>
										<td class="imgBox"><img src="<?php echo base_url();?>uploads/images/products/<?php echo $image_name;?>" alt="img" /></td>
										<td>
											<h3><?php echo $product->category_name;?></h3>
											<?php echo $product->product_name;?><br />
                                            <?php
											if($product->varient_id != '')
											{ 
												$var_arr = explode('|', $product->varient_id);
												$var_val_arr = explode('|', $product->varient_value_id);
												$i = 0;
												foreach($var_arr as $varid)
												{
													$var = getVariantById($varid);
													$var_val = getVariantValueById($var_val_arr[$i]);
													if($lang == 'eng')
													{
														echo $var_val->eng_value;
													}
													else
													{
														echo $var_val->arb_value;
													}
												$i++;
												}
											}
												?>
											<?php if($product->prod_quantity == 0){?>
											<h4><?php echo $shopping_cart_last_piece;?> </h4>
											<?php }?>
										</td>
										<td><strong id="prod_price_<?php echo $i; ?>"><?php echo ($product_price*$product->quantity);?></strong> <?php echo getPriceCur($product_price, true);?></td>
										<td>
											<select id="prod_drp_<?php echo $i; ?>" onchange="calculateOrder(this.value, '<?php echo $i;?>', '<?php echo $product_price;?>', '<?php echo $product->temp_order_id;?>');">
                                            
                                            <?php for($q=1; $q<=$product->total_quantity; $q++){?>
												<option value="<?php echo $q;?>" <?php echo ($product->quantity == $q ? 'selected' : '');?>><?php echo $q;?></option>
											<?php } ?>
                                            </select>
											<a href="javascript:void(0);" onclick="delFromCart('<?php echo $product->temp_order_id; ?>', '<?php echo $i;?>');" ><img src="<?php echo base_url();?>assets/frontend/images/trashCan.png" alt="Del" height="15" width="12" /></a>
										</td>
									</tr>
								<?php 
									$total_amount += ($product_price*$product->quantity);
									$i++;
								} 
							 } else
							 {
								 echo '<tr><td colspan="4">'.$no_records.'</td></tr>';
							 }?>
						</tbody>
						<tfoot>
							<tr>
								<th colspan="4">
									<h2><?php echo $shopping_cart_total;?></h2>
									<strong id="prod_price_total"><?php echo $total_amount; ?> </strong>
									 <?php echo getPriceCur($total_amount, true);?>
								</th>
							</tr>
						</tfoot>
					</table> 
				</div>
			</div>
			<div class="line"></div>
			<div class="defaultPageSty">
				<div class="container max780_cont">
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12 text-center">
                        	<?php if(!empty($products))
									{	?>
								<div class="cartStep1Btn">
									<a href="#step_2" aria-controls="step_2" onclick="calculateTotal();" role="tab" data-toggle="tab" class="edBtn"><?php echo $checkout_continue_shopping;?></a>
								</div>
							 <?php } ?>
						</div>
					</div>
				</div>
			</div>
            <?php $recently_viewed = getRecentViewed();
			if(!empty($recently_viewed)){
			?>
                <div class="container">
                    <div class="productSecFive playSlide green">
                        <h2><?php echo $products_recently_viewed_products;?></h2>
                        <div class="row">
                            <ul>
                                <?php 
                                $recently_viewed = getRecentViewed();
                                foreach($recently_viewed as $product){
                                      $image_arr = getProductImages($product['id']);
                                            ?>
                                  <li>
                                      <div class="imgBox">
                                          <?php if(sizeof($image_arr) > 1){?>						
                                              <div id="Product_id_<?php echo $main;?>" class="carousel slide" data-ride="carousel"> 															
                                                  <!-- Wrapper for slides -->
                                                  <div class="carousel-inner" role="listbox">
                                                      <?php 
                                                      $im=0;
                                                      foreach($image_arr as $image){?>
                                                          <div class="item<?php if($im==0){ echo ' active';}?>"> <a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"><img src="<?php echo base_url();?>uploads/images/products/<?php echo $image[$lang.'_image']?>" alt="Product" height="199" width="129" ></a>
                                                          </div>
                                                      <?php
                                                      $im++; 
                                                      }?>                                                   
                                                  </div>
                                              </div>
                                          <?php }else{ ?>
                                              <?php if($image_arr){
                                                  foreach($image_arr as $image){?>
                                                  
                                                 <a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"> <img src="<?php echo base_url();?>uploads/images/products/<?php echo $image[$lang.'_image']?>" alt="Product" height="199" width="129" /></a>
                                              <?php 
                                                  }
                                              } else
                                              { ?>
                                                 <a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"> <img src="<?php echo base_url();?>uploads/images/products/no_imge.png" alt="Product" height="199" width="129" /></a>
                                              <?php }?>
                                          <?php } ?>
                                      </div>
                                      <!-- Indicators -->
                                      <?php if(sizeof($image_arr) > 1){?>
                                          <ol class="carousel-indicators">
                                          <?php 
                                          $imo=0;
                                          foreach($image_arr as $image){?>
                                              <li data-target="#Product_id_<?php echo $main;?>" data-slide-to="<?php echo $imo;?>"<?php if($imo==0){ echo ' class="active"';}?>></li>
                                          <?php 
                                          $imo++; 
                                          }
                                          ?>
                                      </ol>	
                                      <?php }?>	
                                      <h3><a href="<?php echo lang_base_url().'product/product_page/'.($product['tpl_name'] != '' ? $product['tpl_name'] : $product['id']);?>"><?php echo $product[$lang.'_name']?></a></h3>
                                      <?php  
                                          if($product['has_discount_price'])
                                          {
                                               echo '<h4><span class="discount">'.$product[$lang.'_price'].'</span>'.getPriceCur($product[$lang.'_discount_price']).'</h4>';
                                          }
                                          else
                                          {
                                              echo '<h4>'.getPriceCur($product[$lang.'_price']).'</h4>';
                                          }
                                          
                                          ?>
                                  </li>
                              <?php 
                              $main++;
                              }?>
                            </ul>
                        </div>
                    </div>
                </div>
        <?php }?>
		</div><!-- step one END -->
		<div role="tabpanel" class="tab-pane" id="step_2">
			<div class="shopCartHeading">
				<div class="container max780_cont">
					<ul>
                    	<li><a href="javascript:void(0);" style="text-decoration:none; cursor:default;"><?php echo $checkout_shopping_basket;?></a></li>
                        <li><i class="fa fa-angle-left" aria-hidden="true"></i></li>
						<li class="active"><a href="javascript:void(0);" style="text-decoration:none; cursor:default;"><?php echo $checkout_personal_info;?></a></li>
						<li><i class="fa fa-angle-left" aria-hidden="true"></i></li>
						<li><a href="javascript:void(0);" style="text-decoration:none; cursor:default;"><?php echo $checkout_personal_info_address;?></a></li>
					</ul>
				</div>
				<div class="line"></div>
			</div>
			<div class="carStep1List">
            <div class="defaultPageSty">
				<div class="container max780_cont">
					<div class="overAutoXS">
						<table class="table table-hover">
							<tbody>
                            	<?php 
								$j=1;
								$product_price='';
								$total_prod_amount = '';
								 foreach($products as $product){
									$product_price = $product->prod_price;
									/* if($product->has_discount_price == 1)
									 {
										 $product_price = $product->product_discount_price;
									 }
									 else
									 {
										 $product_price = $product->product_price;
									 }*/
								 ?>
									<tr id="prod_2_tr_<?php echo $j; ?>">
                                        <td>
                                            <a href="javascript:void(0);" onclick="delFromCart('<?php echo $product->temp_order_id; ?>', '<?php echo $j;?>');"><img src="<?php echo base_url();?>assets/frontend/images/trashCan.png" alt="Delete" height="15" width="12" /></a>
                                            <label><?php echo $product->category_name; ?></label>
                                            <?php echo $product->product_name; ?>
                                            <span class="border">&nbsp;</span>
                                        </td>
                                        <td class="amount"><span class="red prod-prices" id="prod_pr_<?php echo $j; ?>"><?php echo ($product_price * $product->quantity); ?></span> <span style="font-size: 16px;display: inline-block;margin: 1px 0 0 0;"><?php echo getPriceCur($product_price, true);?></span></td>
                                    </tr>
                                <?php 
									$total_prod_amount += ($product_price * $product->quantity);
									$j++;
								} ?>
							</tbody>
							<tfoot>
								<tr>
									<th colspan="2">
										<span><?php echo $shopping_cart_total;?></span> <span class="border"></span> <span class="red" id="total-price"><?php echo $total_prod_amount; ?></span> <span style="font-size: 16px;display: inline-block;margin: 1px 0 0 0;"><?php echo getPriceCur($total_prod_amount, true);?></span>
									</th>
								</tr>
							</tfoot>
						</table>
					</div>
					<?php if($this->session->userdata('user_id') && $this->session->userdata('login') == true && $cards){?>
						<form id="paymentFormForCheckout" action="<?php echo base_url(); ?>ajax/makePayment" method="post" onsubmit="return false;">
							<!--<div class="addNewAddStep3">
								<h2><?php echo $shopping_product_already_added_cards;?></h2>
								<ul class="row">
									<?php
									$ch = 1;
									foreach($cards as $card){?>
										<li class="col-md-4 col-sm-6 col-xs-6">
											<div>
												<div class="addCheckBx">
													<input id="checkbox_li_<?php echo $ch;?>" class="card_checkbox" type="radio" name="card_method_old" value="<?php echo $card['id']; ?>">
													<label for="checkbox_li_<?php echo $ch;?>"><span></span></label>
												</div>
												<?php echo $ch;?> : <?php echo $card['customer_name']; ?>
												<br>
												<?php echo $card['card_number']; ?>
												<br>
												<?php echo $card['payment_option']; ?>
												<div class="clearfix"></div>
											</div>
										</li>
										<?php
										$ch++;
									}?>
								</ul>
							</div>
							<input type="hidden" id="card_method_new" name="card_method_new" value="cc_merchantpage">-->
                            <input type="hidden" id="card_method_new" name="card_method_new" value="COD">                            
                            <input type="hidden" id="method_not_selected" value="0">
							<input type="hidden" name="total_amount" value="<?php echo $total_prod_amount; ?>">
							<input type="submit" id="hdnPaymentFormBtn" style="opacity:0;">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label><?php echo $register_first_name;?><span class="redStar">*</span></label>
                                    <input type="text" name="first_name" id="first_name" data-toggle="tooltip" data-placement="top" title="" value="<?php echo $personal_info->first_name;?>" />
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label><?php echo $register_last_name;?><span class="redStar">*</span></label>
                                    <input type="text" name="last_name" id="last_name" data-toggle="tooltip" data-placement="top" title="" value="<?php echo $personal_info->last_name;?>" />
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label><?php echo $register_email;?><span class="redStar">*</span></label>
                                    <input type="text" name="email" id="email" data-toggle="tooltip" data-placement="top" title="" value="<?php echo $personal_info->email;?>" />
                                </div>
                                
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label><?php echo $register_mobile_number;?><span class="redStar">*</span></label>
                                    <input type="text" name="mobile_no" id="mobile_no" data-toggle="tooltip" data-placement="top" title="" <?php if($personal_info->mobile_no != ''){?> value="<?php echo $personal_info->mobile_no;?>" <?php }else{?> value=""<?php }?> />
                                    <input type="hidden" name="mobile_ios2_code" id="mobile_ios2_code" value="<?php echo ($personal_info->mobile_ios2_code != '' ? $personal_info->mobile_ios2_code :'sa|966');?>" />
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label><?php echo $register_phone_number;?></label>
                                    <input type="text" name="phone_no" id="phone_no" data-toggle="tooltip" data-placement="top" title="" <?php if($personal_info->phone_no != ''){?> value="<?php echo $personal_info->phone_no;?>" <?php }else{?> value=""<?php }?> />
                                    <input type="hidden" name="phone_ios2_code" id="phone_ios2_code" value="<?php echo ($personal_info->phone_ios2_code != '' ? $personal_info->phone_ios2_code :'sa|966');?>" />
                                </div>                                
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label><?php echo $register_country;?><span class="redStar">*</span></label>
                                    <select name="country" id="country" class="countryClass" data-toggle="tooltip" data-placement="top" title="">
                                        <option value=""><?php echo $register_country;?></option>
                                        <?php echo getCoutries($personal_info->country);?>
                                    </select>
                                </div>                                
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label><?php echo $register_city;?><span class="redStar">*</span></label>
                                    <select name="city" id="city" class="cityClass" data-toggle="tooltip" data-placement="top" title="">
										<?php echo getCity($personal_info->country, $personal_info->city);?>
                                    </select>
                                </div>
                            </div>
						</form>
                        <!--<div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 addMoreButton">
                                <button type="button" class="btn" id="addNewCard">+</button><?php echo ($lang == 'eng' ? 'Add New Card' : 'إضافة بطاقة جديدة');?>
                            </div>
                        </div>-->
                        <!--<div class="container max960_cont" id="checkoutPaymentTypes" style="display: none;">
    
                            <label><?php echo $register_choose_payment_method;?></label>
                            <div class="radioPayMeth addNewCardAtCheckout">
                                <p>
                                	<input id="checkbox_1" type="radio" name="order_idsED" value="cc_merchantpage" checked="checked">
                                    <label for="checkbox_1"><span></span><img src="<?php echo base_url()?>assets/frontend/images/visaMasterImg.png" alt="master Card" id="cc_merchantpage" height="57" width="125" class="active" /></label>
                                </p>
                                <p>
                                	<input id="checkbox_2" type="radio" name="order_idsED" value="sadad">
                                    <label for="checkbox_2"><span></span><img src="<?php echo base_url()?>assets/frontend/images/sadasImage.png" alt="sadad" id="sadad" height="57" width="97" /></label>
                                </p>
                                <p>
                                	<input id="checkbox_3" type="radio" name="order_idsED" value="COD">
                                    <label for="checkbox_3"><span></span><img src="<?php echo base_url()?>assets/frontend/images/cashDlivImg.png" alt="Cash on Deliver" id="COD" height="57" width="76" /></label>
                                </p>
                            </div>
                            <p>&nbsp;</p>
    
                        </div>-->
                        <p>&nbsp;</p>
                        <!--<section class="merchant-page-iframe">
                            <?php
                           /* $objFort = new PayfortCheckout();
                            $merchantPageData = $objFort->getMerchantPageData();
                            $postData = $merchantPageData['params'];
                            $gatewayUrl = $merchantPageData['url'];*/
                            ?>
                            <div class="cc-iframe-display">
                                <div id="div-pf-iframe" style="display:none">
                                    <div class="pf-iframe-container">
                                        <div class="pf-iframe" id="pf_iframe_content">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>-->
                    <?php }else{ ?>
                    	<form id="paymentFormForCheckout" action="<?php echo base_url(); ?>ajax/makePayment" method="post" onsubmit="return false;">
							<!--<input type="hidden" id="card_method_new" name="card_method_new" value="cc_merchantpage">-->
                            
                            <input type="hidden" id="card_method_new" name="card_method_new" value="COD">
                            
							<input type="hidden" name="total_amount" value="<?php echo $total_prod_amount; ?>">
							<input type="submit" id="hdnPaymentFormBtn" style="opacity:0;">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label><?php echo $register_first_name;?><span class="redStar">*</span></label>
                                    <input type="text" name="first_name" id="first_name" data-toggle="tooltip" data-placement="top" title="" value="<?php echo $personal_info->first_name;?>" />
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label><?php echo $register_last_name;?><span class="redStar">*</span></label>
                                    <input type="text" name="last_name" id="last_name" data-toggle="tooltip" data-placement="top" title="" value="<?php echo $personal_info->last_name;?>" />
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label><?php echo $register_email;?><span class="redStar">*</span></label>
                                    <input type="text" name="email" id="email" data-toggle="tooltip" data-placement="top" title="" value="<?php echo $personal_info->email;?>" />
                                </div>
                                
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label><?php echo $register_mobile_number;?><span class="redStar">*</span></label>
                                    <input type="text" name="mobile_no" id="mobile_no" data-toggle="tooltip" data-placement="top" title="" <?php if($personal_info->mobile_no != ''){?> value="<?php echo $personal_info->mobile_no;?>" <?php }else{?> value=""<?php }?> />
                                    <input type="hidden" name="mobile_ios2_code" id="mobile_ios2_code" value="<?php echo ($personal_info->mobile_ios2_code != '' ? $personal_info->mobile_ios2_code :'sa|966');?>" />
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label><?php echo $register_phone_number;?></label>
                                    <input type="text" name="phone_no" id="phone_no" data-toggle="tooltip" data-placement="top" title="" <?php if($personal_info->phone_no != ''){?> value="<?php echo $personal_info->phone_no;?>" <?php }else{?> value=""<?php }?> />
                                    <input type="hidden" name="phone_ios2_code" id="phone_ios2_code" value="<?php echo ($personal_info->phone_ios2_code != '' ? $personal_info->phone_ios2_code :'sa|966');?>" />
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label><?php echo $register_country;?><span class="redStar">*</span></label>
                                    <select name="country" id="country" class="countryClass" data-toggle="tooltip" data-placement="top" title="">
                                        <option value=""><?php echo $register_country;?></option>
                                        <?php echo getCoutries($personal_info->country);?>
                                    </select>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <label><?php echo $register_city;?><span class="redStar">*</span></label>
                                    <select name="city" id="city" class="cityClass" data-toggle="tooltip" data-placement="top" title="">
										<?php echo getCity($personal_info->country, $personal_info->city);?>
                                    </select>
                                </div>
                            </div>
						</form>
                    
                        <div class="container max960_cont" id="checkoutPaymentTypes">
    
                            <label><?php echo $register_choose_payment_method;?></label>
                            <div class="radioPayMeth addNewCardAtCheckout">
                                <p>
                                	<input id="checkbox_1" type="radio" name="order_idsED" value="cc_merchantpage" checked="checked">
                                    <label for="checkbox_1"><span></span><img src="<?php echo base_url()?>assets/frontend/images/visaMasterImg.png" alt="master Card" id="cc_merchantpage" height="57" width="125" class="active" /></label>
                                </p>
                                <p>
                                	<input id="checkbox_2" type="radio" name="order_idsED" value="sadad">
                                    <label for="checkbox_2"><span></span><img src="<?php echo base_url()?>assets/frontend/images/sadasImage.png" alt="sadad" id="sadad" height="57" width="97" /></label>
                                </p>
                                <p>
                                	<input id="checkbox_3" type="radio" name="order_idsED" value="COD">
                                    <label for="checkbox_3"><span></span><img src="<?php echo base_url()?>assets/frontend/images/cashDlivImg.png" alt="Cash on Deliver" id="COD" height="57" width="76" /></label>
                                </p>
                            </div>
                            <p>&nbsp;</p>
    
                        </div>
                        <p>&nbsp;</p>
                        <section class="merchant-page-iframe">
                            <?php
                            $objFort = new PayfortCheckout();
                            $merchantPageData = $objFort->getMerchantPageData();
                            $postData = $merchantPageData['params'];
                            $gatewayUrl = $merchantPageData['url'];
                            ?>
                            <div class="cc-iframe-display">
                                <div id="div-pf-iframe" style="display:none">
                                    <div class="pf-iframe-container">
                                        <div class="pf-iframe" id="pf_iframe_content">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    <?php }?>
				</div>
            </div>
			</div>
			<div class="line"></div>
			<div class="defaultPageSty">
				<div class="container max780_cont">
					<!--<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12 bigCheckBox">
							<input type="checkbox" name="checkbox" id="checkbox_1"><label for="checkbox_1"><span></span>لقد قرأت واطلعت وأوافق على الشروط والأحكام </label>
						</div>
					</div>-->
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12 text-center">
                        	<!--<a href="#step_3" data-toggle="tab" role="tab" aria-controls="messages" id="Shipping_address_1">--><input type="button" id="submitPaymentFormBtn" value="<?php echo $checkout_continue_shopping;?>" class="edBtn red"><!--</a>-->
							<!--<a href="#step_3" data-toggle="modal" id="Shipping_address_1" class="term_check" data-target=".bs-example-modal-sm"><input type="button" value="اكمال عملية الشراء" class="edBtn red"></a>-->
						</div>
					</div>
				</div>
			</div>
			
		</div><!-- Step two end -->
        
		<!--<div role="tabpanel" class="tab-pane" id="step_3">
			<div class="shopCartHeading">
				<div class="container max960_cont">
					<ul>
						<li><a href="javascript:void(0);"><?php echo $checkout_personal_info;?></a></li>
						<li><i class="fa fa-angle-left" aria-hidden="true"></i></li>
						<li class="active"><a href="javascript:void(0);"><?php echo $checkout_personal_info_address;?></a></li>
					</ul>
				</div>
				<div class="line"></div>
			</div>
			<div class="defaultPageSty">
				<div class="container max960_cont">
					
					<form class="ajax_form_order" action="<?php echo base_url(); ?>ajax/action/" onsubmit="return false;" method="post" id="surveyForm">
                    	<input type="hidden" name="form_type" value="save_order" />
                    	
                        <div class="addNewAddStep3">
							<h2><?php echo 'Choose Payment Method';?></h2>
							<?php if($this->session->userdata('user_id') && $this->session->userdata('login') == true && $addresses){?>
                            	<input type="hidden" id="user_guest" value="0" />
                                <ul class="row">
                                	<?php 
									$ch = 1;
									foreach($addresses as $addr){?>
                                        <li class="col-md-4 col-sm-6 col-xs-6">
                                            <div>
                                                <div class="addCheckBx">
                                                    <input id="checkbox_li_<?php echo $ch;?>" class="address_checkbox" type="radio" name="address_checkbox" value="<?php echo $addr['id']?>">
                                                    <label for="checkbox_li_<?php echo $ch;?>"><span></span></label>
                                                </div>
                                                <?php echo $ch;?> : <?php echo $addr['address_city']?> - <?php echo $addr['street_name']?> <span class="numeric"><?php echo $addr['property_no']?></span> 
                                                <div class="clearfix"></div>
                                                <a href="javascript:void(0);" class="changeAdd"><?php echo $shipping_address1_edit_address;?></a>
                                            </div>
                                        </li>
                                    <?php 
									$ch++;
									}?>
                                </ul>
                            <?php }else
							{?>
								
                            <input type="hidden" id="user_guest" value="1" />
								<input type="hidden" id="card_method_new" name="card_method_new" value="cc_merchantpage">
								<input type="hidden" id="total_amount" name="total_amount" value="<?php echo $total_prod_amount; ?>">
								<div class="container max960_cont" id="checkoutPaymentTypes">

									
									<p class="addBtnActive addNewCardAtCheckoutForGuest">
										<img src="<?php echo base_url()?>assets/frontend/images/visaMasterImg.png" alt="master Card" id="cc_merchantpage" height="57" width="125" class="active" />
										<img src="<?php echo base_url()?>assets/frontend/images/sadasImage.png" alt="sadad" id="sadad" height="57" width="97" />
										<img src="<?php echo base_url()?>assets/frontend/images/cashDlivImg.png" alt="Cash on Deliver" id="COD" height="57" width="76" />
									</p>
									<p>&nbsp;</p>

								</div>
								<section class="merchant-page-iframe">
									<?php
									$objFort = new PayfortCheckout();
									$merchantPageData = $objFort->getMerchantPageData();
									$postData = $merchantPageData['params'];
									$gatewayUrl = $merchantPageData['url'];
									?>
									<div class="cc-iframe-display">
										<div id="div-pf-iframe" style="display:none">
											<div class="pf-iframe-container">
												<div class="pf-iframe" id="pf_iframe_content">
												</div>
											</div>
										</div>
									</div>
								</section>
								<div class="row">
									<div class="col-md-12 col-sm-12 col-xs-12 text-center">
										<input type="submit" value="<?php echo $checkout_continue_shopping; ?>" class="edBtn red sendCheckoutReqForGuest" id="sendCheckoutBtn">
									</div>
								</div>
							<?php }?>
                         </div>

                        <?php if($this->session->userdata('user_id') && $this->session->userdata('login') == true && $addresses){?>
                            <div class="row" id="addMoreButtonParent">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 addMoreButton">
                                    <button type="button" class="btn addButtonTwo">+</button><?php echo $shipping_address1_add_another_address;?>
                                </div>
                            </div>
                        <?php }?>

                    </form>
                    <a href="#orderNumber" class="fancybox" id="success_pop"></a>
				</div>
			</div>
		</div>--><!-- step 3 end -->
	</div>	
</section>


<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <?php echo $checkout_terms_conditions;?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    </div>
  </div>
</div>
<div class="hide" id="optionTemplateTwo">
    <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-12">
            <label><?php echo $shipping_address_street_name;?></label>
            <input type="text" name="street_name" data-toggle="tooltip" data-placement="top" title="" />
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <label><?php echo $shipping_address_property_number;?></label>
            <input type="text" name="property_no" data-toggle="tooltip" data-placement="top" title="" />
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <label><?php echo $shipping_address_city;?></label>
            <input type="text" name="address_city" data-toggle="tooltip" data-placement="top" title="" />
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <label><?php echo $shipping_address_more_details;?></label>
            <input type="text" name="more_detail" data-toggle="tooltip" data-placement="top" title="" />
        </div>
    </div>
</div>