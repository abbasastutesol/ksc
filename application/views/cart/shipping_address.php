

<section class="">

    <div class="container">

        <div class="cartHeading">

            <h1 class="pull-left"><?php /*?><?php echo $products_page_complete_shopping;?>, <?php */?><span><?php echo $checkout_personal_info_address;?></span></h1>

            <?php $isLogin = $this->session->userdata('login'); ?>

            <?php if($isLogin != true) { ?>

            <a href="#."  data-toggle="modal" data-target="#attentionLogin">

                <button class="btn btn-black pull-right"><?php echo $box_log_in;?></button>

            </a>

            <?php } ?>

            <div class="clearfix"></div>

        </div>

        <div class="ckMain_box">

            <div class="ck_Add_area">

                <!-- Change it, If we have Any Address -->

                <?php if(!$addresses) { ?>

                    <div class="ifNo_Address">

                        <h1>

                            <i class="fa fa-ban" aria-hidden="true"></i> <br/>

                            <?php echo $checkout_shipping_no_address_message;?>

                        </h1>

                    </div>

                <?php } else{ ?>



                    <div class="ifHave_Address">

                        <div class="addressBoxes">



                        <?php foreach($addresses as $address) { ?>


                            <div class="AddBox">

                                <div class="whiteBox">

                                    <h1><?php echo $address['address_title']; ?></h1>

                                    <div class="menuBtn">

                                        <div class="dropdown">

                                            <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                                <i class="sprite_ioud ioudSpdropdown"></i>

                                            </button>

                                            <ul class="dropdown-menu" aria-labelledby="dLabel">

                                                <li><a href="javascript:void(0)" onclick="showEdit('<?php echo $address['id']; ?>','<?php echo $address['address_type']; ?>'); $('#address_type').val('<?php echo $address['address_type']; ?>');"><?php echo $checkout_personal_info_edit_address;?></a> </li>

                                                <li><a href="javascript:void(0)" onclick="deleteCartAddress('<?php echo $address['id']; ?>','<?php echo $address['address_type']; ?>', '<?php echo $delete_confirm_general;?>');"><?php echo $shipping_address_remove_address;?></a> </li>

                                            </ul>

                                        </div>

                                    </div>

                                    <div class="sandGrayBox">

                                        <p><?php echo $address['address_1']; ?></p>

                                        <p><?php echo $address['address_2']; ?></p>

                                        <?php echo getCoutryByCode($address['country']).", ".getCityById($address['city']); ?> <br />

                                        <?php echo $shipping_address_phone;?>: <span class="numberArb"><?php echo $address['phone_no']; ?></span>

                                    </div>

                                    <a href="javascript:void(0);" onclick="addressId('<?php echo $address['id']; ?>', '<?php echo $address['country']; ?>', '<?php echo $address['city']; ?>');">

                                        <button class="btn btn-success"><?php echo $checkout_shipping_select_this_address;?></button>

                                    </a>

                                </div>

                            </div>



                        <?php } ?>



                            <form action="<?php echo lang_base_url().'shopping_cart/checkout'; ?>" method="post" id="address_form">

                                <input type="hidden" name="address_id" id="address_id_val" value="">

                                <input type="hidden" name="shipping_method" id="shipping_method" value="">

                                <input type="hidden" name="shipping_id" id="shipping_id_val" value="">

                                <input type="hidden" name="addressType" id="addressType" value="cart_address">

                                <input type="hidden" name="temp_total_amount" id="temp_total_amount" value="">


                            </form>

                            <div class="clearfix"></div>

                        </div>

                        <div class="payOptAddress whiteBox" id="shipping_box" style="display:none;">

                            

                        </div>

                    </div><!--     Display block if we have any Address -->



                <?php } ?>

                <div class="clearfix"></div>
				<?php 
				if(!$addresses && $this->session->userdata('login') == false && !$this->session->userdata('user_id')){ ?>
                    <div class="AddMoreAddressBtn">
    
                        <a href="#." id="shipping_address" class="addAddressReset" data-toggle="modal" data-target="#addAddress">
    
                            <i class="sprite_ioud ioudSpaddplus"></i>
    
                           <?php echo $add_new_address_label;?>
    
                        </a>
    
                    </div>
				<?php }elseif($this->session->userdata('login') == true && $this->session->userdata('user_id')){?>
                    <div class="AddMoreAddressBtn">
    
                        <a href="#." id="shipping_address" class="addAddressReset" data-toggle="modal" data-target="#addAddress">
    
                            <i class="sprite_ioud ioudSpaddplus"></i>
    
                           <?php echo $add_new_address_label;?>
    
                        </a>
    
                    </div>
                <?php }?>


            </div>

            <div class="ck_order_sum">

                <div class="whiteBox">

                    <p><?php echo $checkout_order_summary;?></p>

                    <table class="table">

                        <tbody>
                        
                        <?php
                        $total = 0;

                        $totalQuan = 0;

                        foreach ($products as $key => $product) {

                            $images =  getProductImages($product['id']);
                            foreach($images as $prod_img){
                                if($prod_img['is_thumbnail'] == 1){
                                    $thumbnail = $prod_img['thumbnail'];
                                }
                            }

                        $total += getPriceByOffer($product['id'])*$cart_data[$key]['quantity'];



                        $totalQuan += $cart_data[$key]['quantity'];

                        $loyaltyPopupTotal = $total;
                        ?>



                        <tr>

                            <td class="imgBox text-center"><img src="<?php echo base_url(); ?>uploads/images/thumbs/products/<?php echo $thumbnail; ?>" alt="Pro" height="144" width="111" </td>

                            <td class="text-left">

                                <h3><?php echo $product[$lang.'_name']; ?></h3>

                                <?php echo $shopping_cart_qty;?>: <span><?php echo $cart_data[$key]['quantity']; ?></span>

                            </td>
                            <?php
                            $getPriceOffer = getPriceByOffer($product['id']);
                            $getPriceOffer = currencyRatesCalculate($getPriceOffer);
                            ?>

                            <td class="text-right"><?php echo $getPriceOffer['rate']; ?> <span><?php echo $getPriceOffer['unit'];?></span></td>

                        </tr>



                        <?php

                        }

                        ?>



                        </tbody>

                       <tfoot>

                            
                            <?php if(get_cookie('voucher_code')){?>
                                <tr>
    
                                    <th class="text-right" colspan="2"><?php echo $shopping_cart_voucher_code;?></th>

                                    <?php
                                    $getCouponDiscount = getCouponDiscount();
                                    $couponArr = explode(' ',$getCouponDiscount);
                                    $getCouponDiscountVal = $couponArr[0];
                                    $getCouponDiscountUnit = '';

                                    if (strpos($getCouponDiscount, '%') == false) {
                                        // not have percentage value
                                        $getCouponDiscount = currencyRatesCalculate($getCouponDiscount);
                                        $getCouponDiscountVal = $getCouponDiscount['rate'];
                                        $getCouponDiscountUnit = $getCouponDiscount['unit'];

                                    }

                                    ?>

                                    <th class="text-right"><?php echo $getCouponDiscountVal.' '.$getCouponDiscountUnit;?></th>
    
                                </tr>

                            <?php }?>

                            <?php

                            /*calculate loyalty discount with currency rates changing*/
                            $ifcDiscount = getCouponPrice($total);

                            $loyalty_discount = loyaltyDiscount($total,$ifcDiscount);
                            if($loyalty_discount['rate'] > 0) {
                                $total = $total - $loyalty_discount['rate'];
                            /*======================================================*/

                            ?>
                                <tr>
                                    <th class="text-right" colspan="2"><?php echo $loyalty_discount_label;?></th>

                                    <th class="text-right"><?php echo $loyalty_discount['rate'].' '.getCurrency($lang); ?></th>
                                </tr>
                            <?php } ?>

                            <?php if(get_cookie('giftIds')){
									$gifts = getGiftCardDetail();
									foreach($gifts as $gift)
									{
								?>
                                <tr>
    
                                    <th class="text-right" colspan="2"><?php echo $gift[$lang.'_name'];?></th>
                                    <?php

                                    $gift_amount = currencyRatesCalculate($gift['amount']);
                                    ?>
                                    <th class="text-right"><?php echo $gift_amount['rate'];?> <?php echo $gift_amount['unit'];?></th>
    
                                </tr>
                            <?php 	}
								  }
							?>
                            <!--<tr>

                                <th class="text-right" colspan="2"><?php echo $checkout_shipping_address_taxes;?></th>

                                <th class="text-right">0.00 <?php echo getCurrency($lang);?></th>

                            </tr>-->

                    	</tfoot>

                    </table>

                    <p><?php echo $shopping_cart_total_cost;?> <span>(<?php echo count($products); ?> <?php echo $products_page_similar_products_count;?>)</span></p>

                    <?php

                    $grandTotalPrice = getCouponTotal($total)+getGiftCarTotal();
                    $grandTotalPrice = currencyRatesCalculate($grandTotalPrice);
                    ?>
                    <h3 class="price"><?php echo $grandTotalPrice['rate']; ?> <span class="curr"><?php echo $grandTotalPrice['unit'];?></span> </h3>
                    
                    <a href="#." onclick="submitForm('<?php echo $lang;?>','<?php echo $grandTotalPrice["rate"]; ?>');">

                        <button class="btn btn-success">

                            <i class="sprite_ioud ioudSpcart"></i>

                            <?php echo $shopping_cart_proceed_to_checkout;?>

                        </button>

                        </a>
                    <a href="<?php echo lang_base_url().'shopping_cart'; ?>">
                    <button class="btn btn-success">

                        <?php echo $go_back_to_previous;?>

                    </button>
                    </a>

                </div>

            </div>

            <div class="clearfix"></div>

        </div>

    </div>
    <input id="loyaltyPopupTotal" type="hidden" value="<?php echo $loyaltyPopupTotal; ?>">
</section>



