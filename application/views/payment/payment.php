
    <section class="">
        <div class="container">
            <div class="cartHeading">
                <h1 class="pull-left">Check Out, <span>Payment Method</span></h1>
                <a href="javascript:void(0);"  data-toggle="modal" data-target="#attentionLogin">
                    <button class="btn btn-black pull-right">Log in</button>
                </a>
                <div class="clearfix"></div>
            </div>
            <div class="ckMain_box">
                <div class="payMethodOpt whiteBox">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#payTab_1" aria-controls="payTab_1" role="tab" data-toggle="tab"><i class="payOptBtn sprite_1"></i></a>
                        </li>
                        <li role="presentation">
                            <a href="#payTab_2" aria-controls="payTab_2" role="tab" data-toggle="tab"><i class="payOptBtn sprite_2"></i></a>
                        </li>
                        <li role="presentation">
                            <a href="#payTab_3" aria-controls="payTab_3" role="tab" data-toggle="tab"><i class="payOptBtn sprite_3"></i></a>
                        </li>
                        <li role="presentation">
                            <a href="#payTab_4" aria-controls="payTab_4" role="tab" data-toggle="tab"><i class="payOptBtn sprite_4"></i></a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="payTab_1">
                            <i class="fa fa-lock" aria-hidden="true"></i>
                            <br />
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas suscipit, ex sed vestibulum blandit, eros mi congue ligula, non faucibus dolor odio ut lectus. In vestibulum tempus tortor. 	</p>
                            <div class="payOptForm">
                                <form action="order_placed.php" method="get">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Card Number</label>
                                            <input type="text" placeholder="Enter" />
                                        </div>
                                        <div class="col-md-6">
                                            <label>Card Holder Name</label>
                                            <input type="text" placeholder="Enter" />
                                        </div>
                                        <div class="col-md-6">
                                            <label>Expiry</label>
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <input type="text" placeholder="Month" />
                                                </div>
                                                <div class="col-xs-6">
                                                    <input type="text" placeholder="Year" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Security Code</label>
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <input type="text" placeholder="Enter CVC" />
                                                </div>
                                                <div class="col-xs-6">
                                                    <button type="submit" class="btn btn-success">Pay Now!</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="payTab_2">
                            <i class="fa fa-lock" aria-hidden="true"></i>
                            <br />
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas suscipit, ex sed vestibulum blandit, eros mi congue ligula, non faucibus dolor odio ut lectus. In vestibulum tempus tortor. 	</p>
                            <div class="payOptForm">
                                <form action="javascript:void(0);" method="get">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Card Number</label>
                                            <input type="text" placeholder="Enter" />
                                        </div>
                                        <div class="col-md-6">
                                            <label>Card Holder Name</label>
                                            <input type="text" placeholder="Enter" />
                                        </div>
                                        <div class="col-md-6">
                                            <label>Expiry</label>
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <input type="text" placeholder="Month" />
                                                </div>
                                                <div class="col-xs-6">
                                                    <input type="text" placeholder="Year" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Security Code</label>
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <input type="text" placeholder="Enter CVC" />
                                                </div>
                                                <div class="col-xs-6">
                                                    <button type="submit" class="btn btn-success">Pay Now!</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="payTab_3">
                            <i class="fa fa-lock" aria-hidden="true"></i>
                            <br />
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas suscipit, ex sed vestibulum blandit, eros mi congue ligula, non faucibus dolor odio ut lectus. In vestibulum tempus tortor. 	</p>
                            <div class="payOptForm">
                                <form action="javascript:void(0);" method="get">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Card Number</label>
                                            <input type="text" placeholder="Enter" />
                                        </div>
                                        <div class="col-md-6">
                                            <label>Card Holder Name</label>
                                            <input type="text" placeholder="Enter" />
                                        </div>
                                        <div class="col-md-6">
                                            <label>Expiry</label>
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <input type="text" placeholder="Month" />
                                                </div>
                                                <div class="col-xs-6">
                                                    <input type="text" placeholder="Year" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Security Code</label>
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <input type="text" placeholder="Enter CVC" />
                                                </div>
                                                <div class="col-xs-6">
                                                    <button type="submit" class="btn btn-success">Pay Now!</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="payTab_4">
                            <i class="fa fa-lock" aria-hidden="true"></i>
                            <br />
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas suscipit, ex sed vestibulum blandit, eros mi congue ligula, non faucibus dolor odio ut lectus. In vestibulum tempus tortor. 	</p>
                            <div class="payOptForm">
                                <form action="javascript:void(0);" method="get">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Card Number</label>
                                            <input type="text" placeholder="Enter" />
                                        </div>
                                        <div class="col-md-6">
                                            <label>Card Holder Name</label>
                                            <input type="text" placeholder="Enter" />
                                        </div>
                                        <div class="col-md-6">
                                            <label>Expiry</label>
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <input type="text" placeholder="Month" />
                                                </div>
                                                <div class="col-xs-6">
                                                    <input type="text" placeholder="Year" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Security Code</label>
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <input type="text" placeholder="Enter CVC" />
                                                </div>
                                                <div class="col-xs-6">
                                                    <button type="submit" class="btn btn-success">Pay Now!</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ck_order_sum">
                    <div class="whiteBox">
                        <p>Order Summary</p>
                        <table class="table">
                            <tbody>
                            <tr>
                                <td class="imgBox text-center"><img src="images/product_2.jpg" alt="Pro" height="144" width="111" </td>
                                <td class="text-left">
                                    <h3>Agarwood</h3>
                                    Qty: 1
                                </td>
                                <td class="text-right">120.00 SR</td>
                            </tr>
                            <tr>
                                <td class="imgBox text-center"><img src="images/product_3.jpg" alt="Pro" height="144" width="111" </td>
                                <td class="text-left">
                                    <h3>Agarwood</h3>
                                    Qty: 1
                                </td>
                                <td class="text-right">120.00 SR</td>
                            </tr>
                            <tr>
                                <td class="imgBox text-center"><img src="images/product_1.jpg" alt="Pro" height="144" width="111" </td>
                                <td class="text-left">
                                    <h3>Agarwood</h3>
                                    Qty: 1
                                </td>
                                <td class="text-right">120.00 SR</td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th class="text-right" colspan="2">Fast Shipping</th>
                                <th class="text-right">20.00 SR</th>
                            </tr>
                            <tr>
                                <th class="text-right" colspan="2">Taxes</th>
                                <th class="text-right">0.00 SR</th>
                            </tr>
                            </tfoot>
                        </table>
                        <p>Total Cost <span>(3 Products)</span></p>
                        <h3 class="price">200.00 <span class="curr">SAR</span> </h3>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </section>