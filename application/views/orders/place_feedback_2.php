<section>
	<div class="defaultPageSty">
		<div class="container max960_cont">
			<h1><?php echo $settings_settings;?></h1>
			<div class="settingPgLinks">
				<?php $this->load->view('user/settinglinks'); ?>
			</div>
			<h1><?php echo $settings_rate_order;?></h1>
		</div>
	</div>
		<div class="line"></div>
        <form action="<?php echo lang_base_url();?>my_orders/placeFeedbackSave" class="return_form" method="post">
        	<input type="hidden" name="product_id" value="<?php echo $order['product_id'];?>" />
            <input type="hidden" name="order_id" value="<?php echo $order['order_id'];?>" />
            <input type="hidden" name="order_date" value="<?php echo $order['order_date'];?>" />
            <input type="hidden" name="user_id" value="<?php echo $order['user_id'];?>" />
			<div class="retunExSec">
                <div class="container max960_cont">
                    <div class="productSecFive placeFeedBack2">
                        <div class="row">
                            <ul>
                                <li class="retunConfirmation">
                                    <p><label><?php echo $place_feedback2_order_review;?></label></p>
                                    <p>
                                        <label><?php echo $place_feedback2_order_number;?>: <?php echo str_pad($order['order_id'], 10, "0", STR_PAD_LEFT);?></label><br />
                                        <label><?php echo $place_feedback2_order_date;?>: <?php echo dateFormat($order['order_date']);?></label>
                                    </p>
                                </li>
                                <li>
                                    <?php $prod_image = getProductImages($order['product_id']);
                                          if(!empty($prod_image))
                                          {
                                              $image_name = $prod_image[0][$lang.'_image'];
                                          }
                                          else
                                          {
                                              $image_name = 'no_imge.png';
                                          }	?>
                                    <div class="imgBox"><img src="<?php echo base_url();?>uploads/images/products/<?php echo $image_name;?>" alt="Product" height="199" width="129" /></div>
                                    <h4 class="pull-right"><?php echo $order['price'];?>&nbsp;<?php echo getPriceCur('', true);?></h4>
                                    <div class="startSec pull-left">
                                        <div class="stars stars-example-bootstrap">
                                          <?php $rate = getAverageRating($product['id']);?>
                                          <select class="example-bootstrap" autocomplete="off" onchange="rateProduct(this.value, '<?php echo $product['id']; ?>');">
                                            <option <?php echo ($rate == 1 ? 'selected' : '');?> value="1">1</option>
                                            <option <?php echo ($rate == 2 ? 'selected' : '');?> value="2">2</option>
                                            <option <?php echo ($rate == 3 ? 'selected' : '');?> value="3">3</option>
                                            <option <?php echo ($rate == 4 ? 'selected' : '');?> value="4">4</option>
                                            <option <?php echo ($rate == 5 ? 'selected' : '');?> value="5">5</option>
                                          </select>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
			<div class="defaultPageSty">
                <div class="container max960_cont fontSize14">
                     <?php foreach($questions as $ques){?>
                        <p><?php echo $ques[$lang.'_question'];?><br /></p>
                        <input type="hidden" name="question_id[]" value="<?php echo $ques['id'];?>" />
                        <div class="row margin-botom20">
                            <div class="col-md-12 col-sm-12 col-xs-12 bigCheckBox shoelabelInline checkBoxReturn">
                                <?php $answer = getAnswers($ques['id']);
                                    $an = 1;
                                    foreach($answer as $ans)
                                    {
                                ?>
                                <div <?php echo ($an == 1 ? 'class="margin-right-zero"' : '');?> ><input id="checkbox_<?php echo $ans['id'];?>" type="radio" name="answer_<?php echo $ques['id'];?>" value="<?php echo $ans['id'];?>">&nbsp;<label for="checkbox_<?php echo $ans['id'];?>"><span></span><?php echo $ans[$lang.'_answer']?></label></div>
                                <?php $an++;}?>
                            </div>
                        </div>
                    <?php }?>
                </div>
            </div>
			<div class="container max960_cont textareaNbtn">
                <div class="row">
                    <div class="col-md-3 col-sm-4 col-xs-4 text-left">
                    <p>&nbsp;</p><p>&nbsp;</p>
                        <input type="submit" value="<?php echo $register_send;?>" class="edBtn red">
                    </div>
                    <div class="col-md-9 col-sm-8 col-xs-8">
                        <textarea class="feedbackText" name="other_comments" placeholder="<?php echo $place_feedback2_other_comments;?>" ></textarea>
                    </div>
                </div>
            </div>
       	</form>
</section>