<section>
	<div class="defaultPageSty">
		<div class="container max960_cont">
			<h1><?php echo $settings_settings;?></h1>
			<div class="settingPgLinks">
				<?php $this->load->view('user/settinglinks'); ?>
			</div>
			<h1><?php echo $settings_return_order;?></h1>
		</div>
	</div>
	<div class="line"></div>
    <form action="<?php echo lang_base_url();?>my_orders/returnOrderConfirmation" class="return_form" method="post">
    	<input type="hidden" name="order_id" value="<?php echo $order_id?>" />
        <input type="hidden" name="return_id" value="<?php echo $return_id?>" />
        <div class="retunExSec">
            <div class="container max960_cont">
                <label><?php echo $return_change2_return_order;?></label>
                <div class="productSecFive">
                    <div class="row">
                        <ul>
                            <li>
                                <?php 
                                  $prod_image = getProductImages($order['product_id']);
                                  if(!empty($prod_image))
                                  {
                                      $image_name = $prod_image[0][$lang.'_image'];
                                  }
                                  else
                                  {
                                      $image_name = 'no_imge.png';
                                  }	
                                ?>
                                <div class="imgBox"><img src="<?php echo base_url();?>uploads/images/products/<?php echo $image_name;?>" alt="Product" height="199" width="129" /></div>
                                <h4 class="pull-right"><?php echo $order['price'];?> <span><?php echo getPriceCur($order['price'], true);?></span></h4>
                                <div class="startSec pull-left">
                                    <div class="stars stars-example-bootstrap">
                                      <?php $rate = getAverageRating($product['id']);?>
                                      <select class="example-bootstrap" autocomplete="off" onchange="rateProduct(this.value, '<?php echo $product['id']; ?>');">
                                        <option <?php echo ($rate == 1 ? 'selected' : '');?> value="1">1</option>
                                        <option <?php echo ($rate == 2 ? 'selected' : '');?> value="2">2</option>
                                        <option <?php echo ($rate == 3 ? 'selected' : '');?> value="3">3</option>
                                        <option <?php echo ($rate == 4 ? 'selected' : '');?> value="4">4</option>
                                        <option <?php echo ($rate == 5 ? 'selected' : '');?> value="5">5</option>
                                      </select>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="defaultPageSty">
            <div class="container max960_cont">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <select name="order_filter">
                            <?php foreach($filters as $filter){?>
                                <option <?php if($filter_val == $filter['duration'].'|'.$filter['type']){ ?> selected="selected" <?php }?> value="<?php echo $filter['id'];?>"><?php echo $filter[$lang.'_name'];?></option>
                            <?php }?>
                        </select>
                    </div>
                    <!--<div class="col-md-7 col-sm-7 col-xs-12 bigCheckBox shoelabelInline checkBoxReturn">
                        <?php echo $return_change2_via;?>
                        <div><input id="checkbox_a" type="radio" name="checkbox" value="1"><label for="checkbox_a"><span></span><img src="<?php echo base_url();?>assets/frontend/images/rc2_img_1.png" alt="img" height="30" width="110" /></label></div>
                        <div><input id="checkbox_b" type="radio" name="checkbox" value="1"><label for="checkbox_b"><span></span><img src="<?php echo base_url();?>assets/frontend/images/rc2_img_2.png" alt="img" height="30" width="110" /></label></div>
                    </div>-->
                </div>
                <p>&nbsp;</p>
                <!--<div class="row">
                    <div class="col-md-7 col-sm-7 col-xs-12">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <label><?php echo $register_first_name;?></label>
                                <input type="text" />
                            </div>
            
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <label><?php echo $checkout_card_number;?></label>
                                <input type="text" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-5 col-xs-12">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <label><?php echo $checkout_expiry_date;?></label>
                                <select>
                                    <option>السنة</option>
                                    <option>السنة</option>
                                    <option>السنة</option>
                                </select>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <label>&nbsp;</label>
                                <select>
                                    <option>الشهر</option>
                                    <option>الشهر</option>
                                    <option>الشهر</option>
                                </select>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <label>C V C</label>
                                <input type="text" />
                            </div>
                        </div>
                    </div>
                </div>-->
            </div>
        </div>
        <div class="container max960_cont">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 text-left">
                    <input type="submit" value="<?php echo $register_send;?>" class="edBtn red">
                </div>
            </div>
        </div>
	</form>
</section>