<section>
	<div class="defaultPageSty">
		<div class="container max960_cont">
			<h1><?php echo $settings_settings;?></h1>
			<div class="settingPgLinks">
				<?php $this->load->view('user/settinglinks'); ?>
			</div>
			<h1><?php echo $settings_return_order;?></h1>
		</div>
	</div>
		<div class="line"></div>
        <?php echo $this->session->flashdata('message');?>
	<div class="retunExSec">
		<div class="container max960_cont">
			<label><?php echo $return_change_return_process;?></label>
			<form action="<?php echo lang_base_url();?>my_orders/returnOrder" method="post" id="filter_form">
                <select onchange="document.getElementById('filter_form').submit();" name="filter" id="filter">
                    <?php foreach($filters as $filter){?>
                        <option <?php if($filter_val == $filter['duration'].'|'.$filter['type']){ ?> selected="selected" <?php }?> value="<?php echo $filter['duration'].'|'.$filter['type'];?>"><?php echo $filter[$lang.'_name'];?></option>
                    <?php }?>
                </select>
            </form>
			<div class="productSecFive">
				<div class="row">
					<ul>
                    <?php 
						foreach($orders as $ord){
							
							  $prod_image = getProductImages($ord['product_id']);
							  if(!empty($prod_image))
							  {
								  $image_name = $prod_image[0][$lang.'_image'];
							  }
							  else
							  {
								  $image_name = 'no_imge.png';
							  }	
					?>
						<li>
                        	<div class="checkBoxReturn"><input id="checkbox_<?php echo $ord['order_id'];?>_<?php echo $ord['product_id'];?>" type="radio" name="order_idsED" value="<?php echo $ord['product_id'];?>"><label for="checkbox_<?php echo $ord['order_id'];?>_<?php echo $ord['product_id'];?>"><span></span></label></div>
                        	<form action="<?php echo lang_base_url();?>my_orders/returnOrderSubmit" class="return_form" method="post">
                            	<input type="hidden" name="order_id" value="<?php echo $ord['order_id'];?>" />   
                                <input type="hidden" name="product_id" value="<?php echo $ord['product_id'];?>" /> 
                                <input type="hidden" name="filter_val" value="<?php echo $filter_val;?>" />  
                                <div class="imgBox"><img src="<?php echo base_url();?>uploads/images/products/<?php echo $image_name;?>" alt="Product" height="199" width="129" /></div>
                                <h4><?php echo $ord['price'];?> <span><?php echo getPriceCur($ord['price'], true);?></span></h4>
                                <div class="startSec">
                                    <div class="stars stars-example-bootstrap">
									  <?php $rate = getAverageRating($product['id']);?>
                                      <select class="example-bootstrap" autocomplete="off" onchange="rateProduct(this.value, '<?php echo $product['id']; ?>');">
                                        <option <?php echo ($rate == 1 ? 'selected' : '');?> value="1">1</option>
                                        <option <?php echo ($rate == 2 ? 'selected' : '');?> value="2">2</option>
                                        <option <?php echo ($rate == 3 ? 'selected' : '');?> value="3">3</option>
                                        <option <?php echo ($rate == 4 ? 'selected' : '');?> value="4">4</option>
                                        <option <?php echo ($rate == 5 ? 'selected' : '');?> value="5">5</option>
                                      </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="whyReturnSml">
                                    <div class="text-right">
                                        <select name="reason">
                                            <option value="<?php echo $return_change_better_price;?>"><?php echo $return_change_better_price;?></option>
                                        </select>
                                    </div>
                                    <textarea name="other_comments" placeholder="<?php echo $return_change_other_comments?>" ></textarea>
                                    <input type="submit" class="edBtn red" value="<?php echo $register_send;?>" /> 
                                </div>
                        	</form>
						</li>
                    <?php }?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>