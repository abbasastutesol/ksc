<section>
	<div class="defaultPageSty">
		<div class="container max960_cont">
			<h1><?php echo $settings_settings;?></h1>
			<div class="settingPgLinks">
				<?php $this->load->view('user/settinglinks'); ?>
			</div>
			<h1><?php echo $settings_rate_order;?></h1>
		</div>
	</div>
		<div class="line"></div>
        <p style="text-align:center; color:#0C0"><?php echo $this->session->flashdata('message');?></p>
	<div class="retunExSec">
		<div class="container max960_cont">
			<label><?php echo $return_change_return_process;?></label>
			<P>&nbsp;</P>
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
                	<form action="<?php echo lang_base_url();?>my_orders/placeFeedback" method="post" id="filter_form">
                        <select onchange="document.getElementById('filter_form').submit();" name="filter" id="filter">
                            <?php foreach($filters as $filter){?>
                                <option <?php if($filter_val == $filter['duration'].'|'.$filter['type']){ ?> selected="selected" <?php }?> value="<?php echo $filter['duration'].'|'.$filter['type'];?>"><?php echo $filter[$lang.'_name'];?></option>
                            <?php }?>
                        </select>
                    </form>
				</div>
			</div>
			<div class="productSecFive">
				<div class="row">
					<ul>
                    <?php 
						foreach($orders as $ord){
							
							  $prod_image = getProductImages($ord['product_id']);
							  if(!empty($prod_image))
							  {
								  $image_name = $prod_image[0][$lang.'_image'];
							  }
							  else
							  {
								  $image_name = 'no_imge.png';
							  }	
					?>
						<li>
							<form action="<?php echo lang_base_url();?>my_orders/placeFeedbackSubmit" class="return_form" method="post">
                                    <input type="hidden" name="order_id" value="<?php echo $ord['order_id'];?>" />
                                    <div class="imgBox"><img src="<?php echo base_url();?>uploads/images/products/<?php echo $image_name;?>" alt="Product" height="199" width="129" /></div>
                                    <h4 class="pull-right"><?php echo $ord['price'];?> <span><?php echo getPriceCur($ord['price'], true);?></span></h4>
                                    <div class="startSec pull-left">
                                        <div class="stars stars-example-bootstrap">
										  <?php $rate = getAverageRating($product['id']);?>
                                          <select class="example-bootstrap" autocomplete="off" onchange="rateProduct(this.value, '<?php echo $product['id']; ?>');">
                                            <option <?php echo ($rate == 1 ? 'selected' : '');?> value="1">1</option>
                                            <option <?php echo ($rate == 2 ? 'selected' : '');?> value="2">2</option>
                                            <option <?php echo ($rate == 3 ? 'selected' : '');?> value="3">3</option>
                                            <option <?php echo ($rate == 4 ? 'selected' : '');?> value="4">4</option>
                                            <option <?php echo ($rate == 5 ? 'selected' : '');?> value="5">5</option>
                                          </select>
                                        </div>
                                    </div>
                                    <div class="placeFeedBtn">
                                        <input type="submit" class="edBtn" value="! <?php echo $place_feedback_evaluate_order;?>" />
                                    </div>
                        		</form>
						</li>
                     <?php }?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>