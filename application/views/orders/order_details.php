<section>
	<!-- Tab panes -->
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="requests">
			<div class="container">
              	<?php 
				foreach($orders as $ord){
					
    				  $prod_image = getProductImages($ord['product_id']);
					  if(!empty($prod_image))
					  {
						  $image_name = $prod_image[0][$lang.'_image'];
					  }
					  else
					  {
						  $image_name = 'no_imge.png';
					  }	
					 ?>
                    <div class="myOrderList">
                        <div class="max960_cont">
                            <div class="row">
                                <div class="col-md-2 col-sm-2 col-xs-6 mainTitle">
                                    <div class="center-block"><?php echo $my_orders_order_id;?></div>
                                    <span class="english"><?php echo str_pad($ord['order_id'], 10, "0", STR_PAD_LEFT);?></span>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-6 imgBox"><img src="<?php echo base_url();?>uploads/images/products/<?php echo $image_name;?>" alt="img" /></div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <h3><?php echo getProdCategory($ord['category_id']);?></h3>
                                           <?php echo $ord[$lang.'_name'];?>
                                            <div class="startSec">
                                                <a href="javascript:void(0);" class="active" tabindex="0"><i aria-hidden="true" class="fa fa-star"></i></a>
                                                <a href="javascript:void(0);" class="active" tabindex="0"><i aria-hidden="true" class="fa fa-star"></i></a>
                                                <a href="javascript:void(0);" class="active" tabindex="0"><i aria-hidden="true" class="fa fa-star"></i></a>
                                                <a href="javascript:void(0);" tabindex="0"><i aria-hidden="true" class="fa fa-star"></i></a>
                                                <a href="javascript:void(0);" tabindex="0"><i aria-hidden="true" class="fa fa-star"></i></a>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <strong><?php echo $ord['price'];?></strong> <?php echo getPriceCur($ord['price'], true);?>
                                        </div>
                                        <div class="col-xs-4">
                                            <?php echo $shopping_cart_quantity;?>
                                            <strong class="font13"><?php echo $ord['quantity'];?></strong>
                                        </div>
                                    </div>
                                    <div class="row borderStyle">
                                        <div class="col-xs-4">
                                            <?php echo $request_message;?> <span class="english small"><?php echo ($lang == 'eng' ? date('d-m-Y', strtotime($ord['created_at'])) : convertToArabic(date('d-m-Y', strtotime($ord['created_at']))) );?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-12">&nbsp;</div>
                            </div>
                        </div>
                    </div>
                <?php }?>
			</div>
           <!-- <div class="myOrderList">
            	<div class="row borderStyle">
                    <div class="col-xs-12 text-center">
                        <a href="javascript:void(0);"><h2><strong style="font-size: 20px;"><?php echo $my_orders_bill;?></strong></a>
                    </div>	
                </div>
            </div>	-->	
			<p>&nbsp;</p>
			<p>&nbsp;</p>
		</div>
	</div>
		
			
	<div class="line"></div>
	<div class="container">
		<div class="productSecFive playSlide green">
			<h2><?php echo $products_recently_viewed_products;?></h2>
			<div class="row">
				<ul>
					<?php 
					$recently_viewed = getRecentViewed();
					foreach($recently_viewed as $product){
						  $image_arr = getProductImages($product['id']);
								?>
					  <li>
						  <div class="imgBox">
							  <?php if(sizeof($image_arr) > 1){?>						
								  <div id="Product_id_<?php echo $main;?>" class="carousel slide cntCenter" data-ride="carousel"> 															
									  <!-- Wrapper for slides -->
									  <div class="carousel-inner" role="listbox">
										  <?php 
										  $im=0;
										  foreach($image_arr as $image){?>
											  <div class="item<?php if($im==0){ echo ' active';}?>"> <a href="<?php echo lang_base_url().'product/product_page/'.$product['id'];?>"><img src="<?php echo base_url();?>uploads/images/products/<?php echo $image[$lang.'_image']?>" alt="Product" height="199" width="129" ></a>
											  </div>
										  <?php
										  $im++; 
										  }?>                                                   
									  </div>
								  </div>
							  <?php }else{ ?>
								  <?php if($image_arr){
									  foreach($image_arr as $image){?>
									  
									 <a href="<?php echo lang_base_url().'product/product_page/'.$product['id'];?>"> <img src="<?php echo base_url();?>uploads/images/products/<?php echo $image[$lang.'_image']?>" alt="Product" height="199" width="129" /></a>
								  <?php 
									  }
								  } else
								  { ?>
									 <a href="<?php echo lang_base_url().'product/product_page/'.$product['id'];?>"> <img src="<?php echo base_url();?>uploads/images/products/no_imge.png" alt="Product" height="199" width="129" /></a>
								  <?php }?>
							  <?php } ?>
						  </div>
						  <!-- Indicators -->
						  <?php if(sizeof($image_arr) > 1){?>
							  <ol class="carousel-indicators">
							  <?php 
							  $imo=0;
							  foreach($image_arr as $image){?>
								  <li data-target="#Product_id_<?php echo $main;?>" data-slide-to="<?php echo $imo;?>"<?php if($imo==0){ echo ' class="active"';}?>></li>
							  <?php 
							  $imo++; 
							  }
							  ?>
						  </ol>	
						  <?php }?>	
						  <h3><a href="<?php echo lang_base_url().'product/product_page/'.$product['id'];?>"><?php echo $product[$lang.'_name']?></a></h3>
						  <?php  
									  if($product['has_discount_price'])
									  {
										   echo '<h4><span class="discount">'.$product[$lang.'_price'].'</span>'.getPriceCur($product[$lang.'_discount_price']).'</h4>';
									  }
									  else
									  {
										  echo '<h4>'.getPriceCur($product[$lang.'_price']).'</h4>';
									  }
									  
									  ?>
					  </li>
				  <?php 
				  $main++;
				  }?>
				</ul>
			</div>
		</div>
	</div>

</section>