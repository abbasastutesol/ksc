<section>
	<div class="defaultPageSty">
		<div class="container max960_cont">
			<h1><?php echo $settings_settings;?></h1>
			<div class="settingPgLinks">
				<?php $this->load->view('user/settinglinks'); ?>
			</div>
			<h1><?php echo $settings_return_order;?></h1>
		</div>
	</div>
	<div class="line"></div>
    <form action="<?php echo lang_base_url();?>my_orders/saveConfirmation" class="return_form" method="post"> 
    	<input type="hidden" name="order_id" value="<?php echo $order_id?>" />
        <input type="hidden" name="return_id" value="<?php echo $return_id?>" />   
        <div class="retunExSec">
            <div class="container max960_cont">
                <label><?php echo $return_change2_return_order;?></label>
                <div class="productSecFive">
                    <div class="row">
                        <ul>
                            <li class="retunConfirmation">
                                <?php 
                                      $prod_image = getProductImages($order['product_id']);
                                      if(!empty($prod_image))
                                      {
                                          $image_name = $prod_image[0][$lang.'_image'];
                                      }
                                      else
                                      {
                                          $image_name = 'no_imge.png';
                                      }	
                                ?>
                                <p><label><?php echo $return_change_confirm_account_review;?></label></p>
                                <p>
                                    <label><?php echo $return_change_confirm_total_return;?> : </label><strong><?php echo $order['price'];?></strong> <?php echo strip_tags(getPriceCur($order['price'], true));?>
                                    <span><label><?php echo $return_change_confirm_shipping_through.' '.getShipmentMethod($order['shipment_method']);?> : </label><strong><?php echo $order['shipment_price'];?></strong> <?php echo strip_tags(getPriceCur($order['shipment_price'], true));?></span>
                                </p>
                                <p><label><?php echo $return_change_confirm_amount_due;?> : </label><strong><?php echo ($order['price']-$order['shipment_price']);?></strong> <?php echo strip_tags(getPriceCur('', true));?></p>
                                <input type="hidden" name="total_return_amount" value="<?php echo ($order['price']-$order['shipment_price']);?>" />
                            </li>
                            <li>
                                <div class="imgBox"><img src="<?php echo base_url();?>uploads/images/products/<?php echo $image_name;?>" alt="Product" height="199" width="129" /></div>
                                <h4 class="pull-right"> <?php echo $order['price'];?><?php echo getPriceCur('', true);?></h4>
                                <div class="startSec pull-left">
                                    <div class="stars stars-example-bootstrap">
                                      <?php $rate = getAverageRating($product['id']);?>
                                      <select class="example-bootstrap" autocomplete="off" onchange="rateProduct(this.value, '<?php echo $product['id']; ?>');">
                                        <option <?php echo ($rate == 1 ? 'selected' : '');?> value="1">1</option>
                                        <option <?php echo ($rate == 2 ? 'selected' : '');?> value="2">2</option>
                                        <option <?php echo ($rate == 3 ? 'selected' : '');?> value="3">3</option>
                                        <option <?php echo ($rate == 4 ? 'selected' : '');?> value="4">4</option>
                                        <option <?php echo ($rate == 5 ? 'selected' : '');?> value="5">5</option>
                                      </select>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="defaultPageSty">
            <div class="container max960_cont">
                <p><?php echo $return_change2_via;?> : <br /></p>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 bigCheckBox shoelabelInline checkBoxReturn">
                        <div style="margin-right:0;"><input id="checkbox_a" type="radio" name="shipment_method" value="1"><label for="checkbox_a"><span></span><?php echo $return_change_confirm_aramex;?></label></div>
                        <div><input id="checkbox_b" type="radio" name="shipment_method" value="2"><label for="checkbox_b"><span></span>DHL</label></div>
                    </div>
                </div>
                <p>&nbsp;</p>			
            </div>
        </div>
        <div class="container max960_cont">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 text-left">
                    <input type="submit" value="<?php echo $register_send;?>" class="edBtn red">
                </div>
            </div>
        </div>
    </form>
</section>