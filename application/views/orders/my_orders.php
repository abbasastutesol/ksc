<section>

	<!-- Nav tabs -->
	<div class="myOrdersTabing">
		<div class="container max960_cont">
			<ul class="" role="tablist">
				<li role="presentation" class="active"><a href="#requests" aria-controls="requests" role="tab" data-toggle="tab"><?php echo $my_orders_my_orders;?></a></li>
				<li role="presentation"><a href="#waiting_orders" aria-controls="waiting_orders" role="tab" data-toggle="tab"><?php echo $my_orders_pending_orders;?></a></li>
				<li role="presentation"><a href="#completed_orders" aria-controls="completed_orders" role="tab" data-toggle="tab"><?php echo $my_orders_completed_orders;?></a></li>
				<li role="presentation"><a href="#cancellation_orders" aria-controls="cancellation_orders" role="tab" data-toggle="tab"><?php echo $my_orders_canceled_orders;?></a></li>
			</ul>
		</div>
	</div>
	<!-- Tab panes -->
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="requests">
			<div class="container">
              	<?php
				foreach($orders as $ord){

    				  $prod_image = getProductImages($ord['product_id']);
					  if(!empty($prod_image))
					  {
						  $image_name = $prod_image[0][$lang.'_image'];
					  }
					  else
					  {
						  $image_name = 'no_imge.png';
					  }
					 ?>
                    <div class="myOrderList">
                        <div class="max960_cont">
                            <div class="row">
                                <div class="col-md-2 col-sm-2 col-xs-6 mainTitle">
                                    <div class="center-block"><?php echo $my_orders_order_id;?></div>
                                    <span class="english"><?php echo str_pad($ord['order_id'], 10, "0", STR_PAD_LEFT);?></span>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-6 imgBox"><img src="<?php echo base_url();?>uploads/images/products/<?php echo $image_name;?>" alt="img" /></div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <h3><?php echo getProdCategory($ord['category_id']);?></h3>
                                           <?php echo $ord[$lang.'_name'];?>
                                           <?php $rate = getAverageRating($ord['product_id']);?>
                                            <div class="startSec">
                                            	<?php for($i=0; $i<5; $i++){?>
                                                	<a href="javascript:void(0);" <?php if($i<$rate){?>class="active"<?php }?> tabindex="0"><i aria-hidden="true" class="fa fa-star"></i></a>						<?php }?>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <strong><?php echo $ord['price'];?></strong> <?php echo getPriceCur($ord['price'], true);?>
                                        </div>
                                        <div class="col-xs-4">
                                            <?php echo $shopping_cart_quantity;?>
                                            <strong class="font13"><?php echo $ord['quantity'];?></strong>
                                        </div>
                                    </div>
                                    <div class="row borderStyle">
                                        <div class="col-xs-4">
                                            <?php echo $request_message;?>  <span class="english"><?php echo ($lang == 'eng' ? date('d-m-Y', strtotime($ord['created_at'])) : convertToArabic(date('d-m-Y', strtotime($ord['created_at']))) );?></span>
                                        </div>
                                        <div class="col-xs-4">
                                            <a href="<?php echo lang_base_url();?>my_orders/details/<?php echo $ord['order_id'];?>"><?php echo $my_orders_order_details;?></a>
                                        </div>
                                        <!--<div class="col-xs-4">
                                            <a href="javascript:void(0);"><?php echo $my_orders_bill;?></a>
                                        </div>-->
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-12">&nbsp;</div>
                            </div>
                        </div>
                    </div>
                <?php }?>
			</div>
			<p>&nbsp;</p>
			<p>&nbsp;</p>
		</div>
		<div role="tabpanel" class="tab-pane" id="waiting_orders">
			<div class="container">
			<?php
				foreach($orders as $ord){

					  if($ord['order_status'] != 1)
					  	continue;

    				  $prod_image = getProductImages($ord['product_id']);
					  if(!empty($prod_image))
					  {
						  $image_name = $prod_image[0][$lang.'_image'];
					  }
					  else
					  {
						  $image_name = 'no_imge.png';
					  }
					 ?>
                    <div class="myOrderList">
                        <div class="max960_cont">
                            <div class="row">
                                <div class="col-md-2 col-sm-2 col-xs-6 mainTitle">
                                    <div class="center-block"><?php echo $my_orders_order_id;?></div>
                                    <span class="english"><?php echo str_pad($ord['order_id'], 10, "0", STR_PAD_LEFT);?></span>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-6 imgBox"><img src="<?php echo base_url();?>uploads/images/products/<?php echo $image_name;?>" alt="img" /></div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <h3><?php echo getProdCategory($ord['category_id']);?></h3>
                                           <?php echo $ord[$lang.'_name'];?>
                                            <?php $rate = getAverageRating($ord['product_id']);?>
                                            <div class="startSec">
                                            	<?php for($i=0; $i<5; $i++){?>
                                                	<a href="javascript:void(0);" <?php if($i<$rate){?>class="active"<?php }?> tabindex="0"><i aria-hidden="true" class="fa fa-star"></i></a>						<?php }?>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <strong><?php echo $ord['price'];?></strong> <?php echo getPriceCur($ord['price'], true);?>
                                        </div>
                                        <div class="col-xs-4">
                                            <?php echo $shopping_cart_quantity;?>
                                            <strong class="font13"><?php echo $ord['quantity'];?></strong>
                                        </div>
                                    </div>
                                    <div class="row borderStyle">
                                        <div class="col-xs-4">
                                            <?php echo $request_message;?> <span class="english"><?php echo ($lang == 'eng' ? date('d-m-Y', strtotime($ord['created_at'])) : convertToArabic(date('d-m-Y', strtotime($ord['created_at']))) );?></span>
                                        </div>
                                        <div class="col-xs-4">
                                            <a href="<?php echo lang_base_url();?>my_orders/details/<?php echo $ord['order_id'];?>"><?php echo $my_orders_order_details;?></a>
                                        </div>
                                        <!--<div class="col-xs-4">
                                            <a href="javascript:void(0);"><?php echo $my_orders_bill;?></a>
                                        </div>-->
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-12">&nbsp;</div>
                            </div>
                        </div>
                    </div>
                <?php }?>
			</div>
			<p>&nbsp;</p>
		</div>
		<div role="tabpanel" class="tab-pane" id="completed_orders">
			<div class="container">
				<?php
				foreach($orders as $ord){

					  if($ord['order_status'] != 2)
					  	continue;

    				  $prod_image = getProductImages($ord['product_id']);
					  if(!empty($prod_image))
					  {
						  $image_name = $prod_image[0][$lang.'_image'];
					  }
					  else
					  {
						  $image_name = 'no_imge.png';
					  }
					 ?>
                    <div class="myOrderList">
                        <div class="max960_cont">
                            <div class="row">
                                <div class="col-md-2 col-sm-2 col-xs-6 mainTitle">
                                    <div class="center-block"><?php echo $my_orders_order_id;?></div>
                                    <span class="english"><?php echo str_pad($ord['order_id'], 10, "0", STR_PAD_LEFT);?></span>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-6 imgBox"><img src="<?php echo base_url();?>uploads/images/products/<?php echo $image_name;?>" alt="img" /></div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <h3><?php echo getProdCategory($ord['category_id']);?></h3>
                                           <?php echo $ord[$lang.'_name'];?>
                                            <?php $rate = getAverageRating($ord['product_id']);?>
                                            <div class="startSec">
                                            	<?php for($i=0; $i<5; $i++){?>
                                                	<a href="javascript:void(0);" <?php if($i<$rate){?>class="active"<?php }?> tabindex="0"><i aria-hidden="true" class="fa fa-star"></i></a>						<?php }?>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <strong><?php echo $ord['price'];?></strong> <?php echo getPriceCur($ord['price'], true);?>
                                        </div>
                                        <div class="col-xs-4">
                                            <?php echo $shopping_cart_quantity;?>
                                            <strong class="font13"><?php echo $ord['quantity'];?></strong>
                                        </div>
                                    </div>
                                    <div class="row borderStyle">
                                        <div class="col-xs-4">
                                            <?php echo $request_message;?> <span class="english"><?php echo ($lang == 'eng' ? date('d-m-Y', strtotime($ord['created_at'])) : convertToArabic(date('d-m-Y', strtotime($ord['created_at']))) );?></span>
                                        </div>
                                        <div class="col-xs-4">
                                            <a href="<?php echo lang_base_url();?>my_orders/details/<?php echo $ord['order_id'];?>"><?php echo $my_orders_order_details;?></a>
                                        </div>
                                       <!-- <div class="col-xs-4">
                                            <a href="javascript:void(0);"><?php echo $my_orders_bill;?></a>
                                        </div>-->
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-12">&nbsp;</div>
                            </div>
                        </div>
                    </div>
                <?php }?>
			</div>
			<p>&nbsp;</p>
		</div>
		<div role="tabpanel" class="tab-pane" id="cancellation_orders">
			<div class="container">
				<?php
				foreach($orders as $ord){

					  if($ord['order_status'] != 0)
					  	continue;

    				  $prod_image = getProductImages($ord['product_id']);
					  if(!empty($prod_image))
					  {
						  $image_name = $prod_image[0][$lang.'_image'];
					  }
					  else
					  {
						  $image_name = 'no_imge.png';
					  }
					 ?>
                    <div class="myOrderList">
                        <div class="max960_cont">
                            <div class="row">
                                <div class="col-md-2 col-sm-2 col-xs-6 mainTitle">
                                    <div class="center-block"><?php echo $my_orders_order_id;?></div>
                                    <span class="english"><?php echo str_pad($ord['order_id'], 10, "0", STR_PAD_LEFT);?></span>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-6 imgBox"><img src="<?php echo base_url();?>uploads/images/products/<?php echo $image_name;?>" alt="img" /></div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <h3><?php echo getProdCategory($ord['category_id']);?></h3>
                                           <?php echo $ord[$lang.'_name'];?>
                                            <?php $rate = getAverageRating($ord['product_id']);?>
                                            <div class="startSec">
                                            	<?php for($i=0; $i<5; $i++){?>
                                                	<a href="javascript:void(0);" <?php if($i<$rate){?>class="active"<?php }?> tabindex="0"><i aria-hidden="true" class="fa fa-star"></i></a>						<?php }?>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <strong><?php echo $ord['price'];?></strong> <?php echo getPriceCur($ord['price'], true);?>
                                        </div>
                                        <div class="col-xs-4">
                                            <?php echo $shopping_cart_quantity;?>
                                            <strong class="font13"><?php echo $ord['quantity'];?></strong>
                                        </div>
                                    </div>
                                    <div class="row borderStyle">
                                        <div class="col-xs-4">
                                            <?php echo $request_message;?> <span class="english"><?php echo ($lang == 'eng' ? date('d-m-Y', strtotime($ord['created_at'])) : convertToArabic(date('d-m-Y', strtotime($ord['created_at']))) );?></span>
                                        </div>
                                        <div class="col-xs-4">
                                            <a href="<?php echo lang_base_url();?>my_orders/details/<?php echo $ord['order_id'];?>"><?php echo $my_orders_order_details;?></a>
                                        </div>
                                        <!--<div class="col-xs-4">
                                            <a href="javascript:void(0);"><?php echo $my_orders_bill;?></a>
                                        </div>-->
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2 col-xs-12">&nbsp;</div>
                            </div>
                        </div>
                    </div>
                <?php }?>
			</div>
			<p>&nbsp;</p>
		</div>
	</div>


	<div class="line"></div>
	<div class="container">
		<div class="productSecFive playSlide green">
			<h2><?php echo $products_recently_viewed_products;?></h2>
			<div class="row">
				<ul>
					<?php
					$recently_viewed = getRecentViewed();
					foreach($recently_viewed as $product){
						  $image_arr = getProductImages($product['id']);
								?>
					  <li>
						  <div class="imgBox">
							  <?php if(sizeof($image_arr) > 1){?>
								  <div id="Product_id_<?php echo $main;?>" class="carousel slide cntCenter" data-ride="carousel">
									  <!-- Wrapper for slides -->
									  <div class="carousel-inner" role="listbox">
										  <?php
										  $im=0;
										  foreach($image_arr as $image){?>
											  <div class="item<?php if($im==0){ echo ' active';}?>"> <a href="<?php echo base_url().'product/product_page/'.$product['id'];?>"><img src="<?php echo base_url();?>uploads/images/products/<?php echo $image[$lang.'_image']?>" alt="Product" height="199" width="129" ></a>
											  </div>
										  <?php
										  $im++;
										  }?>
									  </div>
								  </div>
							  <?php }else{ ?>
								  <?php if($image_arr){
									  foreach($image_arr as $image){?>

									 <a href="<?php echo lang_base_url().'product/product_page/'.$product['id'];?>"> <img src="<?php echo base_url();?>uploads/images/products/<?php echo $image[$lang.'_image']?>" alt="Product" height="199" width="129" /></a>
								  <?php
									  }
								  } else
								  { ?>
									 <a href="<?php echo lang_base_url().'product/product_page/'.$product['id'];?>"> <img src="<?php echo base_url();?>uploads/images/products/no_imge.png" alt="Product" height="199" width="129" /></a>
								  <?php }?>
							  <?php } ?>
						  </div>
						  <!-- Indicators -->
						  <?php if(sizeof($image_arr) > 1){?>
							  <ol class="carousel-indicators">
							  <?php
							  $imo=0;
							  foreach($image_arr as $image){?>
								  <li data-target="#Product_id_<?php echo $main;?>" data-slide-to="<?php echo $imo;?>"<?php if($imo==0){ echo ' class="active"';}?>></li>
							  <?php
							  $imo++;
							  }
							  ?>
						  </ol>
						  <?php }?>
						  <h3><a href="<?php echo lang_base_url().'product/product_page/'.$product['id'];?>"><?php echo $product[$lang.'_name']?></a></h3>
						  <?php  
									  if($product['has_discount_price'])
									  {
										   echo '<h4><span class="discount">'.$product[$lang.'_price'].'</span>'.getPriceCur($product[$lang.'_discount_price']).'</h4>';
									  }
									  else
									  {
										  echo '<h4>'.getPriceCur($product[$lang.'_price']).'</h4>';
									  }
									  
									  ?>
					  </li>
				  <?php
				  $main++;
				  }?>
				</ul>
			</div>
		</div>
	</div>

</section>