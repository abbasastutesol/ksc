<div id="page_content">
        <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
            <h1 id="product_edit_name">Edit News</h1>
            <span class="uk-text-muted uk-text-upper uk-text-small" id="product_edit_sn"></span>
        </div>
        <div id="page_content_inner">
            <form action="<?php echo base_url(); ?>admin/latest_news/action" method="post" onsubmit="return false" class="uk-form-stacked ajax_form" id="product_edit_form" enctype="multipart/form-data">
				<input type="hidden" name="form_type" value="update">
				<input type="hidden" name="id" value="<?php echo $news->id; ?>">
                <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
                    
                    <div class="uk-width-xLarge-10-10  uk-width-large-10-10">
                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text">
                                    Details
                                </h3>
                            </div>
                            <div class="md-card-content large-padding">
                                <div class="uk-grid uk-grid-divider uk-grid-medium" data-uk-grid-margin>
                                	<div class="uk-width-large-1-2">
                                     </div>
                                     <div class="uk-width-large-1-2">
                                        <div class="uk-form-row">
                                        </div>
                                     </div>
                                    <div class="uk-width-large-1-2">

                                        <label for="eng_description">English Title</label>
                                        <div class="uk-form-row">

                                            <input type="text" class="md-input" name="eng_title" value="<?php echo $news->eng_title; ?>">
                                        </div>


                                        <label for="eng_description">English Short Description</label>
										<div class="uk-form-row">
                                            
                                            <textarea class="md-input eng_text" name="eng_short_des" id="eng_short_des" cols="30" rows="4"><?php echo $news->eng_short_des; ?></textarea>
                                        </div>

                                        <label for="eng_description">English Long Description</label>
                                        <div class="uk-form-row">

                                            <textarea class="md-input eng_text" name="eng_long_des" id="eng_long_des" cols="30" rows="4"><?php echo $news->eng_long_des; ?></textarea>
                                        </div>

                                    </div>
                                    <div class="uk-width-large-1-2">

                                        <label for="eng_description">Arabic Title</label>
                                        <div class="uk-form-row">

                                            <input type="text" class="md-input" name="arb_title" value="<?php echo $news->arb_title; ?>">
                                        </div>


                                        <label for="arb_description">Arabic Short Description</label>
										<div class="uk-form-row">
                                            
                                            <textarea class="md-input arb_text" name="arb_short_des" id="arb_short_des" cols="30" rows="4"><?php echo $news->arb_short_des; ?></textarea>
                                        </div>

                                        <label for="arb_description">Arabic Long Description</label>
                                        <div class="uk-form-row">

                                            <textarea class="md-input arb_text" name="arb_long_des" id="arb_long_des" cols="30" rows="4"><?php echo $news->arb_long_des; ?></textarea>
                                        </div>




                                    </div>


                                    <div class="uk-width-large-1-1">
                                        <div class="md-card-toolbar">
                                            <div class="md-card-toolbar-actions">
                                                <!--<i class="md-icon material-icons">&#xE146;</i>-->
                                            </div>
                                            <h3 class="md-card-toolbar-heading-text">
                                                Photos
                                            </h3>
                                        </div>
                                        <div class="md-card-content">
                                            <?php
                                            $light_boxes = ''; ?>
                                                <ul class="uk-grid uk-grid-width-small-1-6 uk-text-center" data-uk-grid-margin data-uk-observe id="images-light-box">
												<?php if($news->news_image != ''){?>
                                                        <li class="uk-position-relative image-<?php echo $news->id;?>">
                                                            <img src="<?php echo base_url().'uploads/images/latest_news/'.$news->news_image;?>" alt="" class="img_small" data-uk-modal="{target:'#modal_lightbox_<?php echo $news->id;?>'}"/>
                                                            <div class="uk-form-row">

                                                            </div>
                                                        </li>


                                                        <?php }
                                                        // this is used to avoid extra loop in php when this loop executes we at the bootom echo this variable

                                                        $light_boxes .="<div class='uk-modal' id='modal_lightbox_".$news->id."'><div class='uk-modal-dialog uk-modal-dialog-lightbox'><button type='button' class='uk-modal-close uk-close uk-close-alt'></button><img src='".base_url()."uploads/images/latest_news/".$news->news_image."' alt=''/></div></div>";

                                                    ?>


                                                </ul>

                                        </div>
                                    </div>

                                        <div class="uk-width-large-1-1">
                                            <h3 class="heading_a">
                                                Upload News Image
                                                <p style="color:red; font-size:12px">(1016 x 608 pixels)</p>
                                            </h3>
                                            <div class="uk-grid">
                                                <div class="uk-width-1-1">
                                                    <div id="file_upload-drop" class="uk-file-upload">
                                                        <a class="uk-form-file md-btn">choose file
                                                            <input id="file_upload-select" type="file" class="images_upload" name="news_image"></a>
                                                    </div>
                                                    <div id="file_upload-progressbar" class="uk-progress uk-hidden">
                                                        <div class="uk-progress-bar" style="width:0">0%</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                </div>
                            </div>
                        </div>



                    </div>
                </div>
            </form>

        </div>
    </div>

<!-- light box for image -->

<div id="light_box_of_images" data-uk-observe>
</div>

    <div class="md-fab-wrapper">
        <a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">
            <i class="material-icons">&#xE161;</i>
        </a>
    </div>
    
    <div class="md-fab-wrapper" style="right:95px;">
        <a class="md-fab md-fab-primary" href="<?php echo base_url();?>admin/latest_news" id="">
            <i class="material-icons">keyboard_backspace</i>
        </a>
    </div>

<?php if($light_boxes != ''){
    echo '<script>
		light_box_images = "'.$light_boxes.'";
	</script>';
} ?>
    