



    <section class="customer_service">

        <div class="container">

            <div class="cartHeading fontWhtReg">

                <h1 class=""><?php echo $customer_service_label; ?></h1>

                <div class="clearfix"></div>

            </div>

            <div class="textPageSty">

                <!-- Don't add top Bottom Padding -->

                <?php $this->load->view('layouts/customer_service_left_sidebar'); ?>

                <!-- Don't add top Bottom Padding -->



                <div class="rhtTextDetails">

                    <h1 class="makeShadow"><?php echo $latest_news_label; ?></h1>



                <?php foreach ($latest_news as $key => $news) {

                    if($key == 0){

                        $class = 'open';

                    }else{

                        $class = '';

                    }

                    ?>

                    <div class="newsBox">
		
						<?php if(!empty($news['news_image'])){?>
                        <div class="imgBox">

                            <img src="<?php echo base_url().'uploads/images/latest_news/'.$news['news_image']; ?>" alt="News" height="210" width="218" />

                        </div>
						<?php } ?>

                        <div class="textBox <?php echo $class; ?>">

                            <h2><?php echo $news[$lang.'_title']; ?></h2>

                            <?php echo $news[$lang.'_short_des']; ?>

                            <div class="longDescription">

                                <?php echo $news[$lang.'_long_des']; ?>

                            </div>

                            <a href="javascript:void(0);" class="readMore">

                                <i class="sprite_ioud ioudSpupgolden"></i>

                                <span class="more"><?php echo $read_more;?></span>

                                <span class="less"><?php echo $read_less;?></span>

                            </a>

                        </div>

                        <div class="clearfix"></div>

                    </div>



                <?php } ?>





                </div>



                <div class="clearfix"></div>

            </div>

        </div>

    </section>
