 <!-- Content Wrapper. Contains page content -->

      <div id="page_content">

        <div id="page_content_inner">
        

			<div class="md-card">

                <div class="md-card-content">

                    <div class="uk-grid" data-uk-grid-margin>

                        <div class="uk-width-1-1">

                        	<div class="uk-overflow-container"> 

                            	

                                <table class="uk-table uk-table-align-vertical listing dt_default">

                                    <thead>

                                        <tr>

                                             
                                            <!-- <th>Store Image</th>-->
                                              <th>Order Number</th>
                                              <th>Customer Name</th>

											  <th>Bank Name</th>

                                             <th>Account Holder’s Name</th>
                                             <th>Date & Time of Transfer</th>
                                             <th>Amount Transferred</th>
                                             <th>Mobile number</th>
                                             <th>Date</th>
                                            <?php  if(viewEditDeleteRights('Payment Requests','delete')) {?>
                                              <th>Action</th>
                                            <?php } ?>

                                        </tr>

                                    </thead>

                                    <tbody>

									<?php

									if($payment_requests)

									{
										$i=0;

										foreach($payment_requests as $payment_request)

										{

										?>

											<tr class="<?php echo $payment_request->id;?>">

									

													<td><?php echo $payment_request->order_number;?></td>

													<td><?php echo $payment_request->customer_name;?></td>

                                                   <td><?php echo $payment_request->bank_name;?></td>

													<td><?php echo $payment_request->account_holder;?></td>

                                                    <td><?php echo $payment_request->transfer_date;?></td>
                                                    <td><?php echo $payment_request->transfer_amount; ?>
                                                    </td>
                                                    <td><?php echo $payment_request->mobile_no; ?></td>
                                                    <td><?php echo date('d-m-Y',strtotime($payment_request->created_at)); ?></td>

                                            <?php  if(viewEditDeleteRights('Payment Requests','delete')) {?>
                                                    <td>
													<a href="javascript:void(0);" class="uk-margin-left delete" onclick="deleteRecord(<?php echo $payment_request->id;?>,'admin/customer_service/delete_payment_request','');" title="Delete payment">
                                                        <i class="material-icons md-24 delete">&#xE872;</i>
                                                    </a>
                                                    </td>
                                            <?php } ?>

											</tr>

										<?php

										}
									}

									

									?>

                                           

                                    </tbody>

                                </table>

                            </div>

                            

                        </div>

                    </div>

                </div>

            </div>



        </div>

    </div>

       <!-- /.content-wrapper --> 