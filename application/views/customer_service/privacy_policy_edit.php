<div id="page_content">
    <div id="page_content_inner">
        <form action="<?php echo base_url(); ?>admin/privacy_policy/action" method="post" onsubmit="return false"
              class="ajax_form">
            <div class="md-card">
                <div class="md-card-content"><h3 class="heading_a">Edit Privacy Policy</h3><br>
                    <div class="uk-grid" data-uk-grid-margin><input type="hidden" name="form_type" value="update">
                        <input type="hidden" name="id" value="<?php echo $privacy_policy->id; ?>">
                        <div class="uk-width-medium-1-2"><label>English Title</label>
                            <div class="uk-form-row"><input class="md-input" id="eng_title" name="eng_title"
                                                            value="<?php echo $privacy_policy->eng_title; ?>"></div>
                        </div>
                        <div class="uk-width-medium-1-2"><label>Arabic Title</label>
                            <div class="uk-form-row"><input class="md-input" id="arb_title" name="arb_title"
                                                            value="<?php echo $privacy_policy->arb_title; ?>"></div>
                        </div>
                        <div class="uk-width-medium-1-2"><label>English Description</label>
                            <div class="uk-form-row"><textarea cols="30" rows="4" class="md-input eng_text"
                                                               id="eng_description"
                                                               name="eng_description"><?php echo $privacy_policy->eng_description; ?></textarea>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2"><label>Arabic Description</label>
                            <div class="uk-form-row"><textarea cols="30" rows="4" class="md-input arb_text"
                                                               id="arb_description"
                                                               name="arb_description"><?php echo $privacy_policy->arb_description; ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="md-card">
                <div class="md-card-toolbar"><h3 class="md-card-toolbar-heading-text"> Keyword </h3></div>
                <input type="hidden" name="home_page_id" value="<?php echo $privacy_policy->home_page_id ?>"/> <input
                        type="hidden" name="tpl_name"
                        value="Return Policy"/> <?php $keyword = getPageData($privacy_policy->home_page_id); ?>
                <div class="md-card-content large-padding">
                    <div class="uk-grid uk-grid-divider uk-grid-medium" data-uk-grid-margin>
                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row"><label>English Meta Title</label> <input type="text"
                                                                                              class="md-input"
                                                                                              value="<?php echo $keyword->eng_meta_title; ?>"
                                                                                              name="eng_meta_title"/>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row"><label>Arabic Meta Title</label> <input type="text"
                                                                                             class="md-input"
                                                                                             value="<?php echo $keyword->arb_meta_title; ?>"
                                                                                             name="arb_meta_title"/>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row"><label>Englishn Meta Description</label> <textarea cols="30"
                                                                                                        rows="4"
                                                                                                        class="md-input"
                                                                                                        name="eng_meta_description"><?php echo $keyword->eng_meta_description; ?></textarea>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row"><label>Arabic Meta Description</label> <textarea cols="30" rows="4"
                                                                                                      class="md-input"
                                                                                                      name="arb_meta_description"><?php echo $keyword->arb_meta_description; ?></textarea>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row"><label>Englishn Meta Keyword</label> <textarea cols="30" rows="4"
                                                                                                    class="md-input"
                                                                                                    name="eng_meta_keyword"><?php echo $keyword->eng_meta_keyword; ?></textarea>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <div class="uk-form-row"><label>Arabic Meta Keyword</label> <textarea cols="30" rows="4"
                                                                                                  class="md-input"
                                                                                                  name="arb_meta_keyword"><?php echo $keyword->arb_meta_keyword; ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <?php if(viewEditDeleteRights('Privacy Policy','edit')) { ?>
        <div class="md-fab-wrapper"><a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">
                <i class="material-icons">&#xE161;</i> </a>
        </div>
        <?php } ?>

    </div>
</div>