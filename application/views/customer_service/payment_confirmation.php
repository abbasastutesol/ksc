 <!-- Content Wrapper. Contains page content -->

      <div id="page_content">

        <div id="page_content_inner">
        

			<div class="md-card">

                <div class="md-card-content">

                    <div class="uk-grid" data-uk-grid-margin>

                        <div class="uk-width-1-1">

                        	<div class="uk-overflow-container"> 

                            	

                                <table class="uk-table uk-table-align-vertical listing dt_default">

                                    <thead>

                                        <tr>

                                             <th>Sr#</th>

                                              <th>Order#</th>

											  <th>Name</th>

                                             <th>Bank Name</th>
                                             <th>Account Holder</th>
                                             <th>Transfer Date</th>
                                            <th>Amount</th>
                                             <th>Mobile</th>
                                             <th>Created At</th>
                                            <?php if(viewEditDeleteRights('Payment Confirm','delete')) { ?>
                                              <th>Action</th>
                                            <?php } ?>

                                        </tr>

                                    </thead>

                                    <tbody>

									<?php

									if($payments)

									{

										$i=0;

										foreach($payments as $pay)

										{

										?>

											<tr class="<?php echo $pay->id;?>">

													<td><?php echo ++$i;?></td>

													<td><?php echo $pay->order_number; ?></td>

													<td><?php echo $pay->customer_name;?></td>

                                                   <td><?php echo $pay->bank_name;?></td>

													<td><?php echo $pay->account_holder;?></td>

                                                    <td><?php echo $pay->transfer_date;?></td>
                                                    <td><?php echo $pay->transfer_amount;?></td>
                                                    <td><?php echo $pay->mobile_no;?></td>
                                                    <td><?php echo date('d-m-Y',strtotime($pay->created_at));?></td>
                                            <?php if(viewEditDeleteRights('Payment Confirm','delete')) { ?>
													<td>
													<a href="javascript:void(0);" class="uk-margin-left delete" onclick="deleteRecord(<?php echo $pay->id;?>,'admin/payment_confirmation/contact_delete','');" title="Delete payment"> <i class="material-icons md-24 delete">&#xE872;</i></a>
                                                    </td>
                                            <?php } ?>

											</tr>

										<?php

										}
	

									}

									

									?>

                                           

                                    </tbody>

                                </table>

                            </div>

                            

                        </div>

                    </div>

                </div>

            </div>



        </div>

    </div>

       <!-- /.content-wrapper --> 