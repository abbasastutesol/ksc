

    <section class="customer_service">

        <div class="container">

            <div class="cartHeading fontWhtReg">

                <h1 class=""><?php echo $customer_service_label; ?></h1>

                <div class="clearfix"></div>

            </div>

            <div class="textPageSty">

                <!-- Don't add top Bottom Padding -->

                <?php $this->load->view('layouts/customer_service_left_sidebar'); ?>

                <!-- Don't add top Bottom Padding -->

                <div class="rhtTextDetails">

                    <?php $this->load->view('messages/flash'); ?>

                    <h1 class="makeShadow"><?php echo $distributor_label; ?></h1>

                    <?php echo $distributor[$lang.'_description']; ?>



                    <form action="<?php echo base_url('customer_service/distributor_request'); ?>" method="post" enctype="multipart/form-data" id="distributor_form" name="distributor_form" onsubmit="return false;">

                        <div class="row">

                            <div class="col-md-4 col-sm-6 col-xs-12">

                                <label><?php echo $full_name_label; ?></label>

                                <input type="text" name="full_name" id="full_name" placeholder="<?php echo $enter_here_palceholder; ?>" />

                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12">

                                <label><?php echo $mobile_no_label; ?></label>

                                <input type="text" name="mobile_no" id="mobile_id" placeholder="<?php echo $enter_here_palceholder; ?>" />

                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12">

                                <label><?php echo $distributor_store_name_label; ?></label>

                                <input type="text" name="store_name" id="store_name" placeholder="<?php echo $enter_here_palceholder; ?>" />

                            </div>



                            <div class="col-md-4 col-sm-6 col-xs-12">

                                <label><?php echo $email_address_label; ?></label>

                                <input type="text" name="email" id="email_distributor" placeholder="<?php echo $enter_here_palceholder; ?>" />

                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12">

                                <label><?php echo $country_label; ?></label>

                                <select class="reg_country" name="country" id="country">

                                    <option value=""><?php echo $select_dropdown; ?></option>

                                    <?php echo getCountries_html();?>

                                </select>

                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12">

                                <label><?php echo $city_label; ?></label>

                                <select class="reg_city" name="city" id="city">

                                    <option value=""><?php echo $select_dropdown; ?></option>

                                </select>

                            </div>



                            <div class="col-md-4 col-sm-6 col-xs-12">

                                <label><?php echo $distributor_street_address_label; ?></label>

                                <input type="text" name="street_address" id="street_address" placeholder="<?php echo $enter_here_palceholder; ?>" />

                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12">

                                <label><?php echo $distributor_district_label; ?></label>

                                <input type="text" name="district" id="district" placeholder="<?php echo $enter_here_palceholder; ?>" />

                            </div>

                            <div class="col-md-4 col-sm-6 col-xs-12">

                                <label><?php echo $distributor_store_number_label; ?></label>

                                <input type="text" name="store_number" id="store_number" placeholder="<?php echo $enter_here_palceholder; ?>" />

                            </div>







                            <div class="col-md-4 col-sm-6 col-xs-12">

                                <label><?php echo $distributor_pin_location_label; ?></label>

                                <input type="text" class="location" id="location" name="location" placeholder="<?php echo $enter_here_palceholder; ?>" readonly />



                               



                            </div>





                            <div class="col-md-4 col-sm-6 col-xs-12">

                                <label><?php echo $distributor_store_images_label; ?></label>

                                <div class="fileUploader">

                                    <input class="showFileName" type="text" placeholder="<?php echo $file_upload; ?>">

                                    <div class="btn btn-black showFileType"><?php echo $browse_label; ?></div>

                                    <input class="edBtn attachCV" id="file" name="image[]" type="file" multiple>

                                </div>

                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                             	<div id="location_map" style="height: 320px; position: relative; overflow: hidden; display: none;"></div>
							</div>


                            <div class="col-md-12 col-sm-12 col-xs-12 text-center">

								<input type="submit" value="<?php echo $submit_btn_label; ?>" class="btn btn-black" id="submit_id">

                            </div>



                        </div>

                    </form>

                </div>


                <div class="clearfix"></div>

            </div>

        </div>

    </section>
    
    <script>
    // this is location picker for distributor page
    $('.location').click(function () {

       $('#location_map').show();
        initialize();
    });

    google.maps.event.addDomListener(window, 'load', initialize);

        var saved_location = 'Location has been picked successfully.';

    var myOptions = '';
    var gmarkers = [];
    function removeMarkers() {
        for (i = 0; i < gmarkers.length; i++) {
            gmarkers[i].setMap(null);
        }
    }
    function initialize()
    {
        var map;
        var bounds = new google.maps.LatLngBounds();
        var myLatlng = new google.maps.LatLng(24.720403782761128,46.678762435913086);
        var myOptions = {
            zoom: 6,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControlOptions: {position: google.maps.ControlPosition.CENTRE}
        };
        map = new google.maps.Map(document.getElementById("location_map"), myOptions);
        google.maps.event.addListener(map, "click", function(event) {
            removeMarkers();
            var lat = event.latLng.lat();
            var lng = event.latLng.lng();

            placeMarker(event.latLng);
            $(".location").val(lat+","+lng);
            //$('#location_map').hide();
            //alert(saved_location);
        });

        function placeMarker(location) {
            var marker = new google.maps.Marker({
                position: location,
                map: map
            }); gmarkers.push(marker);
        }

    }

</script>

