<div id="page_content">
    <div id="page_content_inner">
        <form action="<?php echo base_url(); ?>admin/customer_service/action" method="post" onsubmit="return false"

              class="ajax_form">
            <div class="md-card">
                <div class="md-card-content"><h3 class="heading_a">Edit Meduaf program</h3><br>
                    <div class="uk-grid" data-uk-grid-margin>
                        <input type="hidden" name="form_type" value="update_meduaf_program">
                        <input type="hidden" name="id" value="<?php echo $meduaf_program->id; ?>">
                        <div class="uk-width-medium-1-2"><label>English Title</label>
                            <div class="uk-form-row"><input class="md-input" id="eng_title" name="eng_title"
                                                            value="<?php echo $meduaf_program->eng_title; ?>"></div>
                        </div>
                        <div class="uk-width-medium-1-2"><label>Arabic Title</label>
                            <div class="uk-form-row"><input class="md-input" id="arb_title" name="arb_title"
                                                            value="<?php echo $meduaf_program->arb_title; ?>"></div>
                        </div>
                        <div class="uk-width-medium-1-2"><label>English Description</label>
                            <div class="uk-form-row"><textarea cols="30" rows="4" class="md-input eng_text"
                                                               id="eng_desc"
                                                               name="eng_desc"><?php echo $meduaf_program->eng_desc; ?></textarea>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2"><label>Arabic Description</label>
                            <div class="uk-form-row"><textarea cols="30" rows="4" class="md-input arb_text"
                                                               id="arb_desc"
                                                               name="arb_desc"><?php echo $meduaf_program->arb_desc; ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </form>

        <?php if(viewEditDeleteRights('Meduaf program','edit')) { ?>
            <div class="md-fab-wrapper"><a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">
                    <i class="material-icons">&#xE161;</i> </a>
            </div>
        <?php } ?>

    </div>
</div>