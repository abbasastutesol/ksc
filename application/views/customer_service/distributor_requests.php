 <!-- Content Wrapper. Contains page content -->

      <div id="page_content">

        <div id="page_content_inner">
        

			<div class="md-card">

                <div class="md-card-content">

                    <div class="uk-grid" data-uk-grid-margin>

                        <div class="uk-width-1-1">

                        	<div class="uk-overflow-container"> 

                            	

                                <table class="uk-table uk-table-align-vertical listing dt_default">

                                    <thead>

                                        <tr>

                                             
                                            <!-- <th>Store Image</th>-->
                                              <th>Name</th>
                                              <th>Mobile</th>
                                              <th>Store Name</th>

											  <th>Email</th>

                                             <th>Country</th>
                                             <th>City</th>
                                             <th>Address</th>
                                             <th>District</th>
                                            <th>Store No</th>
                                            <th>Date</th>

                                            <?php if(viewEditDeleteRights('Distributors Requests','delete')) { ?>
                                              <th>Action</th>
                                            <?php } ?>

                                        </tr>

                                    </thead>

                                    <tbody>

									<?php

									if($distributors)

									{
										$i=0;

										foreach($distributors as $distributor)

										{

										?>

											<tr class="<?php echo $distributor->id;?>">

									

													<td><?php echo ucfirst($distributor->full_name);?></td>

													<td><?php echo $distributor->mobile_no;?></td>

                                                   <td><?php echo $distributor->store_name;?></td>

													<td><?php echo $distributor->email;?></td>

                                                    <td><?php echo $distributor->country;?></td>
                                                    <td><?php echo $distributor->city; ?>
                                                    </td>
                                                    <td><?php echo $distributor->street_address; ?>
                                                    </td>
                                                    <td><?php echo $distributor->district; ?>
                                                    </td>
                                                    
                                                    <td>
													<?php echo $distributor->store_number; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo date('d-m-Y',strtotime($distributor->created_at)); ?>
                                                    </td>

                                            <?php if(viewEditDeleteRights('Distributors Requests','delete')) { ?>
                                                    <td>
													<a href="javascript:void(0);" class="uk-margin-left delete" onclick="deleteRecord(<?php echo $distributor->id;?>,'admin/customer_service/delete_distributor','');" title="Delete feedback">
                                                        <i class="material-icons md-24 delete">&#xE872;</i>
                                                    </a>
                                                    </td>
                                            <?php } ?>

											</tr>

										<?php

										}
	

									}

									

									?>

                                           

                                    </tbody>

                                </table>

                            </div>

                            

                        </div>

                    </div>

                </div>

            </div>



        </div>

    </div>

       <!-- /.content-wrapper --> 