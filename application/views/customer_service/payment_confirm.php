<section class="customer_service">
        <div class="container">
            <div class="cartHeading fontWhtReg">
                <h1 class=""><?php echo $customer_service_label; ?></h1>
                <div class="clearfix"></div>
            </div>
            <div class="textPageSty">
                <!-- Don't add top Bottom Padding -->
             <?php $this->load->view('layouts/customer_service_left_sidebar'); ?>
                <!-- Don't add top Bottom Padding -->
                <div class="rhtTextDetails">

                    <?php $this->load->view('messages/flash'); ?>

                    <h1 class="makeShadow"><?php echo $payment_confirmation_label; ?></h1>
                    <form class="payment_confirm" action="<?php echo lang_base_url().'customer_service/payment_confirm_save'; ?>" method="post" onsubmit="return false;" id="payment_form" name="payment_form">
                        <div class="row">
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <label><?php echo $payment_order_no_label; ?></label>
                                <input type="text" name="order_number" id="order_number_id" placeholder="<?php echo $enter_here_palceholder; ?>" />
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <label><?php echo $payment_customer_name_label; ?></label>
                                <input type="text" name="customer_name" id="customer_name_id" placeholder="<?php echo $enter_here_palceholder; ?>" />

                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <label><?php echo $payment_bank_name_label; ?></label>
                                <input type="text" name="bank_name" id="bank_name_id" placeholder="<?php echo $enter_here_palceholder; ?>" />

                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <label><?php echo $payment_bank_account_name_label; ?></label>
                                <input type="text" placeholder="<?php echo $enter_here_palceholder; ?>" name="account_holder" id="account_holder_id"/>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <label><?php echo $payment_date_and_time_label; ?></label>
                                <input type="text" name="transfer_date" id="transfer_date_id" class="dateTimeClass" placeholder="<?php echo $enter_here_palceholder; ?>" />
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <label><?php echo $payment_amount_transfer_label; ?></label>
                                <input type="text" name="transfer_amount" id="transfer_amount_id" placeholder="<?php echo $enter_here_palceholder; ?>" />
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <label><?php echo $mobile_no_label; ?></label>
                                <input type="text" name="mobile_no" id="mobile_id" placeholder="<?php echo $enter_here_palceholder; ?>" value="+966" />
                            </div>

                            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                <button type="submit" id="submit_id" class="btn btn-black"><?php echo $submit_btn_label; ?></button>
                            </div>

                        </div>
                    </form>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </section>
