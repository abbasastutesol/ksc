<div id="page_content">
    <div id="page_content_inner">
        <form action="<?php echo base_url(); ?>admin/customer_service/action" method="post" onsubmit="return false"
              class="ajax_form" enctype="multipart/form-data">
            <div class="md-card">
                <div class="md-card-content"><h3 class="heading_a">Edit Payment & Delivery</h3><br>
                    <div class="uk-grid" data-uk-grid-margin><input type="hidden" name="form_type" value="update">
                        <input type="hidden" name="id" value="<?php echo $customer_service->id; ?>">
                        <div class="uk-width-medium-1-2"><label>English Title</label>
                            <div class="uk-form-row"><input class="md-input" id="eng_title" name="eng_title"
                                                            value="<?php echo $customer_service->eng_title; ?>"></div>
                        </div>
                        <div class="uk-width-medium-1-2"><label>Arabic Title</label>
                            <div class="uk-form-row"><input class="md-input" id="arb_title" name="arb_title"
                                                            value="<?php echo $customer_service->arb_title; ?>"></div>
                        </div>
                        <div class="uk-width-medium-1-2"><label>English Description</label>
                            <div class="uk-form-row"><textarea cols="30" rows="4" class="md-input eng_text"
                                                               id="eng_description"
                                                               name="eng_description"><?php echo $customer_service->eng_description; ?></textarea>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2"><label>Arabic Description</label>
                            <div class="uk-form-row"><textarea cols="30" rows="4" class="md-input arb_text"
                                                               id="arb_description"
                                                               name="arb_description"><?php echo $customer_service->arb_description; ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

       <?php if(viewEditDeleteRights('Payment & Delivery','edit')) { ?>
        <div class="md-fab-wrapper"><a class="md-fab md-fab-primary submit_ajax_form" href="javascript:void(0);" id="">
                <i class="material-icons">&#xE161;</i> </a>
        </div>
        <?php } ?>

    </div>
</div>