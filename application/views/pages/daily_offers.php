<section>
	<div class="defaultPageSty">
		<div class="container max960_cont">
			<h1><?php echo $login_daily_offers; ?></h1>
		</div>
	</div>
		<div class="line"></div>
		<div class="retunExSec">
			<div class="container max960_cont">
				<div class="productSecFive placeFeedBack2">
					<div class="row">
                    			
                    	<?php
						
						$check_offer = array(); 
						if($products)
						{
						
							
						foreach($products as $product){
								if(date('Y-m-d') < date('Y-m-d', strtotime($product['start_date'])) || date('Y-m-d') > date('Y-m-d', strtotime($product['end_date'])))
								{
									continue;
								}
							$check_offer[] = $product['id'];
								 $prod_image = getProductImages($product['id']);
								  if(!empty($prod_image))
								  {
									  $image_name = $prod_image[0][$lang.'_image'];
								  }
								  else
								  {
									  $image_name = 'no_imge.png';
								  }
							
							?>
							<ul>
                                <li class="retunConfirmation">
                                    <p><label><?php echo $login_daily_offers; ?></label></p>
                                    <p>
                                        <label><?php echo $daily_offer_start_date;?>: <?php echo dateFormat(date('Y-m-d', strtotime($product['start_date'])))?></label><br />
                                        <label><?php echo $daily_offer_end_date;?>: <?php echo dateFormat(date('Y-m-d', strtotime($product['end_date'])))?></label>
                                    </p>
                                </li>
                                <li>
                                    <a href="<?php echo lang_base_url();?>product/product_page/<?php echo $product['id'];?>"><div class="imgBox"><img src="<?php echo base_url();?>uploads/images/products/<?php echo $image_name;?>" alt="Product" height="199" width="129" /></div>
                                    <h4 class="pull-right"><?php echo $product[$lang.'_price'];?>&nbsp;<?php echo getPriceCur('', true);?></h4></a>
                                </li>
                            </ul>
						<?php }
						}
						if(empty($check_offer))
						{
							echo ($lang == 'eng' ? 'No offers available at this time.' : '.لا يوجد عروض في ذلك الوقت');
						}
						
						
						?>
                    </div>
				</div>
			</div>
		</div>
		
</section>