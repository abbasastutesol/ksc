<?php

class Base_Model extends CI_Model

{

	

	public $table;

	

	public function __construct($table="")

	{

		parent::__construct();

		

		if(!empty ($table))

		{

			$this->table = $table;

			

			$fields = $this->db->list_fields($table);

		

			foreach ($fields as $field)

			{

			  	$this->$field = NULL;

			}

		}

		

	}

	

	/**

	 * Inserts a row into the table and returns the row id

	 * @param data Array

	 * @return insert_id int

	 */

	public function save($data)

	{

		$this->db->insert($this->table,$data);
		
  //echo $this->db->last_query(); exit;
		return $this->db->insert_id();

	}

	

	/**

	 * 

	 * @param array $data

	 * @param array $search

	 */

	public function update($data,$search)

	{

		$this->db->update($this->table,$data,$search);
       
	   //echo $this->db->last_query(); exit;
	   
		if ($this->db->affected_rows() == '1') {

			return true;

		} else {

		// any trans error?

		if ($this->db->trans_status() === FALSE) {

			return false;

		}

		return true;

		}

	}

	

	/**

	 * Get an instance of the model and initialize the properties with table row for supplied ID.

	 * @param unknown $id

	 * @return Base_Model|boolean

	 */

	 

	// Fetch Row by id

	public function get($id,$as_array=false)

	{

		$result = $this->db->get_where($this->table,array('id'=>$id));

		

		if($result->num_rows() > 0)

		{

			$row = $result->row_array();

			

			if($as_array)

			{

				return $row;

			}

			

			foreach ($row as $col=>$val)

			{

				$this->$col = $val;

			}

			

			return $this;

		}

		

		else

		{

			return false;

		} 

	}

	

	// Fetch Single Row

	public function getSingleRow($field,$as_array=false)

	{

		$result = $this->db->get_where($this->table,$field);

		

		if($result->num_rows() > 0)

		{

			$row = $result->row_array();

			

			if($as_array)

			{

				return $row;

			}

			

			foreach ($row as $col=>$val)

			{

				$this->$col = $val;

			}

			

			return $this;

		}

		

		else

		{

			return false;

		} 

	}

	

	// Fetch Row Count by id

	public function getRowCount($fields,$as_array=false)

	{

		$result = $this->db->get_where($this->table,$fields);		

		return $result->num_rows();

	}

    public function getRowCount2($fields,$as_array=false)

    {

        $result = $this->db->get_where($this->table,$fields);

        return $result->result();

    }

	

	

	public function getMultipleRows($fields,$as_array=false, $idOrderBy='asc', $orderBy='id')

	{

		

		

		$this->db->order_by($orderBy,$idOrderBy);

	

		$result = $this->db->get_where($this->table,$fields);

		

		//echo $this->db->last_query(); exit();

		

		if($result->num_rows() > 0)

		{

			

			

			if($as_array)

		    {

			 return $result->result_array();

		    }

		

		    return $result->result();

		}

		

		else

		{

			return false;

		} 

	}

	

	public function getMultipleRowsFields($fields, $fetch_fields, $as_array=false, $idOrderBy='asc')

	{

		

		$this->db->select(implode(',', $fetch_fields));

		if($idOrderBy=='desc')

			$this->db->order_by('id','desc');

	

		$result = $this->db->get_where($this->table,$fields);

		

		//echo $this->db->last_query(); exit();

		

		if($result->num_rows() > 0)

		{

			

			

			if($as_array)

		    {

			 return $result->result_array();

		    }

		

		    return $result->result();

		}

		

		else

		{

			return false;

		} 

	}

	

	public function getWithMultipleFields($fields,$as_array=false)

	{



		$result = $this->db->get_where($this->table,$fields);

		if($result->num_rows() > 0)

		{

			$row = $result->row_array();

			

			if($as_array)

			{

				return $row;

			}

			

			foreach ($row as $col=>$val)

			{

				$this->$col = $val;

			}

			



			

			return $this;

		}

		

		else

		{

			return false;

		} 

		

	}

	

	/**

	 * Gets all the records from the table.

	 */

	public function getAll($as_array=false, $idOrderBy='asc')

	{

		if($idOrderBy=='desc')

			$this->db->order_by('id','desc');

		

		$result = $this->db->get($this->table);

		

		if($as_array)

		{

			return $result->result_array();

		}

		

		return $result->result();

	}

	

	public function getPostComments($id,$as_array=false)

	{

		$result = $this->db->get_where($this->table,array('post_id'=>$id));

		

		if($as_array)

		{

			return $result->result_array();

		}

		

		return $result->result();

	}

	

	public function deleteIn($key, $valuesArr)

	{

		

		$this->db->where_in($key, $valuesArr);

		$this->db->delete($this->table);



	}



	public function delete($search)

	{

		$this->db->delete($this->table,$search);

		if ($this->db->affected_rows() > 0) {

			return true;

		}else

        {

			return false;

		}			



	}
    

	public function getFields()

	{

		$fields = $this->db->list_fields($this->table);

		return $fields;

	}

    public function record_count() {
        return $this->db->count_all($this->table);
    }

    public function getWhereCount($condition) {
        $sql = "SELECT * FROM ".$this->table." WHERE ".$condition;
        $query = $this->db->query($sql);

        return $query->num_rows();
    }

}