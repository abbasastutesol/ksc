<?php include('header.php'); ?>
<main class="main">
	<div class="container">
		<section class="content-wrap projects-sec">
			<div class="page-heading">
				<h1 class="page-title">Projects</h1>
			</div>
			<div class="projects-wrap">
				<div class="project-col">
					<a href="project-detail.php"><img src="images/project-img-1.jpg" alt=""></a>
					<h1><a href="project-detail.php">DUBAI MALL</a></h1>
				</div>
				<div class="project-col">
					<a href="project-detail.php"><img src="images/project-img-2.jpg" alt=""></a>
					<h1><a href="project-detail.php">MADINA AIRPORT</a></h1>
				</div>
				<div class="project-col">
					<a href="project-detail.php"><img src="images/project-img-3.jpg" alt=""></a>
					<h1><a href="project-detail.php">MAKKAH (HARAM EXPANSION)</a></h1>
				</div>
				<div class="project-col">
					<a href="project-detail.php"><img src="images/project-img-4.jpg" alt=""></a>
					<h1><a href="project-detail.php">KINGDOM TOWER</a></h1>
				</div>
			</div>
		</section>
	</div>
</main>
<?php include('footer.php'); ?>