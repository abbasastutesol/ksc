<?php include('header.php'); ?>
<main class="main">
	<div class="container">
		<section class="content-wrap contact-sec">
			<div class="page-heading">
				<h1 class="page-title">Contact</h1>
			</div>
			<div class="showrooms-map">
				<!--Note for programmer: Please find location icon from images, image name is "location-large-icon.png" for large icon and for small icon "location-small-icon.png".-->
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14839.442917872297!2d39.18118516944333!3d21.5913587461082!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x15c3d0e41a565aef%3A0x57fcbd7ca765fe68!2sUnnamed+Road%2C+Al-Rabwah%2C+Jeddah+23448%2C+Saudi+Arabia!5e0!3m2!1sen!2s!4v1511416482530" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
			<div class="contact-form">
				<h2>FEEDBACK FORM</h2>
				<form action="#" class="material">
					<div class="contact-frow">
						<div class="contact-col">
							<input type="text" name="name" placeholder="Name" required>
						</div>
						<div class="contact-col">
							<input type="email" name="email" placeholder="Email" required>
						</div>
						<div class="contact-col">
							<input type="tel" name="mobile" placeholder="MOBILE <span>(For example +966 5X XXX XXXX)</span>">
						</div>
						<div class="contact-col">
							<input type="text" name="company" placeholder="Company">
						</div>
						<div class="contact-col">
							<input type="text" name="position" placeholder="Position">
						</div>
						<div class="contact-col">
							<select name="mySelect" placeholder="Subject">
								<option value="Subject 1">Subject 1</option>
								<option value="Subject 2">Subject 2</option>
								<option value="Subject 3">Subject 3</option>
								<option value="Subject 4">Subject 4</option>
							</select>
						</div>
						<div class="contact-col">
							<textarea name="message" placeholder="MESSAGE"></textarea>
							<input type="submit" value="Submit">
						</div>
					</div>
				</form>
			</div>
			<div class="contact-sidebar">
				<address class="address-col">
					<p>KSC Riyadh Branch - Omar Bin Al Khattab</p>
					<p>Street - Malaz District</p>
					<p>Phone: <a href="tel:011 478 3088">011 478 3088</a></p>
					<p>Fax: 011 478 0591</p>
				</address>
				<address class="address-col">
					<p>KSC Corporate Offices - KSC Building - Baladiya</p>
					<p>Street - Kingdom of Saudi Arabia</p>
					<p>Phone: <a href="tel:012 665 2757">012 665 2757</a></p>
					<p>Fax: 012 665 2712</p>
				</address>
				<address class="address-col">
					<p>KSC Dammam Branch - Aouf Bin Wael</p>
					<p>Street -</p>
					<p>Phone: <a href="tel:013 859 1721">013 859 1721</a></p>
					<p>Fax: 013 859 2283</p>
				</address>
			</div>
		</section>
	</div>
</main>
<?php include('footer.php'); ?>