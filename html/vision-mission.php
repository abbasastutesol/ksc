<?php include('header.php'); ?>
<main class="main">
	<div class="container">
		<section class="content-wrap">
			<div class="left-sidebar">
				<ul>
					<li><a href="company-profile.php">PROFILE</a></li>
					<li class="active"><a href="vision-mission.php">VISION &amp; MISSION</a></li>
					<li><a href="chairmans-message.php">CHAIRMAN’S MESSAGE</a></li>
					<li><a href="ceo-message.php">CEO’S MESSAGE</a></li>
					<li><a href="value.php">VALUE</a></li>
					<li><a href="awards.php">AWARDS</a></li>
				</ul>
			</div>
			<div class="right-content">
				<div class="large-thumbnail"><img src="images/vision-mission-img.png" alt=""></div>
				<h1>VISION &amp; MISSION</h1>
				<p>Our mission is to work with our customers to meet the continuous demand of the market and working together with manufacturers to supply a new innovative products with a great value.</p>
				<p>Our main concern is to work with our business partners to provide a high quality of products at a competitive prices in order to increase our market share and ensure fast-growing market for our products.	</p>
			</div>
		</section>
	</div>
</main>
<?php include('footer.php'); ?>