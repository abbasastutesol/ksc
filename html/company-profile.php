<?php include('header.php'); ?>
<main class="main">
	<div class="container">
		<section class="content-wrap">
			<div class="left-sidebar">
				<ul>
					<li class="active"><a href="company-profile.php">PROFILE</a></li>
					<li><a href="vision-mission.php">VISION &amp; MISSION</a></li>
					<li><a href="chairmans-message.php">CHAIRMAN’S MESSAGE</a></li>
					<li><a href="ceo-message.php">CEO’S MESSAGE</a></li>
					<li><a href="value.php">VALUE</a></li>
					<li><a href="awards.php">AWARDS</a></li>
				</ul>
			</div>
			<div class="right-content">
				<div class="large-thumbnail"><img src="images/company-profile-img.png" alt=""></div>
				<h1>PROFILE</h1>
				<p>We have pleasure in introducing our co. as a magor distributor and stockist of electrical/Mechanical products throughout the Kingdom. Khereiji Showrooms Co. (K.S.C) was formed in 1980 to meet an ever increasing demand for electrical products to the construction and other related industries in Saudi Arabia.</p>
				<p>We have acquired through expert market research a comprehensive range of electrical products supplied through various distributors and exclusive representational agreements. These agreements and right of distribution have enabled K.S.C to become a magor supplier on numerous large projects within the Kingdom, due to the quality materials, competetive pricing and reliable professional service.</p>
				<p>K.S.C team of highly qualified electrical engineers are always pleased to provide technical advice and rewiews of customers trade requirements. We are at the present time the leading supplier of British electrical products in the Kingdom and have agents in U.S.A and UK employed by the El Khereiji Group of companies in the source of our material requirements.</p>
				<p>Please be assured of our best attention and close cooperation, should you favour K.S.C with inquiries.</p>
			</div>
		</section>
	</div>
</main>
<?php include('footer.php'); ?>