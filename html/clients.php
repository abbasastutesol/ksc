<?php include('header.php'); ?>
<main class="main">
	<div class="container">
		<section class="content-wrap clients-page">
			<div class="page-heading">
				<h1 class="page-title">CLIENTS</h1>
			</div>
			<div class="page-content client-logos">
				<ul>
					<li><a href="#"><img src="images/logo-img-1.png" alt=""></a></li>
					<li><a href="#"><img src="images/logo-img-2.png" alt=""></a></li>
					<li><a href="#"><img src="images/logo-img-3.png" alt=""></a></li>
					<li><a href="#"><img src="images/logo-img-4.png" alt=""></a></li>
					<li><a href="#"><img src="images/logo-img-5.png" alt=""></a></li>
					<li><a href="#"><img src="images/logo-img-6.png" alt=""></a></li>
					<li><a href="#"><img src="images/logo-img-7.png" alt=""></a></li>
					<li><a href="#"><img src="images/logo-img-8.png" alt=""></a></li>
					<li><a href="#"><img src="images/logo-img-1.png" alt=""></a></li>
					<li><a href="#"><img src="images/logo-img-2.png" alt=""></a></li>
					<li><a href="#"><img src="images/logo-img-3.png" alt=""></a></li>
					<li><a href="#"><img src="images/logo-img-4.png" alt=""></a></li>
				</ul>
			</div>
		</section>
	</div>
</main>
<?php include('footer.php'); ?>