<?php include('header.php'); ?>
<main class="main">
	<div class="container">
		<section class="content-wrap legalinfo-wrap">
            <div class="page-heading">
                <h1 class="page-title">LEGAL INFORMATION</h1>
            </div>
            <div class="legalinfo-content">
				<h1>Lorem ipsum dolor sit amet</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam volutpat iaculis cursus. Nullam venenatis scelerisque tristique. Cras ultricies ornare egestas. Suspendisse at velit dictum, tempor urna eget, tempus urna. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam nulla tellus, congue id bibendum eget, auctor ut quam. Proin nunc lectus, faucibus faucibus neque dapibus, ultrices vehicula diam. Sed commodo, ante sed pellentesque viverra, arcu nisi iaculis justo, non auctor nunc urna quis ligula. Nullam non maximus libero, ac tincidunt elit.</p>
				<h2>Lorem ipsum dolor sit amet</h2>
				<p>Nullam justo lacus, tristique vitae elementum eget, placerat eget eros. Etiam sodales, lectus eget pellentesque iaculis, odio velit auctor massa, non maximus eros libero in ante. Morbi sodales ligula eu orci commodo, ut tincidunt est aliquet. Morbi sit amet libero id eros sollicitudin pharetra. Duis hendrerit hendrerit ligula bibendum tempor. Quisque eu elit vitae felis varius sollicitudin. Pellentesque porta purus id porttitor consequat. Mauris sagittis est ante, in congue est vulputate tempus. In et neque nulla. Nam varius, mauris sagittis vulputate dignissim, ante dolor suscipit lacus, vitae aliquam nulla enim at nibh. Pellentesque sit amet diam mauris. Sed a lacus pellentesque, suscipit purus nec, fermentum magna. Aenean tempus pharetra efficitur. Proin at magna urna.</p>
				<h3>Lorem ipsum dolor sit amet</h3>
				<p>Integer in iaculis mauris. Mauris tempus neque in odio pharetra fermentum. Fusce et ligula magna. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec eu libero lacus. Nam rhoncus, orci at malesuada volutpat, lectus dui rhoncus ante, eu rhoncus massa lorem a urna. Phasellus ex elit, cursus at aliquet id, convallis eget justo. Aenean at facilisis est. Phasellus pulvinar metus sed tellus sollicitudin, sit amet cursus augue hendrerit. Aenean lacinia elit magna, id porttitor quam posuere id. Nunc nisl elit, blandit nec luctus vitae, maximus ac felis. Aliquam non blandit neque. Donec tincidunt velit ipsum, rhoncus elementum metus tincidunt sed. Sed feugiat eget lacus in sagittis. Suspendisse vitae tincidunt erat. Cras suscipit ipsum lacinia felis ultricies egestas.</p>
				<h4>Lorem ipsum dolor sit amet</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam volutpat iaculis cursus. Nullam venenatis scelerisque tristique. Cras ultricies ornare egestas. Suspendisse at velit dictum, tempor urna eget, tempus urna. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam nulla tellus, congue id bibendum eget, auctor ut quam. Proin nunc lectus, faucibus faucibus neque dapibus, ultrices vehicula diam. Sed commodo, ante sed pellentesque viverra, arcu nisi iaculis justo, non auctor nunc urna quis ligula. Nullam non maximus libero, ac tincidunt elit.</p>
				<h5>Lorem ipsum dolor sit amet</h5>
				<ul>
					<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
					<li>Nullam volutpat iaculis cursus.</li>
					<li>Nullam venenatis scelerisque tristique.</li>
					<li>Suspendisse at velit dictum, tempor urna eget, tempus urna.</li>
				</ul>
				<p>Nullam justo lacus, tristique vitae elementum eget, placerat eget eros. Etiam sodales, lectus eget pellentesque iaculis, odio velit auctor massa, non maximus eros libero in ante. Morbi sodales ligula eu orci commodo, ut tincidunt est aliquet. Morbi sit amet libero id eros sollicitudin pharetra. Duis hendrerit hendrerit ligula bibendum tempor. Quisque eu elit vitae felis varius sollicitudin. Pellentesque porta purus id porttitor consequat. Mauris sagittis est ante, in congue est vulputate tempus. In et neque nulla. Nam varius, mauris sagittis vulputate dignissim, ante dolor suscipit lacus, vitae aliquam nulla enim at nibh. Pellentesque sit amet diam mauris. Sed a lacus pellentesque, suscipit purus nec, fermentum magna. Aenean tempus pharetra efficitur. Proin at magna urna.</p>
				<h6>Lorem ipsum dolor sit amet</h6>
				<ol>
					<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
					<li>Nullam volutpat iaculis cursus.</li>
					<li>Nullam venenatis scelerisque tristique.</li>
					<li>Suspendisse at velit dictum, tempor urna eget, tempus urna.</li>
				</ol>
				<p>Integer in iaculis mauris. Mauris tempus neque in odio pharetra fermentum. Fusce et ligula magna. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec eu libero lacus. Nam rhoncus, orci at malesuada volutpat, lectus dui rhoncus ante, eu rhoncus massa lorem a urna. Phasellus ex elit, cursus at aliquet id, convallis eget justo. Aenean at facilisis est. Phasellus pulvinar metus sed tellus sollicitudin, sit amet cursus augue hendrerit. Aenean lacinia elit magna, id porttitor quam posuere id. Nunc nisl elit, blandit nec luctus vitae, maximus ac felis. Aliquam non blandit neque. Donec tincidunt velit ipsum, rhoncus elementum metus tincidunt sed. Sed feugiat eget lacus in sagittis. Suspendisse vitae tincidunt erat. Cras suscipit ipsum lacinia felis ultricies egestas.</p>
				
            </div>
		</section>
	</div>
</main>
<?php include('footer.php'); ?>