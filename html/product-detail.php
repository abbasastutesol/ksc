<?php include('header.php'); ?>
<main class="main">
	<div class="container">
		<section class="product-detail-sec">
			<div class="pro-dtl-left">
				<div class="page-title">
					<h1>CABLE GLAND <span>Cable Management Systems</span></h1>
					<div class="pro-desc-dtl">
						<div class="pro-atw"><a href="#">ADD TO LIST</a></div>
						<div class="pdf-view"><a href="#">DOWNLOAD BROCHURE</a></div>
					</div>
				</div>
				<div class="product-slider">
					<div id="productCarousel" class="carousel slide product-carousel" data-ride="carousel">
						<div class="carousel-inner">
							<div class="item active">
								<img src="images/product-slider-img-1.jpg" alt="">
							</div>
							<div class="item">
								<img src="images/product-slider-img-3.jpg" alt="">
							</div>
							<div class="item">
								<img src="images/product-slider-img-2.jpg" alt="">
							</div>
						</div>
						<!-- Indicators -->
						<div class="pc-thumbs carousel-indicators">
							<li data-target="#productCarousel" data-slide-to="0" class="active"><img src="images/product-thumb-1.jpg" alt=""></li>
							<li data-target="#productCarousel" data-slide-to="1"><img src="images/product-thumb-2.jpg" alt=""></li>
							<li data-target="#productCarousel" data-slide-to="2"><img src="images/product-thumb-3.jpg" alt=""></li>
						</div>
						<!-- Wrapper for slides -->
					</div>
				</div>
				<div class="product-description">
					<h2>Brand</h2>
					<p>ABB</p>
					<h2>Dimensions</h2>
					<p>Length: 30mm, Width: 20mm, Height: 112mm</p>
					<h2>Color</h2>
					<p>Black | White | Yellow | Red | Grey</p>
					<h2>Weight</h2>
					<p>25 - 30 Grams</p>
					<h2>Overview &amp; Usage</h2>
					<p>Nullam justo lacus, tristique vitae elementum eget, placerat eget eros. Etiam sodales, lectus eget pellentesque iaculis,</p>
				</div>
			</div>
			<div class="pro-right-col">
				<div class="raq-box">
					<ul>
						<li><i class="fa fa-arrow-<?php echo ($lang=='rtl' ? 'left' : 'right'); ?>" aria-hidden="true"></i> <a href="#">REQUEST A QUOTATION</a>
						</li>
						<li><span class="or">or</span> <a href="#." data-toggle="modal" data-target="#Modal_1">EMAIL US</a>
						</li>
					</ul>
				</div>
				<div class="no-products">
					<p>NO PRODUCTS SELECTED</p>
					<div class="staselc-pro">
						<a href="#">START SELECTING</a>
					</div>
				</div>
				<!--Note for programmers: when user will add product into cart then "cart-list" will show.-->
				<div class="cart-list">
					<div class="cart-item">
						<div class="cart-item-col">
							<div class="brand-name">BRAND NAME</div>
							<div class="part-name">PART NAME</div>
							<div class="cat-name">CATEGORY</div>
						</div>
						<div class="cart-item-col">
							<form action="#" method="post">
								<label>Qty</label>
								<input type="number" class="input-qty" step="1" min="1" max="" name="quantity" value="1" title="Qty" size="4" pattern="[0-9]*" inputmode="numeric">
							</form>
						</div>
						<div class="cart-item-col">
							<a href="#" class="dlt-product"><i class="fa fa-trash" aria-hidden="true"></i></a>
						</div>
					</div>
					<div class="cart-item">
						<div class="cart-item-col">
							<div class="brand-name">BRAND NAME</div>
							<div class="part-name">PART NAME</div>
							<div class="cat-name">CATEGORY</div>
						</div>
						<div class="cart-item-col">
							<form action="#" method="post">
								<label>Qty</label>
								<input type="number" class="input-qty" step="1" min="1" max="" name="quantity" value="1" title="Qty" size="4" pattern="[0-9]*" inputmode="numeric">
							</form>
						</div>
						<div class="cart-item-col">
							<a href="#" class="dlt-product"><i class="fa fa-trash" aria-hidden="true"></i></a>
						</div>
					</div>
					<div class="cart-buttons">
						<a href="#" class="clear-list">CLEAR LIST</a>
						<a href="#." data-toggle="modal" data-target="#Modal_2" class="request-btn">REQUEST</a>
					</div>
				</div>
			</div>
		</section>
	</div>
</main>
<?php include('footer.php'); ?>