<?php include('header.php'); ?>
<main class="main">
	<div class="container">
		<section class="content-wrap search-wrap">
            <div class="page-heading">
                <h1 class="page-title">SEARCH RESULTS</h1>
            </div>
            <div class="search-res-msg">
                <p>23 Search Results for: <span>Lorem ipsum dolor sit amet,</span></p>
            </div>
            <div class="search-content">
                <div class="search-post">
                    <h2>Lorem ipsum dolor sit amet</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed leo enim, luctus ac odio id, accumsan tempor purus. Nam in tempor sem, ac tempor ipsum. Duis mollis convallis urna vitae ullamcorper. Etiam eget velit a augue porta accumsan.</p>
                </div>
                <div class="search-post">
                    <h2>Lorem ipsum dolor sit amet</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed leo enim, luctus ac odio id, accumsan tempor purus. Nam in tempor sem, ac tempor ipsum. Duis mollis convallis urna vitae ullamcorper. Etiam eget velit a augue porta accumsan.</p>
                </div>
                <div class="search-post">
                    <h2>Lorem ipsum dolor sit amet</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed leo enim, luctus ac odio id, accumsan tempor purus. Nam in tempor sem, ac tempor ipsum. Duis mollis convallis urna vitae ullamcorper. Etiam eget velit a augue porta accumsan.</p>
                </div>
                <div class="search-post">
                    <h2>Lorem ipsum dolor sit amet</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed leo enim, luctus ac odio id, accumsan tempor purus. Nam in tempor sem, ac tempor ipsum. Duis mollis convallis urna vitae ullamcorper. Etiam eget velit a augue porta accumsan.</p>
                </div>
                <div class="search-post">
                    <h2>Lorem ipsum dolor sit amet</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed leo enim, luctus ac odio id, accumsan tempor purus. Nam in tempor sem, ac tempor ipsum. Duis mollis convallis urna vitae ullamcorper. Etiam eget velit a augue porta accumsan.</p>
                </div>
                <div class="search-post">
                    <h2>Lorem ipsum dolor sit amet</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed leo enim, luctus ac odio id, accumsan tempor purus. Nam in tempor sem, ac tempor ipsum. Duis mollis convallis urna vitae ullamcorper. Etiam eget velit a augue porta accumsan.</p>
                </div>
                <div class="lds-spinner" style="100%;height:100%"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
            </div>
		</section>
	</div>
</main>
<?php include('footer.php'); ?>