<?php include('header.php'); ?>
<main class="main">
	<div class="container">
		<section class="content-wrap">
			<div class="left-sidebar">
				<ul>
					<li><a href="company-profile.php">PROFILE</a></li>
					<li><a href="vision-mission.php">VISION &amp; MISSION</a></li>
					<li><a href="chairmans-message.php">CHAIRMAN’S MESSAGE</a></li>
					<li class="active"><a href="ceo-message.php">CEO’S MESSAGE</a></li>
					<li><a href="value.php">VALUE</a></li>
					<li><a href="awards.php">AWARDS</a></li>
				</ul>
			</div>
			<div class="right-content">
				<h1>CEO’S MESSAGE</h1>
				<div class="ceo-thumbnail"><img src="images/ceo-thumb.png" alt=""></div>
				<div class="ceo-desc">
					<p>Welcome to the enterprising world of KSC!<br>Thanks to your trust through the years, KSC has grown into one of the Kingdom’s leading suppliers of electrical products and lighting solutions. Today, we continue to set the benchmark for quality, support and product excellence with an ever-expanding product portfolio, our partnership with the world’s leading brands and, most importantly, our highly-trained and professional team who bring over 3 decades of expertise to every project.</p>
					<p>Indeed, the depth and quality of our technical support and customer service have become pillars of our success, helping us build enduring relationships in the construction industry.</p>
					<p>Most importantly, more than fulfilling our promise to deliver the highest standards of excellence for our partners, we are proud of the socio-economic contribution we make on the communities we serve. Together, we are generating jobs, shaping tomorrow’s skylines, improving lives and helping shape our future prosperity.</p>
				</div>
				<div class="ceo-info">
					<h2>Raed Ariss<span class="designation">Chief Executive Officer</span><span class="company-name">KSC Group</span></h2>
				</div>
			</div>
		</section>
	</div>
</main>
<?php include('footer.php'); ?>