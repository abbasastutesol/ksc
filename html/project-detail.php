<?php include('header.php'); ?>
<main class="main">
	<div class="container">
		<section class="content-wrap projects-dtl-sec">
			<div class="page-heading">
				<h1 class="page-title">Projects</h1>
			</div>
			<div class="projects-dtl-wrap">
				<div class="project-lrg-img"><img src="images/project-detail-img.jpg" alt=""></div>
				<p>Nullam justo lacus, tristique vitae elementum eget, placerat eget eros. Etiam sodales, lectus eget pellentesque iaculis, odio velit auctor massa, non maximus eros libero in ante. Morbi sodales ligula eu orci commodo, ut tincidunt est aliquet. Morbi sit amet libero id eros sollicitudin pharetra. Duis hendrerit hendrerit ligula bibendum tempor. Quisque eu elit vitae felis varius sollicitudin. Pellentesque porta purus id porttitor consequat. Mauris sagittis est ante, in congue est vulputate tempus. In et neque nulla.</p>
				<ul>
					<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam volutpat iaculis cursus.</li>
					<li>Etiam nulla tellus, congue id bibendum eget, auctor ut quam. Proin nunc lectus, faucibus faucibus neque dapibus, ultrices vehicula diam. Sedcommodo, ante sed pellentesque viverra, arcu nisi iaculi tempus.</li>
				</ul>
				<div class="project-gal">
					<ul>
						<li><a href="#." data-toggle="modal" data-target="#Modal_3"><img src="images/project-gal-img-1.jpg" alt=""></a></li>
						<li><a href="#." data-toggle="modal" data-target="#Modal_3"><img src="images/project-gal-img-2.jpg" alt=""></a></li>
						<li><a href="#." data-toggle="modal" data-target="#Modal_3"><img src="images/project-gal-img-3.jpg" alt=""></a></li>
						<li><a href="#." data-toggle="modal" data-target="#Modal_3"><img src="images/project-gal-img-4.jpg" alt=""></a></li>
						<li><a href="#." data-toggle="modal" data-target="#Modal_3"><img src="images/project-gal-img-5.jpg" alt=""></a></li>
					</ul>
				</div>
			</div>
		</section>
	</div>
</main>
<?php include('footer.php'); ?>