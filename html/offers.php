<?php include('header.php'); ?>
<main class="main">
	<div class="container">
		<section class="content-wrap offers-sec">
			<div class="page-heading">
				<h1 class="page-title">OFFERS</h1>
			</div>
			<div class="products-sec">
				<div class="offers-img"><img src="images/offers-img.jpg" alt=""></div>
				<div class="pro-left-col">
					<div class="products-container">
						<!--Note for programmers: When user click on "ADD TO LIST" button then please add "active" class on "ptro-box".-->
						<div class="ptro-box active">
							<div class="pro-thumb"><img src="images/product-img-1.png" alt=""></div>
							<div class="pro-info">
								<p><span class="brand-name">BRAND NAME</span><span class="part-name">PART NAME</span></p>
								<p>CATEGORY</p>
							</div>
							<div class="pro-dtl">
								<div class="pro-view"><a href="product-detail.php">VIEW</a></div>
								<div class="pro-atw"><a href="#">ADD TO LIST</a></div>
							</div>
						</div>
						<div class="ptro-box">
							<div class="pro-thumb"><img src="images/product-img-2.png" alt=""></div>
							<div class="pro-info">
								<p><span class="brand-name">BRAND NAME</span></p>
								<p>CATEGORY</p>
							</div>
							<div class="pro-dtl">
								<div class="pdf-view"><a href="#" target="_blank">VIEW PDF</a></div>
								<div class="pro-atw"><a href="#">ADD TO LIST</a></div>
							</div>
						</div>
						<div class="ptro-box">
							<div class="pro-thumb"><img src="images/product-img-3.png" alt=""></div>
							<div class="pro-info">
								<p><span class="brand-name">BRAND NAME</span><span class="part-name">PART NAME</span></p>
								<p>CATEGORY</p>
							</div>
							<div class="pro-dtl">
								<div class="pro-view"><a href="product-detail.php">VIEW</a></div>
								<div class="pro-atw"><a href="#">ADD TO LIST</a></div>
							</div>
						</div>
						<div class="ptro-box">
							<div class="pro-thumb"><img src="images/product-img-4.png" alt=""></div>
							<div class="pro-info">
								<p><span class="brand-name">BRAND NAME</span><span class="part-name">PART NAME</span></p>
								<p>CATEGORY</p>
							</div>
							<div class="pro-dtl">
								<div class="pro-view"><a href="product-detail.php">VIEW</a></div>
								<div class="pro-atw"><a href="#">ADD TO LIST</a></div>
							</div>
						</div>
						<div class="ptro-box">
							<div class="pro-thumb"><img src="images/product-img-5.png" alt=""></div>
							<div class="pro-info">
								<p><span class="brand-name">BRAND NAME</span><span class="part-name">PART NAME</span></p>
								<p>CATEGORY</p>
							</div>
							<div class="pro-dtl">
								<div class="pro-view"><a href="product-detail.php">VIEW</a></div>
								<div class="pro-atw"><a href="#">ADD TO LIST</a></div>
							</div>
						</div>
						<div class="ptro-box">
							<div class="pro-thumb"><img src="images/product-img-6.png" alt=""></div>
							<div class="pro-info">
								<p><span class="brand-name">BRAND NAME</span><span class="part-name">PART NAME</span></p>
								<p>CATEGORY</p>
							</div>
							<div class="pro-dtl">
								<div class="pro-view"><a href="product-detail.php">VIEW</a></div>
								<div class="pro-atw"><a href="#">ADD TO LIST</a></div>
							</div>
						</div>
						<div class="ptro-box">
							<div class="pro-thumb"><img src="images/product-img-7.png" alt=""></div>
							<div class="pro-info">
								<p><span class="brand-name">BRAND NAME</span><span class="part-name">PART NAME</span></p>
								<p>CATEGORY</p>
							</div>
							<div class="pro-dtl">
								<div class="pro-view"><a href="product-detail.php">VIEW</a></div>
								<div class="pro-atw"><a href="#">ADD TO LIST</a></div>
							</div>
						</div>
						<div class="ptro-box">
							<div class="pro-thumb"><img src="images/product-img-8.png" alt=""></div>
							<div class="pro-info">
								<p><span class="brand-name">BRAND NAME</span><span class="part-name">PART NAME</span></p>
								<p>CATEGORY</p>
							</div>
							<div class="pro-dtl">
								<div class="pro-view"><a href="product-detail.php">VIEW</a></div>
								<div class="pro-atw"><a href="#">ADD TO LIST</a></div>
							</div>
						</div>
						<div class="ptro-box">
							<div class="pro-thumb"><img src="images/product-img-1.png" alt=""></div>
							<div class="pro-info">
								<p><span class="brand-name">BRAND NAME</span><span class="part-name">PART NAME</span></p>
								<p>CATEGORY</p>
							</div>
							<div class="pro-dtl">
								<div class="pro-view"><a href="product-detail.php">VIEW</a></div>
								<div class="pro-atw"><a href="#">ADD TO LIST</a></div>
							</div>
						</div>
						<div class="ptro-box">
							<div class="pro-thumb"><img src="images/product-img-2.png" alt=""></div>
							<div class="pro-info">
								<p><span class="brand-name">BRAND NAME</span><span class="part-name">PART NAME</span></p>
								<p>CATEGORY</p>
							</div>
							<div class="pro-dtl">
								<div class="pro-view"><a href="product-detail.php">VIEW</a></div>
								<div class="pro-atw"><a href="#">ADD TO LIST</a></div>
							</div>
						</div>
						<div class="ptro-box">
							<div class="pro-thumb"><img src="images/product-img-3.png" alt=""></div>
							<div class="pro-info">
								<p><span class="brand-name">BRAND NAME</span><span class="part-name">PART NAME</span></p>
								<p>CATEGORY</p>
							</div>
							<div class="pro-dtl">
								<div class="pro-view"><a href="product-detail.php">VIEW</a></div>
								<div class="pro-atw"><a href="#">ADD TO LIST</a></div>
							</div>
						</div>
						<div class="ptro-box">
							<div class="pro-thumb"><img src="images/product-img-4.png" alt=""></div>
							<div class="pro-info">
								<p><span class="brand-name">BRAND NAME</span><span class="part-name">PART NAME</span></p>
								<p>CATEGORY</p>
							</div>
							<div class="pro-dtl">
								<div class="pro-view"><a href="product-detail.php">VIEW</a></div>
								<div class="pro-atw"><a href="#">ADD TO LIST</a></div>
							</div>
						</div>
					</div>
					<div class="page-navigation">
						<ul class="pagination">
							<li class="active"><a href="">1</a></li>
							<li class="page"><a href="#2">2</a></li>
							<li class="page"><a href="#3">3</a></li>
							<li class="page"><a href="#4">4</a></li>
							<li class="page"><a href="#5">5</a></li>
							<li class="page"><a href="#6">6</a></li>
							<li class="page"><a href="#7">7</a></li>
							<li class="next page"><a href="#2">Next →</a></li>
							<li class="next page"><a href="#793">Last »</a></li>				
						</ul>
					</div>
				</div>
				<div class="pro-right-col">
					<div class="raq-box">
						<ul>
							<li><i class="fa fa-arrow-<?php echo ($lang=='rtl' ? 'left' : 'right'); ?>" aria-hidden="true"></i> <a href="#">REQUEST A QUOTATION</a></li>
							<li><span class="or">or</span> <a href="#." data-toggle="modal" data-target="#Modal_1">EMAIL US</a></li>
						</ul>
					</div>
					<div class="no-products">
						<p>NO PRODUCTS SELECTED</p>
						<div class="staselc-pro">
							<a href="#">START SELECTING</a>
						</div>
					</div>
					<!--Note for programmers: when user will add product into cart then "cart-list" will show.-->
					<div class="cart-list">
						<div class="cart-item">
							<div class="cart-item-col">
								<div class="brand-name">BRAND NAME</div>
								<div class="part-name">PART NAME</div>
								<div class="cat-name">CATEGORY</div>
							</div>
							<div class="cart-item-col">
								<form action="#" method="post">
									<label>Qty</label>
									<input type="number" class="input-qty" step="1" min="1" max="" name="quantity" value="1" title="Qty" size="4" pattern="[0-9]*" inputmode="numeric">
								</form>
							</div>
							<div class="cart-item-col">
								<a href="#" class="dlt-product"><i class="fa fa-trash" aria-hidden="true"></i></a>
							</div>
						</div>
						<div class="cart-item">
							<div class="cart-item-col">
								<div class="brand-name">BRAND NAME</div>
								<div class="part-name">PART NAME</div>
								<div class="cat-name">CATEGORY</div>
							</div>
							<div class="cart-item-col">
								<form action="#" method="post">
									<label>Qty</label>
									<input type="number" class="input-qty" step="1" min="1" max="" name="quantity" value="1" title="Qty" size="4" pattern="[0-9]*" inputmode="numeric">
								</form>
							</div>
							<div class="cart-item-col">
								<a href="#" class="dlt-product"><i class="fa fa-trash" aria-hidden="true"></i></a>
							</div>
						</div>
						<div class="cart-buttons">
							<a href="#" class="clear-list">CLEAR LIST</a>
							<a href="#." data-toggle="modal" data-target="#Modal_2" class="request-btn">REQUEST</a>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</main>
<?php include('footer.php'); ?>