<?php include('header.php'); ?>
<main class="main">
	<div class="container">
		<section class="content-wrap">
			<div class="left-sidebar">
				<ul>
					<li><a href="company-profile.php">PROFILE</a></li>
					<li><a href="vision-mission.php">VISION &amp; MISSION</a></li>
					<li><a href="chairmans-message.php">CHAIRMAN’S MESSAGE</a></li>
					<li><a href="ceo-message.php">CEO’S MESSAGE</a></li>
					<li><a href="value.php">VALUE</a></li>
					<li class="active"><a href="awards.php">AWARDS</a></li>
				</ul>
			</div>
			<div class="right-content">
				<h1>Awards</h1>
				<ul class="awards-list">
					<li><a href="#." data-toggle="modal" data-target="#Modal_0"><span class="year">2015</span><img src="images/award-img-1.png" alt="" width="198" height="248"></a></li>
					<li><a href="#." data-toggle="modal" data-target="#Modal_0"><span class="year">2013</span><img src="images/award-img-2.png" alt="" width="198" height="248"></a></li>
					<li><a href="#." data-toggle="modal" data-target="#Modal_0"><span class="year">2013</span><img src="images/award-img-3.png" alt="" width="180" height="250"></a></li>
					<li><a href="#." data-toggle="modal" data-target="#Modal_0"><span class="year">2012</span><img src="images/award-img-4.png" alt="" width="190" height="248"></a></li>
					<li><a href="#." data-toggle="modal" data-target="#Modal_0"><span class="year">2002</span><img src="images/award-img-5.png" alt="" width="358" height="250"></a></li>
				</ul>
			</div>
		</section>
	</div>
</main>
<?php include('footer.php'); ?>