<?php include('header.php'); ?>
<section class="banner-sec scrlFX">
	<div class="container">
		<div id="carousel-1" class="carousel slide" data-ride="carousel">
			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				<div class="item active">
					<img src="images/slider-img-1.jpg" alt="">
					<div class="caption-wrap">
						<div class="caption">
							<h1>THE LEADING PROVIDER</h1>
							<p>OF ELECTRICAL SOLUTIONS FOR THE CONSTRUCTION INDUSTRY</p>
							<div class="banner-readmore"><a href="#">READ MORE</a></div>
						</div>
					</div>
				</div>

				<div class="item">
					<img src="images/slider-img-2.jpg" alt="">
					<div class="caption-wrap">
						<div class="caption">
							<h1>BUILDER</h1>
							<p>OF CAPABILITIES</p>
							<div class="banner-readmore"><a href="#">READ MORE</a></div>
						</div>
					</div>
				</div>

				<div class="item">
					<img src="images/slider-img-3.jpg" alt="">
					<div class="caption-wrap">
						<div class="caption">
							<h1>SPECIAL OFFER</h1>
							<div class="banner-readmore"><a href="#">READ MORE</a></div>
						</div>
					</div>
				</div>
			</div>
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#carousel-1" data-slide-to="0" class="active"></li>
				<li data-target="#carousel-1" data-slide-to="1"></li>
				<li data-target="#carousel-1" data-slide-to="2"></li>
			</ol>
			<div class="raq-box">
				<ul>
					<li><i class="fa fa-arrow-<?php echo ($lang=='rtl' ? 'left' : 'right'); ?>" aria-hidden="true"></i> <a href="#." data-toggle="modal" data-target="#Modal_2">REQUEST A QUOTATION</a></li>
					<li><span class="or">or</span> <a href="#." data-toggle="modal" data-target="#Modal_1">EMAIL US</a></li>
				</ul>
			</div>
		</div>
	</div>
</section>
<section class="hm-content scrlFX">
	<div class="container">
		<div class="about-sec">
			<div class="about-vid">Video Tour</div>
			<div class="about-left">
				<div class="hmVideo"><img src="images/video-poster.png" alt=""><img src="images/play-icon.png" alt="" id="video-btn"></div>
			</div>
			<div class="about-right">
				<h1>COMPANY <span>OVERVIEW</span></h1>
				<p>We have pleasure in introducing our co. as a magor distributor and stockist of electrical/Mechanical products throughout the Kingdom. Khereiji Showrooms Co. (K.S.C) was formed in 1980 to meet an ever increasing demand for electrical products to the construction and other related industries in Saudi Arabia. We have acquired through expert market research a comprehensive range of electrical products supplied through various distributors and exclusive representational agreements. These agreements and right of distribution have enabled K.S.C to become a magor supplier on numerous large projects within the Kingdom, due to the quality materials..</p>
				<p><a href="offers.php">READ MORE</a></p>
			</div>
		</div>
	</div>
</section>
<section class="hm-pro-sec scrlFX">
	<div class="container">
		<div class="hm-heading-row">
			<h1>PRODUCT <span>RANGE</span></h1>
		</div>
		<div class="product-sec">
			<div class="product-box" id="pro-box-1">
				<div class="product-thumb"><img src="images/pro-thumb-1.jpg" alt=""></div>
				<div class="product-title">
					<h2>CABLE<span>management systems</span></h2>
				</div>
				<div class="product-desc">
					<h2>CABLE<span>management systems</span></h2>
					<p>Curabitur faucibus, lorem in rhoncus interdum, felis eros porta enim, nec condimentum lorem est ac lacus.</p>
					<div class="prodesc-btn"><a href="products.php">VIEW ALL</a></div>
				</div>
			</div>
			<div class="product-box" id="pro-box-2">
				<div class="product-thumb"><img src="images/pro-thumb-2.jpg" alt=""></div>
				<div class="product-title">
					<h2>EARTHING<span>PROTECTION</span></h2>
				</div>
				<div class="product-desc">
					<h2>EARTHING AND LIGHTING<span>PROTECTION</span></h2>
					<p>Curabitur faucibus, lorem in rhoncus interdum, felis eros porta enim, nec condimentum lorem est ac lacus.</p>
					<div class="prodesc-btn"><a href="products.php">VIEW ALL</a></div>
				</div>
			</div>
			<div class="product-box" id="pro-box-3">
				<div class="product-thumb"><img src="images/pro-thumb-3.jpg" alt=""></div>
				<div class="product-title">
					<h2>WIRING<span>DEVICES</span></h2>
				</div>
				<div class="product-desc">
					<h2>WIRING<span>DEVICES</span></h2>
					<p>Curabitur faucibus, lorem in rhoncus interdum, felis eros porta enim, nec condimentum lorem est ac lacus.</p>
					<div class="prodesc-btn"><a href="products.php">VIEW ALL</a></div>
				</div>
			</div>
			<div class="product-box" id="pro-box-4">
				<div class="product-thumb"><img src="images/pro-thumb-4.jpg" alt=""></div>
				<div class="product-title">
					<h2>PVC<span>CONDUITS</span></h2>
				</div>
				<div class="product-desc">
					<h2>PVC<span>CONDUITS</span></h2>
					<p>Curabitur faucibus, lorem in rhoncus interdum, felis eros porta enim, nec condimentum lorem est ac lacus.</p>
					<div class="prodesc-btn"><a href="products.php">VIEW ALL</a></div>
				</div>
			</div>
			<div class="product-box" id="pro-box-5">
				<div class="product-thumb"><img src="images/pro-thumb-5.jpg" alt=""></div>
				<div class="product-title">
					<h2>LOW VOLTAGE <span>DISTRIBUTION BOARDS</span></h2>
				</div>
				<div class="product-desc">
					<h2>LOW VOLTAGE <span>DISTRIBUTION BOARDS</span></h2>
					<p>Curabitur faucibus, lorem in rhoncus interdum, felis eros porta enim, nec condimentum lorem est ac lacus.</p>
					<div class="prodesc-btn"><a href="products.php">VIEW ALL</a></div>
				</div>
			</div>
			<div class="product-box" id="pro-box-6">
				<div class="product-thumb"><img src="images/pro-thumb-6.jpg" alt=""></div>
				<div class="product-title">
					<h2>CABLE<span>GLANDS</span></h2>
				</div>
				<div class="product-desc">
					<h2>CABLE<span>GLANDS</span></h2>
					<p>Curabitur faucibus, lorem in rhoncus interdum, felis eros porta enim, nec condimentum lorem est ac lacus.</p>
					<div class="prodesc-btn"><a href="products.php">VIEW ALL</a></div>
				</div>
			</div>
			<div class="product-box" id="pro-box-7">
				<div class="product-thumb"><img src="images/pro-thumb-7.jpg" alt=""></div>
				<div class="product-title">
					<h2>CABLE<span>LUGS</span></h2>
				</div>
				<div class="product-desc">
					<h2>CABLE<span>LUGS</span></h2>
					<p>Curabitur faucibus, lorem in rhoncus interdum, felis eros porta enim, nec condimentum lorem est ac lacus.</p>
					<div class="prodesc-btn"><a href="products.php">VIEW ALL</a></div>
				</div>
			</div>
			<div class="product-box" id="pro-box-8">
				<div class="product-thumb"><img src="images/pro-thumb-8.jpg" alt=""></div>
				<div class="product-title">
					<h2>LIGHTING<span>SOLUTIONS</span></h2>
				</div>
				<div class="product-desc">
					<h2>LIGHTING<span>SOLUTIONS</span></h2>
					<p>Curabitur faucibus, lorem in rhoncus interdum, felis eros porta enim, nec condimentum lorem est ac lacus.</p>
					<div class="prodesc-btn"><a href="products.php">VIEW ALL</a></div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="hm-partners scrlFX">
	<div class="container">
		<div class="hm-heading-row">
			<h1>OUR <span>PARTNERS</span></h1>
		</div>
		<div class="partners-wrap client-logos">
			<ul>
				<li><a href="#"><img src="images/logo-img-1.png" alt=""></a></li>
				<li><a href="#"><img src="images/logo-img-2.png" alt=""></a></li>
				<li><a href="#"><img src="images/logo-img-3.png" alt=""></a></li>
				<li><a href="#"><img src="images/logo-img-4.png" alt=""></a></li>
				<li><a href="#"><img src="images/logo-img-5.png" alt=""></a></li>
				<li><a href="#"><img src="images/logo-img-6.png" alt=""></a></li>
				<li><a href="#"><img src="images/logo-img-7.png" alt=""></a></li>
				<li><a href="#"><img src="images/logo-img-8.png" alt=""></a></li>
			</ul>
			<div class="more-logos"><a href="partners.php">Read More</a></div>
		</div>
	</div>
</section>
<section class="hm-showrooms scrlFX">
	<div class="container">
		<div class="hm-heading-row">
			<h1>OUR <span>SHOWROOMS</span></h1>
		</div>
		<div class="showrooms-wrap">
			<div class="showrooms-left"><img src="images/showrooms-img.jpg" alt=""></div>
			<div class="showrooms-right">
				<h1>KSC JEDDAH <span>Head quarter</span></h1>
				<p>BALADIYAH STREET, JEDDAH, KINGDOM OF SAUDI ARABIA. <span>T: <a href="tel:+966 12 1234567">+966 12 1234567</a></span></p>
				<div class="view-map"><a href="showrooms.php" class="red-btn">VIEW MAP</a></div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery("#video-btn").click(function(){
		jQuery(".hmVideo").html('<iframe width="100%" height="100%" src="https://www.youtube.com/embed/5Peo-ivmupE?rel=0&amp;controls=1&amp;showinfo=1&amp;autoplay=1&wmode=opaque&amp;wmode=transparent" frameborder="0" allowfullscreen></iframe>');
	});
});
</script>
<?php include('footer.php'); ?>