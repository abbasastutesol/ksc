<?php include('header.php'); ?>
<main class="main">
	<div class="container">
		<section class="content-wrap showrooms-sec">
			<div class="page-heading">
				<h1 class="page-title">SHOWROOMS</h1>
			</div>
			<div class="showrooms-wrapper">
				<div class="locations-list">
					<ul>
						<li><a href="showrooms.php">JEDDAH</a></li>
						<li><a href="riyadh-showroom.php">RIYADH</a></li>
						<li class="active"><a href="dammam-showroom.php">DAMMAM</a></li>
					</ul>
				</div>
				<div class="showrooms-map">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14839.442917872297!2d39.18118516944333!3d21.5913587461082!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x15c3d0e41a565aef%3A0x57fcbd7ca765fe68!2sUnnamed+Road%2C+Al-Rabwah%2C+Jeddah+23448%2C+Saudi+Arabia!5e0!3m2!1sen!2s!4v1511416482530" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
				<section class="hm-showrooms scrlFX">
					<div class="showrooms-wrap">
						<div class="showrooms-left"><img src="images/showrooms-img.jpg" alt=""></div>
						<div class="showrooms-right">
							<h1>KSC DAMMAM</h1>
							<p>BALADIYAH STREET, JEDDAH, KINGDOM OF SAUDI ARABIA. <span>T: <a href="tel:+966 12 1234567">+966 12 1234567</a></span></p>
						</div>
					</div>
				</section>
			</div>
		</section>
	</div>
</main>
<?php include('footer.php'); ?>