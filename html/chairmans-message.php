<?php include('header.php'); ?>
<main class="main">
	<div class="container">
		<section class="content-wrap">
			<div class="left-sidebar">
				<ul>
					<li><a href="company-profile.php">PROFILE</a></li>
					<li><a href="vision-mission.php">VISION &amp; MISSION</a></li>
					<li class="active"><a href="chairmans-message.php">CHAIRMAN’S MESSAGE</a></li>
					<li><a href="ceo-message.php">CEO’S MESSAGE</a></li>
					<li><a href="value.php">VALUE</a></li>
					<li><a href="awards.php">AWARDS</a></li>
				</ul>
			</div>
			<div class="right-content">
				<h1>CHAIRMAN’S MESSAGE</h1>
				<div class="chairman-thumbnail"><img src="images/chairmans-thumb.png" alt=""></div>
				<div class="chairman-desc">
					<p>I am delighted to welcome you all to our family.<br>I would like to express my earnest gratitude to all our customers and partners who have been part of our growth through the years. From quality to pricing to our ability to provide you with total solutions, we are committed to constantly exceed your highest expectations. We continue to place great importance on our values of integrity, accountability, honesty and respect while providing innovative, integrated solutions to deliver a total approach to your needs.</p>
					<p>The future has never looked more exciting despite the challenges. I am proud to say that KSC’s roadmap to the future is clearly charted, powered by the growth of our trading and manufacturing activities and anchored on over 30 years of trusted expertise, allowing us to grow into a truly integrated solutions provider for our customers. Thank you for your interest in KSC. We look forward to sharing our journey with you as together, we build a brighter world with superior quality and total excellence.</p>
				</div>
				<div class="chairman-info">
					<h2>Sheikh Mohammed Yousef El-Khereiji<span class="designation">Chairman</span><span class="company-name">KSC Group</span></h2>
				</div>
			</div>
		</section>
	</div>
</main>
<?php include('footer.php'); ?>