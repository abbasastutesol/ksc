<?php
if(isset($_GET['lang']) and !empty($_GET['lang'])){
	$lang = $_SEESION['lang']=$_GET['lang'];
}else{
	$lang = $_SEESION['lang']='ltr';
}
//			For English Add Class in Body "eng"
//			For Arabic Add Class in Body "arb"
?>
<html lang="en" <?php if ($lang=='rtl') { echo 'dir="rtl" '; } ?>>
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="images/favicon.ico" type="image/x-icon">
	<title>Khereiji Showrooms Company</title>
	<!--[if gte IE 9]
		<style type="text/css">
			.gradient {
				filter: none;
			}
		</style>
	<![endif]-->
	<!--[if lt IE 9]>
	<!--<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>-->
	<![endif]-->
	<!--<link rel="stylesheet" href="css/jquery-ui_1.12.1_themes.css">-->
	<!--<link href="css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" media="all">-->
	<!--<link href="css/jquery.fancybox.css" rel="stylesheet" type="text/css" media="all">-->
	<!--<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">-->
	<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" media="all">
	<link href="css/<?php echo ($lang=='rtl' ? 'rtl' : 'ltr'); ?>.css" rel="stylesheet" type="text/css" media="all">
	<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=false&controls=1"></script>-->
	<script src="js/jquery-2.1.3.min.js" type="text/javascript"></script>
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
	<script src="js/jquery.material.form.js" type="text/javascript"></script>
	<script src="js/jquery.validate.min.js" type="text/javascript"></script>
	<script src="js/scripts.js" type="text/javascript"></script>
</head>
<?php
$link =  "//$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$pieces = explode("ksc/", $link);
$class_name=str_replace(".php","",$pieces[1]);
 ?>
<body class="<?php echo $class_name; ?> <?php if (stripos($_SERVER['REQUEST_URI'], 'index.php')){echo 'homepage';} ?>">
	<header class="header scrlFX">
		<div class="top-header">
			<div class="container">
				<div class="search-box">
					<form action="search-result.php" method="post">
						<input type="search" name="search" placeholder="SEARCH">
					</form>
				</div>
				<div class="lang-box">
					<a href="#"><?php echo ($lang=='rtl' ? 'English' : 'العربية'); ?></a>
				</div>
			</div>
		</div>
		<div class="btm-header">
			<div class="container">
				<div class="logo"><a href="index.php"><img src="images/logo.jpg" alt="" width="312" height="76"></a></div>
				<nav class="nav menu-main-menu-container">
					<div class="lang-box">
						<a href="#"><?php echo ($lang=='rtl' ? 'English' : 'العربية'); ?></a>
					</div>
					<ul>
						<li class="has-dropdown">
							<a href="javascript:void(0);"><?php echo ($lang=='ltr' ? 'KSC' : 'ش.م.ك'); ?></a>
							<ul class="sub-menu">
								<li class="active"><a href="company-profile.php"><?php echo ($lang=='ltr' ? 'COMPANY PROFILE' : 'أصل الشركة'); ?></a></li>
								<li><a href="vision-mission.php"><?php echo ($lang=='ltr' ? 'MISSION &amp; VISION' : 'رؤيتنا و مهمتنا'); ?></a></li>
								<li><a href="chairmans-message.php"><?php echo ($lang=='ltr' ? 'CHAIRMAN`S MESSAGE' : 'رسالة رئيس مجلس الإدارة'); ?></a></li>
								<li><a href="ceo-message.php"><?php echo ($lang=='ltr' ? 'CEO’S MESSAGE' : 'رسالة المدير التنفيذي'); ?></a></li>
								<li><a href="value.php"><?php echo ($lang=='ltr' ? 'VALUES' : ' قيمنا'); ?></a></li>
								<li><a href="awards.php"><?php echo ($lang=='ltr' ? 'AWARDS' : ' جوائز'); ?></a></li>
							</ul>
						</li>
						<li><a href="clients.php"><?php echo ($lang=='ltr' ? 'CLIENTS' : 'عملاء'); ?></a></li>
						<li><a href="products.php"><?php echo ($lang=='ltr' ? 'PRODUCTS' : 'منتجات'); ?></a></li>
						<li><a href="projects.php"><?php echo ($lang=='ltr' ? 'PROJECTS' : 'مشاريع'); ?></a></li>
						<li><a href="showrooms.php"><?php echo ($lang=='ltr' ? 'SHOWROOMS' : 'صالات العرض'); ?></a></li>
						<li><a href="offers.php"><?php echo ($lang=='ltr' ? 'OFFERS' : 'عروض'); ?></a></li>
						<li><a href="careers.php"><?php echo ($lang=='ltr' ? 'CAREERS' : 'وظائف'); ?></a></li>
						<li><a href="contact.php"><?php echo ($lang=='ltr' ? 'CONTACT' : 'أتصل بنا'); ?></a></li>
					</ul>
				</nav>
				<div class="mob-menu-icon"><a href="javascript:void(0);" class="menu-opener"><span></span></a></div>
			</div>
		</div>
	</header>