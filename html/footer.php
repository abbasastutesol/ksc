<footer class="footer scrlFX">
	<div class="footer-top">
		<div class="container">
			<div class="footer-col">
				<ul>
					<li class="has-dropdown active"><a href="javascript:void(0);"><?php echo ($lang=='ltr' ? 'KSC' : 'ش.م.ك'); ?></a>
						<ul class="sub-menu">
							<li><a href="company-profile.php"><?php echo ($lang=='ltr' ? 'COMPANY PROFILE' : 'أصل الشركة'); ?></a></li>
							<li><a href="vision-mission.php"><?php echo ($lang=='ltr' ? 'MISSION &amp; VISION' : 'رؤيتنا و مهمتنا'); ?></a></li>
							<li><a href="chairmans-message.php"><?php echo ($lang=='ltr' ? 'CHAIRMAN`S MESSAGE' : 'رسالة رئيس مجلس الإدارة'); ?></a></li>
							<li><a href="ceo-message.php"><?php echo ($lang=='ltr' ? 'CEO’S MESSAGE' : 'رسالة المدير التنفيذي'); ?></a></li>
							<li><a href="value.php"><?php echo ($lang=='ltr' ? 'VALUES' : ' قيمنا'); ?></a></li>
							<li><a href="awards.php"><?php echo ($lang=='ltr' ? 'AWARDS' : ' جوائز'); ?></a></li>
						</ul>
					</li>
				</ul>
			</div>
			<div class="footer-col">
				<ul>
					<li><a href="clients.php"><?php echo ($lang=='ltr' ? 'CLIENTS' : 'عملاء'); ?></a></li>
					<li><a href="products.php"><?php echo ($lang=='ltr' ? 'PRODUCTS' : 'منتجات'); ?></a></li>
					<li><a href="projects.php"><?php echo ($lang=='ltr' ? 'PROJECTS' : 'مشاريع'); ?></a></li>
					<li><a href="offers.php"><?php echo ($lang=='ltr' ? 'OFFERS' : 'عروض'); ?></a></li>
				</ul>
			</div>
			<div class="footer-col">
				<ul>
					<li class="has-dropdown"><a href="#"><?php echo ($lang=='ltr' ? 'SHOWROOMS' : 'صالات العرض'); ?></a>
						<ul class="sub-menu">
							<li><a href="showrooms.php"><?php echo ($lang=='ltr' ? 'JEDDAH' : 'منتجات'); ?></a></li>
							<li><a href="riyadh-showroom.php"><?php echo ($lang=='ltr' ? 'RIYADH' : 'مشاريع'); ?></a></li>
							<li><a href="dammam-showroom"><?php echo ($lang=='ltr' ? 'DAMMAM' : 'عروض'); ?></a></li>
						</ul>
					</li>
				</ul>
			</div>
			<div class="footer-col">
				<ul>
					<li class="has-dropdown"><a href="careers.php"><?php echo ($lang=='ltr' ? 'CAREERS' : 'صالات العرض'); ?></a>
						<ul class="sub-menu">
							<li><a href="careers.php"><?php echo ($lang=='ltr' ? 'JOB OPENINGS' : 'منتجات'); ?></a></li>
							<li><a href="#." data-toggle="modal" data-target="#Modal_4"><?php echo ($lang=='ltr' ? 'APPLY' : 'مشاريع'); ?></a></li>
						</ul>
					</li>
				</ul>
			</div>
			<div class="footer-col">
				<ul>
					<li><a href="contact.php"><?php echo ($lang=='ltr' ? 'CONTACT' : 'صالات العرض'); ?></a></li>
					<li><a href="#." data-toggle="modal" data-target="#Modal_6"><?php echo ($lang=='ltr' ? 'NEWSLETTER' : 'صالات العرض'); ?></a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="footer-btm">
		<div class="container">
			<div class="footer-left">
				<div class="copyrights">
					<p>COPYRIGHTS | KSC, 2018</p>
				</div>
				<div class="information"><a href="legal-info.php">Legal Information</a>
				</div>
			</div>
			<div class="footer-right">
				<div class="footer-mail"><a href="mailto:customer.support@ksc.com.sa"><img src="images/envelope-icon.png" alt="">customer.support@ksc.com.sa</a>
				</div>
				<div class="footer-number"><a href="tel:+966 12 6652757"><img src="images/telephone-icon.png" alt="">+966 12 6652757</a>
				</div>
				<div class="footer-sm-links">
					<ul>
						<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</footer>

<!-- Awards Image Modal Start-->
<div id="Modal_0" class="modal fade awards-lightbox" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<div class="modal-body"><img src="images/award-img-1.png" alt="Image">
			</div>
		</div>
	</div>
</div>
<!-- Awards Image Modal end-->
<!-- Email Us Modal Start-->
<div id="Modal_1" class="modal fade raq-lightbox" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<div class="modal-body">
				<h1>Email Us</h1>
				<div class="popup-form">
					<form action="#" class="material">
						<div class="pf-row">
							<input type="text" name="name" placeholder="Name" required>
						</div>
						<div class="pf-row">
							<input type="email" name="email" placeholder="Email" required>
						</div>
						<div class="pf-row">
							<input type="tel" name="mobile" placeholder="MOBILE <span>(For example +966 5X XXX XXXX)</span>">
						</div>
						<div class="pf-row">
							<input type="text" name="company" placeholder="Company">
						</div>
						<div class="pf-row">
							<textarea name="message" placeholder="MESSAGE"></textarea>
						</div>
						<div class="pf-row">
							<div class="pf-col-left"><img src="images/recaptcha-img.png" alt="">
							</div>
							<div class="pf-col-right"><input type="submit" value="REQUEST">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Email Us Modal Modal end-->
<!-- Products Request Modal Start-->
<div id="Modal_2" class="modal fade raq-lightbox" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<div class="modal-body">
				<h1>REQUEST A QUOTE</h1>
				<div class="popup-form">
					<form action="#" class="material">
						<div class="pf-row">
							<input type="text" name="name" placeholder="Name" required>
						</div>
						<div class="pf-row">
							<input type="email" name="email" placeholder="Email" required>
						</div>
						<div class="pf-row">
							<input type="tel" name="mobile" placeholder="MOBILE <span>(For example +966 5X XXX XXXX)</span>">
						</div>
						<div class="pf-row">
							<input type="text" name="company" placeholder="Company">
						</div>
						<div class="pf-row">
							<input type="text" name="country" placeholder="Country">
						</div>
						<div class="pf-row">
							<input type="text" name="city" placeholder="City">
						</div>
						<div class="pf-row">
							<div class="pf-col-left"><img src="images/recaptcha-img.png" alt="">
							</div>
							<div class="pf-col-right"><input type="submit" value="REQUEST">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Products Request  Modal end-->
<!-- Awards Image Modal Start-->
<div id="Modal_3" class="modal fade project-carousel" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<div class="modal-body">
				<div id="projectCarousel" class="carousel slide" data-ride="carousel">
					<!-- Wrapper for slides -->
					<div class="carousel-inner">
						<div class="item active">
							<img src="images/project-detail-slide.jpg" alt="">
							<div class="caption">
								<p>Nullam justo lacus, tristique vitae elementum eget, placerat eget eros. Etiam sodales, lectus eget pellentesque iaculis, odio velit auctor massa, non maximus eros libero in ante. Morbi sodales ligula eu orci commodo, ut tincidunt est aliquet. Morbi sit amet libero id eros sollicitudin pharetra. Duis hendrerit hendrerit ligula bibendum tempor. Quisque eu elit vitae felis varius sollicitudin. Pellentesque porta purus id porttitor consequat. Mauris sagittis est ante, in congue est vulputate tempus. In et neque nulla. Nam varius, mauris sagittis vulputate dignissim, ante dolor suscipit lacus, vitae aliquam nulla enim at nibh.</p>
							</div>
						</div>

						<div class="item">
							<img src="images/project-detail-img.jpg" alt="">
							<div class="caption">
								<p>Nullam justo lacus, tristique vitae elementum eget, placerat eget eros. Etiam sodales, lectus eget pellentesque iaculis, odio velit auctor massa, non maximus eros libero in ante. Morbi sodales ligula eu orci commodo, ut tincidunt est aliquet. Morbi sit amet libero id eros sollicitudin pharetra. Duis hendrerit hendrerit ligula bibendum tempor. Quisque eu elit vitae felis varius sollicitudin. Pellentesque porta purus id porttitor consequat. Mauris sagittis est ante, in congue est vulputate tempus. In et neque nulla. Nam varius, mauris sagittis vulputate dignissim, ante dolor suscipit lacus, vitae aliquam nulla enim at nibh.</p>
							</div>
						</div>
					</div>
					<!-- Left and right controls -->
					<a class="project-left carousel-control" href="#projectCarousel" data-slide="prev"><img src="images/project-prev-arrow.png" alt=""></a>
					<a class="project-right carousel-control" href="#projectCarousel" data-slide="next"><img src="images/project-next-arrow.png" alt=""></a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Awards Image Modal end-->
<!-- Careers Modal Start-->
<div id="Modal_4" class="modal fade raq-lightbox careers-form" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<div class="modal-body">
				<h1>APPLY</h1>
				<div class="popup-form">
					<form action="#" class="material">
						<div class="pf-row">
							<input type="text" name="name" placeholder="Name" required>
						</div>
						<div class="pf-row">
							<input type="email" name="email" placeholder="Email" required>
						</div>
						<div class="pf-row">
							<input type="tel" name="mobile" placeholder="MOBILE <span>(For example +966 5X XXX XXXX)</span>">
						</div>
						<!--Note for programmer: Designer didn't design checkbox and radio button so checkbox and radio buttons are just for design.
						<div class="pf-row">
							<div class="checkbox-wrap">
								<label>Please select option</label>
								<div class="checkbox">
									<label><input type="checkbox" value="">Option 1</label>
								</div>
								<div class="checkbox">
									<label><input type="checkbox" value="">Option 2</label>
								</div>
								<div class="checkbox">
									<label><input type="checkbox" value="" disabled>Option 3</label>
								</div>
							</div>
						</div>
						<div class="pf-row">
							<label>Please select option</label>
							<div class="checkbox">
								<label><input type="radio" value="">Option 1</label>
							</div>
							<div class="checkbox">
								<label><input type="radio" value="">Option 2</label>
							</div>
							<div class="checkbox">
								<label><input type="radio" value="" disabled>Option 3</label>
							</div>
						</div>
						<!--Note End-->
						<div class="pf-row">
							<select name="mySelect" placeholder="Nationality">
								<option value="AF">Afghanistan</option>
								<option value="AX">Åland Islands</option>
								<option value="AL">Albania</option>
								<option value="DZ">Algeria</option>
								<option value="AS">American Samoa</option>
								<option value="AD">Andorra</option>
								<option value="AO">Angola</option>
								<option value="AI">Anguilla</option>
								<option value="AQ">Antarctica</option>
								<option value="AG">Antigua and Barbuda</option>
								<option value="AR">Argentina</option>
								<option value="AM">Armenia</option>
								<option value="AW">Aruba</option>
								<option value="AU">Australia</option>
								<option value="AT">Austria</option>
								<option value="AZ">Azerbaijan</option>
								<option value="BS">Bahamas</option>
								<option value="BH">Bahrain</option>
								<option value="BD">Bangladesh</option>
								<option value="BB">Barbados</option>
								<option value="BY">Belarus</option>
								<option value="BE">Belgium</option>
								<option value="BZ">Belize</option>
								<option value="BJ">Benin</option>
								<option value="BM">Bermuda</option>
								<option value="BT">Bhutan</option>
								<option value="BO">Bolivia, Plurinational State of</option>
								<option value="BQ">Bonaire, Sint Eustatius and Saba</option>
								<option value="BA">Bosnia and Herzegovina</option>
								<option value="BW">Botswana</option>
								<option value="BV">Bouvet Island</option>
								<option value="BR">Brazil</option>
								<option value="IO">British Indian Ocean Territory</option>
								<option value="BN">Brunei Darussalam</option>
								<option value="BG">Bulgaria</option>
								<option value="BF">Burkina Faso</option>
								<option value="BI">Burundi</option>
								<option value="KH">Cambodia</option>
								<option value="CM">Cameroon</option>
								<option value="CA">Canada</option>
								<option value="CV">Cape Verde</option>
								<option value="KY">Cayman Islands</option>
								<option value="CF">Central African Republic</option>
								<option value="TD">Chad</option>
								<option value="CL">Chile</option>
								<option value="CN">China</option>
								<option value="CX">Christmas Island</option>
								<option value="CC">Cocos (Keeling) Islands</option>
								<option value="CO">Colombia</option>
								<option value="KM">Comoros</option>
								<option value="CG">Congo</option>
								<option value="CD">Congo, the Democratic Republic of the</option>
								<option value="CK">Cook Islands</option>
								<option value="CR">Costa Rica</option>
								<option value="CI">Côte d'Ivoire</option>
								<option value="HR">Croatia</option>
								<option value="CU">Cuba</option>
								<option value="CW">Curaçao</option>
								<option value="CY">Cyprus</option>
								<option value="CZ">Czech Republic</option>
								<option value="DK">Denmark</option>
								<option value="DJ">Djibouti</option>
								<option value="DM">Dominica</option>
								<option value="DO">Dominican Republic</option>
								<option value="EC">Ecuador</option>
								<option value="EG">Egypt</option>
								<option value="SV">El Salvador</option>
								<option value="GQ">Equatorial Guinea</option>
								<option value="ER">Eritrea</option>
								<option value="EE">Estonia</option>
								<option value="ET">Ethiopia</option>
								<option value="FK">Falkland Islands (Malvinas)</option>
								<option value="FO">Faroe Islands</option>
								<option value="FJ">Fiji</option>
								<option value="FI">Finland</option>
								<option value="FR">France</option>
								<option value="GF">French Guiana</option>
								<option value="PF">French Polynesia</option>
								<option value="TF">French Southern Territories</option>
								<option value="GA">Gabon</option>
								<option value="GM">Gambia</option>
								<option value="GE">Georgia</option>
								<option value="DE">Germany</option>
								<option value="GH">Ghana</option>
								<option value="GI">Gibraltar</option>
								<option value="GR">Greece</option>
								<option value="GL">Greenland</option>
								<option value="GD">Grenada</option>
								<option value="GP">Guadeloupe</option>
								<option value="GU">Guam</option>
								<option value="GT">Guatemala</option>
								<option value="GG">Guernsey</option>
								<option value="GN">Guinea</option>
								<option value="GW">Guinea-Bissau</option>
								<option value="GY">Guyana</option>
								<option value="HT">Haiti</option>
								<option value="HM">Heard Island and McDonald Islands</option>
								<option value="VA">Holy See (Vatican City State)</option>
								<option value="HN">Honduras</option>
								<option value="HK">Hong Kong</option>
								<option value="HU">Hungary</option>
								<option value="IS">Iceland</option>
								<option value="IN">India</option>
								<option value="ID">Indonesia</option>
								<option value="IR">Iran, Islamic Republic of</option>
								<option value="IQ">Iraq</option>
								<option value="IE">Ireland</option>
								<option value="IM">Isle of Man</option>
								<option value="IL">Israel</option>
								<option value="IT">Italy</option>
								<option value="JM">Jamaica</option>
								<option value="JP">Japan</option>
								<option value="JE">Jersey</option>
								<option value="JO">Jordan</option>
								<option value="KZ">Kazakhstan</option>
								<option value="KE">Kenya</option>
								<option value="KI">Kiribati</option>
								<option value="KP">Korea, Democratic People's Republic of</option>
								<option value="KR">Korea, Republic of</option>
								<option value="KW">Kuwait</option>
								<option value="KG">Kyrgyzstan</option>
								<option value="LA">Lao People's Democratic Republic</option>
								<option value="LV">Latvia</option>
								<option value="LB">Lebanon</option>
								<option value="LS">Lesotho</option>
								<option value="LR">Liberia</option>
								<option value="LY">Libya</option>
								<option value="LI">Liechtenstein</option>
								<option value="LT">Lithuania</option>
								<option value="LU">Luxembourg</option>
								<option value="MO">Macao</option>
								<option value="MK">Macedonia, the former Yugoslav Republic of</option>
								<option value="MG">Madagascar</option>
								<option value="MW">Malawi</option>
								<option value="MY">Malaysia</option>
								<option value="MV">Maldives</option>
								<option value="ML">Mali</option>
								<option value="MT">Malta</option>
								<option value="MH">Marshall Islands</option>
								<option value="MQ">Martinique</option>
								<option value="MR">Mauritania</option>
								<option value="MU">Mauritius</option>
								<option value="YT">Mayotte</option>
								<option value="MX">Mexico</option>
								<option value="FM">Micronesia, Federated States of</option>
								<option value="MD">Moldova, Republic of</option>
								<option value="MC">Monaco</option>
								<option value="MN">Mongolia</option>
								<option value="ME">Montenegro</option>
								<option value="MS">Montserrat</option>
								<option value="MA">Morocco</option>
								<option value="MZ">Mozambique</option>
								<option value="MM">Myanmar</option>
								<option value="NA">Namibia</option>
								<option value="NR">Nauru</option>
								<option value="NP">Nepal</option>
								<option value="NL">Netherlands</option>
								<option value="NC">New Caledonia</option>
								<option value="NZ">New Zealand</option>
								<option value="NI">Nicaragua</option>
								<option value="NE">Niger</option>
								<option value="NG">Nigeria</option>
								<option value="NU">Niue</option>
								<option value="NF">Norfolk Island</option>
								<option value="MP">Northern Mariana Islands</option>
								<option value="NO">Norway</option>
								<option value="OM">Oman</option>
								<option value="PK">Pakistan</option>
								<option value="PW">Palau</option>
								<option value="PS">Palestinian Territory, Occupied</option>
								<option value="PA">Panama</option>
								<option value="PG">Papua New Guinea</option>
								<option value="PY">Paraguay</option>
								<option value="PE">Peru</option>
								<option value="PH">Philippines</option>
								<option value="PN">Pitcairn</option>
								<option value="PL">Poland</option>
								<option value="PT">Portugal</option>
								<option value="PR">Puerto Rico</option>
								<option value="QA">Qatar</option>
								<option value="RE">Réunion</option>
								<option value="RO">Romania</option>
								<option value="RU">Russian Federation</option>
								<option value="RW">Rwanda</option>
								<option value="BL">Saint Barthélemy</option>
								<option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
								<option value="KN">Saint Kitts and Nevis</option>
								<option value="LC">Saint Lucia</option>
								<option value="MF">Saint Martin (French part)</option>
								<option value="PM">Saint Pierre and Miquelon</option>
								<option value="VC">Saint Vincent and the Grenadines</option>
								<option value="WS">Samoa</option>
								<option value="SM">San Marino</option>
								<option value="ST">Sao Tome and Principe</option>
								<option value="SA">Saudi Arabia</option>
								<option value="SN">Senegal</option>
								<option value="RS">Serbia</option>
								<option value="SC">Seychelles</option>
								<option value="SL">Sierra Leone</option>
								<option value="SG">Singapore</option>
								<option value="SX">Sint Maarten (Dutch part)</option>
								<option value="SK">Slovakia</option>
								<option value="SI">Slovenia</option>
								<option value="SB">Solomon Islands</option>
								<option value="SO">Somalia</option>
								<option value="ZA">South Africa</option>
								<option value="GS">South Georgia and the South Sandwich Islands</option>
								<option value="SS">South Sudan</option>
								<option value="ES">Spain</option>
								<option value="LK">Sri Lanka</option>
								<option value="SD">Sudan</option>
								<option value="SR">Suriname</option>
								<option value="SJ">Svalbard and Jan Mayen</option>
								<option value="SZ">Swaziland</option>
								<option value="SE">Sweden</option>
								<option value="CH">Switzerland</option>
								<option value="SY">Syrian Arab Republic</option>
								<option value="TW">Taiwan, Province of China</option>
								<option value="TJ">Tajikistan</option>
								<option value="TZ">Tanzania, United Republic of</option>
								<option value="TH">Thailand</option>
								<option value="TL">Timor-Leste</option>
								<option value="TG">Togo</option>
								<option value="TK">Tokelau</option>
								<option value="TO">Tonga</option>
								<option value="TT">Trinidad and Tobago</option>
								<option value="TN">Tunisia</option>
								<option value="TR">Turkey</option>
								<option value="TM">Turkmenistan</option>
								<option value="TC">Turks and Caicos Islands</option>
								<option value="TV">Tuvalu</option>
								<option value="UG">Uganda</option>
								<option value="UA">Ukraine</option>
								<option value="AE">United Arab Emirates</option>
								<option value="GB">United Kingdom</option>
								<option value="US">United States</option>
								<option value="UM">United States Minor Outlying Islands</option>
								<option value="UY">Uruguay</option>
								<option value="UZ">Uzbekistan</option>
								<option value="VU">Vanuatu</option>
								<option value="VE">Venezuela, Bolivarian Republic of</option>
								<option value="VN">Viet Nam</option>
								<option value="VG">Virgin Islands, British</option>
								<option value="VI">Virgin Islands, U.S.</option>
								<option value="WF">Wallis and Futuna</option>
								<option value="EH">Western Sahara</option>
								<option value="YE">Yemen</option>
								<option value="ZM">Zambia</option>
								<option value="ZW">Zimbabwe</option>
							</select>
						</div>
						<div class="pf-row">
							<div class="fileUploader">
								<input id="txt" type = "text" value = "CV / RESUME" onclick ="javascript:document.getElementById('file').click();">
								<input id="txt-btn" type = "button" value = "BROWSE" onclick ="javascript:document.getElementById('file').click();">
								<input id = "file" type="file" style='visibility: hidden;' name="img" onchange="ChangeText(this, 'txt');"/>
							</div>
						</div>
						<div class="pf-row">
							<div class="pf-col-left"><img src="images/recaptcha-img.png" alt="">
							</div>
							<div class="pf-col-right"><input type="submit" value="Apply">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Careers Us Modal Modal end-->
<!-- Success Message for all forms Modal Start-->
<div id="Modal_5" class="modal fade raq-lightbox success-modal" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<div class="modal-body">
				<h1>SUCCESS</h1>
				<div class="success-msg">
					<p>YOUR APPLICATION HAS BEEN SENT SUCCESSFULLY</p>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Success Message Us Modal Modal end-->
<!-- Newsletter Modal Start-->
<div id="Modal_6" class="modal fade raq-lightbox newsletter-modal" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<div class="modal-body">
				<h1>NEWSLETTER</h1>
				<div class="popup-form">
					<form action="#" class="material">
						<div class="pf-row">
							<input type="email" name="email" placeholder="Email" required>
						</div>
						<div class="pf-row">
							<input type="submit" value="SIGN UP">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Newsletter  Modal end-->
</body>
</html>