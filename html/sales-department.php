<?php include('header.php'); ?>
<main class="main">
	<div class="container">
		<section class="content-wrap">
			<div class="left-sidebar">
				<ul>
					<li class="active"><a href="marketing-department.php">MARKETING DEPARTMENT</a></li>
					<li><a href="tecnology.php">TECHNOLOGY</a></li>
					<li><a href="business-development.php">BUSINESS DEVELOPMENT</a></li>
					<li><a href="internships.php">INTERNSHIPS</a></li>
					<li><a href="sales-department.php">SALES DEPARTMENT</a></li>
				</ul>
			</div>
			<div class="right-content career-wrap">
				<h1>BUSINESS DEVELOPMENT</h1>
				<div class="career-row">
					<div class="career-col">
						<h2>BUSINESS DEVELOPMENT DIRECTOR <span>JEDDAH, SAUDI ARABIA</span></h2>
						<p>Nullam justo lacus, tristique vitae elementum eget, placerat eget eros. Etiam sodales, lectus eget pellentesque iaculis, odio velit auctor massa, non maximus eros libero in ante.</p>
						<ul>
							<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
							<li>Suspendisse at velit dictum, tempor urna eget.</li>
							<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
							<li>Suspendisse at velit dictum, tempor urna eget.</li>
						</ul>
						<div class="apply-btn"><a href="#." data-toggle="modal" data-target="#Modal_4">APPLY</a></div>
					</div>
					<div class="career-col">
						<h2>BUSINESS DEVELOPMENT DIRECTOR <span>JEDDAH, SAUDI ARABIA</span></h2>
						<p>Nullam justo lacus, tristique vitae elementum eget, placerat eget eros. Etiam sodales, lectus eget pellentesque iaculis, odio velit auctor massa, non maximus eros libero in ante.</p>
						<ol>
							<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
							<li>Suspendisse at velit dictum, tempor urna eget.</li>
							<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
							<li>Suspendisse at velit dictum, tempor urna eget.</li>
						</ol>
						<div class="apply-btn"><a href="#." data-toggle="modal" data-target="#Modal_5">APPLY</a></div>
						<!--Note for programmer: "Modal_5" is for success message. when user submit career form the "Modal_5" will show.-->
					</div>
					<div class="career-col">
						<h2>BUSINESS DEVELOPMENT DIRECTOR <span>JEDDAH, SAUDI ARABIA</span></h2>
						<p>Nullam justo lacus, tristique vitae elementum eget, placerat eget eros. Etiam sodales, lectus eget pellentesque iaculis, odio velit auctor massa, non maximus eros libero in ante.</p>
						<h3>BUSINESS DEVELOPMENT DIRECTOR</h3>
						<ul>
							<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
							<li>Suspendisse at velit dictum, tempor urna eget.</li>
							<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
							<li>Suspendisse at velit dictum, tempor urna eget.</li>
						</ul>
						<div class="apply-btn"><a href="#." data-toggle="modal" data-target="#Modal_4">APPLY</a></div>
					</div>
					<div class="career-col">
						<h2>BUSINESS DEVELOPMENT DIRECTOR <span>JEDDAH, SAUDI ARABIA</span></h2>
						<p>Nullam justo lacus, tristique vitae elementum eget, placerat eget eros. Etiam sodales, lectus eget pellentesque iaculis, odio velit auctor massa, non maximus eros libero in ante.</p>
						<h4>BUSINESS DEVELOPMENT DIRECTOR</h4>
						<ul>
							<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
							<li>Suspendisse at velit dictum, tempor urna eget.</li>
							<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
							<li>Suspendisse at velit dictum, tempor urna eget.</li>
						</ul>
						<div class="apply-btn"><a href="#." data-toggle="modal" data-target="#Modal_4">APPLY</a></div>
					</div>
				</div>
			</div>
		</section>
	</div>
</main>
<?php include('footer.php'); ?>