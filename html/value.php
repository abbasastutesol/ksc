<?php include('header.php'); ?>
<main class="main">
	<div class="container">
		<section class="content-wrap">
			<div class="left-sidebar">
				<ul>
					<li><a href="company-profile.php">PROFILE</a></li>
					<li><a href="vision-mission.php">VISION &amp; MISSION</a></li>
					<li><a href="chairmans-message.php">CHAIRMAN’S MESSAGE</a></li>
					<li><a href="ceo-message.php">CEO’S MESSAGE</a></li>
					<li class="active"><a href="value.php">VALUE</a></li>
					<li><a href="awards.php">AWARDS</a></li>
				</ul>
			</div>
			<div class="right-content">
				<div class="large-thumbnail"><img src="images/value-img.png" alt=""></div>
				<h1>VALUE</h1>
				<h2>Committed to total customer satisfaction</h2>
				<p>We listen to the needs of our clients and provide them with honest advice. We deliver quality solutions and remain engaged until our commitment is fulfilled.</p>
				<h2>Honesty with our suppliers</h2>
				<p>We act as honest brokers for our suppliers. We represent their interest and preserve their image in the market.</p>
				<h2>Our People</h2>
				<p>Our people are our most valuable asset. We believe in attracting, developing and encouraging talent. We make their success our Job No 1.</p>
				<h2>Professionalism</h2>
				<p>We remain close to our markets through our branches and the network of professional dealers across the territories we cover. We maintain adequate inventories. We strive to meet our deadlines and we take pride in our jobs.</p>
			</div>
		</section>
	</div>
</main>
<?php include('footer.php'); ?>